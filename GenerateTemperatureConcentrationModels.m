function [deltaC, deltaT, xx, rr,ind_x_orifice, ind_r_centerline] = GenerateTemperatureConcentrationModels(params)
    %TODO: I don't use U0 except for Re check....
    %%default Inputs
    calcFromVirtualOrigin = false;
    displayGraphs = false;
    D = 1e-2; % Nozzle's diameter, [m]
    tot_mfr = 2; % Total mass flow rate, [g/s]
    co2_mfr = 0.5; % CO2 mass flow rate, [g/s]
    Tamb_C = 25; % ambient temperature, [C]
    Tjet_C = 400; %Jet's outlet temperature, [C]
    Camb = 0;%0.0004; % CO2 concentration in the ambient
    P0 = 101325; %[Pa]. Pressure @ outlet is 1 atm = 101325[Pa]
    num_x_diameters = 20; % #diameters to take for x axis
    num_r_diameters = 10; % #diameters to take for r axis in each side
    diameter_fraction_step = 0.5;
    if nargin>0
      calcFromVirtualOrigin = params.calcFromVirtualOrigin;
      displayGraphs = params.displayGraphs;
      D = params.D;
      tot_mfr = params.tot_mfr; % Total mass flow rate, [g/s]
      co2_mfr = params.co2_mfr; % CO2 mass flow rate, [g/s]
      Tamb_C = params.Tamb_C; % ambient temperature, [C]
      Tjet_C = params.Tjet_C; %Jet's outlet temperature, [C]
      Camb = params.Camb;% CO2 concentration in the ambient
      P0 = params.P0; %[Pa]. Pressure @ outlet
      num_x_diameters = params.num_x_diameters; % #diameters to take for x axis
      num_r_diameters = params.num_r_diameters; % #diameters to take for r axis in each side
      diameter_fraction_step = params.diameter_fraction_step;
    end
    
    %Constants
    R = 8.314459848; % universal gas constant [J/mol/K]
    molar_mass_air = 28.9645; %[g/mol]
    moalr_mass_co2 = 44.01; %[g/mol]
    
    % Auxiliary functions
    C2K = @(T) T + 273.15; % Celsius to Kelvin
    rhoIdealGas = @(p,T,mol_mas) p/(R/mol_mas*1e3*T); % Ideal gas equation, T[K], p[Pa], mol_mas[g/mol]
    %%
    Tamb = C2K(Tamb_C);
    Tjet = C2K(Tjet_C);
    
    A = pi/4*D^2; %Nozzle's area
    C0 = co2_mfr/tot_mfr;
    
    [mu_co2, mu_air] = CalcViscousity(Tjet); % get the viscousity in the jet's temp
    
    mu_mass_avged = ( co2_mfr*mu_co2 + (tot_mfr-co2_mfr)*mu_air ) / tot_mfr;% Mass-averaged viscousity
    
    rho_air = rhoIdealGas(P0,Tjet,molar_mass_air);
    rho_co2 = rhoIdealGas(P0,Tjet,moalr_mass_co2);
    %%% I can take just mu_air, instead of mu_mass_avged. ~1.5% error
    rho_avged = C0*rho_co2 + (1-C0)*rho_air;%averaged density [kg/m^3]
    U0 = tot_mfr*1e-3/A/rho_avged; % Outlet velocity [m/s]
    Re = rho_avged*U0*D / mu_mass_avged % Reynolds number in the orifice
    Re_alternative = tot_mfr*1e-3/A*D/mu_mass_avged; % since m_dot = rho*U*A
    
    g = 9.8; %gravity [m/s^2]
    beta = 1/Tjet; % thermal expansion coefficient for ideal gas
    ni = mu_mass_avged/rho_avged; % kinematic viscosity [m^2/s]
    Gr = g*beta*(Tjet-Tamb)*D^3/ni^2;% Grashof number
    Ar = Gr/Re^2; %Archimedes number, to compare buoyant to inertial forces
    if Ar > 1e-4
      warning('Buoyancy might not be neglegable!');
    end
    
%     alpha_air = 2.2e-5; %thermal diffusivity [m^2/s]  of air @ T=300K
%     Pr = ni/alpha_jet % Prandtl number - ratio of momentum and thermal diffusivites
    Pr = 0.71;
    Pe = Re*Pr; % Peclet number - advection to diffusion ratio in forced convection
    
    
    x = D*(0: diameter_fraction_step: num_x_diameters); % Note: x=0 is the virtual origin 
    r = D*(-num_r_diameters: diameter_fraction_step: num_r_diameters);
    
    x_orifice = 5*D/2; % orifice is 5D/2 from the virtual origin x=0
    ind_x_orifice = find(x==x_orifice);
    ind_r_centerline = find(r==0);
    [xx, rr] = meshgrid(x, r);
    
    xx = xx(:,ind_x_orifice:end);
    rr = rr(:,ind_x_orifice:end);
%     deltaC = deltaC(:,ind_x_0:end);
%     deltaT = deltaT(:,ind_x_0:end);
    
    %% Calc the profiles based on "Enviromental Fluid Mechanics" ch.9.
    %% There, a universal opening angle of ~11.8 deg is used
    deltaC = 0.5*5*D./xx*(C0-Camb).*exp(-50*rr.^2./xx.^2); % 0.5 factor was added to fix orifice's value to C0 
   
%     C = 5*D./xx.*exp(-50*rr.^2./xx.^2);
%     C = exp(-50*rr.^2./xx.^2);

%     deltaT = 0.5*5*D./xx.*(Tjet-Tamb).*exp(-50*rr.^2./xx.^2);

    % "Convective Heat Transfer", 4th edition, p.411 :
    % T-T8 = (Tc-T8) * exp(-(r/bT)^2)
    % Where: bT ~ 0.127x
    %        Tc-T8 =~ 5.65(T0-T8)*D/x
    deltaT = 0.5*5.65*D./xx.*(Tjet-Tamb).*exp(-62*rr.^2./xx.^2);
    % Thus, the virtual origin is -3.937D ~ -4D from the orifice.
        
    %% Display
    if displayGraphs
      xx = xx/D; rr = rr/D; % for display
      surf(xx,rr,deltaC); hold on;
      plot3(x_orifice/D, 0, deltaC(ind_r_centerline,ind_x_orifice), 'r*');
      xlabel('x/D'); ylabel('r/D'); zlabel('\DeltaC');
      title('Concentration distribution');
      
      figure;
      surf(xx,rr,deltaT); hold on;
      plot3(x_orifice/D, 0, deltaT(ind_r_centerline,ind_x_orifice), 'r*');
      xlabel('x/D'); ylabel('r/D'); zlabel('\DeltaT[C]');
      title('Temperature distribution');
    end
end