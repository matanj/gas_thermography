function compare_spectra_to_mySpectra
% Compare spectra from: 
% http://hitran.iao.ru/
SELECT_TAU_OR_R_IS_TAU = true; % 'true' for tau, 'false' for R
SELECT_CO2_OR_H2O_IS_CO2 = true; % 'true' for CO2, 'false' for H2O
%% Assure below match reference file:
hitranParams.wn_step = 0.01; % HITRAN's wavenumber step [cm^-1]
hitranParams.min_wn = 2000; % HITRAN's minimal wavenumber [cm^-1]
hitranParams.max_wn = 3333; % HITRAN's maximal wavenumber [cm^-1]
hitranParams.HW_to_eval = 500; % Number of HWHM for Voigt evaluation. Orignally 6000 by Ilya. 500 also good enough.
hitranParams.min_uncertainty_code = '000000';%'342222';% minimal uncertainty 6-digit code, see http://hitran.org/docs/uncertainties/
hitranParams.line_strength_th = 1e-28; % Line stregth threshold.
hitranParams.hitranFileFullPath = 'C:\Users\matanj\Documents\Repositories\gas_thermography\pixelwise_calibration\data\hitranCO2_H2O_2000_3333_main_isotopologs.par';
%% Atmosphere params:
atm_params.OPL = 1; % Atmospheric path [cm]
atm_params.co2_concentration = 1; % Atmospheric CO2 concentration [fraction]
atm_params.T = 596; % Atmospheric temperature [K]
atm_params.RH = 0; % Atmospheric Water Relative Humidity (RH) [%]
atm_params.pressure_tot = 1; % [atm]
%% Ref file:
baseDir = 'C:\Users\matanj\Downloads\SpectrMol_tran_CO2_2cm.txt';
[ref_file, ref_dir] = uigetfile({'*.txt','Reference spectra file'},'Select reference file',baseDir);
fileFullPath = fullfile(ref_dir, ref_file);
fid_r = fopen(fileFullPath,'r');
ref_data = textscan(fid_r, ['%s'], 'delimiter', '','whitespace', '', 'CommentStyle','#');
fclose(fid_r);
ref_data = split(ref_data{:});
if size(ref_data,2) > 2
  ref_data = ref_data(:,2:3);
end
ref_data = cellfun(@str2num, ref_data, 'UniformOutput', false);
ref_data = cell2mat(ref_data);

if SELECT_TAU_OR_R_IS_TAU
  [wn_vect, ~, spectra_wn] = MyGetSpectrumGPU(~SELECT_CO2_OR_H2O_IS_CO2, 'tau', hitranParams, atm_params);
  ylabel_str = 'Transmittance';
else
  [wn_vect, ~, spectra_wn] = MyGetSpectrumGPU(~SELECT_CO2_OR_H2O_IS_CO2, 'radiance', hitranParams, atm_params);
  % convert to their units:
   % L_BB = 2�pi�h�c�WN^3 / (exp(h�c�WN/(k�T)) - 1) in  [erg�c-1�cm-2�Hz-1]
   % whereas my calc doen't have pi and in [J].
  spectra_wn = pi * 1e7 * spectra_wn;
  ylabel_str = 'Total Radiance [erg�c-1�cm-2�Hz-1]';
end

% % TODO: remove:
% wn_vect = wn_vect(1:end-1);
% spectra_wn = spectra_wn(2:end);

if SELECT_CO2_OR_H2O_IS_CO2
  title_str = 'CO_2: ';
else
  title_str = 'H_2O: ';
end
figure; plot(ref_data(:,1),ref_data(:,2),'r'); 
hold on; plot(wn_vect,spectra_wn,'b'); legend('ref', 'calc');
title([title_str,'T = ',num2str(atm_params.T),'K, L = ' num2str(atm_params.OPL),'cm']);
xlabel('wn [cm^-1]');
ylabel(ylabel_str);

sum_ref = sum(ref_data(:,2)) %/ (ref_data(end,1) - ref_data(1,1) )
sum_calc = sum(spectra_wn) %/ (wn_vect(end) - wn_vect(1) )
end