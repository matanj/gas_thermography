function PiecewiseAvg(wnArray,intensity_WN,wlResolutions,data_info)
    %% Intro
    %{
    Piecewise averaging script for HITRAN data
    Written by Iliya on 27/03/14
    Notations: wl = wavelenght, wn = wavenumber
    INPUTS:
         wnArray - Array of wavenumbers for which the intensities are given.
    intensity_WN - Array of line intensities
    %}  
    %% Initialization
    wnArray=flipud(wnArray); intensity_WN=flipud(intensity_WN);

    minWavelength = floor(wnArray(1)^-1*1E7);
    maxWavelength = ceil(wnArray(end)^-1*1E7);
    WNStep = abs(wnArray(2)-wnArray(1));

    %% Converting to wavelengths
    wlArray = wnArray.\1E7; 
    % Converting the Intensity
    intensity_WL = (wnArray.^2) .* intensity_WN; 
    clear intensity_WN wnArray;
    %% Piecewise averaging
    AvgsDB = struct('Radiance',[],'WaveLength',[]); cnt=1;
    for WLStep = wlResolutions
        flag=0;
        % Note: the +-wavelengthStep/2 is done so that the center of the bin
        % corresponds to the desired wavelength. Then we have as many bins as
        % wavelengths. The last element isn't divided by 2 so that the last 
        % bin exists even if the WLarray doesn't exactly accomodate a whole
        % number of "WLStep"s.
        binBorders = (minWavelength-WLStep/2:WLStep:maxWavelength+WLStep)';     
        % Find the approppriate bins:
        [numInBin,whichBin]=histc(wlArray,binBorders);
        
        if numInBin(end)==0
            if numInBin(end-1)==0 %side effect of last bin
                numInBin(end-1:end)=[];
                flag=1;
            else
                numInBin(end)=[];
            end
        else
            warning(['There were ' num2str(numInBin(end)) ' values unassigned to bins']);
        end
        if sum(numInBin)<numel(wlArray)
            warning(['There were ' num2str(numel(wlArray)-sum(numInBin)) ' values unassigned to bins']);            
        end
        AvgsDB(cnt).Radiance   = accumarray(whichBin,intensity_WL)./numInBin;
        AvgsDB(cnt).WaveLength = binBorders(1:end-1)'+WLStep/2; 
        if flag %another side effect of last bin
            AvgsDB(cnt).WaveLength(end) = [];
        end
        cnt=cnt+1;
    end
    AvgsDB(cnt).Radiance = intensity_WL; AvgsDB(cnt).WaveLength = wlArray;
    clear intensity_Lambda numInBin whichBin binBorders cnt
    %% "Postprocessing"
    hFig=figure(99); set(hFig,'Visible','off');
    hold all; grid on; set(gca, 'GridLineStyle', '-'); grid(gca,'minor');
    for ind1=1:length(wlResolutions)
        plot(AvgsDB(ind1).WaveLength,AvgsDB(ind1).Radiance,'DisplayName',...
            [num2str(wlResolutions(ind1)) 'nm step'],'LineWidth',2.0);
    end
    % plot(AvgsDB(end).WaveLength,AvgsDB(end).Radiance,'DisplayName','Unaveraged');
    legend('-DynamicLegend','Location','NorthEast');
    xlabel('Wavelength [nm]','FontSize',24);
    ylabel('Radiance [W * sr^{-1} * cm^{-2} * nm^{-1}]','FontSize',24);
    title({['Piecewise averaged HITRAN ' data_info.spectrumType ' spectrum data for:'], ...
          [data_info.GasComposition ', ' data_info.Temp 'K, '...
           data_info.Pres 'atm, WN step: ' WNStep ...
           'cm^{-1}, Opt. path len.: ' data_info.OpticalPath 'm']},'FontSize',20);
    set(gca,'FontSize',18);
    % custom datatip
    dcm_obj = datacursormode(hFig);
    set(dcm_obj,'enable','on'); set(dcm_obj,'UpdateFcn',@datatipWL)
    % Make Visible
    set(hFig,'Visible','on');
    maximize;
end

function output_txt = datatipWL(~,event_obj)
    % Display the position of the data cursor
    % obj          Currently not used (empty)
    % event_obj    Handle to event object
    % output_txt   Data cursor text string (string or cell array of strings).

    pos = get(event_obj,'Position');
    output_txt = {['\lambda: ',num2str(pos(1),4)],...
        ['I(\lambda): ',num2str(pos(2),4)]};

    set(0,'ShowHiddenHandles','on');                       % Show hidden handles
    hText = findobj('Type','text','Tag','DataTipMarker');  % Find the data tip text
    set(0,'ShowHiddenHandles','off');                      % Hide handles again
    set(hText,'Interpreter','tex');                        % Change the interpreter
end