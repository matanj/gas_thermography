function aStruct = putCellsInStructArrayField(aStruct,varargin)
 %% Check struct
 if ~isstruct(aStruct)
     error('first input supplied is not a struct.')
 end % if
 
 if sum(size(aStruct)>1)>1 % if more than one non-singleton dimension
     error('I don''t want to sort your multidimensional struct array.')
 end % if
 %% Check varagin length
 nvargin=length(varargin);
 if mod(nvargin,2)
     error('Incorrect amount of passed-in variables - should be fieldname-valueCell pairs!');
 end
 %% Dealing with fieldname-valueCell pairs
 for ind1=1:2:nvargin
 %% Check fieldnames
     if ~isfield(aStruct, varargin{ind1}) % if fieldNamesCell is a simple string of a valid fieldname
         error(['''' varargin{ind1} ''' isn''t a valid fieldname in the struct.']);
     elseif ~iscell(varargin{ind1+1})
         error(['2nd input of pair number [' num2str(ind1) '] is not a cell array.'])
     end % if isfield
     %assuming the lengths are correct
     [aStruct.(varargin{ind1})]=deal(varargin{ind1+1}{:});
 end
end