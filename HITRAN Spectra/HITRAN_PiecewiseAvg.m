%% Intro
% Piecewise averaging script for HITRAN absorption data, retreivable from
% http://hitran.iao.ru/gasmixture/spectr
% Written by Iliya on 27/03/14
%% Initialization
fclose('all'); clear variables; close all; clc; 
%% Definitions
Path = 'CO2_600k_HITRAN.absorp'; % cd(Path);
delim = '  ';
definedOpticalPath = 1E-2;
desiredWavelengthResolutions = [5,10]; %in nm
%% Loading
fid = fopen(Path,'r');
% DEBUG: fseek(fid,0,'bof');
hdr_lines=textscan(fid,'%s',4,'delimiter','\n');
txt_lines=textscan(fid,['%f' delim '%f'],'delimiter','\n','HeaderLines',1);
          fclose(fid);
waveNumber=flipud(txt_lines{1}); intensity_W=flipud(txt_lines{2});
% Parse header
if 1
    %% Rearrange header:
    hdr = regexp(hdr_lines{1}{3},'[# ]','split');
    hdr(cellfun(@isempty, hdr))=[];
    %% Extract values
    tmp = regexp(hdr{3},'-','split');
    loaded_minWN = str2double(tmp{1}); loaded_maxWN=str2double(tmp{2}); % cm^-1
    loaded_Temp = str2double(hdr{6});  % K
    loaded_Pres = str2double(hdr{9});  % atm
    loaded_WNStep = str2double(hdr{12}); % cm^-1
    loaded_Contour = hdr{17}(1:end-1);  
    tmp = hdr_lines{1}{2}(16:end);
    tmp = regexprep(tmp,'<sub>','_{');
    loaded_GasComposition = regexprep(tmp,'</sub>','}');
end
minWavelength  = round(loaded_maxWN^-1*1E5) *1E2;
maxWavelength  = round(loaded_minWN^-1*1E5) *1E2;

clear txt_lines delim Path fid;
%% Converting to wavelengths
waveLength = (1./waveNumber)*1E7; 
% Converting the Intensity
intensity_Lambda = (waveNumber.^2) .* intensity_W; 
clear intensity_W waveNumber;
%% Piecewise averaging
AvgsDB = struct('Radiance',[],'WaveLength',[]); cnt=1;
for WLStep = desiredWavelengthResolutions
    % Note: the +-wavelengthStep/2 is done so that the center of the bin
    % corresponds to the desired wavelength. Then we have as many bins as
    % wavelengths.
    binBorders = (minWavelength-WLStep/2:WLStep:maxWavelength+WLStep/2)';
    % Find the approppriate bins:
    [numInBin,whichBin]=histc(waveLength,binBorders);
    if numInBin(end)==0
        numInBin(end)=[];
    else
        warning(['There were ' num2str(numInBin(end)) 'values unassigned to bins']);
    end
    AvgsDB(cnt).Radiance   = accumarray(whichBin,intensity_Lambda)./numInBin;
    AvgsDB(cnt).WaveLength = binBorders(1:end-1)'+WLStep/2; 
    cnt=cnt+1;
end
AvgsDB(cnt).Radiance = intensity_Lambda; AvgsDB(cnt).WaveLength = waveLength;
clear intensity_Lambda numInBin whichBin binBorders cnt
%% "Postprocessing"
hFig=figure(101); set(hFig,'Visible','off');
hold all; grid on; set(gca, 'GridLineStyle', '-'); grid(gca,'minor');
for ind1=1:length(desiredWavelengthResolutions)
    plot(AvgsDB(ind1).WaveLength,AvgsDB(ind1).Radiance,'DisplayName',...
        [num2str(desiredWavelengthResolutions(ind1)) 'nm step'],'LineWidth',2.0);
end
% plot(AvgsDB(end).WaveLength,AvgsDB(end).Radiance,'DisplayName','Unaveraged');
legend('-DynamicLegend','Location','NorthEast');
xlabel('Wavelength [nm]','FontSize',24);
ylabel('Radiance [W * sr^-1 * cm^-2 * nm^-1]','FontSize',24);
title({'Piecewise averaged HITRAN radiance spectrum data for:', ...
      [loaded_GasComposition ', ' num2str(loaded_Temp) 'K, '...
       num2str(loaded_Pres) 'atm, WN step: ' num2str(loaded_WNStep) ...
       'cm^-^1, Countour type: ' loaded_Contour ', OP: '...
       num2str(definedOpticalPath,'%2.1E') 'm']},'FontSize',20);
set(gca,'FontSize',18);
% custom datatip
dcm_obj = datacursormode(hFig);
set(dcm_obj,'enable','on'); set(dcm_obj,'UpdateFcn',@datatipWL)
% Make Visible
set(hFig,'Visible','on');
% Maximize
drawnow % Required to avoid Java errors
jFig = get(handle(hFig), 'JavaFrame'); 
jFig.setMaximized(true);