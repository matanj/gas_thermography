function out = Voigt(p_tot,p_self,gamma_air_REF,gamma_self_REF,T,n,...
                     Wavenumber, mWeight,WN_array)
%% Initialization
% close all force; fclose('all'); clear variables; clc; 
% dbstop if error;
%{
DEBUG:
ANS=Voigt(1,0.1,0.0687,0.077, 600, 0.71, 2500.001640, 44,1000);
Verified against:
    http://www.mathworks.com/matlabcentral/fileexchange/45058-deconvolution-mordenite-zeolite/
    by Nikolay Cherkasov, 16 Jan 2014.
Using:
    load('data4VNC.mat'); ANS1=voigt_NC(x,0,HW_D,HW_L);
%}
%% Constants
kB = 8314.462175; % Bolzmann's constant - [mJ]/[mol*K]
c = 299792458; % [m]/[s]
l4 = log(4);
%% Definitions
T_REF = 296; % [K]
% mCO2= 16+12+16; %[g]/[mol] 
%% Lorentz
% HW_L = gamma_air_REF*(p_tot-p_self)*(T_REF/T)^n + gamma_self_REF * p_self; %n is apparently "n_air"
HW_L = (gamma_air_REF * (p_tot-p_self) + gamma_self_REF * p_self)*(T_REF/T)^n;
%% Doppler
HW_D = sqrt(kB*l4/c^2)*sqrt(T/mWeight)*Wavenumber;
sig_D = HW_D/sqrt(l4);
%% Voigt Evaluation
% x = [(nu - nu_0) / HW_D] * sqrt(ln(2))
x = WN_array - Wavenumber; % WN_array is assumed to be of the desired resolution.
z = (x + 1i*HW_L)./(sig_D*sqrt(2));
out = (real(Faddeeva_w(z))/(sig_D*sqrt(2*pi)));

% out.evals = (real(Faddeeva_w(z))/(sig_D*sqrt(2*pi)));
%{
%% Absorption coefficient calculation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%        -----Ripoff from <Peter T. S. DeVore> starts here-----           %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Voigt halfwidth approx.:
out.HW_V = (0.5346 * HW_L) + sqrt(0.2166 * (HW_L ^ 2) + (HW_D ^ 2));
% Absorption cross-section
out.x = HW_L / out.HW_V;
out.y = abs(WN_array - Wavenumber) / out.HW_V;
%}