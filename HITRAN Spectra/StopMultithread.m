function StopMultithread
% StopMultithread checks whether a parallel-pool is open, and deletes it so.
%% Query the size of the current pool, if exists (updated for MATLAB 2014a)
poolobj = gcp('nocreate'); % If no pool, do not create new one.
if isempty(poolobj)
    disp('No parpool open. Returning...');
    return;
else
    poolobj.delete;
end