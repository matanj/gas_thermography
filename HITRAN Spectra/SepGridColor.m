function SepGridColor(majorX, majorY, minorX, minorY,majSty,minSty)
% Taken from: http://stackoverflow.com/a/17094350/3372061
ax1 = gca;   %# get a handle to first axis

%# create a second transparent axis, same position/extents, same ticks and labels
ax2 = copyobj(ax1,gcf);
ax3 = copyobj(ax1,gcf);

delete(get(ax2,'Children'));
delete(get(ax3,'Children'));

set(ax2, 'Color','none', 'Box','off','YTickLabel',[],'YTickLabel',[],...
    'GridLineStyle', majSty,...
    'XGrid','on','YGrid','on',...
    'XMinorGrid','off','YMinorGrid','off',...
    'XColor',majorX,'YColor',majorY);
set(ax3,'Box','off','YTickLabel',[],'YTickLabel',[],...
    'MinorGridLineStyle',minSty,...
    'XGrid','off','YGrid','off',...
    'XMinorGrid','on','YMinorGrid','on',...
    'XColor',minorX,'YColor',minorY);

set(ax1, 'Color','none', 'Box','on')

handles = [ax3; ax2; ax1];
c = get(gcf,'Children');
for i=1:length(handles)
    c = c(c ~= handles(i));
end
set(gcf,'Children',[c; flipud(handles)]);

linkaxes([ax1 ax2 ax3]);
end