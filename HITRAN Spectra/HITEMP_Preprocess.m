%% Initialization
close all force; fclose('all'); clear variables; clc; 
dbstop if error;
%{
DEBUG:

%}
%% Constants

%% Definitions
wnFiltering = false; %if false, the following 2 values are meaningless
wnMin = 2250; %cm^-1 , ~4400nm
wnMax = 2500; %cm^-1 , ~4150nm

molecule_index_to_keep = 2;
isotopologues_to_keep = 1;
number_of_lines_to_read_per_pass = 5000000; %it is possible to reduce this
%{
DB FIELDS:
molecule_number
isotopologue_number
line_center_wavenumber_1_per_cm
line_strength_at_reference_temperature_cm_per_molecule
A_coefficient_1_per_s
air_broadened_half_width_1_per_cm_per_atm
self_broadened_half_width_1_per_cm_per_atm


air_pressure_induced_line_shift_1_per_cm_per_atm
%}
%% Select file
[FileName,PathName] = uigetfile('*.par','Please select an unfiltered HITRAN\HITEMP DB file','I:\');
% if strcmpi(FileName(end-3:end),'mat');
%    disp('You have selected a .MAT file, no processing needed.'); 
% end
%% Open files for reading & writing
fid_r = fopen([PathName FileName],'r');
fid_w = fopen([PathName FileName 'F'],'wt');
%% Load DB into memory
display('Reading from DB...');
% while(true)
try    
%% Read data
    feature('DefaultCharacterSet','UTF8');
    HITRAN_data = textscan(fid_r, ...
         ['%2c' '%1u' '%1c' '%11f' '%145c'], number_of_lines_to_read_per_pass, 'delimiter', '', ...
            'whitespace', '');
    HITRAN_data{1} = str2num(HITRAN_data{1}); %#ok<ST2NM>
    if ~all(HITRAN_data{3}==32)% 32 is the ascii value for space
      disp('It is unsafe to remove the 3rd cell...'); %may happen when very large WNs are taken.
    end 
    HITRAN_data(3) = [];
    display('Reading finished successfully!');
catch
    display('Out of memory (probably...). Try uncommenting the loop (+break condition)!');
end
    %{
    %% Break condition ("loop & a 1/2")
	number_of_lines_read_this_pass = numel(HITRAN_data{1});
    if(~number_of_lines_read_this_pass)
		break;   
    end
    %}
%% Remove unwanted molecules & isotopologues
display('Removing undesired lines...');
% Molecules
tmp = find(~ismember(HITRAN_data{1},molecule_index_to_keep),1);
if ~isempty(tmp)
    HITRAN_data{1}(tmp,:)=[];
    HITRAN_data{2}(tmp,:)=[];
    HITRAN_data{3}(tmp,:)=[];
    HITRAN_data{4}(tmp,:)=[];
end
% Isotopologues
tmp = find(~ismember(HITRAN_data{2},isotopologues_to_keep));    
if ~isempty(tmp)
    HITRAN_data{1}(tmp,:)=[];
    HITRAN_data{2}(tmp,:)=[];
    HITRAN_data{3}(tmp,:)=[];
    HITRAN_data{4}(tmp,:)=[];
end
% Wavenumbers
if wnFiltering
    tmp = [find(HITRAN_data{3} < wnMin,1,'last') find(HITRAN_data{3} > wnMax,1,'first')];
    if ~isempty(tmp)
        HITRAN_data{1}([1:tmp(1) tmp(2):end],:)=[];
        HITRAN_data{2}([1:tmp(1) tmp(2):end],:)=[];
        HITRAN_data{3}([1:tmp(1) tmp(2):end],:)=[];
        HITRAN_data{4}([1:tmp(1) tmp(2):end],:)=[];
    end    
end
clear tmp
%% Prepare to write back to file
% Format
HITRAN_data{1} = num2str(HITRAN_data{1},'%02u');
HITRAN_data{2} = num2str(HITRAN_data{2});
HITRAN_data{3} = num2str(HITRAN_data{3},'%012f');
% HITRAN_data{4} is already char so no further processing is needed
% Join lines
HITRAN_data = cellstr([HITRAN_data{:}]);
% Write to file    
display('Writing to file...');
fprintf(fid_w,'%s\n', HITRAN_data{:});
% end
display(['Done processing ''' FileName '''!']); [a,f]=audioread('success.wav'); sound(a,f);
fclose('all');
delete([PathName FileName]);