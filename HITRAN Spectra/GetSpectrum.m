%% Preparation
close all force; fclose('all'); clear variables; clc; 
dbstop if error;
addpath(fullfile(cd,'Faddeeva'));
% StartMultithread;
%{
DEBUG:

%}
%% Definitions
% Wavenumber-related:
wnMin = 2250; %cm^-1 , ~4400nm
wnMax = 2450; %cm^-1 , ~4150nm
wnRes = 5E-4; % about 1/10 of the WN difference between lines in the DB.
% "Experiment"-related:
OPL = 1; %Optical path length, [cm] %NOTE: in HITRAN-web it's in [m]!
pressure_Tot=1; %atm
pressure_CO2=1; %atm
tSimu=900; %K
HW_to_eval=6000; %TODO: Translation from grid "bins" to voigt HW
%% Constants
% numRelevantLines = 228027; % For 2-ISO files
numRelevantLines = 208597; % this is the amount of lines belonging to the prime
                           % isotopologue of CO2 in the range: 2250<WN<2400 cm^-1.
                           % Should be defined for DB files having data for
                           % WN outside the range of interest.
mol_wei_CO2=44; %g/mol
n_self_temp = 7E-5;% [cm^-1 * atm^-1 * K ^-1] {see info just below}
% ^ = self-induced pressure-shift coefficient temperature-dependence
% based on "Self-induced pressure shift and temperature dependence measurements 
% of CO2 at 2.05 um with a tunable diode laser spectrometer" by Li et al. (2012)
tRef = 296;

physConsts.c = 29979245800; % (cm/s) (exact)
physConsts.c_unc = 0; % (cm/s) (exact)
physConsts.e = 1.60217665e-19; % (C) 
physConsts.e_unc =  0.000000035e-19; % (C) 
physConsts.h = 6.62606957e-34; % (J.s)
physConsts.h_unc = 0.00000029e-34; % (J.s)
physConsts.k_B = 1.3806488e-23; % (J/K)
physConsts.k_B_unc = 0.0000013e-34; % (J.s)

c2 = physConsts.h * physConsts.c / physConsts.k_B;

% Translating pressure into different units:
  % pV = NkT -> p = nkT -> n = p/kT -> 
  % n (1/cm^3) = n (1/m^3) * 1e-6 (m/cm)^3 = p (Pa) / (1.38e-23 (J/K) * Tref (K)) * 1e-6 (m/cm)^3 
  % = p (atm) * (101325 Pa/atm) / (1.3806e-23 (J/K) * Tref (K)) * 1e-6 (m/cm)^3 
  % = p (atm) * 101325 * 1e-6 / (1.3806e-23 * 296) * (Pa * m^3 / J) * (1/(atm * cm^3))
  % = p (atm) * 2.48e19 * (1/(atm * cm^3))
atm_to_molecules_per_cm3 = 101325 * 1e-6 / (1.3806e-23 * tSimu);
%% Initialization
wnGrid = (wnMin:wnRes:wnMax)';
gridSize = numel(wnGrid);

intensityBins(gridSize,1)=0;
absorption_x_section_cm2_per_molecule(gridSize,1)=0;
absorption_coeff_per_cm(gridSize,1)=0;

load ('TIPS2008_Data.mat','C12','T','TIPS12');
Q_tot_ref = TIPS12(tRef); %This is only good for the PRIME ISOTOPOLOGUE!
Q_tot_new = TIPS12(tSimu);
clear C12 T TIPS12
%% Select file
[FileName,PathName] = uigetfile('*.parF','Please select a filtered HITRAN\HITEMP DB file','F:\Iliya-PhD\Spectral DB\HITEMP-CO2-1ISOT\02_2250-2500_HITEMP2010.parF');
% A "filtered DB file" contains only the isotopologues of interest.
% Possibly, also only the WN of interest.
%% Open files for reading & writing
fid_r = fopen([PathName FileName],'r');
%% Read formatted DB contents
feature('DefaultCharacterSet','UTF8');
HITRAN_data = textscan(fid_r, ...
        ['%2c' '%1f' '%12f' '%10f' '%10f' ...
        '%5f' '%5f' '%10f' '%4f' '%8f' ...
        '%15c' '%15c' '%15c' '%15c' '%6c' ...
        '%12c' '%1c' '%7f' '%7f'], ...
        numRelevantLines, 'delimiter', '', ...
        'whitespace', '');
HITRAN_data{1}=str2num(HITRAN_data{1}); %#ok<ST2NM>
numLinesRead = size(HITRAN_data{2},1); % this is for the case when numRelevantLines is unknown
%% Put cells into struct
db_Struct = struct(...
             'MoleculeIndexNum',num2cell(HITRAN_data{1})...
            ,'IsotopologueNumber',num2cell(HITRAN_data{2})...    
            ,'Wavenumber',num2cell(HITRAN_data{3})...               
            ,'LineStrength',num2cell(HITRAN_data{4})...
            ,'A_coeff',num2cell(HITRAN_data{5})...
            ,'BroadeningFromAir',num2cell(HITRAN_data{6})...
            ,'BroadeningFromSelf',num2cell(HITRAN_data{7})...
            ,'LowerEnergy',num2cell(HITRAN_data{8})...
            ,'TemperatureCoeffAir',num2cell(HITRAN_data{9})...
            ,'PressureShiftCoeff',num2cell(HITRAN_data{10})...
            ,'USGQ',cellstr(HITRAN_data{11})... 
            ,'LSGQ',cellstr(HITRAN_data{12})... 
            ,'USLQ',cellstr(HITRAN_data{13})... 
            ,'LSLQ',cellstr(HITRAN_data{14})... 
            ,'Unct',cellstr(HITRAN_data{15})... 
            ,'RefI',cellstr(HITRAN_data{16})... 
            ,'AvailFlag',cellstr(HITRAN_data{17})...
            ,'StatisticWeightU',num2cell(HITRAN_data{18})...
            ,'StatisticWeightL',num2cell(HITRAN_data{19})...
            );
clear HITRAN_data
peak_absorbance(numLinesRead,1)=0;
%% Recalculate some things
if tSimu~=tRef
% Intensity ("Q_tot method" - Simeckova, et al. [2006], eq. 9)
    db_Struct = putCellsInStructArrayField(db_Struct,'LineStrength',...
        num2cell([db_Struct.LineStrength] * (Q_tot_ref / Q_tot_new) .* ...
        exp(-c2 * [db_Struct.LowerEnergy] * (1/tSimu - 1/tRef)) .* ...
        (1 - exp(-c2*[db_Struct.Wavenumber]/tSimu)) ./ ...
        (1 - exp(-c2*[db_Struct.Wavenumber]/tRef )) ));    
end
% Temperature\Pressure WN shift    
    pres_shift = [db_Struct.PressureShiftCoeff] + n_self_temp*(tSimu-tRef);
    wn_shifted = [db_Struct.Wavenumber]+pres_shift*pressure_Tot;    
%{ 
  A note on pressure broadening, pressure-induced shift and temp. dependence:
    According to eq. 1-3 in the article "Air-and self-broadened half widths, 
    pressure-induced shifts, and line mixing in the ?2 band of 12CH4" by Smith 
    et al. (2014), there are several other coefficients that play a role in
    broadening:
     n2 - temperature dependence exponent of the *self*-broadened halfwidth.
     d' - temperature dependence of the pressure-induced shift coefficient.
  
%}
    wn_nearest = round(wn_shifted/wnRes)*wnRes;
    eval_grid = round([max((wn_nearest-wnMin)/wnRes-HW_to_eval,1);...
                 min((wn_nearest-wnMin)/wnRes+HW_to_eval,gridSize)])';
clear pres_shift wn_shifted
%% Graphical aids:
% Chart
hFig=figure('Name','Iliya''s DB Explorer Output','NumberTitle','off'); 
hold all; grid on; set(gca, 'GridLineStyle', '-'); grid(gca,'minor');
hPlot=plot(wnGrid,intensityBins); maximize(hFig);
set(gca,'XTick',wnMin:(wnMax-wnMin)/15:wnMax);
xlabel('Wavenumber [cm^{-1}]'); ylabel('Intensity [cm/mol]');
% Waitbar
hWB=waitbar(0,['0 of ' num2str(numRelevantLines) ' lines computed.']);
set(hWB,'Position',[1003,755,270,56]);
hw=findobj(hWB,'Type','Patch'); set(hw,'EdgeColor',[0 1 0],'FaceColor',[0 1 0]);
tID=tic; %DEBUG
for ind1=1:numLinesRead
% parfor ind1=1:3000
%% Skipping irrelevant WN
%{
% The following 3 lines code are only needed if numRelevantLines is unknown apriori, 
% or if relevant WN do not start at the beginning of the file.
    if db_Struct(ind1)<wnMin || db_Struct(ind1)>wnMax
        continue
    end  
%}
%% Evaluating line contributions to nearby "WN bins"
    intensityBins(eval_grid(ind1,1):eval_grid(ind1,2)) = ...
        intensityBins(eval_grid(ind1,1):eval_grid(ind1,2)) +...
        db_Struct(ind1).LineStrength * Voigt(pressure_Tot,pressure_CO2,...
        db_Struct(ind1).BroadeningFromAir,db_Struct(ind1).BroadeningFromSelf,...
        tSimu,db_Struct(ind1).TemperatureCoeffAir,wn_nearest(ind1),...
        mol_wei_CO2,wnGrid(eval_grid(ind1,1):eval_grid(ind1,2)));
%{ 
    voigt_tmp = Voigt(pressure_Tot,pressure_CO2,...
        db_Struct(ind1).BroadeningFromAir,db_Struct(ind1).BroadeningFromSelf,...
        tSimu,db_Struct(ind1).TemperatureCoeffAir,wn_nearest(ind1),...
        mol_wei_CO2,wnGrid(eval_grid(ind1,1):eval_grid(ind1,2)));
    intensityBins(eval_grid(ind1,1):eval_grid(ind1,2)) = ...
        intensityBins(eval_grid(ind1,1):eval_grid(ind1,2)) +...
        db_Struct(ind1).LineStrength * voigt_tmp.evals;
   
%% Absorption coefficient calculation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%% -----Ripoff from <Peter T. S. DeVore> starts here-----
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
absorption_x_section = ... at line center, [cm^2 / molecule]
         db_Struct(ind1).LineStrength ./ (2 * voigt_tmp.HW_V * ...
         (1.065 + (0.447 * voigt_tmp.x) + (0.058 * (voigt_tmp.x .^ 2)) ) );
absorption_x_section_cm2_per_molecule = ...
        absorption_x_section .* ( ((1 - voigt_tmp.x) .* exp(-0.693 .* (voigt_tmp.y .^ 2))) + ...
        voigt_tmp.x ./ (1 + (voigt_tmp.y .^ 2)) + (0.016 * (1 - voigt_tmp.x) .* voigt_tmp.x .* ...
        (exp( -0.0841 .* (voigt_tmp.y .^ 2.25)) - 1 ./ (1 + 0.021 .* (voigt_tmp.y .^ 2.25)))) );   
absorption_coeff_per_cm(eval_grid(ind1,1):eval_grid(ind1,2)) = ...
        absorption_coeff_per_cm(eval_grid(ind1,1):eval_grid(ind1,2)) + ...
        + atm_to_molecules_per_cm3 * pressure_CO2 ...
        .* absorption_x_section_cm2_per_molecule;    
peak_absorbance(ind1) = atm_to_molecules_per_cm3 * pressure_CO2 * ...
                        absorption_x_section * OPL;
%}
    %% Show Progress
    if ~mod(ind1,500) %update progress bar every so many lines
        waitbar(ind1/numRelevantLines,hWB,{[num2str(ind1) ' of '...
                num2str(numRelevantLines) ' lines computed. ('...
                num2str(ind1/numRelevantLines*100,3) '%)'],...
                ['Elapsed time: ' num2str(toc(tID)) 's.']});
        set(hPlot,'YData',intensityBins); 
%         drawnow; %DEBUG
%         disp(); %DEBUG
    end
end
% set(hPlot,'YData',intensityBins*(atm_to_molecules_per_cm3 * pressure_CO2));
waitbar(1,hWB,{'All done!',['Elapsed time: ' num2str(toc(tID)) 's.']});
% plot(wnGrid,intensityBins);
%% Postprocessing
% Prepare strings for averaging
if pressure_CO2/pressure_Tot==1
    gasComposition = 'Pure';
else
    gasComposition = ['Air with ' num2str(pressure_CO2/pressure_Tot*100) '%'];
end
gasComposition = [gasComposition ' CO_2'];
data_info = struct(...
     'spectrumType','absorption'...
    ,'GasComposition', gasComposition...
    ,'Temp',num2str(tSimu)...
    ,'Pres',num2str(pressure_Tot)...
    ,'OpticalPath',num2str(0)...
    );
% WL-based averaging
PiecewiseAvg(wnGrid,intensityBins,[4,6.5],data_info);

if 1 %More Plots:
%     k = intensityBins*(atm_to_molecules_per_cm3 * pressure_CO2);
    k = intensityBins*atm_to_molecules_per_cm3; %seems to be more compatible with H.o.t.W
    T_i_w = exp(-k*OPL);
    L_BB = 2*physConsts.h*physConsts.c^2*wnGrid.^3 ./ (...
         exp(physConsts.h*physConsts.c*wnGrid/(physConsts.k_B*tSimu))-1);

    figure(102); plot(wnGrid,k); grid minor;
    xlabel('Wavenumber [cm^{-1}]'); ylabel('Absorption Coefficient [cm^{-1}]'); 

    figure(103); plot(wnGrid,T_i_w); grid minor;
    xlabel('Wavenumber [cm^{-1}]'); 
    ylabel(['Transmittance Spectrum for OPL=' num2str(OPL) 'cm']);

    figure(104); plot(wnGrid,1-T_i_w); grid minor;
    xlabel('Wavenumber [cm^{-1}]'); 
    ylabel(['Absorption Spectrum for OPL=' num2str(OPL) 'cm']); 
    
    figure(105); plot(wnGrid,L_BB.*(1-T_i_w)); grid minor; 
    xlabel('Wavenumber [cm^{-1}]'); ylabel('Radiance Spectrum [W sr^{-1} cm^{-2} cm^{-1}]');    
end
%% Cleanup
% StopMultithread;