function StartMultithread
% StartMultithread checks whether a parallel-pool is already open, and if not,
% attempts to open a pool equal to the amount of available processors.
%% Get amount of available local cores
% N=eval(getenv('NUMBER_OF_PROCESSORS'));% << Returns # of logical cores 
N=feature('numCores');                   % << Returns # of physical cores,
                                         %    run w/o ";" to see more info.
%% Query the size of the current pool, if exists (updated for MATLAB 2014a)
poolobj = gcp('nocreate'); % If no pool, do not create new one.
if isempty(poolobj)
    poolsize = 0;
else
    poolsize = poolobj.NumWorkers;
end
%% Try to create a pool if doesn't exist
if N>1 && ~poolsize
    try
        %this will work if the 'local' (default) profile is configured to
        %have the maximun number of available processors
        parpool(N);
    catch ParExcep
        if strcmp(ParExcep.identifier,'parallel:cluster:MatlabpoolRunValidation')
            fprintf('Problem encountered with default matlabpool profile. Attempting to create a new profile...\n');
        end
        C = parcluster('local');
        C.NumWorkers=N;
        if strcmp(questdlg({['Would you like to update the default processor number to ' num2str(N) '?'],...
                'Note: there may not be a speedup for hyper-threaded logical cores (Intel).'},'Confirm Default matlabpool Profile Change'),'Yes')
            saveProfile(C);
        end
        parpool local
    end
end