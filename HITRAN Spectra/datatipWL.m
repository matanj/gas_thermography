function output_txt = datatipWL(obj,event_obj)
% Display the position of the data cursor
% obj          Currently not used (empty)
% event_obj    Handle to event object
% output_txt   Data cursor text string (string or cell array of strings).

pos = get(event_obj,'Position');
output_txt = {['\lambda: ',num2str(pos(1),4)],...
    ['I(\lambda): ',num2str(pos(2),4)]};

set(0,'ShowHiddenHandles','on');                       % Show hidden handles
hText = findobj('Type','text','Tag','DataTipMarker');  % Find the data tip text
set(0,'ShowHiddenHandles','off');                      % Hide handles again
set(hText,'Interpreter','tex');                        % Change the interpreter