%% Implementing: "Noise Estimation and Filtering Using Block-Based Singular Value Decomposition", Konstantinos Konstantinides,
%%  Balas NataTrajan, and Gregory S. Yovanof, 1997
%%%%%%%%% Main function %%%%%%%%%
function SVDDenoisingTest
clc; close all;
dbstop if error;
bSize = 2;
tmpFileName = 'temp.jpg';

path(path,'Z:\Matan\');
progressbar('th');

imOrig = imread('moon.tif');

if ndims(imOrig) == 3 %rgb
    imOrig = imOrig(:,:,1);
end

%chop image to match block size:
imSize = size(imOrig);
imOrig = imOrig(1:bSize*floor(imSize(1)/bSize), 1:bSize*floor(imSize(2)/bSize));
imOrig = double(imOrig);
sigma = 20;
im = imOrig + sigma*randn(size(imOrig));
im = mat2gray(im);
im = uint8(255*im);
im = double(im);


im = imread('Z:\Matan\Matan stuff\noisyMoon.tif');
imSize = size(im);
im = im(1:bSize*floor(imSize(1)/bSize), 1:bSize*floor(imSize(2)/bSize));
im = double(im);

upperBound = ceil(sqrt(numel(im))*sigma);

%devide to blocks
blocks = divideIm2Blocks(im, bSize);
%Calibrating threshold:
upperBound = 130;
r = zeros(upperBound, 1);
de = 1;
eStart = 80;

if 0
  for e = eStart:de:upperBound
    filtImage = SVDFiltering(blocks,e);
    filtImage = uint8(filtImage);
    imwrite(filtImage,tmpFileName,'BitDepth',8,'mode', 'lossless');
    info = dir(tmpFileName);
    r(e) = info.bytes;
    %DEBUG I2 = imread('temp.jpg');
    %DEBUG isequal(filtImage,I2)
    %DEBUG figure; imagesc(diff(filtImage-I2)); axis image; colorbar
    progressbar(e/upperBound);
  end
end

eStar = 98

%calc 2nd derivetive of ratio(e):
d2rde2old = diff(diff(r))/de^2;
d2rde2 = zeros(size(r));
for i = 1:numel(r)-2
  d2rde2(i) = (r(i+2)-2*r(i+1)+r(i))/de^2;
end
% for i = 2:numel(r)-1
%   d2rde2(i) = (r(i+1)-2*r(i)+r(i-1))/de^2;
% end


figure; plot(d2rde2,'*'); hold on; title('2nd derivative of compression ratio');
plot(d2rde2old,'r');
eStar = find(d2rde2 == max(d2rde2(:)), 1, 'first'); % optimal threshold
text(eStar, d2rde2(eStar), 'Max');
%If we chose 'upperBound' too small, comression ratio may be monotonically
%decreasing:
if eStar == 1
  eStar = upperBound;
end

%Filter with the optimal threashold:
imFiltSVD = SVDFiltering(blocks,eStart+eStar-1);
% imFiltSVD = SVDFiltering(blocks,150);

%% Plots and SNR
% Calc SNR w/o any filtering
SNROrig = snr(imOrig, im-imOrig);
figure; imshow(mat2gray(im)); colorbar;
title(['Original image with noise, SNR = ', num2str(SNROrig)]);

%Calc SNR with simple low pass:
lpfSize = 5;
imLP = conv2(double(im), ones(lpfSize)/lpfSize^2, 'same');
SNRFiltLP = snr(imLP, imLP - im);
noiseLP = im-imLP;
figure;imshowpair(mat2gray(imLP), mat2gray(abs(noiseLP)), 'method', 'montage'); colorbar;
title(['Denoising using simple LPF, SNR = ', num2str(SNRFiltLP)]);
%figure; histogram(noiseLP); title(['estimated noise from LPF, std = ',num2str(std(noiseLP(:)))]);
% Calc SNR with weiner adaptive filter
imWeiner = wiener2(im,[5 5]);
SNRFiltWiener = snr(imWeiner, imWeiner - im);
noiseWeiner = im-imWeiner;
figure;imshowpair(mat2gray(imWeiner), mat2gray(abs(noiseWeiner)), 'method', 'montage'); colorbar;
title(['Denoising using adaptive Weiner filter, SNR = ', num2str(SNRFiltWiener)]);
%figure; histogram(noiseWeiner); title(['estimated noise from Weiner filter, std = ',num2str(std(noiseWeiner(:)))]);
% Calc SNR with SVD filtering
SNRFiltSVD = snr(imFiltSVD, imFiltSVD - im);
noiseSVD = (im-imFiltSVD);
figure; imshowpair(mat2gray(imFiltSVD), mat2gray(abs(noiseSVD)), 'method', 'montage'); colorbar;
% figure; imshowpair(mat2gray(imFiltSVD), mat2gray(im), 'method', 'montage'); colorbar;
title(['Denoising using SVD with th = ',num2str(eStart+eStar-1),' SNR = ', num2str(SNRFiltSVD)]);
%figure; histogram(noiseSVD); title(['estimated noise from SVD filter, std = ',num2str(std(noiseSVD(:)))]);

% Calc SNR with NLM filtering
imFiltNLM = imread('Z:\Matan\Matan stuff\Moon_fullNLM_h12_tws7_sws15.tif');

imFiltNLMSize = size(imFiltNLM);
imFiltNLM = imFiltNLM(1:bSize*floor(imFiltNLMSize(1)/bSize), 1:bSize*floor(imFiltNLMSize(2)/bSize));
imFiltNLM = double(imFiltNLM);
SNRFiltNLM = snr(imFiltNLM, imFiltNLM - im);
noiseNLM = (im-imFiltNLM);
figure; imshowpair(mat2gray(imFiltNLM), mat2gray(abs(noiseNLM)), 'method', 'montage'); colorbar;
title(['Denoising using NLM , SNR = ', num2str(SNRFiltNLM)]);
%figure; histogram(noiseNLM); title(['estimated noise from NLM filter, std = ',num2str(std(noiseNLM(:)))]);

% delete garbage:
delete(tmpFileName);
end

%%%%%%%%% SVD filtering with threshold %%%%%%%%%
function filtImage = SVDFiltering(blocks,th)
  %For each block, replace that block with the filtered one: 
  truncSVDwithTH = @(bloxs) truncSVD(bloxs, th);
  filtBlocks = cellfun(truncSVDwithTH, blocks,'UniformOutput', false);
  %Build reconstructed image from the filtered blocks:
  filtImage = cell2mat(filtBlocks);
%   figure; imshow(mat2gray(cell2mat(blocks)));%DEBUG
%   figure; imshow(mat2gray(filtImage));%DEBUG
%   figure; imagesc(diff(filtImage-cell2mat(blocks))); axis image; colorbar%DEBUG
end

%%%%%%%%% Divide image into blocks %%%%%%%%%
function subBlocks = divideIm2Blocks(I, bSize)
subBlocks = {};
if nargin < 2 || bSize < 2
  error('invalid args');
end
s   = size(I);
nB = ceil(s/bSize);
rowsVec = repmat(bSize, [nB(1) 1]);
colsVec = repmat(bSize, [nB(2) 1]);
subBlocks = mat2cell(I, rowsVec, colsVec);
% cellfun(@(x) mean(x(:)),C,'UniformOutput',false);
end

%%%%%%%%% SVD lossy compression %%%%%%%%%
function imTrunc = truncSVD(im,th)
  if nargin < 2 || th < 0
    error('invalid arg');
  end
  [U,S,V] = svd(im);
  ss = diag(S);
%   ss(ss < th) = 0;
%   imTrunc = U*diag(ss)*V';
  ind = find(ss<th, 1, 'first') - 1;
  imTrunc = U(:,1:ind)*S(1:ind,1:ind)*V(:,1:ind)';
end