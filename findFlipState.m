function [correctFlip,flipState] = findFlipState(baseline,mask)
% FINDFLIPSTATE Find the correct image flip setting (none, lr, ud, lr+ud)

S(4) = sumN(abs(imgradientxy(baseline-rot90(mask,2))));
S(3) = sumN(abs(imgradientxy(baseline-flipud(mask))));
S(2) = sumN(abs(imgradientxy(baseline-fliplr(mask))));
S(1) = sumN(abs(imgradientxy(baseline-mask)));

% states = cat(3,baseline-mask, baseline-fliplr(mask), baseline-flipud(mask), baseline-rot90(mask,2));
[~,flipState] = min(S);

switch flipState
  case 1
    correctFlip = mask;
  case 2
    correctFlip = fliplr(mask);
  case 3 
    correctFlip = flipud(mask);
  case 4
    correctFlip = rot90(mask,2);
end

function out = sumN(A)
out = nansum(A(:));