clear all
% close all
clc

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Demo file for deconvtv
% Image 'salt and pepper' noise removal
% 
% Stanley Chan
% University of California, San Diego
% 20 Jan, 2011
%
% Copyright 2011
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
rng(666);
% Prepare images
f_orig  = im2double(imread([pwd,'\data\building.jpg']));
% f_orig = rgb2gray(f_orig);
[rows cols frames] = size(f_orig);
H       = fspecial('gaussian', [9 9], 2);
% H       = fspecial('gaussian', [3 3], 1);
g       = imfilter(f_orig, H, 'circular');
% g       = imnoise(g, 'salt & pepper', 0.05);
g       = imnoise(g, 'salt & pepper', 0.1);
% g       = imnoise(g, 'gaussian',0,0.001);
% Setup parameters (for example)
opts.rho_r   = 5;
opts.rho_o   = 100;
opts.beta    = [1 1 0];
opts.print   = true;
opts.alpha   = 0.7;
opts.method  = 'l1';

% Setup mu
mu           = 20;

% Main routine
tic
out = deconvtv(g, H, mu, opts);
toc

% out2 = medfilt2(out.f);
% figure(3); imshow(out2); title('after median filter');

% Display results
figure;
imshow(g);
title('input');

figure;
imshow(out.f);
title('output');