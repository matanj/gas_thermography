classdef GasHelper
  %GASHELPER static Utility class for working with camera's gas measurements

  properties (Constant)
    h = 6.62606957e-34; %Planck constant[J*s]
    c = 299792458; %speed of light[m/s]
    Kb = 1.3806488e-23; % Boltzmann constant[J/K]
    Na = 6.02e23; % Avogadro's number
    Rgas = 0.08206; % Gas constant, [L*atm*mol^-1*K^-1]
    C1 = 2*GasHelper.h*GasHelper.c^2; % 1st Planck constant
    C2 = GasHelper.h*GasHelper.c/GasHelper.Kb; %2nd Planck constant
    C2K_offset = 273.15; % Celsius to Kelvin offset
  end

  methods (Static = true)

    function T_K = C2K(T_C)
       % Celsius to Kelvin
      T_K = T_C + GasHelper.C2K_offset;
    end
    
    function T_C = K2C(T_K)
      % Kelvin to Celsius
      T_C = T_K - GasHelper.C2K_offset;
    end
    
    function sig = HWHM2sigma(HWHM)
      % returns 'sigma' for a gaussian with a given HWHM
      sig = HWHM/sqrt(2*log(2));
    end
    
    function HWHM = sigma2HWHM(sig)
      % returns 'HWHM' for a gaussian with a given sigma 'sig'
      HWHM = sqrt(2*log(2))*sig;
    end
    
    function y = gaussianFrom3Params(x, params)
      % returns the evalued Gaussian of the form a*exp(-(x-mu)^2/(2*sig^2))
      if nargin<2 || (numel(params)~=3)
        error('invalid args!');
      end
      [a,mu,sig] = deal(params(1),params(2),params(3));
      y = a*exp(-((x-mu).^2)./(2*sig.^2));
    end
    
    function wn = wl2wn(wl_um)
      % wavelenth [um] to wavenumber [cm^-1]
      wn = 1e4./wl_um;
    end
    
    function wl_um = wn2wl(wn_cm_1)
      % wavenumber [cm^-1] to wavelength [um]
      wl_um = 1e4./wn_cm_1;
    end
    
    function [wn, f_wn] = spectralFunc_wl2wn(wl_um, f_wl, isRatioFunc)
      % Using the Jacobian transformation, since f_wn*d_wn = f_wl*d_wl .
      % For transmission/absorption/reflection spectra, or any ratio-based
      % spectral function, no need to apply Jacobian, as it cancel out.
      if nargin < 3
        isRatioFunc = false;
      end
      wl_um = wl_um(:); f_wl = f_wl(:); % force column vectors
      wn = flipud(GasHelper.wl2wn(wl_um));
      if isRatioFunc
        f_wn = flipud(f_wl);
      else
        f_wn = flipud(f_wl)*1e4./(wn.^2);
      end
    end
    
    function [wl_um, f_wl] = spectralFunc_wn2wl(wn_cm_1, f_wn, isRatioFunc)
      % using the Jacobian transformation, since f_wn*d_wn = f_wl*d_wl .
      % For transmission/absorption/reflection spectra, or any ratio-based
      % spectral function, no need to apply Jacobian, as it cancel out.
      if nargin < 3
        isRatioFunc = false;
      end
      wn_cm_1 = wn_cm_1(:); f_wn = f_wn(:); % force column vectors
      wl_um = flipud(GasHelper.wn2wl(wn_cm_1));
      if isRatioFunc
        f_wl = flipud(f_wn);
      else
        f_wl = flipud(f_wn)*1e4./(wl_um.^2);
      end
    end
    
    function y = getDetectorSCD_TF_WL(wl_range, cfit_obj_path)
      % Returns the transfer function of the Camera's detector for a given
      % wl_range in [um]. Detector's info assumed stored in a 'cfit' object.
      if nargin < 2
        cfit_obj_path = 'Z:\Matan\Camera Transmittance Fits\TransmittanceFitsNew.mat';
      end
      if nargin < 1
        wl_range = (1.5:0.01:6)';
      end
      detector_cfit_obj = load(cfit_obj_path, 'Detector_SCD');
      detector_cfit_obj = detector_cfit_obj.Detector_SCD;
      y = detector_cfit_obj(wl_range);
    end
    
    
    function [T_rec_C] = R2T_rec(R_est, calib_T, e_eff, trans_eff, Kw, a0, a1)
      % gets R_est, calib_T[K], effective emissivity, effected transmisivity, and
      % calibraion params (either 2 or 3) and returns reconstructed temperatrure[C].
      if nargin < 6
        error('invalid args!');
      end
      tol = 1e-10;
      if nargin == 7 % 3 parameters calibration:
        % solves the quadratic equation Y = p3+ln(e_eff)+ln(trans_eff) + p2*X + p1*X^2, for X = 1/T & Y = ln(R_est)
        discriminent = a0.^2 - 4*a1.*( Kw+log(e_eff)+log(trans_eff)-log(R_est) );
        x1 = (-a0 + sqrt(discriminent) )./2./a1;
        x2 = (-a0 - sqrt(discriminent) )./2./a1;
        assert(all(x1(~isnan(x1)))>tol && all(x2(~isnan(x2)))>tol && isreal(x1) && isreal(x2));
        T_rec = nan(size(R_est));
        % Get temperature map: take only T>0:
        T_rec(x1>0) = 1./x1(x1>0);
        T_rec(x1<0) = 1./x2(x1<0);
      else % 2 parameters calibration:
        % solves the linear equation Y = p3+ln(e_eff)+ln(trans_eff)+p2*X, for X = 1/T & Y = ln(R_est)
        x = (log(R_est)-log(e_eff)-log(trans_eff)-Kw ) ./ a0;
        assert(all(x(~isnan(x)))>tol && isreal(x));
        T_rec = 1./x;
      end
      T_rec = T_rec * max(calib_T); % since we have normalized by max(T) to improve the fit process
      T_rec_C = GasHelper.K2C(T_rec);
    end
    
    function [R] = T2R(T, calib_T, e_eff, trans_eff, Kw, a0, a1)
      % gets T[K], calib_T[K], effective emissivity, effective transmisivity of the atmosphere, and
      % calibraion params (either 2 or 3) and returns R in camera units.
      if nargin < 6
        error('invalid args!');
      end
      X = max(calib_T(:))./T;% since we have normalized by max(calib_T) to improve the fit process
      Kw = exp(Kw);  % NOTE: since here Kw is already after log! so need to invert it..
      if nargin == 7 % 3 parameters calibration:
        R = e_eff.*trans_eff.*Kw.*exp(a0.*X+a1.*X.^2);
      else % 2 parameters calibration:
        R = e_eff.*trans_eff.*Kw.*exp(a0.*X);
      end
      assert( all(R(~isnan(R))>0) && isreal(R) );
    end
    
    function [trans_eff] = getEffectiveTransmittance(filterParams, atmParams, weightType)
      % Get a HITRAN-generated transmission spectra (file with
      % wavenumber-transmission arrays) and calculates a representative
      % transmissivity, based on averaging in the meaningful spectral range
      % and using Planck as weighting function.
      % 'atmParams' - struct with fields: 
      %             'T' - temperature in [C].
      %             'atmTransmittanceFileName' - path of the HITRAN file.
      % 'weightType' - '0' for Planck (default), '1' for Planck & detector's tf
      if nargin < 2
        error('Invalid args!');
      end
      if nargin < 3
        weightType = 0;
      end
      tempCell = struct2cell(filterParams);
      [~,~,~,wl_cut_on,wl_cut_off,~] = deal(tempCell{:});
      wn_cut_on = GasHelper.wl2wn(wl_cut_off);
      wn_cut_off = GasHelper.wl2wn(wl_cut_on);
      
      % get an "effective" transmittance in the atmosphere:
      load(atmParams.atmTransmittanceFileName);
      % assure loaded data is in column vectors:
      wn = wn(:); 
      transmittance = transmittance(:);
%            
%       wn_l_filter = 2278; % optical filter cut-on
%       wn_h_filter = 2301; % optical filter cut-off
%       
      wn_ROI = wn(wn>=wn_cut_on & wn<=wn_cut_off );
      wn_ROI_ind = wn >= wn_ROI(1) & wn <= wn_ROI(end);
     
      % Calc the relevant Planck:
      [Lv] = GasHelper.PlanckWN(wn_ROI, GasHelper.C2K(atmParams.T));
      
      if weightType > 0 
        % weight by Planck & detector's tf (rational - the only functions in
        % the chain that directly relate photon flux to radiation):
        
        % Get transfer function of the Camera's detector tf:
        wl_ROI = GasHelper.wn2wl(wn_ROI);
        detector_tf = GasHelper.getDetectorSCD_TF_WL(wl_ROI); % get transfer funcrtion in wl
        [~, detector_tf] = GasHelper.spectralFunc_wl2wn(wl_ROI, detector_tf, true); % transform the function to wn
        weight_fnc = Lv.*detector_tf;
      else
        % Weighting by Planck:
        weight_fnc = Lv;
      end
      trans_eff = trapz(wn_ROI,transmittance(wn_ROI_ind).*weight_fnc) / trapz(wn_ROI,weight_fnc);     
    end
    
    function [trans_eff] = getEffectiveTransmittanceWeightByFilter(filterParams, atmTransmittanceFileName)
      % get an "effective" transmittance in the atmosphere:
      load(atmTransmittanceFileName);
      tempCell = struct2cell(filterParams); 
      [filter_CWL,filter_HWHM,th_h,wl_cut_on,wl_cut_off,shape] = deal(tempCell{:});
      %{
      numHWHM = 3;
      filter_CWN = 1e4/filter_CWL; %to [cm^-1]
      filter_wn_l = 1e4/(filter_CWL + numHWHM*filter_HWHM);
      filter_wn_h = 1e4/(filter_CWL - numHWHM*filter_HWHM);
      wn_ROI_ind = wn >= filter_wn_l & wn <= filter_wn_h;
      wn_ROI = wn(wn_ROI_ind);
      %}
      wl = flipud(GasHelper.wn2wl(wn(:))); % cm2um
      wl_ROI = wl((wl >= wl_cut_on) & (wl <= wl_cut_off));
      
      switch shape
        case 'triangle'% Crude approximation for the filter's response as a triange.
%           fr = [linspace(th_l,th_h,ceil(numel(wn_ROI)/2)), linspace(th_h,th_l,ceil(numel(wn_ROI)/2))];
          fr_l = trimf(wn_ROI, [wn_ROI(1), wn_ROI(floor(numel(wn_ROI)/2)+1), wn_ROI(end)]); % normalized triangle
          fr_l = fr_l*(th_h-cut_on) + cut_on; % scale to required shape
        otherwise % Default is 'Gaussian':
          sig = GasHelper.HWHM2sigma(filter_HWHM); % Since: HWHM = sqrt(2*log2)*sigma
          fr_l = GasHelper.gaussianFrom3Params(wl_ROI, [th_h, filter_CWL, sig]);
      end
%       figure; plot(wl_ROI, fr_l);
      [wn_ROI, fr] = GasHelper.spectralFunc_wl2wn(wl_ROI, fr_l, true); % convert to wn 
      wn_ROI_ind = wn >= wn_ROI(1) & wn <= wn_ROI(end);
      filter_response = double(wn_ROI_ind);
      filter_response(filter_response>0) = fr;
%       trans_eff = mean(transmittance(wn_ROI_ind).*filter_response(wn_ROI_ind));
%       % TODO: weighting by the filter. check if by Planck is better.
      trans_eff = trapz(wn_ROI, transmittance(wn_ROI_ind).*filter_response(wn_ROI_ind)) / ...
                   trapz(wn_ROI,filter_response(wn_ROI_ind));
    end
    
    function [trans_eff] = getWeightedTransmittance(hitranParams, optics_tf_wn, path_params)
      % Get a HITRAN-generated transmission spectra (file with
      % wavenumber-transmission arrays) and calculates a representative
      % transmissivity, based on averaging in the meaningful spectral range
      % and using Planck & optics as weighting function.
      % 'hitranParams' - struct with fields: 
      %             'min_wn', 'max_wn', 'wn_step' [cm^-1]
      %             'HW_to_eval' - number of HWHM for Voigt evaluation
      %             'hitranFileFullPath' - path of the HITRAN file.
      % 'optics_tf_wn' - Transfer function of optics (filter, detector,
      %                  Lens) in wavenumber
      % 'path_params' - struct with fields:
      %             'OPL' - length [cm], 'T' [K]
      %             'pressure_tot' - total pressure [atm]
      %             'co2_concentration' []
      if nargin < 3
        error('Invalid args!');
      end
      % get an transmittance in the path:
      [wn_vect, ~, tau_spectra_wn] = MyGetSpectrumGPU(true, 'tau', hitranParams, path_params);
      % assure input data is in column vectors:
      wn_vect = wn_vect(:); tau_wn = tau_spectra_wn(:);
      % Calc the relevant Planck:
      [Lv] = GasHelper.PlanckWN(wn_vect, path_params.T);
      % weight by Planck & detector's tf
      weight_fnc = Lv.*optics_tf_wn;
      % Average the spectral transmittance:
      trans_eff = trapz(wn_vect, tau_wn.*weight_fnc) / trapz(wn_vect, weight_fnc);     
    end
    
    
    function [y] = filterPrctile(x, p, val)
      % Assigns 'val' to places where x is not in the percentile range 'p'. if
      % 'val' not stated, 'NaN' is assigned by default.
      if nargin <2 || p<0 || p > 100
        error('invalid args!')
      end
      if nargin < 3
        val = nan;
      end
      x( x < prctile(x(:), p) | x > prctile(x(:), 100-p) ) = val;
      y = x;
    end
    
    function [y] = filterStds(x, nStds, val)
      % Assigns 'val' to places where x is not in the range of nStds from its mean. if
      % 'val' not stated, 'NaN' is assigned by default.
      if nargin <1
        error('invalid args!')
      end
      if nargin < 2
        nStds = 2;
        val = nan;
      end
      if nargin < 3
        val = nan;
      end
      x( abs(x-nanmean(x(:))) > nStds*nanstd(x(:)) ) = val;
      y = x;
    end
    
    function [subBlocks] = divideIm2Blocks(I, bSize)
      % divide image to bSize*bSize blocks
      if nargin < 2
        error('invalid args');
      end
      s = size(I);
      nB = ceil(s/bSize);
      rowsVec = repmat(bSize, [nB(1) 1]);
      colsVec = repmat(bSize, [nB(2) 1]);
      subBlocks = mat2cell(I, rowsVec, colsVec);
    end
    
    function [im_rec] = imFromFilteredBlocks(im, bSize)
      % Gets image, divide it to blocks of size 'bSize', applies operation
      % on each, and outputs the reconstructed image.
      imSize = size(im);
      %chop image to match block size:
      im = im(1:bSize*floor(imSize(1)/bSize), 1:bSize*floor(imSize(2)/bSize));
      %devide to blocks
      im_blocks = GasHelper.divideIm2Blocks(im, bSize);
      % set in each block the mean value of it:
      setMeanVal = @(block) nanmean(block(:))*ones(size(block));
      im_rec = cellfun(setMeanVal, im_blocks,'UniformOutput', false);
      %Build reconstructed image from the filtered blocks:
      im_rec = cell2mat(im_rec);
    end
    
    
    function [meanRadiance] = getRadianceFromHITRAN_HAPI(T, C, l, script_path)
      % Get mean (spectral) radiance HITRAN, by running a python script
      % based on HAPI
      if nargin < 3
        error('invalid args!');
      end
      if nargin<4
        scriptPath = 'C:\Users\matanj\Documents\PythonScripts\getEmissionConstOPL.py';
      else
        scriptPath = script_path;
      end
      %build command string:
      argString = [num2str(T),' ',num2str(C),' ',num2str(l)];
      commandStr = ['C:\Users\matanj\AppData\Local\Continuum\Anaconda3\python.exe ',...
        scriptPath, ' ', argString];
      [status, ~] = system(commandStr);
      if status == 0 % if succeeded:
        [script_folder, ~, ~] = fileparts(script_path);
        meanEmissionMatrixFileName = ['meanEmissionMatrix_',num2str(T),'C_','C',num2str(C),'_',num2str(l),'cm.mat'];
        load(fullfile(script_folder,meanEmissionMatrixFileName));
        meanRadiance = meanEmission;
      else
        error('failed!');
      end
    end
    
    function [nu, transmission] = getTransmissionFromHITRAN_HAPI(T, C, l, RH, script_path, delete_generated_file)
      % Get mean (spectral) transmittance from HITRAN, by running a python script
      % based on HAPI.
      % INPUTS: T[C], C[ppm], l[cm], RH[%], [script_path], [delete_generated_file]
      if nargin < 4
        error('invalid args!');
      end
      if nargin < 5 || isempty(script_path)
        scriptPath = 'C:\Users\matanj\Documents\PythonScripts\getTransmissivity.py';
      else
        scriptPath = script_path;
      end
      if nargin < 6
        delete_generated_file = true; % by default, the generated file is deleted.
      end
      [script_folder, ~, ~] = fileparts(scriptPath);
      original_dir = pwd; % backup
      cd(script_folder); % run script from its directory
      %build command string:
      argString = [num2str(T),' ',num2str(C),' ',num2str(l),' ',num2str(RH)];
      commandStr = ['C:\Users\matanj\AppData\Local\Continuum\Anaconda3\python.exe ',...
                    scriptPath, ' ', argString];
      %Expected generated file from the script:
      TransmissionMatrixFileName = ['Atmospheric_Transmittance_T',num2str(T),'C_','C',num2str(C),'ppm_L',num2str(l),'cm_RH',num2str(RH),'.mat'];
      generated_file = fullfile(script_folder,TransmissionMatrixFileName);
      [status, ~] = system(commandStr,'-echo'); % Executing
      cd(original_dir); % return to the calling directory
      if status == 0 % if succeeded:
        load(generated_file);
        nu = wn;
        transmission = transmittance;
        if delete_generated_file
          delete(generated_file);
        end
      else
        delete(generated_file); % Will throw warning if not exist 
        error('failed!');
      end
    end
    
    function [stats, object] = processImGetStats(im, hAx, calcStats)
      %Function to process an image and extract the object and optionally its stats
      %'im' - image
      %'hAx' - handle to an axis to modify.
      %'calcStats' - whether to output statistics in 'stats' or not.
      stats = [];
      lvl = graythresh(mat2gray(im)); binaryImage = im2bw(mat2gray(im), lvl); % alternative
      binaryImage = imfill(binaryImage, 'holes');
      labeledImage = bwlabel(binaryImage, 8);
      
      blobMeasurements = regionprops(logical(labeledImage), 'all'); % Get all the blob properties
      numberOfBlobs = size(blobMeasurements, 1);
      assert(numberOfBlobs == 1); % allow one blob
      
      object = im .* binaryImage;
      object(~object) = NaN;
      
      if nargin > 1 %need to calc stats and change given axis plot
        DLInterval = 10; % for histogram bin size
        axes(hAx); hold on;
        boundaries = bwboundaries(binaryImage);
        for k = 1 : numberOfBlobs % preferably just one...
          thisBoundary = boundaries{k};
          plot(thisBoundary(:,2), thisBoundary(:,1), 'r', 'LineWidth', 1.5);
          blobCentroid = blobMeasurements(k).Centroid;	% Get centroid
          plot(blobCentroid(1),blobCentroid(2),'r+', 'MarkerSize', 20);
        end
        hold off;
        % Calc Stats:
        if calcStats
          maxDL = nanmax(object(:));
          minDL = nanmin(object(:));
          nBins =  int16((maxDL-minDL)/DLInterval);
          [hist,edges] = histcounts(object,nBins);
          [~,I] = nanmax(hist(:));
          stats.Mean = nanmean(object(:));
          stats.MCDL = edges(I+1);
          stats.Max = maxDL;
        end
      end
    end
    
    function [caseList, I, hPropsListStructFiltered] = getMatchingCase(hTotFileList, hPropsListStruct, caseParamsNames, caseParamVals)
      % Query list of files according to properties.
      % Allows reqursive queries by this scheme:
      % [caseList1, I1, hPropsListStructFiltered] = getMatchingCase(hTotFileList, hPropsListStruct, caseParamsNames, caseParamVals)
      % [caseList2, I2, ~] = getMatchingCase(caseList1, hPropsListStructFiltered, caseParamsNames, caseParamVals)
      
      %INPUT: 'hTotFileList' - Total list of files of the experiment. cell array.
      %       'hPropsListStruct' - struct array holding experiment props per
      %           file. may be > than actual number mentioned in the filenames' format.
      %       'caseParamsNames' - parameters names to query. cell array.
      %       'caseParamVals' - associated values. cell array.
      %OUTPUT: 'caseList' - String cell array of filenames that match the input query.
      %        'I' - indices array of corresponding files inside input list
      %              'hTotFileList'
      %        'hPropsListStructFiltered' - to allow recuring
      
      caseList = {}; I = [];
      if nargin < 4
        return;
      end
      I = 1:numel(hTotFileList);
      for indCaseParams = 1:numel(caseParamsNames)
        %The following can handle cell array as well as numeric values:
        subI = find(ismember(hPropsListStruct.(caseParamsNames{indCaseParams}), caseParamVals{indCaseParams} ));
        I = intersect(subI, I);
      end
      caseList = {hTotFileList(I)};
      caseList = caseList{1};
      hPropsListStructFiltered = structfun(@(x) x(I), hPropsListStruct, 'UniformOutput', false);
    end
    
    function [bgName] = getBGfileFromIMfile(imName, baseDir)
      [propsIm.G,propsIm.T,propsIm.CWL,propsIm.IT,propsIm.isBG,~] = parseFilename(imName);
      totalFilesList = dir([baseDir,'/*.ptw']);
      if isempty(totalFilesList)
        error('invalid folder path, or no .ptw files there...');
      end
      totalFilesList = {totalFilesList(:).name}';
      [props.G,props.T,props.CWL,props.IT,props.isBG,props.name] = parseFilename(totalFilesList); %divide list to params
      [bgName,~,~] = GasHelper.getMatchingCase(totalFilesList, props,...
        {'T','CWL','IT','isBG'}, {propsIm.T,propsIm.CWL,propsIm.IT,true});
    end
    
    function maximizeFigure(hFig)
      % maximize current figure
      if nargin > 0
        set(hFig,'units','normalized','outerposition',[0 0 1 1]);
      else
        set(gcf,'units','normalized','outerposition',[0 0 1 1]);
      end
    end
    
    function [photons_flux] = HITRAN_radiance2Photon_Radiance(R_hitran, wl_nm)
      %% Scale the HITRAN radiance to photon flux (the camera counts photons):
      %{
      %Definitions: number density = nx, mixing ratio = Cx.
      % nx = Cx * na  (here na is the number density of air)
      % na = Na*N/V   (Na - Avogadro's number, N - number of moles of air)
      % For atmoshepric air: P*V = N*R*T  (R = 8.31 [J*mol^-1*K^-1])
      % ===> nx = Na*P*Cx/(R*T)
      % (Px = P*Cx is the partial pressure)

      N_CO2_molecules = Na * pressure * (OPL_step*1e-2)^3 / Rgas ./ T_model; % n = Na * PV/RT % TODO: incorrect!!
      N_CO2_molecules = N_CO2_molecules(rows,cols);
      N_CO2_molecules =  N_CO2_molecules(:, round(end/2)+1:end);
      %}

      %{
      %% THE following is in [MKS]:
      %% h = 6.626e-34 [J*s],     c = 2.9979245e8 [m/s],      k = 1.380658e-23 [J/K]
      %% Plank's law in wavenumber units: Lv = 2e8hc^2v^3*1/(exp(100h*c*v/(k*T))-1) [W/m^2/sr/cm^-1]. (Spectral radiance)
      %% Convert Plack's law from W/area/sr/cm^-1 to #photons/s/area/sr/cm^-1 (Spectral photon radiance):
      %%    ==> Lp = Lv/photon_energy = Lv/100hcv = 2e6*c*v^2/(exp(100hcv/kT)-1) [photon/s/m^2/sr/cm^-1]
      %% NOTE: the Stefan Boltzmann law for photon radiant emittance dictates T^3 relation rather than the well-known T^4!!!

      %% HITRAN gives spectral radiance in [W/sr/cm^2/cm^-1], so need to
      %% divide by photon's energy. 
      %% Two options: 1) Incorporate this during spectral radiance
                          calculation (currently I integrate in the python script)
                      2) divide by the average wavenumber, assuming the band
                          is narrow. 
      %}
      photons_flux = R_hitran ./ GasHelper.getPhotonEnergyFromWN(1e7./wl_nm); % converting filter_CWL in [nm] to [cm^-1] and apply above
    end
    
    function [R_camera] = photons_flux2R_camera(photons_flux, solid_angle, detector_area_cm2)
      R_camera = photons_flux*solid_angle*detector_area_cm2; %  TODO: verify if indeed no need to multiply by the solid angle according to assumption (6)?
    end
    
    function [new_im, RB] = fixJetAngle(im, Rim, in_theta_deg)
      %% Apply 2D affine rotating transformation to fix for the angle of the Jet.
      % If angle is not specified, Radon transformation is used to estimate angle.
      %% NOTE: should apply same fix to the calibration maps, or rotate back before using the calibration
      % NOTE: I trim the new image to the original size. 'theta' is assumed
      % small enough so that the skew is negligable.
      
      % TODO: Check also: https://www.mathworks.com/help/images/example-performing-image-registration.html
      
      S = 1; % scale factor
      FILL_VAL = NaN; % fill value for the by-product of the rotation transform.'NaN' reduces effects of interpolation.
      theta_deg =  90 - DetectAngle(im); % '90' as desired jet is vertical
      if nargin > 2
        theta_deg = -in_theta_deg;
      end
      % Pure Rotation transformation matrix:
      tform = affine2d([S.*cosd(theta_deg) -S.*sind(theta_deg) 0; S.*sind(theta_deg) S.*cosd(theta_deg) 0; 0 0 1]);
%       % Use landmarks to create the transformation matrix via fit:
%       % tform = fitgeotrans(movingPoints, fixedPoints, 'projective');

%       [new_im, RB]= imwarp(im,tform,'cubic','fillvalues',FILL_VAL);
      
      if nargin < 2 || (nargin >=2 && isempty(Rim) )
        Rim = imref2d(size(im)); % Consider the input image's coordinate frame as the World coordinate frame.
      end
      [new_im, RB]= imwarp(im,tform,'cubic','fillvalues',FILL_VAL, 'OutputView', Rim);
      
      % DEBUG: check that x' = A^-1(A(x)) = x
%       new_im(new_im==FILL_VAL) = nan;% mask the rotation transformation by-product
%       mask = ~isnan(new_im);
      %{
      % get the inner image w/o transformation's by-products:
      % need to distinguish between angles signs since the inner rectangle
      % usually intersects each of the outer one's limits by more that one
      % pixel.
      if theta_deg < 0 % rotate CW
        idx_row1 = find(mask(:,1),1,'last');
        idx_row2 = find(mask(:,end),1,'first');
        idx_col1 = find(mask(1,:),1,'first');
        idx_col2 = find(mask(end,:),1,'last');
      else % rotated CCW
        idx_row1 = find(mask(:,1),1,'first');
        idx_row2 = find(mask(:,end),1,'last');
        idx_col1 = find(mask(1,:),1,'last');
        idx_col2 = find(mask(end,:),1,'first');
      end
      min_row = min(idx_row1, idx_row2);
      max_row = max(idx_row1, idx_row2);
      min_col = min(idx_col1, idx_col2);
      max_col = max(idx_col1, idx_col2);
      new_im = new_im(min_row:max_row, min_col:max_col);
      %}

%       [new_im, mask] = GasHelper.get_inner_image_from_rotated_jet(new_im);
    end
    
    function [inner_im, mask] = get_inner_image_from_rotated_jet(rot_im)
      % get rotated image and a 'NaN' filler value of the rotation by-product.
      % Output: inner image where rows/cols not include any fill vvalue of NaN.
      [M,N] = size(rot_im);
      % Work on a BW image to simplify:
      % close inner holes (inner sections of NaNs, not by-product of rotation):
      inner_nans_area_th = 10; % threshold of the area of inner NaNs
      val_mask = ~bwareaopen(isnan(rot_im), inner_nans_area_th);
      % Get the number of pixels between NaN in each rows:
      numNanInRow = zeros(M,1);
      for indRow = 1 : M
        lastInd = find(val_mask(indRow,:), 1,'last');
        if isempty(lastInd)
          numNanInRow(indRow) = N;
        else
          numNanInRow(indRow) = lastInd - find(val_mask(indRow,:), 1,'first');
        end
      end
      [NN, edges] = histcounts(numNanInRow, N);
      [~,indMax] = max(NN);
      properRows = find(abs(numNanInRow-edges(indMax)) < 2 );
      inner_im = rot_im(properRows,:);
      val_mask = ~isnan(inner_im);
      % Do now for the Cols as well:
      numNanInCols = zeros(N,1);
      for indCol = 1 : N
        lastInd = find(val_mask(:,indCol), 1,'last');
        if isempty(lastInd)
          numNanInCols(indCol) = M;
        else
          numNanInCols(indCol) = lastInd - find(val_mask(:,indCol), 1,'first');
        end
      end
      [NN, edges] = histcounts(numNanInCols, M);
      [~,indMax] = max(NN);
      properCols = find(abs(numNanInCols-edges(indMax)) < 2 );
      inner_im = inner_im(:,properCols);
      mask = false(M,N);
      mask(properRows,properCols) = true;
    end
    
    function [new_im, D_nozzle_pixels, m2pixel, pos_nozzle, theta, D_nozzle] = trimIm2Nozzle(im, D_nozzle, GET_D_EFF, CROP_IM)
      % Gets image of jet WITH nozzle and the nozzle diameter[m].
      % Returns the cropped jet, diameter in pixels, the scale factor,
      % position of the nozzle and theta - horizontal skew of the jet.
      % If 'GET_D_EFF' == true, two scans are required from the user; 1st
      % of the nozzle diameter to get the m2pixel factor, and 2nd of the
      % effective diameter. 'D_nozzle' is then modified to the effective
      % diameter.
      % NOTE: x & y axes are inveresly used here because of the 'imagesc'
      % usage
      if nargin < 3
        GET_D_EFF = false;
        CROP_IM = true;
      elseif nargin < 4
        CROP_IM = true;
      end
      h_fig = GasHelper.plotIm(im,'Select nozzle''s orifice & press a key to continue');
      GasHelper.maximizeFigure(h_fig);% maximize figure for convienience
      figure(h_fig); % bring it to top!
      %%Estimate the nozzle position:
      % We need the two direction, not only horizontal direvative, since we
      % get sometimes peaks in the jet itself rather than the pipe alone.
      gradX = abs(diff(im,1,2));
      gradY = abs(diff(im,1,1));
      gradXBW = gradX > max(gradX(:))/1.5;
      gradYBW = gradY > max(gradY(:))/1.5; 
      % Remove unconneced pixels:
      gradXBW2 = bwareaopen(gradXBW,3);% This mask contains the vertical nozzle position as well as jet artifacts.
      gradYBW2 = bwareaopen(gradYBW,3);% This mask contains the horizontal nozzle position.
      % Use the horizontal line to restrict the other mask. For this to
      % work, 'gradYBW' assumed to have maximum of several line associated
      % with horizontal nozzle.
      horizLine = find(gradYBW2, 1, 'first'); 
      %%% TODO : use better estimate, e.g. using OTSU
      %{
      nLevels = 3;
      O = otsu(im,nLevels);
      figure; histogram(im(:)); hold on; histogram(im(O==nLevels));
      GasHelper.plotIm(im.*(O==nLevels));
      %}
      % get the first upper-left point:
      [upperLeft_x, upperLeft_y] = ind2sub(size(gradXBW2), find(gradXBW2, 1, 'first'));%This might colossiasly fail
      upperRight_y = find(gradXBW2(upperLeft_x,:),1,'last');
      if isempty(upperLeft_x) || isempty(upperLeft_y) || isempty(upperRight_y)
        h_nozzle = imdistline(gca);
      else
        if abs(upperRight_y-upperLeft_y) < 2 % arbitrary addition to prevent 0 width drag bar
          upperRight_y = upperRight_y + 10;
        end
        % Magical alignment: some is because of the diff operators
        upperLeft_x = upperLeft_x - floor(6*abs(upperRight_y-upperLeft_y)/82); % considering also scale. TODO: magic
        upperRight_x = upperLeft_x;
        upperRight_y = upperRight_y + 1;
        upperLeft_y = upperLeft_y + 1;
        %Set the drag bar in position:
        h_nozzle = imdistline(gca, [upperLeft_y upperRight_y],[upperLeft_x upperRight_x]);
      end
      apiObj = iptgetapi(h_nozzle);
      % constrain choosing only within the image:
      fcn = makeConstrainToRectFcn('imline', get(gca,'XLim'),get(gca,'YLim'));
      apiObj.setDragConstraintFcn(fcn);
      input('\nAssure the drag bar is slightly above nozzle and matches the declared D & Press any key to continue.\n','s');
      theta = apiObj.getAngleFromHorizontal(); %[deg]
      %Get the nozzle diameter in pixels and the m2pixel scale factor
      D_nozzle_pixels = ceil(apiObj.getDistance());%[pixels]
      m2pixel = D_nozzle_pixels / D_nozzle;
      if GET_D_EFF
        apiObj = iptgetapi(h_nozzle);
        % constrain choosing only within the image:
        fcn = makeConstrainToRectFcn('imline', get(gca,'XLim'),get(gca,'YLim'));
        apiObj.setDragConstraintFcn(fcn);
        title('Select the drag bar on the Effective Diameter');
        input('\n\nNow set the drag bar in Effective Diameter & Press any key to continue.\n','s');
        D_nozzle_pixels = ceil(apiObj.getDistance());%[pixels]
        D_nozzle = D_nozzle_pixels / m2pixel; % update D_nozzle
      end
      pos_nozzle = apiObj.getPosition();
      if CROP_IM
        new_im = im(1:floor(pos_nozzle(1,2)), :);%crop the pipe from the image
      else
        new_im = im;
      end
      close(h_fig);
    end
    
    function [data] = GetCFDdataFromANSYS(CFDpath)
      % Gets data from ANSYS CFD simulation saved in .xlsx file
      assert(~isequal(CFDpath,0));
      [~,txt,data]  = xlsread(CFDpath);
      data = data(size(txt,1)+1:end, :); % remove leading information rows
      data = [data(:,2), data(:,3), data(:,5)];% ColB = X, ColC = Y, ColE = Tot_temperature
      data = cell2mat(data);
    end
    
    function [new_im, new_rows, new_cols, symAxis] = InverseAbelImage(im, m2pixel, symAxis, jetBW)
      % Apply Inverse Abel on an image, row by row.
      % Method: Nelson-Olsen.
      % In each row, the radius is estimated using Otsu thresholding
      % with several levels.
      % INPUT: image 'im', 'm2pixel' - meter2pixel factor. 
      %     OPTIONAL: 'symAxis' - symmetry axis vector. If not specified,
      %     prompts the user to choose area of interest and calculates this
      %     axis.
      %               'jetBW' - mask of the actual jet in the image.
      % OUTPUT: Image after inversion, and 'new_rows' & 'new_cols' to be
      % used to crop other images to the ROI of the Jet.
      %% TODO: check for NaN in input!
      %% TODO: see if m2pixel is needed, or better to use pixel units.
      if nargin==3 || nargin < 2
        error('invalid args');
      end

      if nargin < 3
        %% Calc the symmetry axis
        nLevels = 6; % Number of levels for Otsu, found good enough to not lose
        % too much of the jet and maintain fairly big contingious regions
        % within the jet, yet filter out garbage.
        [~, rect, ~] = GasHelper.cropImageUI(im,'Abel Inversion: Select area, then confirm by double clicking');
        % Crop just the rows:
        new_rows = (rect(2):rect(2)+rect(4))';
        imCroppedRows = im(new_rows,:);
        %Apply Otsu's thresholding and further morphological opening to get
        %rid of small BLOBs, if needed (typically in images with small SNR):
        jetBW = GasHelper.getActualJetFromImage(imCroppedRows, nLevels);
        [symAxis, ~] = GasHelper.getAxisOfSymFromJetIm(jetBW.*imCroppedRows);
      else
        imCroppedRows = im;
        new_rows = 1:size(im,1);
        symAxis = symAxis;
      end
      % Just for plot purpose:
      imAxisMarked = jetBW.*imCroppedRows;
      imAxisMarked(:,symAxis) = min(imAxisMarked(:));
      GasHelper.plotIm(imAxisMarked,'Calculated Axis of symmetry');
      
      [M,N] = size(imCroppedRows);
      new_lines = cell(M,1);
      
      % Apply Abel inversion per row:
      for l=1:M % loop on each row and perform abel inversion
        line = imCroppedRows(l,:);
        IndLast = find(jetBW(l,:) > 0, 1, 'last');
        if isempty(IndLast) % if this row is all zeros in 'jetBW'
          new_lines{l} = nan(N, 1); % convert to 'nan' the whole row
          continue;
        end
        assert(symAxis < IndLast,'Error - jet edge index is smaller than jet centerline index!');
        line = line(symAxis:IndLast);
        Radii = length(line) /m2pixel*1e2; % estimated Radius in [cm]
        F_in = line;
        %% Among FE, ASAI & NO (Nelson-Olsen) methods - NO seems most accurate.
        F_in = F_in - min(F_in); % hopefully the minim
        f_rec_NO = NestorOlsenAbelInversion(F_in, Radii);
        %{
            f_rec_FE = abel_inversion(F_in, Radii, 10, 0, 0, 0);
            y=(0.0001:1:numel(F_in))*(Radii/numel(F_in));
            r = y;
             knots = 0.:.01*Radii:Radii;
             f_rec_ASAI = ASAI(r, F_in, y, knots);
        %}
        % Normalizing the distribution according to the centerline:
        % twice its integral should equal the measurement in the center.
        f_rec_NO = f_rec_NO *F_in(1) /(2*trapz(f_rec_NO));
        %       f_rec_FE = f_rec_FE *F_in(1) /(2*trapz(f_rec_FE));
        %       f_rec_ASAI = f_rec_ASAI *F_in(1) /(2*trapz(f_rec_ASAI));
        new_lines{l} = [flipud(f_rec_NO); f_rec_NO];
      end
      sizes = cellfun(@size, new_lines, num2cell(ones(M,1))); % always even by built
%       new_im = nan(M,max(sizes));% set the new image after abel inversion
      new_im = nan(M,N);% set the new image after abel inversion
      for indRow = 1 : M
%         new_im(indRow, 1+(max(sizes)-sizes(indRow))/2 : end-(max(sizes)-sizes(indRow))/2 ) = new_lines{indRow};
        new_im(indRow, 1+(N-sizes(indRow))/2 : end-(N-sizes(indRow))/2 ) = new_lines{indRow};
      end
      % DEBUG:  GasHelper.plotIm(new_im,'image after Abel Inversion');
%       new_cols = (symAxis - max(sizes)/2: symAxis+max(sizes)/2-1)';
      new_cols = (1:size(im,2))';
    end
    
    
    
    function [new_im] = InverseAbelHalfImage(im, cm2pixel, method, scale_output)
      % Apply Inverse Abel on an half part of a symmetric image, row by row. 
      % Method: Nelson-Olsen (default) or FE (Fourier expansion).
      % INPUT: 'im' - image with axis of symmetty in first column, masked so that pixels out of radius are zero.
      %        'cm2pixel' - cm2pixel factor. 'method' - 'NO' or 'FE'.
      %        'normalize' - Normalize NO by the experimental fit or not.
      % OUTPUT: Image after inversion.
      %% TODO: check for NaN in input!
      %% TODO: see if cm2pixel is needed, or better to use pixel units.
      if nargin < 3
        method = 'NO'; % Nestor-Olsen
      end
      if nargin < 4 
        scale_output = false; % whether to normalize the output or not.
      end
      if scale_output
        NO_fit = load('Nestor_Olsen_correction_factor_fit.mat');
        NO_fit = NO_fit.Nestor_Olsen_correction_factor_fit;
      end
      [M,N] = size(im);
      new_lines = cell(M,1);
      % Apply Abel inversion per row:
      for l=1:M % loop on each row and perform abel inversion
        line = im(l,:);
        IndLast = find(line>0, 1, 'last'); % Radius
        if isempty(IndLast) % if this row is all zeros in 'jetBW'
          new_lines{l} = nan(N, 1); % convert to 'nan' the whole row
          continue;
        end
        assert(IndLast>0,'Error - jet edge miscalculated!');
        line = line(1:IndLast);
        Radii = length(line) /cm2pixel; % estimated Radius in [cm]
        F_in = line;
        %% Among FE, ASAI & NO (Nelson-Olsen) methods - NO seems most accurate.
%         F_in = F_in - min(F_in); % hopefully the minimum. TODO: remove
        F_in = F_in - F_in(end);
        switch method
          case 'FE'
            f_rec = abel_inversion(F_in, Radii, 10, 0, 0, cm2pixel==1);
          otherwise
            f_rec = NestorOlsenAbelInversion(F_in, Radii);
        end
        %{
            y=(0.0001:1:numel(F_in))*(Radii/numel(F_in));
            r = y;
             knots = 0.:.01*Radii:Radii;
             f_rec_ASAI = ASAI(r, F_in, y, knots);
        %}
        if scale_output && isequal(method, 'NO')
%           % Normalizing the distribution according to the centerline:
%           % twice its integral should equal the measurement in the center.
%           f_rec = f_rec *F_in(1) /(2*trapz(f_rec));
          f_rec = NO_fit(numel(line)) * f_rec; % use the experimental fit from "abel_inversion_test.m"
        end
        % update OUTPUT:
        new_lines{l} = f_rec;
      end
      sizes = cellfun(@size, new_lines, num2cell(ones(M,1)));
%       new_im = nan(M,max(sizes));% set the new image after abel inversion
      new_im = nan(M,N);% set the new image after abel inversion
      for indRow = 1 : M
        new_im(indRow, 1:sizes(indRow)) = new_lines{indRow};
      end
    end
    
    
    function [new_im, new_rows, new_cols, symAxis] = InverseAbelHalfImage_OLD(im, m2pixel, symAxis, jetBW)
      % Apply Inverse Abel on an half part of a symmetric image, row by row. 
      % Method: Nelson-Olsen.
      % INPUT: image 'im', 'm2pixel' - meter2pixel factor. 
      %     OPTIONAL: 'symAxis' - symmetry axis vector. If not specified,
      %     prompts the user to choose ROI and calculates this axis.
      %               'jetBW' - mask of the actual jet in the image.
      % If last 2 args are omitted, the radius in each row is estimated 
      % using Otsu thresholding with several levels.
      % OUTPUT: Image after inversion, and 'new_rows' & 'new_cols' to be
      % used to crop other images to the ROI of the Jet.
      %% TODO: check for NaN in input!
      %% TODO: see if m2pixel is needed, or better to use pixel units.
      if nargin==3 || nargin < 2
        error('invalid args');
      end

      if nargin < 3
        %% Calc the symmetry axis
        nLevels = 6; % Number of levels for Otsu, found good enough to not lose
        % too much of the jet and maintain fairly big contingious regions
        % within the jet, yet filter out garbage.
        [~, rect, ~] = GasHelper.cropImageUI(im,'Abel Inversion: Select area, then confirm by double clicking');
        % Crop just the rows:
        new_rows = (rect(2):rect(2)+rect(4))';
        imCroppedRows = im(new_rows,:);
        %Apply Otsu's thresholding and further morphological opening to get
        %rid of small BLOBs, if needed (typically in images with small SNR):
        jetBW = GasHelper.getActualJetFromImage(imCroppedRows, nLevels);
        [symAxis, ~] = GasHelper.getAxisOfSymFromJetIm(jetBW.*imCroppedRows);
      else
        imCroppedRows = im;
        new_rows = 1:size(im,1);
        symAxis = symAxis; % needed since its input & output argument
      end
      % Just for plot purpose:
      imCroppedRows = imCroppedRows.*jetBW; % apply shape mask
      imAxisMarked = imCroppedRows;
      imAxisMarked(:,symAxis) = min(imAxisMarked(:));
      GasHelper.plotIm(imAxisMarked,'Calculated Axis of symmetry');
      
      [M,N] = size(imCroppedRows);
      new_lines = cell(M,1);
      
      % Apply transform only on the right side:
      imCroppedRows = imCroppedRows(:, symAxis:end);
      
      % Apply Abel inversion per row:
      for l=1:M % loop on each row and perform abel inversion
        line = imCroppedRows(l,:);
        IndLast = find(line>0, 1, 'last'); % Radius
        if isempty(IndLast) % if this row is all zeros in 'jetBW'
          new_lines{l} = nan(N, 1); % convert to 'nan' the whole row
          continue;
        end
        assert(IndLast>0,'Error - jet edge miscalculated!');
        line = line(1:IndLast);
        Radii = length(line) /m2pixel*1e2; % estimated Radius in [cm]
        F_in = line;
        %% Among FE, ASAI & NO (Nelson-Olsen) methods - NO seems most accurate.
%         F_in = F_in - min(F_in); % hopefully the minimum. TODO: remove
        f_rec_NO = NestorOlsenAbelInversion(F_in, Radii); % TODO: f(R)===0, introduces errors
       
%         f_rec_FE = abel_inversion(F_in, Radii, 10, 0, 0, 0);
        %{
            y=(0.0001:1:numel(F_in))*(Radii/numel(F_in));
            r = y;
             knots = 0.:.01*Radii:Radii;
             f_rec_ASAI = ASAI(r, F_in, y, knots);
        %}
        % Normalizing the distribution according to the centerline:
        % twice its integral should equal the measurement in the center.
        f_rec_NO = f_rec_NO *F_in(1) /(2*trapz(f_rec_NO));
%         f_rec_FE = f_rec_FE *F_in(1) /(2*trapz(f_rec_FE));
        %       f_rec_ASAI = f_rec_ASAI *F_in(1) /(2*trapz(f_rec_ASAI));
        
        % update OUTPUT:
        new_lines{l} = f_rec_NO;
%         new_lines{l} = [flipud(f_rec_NO); f_rec_NO];
      end
      sizes = cellfun(@size, new_lines, num2cell(ones(M,1)));
%       new_im = nan(M,max(sizes));% set the new image after abel inversion
      new_im = nan(M,N-symAxis+1);% set the new image after abel inversion
      for indRow = 1 : M
% %         new_im(indRow, 1+(max(sizes)-sizes(indRow))/2 : end-(max(sizes)-sizes(indRow))/2 ) = new_lines{indRow};
%         new_im(indRow, 1+(N-sizes(indRow))/2 : end-(N-sizes(indRow))/2 ) = new_lines{indRow};
        new_im(indRow, 1:sizes(indRow)) = new_lines{indRow};
      end
      % DEBUG:  GasHelper.plotIm(new_im,'image after Abel Inversion');
%       new_cols = (symAxis - max(sizes)/2: symAxis+max(sizes)/2-1)';
      new_cols = (symAxis:size(im,2))';
    end
    
    
    function [FA] = ApplyForwardAbelJet(im, pixel2units)
      % Applies Forward Abel on an image.
      %INPUTS: 'im' - axisymmetric distribution around the X axis. X axis 
      % assumed as the 1st column, so this is a half of the symmetric image.
      % 'pixel2units' - convertion from pixels to units. Default = 1.
      if nargin < 2
        pixel2units = 1;
      end
      % Get the Jet boundaries:
      nLevels = 6;
      jetBW = GasHelper.getActualJetFromImage(im,nLevels);
      % Get the axis of symmetry. assuming intensity is maximal near it:
      [axisCol, ~] = GasHelper.getAxisOfSymFromJetIm(im);
      numColsRef = size(im,2);
      
      im = im.*jetBW;
      im = im(:, axisCol:end); % only one side of the "symmetric" distribution is needed

      [M,N] = size(im);
      FA = zeros(M,N);
      for indRow = 1 : M
        IndLast = find(im(indRow,:) > 0, 1, 'last');
        FA(indRow,:) = forwardAbel(im(indRow,1:IndLast), IndLast*pixel2units); % TODO!!!!! change to actual radius
      end
      FA = [fliplr(FA(:,2:end)), FA];
      
      % Align the forward-abel image to the initial image of the jet:
      FA = GasHelper.AlignImagesHoriz(FA, N, axisCol, numColsRef);
    end
    
    function [FA] = ApplyForwardAbel(im, pixel2units)
      % Applies Forward Abel on an image.
      %INPUTS: 'im' - axisymmetric distribution around the X axis. X axis 
      % assumed as the 1st column, so this is a half of the symmetric image.
      % This image is assumed to be masked such that pixel outside the
      % shape are zero.
      % 'pixel2units' - convertion from pixels to units. Default = 1.
      if nargin < 2
        pixel2units = 1;
      end
      [M,N] = size(im);
      FA = zeros(M,N);
      for indRow = 1 : M
        IndLast = find(im(indRow,:) > 0, 1, 'last'); % get the radius
        FA(indRow,1:IndLast) = forwardAbel(im(indRow,1:IndLast), IndLast*pixel2units);
      end
    end
    
    
    function [new_im] = AlignImagesHoriz(im, col, colRef, numColsRef)
      % Aligns horizontally 'im' so that 'col' column in 'im'
      % would be on 'colRef' and number of columns will be 'numColsRef'. 
      % Cutting/padding with 'NaN' will be performed, if necessary.
      [M, N] = size(im);
      new_im = nan(M, numColsRef); % initialiation
      
      % Treat the right side of the image:
      if N-col <= numColsRef-colRef % we can copy entire right side of 'im'
        new_im(:,colRef:colRef+N-col) = im(:, col:end);
      else
        new_im(:,colRef:end) = im(:, col:col+numColsRef-colRef); % cut the right side of 'im'
      end
      % Treat the left side of the image (not including 'col'):
      if col <= colRef % we can copy entire left side of 'im'
        new_im(:,colRef-col+1:colRef-1) = im(:, 1:col-1);
      else
        new_im(:,1:colRef-1) = im(:, col-colRef+1:col-1); % cut the left side of 'im'
      end
    end
    
    function [symCol, skew_angle_deg] = getAxisOfSymFromJetIm(im,method)
      % Assuming: Max intensity is near jet axis and the axis is a column.
      if nargin < 2
        method = 'gaussians';
      end
      if isvector(im)
        [~,symCol] = nanmax(im);
        skew_angle_deg = [];
      else
        switch method
          case 'gaussians'
            [M,N] = size(im);
            x = (1:N)';
            Idx = nan(size(x));
            for i = 1 : M
              y = im(i,:)';
              ft = fittype('a*exp(-0.5*((x-mu)/sigma).^2) + b',...
                'independent','x', 'coefficients', {'a','b','mu','sigma'});
              StartPoint = [nanmax(y), nanmin(y), floor(N/2), floor(N/10)]; % Guess of b=0 & a=1/6
              [fit1,gof,fitinfo] = fit(x,y,ft,'StartPoint',StartPoint);
              Idx(i) = round(fit1.mu);
            end
          case 'max'
            [~,Idx] = nanmax(im,[],2);
            stdIdxs = nanstd(Idx);
            if stdIdxs > 10 % indication for outliers, e.g. in low SNR
              Idx = Idx(abs(Idx-nanmean(Idx)) < 0.5*stdIdxs);
            end
        end
        % Find the axis of symmetry from a fit of the maxs to a line:
        x = (1:length(Idx))';
        % GasHelper.plotIm(im,'Estimated max points'); hold on; scatter(Idx, x, 'r.'); % DEBUG
        coeffs = polyfit(x, Idx, 1); % expecting the 'x' coeff to be sub-pixel (<1), ideally 0.
        % Mean the maxima:
        symCol = round(nanmean(Idx(end-floor(numel(x)/2):end)));% TODO: use just several rows above nozzle?
        skew_angle_deg = atand(coeffs(1));
      end
    end
    
    function [mainBW, Blob_area_th] = getMainObjFromImage(im,nLevels)
      if nargin<2 || (nargin==2 && isempty(nLevels))
        nLevels = 6;
      end
      imOtsu = otsu(im, nLevels);
      imOtsuBW = imOtsu > 1;
      BLOBs = regionprops('table', imOtsuBW, 'Area');
      if numel(BLOBs) < 2 %  if one BLOB, it's assumed the main, no need to further filter.
        mainBW = imOtsuBW;
        Blob_area_th = BLOBs.Area;
        return;
      end
      Blobs_area_arr = sort(table2array(BLOBs),'descend');
      % At this point it is assumed that the main object is the 1st BLOB, and it is
      % by far greater in area than the next ones:
      Blob_area_th = Blobs_area_arr(2)+1; %'+1' is since 'bwareaopen' removes area < Blob_area_th
      % Apply image opening, to get rid of the extra noise:
      mainBW = bwareaopen(imOtsuBW,Blob_area_th);
    end
    
    function [jetBW] = getActualJetFromImage(im,nLevels)
      %Apply Otsu's thresholding with optional 'nLevels' and further morphological opening to get
      %rid of small BLOBs, if needed (typically in images with small SNR).
      % OUTPUT: 'jetBW' - BW image of the actual jet, w/o noisy blobs
      if nargin<2
        nLevels = [];
      end
      [jetBW, Blob_th] = GasHelper.getMainObjFromImage(im,nLevels);
      [jetBW2, Blob_th2 ] = GasHelper.getMainObjFromImage(im, 3); % small number of level, for low SNR images
      % Check intersection between 2 thresholded images; for normal SNR
      % images, there should be intersection. for small SNR, take the
      % stronger filtered image:
      if sum(sum((jetBW & jetBW2))) < min(Blob_th, Blob_th2)
        jetBW = jetBW2;
      end
      % DEBUG: GasHelper.plotIm(jetBW,'Otsu with nLevels lvls of the image');
      % DEBUG: GasHelper.plotIm(jetBW2,'Otsu with 3 lvls of the image');
    end
    
    function [SNR] = calc_weights_for_IT_fusion(mu_DL)
      % Generates the weights for the MultiITRP procedure.
      % Taken from Iliya's paper eq.3.
      SNR = 1245*exp(4.438e-5 * mu_DL) + exp( 4.438e-5 * mu_DL * (-6.301) ) ; 
    end
    
    function [R_est] = GetMeasurementRByMultiIT2RP(baseDir, tempStr, im, rows, cols, biasSubframe, ALL_PIXELS_SEE_OBJECT, FLIP_BG_IMG_HORIZONTAL, FLIP_BG_IMG_VERTICALLY, N_FRAMES)
      %% estimate the measurement's radiation from DL = R*IT^P
      if  ~exist('N_FRAMES', 'var')
        N_FRAMES = 0;
      end
      if  ~exist('FLIP_BG_IMG_HORIZONTAL', 'var')
        FLIP_BG_IMG_HORIZONTAL = false;
      end
      if  ~exist('FLIP_BG_IMG_VERTICALLY', 'var')
        FLIP_BG_IMG_VERTICALLY = false;
      end
      USE_BIAS_FRAME = ~isempty(biasSubframe);
      totalFilesList = dir([baseDir,'/*.ptw']);
      if isempty(totalFilesList)
        error('invalid folder path, or no .ptw files there...');
      end
      totalFilesList = {totalFilesList(:).name}';
      [props.G,props.T,props.CWL,props.IT,props.isBG,props.name] = parseFilename(totalFilesList); %divide list to params
      [caseList,~,~] = GasHelper.getMatchingCase(totalFilesList, props, {'T'}, {str2double(tempStr)});
      [IMList,~,~] = GasHelper.getMatchingCase(caseList, props, {'isBG'}, {false});
      [BGList,~,~] = GasHelper.getMatchingCase(caseList, props, {'isBG'}, {true});
      %Calc mask to allow equal #pixels in all objects (calc from 1st image pair).
      if ~ALL_PIXELS_SEE_OBJECT
        objMask = ones(size(im,1), size(im,2));%initial mask
        imPair = ImageBGPairExt([baseDir,IMList{1}]);
        imPair.setBG([baseDir,BGList{1}]);   
        subIm = abs(imPair.getSubtracted(FLIP_BG_IMG_HORIZONTAL, FLIP_BG_IMG_VERTICALLY));
        if N_FRAMES > size(subIm, 3) || ~N_FRAMES % average all available frames
          N_FRAMES = size(subIm, 3);
        end
        subIm = mean(subIm(:,:,1:N_FRAMES), 3);
        [~, object] = GasHelper.processImGetStats(subIm);
        objMask(isnan(object)) = NaN;
      end
      % prealloc:
      DLs_IM = zeros(size(im,1), size(im,2), numel(BGList));
      DLs_BG = zeros(size(im,1), size(im,2), numel(BGList));
      DLs = zeros(size(im,1), size(im,2), numel(BGList));
      ITs = zeros(numel(BGList), 1);
      for indIMPair = 1:numel(BGList)
        imPair = ImageBGPairExt([baseDir,IMList{indIMPair}]);
        imPair.setBG([baseDir,BGList{indIMPair}]);
        %subtract the bias frame from each frame:
        if USE_BIAS_FRAME
          im = imPair.getImage();
          bg = imPair.getBG();
          DLs_IM(:,:,indIMPair) = im(rows,cols) - biasSubframe(rows,cols);
          DLs_BG(:,:,indIMPair) = bg(rows,cols) - biasSubframe(rows,cols);
        else
          subIm = abs(imPair.getSubtracted(FLIP_BG_IMG_HORIZONTAL, FLIP_BG_IMG_VERTICALLY));
          if N_FRAMES > size(subIm, 3) || ~N_FRAMES % average all available frames
            N_FRAMES = size(subIm, 3);
          end
          subIm = mean(subIm(:,:,1:N_FRAMES), 3);
          DLs(:,:,indIMPair) = subIm(rows,cols);
        end
        ITs(indIMPair) = double(imPair.IT)*1e6; %change IT to [us]
      end
      if USE_BIAS_FRAME
        % TODO: Need to extend the capability of
        % 'suitBiasSubframeFromBiasFrame' to take the correct crop when the
        % image was cropped arbitrarily.
        % (Bad example: Z:\FLIR_Experimental\16-07-24_CloseJet\G101_T500_F4370_IT2530.2_IM_JetDistanceM.ptw)
        [R_est_im,~] = MultiITtoRP(ITs, DLs_IM);
        [R_est_bg,~] = MultiITtoRP(ITs, DLs_BG);
        R_est = R_est_im - R_est_bg;
      else
        if size(DLs,3) > 1 % need more than one IT for this:
          [R_est,~] = MultiITtoRP(ITs, DLs); % For now this is the favored one as the bias frame process inject noise
        else % use reciprocity rule:
          R_est = DLs ./ ITs;
        end
        %{
        mu_DL = nanmean( reshape( DLs, [], size(DLs,3) ) , 1);
        [SNR] = GasHelper.calc_weights_for_IT_fusion(mu_DL);
        [R_est,~] = MultiITtoRP(ITs, DLs, [], SNR); % For now this is the favored one as the bias frame process inject noise
        %}
      end
      if ~ALL_PIXELS_SEE_OBJECT % apply mask if needed
        R_est = R_est .* repmat(objMask, [1,1,size(R_est,3)] );
      end
    end
    
    
    function [T, Calib_params] = getCalibrationFromR(CalibFilePath, num_calib_params_2_or_3, is_representative_R, apply_atm_model_in_calibraion, calib_experiment_params)
      % INPUTS: 'CalibFilePath', a file with calibration experimental data:
      %         {R, experimentCasesArr}, 
      %         'is_representative_R' - whether to calculate single set of
      %         parameters based on representative R, or per-pixel.
      %         'apply_atm_model_in_calibraion' - apply atmosheric
      %         transmittance model
      %         'calib_experiment_params' - has the fields: 'e_BB' -
      %         black-body assumed emissivity. 'filterParams' - struct with
      %         the filter parameters used. 'atmParams' - struct with
      %         atmospehric path's params, as well as HITRAN's transmission coeff file.
      % OUTPUTS: 'T' - array of temperatures[K] of the calibration process
      %         'Kw','a0',('a1') - calibration parameters.
      if nargin < 5
        error('invalid #input args!');
      end
      %% Get calibration experimental data: {R, experimentCasesArr}:
      [CalibFileName, CalibFilePath] = uigetfile({'*.mat','MATLAB data'},'Select Calibration matrix',CalibFilePath);
      if isempty(CalibFileName)
        error('invalid folder path, or claibration matrix there...');
      end
      load([CalibFilePath, CalibFileName]); % OUTPUT: 4th dim matrix 'R', and 'experimentCasesArr'.
      %Rotrou 2006: NIR thermography with silicon FPA, [17]:
      % For each pixel we find the following 3 (or 4) params:
      %   Kw, a0, a1, (a2), by this fit:
      %  ln(DL_normalized) = ln(Kw) - C2./T/.lambda_x =~  ln(Kw) - C2.*(a0 + a1.*T + ...)
      % log(Kw)-C2*(a0+a1.*X+a2.*X.^2) yields good results..
      %%
      % Problems:
      % 1) Need to use Plate BB for this calibration - all pixels should see the
      %     same radiation
      % 2) No radiation calibration - it is for DL...
      % 3) Model seems Weinish (https://en.wikipedia.org/wiki/Sakuma-Hattori_equation, 9th form)
      %     but my range might not hold Wein's assumption!!!
      
      %% estimate R from DL = R*IT^P model
      T = GasHelper.C2K(experimentCasesArr.temps); % T[K] used in the calibration
      
      if is_representative_R
        progressbar('Temp')
        R_est = nan(size(T));
        for indT = 1:numel(T) % Loop over temperatures
          R_est(indT) = HistogramHelper.getFittedGauss3Peak(squeeze(R(indT,1,:,:)),[],[],[],3); % Get representative R from the R image
          progressbar(indT/numel(T));
        end
      else
        R_est = squeeze(R(:,1,:,:));
      end
          
      X = max(T)./T; % the independant variable. scaled to ease estimation.
      
      [~, nX, nY] = size(R_est);
      
      % Prealloc: parameter matrices
      Kw = nan(nX, nY);
      a0 = nan(nX, nY);
      if num_calib_params_2_or_3 == 3
        a1 = nan(nX, nY);
      end
      %%
      % Formulate as a M*P = Y system , where P is the desired parameters vector.
      % We have: ln(DL) =  ln(Kw)-C2*(a0/T + a1/T^2)
      % Denote Y = ln(DL), X = 1/T. We write:
      % Y = ln(Kw) - a0*X + a1*X.^2.
      % Denote: p3 = log(Kw), p2 = -C2*a0, p1 = -C2*a1,  we get:
      % (*) Y = p3 + p2*X + p1*X^2 ======>    Y = [X.^2 X ones(size(X))]*P
      % =====> P  =  M \ Y
      
      % Using (*), we can afterward solve for T :
      % X1,2 = (-p2 +- sqrt(p2^2 - 4*p1*(p3-Y)))/2/p1
      % ===> T = 1/X = 2*p1 ./ (-p2 +- sqrt(p2^2 - 4*p1*(p3-ln(DL_n))))
      
      % And if the object is of known "extended effective emmisivity" (my term),
      % we have: ln(DL) =  ln(Kw) - ln(e) -C2*(a0/T + a1/T^2)
      % that is, (**) Y = p3 - ln(e) + p2*X + p1*X^2
      % solving we get: X1,2 = (-p2 +- sqrt(p2^2 - 4*p1*(p3-ln(e)-Y)))/2/p1
      
      Y = log(R_est) - log(calib_experiment_params.e_BB)*ones(size(R_est)); % measurement - log of DL normalized by IT
      
      if apply_atm_model_in_calibraion % Apply atmospheric transmittance:
        [AtmFileName, AtmFilePath] = uigetfile({'*.mat','MATLAB data'},'Select atm transmittance matrix',calib_experiment_params.atmParams.atmTransmittanceFileName);
        atm_trans_FileName = [AtmFilePath,AtmFileName];
        if isempty(atm_trans_FileName)
          error('invalid path, or transmittance matrix there...');
        end
%         trans_eff = GasHelper.getEffectiveTransmittance(calib_experiment_params.filterParams, atm_trans_FileName);
        calib_experiment_params.atmParams.atmTransmittanceFileName = atm_trans_FileName;
        trans_eff = GasHelper.getEffectiveTransmittance(calib_experiment_params.filterParams, calib_experiment_params.atmParams, 1);
        Y = Y - log(trans_eff)*ones(size(R_est));
      end
      
      if is_representative_R % find 2/3 parameters for the whole image
        if num_calib_params_2_or_3 == 3
          M = [X.^2 X ones(size(X))];
          P = M \ Y;
          a1 = P(1);
          a0 = P(2);
          Kw = P(3);
          Kw = Kw*ones(size(R,3), size(R,4));
          a0 = a0*ones(size(R,3), size(R,4));
          a1 = a1*ones(size(R,3), size(R,4));
        else
          M = [X ones(size(X))];
          P = M \ Y;
          a0 = P(1);
          Kw = P(2);
          Kw = Kw*ones(size(R,3), size(R,4));
          a0 = a0*ones(size(R,3), size(R,4));
        end
      else % find 2/3 parameters for each Pixel
        for i = 1: nX
          for j = 1 : nY
            if ~any(isnan(R_est(:,i,j)))
              if num_calib_params_2_or_3 == 3
                P = [X.^2 X ones(size(X))] \ Y(:,i,j); % M is probably ill conditioned!!
                %{
        %%%%% Verifying the solution with several methods %%%%%
        M = [X.^2 X ones(size(X))];
        P2 = inv(M'*M)*M'*Y(:,i,j); % using pseudo-inverse
        [U,S,V] = svd(M,0);
        P3 = V*( (U'*Y(:,i,j))./diag(S) ); %using SVD
        if norm(P2-P,inf) > 1e-11 || norm(P3-P,inf) > 1e-11
          error('Mismatch!!!!');
        end
                %}
                Kw(i,j) = P(3);
                a0(i,j) = P(2);
                a1(i,j) = P(1);
              else
                P = [X ones(size(X))] \ Y(:,i,j); % M is probably ill conditioned!!
                Kw(i,j) = P(2);
                a0(i,j) = P(1);
              end
            end
          end
          progressbar(i/nX, j/nY);
        end
        progressbar(i/nX, 0);
      end
      % Output the calibration params:
      Calib_params.Kw = Kw;
      Calib_params.a0 = a0;
      if num_calib_params_2_or_3 == 3
        Calib_params.a1 = a1;
      end
      
    end
    
    function [F] = generateInterpolantFromHITRANData(Temps, concentrations, HITRAN_calculated_matrix)
      % Generates interpolant object 'F' from the calculated HITRAN matrix
      % (emissivity or Radiance, per temps per concentrations and a fixed
      % OPL)
      [cT, cC] = ndgrid(Temps,concentrations);
      F = griddedInterpolant(cT,cC,HITRAN_calculated_matrix);
    end
    
    function [e_eff] = interpHITRANData(avgTBlocks, avgCBlocks, Interpolant)
      % INPUTS: 'size_rows', 'size_cols' - size of the image.
      %         'avgTBlocks', 'avgCBlocks' - the averaged temperature and
      %         concentrations within the blocks image.
      %         'Interpolant' - interpolant object from 'generateInterpolantFromHITRANDATA'.
      % OUTPUTS: 'e_eff' - an image of emissivity or radiance, depending on
      % inputs.
      if nargin < 3 || (numel(avgTBlocks) ~= numel(avgCBlocks))
        error('invalid args!');
      end
      e_map = nan(size(avgTBlocks));
      % Perform 2d interpolation of the HITRAN data:
      %below, 'unique' is replaced by 'uniquetol' to reduce the memory size in
      %cases of small block size:
      uniquetolFlag = false;
      tolUniquetol = 1e-4;
      Temps = unique(avgTBlocks);
      if numel(Temps) > 1e4 % indication to possible memory problem
        uniquetolFlag = true;
        Temps = uniquetol(avgTBlocks,tolUniquetol,'DataScale', nanmax(avgTBlocks(:))); % tolerance is scaled by max(avgTBlocks)
        Concenrations = uniquetol(avgCBlocks,tolUniquetol,'DataScale', nanmax(avgCBlocks(:)));
      else
        Concenrations = unique(avgCBlocks);
      end
      Temps = Temps(Temps>0); % also get rid of NaNs
      Concenrations = Concenrations(Concenrations>0);% also get rid of NaNs
      % Use 'griddedInterpolant' object 'F' for interpolation:
      [Tq, Cq] = ndgrid(Temps,Concenrations); % replaced meshgrid
      InterpolatedData = Interpolant(Tq,Cq);
%       disp(['meanEmissionInterp size = ',num2str(size(InterpolatedData))]);
      % apply interpolation to get the emissivity map:
      for i = 1:size(avgTBlocks,1)
        for j = 1:size(avgTBlocks,2)
          if avgTBlocks(i,j) == 0 || isnan(avgTBlocks(i,j))
%             e_map(i,j) = nan; %TODO: Not needed if e_map is initialized as nan
          else
            if uniquetolFlag
              e_map(i,j) = InterpolatedData( find(abs(Temps-avgTBlocks(i,j))==min(abs(Temps-avgTBlocks(i,j)))), find(abs(Concenrations-avgCBlocks(i,j))==min(abs(Concenrations-avgCBlocks(i,j)))) );
            else
              e_map(i,j) = InterpolatedData( Temps==avgTBlocks(i,j), Concenrations==avgCBlocks(i,j) );
            end
          end
        end
      end
%       e_map(e_map==1) = nan; % TODO: Not needed if e_map is initialized as nan
      e_eff = e_map;
    end
    
    function [outCellArr] = cropROIFromImagesCellArray(rows, cols, imCellArr)
      %INPUT: 'rows','cols' - index vectors representing the ROI. 'imCellArr' - cell array of images
      %OUTPUT: 'outCellArr' - cell array of cropped images according to ROI
      if nargin < 3
        error('invalid args!');
      end
      func = @(im) im(rows,cols); % cropping function
      outCellArr = cellfun(func, imCellArr, 'UniformOutput', false);
    end
    
    function [Lv] = PlanckWN(wn,T)
      % INPUT: 'wn' - wavenumber [cm^-1]. 'T' - temperature [K].
      % OUTPUT: 'Lv' - Spectral Radiance in [W/m^2/sr/cm^-1]
      % Plank's law in wavenumber units [W/m^2/sr/cm^-1]:
      % Lv = 2e8hc^2v^3/(exp(100h*c*v/(k*T))-1) = 1e8*C1*v^3/(exp(100*C2*v/T))-1)
      Lv = 1e8*GasHelper.C1.*wn.^3./(exp(1e2*GasHelper.C2.*wn./T) - 1);
    end
    
    function [Lv] = WeinWN(wn,T)
      % INPUT: 'wn' - wavenumber [cm^-1]. 'T' - temperature [K].
      % OUTPUT: 'Lv' - Spectral Radiance in [W/m^2/sr/cm^-1]
      % Wein's approximation to Planck's law in wavenumber units:
      % Lv = 1e8*C1*v^3/exp(100*C2*v/T)
      Lv = 1e8*GasHelper.C1.*wn.^3./exp(1e2*GasHelper.C2.*wn./T);
    end
    
    function [error_pecentage] = checkWeinError(wn, T)
      % OUTPUT: percentage error between Planck and Wein
      Lv = GasHelper.PlanckWN(wn,T);
      Wv = GasHelper.WeinWN(wn,T);
      error_pecentage = 100*abs(Wv-Lv)./Lv;
    end
    
    function [photon_energy] = getPhotonEnergyFromWN(wn)
      % INPUT: 'wn' - wavenumber [cm^-1]. OUTPUT: 'photon_energy' [J]
      photon_energy = 1e2*GasHelper.h*GasHelper.c.*wn; % [J] 
    end
    
    function [h_fig] = plotIm(im, title_str, x_axis, h_fig)
      % plots either 1D or image, depending on 'im'. if 1D 'x_axis' is
      % optional. 'h_fig' is optional only for 1D im.
      if nargin < 2
        title_str = '';
      end
      isSourceVector = any(size(im)==1);% 'im' is vector, use plot()
      isAppend2Plot = isSourceVector && nargin==4; % append to plot, or create new figure
      if isAppend2Plot
        figure(h_fig); hold on;
      else
        h_fig = figure;
      end
      if isSourceVector% 'im' is vector, use plot()
        if nargin < 3 || (nargin >= 3 && isempty(x_axis))
          plot(im);
        else
          plot(x_axis, im);
        end
      else
        imagesc(im); colorbar; axis image; % impixelinfo;
      end
      title(title_str);
      % close the appending option to the given figure:
      if isAppend2Plot
        figure(h_fig); hold off;
      end
    end
    
    function [h_fig] = surfIm(im, title_str)
      if nargin < 2
        title_str = '';
      end
      h_fig = figure; surf(im,'edgecolor','none'); view(0,-85); title(title_str);
    end
    
    function [h_fig] = contourIm(im, title_str)
      if nargin < 2
        title_str = '';
      end
      h_fig = figure; contour(im,'edgecolor','none'); view(0,-85); title(title_str);
    end
    
    
    function [h_fig] = histIm(im, nBins, title_str)
      if nargin < 3
        title_str = '';
      end
      h_fig = figure; histogram(im,nBins); title(title_str);
    end
    
    function [lines, rect, h_fig] = cropImageUI(im, title_str)
      % Asks the user to crop image and returns the the ROI image & its boundaries.
      if nargin < 2
        title_str = '';
      end
      h_fig = GasHelper.plotIm(im,title_str);
      GasHelper.maximizeFigure(); % maximize figure for convienience
      [lines, rect] = imcrop;
      %round coordinates, as they are fractions:
      rect(1:2) = ceil(rect(1:2));
      rect(3:4) = floor(rect(3:4));
    end
    
    function [diffIm] = compareTempsSimJet2CalcJet(SimIm, CalcIm)
      % Compares the simulted Temperature field with the calculated field
      if ~GasHelper.checkImSizes(SimIm, CalcIm)
        error('GasHelper::compareTempsSimJet2CalcJet: images sizes are different!');
      end
      diffIm = CalcIm-SimIm;
      GasHelper.plotIm(diffIm,'Difference between Simulated T & Calculated T [^oC]');
    end
    
    function [isEqual] = checkImSizes(im1, im2)
      [M1, N1] = size(im1);
      [M2, N2] = size(im2);
      isEqual = (M1==M2) && (N1==N2);
    end
    
    function sig_detrend = myDetrend(sig)
      % See https://www.mathworks.com/help/signal/examples/peak-analysis.html
      poly_deg = 6;
      sig = sig(:);
      x = (1:numel(sig))';
      [p,~,mu] = polyfit(x, sig, poly_deg); % fit to polynomial with scale and shift
      f_y = polyval(p, x, [], mu); % assumed trend
      sig_detrend = sig - f_y;
    end
    
    function xlims = get_xLims_from_cfit(cfit_obj)
      coeffVals = coeffvalues(cfit_obj);
      xlims = [coeffVals.breaks(1), coeffVals.breaks(end)];
    end
    
    function xlims_intersection = get_xLims_intersection_from_cfits(cfits_cellArr)
      xlims_intersection = [-Inf, Inf];
      for i = 1:numel(cfits_cellArr)
        xlims = GasHelper.get_xLims_from_cfit(cfits_cellArr{i});
        xlims_intersection = [max(xlims_intersection(1),xlims(1)), min(xlims_intersection(2),xlims(2))];
      end
    end
    
    function [x, total_tf] = get_total_tf_from_cfits(cfits_cellArr, x_vec)
      xlims_intersection = GasHelper.get_xLims_intersection_from_cfits(cfits_cellArr);
      if nargin < 2 
        x = (xlims_intersection(1) : 1e-4 : xlims_intersection(2))';
      else
        assert(xlims_intersection(1)<=x_vec(1) && xlims_intersection(2)>=x_vec(end), 'Error: ''x'' vector is greater than the intersection x vector of fits'); 
        x = x_vec;
      end
      eval_cfit = @(cfit_f) feval(cfit_f, x);
      feval_cfits = cellfun(eval_cfit, cfits_cellArr,'UniformOutput',false);
      feval_cfits = cell2mat(feval_cfits);
      total_tf = feval_cfits(:,1);
      for i = 2 : size(feval_cfits,2)
        total_tf = total_tf .* feval_cfits(:,i);
      end
    end
    
    function [out] = compare_spectra(x1, x2)
      % TODO DOODODOD
      assert(numel(x1)==numel(x2));
      compare_fnc = @(x,y) norm(x-y,2); %Euclidian norm
      out = compare_fnc(x1,x2);
    end
    
    function [wl_vect, optics_cfits, optics_tf] = get_optics_tf(apparatus_fits_path, hitranParams, plot_tf)
      % Load apparatus' transfer functions - Detector, Lens & Filter %%%
      apparatus_cfits = load(apparatus_fits_path);
      optics_cfits.Lens = apparatus_cfits.L0118; % [%]
      optics_cfits.Detector = apparatus_cfits.Detector_SCD;
      optics_cfits.Filter = apparatus_cfits.Filt4370; % [%]
      % Get HITRAN's wavenumbers & wavelength vectors:
      wn_vec = (hitranParams.min_wn : hitranParams.wn_step : hitranParams.max_wn-hitranParams.wn_step)'; % [cm^-1]
      wl_vect = flipud(GasHelper.wn2wl(wn_vec)); % [um]
      % Get the intersecting wavelength range [um] of all fits:
      cfits_arr = {optics_cfits.Lens, optics_cfits.Detector, optics_cfits.Filter};
      [wl_vect, optics_tf] = GasHelper.get_total_tf_from_cfits(cfits_arr, wl_vect);
      optics_tf = 1e-4*optics_tf; % fix since the Lens & Filter tfs are in [%]
      if plot_tf
        figure; plot(wl_vect, 0.01*optics_cfits.Lens(wl_vect), 'DisplayName','Lens'); hold on;
        plot(wl_vect, 0.01*optics_cfits.Filter(wl_vect), 'DisplayName','Filter');
        plot(wl_vect, optics_cfits.Detector(wl_vect), 'DisplayName','Detector');
        plot(wl_vect, optics_tf, 'DisplayName','All');
        title('Optics Transfer functions'); xlabel('\lambda[um]');
        legend('-DynamicLegend');
      end
    end
    
    function geometry = getGeometryFromIm(im, distance_cm, detector_area_cm2)
      % Assumes small angles approx. -> valid if object cover all pixel's FOV
      h_fig = GasHelper.plotIm(im,'Select interval in the BB image');
      GasHelper.maximizeFigure(h_fig);% maximize figure for convienience
      %Set the drag bar:
      h_nozzle = imdistline(gca);
      apiObj = iptgetapi(h_nozzle);
      % constrain choosing only within the image:
      fcn = makeConstrainToRectFcn('imline', get(gca,'XLim'),get(gca,'YLim'));
      apiObj.setDragConstraintFcn(fcn);
      input('\nAssure the drag bar is set & Press any key to continue.\n','s');
      dist_cm = input('\nPlease input this distance in [cm].\n','s');
      dist_cm = str2double(dist_cm);
      assert(dist_cm>0);
      %Get the interval in pixels and the cm2pixel scale factor
      dist_pixels = ceil(apiObj.getDistance());%[pixels]
      geometry.pixel_FOV_cm = dist_cm / dist_pixels;
      close(h_fig);
      % Approximated solid angle of a pixel:
      geometry.solid_angle = (geometry.pixel_FOV_cm/distance_cm)^2;
      geometry.geometryFactor = geometry.solid_angle * detector_area_cm2;
    end
    
    function [calibParams] = getSpectralCalibrationFromR(CalibFilePath, wl_vect, hitranParams, calib_experiment_params, optics_tf_wl, is_representative_R, is_tau_outside_integral)
      % calculates calibration parameters using the "full" integral. 
      % Assuming 'DL = R*IT^p' and that:
      % R = g*K*integral(irradiance_photon_flux_BB*tau_BB*filter*Lens*Detector*d_lambda).
      % Where: 'g' is the geometry.
      % Here, K acts as unit conversion factor, as well as gain of
      % each pixel, therefore it's a "half-NUC".
      if nargin < 7
        error('invalid #input args!');
      end
      if isempty(CalibFilePath)
        % Get calibration experimental data: {R, experimentCasesArr}:
        [CalibFileName, CalibFilePath] = uigetfile({'*.mat','MATLAB data'},'Select Calibration matrix',CalibFilePath);
        if isempty(CalibFileName)
          error('invalid folder path, or claibration matrix there...');
        end
        load([CalibFilePath, CalibFileName]); % OUTPUT: 4th dim matrix 'R', and 'experimentCasesArr'.
      else
        load(CalibFilePath); % OUTPUT: 4th dim matrix 'R', and 'experimentCasesArr'.
      end
      % Get transmission spectrum:
      if ~isfield(hitranParams, 'tau_spectra_wn')
        [wn_vect, ~, tau_spectra_wn] = MyGetSpectrumGPU(true, 'tau', hitranParams, calib_experiment_params.atmParams);
      else
        wn_vect = flipud(GasHelper.wl2wn(wl_vect));
        tau_spectra_wn = hitranParams.tau_spectra_wn;
      end
      if is_tau_outside_integral
        tau_spectra_wn = ones(size(wn_vect));
      end
      % convert spectra to wavelengths:
      [~, tau_spectra_wl] = GasHelper.spectralFunc_wn2wl(wn_vect, tau_spectra_wn, true);
      % convert optics to wn if needed:
      [~, optics_tf_wn] = GasHelper.spectralFunc_wl2wn(wl_vect, optics_tf_wl, true);
      % estimate R from DL = R*IT^P model
      T = GasHelper.C2K(experimentCasesArr.temps); % T[K] used in the calibration
      % calculate the integrals for given T:
      integral_vect_wl = zeros(numel(T),1);
      integral_vect_wn = integral_vect_wl; % just for check
      % Calculate geometry factor (small angles approx.):
      if ~isfield(calib_experiment_params, 'geometryStruct')
        detector_pitch_um = calib_experiment_params.detector_pitch_um; % [um]
        detector_area_cm2 = (detector_pitch_um*1e-4)^2; % Detector's area in [cm^2], to match HITRAN units
        geometryStruct = GasHelper.getGeometryFromIm( squeeze(R(end,1,:,:)),...
                  calib_experiment_params.atmParams.OPL, detector_area_cm2 );
      else
        geometryStruct = calib_experiment_params.geometryStruct;
      end
      geometry = geometryStruct.geometryFactor;
      % Get averaged trasmissivity weighted by Planck & optics tf:
      if is_tau_outside_integral
        [tau_avg] = GasHelper.getWeightedTransmittance(hitranParams, optics_tf_wn, calib_experiment_params.atmParams);
      end
      % Calculate integrals: Here geometry factor is within the integral,
      % to reduce numeric errors due to high values (photon flux)
      for indT = 1:numel(T)
        % Planck in [photons/s/cm^2/sr/um]:
        L_BB_photons_wl = 2e14*GasHelper.c./(wl_vect.^4)./(exp(GasHelper.C2*1e6./wl_vect./T(indT)) - 1); % "Calculating BB Radiance V2.pdf", eq.10, only in cm^2 rather than m^2
        % Planck in [photons/s/cm^2/sr/cm^-1]
        L_BB_photons_wn = 2*100*GasHelper.c .* (wn_vect.^2) ./ (exp(100*GasHelper.C2*wn_vect/T(indT)) -1);
        % DEBUG: trapz(wn_vect,L_BB_photons_wn) - trapz(wl_vect,L_BB_photons_wl)
        integral_vect_wl(indT) = trapz(wl_vect, geometry * calib_experiment_params.e_BB .* L_BB_photons_wl .* tau_spectra_wl .* optics_tf_wl);
        integral_vect_wn(indT) = trapz(wn_vect, geometry * calib_experiment_params.e_BB .* L_BB_photons_wn .* tau_spectra_wn .* optics_tf_wn);
      end
      % DEBUG: disp(integral_vect_wl - integral_vect_wn);
      
      % Calculate the calibration parameters:
      if is_representative_R
        progressbar('Temp')
        K = zeros(numel(T),1);
        R_est = nan(size(T));
        for indT = 1:numel(T) % Loop over temperatures
          R_est(indT) = HistogramHelper.getFittedGauss3Peak(squeeze(R(indT,1,:,:)),[],[],[],3); % Get representative R from the R image
          K(indT) = R_est(indT) ./ integral_vect_wl(indT);
          progressbar(indT/numel(T));
        end
        K = mean(K); % TODO: replace with better?
      else % find parameters for each Pixel
        R_est = squeeze(R(:,1,:,:)); % remove filter dimension
        [~, nX, nY] = size(R_est);
        integral_vect_wl = repmat(integral_vect_wl, 1, nX, nY);
        K = R_est ./ integral_vect_wl;
        K = squeeze(mean(K, 1)); % TODO: replace with better?
      end
      % If tau is outside the integral, divide by it here:
      if is_tau_outside_integral
        K = K ./ tau_avg;
      end
      % Output the calibration params:
      calibParams.T_K = T;
      calibParams.wl_vect = wl_vect;
      calibParams.K = K;
      calibParams.optics_tf = optics_tf_wl;
      calibParams.geometryStruct = geometryStruct;
      calibParams.tau_spectra_wn = tau_spectra_wn;
      calibParams.filter_OD = calib_experiment_params.filter_OD;
      calibParams.isPixelwise = ~is_representative_R;
    end
    
    function [calibParams] = getSpectralCalibrationFromSingleR(R, T_K, wl_vect, hitranParams, calib_experiment_params, optics_tf_wl, is_representative_R)
      % Gets K from a single R BB image of temperature T_K.
      % Get transmission spectrum:
      if ~isfield(hitranParams, 'tau_spectra_wn')
        [wn_vect, ~, tau_spectra_wn] = MyGetSpectrumGPU(true, 'tau', hitranParams, calib_experiment_params.atmParams);
      else
        wn_vect = flipud(GasHelper.wl2wn(wl_vect));
        tau_spectra_wn = hitranParams.tau_spectra_wn;
      end
      % convert spectra to wavelengths:
      [~, tau_spectra_wl] = GasHelper.spectralFunc_wn2wl(wn_vect, tau_spectra_wn, true);
      % convert optics to wn if needed:
      [~, optics_tf_wn] = GasHelper.spectralFunc_wl2wn(wl_vect, optics_tf_wl, true);
      % Calculate geometry factor (small angles approx.):
      if ~isfield(calib_experiment_params, 'geometryStruct')
        detector_pitch_um = calib_experiment_params.detector_pitch_um; % [um]
        detector_area_cm2 = (detector_pitch_um*1e-4)^2; % Detector's area in [cm^2], to match HITRAN units
        geometryStruct = GasHelper.getGeometryFromIm(R, ...
          calib_experiment_params.atmParams.OPL, detector_area_cm2 );
      else
        geometryStruct = calib_experiment_params.geometryStruct;
      end
      geometry = geometryStruct.geometryFactor;
      % Calculate integrals: Here geometry factor is within the integral,
      % to reduce numeric errors due to high values (photon flux).
      % Planck in [photons/s/cm^2/sr/um]:
      L_BB_photons_wl = 2e14*GasHelper.c./(wl_vect.^4)./(exp(GasHelper.C2*1e6./wl_vect./T_K) - 1); % "Calculating BB Radiance V2.pdf", eq.10, only in cm^2 rather than m^2
      % Planck in [photons/s/cm^2/sr/cm^-1]
      L_BB_photons_wn = 2*100*GasHelper.c .* (wn_vect.^2) ./ (exp(100*GasHelper.C2*wn_vect/T_K) -1);
      % DEBUG: trapz(wn_vect,L_BB_photons_wn) - trapz(wl_vect,L_BB_photons_wl)
      integral_vect_wl = trapz(wl_vect, geometry * calib_experiment_params.e_BB .* L_BB_photons_wl .* tau_spectra_wl .* optics_tf_wl);
      % DEBUG: integral_vect_wn = trapz(wn_vect, geometry * calib_experiment_params.e_BB .* L_BB_photons_wn .* tau_spectra_wn .* optics_tf_wn);
      % DEBUG: disp(integral_vect_wl - integral_vect_wn);
      
      % Calculate the calibration parameters:
      if is_representative_R
        R_est = HistogramHelper.getFittedGauss3Peak(R,[],[],[],3); % Get representative R from the R image
      else % find parameters for each Pixel
        R_est = R;
      end
      K = R_est ./ integral_vect_wl;
      % Output the calibration params:
      calibParams.T_K = T_K;
      calibParams.wl_vect = wl_vect;
      calibParams.K = K;
      calibParams.optics_tf = optics_tf_wl;
      calibParams.geometryStruct = geometryStruct;
      calibParams.tau_spectra_wn = tau_spectra_wn;
      calibParams.filter_OD = calib_experiment_params.filter_OD;
      calibParams.isPixelwise = ~is_representative_R;
    end
    
    function R = getRfromSpectralCalibration(calibParam, geometry, wn_vect, radiance_photon_flux, tau)
      % Calculates R using the "full" integral.
      % Assuming the 'DL = R*IT^p' and that 'R = geometry*K*int(dl)', where:
      % int(dl) = integral(irradiance_photon_flux*tau*filter*Lens*Detector*dl).
      % INPUTS:
      %     'calibParam' - includes T vector, optics_tf & K
      %     'geometry' - Assuming small angles approx., this factor is out of the
      %     integral and equals solid_angle*detector_area[cm^2].
      %     'wn_vect' - vector of wavenumbers.
      %     'radiance_photon_flux' - spectral radiance photon flux.
      %     'tau' - spectral transmission of the medium.
      % Note: there's a freedom to integrate over wavelengths or
      % wavenumbers. Here we assume optics in wavenumbers as the
      % rest functions are in this units.
      
      [~, optics_tf_wn] = GasHelper.spectralFunc_wl2wn(flipud(GasHelper.wl2wn(wn_vect)), calibParam.optics_tf, true);
      R = geometry * calibParam.K .*...
          trapz(wn_vect, radiance_photon_flux .* tau .* optics_tf_wn );
    end
    
    function tail_val = get_filter_tail_from_OD(filter_tf, OD)
      tail_val = max(filter_tf)/10^OD;
    end
    
    function new_optics_tf_wl = modify_optics_tf_by_OD(wl_vect, optics_cfits, OD)
      % modifies the filter tf assuming same OD for both sides
      orig_filter_tf = optics_cfits.Filter(wl_vect);
      lens_tf = optics_cfits.Lens(wl_vect);
      detector_tf = optics_cfits.Detector(wl_vect);
      tail_value = GasHelper.get_filter_tail_from_OD(orig_filter_tf, OD);
      filter_tf = orig_filter_tf;
      filter_tf(orig_filter_tf<=tail_value) = tail_value;
      new_optics_tf_wl = 1e-4 .* filter_tf .* lens_tf .* detector_tf; % 1e-4 since 2 tf are in [%]
    end
    
    function [xdata, ydata] = get_data_from_figure_of_plot(figurePath)
      h_fig = open(figurePath);
      axesObjs = get(h_fig, 'Children');  %axes handles
      dataObjs = get(axesObjs, 'Children'); %handles to low-level graphics objects in axes
%       objTypes = get(dataObjs, 'Type');  %type of low-level graphics object
      xdata = get(dataObjs, 'XData');  %data from low-level grahics objects
      ydata = get(dataObjs, 'YData');
    end
    
    function [X,Y,hF] = fit_line_by_ransac(xData, yData, iterNum, thDist, thInlrRatio, plotResults)
      % Apply RANSAC for fitting a line from noisy data with outliers:
      pts = [(xData(:))'; (yData(:))'];
      if nargin < 3
        iterNum = 1000;
      end
      if nargin < 4
        thDist = 2;
      end
      if nargin < 5
        thInlrRatio = .5;
      end
      if nargin < 6
        plotResults = false;
      end
      [t,r] = ransac(pts,iterNum,thDist,thInlrRatio);
      k1 = -tan(t);
      b1 = r/cos(t);
      hF = [];
      if plotResults
        hF = figure; plot(pts(1,:), pts(2,:), '.k'); hold on;
        plot(pts(1,:),k1*pts(1,:)+b1,'r');
        %DEBUG: least square fitting
        coef2 = polyfit(pts(1,:),pts(2,:),1);
        k2 = coef2(1);
        b2 = coef2(2);
        plot(pts(1,:),k2*pts(1,:)+b2,'g');
        legend('data points','RANSAC','least-squares fit');
      end
      X = pts(1,:);
      Y = k1*pts(1,:)+b1;
    end
    
    function y = hyper_gaussian(c,x)
      % return the hyper-gaussian model, with parameter vector c. 
      % c(5)==1 is normal gaussian.
      y = c(1) * exp( -(abs(x-c(3))./c(4)).^(2*c(5)) ) + c(2) ;
    end
    
    function [x, resnorm, exitflag, output] = gaussfitvector(x0, xdata, ydata, lb, ub, nVal)
      % Fit data fit to a hyper-gauss function.
      % Source: https://www.mathworks.com/matlabcentral/newsreader/view_thread/167707
      % Model:
      %       ydata = x(1) * exp( -(abs(xdata-x(3))./x(4)).^(2*x(5)) ) + x(2)
      % Where: x === [xLinear, xNonLinear] = [a,b,c,d,n]'.
      % n assumed integer and controls the "squarness" of the function.
      %   n==1 is a pure Gaussian.
      % The problem is splitted to Linear & Non-Linear parts, extending its
      % robustness.
      if nargin < 3
        error('invalid args!');
      end
      if isempty(x0)
        x0 = [5, 50, 1]'; % default initial guess on the non-linear parameters
      end
      if nargin < 5 || isempty(lb) || isempty(ub)
        lb = [0, 0, 1]'; % lower bounds on x
        ub = [500, 500, 9]'; % upper bounds on x
      end
      if nargin == 6 % Force value for n
        lb(end) = nVal;
        ub(end) = nVal;
      end
      xdata = xdata(:);
      ydata = ydata(:);
      
      function yEst = gaussfitvector_linear_part(lam, xdata, ydata)
        % Helper function for splitting the fit problem of:
        %        y = a*exp(-((x-lam(1))./lam(2)).^(2*lam(3)))+b
        %(hyper-gaussian) to its linear & non-linear parts.
        % The vector of linear parameters is: c === [a,b]'
        A = [exp(-(abs(xdata-lam(1))/lam(2)).^(2*lam(3))), ones(numel(xdata), 1)]; % build the A matrix
        c_params = A\ydata; % solve A*c = y for linear parameters c
        yEst = A*c_params; % return the estimated response based on c
      end
      
      F2 = @(x,xdata) gaussfitvector_linear_part(x, xdata, ydata);
      [xNonLinear, resnorm, ~, exitflag, output] = lsqcurvefit(F2, x0, xdata, ydata, lb, ub);
      assert(norm(imag(xNonLinear),Inf)<1e-13,'Complex results!!');
      xNonLinear = real(xNonLinear);
      % Get the linear part:
      A = [ exp(-(abs(xdata-xNonLinear(1))/xNonLinear(2)).^(2*xNonLinear(3))), ones(numel(xdata),1) ];
      xLinear = A \ ydata;
      x = [xLinear; xNonLinear];
    end
    
    function [cIm, thetaIm] = generate_2d_models_from_hyper_gaussian_fits(im, symAxis, params)
      %% Fit model at each row to  a*exp(-((x-c)./d).^(2*n))+b (hyper-gaussian)
      Tamb = params.Tamb;
      Catm = params.Camb;
      Cjet = params.Cjet;
      thetaIm = Tamb*ones(size(im));
      for iRow = 1:size(im,1)
        x = (1:find(~isnan(im(iRow,symAxis:end)), 1, 'last'))';
        y =  im(iRow, symAxis-1+x)';
        %{
        hg = fittype('a*exp(-((x-c)./d).^(2*n))+Tamb+b',...
          'problem',{'n','Tamb'},...
          'independent','x');
        n = 2; % n==1 - gaussian. n>1 ===> more top-hat
        [gauss_fit, gof] = fit(x, y, hg, 'problem',{n,Tamb});
        figure; plot(gauss_fit, x, y); hold on;
        %}
        min_resnorm = Inf;
        for nVal = 1:21 % loop over integer n values AKA squarness of the hyper-gaussian:
          [c_vec, resnorm, ~, ~] = GasHelper.gaussfitvector([], x, y, [],[], nVal);
          if resnorm < min_resnorm
            min_resnorm = resnorm;
            opt_c = c_vec;
          end
          plot(x, GasHelper.hyper_gaussian(c_vec, x) );
        end
%         title(['n* = ',num2str(opt_c(end))]);
        y = GasHelper.hyper_gaussian(opt_c,x);
        % flatten some more:
        [yMax,ind] = nanmax(y);
        y(1:ind) = yMax;
        thetaIm(iRow, symAxis:symAxis+numel(y)-1) = y;
        thetaIm(iRow, symAxis-1-numel(y)+1:symAxis-1) = flipud(y);
      end
      cIm = (thetaIm - Tamb)/(nanmax(thetaIm(:))-Tamb); % normalized C* == T*
      cIm = cIm*(Cjet - Catm) + Catm; % scale back
    end
    
    function [solT] = Thermocouple_loses( props, plotGraphs )
      %% Based on Incropera Q7.78 C and comply with its nameology.
      %% All values given in [MKS]
      % INPUT: 'props' - struct with the fields: 'TC_epsi' - T.C. emissivity.
      %               'TC_D' - T.C. diameter, 'gas_viscosity', 'k_g', 'Pr',
      %               'Tamb' - ambient temperature,
      %               'Tinf' - true temperature of the gas. leave [] if
      %               measured T is known.
      %               'Tm' - measured temperature. leave [] if Tinf known.
      %               'V' - Velocity of the flow.
      % OUTPUT: 'solT' - Estimated measured Tm[K], or Tinf[K].
      sigma = 5.67e-8;
      if nargin < 1
        plotGraphs = false;
        % T.C. properties:
        epsi = 0.5;
        D = 1e-3; % [m]
        % Flow properties:
        Tinf = 1000;
        Tm = [];
        viscosity = 50e-6;
        k_g = 0.05;
        Pr = 0.69;
        % Ambient properties:
        Tc = 400;
      else
        epsi =      props.TC_epsi;
        D =         props.TC_D;
        viscosity = props.gas_viscosity;
        k_g =       props.k_g;
        Pr =        props.Pr;
        Tc =        props.Tamb;
        Tinf =      props.Tinf;
        Tm =        props.Tm;
      end
      % Solver parameters:
      search_range_Tmin = Tc;
      search_range_Tmax = Tinf;
      syms T
      %% Plot graphs of trends:
      if plotGraphs
        % epsilon influence:
        V = 1:0.2:25;
        Re = V*D/viscosity; % #Reynolds
        Nu = 2 + (0.4*Re.^0.5 + 0.06*Re.^(2/3))*Pr^0.4 ; % Whitaker correlation for external flow
        h_avg = k_g*Nu/D; % Averaged convection heat coeff.
        % Solve the heat balance:
        solT = zeros(numel(V),1);
        for i=1:numel(V)
          solT(i) = vpasolve(h_avg(i)*(Tinf-T) == epsi*sigma*(T^4-Tc^4), T, [search_range_Tmin search_range_Tmax]);
        end
        figure; plot(V,solT);
        xlabel('V [m/s]'); ylabel('T_s_s[K]');
        title(['Thermocouple''s s.s. temperature Vs V for \epsilon=',num2str(epsi)]);
        % epsilon influence:
        V = 5;
        epsi = 0.1:0.01:1;
        solT = zeros(numel(epsi),1);
        Re = V*D/viscosity;
        Nu = 2 + (0.4*Re.^0.5 + 0.06*Re.^(2/3))*Pr^0.4 ;
        h_avg = k_g*Nu/D;
        for i=1:numel(epsi)
          solT(i) = vpasolve(h_avg*(Tinf-T) == epsi(i)*sigma*(T^4-Tc^4), T, [search_range_Tmin search_range_Tmax]);
        end
        figure; plot(epsi,solT);
        xlabel('\epsilon of thermocouple'); ylabel('T_s_s[K]');
        title(['Thermocouple''s s.s. temperature Vs \epsilon of the thermocouple for V=',num2str(V),'[m/s]']);
      end
      %% Now for the actual case:
      if nargin < 1
        V = 30; %[m/s]
        epsi = 0.5;
      else
        V = props.V;
        epsi = props.TC_epsi;
      end
      Re = V*D/viscosity;
      Nu = 2 + (0.4*Re.^0.5 + 0.06*Re.^(2/3))*Pr^0.4 ;
      h_avg = k_g*Nu/D;
      if isempty(Tm) % calc T
        solT = vpasolve(h_avg*(Tinf-T) == epsi*sigma*(T^4-Tc^4), T, [search_range_Tmin search_range_Tmax]);
        solT = double(solT);
      else % calc Tinf
        Tm = props.Tm;
        solT = epsi*sigma/h_avg*(Tm^4-Tc^4) + Tm ;
      end
    end
    
    function [I] = forward_onion_peeling_old(emission_vect, tau_vect)
      % Performs forward Onion-Peeling on axissymmetric jet (on a single row).
      % The emission and transmission profiles in the jet are assumed
      % f(r). They are approximated as rings of constant emission &
      % transmission. 'yi' are the samples of these rings, where 'y' is the
      % projection's position axis.
      % Input 'tau_vect' == 1 for approximation of only emission (no
      % self-absorption).
      emission_vect = emission_vect(:); tau_vect = tau_vect(:);
      R0 = numel(emission_vect);
      % flip since we go from the outside inward here:
      emission_vect = flipud(emission_vect);
      tau_vect = flipud(tau_vect);
      dy = 1; % we work in [pixels]
      I = zeros(1, R0);
      for i = 1 : R0 % currect projection' location yi
        yi = R0 + dy/2 - i; % sample position in the ring
        for j = 1 : i % loop over rings that affect location yi
          % calculate the transmission factors:
          tau_part1 = 1;
          for m = 1 : j-1
            tau_part1 = tau_part1*tau_vect(m);
          end
          tau_part2 = 1;
          for m = j+1 : i
            tau_part2 = tau_part2*tau_vect(m);
          end
          % calculate the Jacobian factors:
          if j==i
            Aij = 0;
          else
            Aij = sqrt((R0-j)^2-yi^2);
          end
          Aijm1 = sqrt(((R0-(j-1))^2-yi^2));
          % Add the current ring's donation:
          I(i) = I(i) + ...
                 ( Aijm1 - Aij ) * emission_vect(j) * ...
                 tau_part1 * (1 + tau_vect(j)*tau_part2^2);
        end
      end
      I = fliplr(I); % flip since we calculated outside-inward
    end
    
    function [I] = forward_onion_peeling(emission_vect, tau_vect, A)
      % Performs forward Onion-Peeling on axissymmetric jet (on a single row).
      % The emission and transmission profiles in the jet are assumed
      % f(r). They are approximated as rings of constant emission &
      % transmission. 'yi' are the samples of these rings, where 'y' is the
      % projection's position axis.
      % Input 'tau_vect' == 1 for approximation of only emission (no
      % self-absorption).
      % 'A' is the matrix of the geometric terms, where:
      %         A(i,j) = 0                      ,  if  j > i
      %                = alpha(i,j-1)-alpha(i,j),  else
      emission_vect = emission_vect(:); tau_vect = tau_vect(:);
      R0 = numel(emission_vect);
      % BUG fix 1/10/17: added changing 'nan' to 0 for emission and 1 for tau:
      emission_vect(isnan(emission_vect)) = 0;
      tau_vect(isnan(tau_vect)) = 1;
      % flip since we go from the outside inward here:
      emission_vect = flipud(emission_vect);
      tau_vect = flipud(tau_vect);
      I = zeros(1, R0);
      for i = 1 : R0 % currect projection' location yi
        for j = 1 : i % loop over rings that affect location yi
          % calculate the transmission factors:
          tau_part1 = 1;
          for m = 1 : j-1
            tau_part1 = tau_part1*tau_vect(m);
          end
          tau_part2 = 1;
          for m = j+1 : i
            tau_part2 = tau_part2*tau_vect(m);
          end
          % Add the current ring's donation:
          I(i) = I(i) + ...
                 A(i,j) * emission_vect(j) * ...
                 tau_part1 * (1 + tau_vect(j)*tau_part2^2);
        end
      end
      I = fliplr(I); % flip since we calculated outside-inward
    end
    
    
    function [I] = forward_onion_peeling_no_self_absorp(emission_vect, A)
      %INPUT: emission_vect - emission term vector.
      %       A - matrix of geometric terms for that row
      %OUTPUT: I - the vector of projection, where:
      %        I(i) = 2 * sum_from_j=1:i ( (a(i,j-1)-a(i,j))*emission(j) )
      if ~all(isnan(emission_vect))
        emission_vect(isnan(emission_vect)) = 0;
        I = 2*flipud( A * flipud(emission_vect(:)) ); %flip since we calculated outside-inward
      else
        I = nan(size(emission_vect));
      end
    end
    
    function [A] = calculate_geometric_matrix(R0)
      % Calculates the geometric terms matrix A for the forward-onion transform,
      % where:  A(i,j) = 0                      ,  if  j > i
      %                = alpha(i,j-1)-alpha(i,j),  else
      dy = 1; % [pixel]
      A = zeros(R0,R0);
      alpha = @(ii,jj) (ii~=jj)*sqrt( (R0-jj)^2 - (R0+dy/2-ii)^2 );%alpha(i,j)
      for ii = 1 : R0
        for jj = 1 : ii-1
            A(ii,jj) = alpha(ii,jj-1) - alpha(ii,jj);
        end
      end
    end
    
    
    function sigma = get_sigma_of_gaussian(x2,mu,Ta,T0, T_x2)
      % gets 'sigma' from:  T(x) = (T0-Ta)exp(-0.5*((x-mu)/sigma)^2) + Ta,
      % based on a point x2 and value T(x2) such that: x2 > mu && Ta < T(x2) < T0.
%       sigma = abs(x2-mu)/sqrt(-2*log((T_x2-Ta)/(T0-Ta)));
        sigma = abs(x2-mu)/sqrt(-2*log(0.5));
    end
    
    function sigma = get_sigma_of_gaussian_from_HWHM(HWHM)
      % @ HWHM: (T0+Tinf)/2 = (T0-Tinf)*exp(-0.5*(HWHM/sigma)^2) + Tinf
      % ===>   0.5 = exp(-0.5(HWHM/sigma)^2) ===> HWHM = sqrt(2*ln2)*sigma
      sigma = HWHM / sqrt(2*log(2));
    end
    
    function y = gen_gaussian(x, a, mu, sigma, b)
       y = a*exp(-0.5*((x-mu)/sigma).^2) + b;
    end
    
    function [X, Y] = get_edge_line_from_image(im, ransac_th, ransac_inliers_ratio)
      % Assumes that outside of the jet, im == nan.
      % NOTE: this function may be used for both pontential core, or the
      % boundary of the jet (when the input image includes the background
      % and that is the flat region).
      if( ~exist('ransac_th', 'var') )
        ransac_th = 2;
      end
      if( ~exist('ransac_inliers_ratio', 'var') )
        ransac_inliers_ratio = .1;
      end
      nBins = 10;
      centerline = im(:,1);
      im = im(~isnan(centerline), :);
      centerline = centerline(~isnan(centerline));
      %{
         GasHelper.plotIm(R_est_Abel_Inverted(end,:)); hold on; indPlot = numel(centerline);
         while 1
          indPlot = indPlot - 20;
          if indPlot < 1
             break
           end
            plot(R_est_Abel_Inverted(indPlot,:));
         end
      %}
      % hwhm = zeros(size(R_est_Abel_Inverted,1),1);
      potential_core_edge = zeros(size(im,1),1);
      for indRow = 1 : size(im,1)
        %   hwhm(indRow) = find(abs(R_est_Abel_Inverted(indRow,:) - centerline(indRow)/2) < max(abs(diff(R_est_Abel_Inverted(indRow,:)))), 1, 'first');
        %   difff = diff(diff(R_est_Abel_Inverted(indRow,:))./R_est_Abel_Inverted(indRow,2:end));
        %   [~,edges] = histcounts(abs(difff), 10);
        %   th = edges(2);
        %   th = -0.01;
        %   potential_core_edge(indRow) = find(difff <= th , 1, 'first');
        %   dd = diff(diff(R_est_Abel_Inverted(indRow,:)));
        %   potential_core_edge(indRow) = find(dd==nanmin(dd), 1, 'first');
        [N,edges] = histcounts(im(indRow,:), nBins);
        maxN_ind = find(N==max(N),1,'first');
        % BUG FIX below: was 'edges(N==max(N))'. Rational:
        % '(maxN_ind<(nBins/2))' determines if the constant part is in the
        % beginning of the line (potential core) or in the end
        % (background).
        th = edges(maxN_ind + (maxN_ind<(nBins/2)) ); % 
        if ~th
          th = edges(2);
        end
        if th(1)<0
          th = edges(edges > 0);
        end
        th = th(find(th>0,1,'first')); % for case of multiple values
        potential_core_edge(indRow) = find(im(indRow,:) <= th , 1, 'first');
      end
      hR = [];
      for iRANSAC = 1 : 3
        close(hR);
        [X,Y,~] = GasHelper.fit_line_by_ransac(1:size(im,1), potential_core_edge, 1000, ransac_th, ransac_inliers_ratio, false);
        hR = GasHelper.plotIm(im); hold on;
        scatter(potential_core_edge, 1:size(im,1),'*r');
        plot(Y, X,'b');
        
        disp(['RANSAC parameters: TH = ',num2str(ransac_th),', inliersRatio = ',num2str(ransac_inliers_ratio),'.']);
%         disp('If fit is OK, enter ''0'' twice. Otherwise, enter distance TH in [pixels], followed by inliersRatio:');
        tmp = input('If fit is good, hit Enter. Otherwise, enter TH in [pixels]:');
        if isempty(tmp)
          break;
        else
          ransac_th = tmp;
          tmp = input('If inliersRatio is good, hit Enter. Otherwise, enter inliersRatio:');
          if ~isempty(tmp)
            ransac_inliers_ratio = tmp;
          end
        end
      end
    end
    
    function [X, Y, hfig] = get_jet_boundary_from_half_image(im, ransac_th, ransac_inliers_ratio, nBins)
      if ( ~exist('ransac_th', 'var') )
        ransac_th = 2;
      end
      if ( ~exist('ransac_inliers_ratio', 'var') )
        ransac_inliers_ratio = .1;
      end
      if ( ~exist('nBins', 'var') )
        nBins = 10; % set th to 10%
      end
      potential_core_edge = zeros(size(im,1),1);
      for indRow = 1 : size(im,1)
        [N,edges] = histcounts(im(indRow,:), nBins);
        maxN_ind = find(N==max(N),1,'first');
        % BUG FIX below: was 'edges(N==max(N))'. Rational:
        % '(maxN_ind<(nBins/2))' determines if the constant part is in the
        % beginning of the line (potential core) or in the end
        % (background).
        th = edges(maxN_ind + (maxN_ind<(nBins/2)) ); %
        %         if ~th
        %           th = edges(2);
        %         end
        %         if th(1)<0
        %           th = edges(edges > 0);
        %         end
        %         th = th(find(th>0,1,'first')); % for case of multiple values
        potential_core_edge(indRow) = find(im(indRow,:) <= th , 1, 'first');
      end
      hR = [];
      for iRANSAC = 1 : 3
        close(hR);
        [X,Y,~] = GasHelper.fit_line_by_ransac(1:size(im,1), potential_core_edge, 1000, ransac_th, ransac_inliers_ratio, false);
        hR = GasHelper.plotIm(im); hold on;
        scatter(potential_core_edge, 1:size(im,1),'*r');
        plot(Y, X,'b');
        disp(['RANSAC parameters: TH = ',num2str(ransac_th),', inliersRatio = ',num2str(ransac_inliers_ratio),'.']);
        tmp = input('If fit is good, hit Enter. Otherwise, enter TH in [pixels]:');
        if isempty(tmp)
          break;
        else
          ransac_th = tmp;
          tmp = input('If inliersRatio is good, hit Enter. Otherwise, enter inliersRatio:');
          if ~isempty(tmp)
            ransac_inliers_ratio = tmp;
          end
        end
      end
      if nargout > 2 && exist('hR', 'var')
        hfig = hR;
      end
    end
    
    function [X, Y, potential_core_points] = get_potential_core_from_half_image(im, ransac_th, ransac_inliers_ratio, nBins, FIT_TO_LINE_METHOD)
      if ( ~exist('ransac_th', 'var') || isempty(ransac_th) )
        ransac_th = 2;
      end
      if ( ~exist('ransac_inliers_ratio', 'var') || isempty(ransac_inliers_ratio) )
        ransac_inliers_ratio = .1;
      end
      if ( ~exist('nBins', 'var') || isempty(nBins) )
        nBins = 10; % set th to 10%
      end
      if ( ~exist('FIT_TO_LINE_METHOD', 'var') || isempty(FIT_TO_LINE_METHOD) )
        FIT_TO_LINE_METHOD = 'WLS'; % set weighted least-squares
      end
      potential_core_edge = zeros(size(im,1),1);
      for indRow = 1 : size(im,1)
        row = im(indRow,:);
        [N,edges] = histcounts(row, nBins);
        maxN_ind = find(N==max(N),1,'first');
        % Check if the flat region is indeed belongs to potential core.
        % This might not be the case e.g. far away upstream.
        % In this case, this is an outlier and we'll use the maxN_ind to
        % restrict the search to [1,maxN_ind]. We do one iteration of
        % recalculation. If the flat region is still in the end of the
        % interval, we consider this row as an outlier by assigning NaN.
        is_flat_region_near_centerline = maxN_ind>(nBins/2);
        if is_flat_region_near_centerline
          th = edges(maxN_ind);
        else
          th = edges(maxN_ind+1);
          ind = find(row <= th , 1, 'first');
          row = im(indRow,1:ind);
          [N,edges] = histcounts(row, nBins); % TODO: binning should be changed now ?
          maxN_ind = find(N==max(N),1,'first');
          th = edges(maxN_ind); % FIXED from: th = edges(maxN_ind+1);
          is_flat_region_near_centerline = maxN_ind>(nBins/2);
        end
        %         if ~th
        %           th = edges(2);
        %         end
        %         if th(1)<0
        %           th = edges(edges > 0);
        %         end
        %         th = th(find(th>0,1,'first')); % for case of multiple values
        if is_flat_region_near_centerline
          potential_core_edge(indRow) = find(im(indRow,:) <= th , 1, 'first');
        else % Possible Outlier
          potential_core_edge(indRow) = NaN;
        end
      end
      if isequal(FIT_TO_LINE_METHOD,'OTSU')
        N_LEVELS = 2; % 6/12/17 changed from 3
        imm = otsu(im,N_LEVELS);
        potential_core_edge = imm == N_LEVELS;
        potential_core_edge2 = sum(potential_core_edge, 2);
        estimated_otsu_potential_core2 = find(~potential_core_edge2,1,'last');
        potential_core_edge = potential_core_edge2;
        estimated_otsu_potential_core = estimated_otsu_potential_core2;
        %{
        N_LEVELS = 3; % 6/12/17 changed from 3
        imm = otsu(im,N_LEVELS);
        potential_core_edge = imm == N_LEVELS;
        potential_core_edge3 = sum(potential_core_edge, 2);
        estimated_otsu_potential_core3 = find(~potential_core_edge3,1,'last');
        estimated_otsu_potential_core = round((estimated_otsu_potential_core2 + estimated_otsu_potential_core3)/2 );
        potential_core_edge = (potential_core_edge2 + potential_core_edge3 )/2; % BUG - see the graph
        potential_core_edge(end) = round( potential_core_edge2(end) + potential_core_edge3(end) )/ 2; % I use below only last cell
        %}
      end
      switch FIT_TO_LINE_METHOD
        case 'RANSAC'
          hR = [];
          for iRANSAC = 1 : 3
            close(hR);
            [X,Y,~] = GasHelper.fit_line_by_ransac(1:size(im,1), potential_core_edge, 1000, ransac_th, ransac_inliers_ratio, false);
            hR = GasHelper.plotIm(im); hold on;
            scatter(potential_core_edge, 1:size(im,1),'*r');
            plot(Y, X,'b');
            disp(['RANSAC parameters: TH = ',num2str(ransac_th),', inliersRatio = ',num2str(ransac_inliers_ratio),'.']);
            tmp = input('If fit is good, hit Enter. Otherwise, enter TH in [pixels]:');
            if isempty(tmp)
              break;
            else
              ransac_th = tmp;
              tmp = input('If inliersRatio is good, hit Enter. Otherwise, enter inliersRatio:');
              if ~isempty(tmp)
                ransac_inliers_ratio = tmp;
              end
            end
          end
        case 'OTSU' % TODO: check
          X = (1:size(im,1))';
          slope = (potential_core_edge(end)-0)/ (X(end)-estimated_otsu_potential_core);
          Y = slope.*X - estimated_otsu_potential_core*tan(slope);
        otherwise % 'WLS' (Weighted least-squares):
          X = (1:size(im,1))';
          potential_core_edge(isnan(potential_core_edge)) = 0;
          weights = X;
          weights(~potential_core_edge) = 0; % marked outliers
          outliers = excludedata(X,potential_core_edge,'indices',~potential_core_edge);
          ft = fittype('a*x+b');
          StartPoint = [0 1/6]; % Guess of b=0 & a=1/6
          % DEBUG:
          %{
          [fit1,gof,fitinfo] = fit(X,potential_core_edge,ft,'StartPoint',StartPoint);
          residuals = fitinfo.residuals;
          I = abs( residuals) > 1.5 * std( residuals );
          outliers = excludedata(X,potential_core_edge,'indices',I);
          fit2 = fit(X,potential_core_edge,ft,'StartPoint',StartPoint,...
                    'Exclude',outliers, 'Weights',X);
          fit3 = fit(X,potential_core_edge,ft,'StartPoint',StartPoint,'Robust','on', 'Weights',X);
          figure;
          plot(fit1,'r-',X,potential_core_edge,'k.',outliers,'m*')
          hold on
          plot(fit2,'c--')
          plot(fit3,'b:')
          legend( 'Data', 'Data excluded from second fit', 'Original fit',...
            'Fit with points excluded', 'Robust fit' )
          hold off
          %}
          fit3 = fit(X,potential_core_edge,ft,'StartPoint',StartPoint,'Robust','on','Exclude',outliers,'Weights',weights);
          Y = fit3(X);
          GasHelper.plotIm(im); hold on;
          scatter(potential_core_edge, 1:size(im,1),'*r');
          plot(Y, X,'b');
      end
      potential_core_points = potential_core_edge;
    end
    
    
    function [nx] = normalize_signal(x, method)
      % Either {'minmax'} or 'statistical' normalization
      if nargin < 2
        method = 'minmax';
      end
      x = x(:);
      switch method
        case 'statistic'
          nx = (x - nanmean(x)) / nanstd(x);
        otherwise % 'minmax'
          nx = (x - nanmin(x)) / (nanmax(x) - nanmin(x));
      end
    end
    
    function [jetBWmask,hfig] = get_mask_of_jet(im,symAxis, use_convergence_to_strip)
      % 'use_convergence_to_strip': if true, approach like in calculating setling time of a step
      % response in Control Theory. We define convegence of the signal to 'th'
      % negiberhood around the final value (the background radiation in
      % the boundaries of the image).
      if nargin < 3
        use_convergence_to_strip = false;
      end
      ransac_th = 10;
      ransac_inliers_ratio = 0.1;
      if use_convergence_to_strip
        halfIm = im(:,symAxis:end);
        halfIm = imgaussfilt(halfIm, 5);
        jetBWmask = false(size(im));
        radiation_range_vect = halfIm(:,1) - halfIm(:,end);
        th_vect = halfIm(:,end) + 0.07*radiation_range_vect;
        th_half_im = repmat(th_vect, [1, size(halfIm,2)]);
        jetBWmask(:, symAxis:end) = halfIm > th_half_im;
        half_jetBWmask = jetBWmask(:, symAxis:end);
        edge_curve = zeros(size(half_jetBWmask,1),1);
        for indRow = 1 :size(half_jetBWmask,1)
          edge_curve(indRow) = find(half_jetBWmask(indRow,:) , 1, 'last');
        end
        hR = [];
        for iRANSAC = 1 : 3
          close(hR);
          [Xb,Yb,~] = GasHelper.fit_line_by_ransac(1:size(half_jetBWmask,1), edge_curve, 1000, ransac_th, ransac_inliers_ratio, false);
          hR = GasHelper.plotIm(half_jetBWmask); hold on;
          scatter(edge_curve, 1:size(half_jetBWmask,1),'*r');
          plot(Yb, Xb,'b');
          disp(['RANSAC parameters: TH = ',num2str(ransac_th),', inliersRatio = ',num2str(ransac_inliers_ratio),'.']);
          tmp = input('If fit is good, hit Enter. Otherwise, enter TH in [pixels]:');
          if isempty(tmp)
            break;
          else
            ransac_th = tmp;
            tmp = input('If inliersRatio is good, hit Enter. Otherwise, enter inliersRatio:');
            if ~isempty(tmp)
              ransac_inliers_ratio = tmp;
            end
          end
        end
        jetBWmask = zeros(size(im));
        for iRow = 1 : size(half_jetBWmask,1)
          for iCol = 1 : size(half_jetBWmask,2)
            half_jetBWmask(iRow,iCol) = iCol <= Yb(iRow);
            jetBWmask(iRow, symAxis+iCol-1) = iCol <= Yb(iRow);
%             jetBWmask(iRow, symAxis-iCol) = iCol <= Yb(iRow); %Possible bug, omitted 18/9/17
          end
        end
      else
        % Other version:
        nBins = 10;
        [Xb, Yb, hR] = GasHelper.get_jet_boundary_from_half_image(im(:,symAxis:end), ransac_th, ransac_inliers_ratio, nBins);
%         [Xb, Yb] = GasHelper.get_edge_line_from_image(im(:,symAxis:end)); % Swapped from 'R_est', works better on non-LPF-filtered image!!
        boundary_slope = (Yb(1)-Yb(end))/(Xb(1)-Xb(end));
        assert(boundary_slope < 0, 'Jet Boundary estimated wrong')
        %       jet_angle_d = atand(abs(boundary_slope));
        %       virtual_source_pixels = Yb(1) / abs(boundary_slope);
        %       virtual_source_D = virtual_source_pixels / D_nozzle_pixels;
        halfJetBW = zeros(numel(Xb), size(im,2)-symAxis+1);
        jetBWmask = zeros(size(im));
        for iRow = 1 : size(halfJetBW,1)
          for iCol = 1 : size(halfJetBW,2)
            halfJetBW(iRow,iCol) = iCol <= Yb(iRow);
            jetBWmask(iRow, symAxis+iCol-1) = iCol <= Yb(iRow);
%             jetBWmask(iRow, symAxis-iCol) = iCol <= Yb(iRow); %Possible bug, omitted 18/9/17
          end
        end
      end
      if nargout > 1 && exist('hR', 'var')
        hfig = hR;
      end
    end
    
    
    function [edge_line] = get_jet_boundary_line(half_jet)
      use_convergence_to_strip = true;
      edge_line = zeros(size(half_jet,1), 1);
      for iRow = 1 : size(half_jet,1)
%         th = max(half_jet(iRow,1)/15, min(half_jet(iRow,:)));
        th = (max(half_jet(iRow,:)) - min(half_jet(iRow,:)))/20 + min(half_jet(iRow,:));
        edge_line(iRow) = find(half_jet(iRow,:) < th, 1, 'First');
      end
      figure; plot(edge_line); hold on;
      if use_convergence_to_strip
        halfIm = imgaussfilt(half_jet, 5);
%         radiation_range_vect = halfIm(:,1) - halfIm(:,end);
%         th_vect = halfIm(:,end) + 0.05*radiation_range_vect;
        % bug fix of above
        radiation_range_vect =  nanmax(halfIm,[],2) - nanmin(halfIm,[],2);
        th_vect = nanmin(halfIm,[],2) + 0.05*radiation_range_vect;
        for indRow = 1 :size(half_jet,1)
          edge_line(indRow) = find(half_jet(indRow,:) > th_vect(indRow) , 1, 'last');
        end
      end
      plot(edge_line);
    end
    

    function [T_im, C_im] = gen_T_C_images_from_model(jet_model, atm_params, N_ROWS, edge_line)
      % Generates Temperature & Concentraion images from the simple jet model, 
      % where T & C are assumed to be of the same family of functions.
      %
      % To get output dimensions of the original half-jet image, simply
      % supply the function with 'edge_line' = ones(N_COLS,1).
      is_constant_pc = false;
%       if isfield(jet_model,'is_constant_pc')
%         is_constant_pc = jet_model.is_constant_pc;
%       end
      MAGIC_NUM = 1;%0.98; % parameter in [0,{1}] that sets value on the potential core edge to be MAGIC_NUM*value@centerline
      MAGIC_NUM_CL = 1;%0.98; % parameter [0,{1}] that sets value on the potential core decay along centerline
      Tnozzle = jet_model.T0; % Temperature @ nozzle [K]
      Cnozzle = jet_model.C0; % concentration @ nozzle [1]
      X_PC = jet_model.X_PC; % Potential Core height [pixels]
      Y_PC = jet_model.Y_PC; % Potential Core width @ nozzle [pixels]
      r_0_5_nozzle = jet_model.r_0_5_nozzle; % Half-width @ nozzle [pixels]
      r_0_5_up = jet_model.r_0_5_up; % Half-width @ upper row of image [pixels]
      b = jet_model.b; % Parameter 'b' [pixels] of the centerline model:
      Ta = atm_params.T ; % atmosphere temperature [K]
      Ca = atm_params.co2_concentration; % atmosphere CO2 concentration [1]
      % Calculate centerline profiles & Potential Core & jet's edge lines:
      x = 1 : N_ROWS;
      r_0_5_line = linspace(r_0_5_up, r_0_5_nozzle, N_ROWS); % half-width line
      % if jet's boundary isn't given, calculate it from the two points:
      if nargin < 4 || isempty(edge_line)
        Y_up = jet_model.Y_up;% Jet width @ the upper row of the image [pixels]
        Y_nozzle = jet_model.Y_nozzle; % Jet width @ the bottom row of the image [pixels]
        edge_line = linspace(Y_up, Y_nozzle, N_ROWS); % jet edge line
      end
      mu_vect = ones(size(x)); % Potential core line
      T_cl = Tnozzle*ones(size(x)); % Centerline temperature [K]
      C_cl = Cnozzle*ones(size(x));% Centerline CO2 concentration [1]
      if X_PC < 1 % if the potential core continues beyond the image
        PC_slope = Y_PC/(N_ROWS-X_PC); % Potential core slope
        Y_PC_up = Y_PC - PC_slope*N_ROWS;
        mu_vect = ceil(linspace(Y_PC_up, Y_PC, N_ROWS)); % so that in [pixels]
      else
        mu_vect(x >= X_PC) = ceil(linspace(1, Y_PC, N_ROWS-X_PC+1)); % so that in [pixels]
        x0 = N_ROWS - X_PC;
        x2 = x0+1 : N_ROWS;
        T_cl(X_PC:end) = wrev(linspace(Tnozzle, MAGIC_NUM_CL*Tnozzle, N_ROWS-X_PC+1)); % inside the potential core, flip because of different coordinates
        C_cl(X_PC:end) = wrev(linspace(Cnozzle, MAGIC_NUM_CL*Cnozzle, N_ROWS-X_PC+1)); % inside the potential core, flip because of different coordinates
        T_cl(1:X_PC) = wrev((MAGIC_NUM_CL*Tnozzle-Ta)*(x0-b)./(x2 - b) + Ta); % flip because of different coordinates
        C_cl(1:X_PC) = wrev((MAGIC_NUM_CL*Cnozzle-Ca)*(x0-b)./(x2 - b) + Ca); % flip because of different coordinates
      end
      mu_vect = max(1, mu_vect); % enforce minimal width of 1 pixel
      
      T_im = zeros(N_ROWS, max(ceil(edge_line)));
      C_im = T_im;
      
      for iRow = 1 : N_ROWS % Loop over rows:
        T0 = T_cl(iRow);
        C0 = C_cl(iRow);
        mu = mu_vect(iRow);
        r = ceil(edge_line(iRow));
        r_0_5 =  ceil(r_0_5_line(iRow));
        % Use the common r_0.5 definition in litrature:
        HWHM = r_0_5 - mu;
        sigma = GasHelper.get_sigma_of_gaussian_from_HWHM(HWHM); 
        if is_constant_pc
          T_vect = T0*ones(1, r);
          T_vect(mu:end) = GasHelper.gen_gaussian(mu:r, (T0-Ta), mu-1, sigma, Ta);
          C_vect = C0*ones(1, r);
          C_vect(mu:end) = GasHelper.gen_gaussian(mu:r, (C0-Ca), mu-1, sigma, Ca);
        else
          T_vect = zeros(1, r);
          C_vect = zeros(1, r);
          T_vect(1:mu) = linspace(T0, MAGIC_NUM*T0, mu);
          C_vect(1:mu) = linspace(C0, MAGIC_NUM*C0, mu);
          T_vect(mu:end) = GasHelper.gen_gaussian(mu:r, (MAGIC_NUM*T0-Ta), mu-1, sigma, Ta);
          C_vect(mu:end) = GasHelper.gen_gaussian(mu:r, (MAGIC_NUM*C0-Ca), mu-1, sigma, Ca);
        end
        T_im(iRow,1:r) = T_vect;
        C_im(iRow,1:r) = C_vect;
      end
    end
    
 
    function [T_im, C_im] = gen_T_C_images_from_model_separated(jet_model, atm_params, N_ROWS, edge_line)
      % Generates Temperature & Concentraion images from the simple jet model, 
      % where T & C are assumed to be of the same family of functions, BUT
      % don't share some geometric parameters. (Y_PC - the potential core
      % width @ orifice. The rational: no C boundary layer there, but might
      % be T boundary layer.)
      % To get output dimensions of the original half-jet image, simply
      % supply the function with 'edge_line' = ones(N_COLS,1).
      C_Y_PC = jet_model.C_Y_PC;% C Potential Core width @ nozzle [pixels]% Potential Core width @ nozzle [pixels]
      is_constant_pc = false;
      MAGIC_NUM = 1;%0.98; % parameter in [0,{1}] that sets value on the potential core edge to be MAGIC_NUM*value@centerline
      MAGIC_NUM_CL = 1;%0.98; % parameter [0,{1}] that sets value on the potential core decay along centerline
%       if isfield(jet_model,'is_constant_pc')
%         is_constant_pc = jet_model.is_constant_pc;
%       end
      Tnozzle = jet_model.T0; % Temperature @ nozzle [K]
      Cnozzle = jet_model.C0; % concentration @ nozzle [1]
      X_PC = jet_model.X_PC; % Potential Core height [pixels]
      Y_PC = jet_model.Y_PC; % Potential Core width @ nozzle [pixels]
      r_0_5_nozzle = jet_model.r_0_5_nozzle; % Half-width @ nozzle [pixels]
      r_0_5_up = jet_model.r_0_5_up; % Half-width @ upper row of image [pixels]
      b = jet_model.b; % Parameter 'b' [pixels] of the centerline model
      Ta = atm_params.T ; % atmosphere temperature [K]
      Ca = atm_params.co2_concentration; % atmosphere CO2 concentration [1]
      % Calculate centerline profiles & Potential Core & jet's edge lines:
      x = 1 : N_ROWS;
      r_0_5_line = linspace(r_0_5_up, r_0_5_nozzle, N_ROWS); % half-width line
      % if jet's boundary isn't given, calculate it from the two points:
      if nargin < 4 || isempty(edge_line)
        Y_up = jet_model.Y_up;% Jet width @ the upper row of the image [pixels]
        Y_nozzle = jet_model.Y_nozzle; % Jet width @ the bottom row of the image [pixels]
        edge_line = linspace(Y_up, Y_nozzle, N_ROWS); % jet edge line
      end
      mu_vect = ones(size(x));   % T Potential core line
      C_mu_vect = ones(size(x)); % C Potential core line
      T_cl = Tnozzle*ones(size(x)); % Centerline temperature [K]
      C_cl = Cnozzle*ones(size(x));% Centerline CO2 concentration [1]
      if X_PC < 1 % if the potential core continues beyond the image
        PC_slope = Y_PC/(N_ROWS-X_PC); % Potential core slope
        Y_PC_up = Y_PC - PC_slope*N_ROWS;
        mu_vect = ceil(linspace(Y_PC_up, Y_PC, N_ROWS)); % so that in [pixels]
        C_PC_slope = C_Y_PC/(N_ROWS-X_PC); % Potential core slope
        C_Y_PC_up = C_Y_PC - C_PC_slope*N_ROWS;
        C_mu_vect = ceil(linspace(C_Y_PC_up, C_Y_PC, N_ROWS)); % so that in [pixels]
      else
        mu_vect(x >= X_PC) = ceil(linspace(1, Y_PC, N_ROWS-X_PC+1)); % so that in [pixels]
        C_mu_vect(x >= X_PC) = ceil(linspace(1, C_Y_PC, N_ROWS-X_PC+1)); % so that in [pixels]
        x0 = N_ROWS - X_PC;
        x2 = x0+1 : N_ROWS;
        T_cl(X_PC:end) = wrev(linspace(Tnozzle, MAGIC_NUM_CL*Tnozzle, N_ROWS-X_PC+1)); % inside the potential core, flip because of different coordinates
        C_cl(X_PC:end) = wrev(linspace(Cnozzle, MAGIC_NUM_CL*Cnozzle, N_ROWS-X_PC+1)); % inside the potential core, flip because of different coordinates
        T_cl(1:X_PC) = wrev((MAGIC_NUM_CL*Tnozzle-Ta)*(x0-b)./(x2 - b) + Ta); % flip because of different coordinates
        C_cl(1:X_PC) = wrev((MAGIC_NUM_CL*Cnozzle-Ca)*(x0-b)./(x2 - b) + Ca); % flip because of different coordinates
      end
      mu_vect = max(1, mu_vect); % enforce minimal width of 1 pixel
      C_mu_vect = max(1, C_mu_vect); % enforce minimal width of 1 pixel
      T_im = zeros(N_ROWS, max(ceil(edge_line)));
      C_im = T_im;
      
      for iRow = 1 : N_ROWS % Loop over rows:
        T0 = T_cl(iRow);
        C0 = C_cl(iRow);
        T_mu = mu_vect(iRow);
        C_mu = C_mu_vect(iRow);
        r = ceil(edge_line(iRow));
        r_0_5 =  ceil(r_0_5_line(iRow));
        % Use the common r_0.5 definition in litrature:
        HWHM_T = r_0_5 - T_mu;
        HWHM_C = r_0_5 - C_mu;
        sigma_T = GasHelper.get_sigma_of_gaussian_from_HWHM(HWHM_T);
        sigma_C = GasHelper.get_sigma_of_gaussian_from_HWHM(HWHM_C);
        if is_constant_pc
          T_vect = T0*ones(1, r);
          C_vect = C0*ones(1, r);
          T_vect(T_mu:end) = GasHelper.gen_gaussian(T_mu:r, (T0-Ta), T_mu-1, sigma_T, Ta);
          C_vect(C_mu:end) = GasHelper.gen_gaussian(C_mu:r, (C0-Ca), C_mu-1, sigma_C, Ca);
        else
          T_vect = zeros(1, r);
          C_vect = zeros(1, r);
          T_vect(1:T_mu) = linspace(T0, MAGIC_NUM*T0, T_mu);
          C_vect(1:C_mu) = linspace(C0, MAGIC_NUM*C0, C_mu);
          T_vect(T_mu:end) = GasHelper.gen_gaussian(T_mu:r, (MAGIC_NUM*T0-Ta), T_mu-1, sigma_T, Ta);
          C_vect(C_mu:end) = GasHelper.gen_gaussian(C_mu:r, (MAGIC_NUM*C0-Ca), C_mu-1, sigma_C, Ca);
        end
        T_im(iRow,1:r) = T_vect;
        C_im(iRow,1:r) = C_vect;
      end
    end
 
    
    function [S,nS] = get_similarity_of_2_arrays(a, b, method, w)
      % 'a','b' : Two 1D arrays to compare.
      % 'method': Similarity metric: {'NRMSE'},'cosine', 'cross_correlation','RMSE'
      % 'w'     : Optional vector of weights. {1/N}
      % OUTPUT: 'S' - similarity measure. 'nS' - with normalization.
      % NOTE: meaning of output differs according to method.
      % TODO: check Weighted Sample covariance: 
      % https://en.wikipedia.org/wiki/Weighted_arithmetic_mean#Weighted_sample_variance
      a = a(:); b = b(:); % assure same known dimensions
      N = numel(a); % length of the arrays
      S = [];
      if ~exist('w', 'var')
        w = ones(size(a))/N;
      else
        w = w(:);
      end
      if ~exist('method', 'var')
        method = 'NRMSE';
      end
      switch method
        case 'cosine'
          nS = a'*b / norm(a) / norm(b);% Cosine similarity
        case 'cross_correlation'
          nS = corrcoef(a,b);
          nS = nS(1,2);
        case 'RMSE'
          na = GasHelper.normalize_signal(a);
          nb = GasHelper.normalize_signal(b);
          nS = sqrt( w' * ((na-nb).^2) ); % No need to divide by N (mean) as the weights already doing this.
        case 'NRMSE' % See "https://en.wikipedia.org/wiki/Root-mean-square_deviation#Normalized_root-mean-square_deviation"
          deltas = a-b;
          nS = sqrt( (w'*(deltas.^2))) / (nanmax(deltas)-nanmin(deltas)); % Again, no need to divide by N because of 'w'.
%           nS = sqrt( (w'*(deltas.^2))) / nanmean(deltas);
          S = rms(a-b);
        otherwise
      end
    end
    
    
    function [S] = distance_between_images(im1, im2, w)
      % im1,im2 - images.
      % {w} - (optional) image of weights.
      if nargin < 3 || isempty(w) % if not supplied - no weights:
        w = ones(size(im1));
      end
%       w = (1:M)/(M*(M+1)/2); % divide by sum(1:M) faster
      err = w .* (im1-im2); % with weights applied
      S.L1 = nansum(abs(err(:))); % L1 norm
%       rms_val = rms(err(:)); % Problem: RMS is sensative to large spikes because of the squaring
      S.L2 = norm(err); % L2 norm. Problem: sensative to large spikes because of the squaring
%       S.fro = norm(err,'fro'); % Frobenius Norm - same as L2 in my case.
    end
    
    function tv_norm = tvnorm(I, method)
      if nargin < 2
        method = 'l2';
      end
      [gx,gy] = gradient(I);
      switch method
        case 'l1'
          tmp = abs(gx + gy);
        case 'l2'
%           [Gmag, ~] = imgradient(I);
          tmp = sqrt(gx.^2 + gy.^2);
        otherwise
          error('Invalid method');
      end
      tv_norm = nansum(tmp(:)); % sum over all pixels
    end
    
    function [x, cost_vect, norm_step_vcet, nIters] = my_pattern_Search(x0, f, varargin)
      % Naive Pattern Search with the minimal mesh size N+1 for N unknowns.
      % e.g. if N = 3, directions are [1 0 0], [0 1 0], [0 0 1], [-1 -1 -1].
      %Optional args in varargin:
      % lb, ub   : vectors of lower and upper bounds.
      % tol      : tolerance for convergence
      % maxIters : max number of iteration.
      n = numel(x0);
      numvarargs = length(varargin);
      %Set defaults for optional inputs:
      lb = 0.5*x0;
      ub = 1.5*x0;
      tol = 1e-3;
      maxIters = 5e2;
      optargs = {lb, ub, tol, maxIters};
      % put defaults into the valuesToUse cell array,
      % and overwrite the ones specified in varargin.
      optargs(1:numvarargs) = varargin;
      % Place optional args in memorable variable names
      [lb, ub, tol, maxIters] = optargs{:};
      %%%%
      ds = [eye(n), -ones(n,1)]; % mesh directions matrix
      steps = abs(x0-lb)/3; % initialize steps vector
      opt_cost = f(x0); % optimal cost so far
      cost_vect = nan(maxIters,1);
      norm_step_vcet = nan(maxIters,1);
      nIters = 1;
      while norm(steps) > tol && nIters < maxIters
        flag = false; % Successful poll flag
        for id = 1 : n+1 % loop over mesh (of size N+1)
          x = x0 + steps.*ds(:,id);
          if any( bsxfun(@lt, x, lb) | bsxfun(@gt, x, ub) )  % if x is out of bounds, try another direction
            continue;
          else
            cost = f(x);
            if cost < opt_cost % we got improvement:
              flag = true;
              x0 = x;
              opt_cost = cost;
              cost_vect(nIters) = cost;
            end
          end
        end
        if flag % successful poll - increase mesh size
          steps = 2*steps;
        else    % unsuccessful poll - reduce mesh size
          steps = 0.5*steps;
        end
        norm_step_vcet(nIters) = norm(steps);
        nIters = nIters + 1;
      end
      cost_vect = cost_vect(1:nIters-1);
      norm_step_vcet = norm_step_vcet(1:nIters-1);
    end
    
    function [x, cost_vect, norm_step_vcet, nIters] = my_pattern_Search_constrained(x0, f, A, b, varargin)
      % Naive Pattern Search with the minimal mesh size N+1 for N unknowns.
      % e.g. if N = 3, directions are [1 0 0], [0 1 0], [0 0 1], [-1 -1 -1].
      % Solutions have to satisfy the linear constrains Ax <=b. 
      % Optional args in varargin:
      % lb, ub   : vectors of lower and upper bounds.
      % tol      : tolerance for convergence
      % maxIters : max number of iteration.
      n = numel(x0);
      numvarargs = length(varargin);
      %Set defaults for optional inputs:
      lb = 0.5*x0;
      ub = 1.5*x0;
      tol = 1e-3;
      maxIters = 5e2;
      optargs = {lb, ub, tol, maxIters};
      % put defaults into the valuesToUse cell array,
      % and overwrite the ones specified in varargin.
      optargs(1:numvarargs) = varargin;
      % Place optional args in memorable variable names
      [lb, ub, tol, maxIters] = optargs{:};
      %%%%
      if any(A*x0 > b) || any(x0<lb) || any(x0>ub)
        error('Initial point doesn''t meet constraints');
      end
      ds = [eye(n), -ones(n,1)]; % mesh directions matrix
      steps = abs(x0-lb)/3; % initialize steps vector
      opt_cost = f(x0); % optimal cost so far
      cost_vect = nan(maxIters,1);
      norm_step_vcet = nan(maxIters,1);
      nIters = 1;
      while norm(steps) > tol && nIters < maxIters
        flag = false; % Successful poll flag
        for id = 1 : n+1 % loop over mesh (of size N+1)
          x = x0 + steps.*ds(:,id);
          if any( bsxfun(@lt, x, lb) | bsxfun(@gt, x, ub) ) || any(A*x > b)  % if x is out of bounds, try another direction
            continue;
          else
            cost = f(x);
            if cost < opt_cost % we got improvement:
              flag = true;
              x0 = x;
              opt_cost = cost;
              cost_vect(nIters) = cost;
            end
          end
        end
        if flag % successful poll - increase mesh size
          steps = 2*steps;
        else    % unsuccessful poll - reduce mesh size
          steps = 0.5*steps;
        end
        norm_step_vcet(nIters) = norm(steps);
        nIters = nIters + 1;
      end
      cost_vect = cost_vect(1:nIters-1);
      norm_step_vcet = norm_step_vcet(1:nIters-1);
    end
    
    function [Gnum] = numeric_gradient(f, x)
      % Gets function handle "myfunc", vector "x" and calculates 
      % numerical gradient "Gnum". 
      n = length(x);
      e = eye(n);
      f1 = zeros(n,1);
      f2 = zeros(n,1);
      epsi = eps^(1/3)*max(abs(x)); % TODO - check
      for i=1:n
        f1(i) = f(x+epsi*e(:,i));
        f2(i) = f(x-epsi*e(:,i));
      end
      Gnum = (f1-f2)/2/epsi;
    end
    
    function [alpha] = armijo(x,f,gx,alpha0,d,beta,sigma)
      % Calculate step size 'alpha' using armijo's rule
      alpha = alpha0;
      j = 1;
      while (j>0)
        x_new = x + alpha.*d;
        if (f(x_new)<=f(x)+sigma*alpha*gx'*d)
          j = 0;
        else
          alpha = alpha*beta;
        end
      end
    end
    
    function [x,fopt] = gradientDescent(f, x0, lb, ub)
    % Abused Gradient Descent to incorporate bounds.
      eps = 1e-5;% stopping criteria on Gradient norm
      alpha0 = min(1000,norm(x0)); % initial step size
%       sigma = 0.25;%Armijo rule initial paramters
%       beta = 0.5;%Armijo rule initial paramters
      maxIter = 1e3;
      x = x0;
      g = GasHelper.numeric_gradient(f, x);
      flag = norm(g) >= eps;
      k = 1;
      f_old = f(x);
      while flag
        d = -g; % Gradient Descent method
        alpha = alpha0; % initial step size
%         % Calc step size via Armijo rule
%         alpha = GasHelper.armijo(x,f,g,alpha,d,beta,sigma);
        % Loop for enforcing bounds and adjust step size:
        for iStep = 1 : 10
          x_possible = x + alpha*d;
          % if x is out of bounds or no improvement, reduce step size:
          f_new = f(x_possible);
          if any( bsxfun(@lt, x_possible, lb) | bsxfun(@gt, x_possible, ub) ) || f_new >= f_old
            alpha = alpha / 2;
          else
            break;
          end
        end
        % Update new point
%         x = x + alpha*d;
        x = x_possible;

        % checking Stopping criteria and updating:
        g = GasHelper.numeric_gradient(f, x);
        flag = (norm(g) >= eps) && (k < maxIter);
        k = k+1;
        f_old = f_new;
      end
      fopt = f(x);
    end
    
    function [mask] = FLIR_mask_location_of_image_in_full_frame(imgInfo)
      MAX_ROWS = 512;
      MAX_COLS = 640;
      mask = zeros(MAX_ROWS,MAX_COLS);
      cliprect_im = imgInfo.m_cliprect;
      % '1' because FLIR index start @ 0 and MATLAB @ 1,
      % and also axes are opposite between those SW:
      mask( cliprect_im(2)+1 : cliprect_im(2)+cliprect_im(4), ...
            cliprect_im(1)+1 : cliprect_im(1)+cliprect_im(3) ) = 1;
    end
    
    function normalize_axes_full_im(im,D_nozzle_pixels, symAxis, vertical_tick_factor, lateral_tick_factor)
      yt = fliplr(size(im,1) : -D_nozzle_pixels/vertical_tick_factor : 1);
      ytl = strsplit(num2str((numel(yt)-1:-1:0)/vertical_tick_factor), ' ');
      set(gca,'ytick',yt);
      set(gca,'yticklabel',ytl);
      xt = [fliplr(symAxis-D_nozzle_pixels/lateral_tick_factor : -D_nozzle_pixels/lateral_tick_factor : 1) symAxis : D_nozzle_pixels/lateral_tick_factor : size(im,2)];
      xtickl = (xt-symAxis)/D_nozzle_pixels;
      xtickl = round(xtickl, 1);
      xtl = strsplit(num2str(xtickl(floor(xtickl*lateral_tick_factor)==xtickl*lateral_tick_factor)), ' ');
      set(gca,'xtick',xt );
      set(gca,'xticklabel',xtl);
      xlabel('y/D'); ylabel('x/D');
    end
    
    
    function normalize_axes_half_im(im, D_nozzle_pixels, vertical_tick_factor, n_lateral_ticks)
      yt = fliplr(size(im,1):-floor(D_nozzle_pixels/vertical_tick_factor):1);
      ytl = strsplit(num2str((numel(yt)-1:-1:0)/vertical_tick_factor), ' ');
      set(gca,'ytick',yt);
      set(gca,'yticklabel',ytl);
      xt = strsplit(num2str(linspace(0, ceil(size(im,2)/D_nozzle_pixels), n_lateral_ticks)), ' ');
      set(gca,'xtick',1+linspace(0, ceil(size(im,2)/D_nozzle_pixels), n_lateral_ticks)*D_nozzle_pixels );
      set(gca,'xticklabel',xt);
      xlabel('y/D'); ylabel('x/D');
    end
    
    function Compare_TC_samples_to_reconstructed_T(T_im, T_meas, D_nozzle_pixels, symAxis, case_title, show_abs_err, mid_title, desc_title)
      % Gets reconstructed temperature image 'T_im' in [K] and matches to a Nx4
      % matrix of N measurements 'T_meas': 1st column is #row, 2nd is #col
      % (in the raw image), 3rd is the measured temperature [C],
      % 4th is the measurement estimated error [C].
      % 'D_nozzle_pixels' - Effective Jet Diameter [pixels]
      % 'symAxis' - #columm corresponds to the axis of symmetry in the raw image
      % 'case_title' - optional string of the experiment case.
      
      % Assumption: If #rows of T_im  < original image, this could've been
      % done by only cropping the original image from its 1st row downward
      % by some N rows.
      if nargin < 5
        case_title = '';
      end
      if nargin < 6 || isempty(show_abs_err)
        show_abs_err = true;
      end
      if nargin < 8 || isempty(mid_title) || isempty(desc_title)
        mid_title = 'Reconstructed Temperature [K]';
        desc_title = 'Red pairs: Absolute [K] and Relative [%] errors';
      end
      assert(size(T_meas,2) == 4);
      [M,N] = size(T_im);

      T_meas(:,2) = abs(T_meas(:,2) - symAxis) + 1; % location w.r.t. centerline
      T_meas(:,3) = T_meas(:,3) + 273.15; % convert to [K]
%       i_meas = sub2ind(size(T_im), T_meas(:,1), T_meas(:,2) );
%       abs_err = T_im(i_meas) - T_meas(:,3); % difference [K]
      abs_err = nan(size(T_meas,1), 1);
      rel_err = nan(size(abs_err));
      weights = nan(size(abs_err));
      strs    = cell(size(abs_err));
      for iMeasure = 1 : size(T_meas,1)
        if T_meas(iMeasure,1) <= M && T_meas(iMeasure,2) <= N
          abs_err(iMeasure) = T_im(T_meas(iMeasure,1), T_meas(iMeasure,2)) - T_meas(iMeasure,3);
          rel_err(iMeasure) = 100* abs(abs_err(iMeasure) / T_meas(iMeasure,3));
%           strs{iMeasure} = [num2str(abs_err(iMeasure), 3),'(', num2str(rel_err(iMeasure), 3),'%)'];
%           strs{iMeasure} = [num2str(abs_err(iMeasure), 2), ' ',num2str(rel_err(iMeasure), 3)];
          if show_abs_err
            strs{iMeasure} = [sprintf('%d', round(abs_err(iMeasure))), ' ', sprintf('%.2f', rel_err(iMeasure))];
          else
            strs{iMeasure} = sprintf('%.2f', rel_err(iMeasure));
          end
          weights(iMeasure) = 1 / ( ((T_meas(iMeasure,1)-M)^2 + (T_meas(iMeasure,2)^2) )+ 1);
        end
      end
      h = GasHelper.plotIm(T_im);
      GasHelper.addTitle2Colorbar(h, 'T[K]');
      RMSE = sqrt(nansum(weights.*(abs_err.^2))/nansum(weights));
      MAE = nansum(weights.*(abs(abs_err)))/nansum(weights);
      title({[case_title,mid_title], desc_title, ['Weighted MAE = ', sprintf('%.3f',MAE),', Weighted RMSE = ',sprintf('%.3f',RMSE)] });
      hold on;
      scatter(T_meas(:,2), T_meas(:,1), '*k');
      text_props = {'Color','r','FontSize',10};
      text(T_meas(:,2)+1, T_meas(:,1)-5, strs, text_props{:});
%       text(T_meas(:,2), T_meas(:,1), [num2str(abs_err, 3);'(', num2str(rel_err, 3),'%)'], text_props{:});
      GasHelper.normalize_axes_half_im(T_im, D_nozzle_pixels, 2, 5);
      GasHelper.maximizeFigure;
    end
    
    function [T_fit] = get_centerline_T_fit(TC_meas, T_inf)
      % Gets vector TC_meas[K] of centerline T measurements and T_inf[K].
      % fits to the model:
      % T_cl(x) = (T0-T_inf) ./ (x - b) * (X_PC - b) + T_inf
      T_fit = cftool
      
      T_cl(x) = (x < XPC).*T0 + (x >= XPC).*((T0-298)./(x-b).*(XPC-b) + 298)
      
      T(x,y) = (y <= mu(x)).*T_cl(x) + (y > mu(x)).*( (T_cl(x)-298).*exp(-0.5*((y-mu(x))./sig(x)).^2) + 298)
      
    end
    
    
    function [D_eff_height] = get_D_height_above_orifice(im, options)
      % Works nice with the raw DL images or the R image.
      % D_eff_height - the row corresponds to the effective diameter within
      % the input image 'im'.
      N = size(im,1); % Assumed the orifice' row!
      MAX_HEIGHT_PIXELS = floor(N/4);
      USE_THEORY = false;
      if nargin == 2
        if isfield(options,'MAX_HEIGHT_PIXELS')
          MAX_HEIGHT_PIXELS = options.MAX_HEIGHT_PIXELS;
        end
        if isfield(options,'USE_THEORY') && isfield(options,'D0')
          USE_THEORY = options.USE_THEORY;
          D0 = options.D0;
        end
      end
      if USE_THEORY % Assumes Vena contracta is D/2 above orifice and A/A0 = 0.62
        % SOURCE: https://mysite.du.edu/~jcalvert/tech/fluids/orifice.htm
        % Or: R. L. Daugherty and J. B. Franzini, Fluid Mechanics, 6th ed. (New York: McGraw-Hill, 1965). pp. 338-349
        D_eff_height = N-floor(D0/2);
      else
        agy = abs(diff(im, 1, 1));
        agy = abs(im);
        row_vec = N-1:-1:N-MAX_HEIGHT_PIXELS;
%         ss = nansum(agy(row_vec,:), 2);
        ss2 = nanmean(agy(row_vec,:), 2);
%         sss = smooth(ss); % Moving average of order 5
        sss2 = smooth(ss2); % Moving average of order 5
        diffss = abs(diff(ss2));
%         figure; plot(ss); hold on; plot(sss); plot(abs(diff(ss))); xlabel('rows above nozzle');
%         diffss = abs(diff(ss));
%         iHeight = find(otsu(diffss,2) < 2, 1, 'first');
%         iHeight = find(otsu(sss,4) < 3, 1, 'first');
%         line([iHeight iHeight], ylim); % reference line
%         legend('sum(row)', 'smoothed sum', 'diff(smoothed sum)', 'Estimated height');
        figure; plot(sss2); hold on; plot(diffss); xlabel('rows above nozzle');
%         iHeight2 = find(otsu(sss2,4) < 3, 1, 'first');
        iHeight2 = find(otsu(diffss,2) < 2, 1, 'first');
        line([iHeight2 iHeight2], ylim); % reference line
        legend('smoothed means(rows)', '|diff(smoothed means)|', 'Estimated height'); 
        D_eff_height = row_vec(iHeight2);
      end
    end
    
    function [D_eff_pixels] = get_D_eff(im_row, options)
      % Works nice with the Inverse Abel of R image. assumes 'im_row' was
      % already found as the row above the orifice of the effective
      % Diameter.
      PEAK_MIN_WIDTH = 3;
      PEAK_MIN_DISTANCE = 5;
      PEAK_MIN_PROMINENCE = 0.03; % Shit!
      USE_THEORY = false;
      HALF_IM = true; % In particular, if the input is Abel Inverse.
      if nargin == 2 && ~isempty(options)
        if isfield(options,'PEAK_MIN_WIDTH')
          PEAK_MIN_WIDTH = options.PEAK_MIN_WIDTH;
        end
        if isfield(options,'PEAK_MIN_DISTANCE')
          PEAK_MIN_DISTANCE = options.PEAK_MIN_DISTANCE;
        end
        if isfield(options,'MAX_HEIGHT_PIXELS')
          MAX_HEIGHT_PIXELS = options.MAX_HEIGHT_PIXELS;
        end
        if isfield(options,'HALF_IM')
          HALF_IM = options.HALF_IM;
        end
        if isfield(options,'USE_THEORY') && isfield(options,'D0')
          USE_THEORY = options.USE_THEORY;
          D0 = options.D0;
        end
      end
      if USE_THEORY % Assumes Vena contracta is D/2 above orifice and A/A0 = 0.62
        % SOURCE: https://mysite.du.edu/~jcalvert/tech/fluids/orifice.htm
        % Or: R. L. Daugherty and J. B. Franzini, Fluid Mechanics, 6th ed. (New York: McGraw-Hill, 1965). pp. 338-349
        D_eff_pixels = ceil(sqrt(0.62)*D0);
      else
        g = abs(diff(im_row));
        g = g(~isnan(g));
        [pks, locs, w, p] = findpeaks(g,'Annotate','extents','WidthReference','halfheight', 'MinPeakDistance',PEAK_MIN_DISTANCE, 'MinPeakWidth',PEAK_MIN_WIDTH, 'MinPeakProminence', PEAK_MIN_PROMINENCE*nanmax(g) );
%         findpeaks(g,'Annotate','extents','WidthReference','halfheight', 'MinPeakDistance',PEAK_MIN_DISTANCE, 'MinPeakWidth',PEAK_MIN_WIDTH, 'MinPeakProminence', PEAK_MIN_PROMINENCE*nanmax(g) ); % DEBUG
        [~, I] = sort(pks,'descend');
        if HALF_IM
          if ~isempty(I)
            I = I(1);
            D_eff_pixels = 2 * locs(I);
          else
            I = otsu(g, 4);
            D_eff_pixels = 2*find(I==4,1,'first');
          end
        else
          I = I(1:2); % Take the most two prominent peaks
          pks = pks(I);
          locs = locs(I);
          w = w(I);
          D_eff_pixels = abs(locs(2)-locs(1));
        end
      end
    end
    
    function [D_eff_height, D_eff, width_line] = get_D_height_above_nozzle(im, symAxis, options)
      N = size(im,1); % Assumed the orifice' row!
      PEAK_MIN_WIDTH = 3;
      PEAK_MIN_DISTANCE = 5;
      PEAK_MIN_PROMINENCE = 0.03; % Shit!
      MAX_HEIGHT_PIXELS = floor(N/4); % search only this height above last row (nozzle?)
      USE_THEORY = false;
      if nargin == 3 && ~isempty(options)
        if isfield(options,'PEAK_MIN_WIDTH')
          PEAK_MIN_WIDTH = options.PEAK_MIN_WIDTH;
        end
        if isfield(options,'PEAK_MIN_DISTANCE')
          PEAK_MIN_DISTANCE = options.PEAK_MIN_DISTANCE;
        end
        if isfield(options,'MAX_HEIGHT_PIXELS')
          MAX_HEIGHT_PIXELS = options.MAX_HEIGHT_PIXELS;
        end
        if isfield(options,'USE_THEORY')
          USE_THEORY = options.USE_THEORY;
        end
        if isfield(options,'D0')
          D0 = options.D0;
        end
      end
      if USE_THEORY % Assumes Vena contracta is D/2 above orifice and A/A0 = 0.62
        % SOURCE: https://mysite.du.edu/~jcalvert/tech/fluids/orifice.htm
        % Or: R. L. Daugherty and J. B. Franzini, Fluid Mechanics, 6th ed. (New York: McGraw-Hill, 1965). pp. 338-349
        D_eff_height = N-floor(D0/2);
        D_eff = ceil(sqrt(0.62)*D0);
        width_line = [];
      else
        if 1
          agy = abs(diff(im, 1, 1));
          row_vec = N-1:-1:N-MAX_HEIGHT_PIXELS;
          ss = nansum(agy(row_vec,:), 2);
          figure; plot(ss); hold on; plot(diff(ss)); legend('sum(row)','diff(sum)'); xlabel('rows above nozzle');
%           [N,edges] = histcounts(ss,50);
          iHeight = find(otsu(ss,4) < 2, 1, 'first');
          D_eff_height = row_vec(iHeight) + 1;
        else
          [gx,~] = gradient(im);
          gx = abs(gx);
          row_vec = N:-1:N-MAX_HEIGHT_PIXELS;
          D_eff = zeros(size(row_vec));
          for i = 1 : numel(row_vec)
            g = gx(row_vec(i) , :);
            g = g(~isnan(g)); % For the Inverse Abel case
            [pks, locs, w, p] = findpeaks(g,'Annotate','extents','WidthReference','halfheight', 'MinPeakDistance',PEAK_MIN_DISTANCE, 'MinPeakWidth',PEAK_MIN_WIDTH, 'MinPeakProminence', PEAK_MIN_PROMINENCE*nanmax(g) );
            [~, I] = sort(pks,'descend');
            I = I(1:2); % Take the most two prominent peaks
            pks = pks(I);
            locs = locs(I);
            w = w(I);
            D_eff(i) = abs(locs(2)-locs(1));
          end
          width_line.rows = row_vec;
          width_line.vals = D_eff;
          GasHelper.plotIm(im(:,symAxis:end),'Estimating effective D height & width:');
          hold on;
          plot(width_line.vals/2 , width_line.rows);
          line_g = abs(diff(width_line.vals(1:MAX_HEIGHT_PIXELS)));
          figure; plot(width_line.rows(1:MAX_HEIGHT_PIXELS-1), line_g);
          title('Gradient of the above line');
          [~,D_eff_height_ind] = nanmax(line_g);
          D_eff_height = width_line.rows(D_eff_height_ind);
          D_eff = width_line.vals(D_eff_height_ind);
        end
      end
    end
    
    function [Re, Pe, Gr] = cacl_jet_dimensionless_numbers(params)
      D = params.D; % diameter [m]
      tot_mfr = params.tot_mfr; % Total mass flow rate, [g/s]
      co2_mfr = params.co2_mfr; % CO2 mass flow rate, [g/s]
      Tamb_C = params.Tamb_C; % ambient temperature, [C]
      Tjet_C = params.Tjet_C; %Jet's outlet temperature, [C]
      P0 = params.P0; %[Pa]. Pressure @ outlet
      %Constants
      R = 8.314459848; % universal gas constant [J/mol/K]
      molar_mass_air = 28.9645; %[g/mol]
      moalr_mass_co2 = 44.01; %[g/mol]
      g = 9.8; %gravity [m/s^2]
      % Auxiliary functions
      C2K = @(T) T + 273.15; % Celsius to Kelvin
      rhoIdealGas = @(p,T,mol_mas) p/(R/mol_mas*1e3*T); % Ideal gas equation, T[K], p[Pa], mol_mas[g/mol]
      Tamb = C2K(Tamb_C);
      Tjet = C2K(Tjet_C);
      A = pi/4*D^2; %Nozzle's area
      C0 = co2_mfr/tot_mfr;
      [mu_co2, mu_air] = CalcViscousity(Tjet); % get the viscousity in the jet's temp
      mu_mass_avged = ( co2_mfr*mu_co2 + (tot_mfr-co2_mfr)*mu_air ) / tot_mfr;% Mass-averaged viscousity
      rho_air = rhoIdealGas(P0,Tjet,molar_mass_air);
      rho_co2 = rhoIdealGas(P0,Tjet,moalr_mass_co2);
      %%% I can take just mu_air, instead of mu_mass_avged. ~1.5% error
      rho_avged = C0*rho_co2 + (1-C0)*rho_air;%averaged density [kg/m^3]
      U0 = tot_mfr*1e-3/A/rho_avged; % Outlet velocity [m/s]
      Re = rho_avged*U0*D / mu_mass_avged; % Reynolds number in the orifice
      Re_alternative = tot_mfr*1e-3/A*D/mu_mass_avged; % since m_dot = rho*U*A
      beta = 1/Tjet; % thermal expansion coefficient for ideal gas
      ni = mu_mass_avged/rho_avged; % kinematic viscosity [m^2/s]
      Gr = g*beta*(Tjet-Tamb)*D^3/ni^2;% Grashof number
      if Gr/Re^2 > 1e-4 %compare buoyant to inertial forces
        warning('Buoyancy might not be neglegable!');
      end
      Pr = 0.71;
      Pe = Re*Pr; % Peclet number - advection to diffusion ratio in forced convection
    end
    
    function [figs_handles] = get_all_figures()
      figs_handles = findall(groot, 'Type', 'figure');
    end
    
    function [status] = save_all_figures(parentdir,dirname)
      % Save all figures to a directory as .png file, and all figures
      % to a single .fig file named as 'dirname'.
      % .png file names are based on the figures' titles, after casting to valid
      % file names.
      if nargin < 2 
        error('Invalid args!')
      end
      dirpath = fullfile(parentdir,dirname);
      if ~exist(dirpath, 'dir')
        status = mkdir(parentdir, dirname);
      end
      orig_folder = cd(dirpath);
      % Save all figures to .png files with approx. their title names:
      figHandles = GasHelper.apply_func_to_all_figures(@GasHelper.save_fig_with_valid_filename_from_fig_title);
      fig_file_name = [dirname,'.fig'];
      savefig(figHandles, fig_file_name ,'compact'); % save all figures in one .fig
      cd(orig_folder);
    end
    
    function [figHandles] = apply_func_to_all_figures(func_handle)
      figHandles = GasHelper.get_all_figures();
      for ih = 1 : numel(figHandles)
        func_handle(figHandles(ih));
      end
    end
    
    function [fig_name] = save_fig_with_valid_filename_from_fig_title(hF)
      fig_name = (sprintf('figure_%d',hF.Number));
%       fig_name = hF.CurrentAxes.Title.String;
%       if ~isequal(class(fig_name), 'char') % if 'cell'
%         fig_name = [fig_name{1} fig_name{2}];
%       end
%       if (length(fig_name) > namelengthmax) || isempty(fig_name)
%         warning('File name is empty / longler than namelengthmax! modifing to figure number...');
%         fig_name = (sprintf('figure_%d',hF.Number));
%       end
%       fig_name = genvarname(fig_name); % make it a valid filename
      hgexport(hF, sprintf('%s.png',fig_name), hgexport('factorystyle'), 'Format', 'png');
    end
    
    function add_pixelwise_info_to_fig(hF)
      figure(hF);
      impixelinfo;
    end
    
    function toggle_info_to_all_figs()
      GasHelper.apply_func_to_all_figures(@GasHelper.add_pixelwise_info_to_fig);
    end
    
    
    function [] = addTitle2Colorbar(h_figure, titleStr)
      ax = findall(h_figure, 'type', 'axes');
      ylabel(ax.Colorbar, titleStr);
    end
    
    function [hF] = plot_rel_error_half_im(im_m, im_sim, weights_im, D_nozzle_pixels, title_str)
      if nargin<5 || isempty(title_str)
        title_str = '';
      end
      rel_error_im = 100*abs((im_m-im_sim)./im_m);
      rel_error_im(~weights_im) = NaN;
      hF = GasHelper.plotIm(rel_error_im, title_str);
      GasHelper.addTitle2Colorbar(hF, 'Relative error [%]');
      GasHelper.normalize_axes_half_im(im_m, D_nozzle_pixels, 2, 5);
    end
    
    
     
  end%Methods
end%ClassDef