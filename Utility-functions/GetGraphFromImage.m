close all; clear all; clc;
% Assumes: image is "simple" and rectified.
%       image pixel [col,row], and origin is top left.
%       Single graph (for several non overlapping graphs - will plot the
%       upper one).
%       Poor results, zoom sensative...

graph_th = 5; % assumed gray level th for finding the graph
debug = 0; % plotting figures
plot_output = 1; % plot final output graph

[imFile, imPath] = uigetfile('*.*','Select image');
assert(~isequal(imFile,''));
fullName = [imPath,imFile];
[pathstr,name,ext] = fileparts(fullName);
NewFile = [name,'_data_from_image'];

im = imread(fullName);
if size(im,3) > 3
  im = im(:,:,1:3);
end
im = rgb2gray(im);
[Nrows,Ncols] = size(im);
figure; imshow(im); impixelinfo;

%%
disp('Choose some grid cube high limit:');
[~,higher_grid_row] = ginput(1);
higher_grid_row = int16(higher_grid_row);
higher_grid_val =input('Enter its value:\n');
disp('Choose same grid cube low limit:');
[~,lower_grid_row] = ginput(1);
lower_grid_row = int16(lower_grid_row);
lower_grid_val = input('Enter its value:\n');
disp('Choose some point on the graph:');
[x,y] = ginput(1);
graph_color = im(uint16(y),uint16(x));
disp('Choose the graph starting x-val:');
[graph_col_start,~] = ginput(1);
graph_col_start = int16(graph_col_start);
graph_x_start = input('Enter its value:\n');
disp('Choose the graph ending x-val:');
[graph_col_end,~] = ginput(1);
graph_col_end = int16(graph_col_end);
graph_x_end = input('Enter its value:\n');
pixel_diff = double(abs(higher_grid_row-lower_grid_row));
grid_val_diff = abs(higher_grid_val-lower_grid_val);
x_diff = double(abs(graph_x_end - graph_x_start));
x_pixel_diff = abs(graph_col_end-graph_col_start);

assert(pixel_diff && grid_val_diff && x_diff && x_pixel_diff);
if ~debug
    close all;
end

dval = grid_val_diff/pixel_diff;
vals(higher_grid_row) = higher_grid_val;
%%
vals(higher_grid_row:-1:1) = higher_grid_val:dval:higher_grid_val+dval*(double(higher_grid_row)-1);
vals(higher_grid_row+1:Nrows) = higher_grid_val-dval:-dval:higher_grid_val-dval*(Nrows-double(higher_grid_row));


im = im(:,graph_col_start:graph_col_end);
indices = abs(im - graph_color) <= graph_th;
if debug
    figure; imshow(indices);
end
Ncols = size(im,2);
graph_vals = repmat(vals',1,Ncols).*indices;
if debug
    figure; imshow(graph_vals); impixelinfo;
end

x_vect = linspace(graph_x_start, graph_x_end, Ncols);
y_vect = zeros(Ncols,1);
for col=1:Ncols
    [~,~,y_vect(col)] = find(graph_vals(:,col),1);
end

if debug || plot_output
    figure; plot(x_vect,y_vect);
end
data.x = x_vect';
data.y = y_vect;
save([imPath,NewFile], 'data');