classdef ImageBGPairExt < ImageBGPair
  %IMAGEBGPAIREXT Is a class extending IMAGEBGPAIR
  %  Allows retreival of image & BG in addition to subtracted image.
  
  properties (GetAccess = public, SetAccess = private)
      isPairValid = false;
  end
  
  methods

    function obj = ImageBGPairExt(varargin)% Constuctor, allow 1 or 2 inputs
        %Call like: obj = ImageBGPairExt(ImageFullPath,IMG_Info)% Constuctor
        obj = obj@ImageBGPair(varargin{:});
        obj.isPairValid = false;
    end
        
    function setBG(thisIBP, BackgroundFullPath, BG_Info) %Extends setBG()
        if nargin >=3
            thisIBP.setBG@ImageBGPair(BackgroundFullPath,BG_Info);
        else
            thisIBP.setBG@ImageBGPair(BackgroundFullPath);
        end
        thisIBP.isPairValid = true;
    end

    function out = getImage(thisIBP)
        if thisIBP.isPairValid
            out = PTWFileUtils.ReadAllFramesFromFile(thisIBP.ImageFullPath,     thisIBP.IMG_Info);
            out = mean(out,3);
        else
            throw(MException('getImage:invalidPair', 'Pair is invalid'));
        end
    end
    function out = getBG(thisIBP)
        if thisIBP.isPairValid
            out = PTWFileUtils.ReadAllFramesFromFile(thisIBP.BackgroundFullPath, thisIBP.BG_Info);
            out = mean(out,3);
        else
            throw(MException('getImage:invalidPair', 'Pair is invalid'));
        end
    end
    
    
    function out = getSubtracted(thisIBP,flip_BG_horiz, flip_BG_vert)%Extends father
      % OUTPUT:
      % 3- or 4-D array, where dims 1,2 are physical, dim 3 is frames per file, and dim 4
      % is different ITs (in case thisIBP is a vector of equally-sized image pairs). 
      % NOTE:
      %   When a 4-D array is returned, it is usually safe to average it over the 3rd 
      %   dimension. This is not done automatically because there exist algorithms smarter
      %   than "plain averaging" (e.g. NLM-Multi).
      if ~isscalar(thisIBP) && ismatrix(thisIBP) %input is array of "ImageBGPair"s
        out = cell(size(thisIBP)); % preallocation
        for ind1 = 1:numel(thisIBP)
          out{ind1} = thisIBP(ind1).getSubtracted;
        end
        % convert to numeric matrix if all cells are the same size
        if ~diff(any(diff(cell2mat(cellfun(@size,out,'UniformOutput',false)))))
          out = cat(4,out{:});
        end
        return
      elseif isempty(thisIBP.BG_Info)
        warning('Attempterd to obtain subtracted image for an incomplete pair!');
        out = []; return
      end
      img = PTWFileUtils.ReadAllFramesFromFile(thisIBP.ImageFullPath,     thisIBP.IMG_Info);
      bg  = PTWFileUtils.ReadAllFramesFromFile(thisIBP.BackgroundFullPath, thisIBP.BG_Info);
      if nargin==3
        if flip_BG_horiz
          bg = fliplr(bg);
        end
        if flip_BG_vert
          bg = flipud(bg);
        end
      end
      % First average the BG and then subtract it from each images. 
      %   There may be a different amount of images and bg frames.
      out = bsxfun(@minus, double(img), mean(bg,3)); % the output of `mean` is already double 
    end
    
    
  end
  
  methods (Static  = true)
      function h = plotPairSubtract(hIm, hBG, hSub, sameScale)
          M = 2; N = 3;
          nBins = 128;
          if nargin < 4
              sameScale = false;
          end
          h = figure;
          if sameScale
              minlim = min( min(hIm(:)), min(hBG(:)) );
              maxlim = max( max(hIm(:)), max(hBG(:)) );
              cm = [minlim maxlim];
              ax(1) = subplot(M,N,1); imagesc(hIm, cm); colorbar; axis image; impixelinfo;
              ax(2) = subplot(M,N,2); imagesc(hBG, cm); colorbar; axis image; impixelinfo;
              ax(3) = subplot(M,N,3); imagesc(hSub, cm); colorbar; axis image; impixelinfo;
          else
              ax(1) = subplot(M,N,1); imagesc(hIm); colorbar; axis image; impixelinfo;
              ax(2) = subplot(M,N,2); imagesc(hBG); colorbar; axis image; impixelinfo;
              ax(3) = subplot(M,N,3); imagesc(hSub); colorbar; axis image; impixelinfo;
          end
          ax(4) = subplot(M,N,4); histogram(hIm,nBins);
          ax(5) = subplot(M,N,5); histogram(hBG,nBins);
          ax(6) = subplot(M,N,6); histogram(hSub,nBins);
          axes(ax(1)), title('Image');
          axes(ax(2)), title('BG');
          axes(ax(3)), title('Subtracted');
          axes(ax(4)), title('Image Histogram');
          axes(ax(5)), title('BG Histogram');
          axes(ax(6)), title('Subtracted Histogram');
      end
  end
  
end

