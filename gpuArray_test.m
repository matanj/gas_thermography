function gpuArray_test
  clc; format short;
  Niters = [1 10 100 1000 10000];
  N = 200;
  X = 10*rand(N,'double');
  GPUMemSizeMB = 2048; % [MB]
  
  f = @(x) x.^2; % function to be performed
  
  tGPU = zeros(size(Niters));
  tCPU = zeros(size(Niters));
  
  GPUMemSizeB = GPUMemSizeMB*1024*1024; % [Bytes]
  Xsize = whos('X');
  assert(Xsize.bytes < GPUMemSizeB,'X size exceeds GPU memory!');
  
  for indN = 1 : numel(Niters)
    n = Niters(indN);
    % GPU:
    tic
    for i = 1:n
        G = gpuArray(X);
        G2 = f(G);
        G1 = gather(G2);
    end
    tGPU(indN) = toc;
    
    % CPU:
    tic
    for i = 1:n
        X2 = f(X);
    end
    tCPU(indN) = toc;
  end
  
  disp(['||difference|| = ',num2str(norm(G1-X2))]);
  
  figure; loglog(Niters, tCPU,'DisplayName','CPU'); hold on; 
  loglog(Niters, tGPU,'DisplayName','GPU');
  title(['GPU Vs CPU: square matrix size = ',num2str(N)]);
  legend('-DynamicLegend');
  xlabel('log(#iteration)');
  ylabel('log(time [sec])');
  
end