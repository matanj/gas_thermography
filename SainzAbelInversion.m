function [ i_rec ] = SainzAbelInversion( i_rec_NO, i_rec_ASAI )
%Implements: Sainz (2006) - "Abel Inversion applied to a small set of Emission Data from a Microwave Plasma"
% Basically it just an a-posteriori criterion to whether use Nestor-Olsen
% or ASAI:
%(1) Find i(r) from I(r) using N-O and ASAI separately
%(2) Consider N-O i(r): 
%       if there's a r_max such that i(r_max) > i(0), use N-O in 0<=r<r_max, and ASAI in r>r_max.
%       else: use only ASAI.
if nargin < 2 || numel(i_rec_NO) ~= numel(i_rec_ASAI)
  error('invalid args!');
end
i_rec = zeros(size(i_rec_NO));
[~, ind] = max(i_rec_NO);
if ind > 1
  i_rec(1:ind) = i_rec_NO(1:ind);
  i_rec(ind+1:end) = i_rec_ASAI(ind+1:end);
else
    i_rec = i_rec_ASAI;
end

end

