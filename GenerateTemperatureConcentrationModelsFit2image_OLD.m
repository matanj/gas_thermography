function [CC, TT, depths] = GenerateTemperatureConcentrationModelsFit2image_OLD(imSize, m2pix, xNozzle_loc, r0_loc, params)
% Gets image 'im', and put on it a map of temperature & concentration based
% on a known gaussian model
% INPUTS:
%   'imSize'  - size of the image
%   'm2pix' - meter to pixels conversion
%   'xNozzle_loc' - index location of the nozzle
%   'r0_loc' - index location of the jet's symmetry axis
%   'params' - struct with experiment conditions
% OUTPUTS:
%   'C', 'T' - images with concentrations & temperatures modeled
%   'paths' - image with each pixel representing estimated depth of the jet

%% Coordinate system: x axis - along the centerline, y axis - radial

%% TODO: improve model when CFD isn't used
  if nargin < 4
    error('invalid args!');
  end
  %%default Inputs
  useCFD = true;
%   CFDpath = 'Z:\Matan\Jet CFD\FreeJet.xlsx';
  D = 1e-2; % Nozzle's diameter, [m]
  tot_mfr = 2; % Total mass flow rate, [g/s]
  co2_mfr = 0.5; % CO2 mass flow rate, [g/s]
  Tamb_C = 25; % ambient temperature, [C]
  Tjet_C = 400; %Jet's outlet temperature, [C]
  Camb = 0;%0.0004; % CO2 concentration in the ambient
  P0 = 101325; %[Pa]. Pressure @ outlet is 1 atm = 101325[Pa]
  if nargin>4
    useCFD = params.useCFD;
%     CFDpath = params.CFDpath;
    data = params.data;
    D = params.D;
    tot_mfr = params.tot_mfr; % Total mass flow rate, [g/s]
    co2_mfr = params.co2_mfr; % CO2 mass flow rate, [g/s]
    Tamb_C = params.Tamb_C; % ambient temperature, [C]
    Tjet_C = params.Tjet_C; %Jet's outlet temperature, [C]
    Camb = params.Camb;% CO2 concentration in the ambient
    P0 = params.P0; %[Pa]. Pressure @ outlet
  end

  M = imSize(1);
  N = imSize(2);

  %Constants
  R = 8.314459848; % universal gas constant [J/mol/K]
  molar_mass_air = 28.9645; %[g/mol]
  moalr_mass_co2 = 44.01; %[g/mol]

  % Auxiliary functions
  C2K = @(T) T + 273.15; % Celsius to Kelvin
  rhoIdealGas = @(p,T,mol_mas) p/(R/mol_mas*1e3*T); % Ideal gas equation, T[K], p[Pa], mol_mas[g/mol]
  %%
  Tamb = C2K(Tamb_C);
  Tjet = C2K(Tjet_C);
  A = pi/4*D^2; %Nozzle's area[m^2]
  C0 = co2_mfr/tot_mfr;

  [mu_co2, mu_air] = CalcViscousity(Tjet); % get the viscousity in the jet's temp
  mu_mass_avged = ( co2_mfr*mu_co2 + (tot_mfr-co2_mfr)*mu_air ) / tot_mfr;% Mass-averaged viscousity

  rho_air = rhoIdealGas(P0,Tjet,molar_mass_air);
  rho_co2 = rhoIdealGas(P0,Tjet,moalr_mass_co2);
  %%% I can take just mu_air, instead of mu_mass_avged. ~1.5% error
  rho_avged = C0*rho_co2 + (1-C0)*rho_air;%averaged density [kg/m^3]
  U0 = tot_mfr*1e-3/A/rho_avged; % Outlet velocity [m/s]
  Re = rho_avged*U0*D / mu_mass_avged; % Reynolds number in the orifice
%   Re_alternative = tot_mfr*1e-3/A*D/mu_mass_avged; % since m_dot = rho*U*A

  g = 9.8; %gravity [m/s^2]
  beta = 1/Tjet; % thermal expansion coefficient for ideal gas
  ni = mu_mass_avged/rho_avged; % kinematic viscosity [m^2/s]
  Gr = g*beta*(Tjet-Tamb)*D^3/ni^2;% Grashof number
  Ar = Gr/Re^2; %Archimedes number, to compare buoyant to inertial forces
  if Ar > 1e-4
    warning('Buoyancy might not be neglegable!');
  end

  %     alpha_air = 2.2e-5; %thermal diffusivity [m^2/s]  of air @ T=300K
  %     Pr = ni/alpha_jet % Prandtl number - ratio of momentum and thermal diffusivites
  
  Pr = 0.71;
  Pe = Re*Pr; % Peclet number - advection to diffusion ratio in forced convection


  % x transformation: x = x0_loc - x_im %
  % r (y) transformation: r = y_im - r0_loc
  x_im = (1:xNozzle_loc)';
  y_im = (1:N)';
  
  %TODO: should have a LUT where I enter Re and get fit parameters.

  if useCFD
    %% NOTE: it is assumed that the CFD image is greater than the measurement, and 
    %% for the CFD: Tjet = 600K, Tamb = 300K 
    CFD_PIPE_LENGTH_M = 0.11; % The pipe length in the CFD [m]
    % build the x & r vectors in [m].
    % NOTE: here x is decreasing
    x = (xNozzle_loc - x_im)./m2pix;
    r = (y_im - r0_loc)./m2pix;
    r = r(r>=0);
    
    %TODO: better to use spatial reference object and align this system to
    %the original image.
    
    % TODO: maybe need to sort by X
    X = data(:,1) - CFD_PIPE_LENGTH_M; % set the orifice as X=0
    R = data(:,2);
    T = data(:,3);
    T = (T-300)/(600-300)*(Tjet-Tamb) + Tamb; % convert to my case using temperature dimentioness variable 'theta'
    
    [X,I] = sort(X);
    R = R(I);
    T = T(I);
    R = ceil(m2pix * R(X>=0));
    T = T(X>=0);
    X = ceil(m2pix * X(X>=0));
    
    [TT] = griddata(R',X,T,r',x);
    TT = TT(:,2:end);
    if r0_loc <= N/2
      TT = [fliplr(TT(:,2:r0_loc+1)), TT];
    else
      TT = [nan(M,r0_loc-size(TT,2)+1), fliplr(TT(:,2:end)), TT];
    end
    CC = (TT- Tamb)/(Tjet-Tamb)*(C0-Camb) + Camb; % get the concentrations
    depths = [];
  else
    %% Get an estimate of the centerline T & C by eq. [1] in [Xu, 2002]:
    % Here, x0 is the location of virtual origin:
    % (Tc-Tamb)/(Tj-Tamb) = C1*D/(x-x0) and
    % C1=4.6, x0 = 2.6D @ Re~8e4  &  C1=4.6, x0 = 4.7D @ Re~1.6e4
    % ====> Tc = Tamb + C1*D/(x-x0)*(Tj-Tamb)
    if Re < 2e4
      x0 = 4.7*D;
    else
      x0 = 2.6*D;
    end
%     radii_expansion_rate = D/2/x0;

    % build the x & r vectors in [m].
    % NOTE: here x is decreasing
    x = x0 + (xNozzle_loc - x_im)./m2pix;
    r = (y_im - r0_loc)./m2pix;
    
    [xx,rr] = meshgrid(x,r);
    xx = xx';
    rr = rr';
    
    %% Calc the profiles based on "Enviromental Fluid Mechanics" ch.9.
    %% There, a universal opening angle of ~11.8 deg is used
    CC = Camb +  0.5*5*D./xx*(C0-Camb).*exp(-50*rr.^2./xx.^2); % 0.5 factor was added to fix orifice's value to C0
    
    %     C = 5*D./xx.*exp(-50*rr.^2./xx.^2);
    %     C = exp(-50*rr.^2./xx.^2);
    
    %     deltaT = 0.5*5*D./xx.*(Tjet-Tamb).*exp(-50*rr.^2./xx.^2);
    
    % "Convective Heat Transfer", 4th edition, p.411 :
    % T-T8 = (Tc-T8) * exp(-(r/bT)^2)
    % Where: bT ~ 0.127x
    %        Tc-T8 =~ 5.65(T0-T8)*D/x
    
    TT = Tamb + 0.5*5.65*D./xx.*(Tjet-Tamb).*exp(-62*rr.^2./xx.^2); %OLD
%     TT = Tamb + (xx-x0<3*D).*(Tjet-Tamb) +...
%       (xx-x0>=3*D).*0.5*5.65*D./xx.*(Tjet-Tamb).*exp(-62*rr.^2./xx.^2);
    % Thus, the virtual origin is -3.937D ~ -4D from the orifice.
    
    D_x = 0.127*(x-x(end))+D;% diameter of the jet as function of x
    %   cc = 5.65/2/x(end); % a constant
    %   T_cl_x = Tamb + (Tjet-Tamb)*0.5*5.65./x/cc; % centerline temperature as function of x
    %   C_cl_x = 0.5*C0*0.5*5*D./x; % centerline concentration as function of x
    depths = nan(M,N);
    for ix = 1:M %iterate over x:
      Dx = D_x(ix); % estimated jet diameter
      depths(ix, abs(r) <= Dx/2 ) = 2*sqrt((Dx/2)^2 - r(abs(r) <= Dx/2).^2);
    end
    
  end
  
end