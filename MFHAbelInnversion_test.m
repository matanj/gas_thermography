clear; close all; warning off; dbstop if error

% rng('default');
% rng(999,'twister'); % set the random generator and the seed
%% %%%%% Simulating arbitrary f(r) and reconstruct it: %%%%%%%%%%%%
%% Noise can be added to the sensor as well as to the initial distribution.
alpha = 0.1; % MFH parameter
R = 101; %Radius [pixels]
n = R;

r = (0:R-1)';
e_r = zeros(size(r));
X = r;
theta = linspace(0,2*pi,length(X));
[TH,rr] = meshgrid(theta,X);
f_no_noise = exp(-5*(rr/R).^2);
%  f_no_noise = 10./(rr+1);
 
sigma = 0*min(f_no_noise(:));
f = f_no_noise + sigma*randn(size(f_no_noise));

disp(['SNR (f(r)) = ',num2str(snr(f_no_noise, f-f_no_noise)),'[dB]']);

% Simulate the abel transform that reflects the sensor integration
% nature along LOS:
h=zeros(length(X),1);    % allocate result vector
for c=1:length(X)
  x=X(c);
  % evaluate Abel-transform equation (1)
  %         fun = @(r) (17.*(r./R).^4-32.*(r./R).^3+14.*(r./R).^2+1).*r./sqrt(r.^2 - x.^2);
  fun = @(r) (R*exp(-0.01*r.^2)).*r./sqrt(r.^2 - x.^2);
  fun = @(r) (f(c,1)).*r./sqrt(r.^2 - x.^2);
  h(c,1)=2*integral(fun,x,R);
end

I_no_noise = [flipud(h(:,1)) ; h(1:end,1)]; % For the MFH algorithm we need entire row
sigmaI = 0*min(I_no_noise(:));
I = I_no_noise + sigmaI*randn(size(I_no_noise));
disp(['SNR (I(r)) = ',num2str(snr(I_no_noise, I-I_no_noise)),'[dB]']);
% I = I - min(I(:));
if numel(I) ~= 2*R
  error('radius and size of data mismatch!');
end
%     figure; plot(X,h/max(h(:)),'r', X,f(:,1)/max(f(:,1)),'b'); title('Sensor simulated vs true');
tic
f_rec_NO = NestorOlsenAbelInversion( I(n+1:end), R);
toc
tic
[f_rec_MFH] = MFHAbelInversion( I, R, alpha );
toc
tic
f_rec_FE = abel_inversion(I(n+1:end), R, 10, 0, 0, 1);
toc
% tic
% f_rec_Efim = iterativeAbelInversion(I(n+1:end),R);
% toc

tic
% y = 0.0000001:R;
y=(0.0001:1:numel(I(n+1:end)))*(R/numel(I(n+1:end)));
r = y;
knots = 0.:.01*R:R;
f_rec_ASAI = ASAI( r, I(n+1:end)', y, knots );
toc



f_rec_MFH = 0*f_rec_MFH;

err_MFH = f_rec_MFH - f_no_noise(:,1);
err_MFH_norm2 = norm(err_MFH)
err_MFH_normInf = norm(err_MFH,inf)
err_FE = f_rec_FE - f_no_noise(:,1);
err_FE_norm2 = norm(err_FE)
err_FE_normInf = norm(err_FE,inf)
%% Plots:
figure; plot(I); title('Measurement I(r), with noise');
% figure; plot(r,f_no_noise(:,1)/max(f_no_noise(:,1)),'b', r,h/max(h),'k', r,e_r/max(e_r),'r');
figure; plot(r,f_no_noise(:,1),'b', r,f_rec_FE,'m', r,f_rec_MFH,'r', r, f_rec_NO,'g', r, f_rec_ASAI,'y');
title(['Comparison for R = ',num2str(n),' & SNR = ',num2str(snr(I_no_noise, I-I_no_noise)),'dB']);
legend('Original (with or w/o noise)','FE',['MFH, \alpha=',num2str(alpha)],'N-O', 'ASAI');
xlabel('X/R'); ylabel('Normalized');
% figure; plot(r,f_no_noise(:,1)/max(f_no_noise(:,1)),'b', r,f_rec_FE/max(f_rec_FE),'m', r,f_rec_MFH/max(f_rec_MFH),'r', r, f_rec_NO/max(f_rec_NO),'g');
% title(['Normalized - Comparison for R = ',num2str(n),' & SNR = ',num2str(snr(I_no_noise, I-I_no_noise)),'dB']);
% legend('Original (with or w/o noise)','FE',['MFH, \alpha=',num2str(alpha)],'N-O');