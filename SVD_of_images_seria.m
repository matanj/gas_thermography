%% images seria representation in SVD:
%% Principal component thermography for flaw contrast enhancement and flaw depth characterisation in composite structures
%% N. Rajic 2002

%%%%%%%%% Main function %%%%%%%%
%% PCA is sensative to scaling. 
function SVD_of_images_seria
clc; close all;
dbstop if error;

SUBTRACT_BG = true;
defaultImages = 'Z:\Matan\Gas measurements\15_12_24\BB1200_200C_1.ptw';

path(path,'Z:\Matan\');
progressbar('stuff');

%% load data series:
[file, filePath] = uigetfile({'*.ptw','FLIR ptw files'},'Select ptw file',defaultImages);
if isempty(file)
  error('invalid file, exiting');
end
imageFileName = [filePath file];
ptwInfo.m_filename = imageFileName;
ptwInfo = FLIR_PTWFileInfo(ptwInfo);
nFrames2take = min(ptwInfo.m_nframes, 20);

frameHolder(ptwInfo.m_rows,ptwInfo.m_cols,nFrames2take)=0; %Preallocation [rows, cols, time]
for ind1 = ptwInfo.m_firstframe: nFrames2take % ptwInfo.m_firstframe === 1
  [frameHolder(:,:,ind1),~,~] = FLIR_PTWGetFrame(ptwInfo.m_filename, ind1-1);
  progressbar(ind1/3/nFrames2take);
end

if SUBTRACT_BG
  [file, filePath] = uigetfile({'*.ptw','FLIR ptw files'},'Select BG ptw file',defaultImages);
  if isempty(file)
    error('invalid file, exiting');
  end
  imageFileName = [filePath file];
  ptwInfo.m_filename = imageFileName;
  ptwInfo = FLIR_PTWFileInfo(ptwInfo);
  frameHolder2(ptwInfo.m_rows,ptwInfo.m_cols,nFrames2take)=0; %Preallocation [rows, cols, time]
  for ind1 = ptwInfo.m_firstframe: nFrames2take % ptwInfo.m_firstframe === 1
    [frameHolder2(:,:,ind1),~,~] = FLIR_PTWGetFrame(ptwInfo.m_filename, ind1-1);
    progressbar(1/3 + ind1/3/nFrames2take);
  end
  frameHolder = bsxfun(@minus, double(frameHolder), frameHolder2);
end

%% rearrange images from 3D matrix to 2D: [M, N, k] => [M*N, k]
dataSize = size(frameHolder);
imSeries = reshape(frameHolder, [dataSize(1)*dataSize(2), dataSize(3)]);
%% Standartization procedure, to remove effects of detector pixel-to-pixel variations:
im = detrend(imSeries, 0); % subtract the mean columnwise

%%method 1 for calculating EOF:
%Use F = detrend(imSeries,0);
%R = F'*F; %better R = cov(F);% Covariance matrix
%[C, L] = eig(R);
%PCi = F*C(:,i);

%%Method 2: 
% F = detrend(imSeries,0);
% [C, lambda, CC] = svd(F);
% PCi = F*CC(:,i);


%diag(L)/trace(L); %vector of 'amount of variance explained' for each
%eigenvalue



%% The standartization also comrpresses/expands fluctuations in all dims to unit variance,
%% Thus only mean subtraction is done.
%%Mean subtraction (mean centering) is a must:
%%https://en.wikipedia.org/wiki/Principal_component_analysis

%% Calc SVD:
[U, S, V] = svd(im);
progressbar(1);
s = diag(S); figure; semilogy(s)
% first = U(:,1)*S(1,1)*V(:,1)';  figure; imagesc(first); axis image; colorbar;
% second = U(:,2)*S(2,2)*V(:,2)'; figure; imagesc(second); axis image; colorbar;
%% Get the EOF: reform each vector in U - reverese of the above raster-like condensation:
EOFSet = reshape(U, dataSize(1), dataSize(2), []);
th = s(4);

imm = reshape(im, dataSize(1), dataSize(2), []);
imm = mat2gray(imm);
[~,~,~, imTrunc] = truncSVD(imm(:,:,1),th);
imm = uint8(255*imm(:,:,1));

n = 64;
imm = (imm/n)*n;

imTrunc = uint8(255*mat2gray(imTrunc));
figure; imshow(imm(:,:,1)); axis image; colorbar; impixelinfo;
figure; imagesc(imTrunc); axis image; colorbar

% %% DO FFT2, for comparison:
% Y = fftshift(fft2(EOFSet(:,:,1)));
% figure; imshow(mat2gray(20*log10(abs(Y(1:100,:) + 1))), []); colorbar; title('magnitude');
% figure; imshow(mat2gray(angle(Y(1:100,:))), []); colorbar; title('phase');
end

%%%%%%%%% SVD lossy compression %%%%%%%%%
function [U, S, V, imTrunc] = truncSVD(im,th)
% Returns the SVD trunced image and its associated matrices.
% Use with th=0 is equivalet to svd(im).
  if nargin < 2 || th < 0
    error('invalid arg');
  end
  [U,S,V] = svd(im);
  ss = diag(S);
  ind = find(ss<th, 1, 'first') - 1;
  U = U(:,1:ind);
  S = S(1:ind,1:ind);
  V = V(:,1:ind);
  imTrunc = U*S*V';
end