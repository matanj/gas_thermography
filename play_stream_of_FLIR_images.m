function play_stream_of_FLIR_images(ims, fps)
if nargin < 2
  fps = 30;
end
figure; hf = imagesc(ims(:,:,1));
tic
for iF = 1 : size(ims,3)
  hf.CData = ims(:,:,iF);
  drawnow;
  java.lang.Thread.sleep((1/fps)*1000); % Better than pause(1/fps);
end
toc
end
  