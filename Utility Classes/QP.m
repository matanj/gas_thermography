classdef (Abstract = true) QP
  %QP A utility class for Quick Plotting of common things
  
  methods (Abstract = false, Access = public, Static = true)
    % Just plot an image
    function varargout = im(data, ax)
      if nargin < 2 % create a new figure if needed
        hFig = figure(); maximize(); hFig.Visible = 'off'; hIm = imagesc(data);
      else
        hIm = imagesc(ax,data);
      end
      axis image; colormap(viridis(200)); colorbar;
      %% Outputs:
      if nargout >= 1
        varargout{1} = hIm;
      end
      %% Show Figure:
      hFig.Visible = 'on';
    end
    
    % Plot an image with no outliers
    function varargout = imno(data)            
      hIm = QP.im(removeOutliers(data));      
      function IM = removeOutliers(IM, tolspec)
        SZ = size(IM);   
        MAX_FRAME_SZ = [512 640];
        if nargin < 2
          tolspec = 0.005 / prod(size(IM)./MAX_FRAME_SZ);  
        end
        if numel(tolspec) == 1
          tolspec = [0+tolspec, 100-tolspec];
        end

        % Turn data into rows:
        switch numel(SZ)
          case 3
            IM = reshape(IM,SZ(3),[]);
          case 2
            IM = IM(:).';
        end
        pct = prctile(IM,tolspec,2);
        IM = ( IM .* (0./(bsxfun(@ge,IM,pct(:,1,:)) & bsxfun(@le,IM,pct(:,2,:)))+1));
        IM = inpaint_nans(reshape(IM,SZ),3);

        % Detect salt & pepper (requires the computer vision toolbox):
        IM(spNoiseInds(IM,SZ)) = NaN;
        IM = inpaint_nans(IM,3); %inpaint again

        function noiseIndices = spNoiseInds(img,SZ)
          hf = detectHarrisFeatures(img); % http://chat.stackoverflow.com/transcript/message/36449591#36449591
          noiseIndices = sub2ind(SZ, round(hf.Location(:,2)), round(hf.Location(:,1)));
        end
      end
      %% Outputs:
      if nargout >= 1
        varargout{1} = hIm;
      end
    end
    
    % Plot an image with a linked histogram
    function varargout = imh(data)            
      hFig = figure(); maximize(); hFig.Visible = 'off';
      hIm = QP.im(data, subplot(4,1,1:3));
      hHist = histogram(subplot(4,1,4), data(:));
      colormap(viridis(hHist.NumBins)); cMap = colormap(hFig);
      hFig.UserData = datacursormode(hFig);
      set(hFig.UserData,'Enable','on','UpdateFcn',@customMonitorDatatip);      
      %% Outputs:
      if nargout >= 1
        varargout{1} = hIm;
      end
      if nargout >= 2
        varargout{2} = hHist;
      end
      %% Show Figure:
      hFig.Visible = 'on';
      %% Nested functions:
      function output_txt = customMonitorDatatip(~,event_obj)
        % Display the position of the data cursor
        % obj          Currently not used (empty)
        % event_obj    Handle to event object
        % output_txt   Data cursor text string (string or cell array of strings).
        pos = get(event_obj,'Position');

        if isa(event_obj.Target,'matlab.graphics.primitive.Image')  
          % This means the datatip was placed on the image and not the histogram, so we also 
          % create a point on the histogram.  
          datatipSuspects = [findall(ancestor(hIm,  'Axes'),'Type','hggroup');...
                             findall(ancestor(hHist,'Axes'),'Type','hggroup')];          
          if numel(datatipSuspects) >= 2
            delete(datatipSuspects([false; arrayfun(@(x)isa(x,'matlab.graphics.shape.internal.PointDataTip'), datatipSuspects(2:end))]));
          end
          % Clear current data cursors:
          %   handles.Extras.dcm_obj.removeAllDataCursors;
          val = event_obj.Target.CData(sub2ind(size(event_obj.Target.CData),pos(2),pos(1)));                   
          output_txt = {['[X,Y]: '  mat2str(pos)];...
                        ['Value: ', num2str(val,5)]};
          % Also make a datatip on the histogram:
          bin_pos = discretize(val,hHist.BinEdges);
          hDT = createDatatip(hFig.UserData, hHist);
          hDT.Cursor.DataIndex = bin_pos;
        else % (The histogram was clicked)
          cm = cMap;
          if hHist.NumBins == size(cm,1) && all(abs(hHist.BinLimits - hIm.Parent.CLim) < 1E-6)
            cmInd = find(hHist.BinEdges < pos(1),1,'last');
          else
            % Find the data limits of the clicked bin:
            binLim = hHist.BinEdges(find(hHist.BinEdges < pos(1),1,'last')+[0,1]);
            % Paint the relevant pixels in the image red:
            cMapping = linspace(hIm.Parent.CLim(1), hIm.Parent.CLim(2), size(cMap,1));                    
            [~,cmInd] = ismembertol(binLim,cMapping, 0.99*diff(cMapping(1:2)));
            cmInd = min(cmInd):max(cmInd);
          end
          if all(cmInd ~= 0)
            cm(cmInd,:) = repmat([1,0,0], [nnz(cmInd),1]);
            colormap(hFig,cm);
          end
        %   obj.Draggable = 'off';
          hist_output_text = {['Value: ', num2str(pos(1),5)],...
                              ['Percentage of values in bin: ' num2str(100*pos(2)/sum(hHist.Values)) '%']};
          % Just assign the histogram datatip text to output:
          output_txt = hist_output_text;
        end
      end
    end
    
    % Add a colorbar to the current image
    function varargout = cb(ax)
      if nargin == 0
        ax = gca;
      end
      if metaclass(get(ax,'Children')) == ?matlab.graphics.primitive.Image
        % Means this is a valid image
        hCb = colorbar(ax);
      else %typically, ?matlab.graphics.chart.primitive.Histogram
        p = findobj(gcf,'type','Image');
        hCb = colorbar(ancestor(p(1),'Axes'));          
      end
      %% Outputs:
      if nargout >= 1
        varargout{1} = hCb;
      end
    end

    
  end
  
end

