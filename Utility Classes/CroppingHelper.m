classdef CroppingHelper

%     properties (SetAccess = private, GetAccess = public)
%         lastOffset = [0,0];
%     end

    methods (Access = public, Static = true)
            
        function im = findCircle(IM,varargin)
            %TODO: input parser
            % This method searches for a circle in the first image within
            % the passed-in cell, and crops all images in the cell
            % according to the found position.
            switch nargin
                case 2
                    dst = varargin{1};
                otherwise
                    display(['nargin = ' num2str(nargin) '. Aborting...']);
                    return
            end
            
            %get radius range from distance
            radMat = CroppingHelper.findRadiiFromDst(dst);
            %get a single valid circle
            [circen, cirrad] = CroppingHelper.findSingleCenter(IM,radMat);
            % cut a bounding square around the circle
            im = CroppingHelper.getBoxedCircle(IM, cirrad, circen);
        end
        
        % This is a replacement\workaround for a "Public Static" field
        function val = lastCirRad(newval)         
            %% Get\Set the value:
            persistent currentval;
            if nargin >= 1
                %% Making sure the variable can only be set from within the class:
                tmp_stack = dbstack;  % < + \/ = Crazy hack :) 
                if length(tmp_stack)==1 || ~strfind(tmp_stack(2).name,'CroppingHelper.')
                    warning('Trying to set a static property from outside the class.');
                    return
                else
                    currentval = newval;    
                end                 
            end
            val = currentval;
        end
        
        %TODO: redo with SetAccess=private, GetAccess=public
        function val = lastCirCen(newval)         
            %% Get\Set the value:
            persistent currentval;
            if nargin >= 1
                %% Making sure the variable can only be set from within the class:
                tmp_stack = dbstack;  % < + \/ = Crazy hack :) 
                if length(tmp_stack)==1 || ~strfind(tmp_stack(2).name,'CroppingHelper.')
                    warning('Trying to set a static property from outside the class.');
                    return
                else
                    currentval = newval;    
                end                 
            end
            val = currentval;
        end

        function val = lastOffset(newval)         
            %% Get\Set the value:
            persistent currentval;
            if nargin >= 1
                %% Making sure the variable can only be set from within the class:
                tmp_stack = dbstack;  % < + \/ = Crazy hack :) 
                if length(tmp_stack)==1 || ~strfind(tmp_stack(2).name,'CroppingHelper.')
                    warning('Trying to set a static property from outside the class.');
                    return
                else
                    currentval = newval;    
                end                 
            end
            val = currentval;
        end        
        
        function bounding_sq = getBoundingSquare(cirrad,circen,sf)
           bounding_sq = [round(circen{1}(2)-cirrad{1}*sf),...
                          round(circen{1}(2)+cirrad{1}*sf),...
                          round(circen{1}(1)-cirrad{1}*sf),...
                          round(circen{1}(1)+cirrad{1}*sf)];
           bounding_sq(bounding_sq < 1) = 1; %fix for nonpositive
           if (bounding_sq(2) - bounding_sq(1) ~= ...
               bounding_sq(4) - bounding_sq(3)) %fix for non-square
                bounding_sq(4) = bounding_sq(3) + bounding_sq(2) - bounding_sq(1);
           end
           CroppingHelper.lastCirRad(cirrad{1});
           CroppingHelper.lastCirCen(circen{1});
        end
        
        function im = nanCrop(IM,varargin)
            % IM is assumed to be square!
            % safety_factor <= 1 & is assumd to be included in cirrad.
            imSz = size(IM{1});
            switch nargin
                case 1
                    cirrad = CroppingHelper.lastCirRad;
                    circen = CroppingHelper.lastCirCen;
                case 2
                    cirrad = varargin{1};
                    circen = imSz/2;
                case 3
                    cirrad = varargin{1};
                    circen = varargin{2};
                otherwise
                    % ???
            end
            im = IM; nIms = numel(IM);
            upto = @(x)(1:x)';
            sqrRad = cirrad.^2;
            [XX,YY] = meshgrid(upto(imSz(1)),upto(imSz(2)));

            in_radius = ((XX-circen(1)).^2 + (YY-circen(2)).^2) < sqrRad;
            for ind1=1:nIms
                im{ind1}(in_radius==0)=NaN;
            end
        end
        
        function imCell = nanCropWithOffset(IMCell,offset,varargin)
            % Similar to nanCrop but the cropping center is offset by 
            % offset = [x y]
            CroppingHelper.lastOffset(round(size(IMCell{1})/2 - offset));            
            imCell = ...
               cellfun(@(x)circshift(x,CroppingHelper.lastOffset),... reverse circshift 
                 CroppingHelper.nanCrop(...of the cropped image
                   cellfun(@(x)circshift(x,-CroppingHelper.lastOffset),...of the circshifted
                   IMCell,'UniformOutput',false),...original image 
                   varargin{:}),...with the same inputs
               'UniformOutput',false);  
            %% Removing separated blobs
            out = nan(size(imCell{1}));            
            % Solution 1 by rayryeng: http://chat.stackoverflow.com/transcript/message/24632096#24632096
            %{
                % Apply regionprops
                s = regionprops(~isnan(imCell{1}), 'Area', 'PixelIdxList'); 
                % Find the region with the max area
                [~,id] = max([s.Area]);
                % Create an output mask with pixels from largest area
                out(s(id).PixelIdxList) = 1;
            %}
            % Solution 2 by Jonas: http://chat.stackoverflow.com/transcript/message/24638570#24638570
            % Create a connected component labeled image:
                tf = ~isnan(imCell{1});
                lblImg = bwlabel(tf);
                % Keep the most frequent value only:
                out(lblImg == mode(lblImg(lblImg>0))) = 1;            
            % Remove out extraneous closing areas by ANDing with inverted image
            % then reinvert to bring back to original label scheme
            imCell = {(imCell{1} .* out)};            
        end
    end %public static methods
    
    methods (Access = private, Static = true)
        function radMat = findRadiiFromDst(dst)
            safetyFactor = 1.00;
            radMat = CroppingHelper.findRadiiFromDstWithSF(dst,safetyFactor);
        end
        
        function radMat = findRadiiFromDstWithSF(dst,sf)
            % This method returns the expected disc radius based on
            % calibration of the camera's magnificaiton.
            radmax =  ceil(25374*dst^-0.964*sf);
            radmin = floor(25374*dst^-0.964/sf)-1;
            radMat = [radmin, radmax];
        end
        
        function [circen, cirrad] = findSingleCenter(IM,radMat)           
            persistent depth; if isempty(depth), depth = 000; end
            persistent sense; if isempty(sense), sense = .92; end
            % This method returns a single valid found circle.
            nIms = numel(IM);
            circen{nIms} = []; %Preallocation
            cirrad{nIms} = []; %Preallocation
            for ind1 = 1:nIms
                %%TODO: see what greythresh does exactly...
                [circen{ind1}, cirrad{ind1}] = imfindcircles(IM{ind1},radMat,'Sensitivity',sense); 
            end

            if any(cellfun(@length,cirrad)>1)
                warning(['Too many circles found. Please make sure that MIN_RADIUS '... 
                         'and MIN_RADIUS are defined correctly. More than 2 centers'...
                         ' may be considered problematic. Currently found: ' ...
                         num2str(max(cellfun(@length,cirrad))) ' centers.']);
                for ind1=1:nIms
                    cirrad{ind1} = cirrad{ind1}(1);
                    circen{ind1} = circen{ind1}(1,:);
                end
            end
            if any(cellfun(@isempty,cirrad))
                if depth <= 3
                    warning(['No circles found using the current parameters,'...
                             'retrying with larger sensitivity.']); 
                    % Note: sensitivity is increased via 2 mechanisms:
                    % 1. Larger radius bounds
                    % 2. Larger sensitivity parameter
                    depth = depth+1; sense = sense + 0.02;
%                     CroppingHelper.findSingleCenter(IM,radMat); %DEBUG
                    [circen, cirrad] = CroppingHelper. ...
                        findSingleCenter(IM,round([radMat(1)*0.98 radMat(2)*1.02]));
                else
                    depth = 0; sense = .92;
                    error('No circles found.');                
                end
            else
            	%Updating "static" fields:
                CroppingHelper.lastCirCen(circen{1});
                CroppingHelper.lastCirRad(cirrad{1});
            end
        end
                
        function im = getBoxedCircle(IM, cirrad, circen)
            %%%%%%%TODO: make CROP_SAFETY_FACTOR an input
            % This method returns a square image cropped from IM bounding 
            % the circle defined by (cirrad, circen).
            
            CROP_SAFETY_FACTOR = 1; %can be >1 if the user wants to leave 
            % some extra pixels, <1 if the user wants to remove the edges 
            % (including some non-wanted effects) or =1 to keep the 
            % passed-on radius setting.
                        
            nIms = numel(IM); im{nIms} = []; %Preallocation
            for ind1=1:nIms  
                b_sq = CroppingHelper.getBoundingSquare(cirrad,circen,CROP_SAFETY_FACTOR);
                im{ind1} = IM{ind1}(b_sq(1):b_sq(2),b_sq(3):b_sq(4));
            end
            
            if diff(imSz) %no change will result in 0.
               warning('Image is not square, cropping to the smaller dimension...');
               mindim = min(imSz);
               for ind1=1:nIms
                   im{ind1} = im{ind1}(upto(mindim),upto(mindim));
               end
               % TODO: Update lastdim
            end
        end  
    end %private static methods
    
end %class