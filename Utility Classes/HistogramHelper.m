classdef HistogramHelper
% DESCRIPTION GOES HERE  
  properties (Constant = true, Access = private)
    DEFAULT_FIT_ORDER = 3;
    ALLOWED_FIT_ORDER = [2,3];
    DEFAULT_CUTOFF_PERCENTILE = 98;
  end
  
  methods (Access = public, Static = true)
    
    function peak = getFittedGauss3Peak(data,cut_on_val,cut_off_val,nBins,fitOrder,allowNegativeData)
      if ~nargin, peak = NaN; return; end %nargin == 0 is invalid.
      if isequal(data, round(data)); intData = true; else intData = false; end
      
      % Create a "clean" data vector:
      data_vec = data(:);
      if nargin < 6 || ~allowNegativeData
        data_vec(data_vec<0) = NaN;
      end
      
      if nargin == 1 || isempty(cut_on_val)
        cut_on_val = min(data_vec);
      end
      if nargin < 3 || isempty(cut_off_val) % 2 or 1
        if intData
          cut_off_val = ceil(prctile(data_vec,HistogramHelper.DEFAULT_CUTOFF_PERCENTILE));
        else
          cut_off_val = prctile(data_vec,HistogramHelper.DEFAULT_CUTOFF_PERCENTILE);
        end
      end
      if nargin < 4 || isempty(nBins)%3, 2 or 1
        if isinteger(data) || isequal(data,floor(data))
          nBins = cut_off_val - cut_on_val + 1;
        else
          nBins = 2*calcnbins(data_vec(data_vec >= cut_on_val & data_vec <= cut_off_val),'fd');
        end
      end
      if nargin < 5 || isempty(fitOrder) || ~any(fitOrder == HistogramHelper.ALLOWED_FIT_ORDER)
        fitOrder = HistogramHelper.DEFAULT_FIT_ORDER;
      end
      % Compute the histogram of the data:
      [N,e] = histcounts(data_vec(data_vec >= cut_on_val & data_vec <= cut_off_val), nBins);
      %DEBUG: figure(); histogram(data_vec(data_vec >= cut_on_val & data_vec <= cut_off_val), nBins);
      % Fit the histogram with a model and find the peak:
      gauss_fit = fit((e(1:end-1)+0.5*(e(2)-e(1)))',N',['gauss' num2str(fitOrder)]);
      %DEBUG: hold on; plot(gauss_fit,'fit');
      peak = fminsearch(@(x)-gauss_fit(x),min(e(N==max(N))));
    end       
    
    function V = runLengthDecode(runLengths, values)
    if nargin<2
      values = 1:numel(runLengths);
    end
      V = repelem(values, runLengths);
    end
    
  end 
  
end