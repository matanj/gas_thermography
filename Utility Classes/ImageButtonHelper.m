classdef ImageButtonHelper < handle
  %IMAGEBUTTONHELPER Helper class for putting images into UI controls.
  %   For more info see: http://undocumentedmatlab.com/blog/images-in-matlab-uicontrols-and-labels
  
  properties (Access = private, Constant = true)
    BASE_HTML_START = '<html><img src="';
    BASE_HTML_END = '"/></html>';
    
    FILE_URI_HEADER = 'file:/';
  end
  
  methods (Static = true)
    function newStr = rotateSlashes(oldStr)
      newStr = strrep(oldStr,'\','/');
    end
  
    function setButtonImageFromFile(hButton,imgPath)
      hButton.String = [ImageButtonHelper.BASE_HTML_START ImageButtonHelper.FILE_URI_HEADER ...
        ImageButtonHelper.rotateSlashes(imgPath) ImageButtonHelper.BASE_HTML_END];
    end
    
    function setButtonImageFromURL(hButton,imgURL)
      hButton.String = [ImageButtonHelper.BASE_HTML_START imgURL ImageButtonHelper.BASE_HTML_END];
    end
  end
  
end

