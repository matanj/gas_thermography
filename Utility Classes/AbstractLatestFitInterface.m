classdef (Abstract) AbstractLatestFitInterface < handle
% DESCRIPTION GOES HERE
  properties (Abstract = true, Access = protected, Constant = true)
    LATEST_FITS_FILENAME;  
  end
  
  properties (Abstract = true, Access = protected)
    fitCell
  end
  
  methods (Abstract = true, Access = public)    
    % It is assumed at this stage that the sfit objects are stored in the cell array
    % "fitCell" after the appropriate loading/processing was done in the constructor.
    rHandle = getT2R(helperObj,filterNum)
    pHandle = getP(helperObj,filterNum)
  end
  
end