function [G,T,CWL,IT,isBG,name] = parseFilename(filename)
% TODO: Accept varargin (cell of parameter names etc.), return varargout
% This function decodes measurement information from the filename.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Desired Filenames: G001_T250_F3.453_IT549.8_BG_Geometry.ptw %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if ischar(filename) % Case of single input string
  %% Split
  s = strsplit(filename, '_');
  %% Group
  G = strsplit(s{1},'G');
  G = str2double(G{2});
  %% Temperature
  T = strsplit(s{2},'T');
  T = str2double(T{2});
  %% Filter CWL
  CWL = strsplit(s{3},'F');
  CWL = str2double(CWL{2});
  %% IT
  IT = strsplit(s{4},'IT');
  IT = str2double(IT{2});
  %% isBG
  isBG = strcmp(s{5},'BG');
  %% Custom string (experiment name)
  [~, name, ~] = fileparts(s{6});  
else
  %% Split
  s = regexp(filename, '_','split'); s = vertcat(s{:});
  %% Group
  G = regexp(s(:,1),'G','split'); G = vertcat(G{:});
  G = str2double(G(:,2));
  %% Temperature
  T = regexp(s(:,2),'T','split'); T = vertcat(T{:});
  T = str2double(T(:,2));
  %% Filter CWL
  CWL = regexp(s(:,3),'F','split'); CWL = vertcat(CWL{:});
  CWL = str2double(CWL(:,2));
  %% IT
  IT = regexp(s(:,4),'IT','split'); IT = vertcat(IT{:});
  IT = str2double(IT(:,2));
  %% isBG
  isBG = strcmp(s(:,5),'BG') | strcmp(s(:,5),'DF') ;
  %% Custom string (experiment name)
  [~, name, ~] = cellfun(@fileparts,s(:,6),'UniformOutput',false);
end

