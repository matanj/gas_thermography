classdef ImFH < handle
    %IMFH The Image Filtering Helper
    
    properties (Access = private)
        curr_img
    end
    
    properties (Access = private, Constant = true)
        DEFAULT_H = 3;
        DEFAULT_SEARCH_WINDOW_SZ = 7;
        DEFAULT_TEMPLATE_WINDOW_SZ = 21
    end
    
    methods (Access = public)
        
        function obj = ImFH(img) %constructor
            obj.curr_img = img;
            ImFH.toGPU(obj);
        end
        
        function img = getImg(obj)
            img = obj.curr_img;
        end
        
        function plot(obj)
            figure(); 
            imagesc(obj.curr_img);
            colormap(gray(200));
            pbaspect([fliplr(size(obj.curr_img)) 1]);
        end
        
        % Apply an "oil paint" (MCDL) filter
        function obj = oil(obj,windowSz)            
          try
            tmp = double(gather(obj.curr_img));
            obj.curr_img = uint8(nlfilter(tmp,windowSz*[1 1],@ImFH.getMCDL));
            ImFH.toGPU(obj);          
          catch
              warning('Unable to apply "oil" filter.');
          end
        end
        
        % Apply a "median" filter
        function obj = med(obj,windowSz)            
            obj.curr_img = medfilt2(obj.curr_img,windowSz*[1 1]);
            ImFH.toGPU(obj);                      
        end
        
        % Apply a "gaussian blur" Filter
        function obj = gblur(obj,windowSz,sig)
            G = fspecial('gaussian',windowSz*[1 1],sig);
            obj.curr_img = imfilter(obj.curr_img,G,'same');
%             ImFH.toGPU(obj);                      
        end
        
        % Applies a "fast nonlocal means" filter
        function obj = fnlm(obj,varargin)
            if nargin==4 && isnumeric(varargin{2})
                h = varargin{1}; % amount of "denoising"
                t = varargin{2}; % window size
                s = varargin{3}; % size of region where to search
            else
                p = inputParser;
                p.addParameter('h',ImFH.DEFAULT_H,...
                                @(x)validateattributes(x,{'numeric'},{'positive'}));
                p.addParameter('templateWindowSize',ImFH.DEFAULT_TEMPLATE_WINDOW_SZ,...
                                @(x)validateattributes(x,{'numeric'},{'odd','positive'}));
                p.addParameter('searchWindowSize',ImFH.DEFAULT_SEARCH_WINDOW_SZ,...
                                @(x)validateattributes(x,{'numeric'},{'positive'}));
                p.parse(varargin{:}); inp = p.Results;
                
                h = inp.h;
                t = inp.templateWindowSize;
                s = inp.searchWindowSize;
            end
            
            if isa(gather(obj.curr_img),'double')
                warning('''double'' type is unsupported. Please use either uint8 or uint16.');
            else                
                obj.curr_img = cv.cuda.fastNlMeansDenoising(gather(obj.curr_img),...
                    'h',h,...
                    'templateWindowSize',t,...
                    'searchWindowSize',s);
            end

            ImFH.toGPU(obj);          
        end
        
        % Apply a K-nearest-neighbours filter
        function obj = knn(obj,varargin)            
            warning('the knn filter is currently unimplemented');
        end
    end
    
    methods (Access = public, Static = true)
        function out = getMCDL(im)
            [N,X] = hist(im(:),double(calcnbins(gather(im(~isnan(im(:)))),'fd')));
            out = mean(X(N==max(N)));
        end    
    end
    
    methods (Access = private, Static = true)
        function toGPU(obj)
            try
                obj.curr_img = gpuArray(obj.curr_img);
            catch
                warning('Unable to convert image into a gpuArray.');
            end
        end

        function out = double_TO_RGB_uint8(img)
        %   assert(isa(img, 'double'));
            R = floor(img);
            G = floor((img-R)*256);
            B = floor(((img-R)*256-G)*256);        
            out = uint8(cat(3,R,G,B));
        end
    
        function out = RGB_uint8_TO_double(img)
            out = double(img(:,:,1)) + double(img(:,:,2))/2^8 + ...
                  double(img(:,:,3))/2^16;
        end
            
    end

end

