classdef BiasFrameHelper < handle & AbstractLatestBiasFrameInterface
  
  properties (Access = protected, Constant = true)
    LATEST_FRAME_FILENAME = 'BiasFrame_Reconstr_Nov2016'; % Reconstructed
    FULL_FRAME_WIDTH = 640;
    FULL_FRAME_HEIGHT = 512;
  end
  
  properties (Access = protected)
    biasFrame = []; % The bias frame, in [DL]
  end
  
  methods (Access = public)
    % This constructor is has to be modified according to the structure of the data such
    % that in the end fitCell will contain an sfit object.
    function helperObj = BiasFrameHelper()
      tmp = load(BiasFrameHelper.LATEST_FRAME_FILENAME);
      if numel(fieldnames(tmp)) == 1
        helperObj.biasFrame = tmp.(char(fieldnames(tmp)));
      else
        throw(IllegalArgumentException('BiasFrameHelper:badBiasFrameFile',...
          'The bias frame file contains more than one field - that''s big nono!'));
      end
    end
    
    % It is assumed at this stage that:
    % 1) "fitCell" contains sfit objects (after the appropriate loading/processing was done in the constructor.)
    % 2) Each fit contains parameters named A, B, C that relate T to R
    function bias_frame = getBF(helperObj)
      bias_frame = helperObj.biasFrame;
    end
    
    function [ croppedBF ] = cropBFToFrame(helperObj, imInfo)
      % Crops the full-size bias frame according to the location of the given
      % PTW image as specified by its 'imInfo'.
      if nargin ~= 2 || ~isstruct(imInfo)
        throw(IllegalArgumentException('BiasFrameHelper:cropBFToFrame',...
          ['The number of inputs is incorrect! Should be 2, found: ' num2str(nargin) '.']));
      end
      % Get the location of the image in the camera's 512x640 coordinates
      crop_rect = imInfo.m_cliprect + [1 1 -1 -1]; % Altair-to-MATLAB indexing difference
      croppedBF = helperObj.biasFrame(crop_rect(2):crop_rect(2)+crop_rect(4),...
                                      crop_rect(1):crop_rect(1)+crop_rect(3));
    end
    
    function [ fncBF ] = getFlippedandCroppedBF(helperObj, img, imInfo)
      % figure(); imagesc(img); axis image;
      bigIm = NaN(helperObj.FULL_FRAME_HEIGHT, helperObj.FULL_FRAME_WIDTH);
      cliprect_FIX = [640-imInfo.m_cliprect(1)-imInfo.m_cliprect(3), imInfo.m_cliprect(2:4)];
      crop_rect = cliprect_FIX + [1 1 -1 -1]; % Altair-to-MATLAB indexing difference
      bigIm(crop_rect(2):crop_rect(2)+crop_rect(4), ...
            crop_rect(1):crop_rect(1)+crop_rect(3) ) = img;
      % figure(); imagesc(bigIm); axis image;
      fncBF = helperObj.findFlipState(bigIm, helperObj.biasFrame);
      if any(fncBF(:)==0) % SHOULD NOT HAVE ZEROS!!! Exactly-zeros indicate "lost" negative values.
        LogHelper.w(LogMessage('Zero values found in BF! Negative offsets will not be corrected!'));
      end
      % figure(); imagesc(fncBF); axis image;
      fncBF = fncBF(crop_rect(2):crop_rect(2)+crop_rect(4), ...
                    crop_rect(1):crop_rect(1)+crop_rect(3) );
      % figure(); imagesc(double(img) - fncBF); axis image;
    end

  end
  
  methods (Access = public, Static = true)
    
    function [correctFlip,flipState] = findFlipState(baseline,mask)
      % FINDFLIPSTATE Find the correct image flip setting (none, lr, ud, lr+ud)
      %   findFlipState works by computing the image gradient along the horizontal
      %   direction and choosing the rotation option that results in the smoothest
      %   image, that is, having the smallest gradient.
      % INPUTS:
      % baseline - typically the object image.
      % mask - the element whose rotated version we output. Typically the bias frame.
      % By Iliya, 19/07/2016
      
      S(4) = BiasFrameHelper.sumN(abs(imgradientxy(baseline-rot90(mask,2))));
      S(3) = BiasFrameHelper.sumN(abs(imgradientxy(baseline-flipud(mask))));
      S(2) = BiasFrameHelper.sumN(abs(imgradientxy(baseline-fliplr(mask))));
      S(1) = BiasFrameHelper.sumN(abs(imgradientxy(baseline-mask)));
      
      % states = cat(3,baseline-mask, baseline-fliplr(mask), baseline-flipud(mask), baseline-rot90(mask,2));
      [~,flipState] = min(S);
      
      switch flipState
        case 1
          correctFlip = mask;
        case 2
          correctFlip = fliplr(mask);
        case 3
          correctFlip = flipud(mask);
        case 4
          correctFlip = rot90(mask,2);
      end      
    end
    
    function [ croppedBF ] = alignAndCropBF(BF, IM, imInfo)
      % This function finds the flip state and returns a cropped BF
      if nargin ~= 3 || isempty(BF) || isempty(IM) || isempty(imInfo)
        throw(IllegalArgumentException('BiasFrameHelper:alignAndCropBF',...
          ['Some inputs are missing or undefined! Should be 3, found: ' num2str(nargin) '.']));
      end
      if any(size(IM) > size(BF))
        throw(IllegalArgumentException('BiasFrameHelper:alignAndCropBF',...
          'The image cannot be larger than the bias frame in any direction!'));
      end
      if isequal(size(IM),size(BF))
        % fallback to findFlipState:
        croppedBF = BiasFrameHelper.findFlipState(IM,BF);
      else
        crop_rect = imInfo.m_cliprect + [1 1 -1 -1];
        fullsizeIm = NaN(size(BF));
        fullsizeIm(crop_rect(2):crop_rect(2)+crop_rect(4), ...
                   crop_rect(1):crop_rect(1)+crop_rect(3) ) = IM;
        croppedBF = BiasFrameHelper.cropBFToFrameStatic(...
                      BiasFrameHelper.findFlipState(fullsizeIm,BF), imInfo);
      end      
    end
    
    function [ croppedBF ] = cropBFToFrameStatic(BF, imInfo)
      % Crops the full-size bias frame according to the location of the given
      % PTW image as specified by its 'imInfo'.
      if nargin ~= 2 || isempty(imInfo)
        throw(IllegalArgumentException('BiasFrameHelper:cropBFToFrame',...
          ['Some inputs are missing or undefined! Should be 2, found: ' num2str(nargin) '.']));
      end
      % Get the location of the image in the camera's 512x640 coordinates
      crop_rect = imInfo.m_cliprect + [1 1 -1 -1]; % Altair-to-MATLAB indexing difference
      croppedBF = BF(crop_rect(2):crop_rect(2)+crop_rect(4), ...
                     crop_rect(1):crop_rect(1)+crop_rect(3) );
    end
    
    function BF = estimateBiasFrame(DL,IT,P,offsetByITZero)
      % 
      % IT&DL do not have to be sorted, but have to correspond to each other!
      % IT Should be specified in [us] (i.e. IT_in_file*1E6)
      if nargin < 4
        offsetByITZero = false;
      end
      if nargin < 3
        P = @(R)1;
      end
      %% Sort by IT:
      [IT,I] = sort(IT,'ascend');
      DL = DL(:,:,I);
      %% Approximate bias using linear fitting:
      % Stage 1: Find data for negative biases (based on high-BF, IT_BF,IT frames)
      firstUnsat = find(min(reshape(DL,numel(DL(:,:,1)),size(DL,3)),[],1),1,'first');
      if isempty(firstUnsat) || firstUnsat > numel(IT) - 2
        firstUnsat = numel(IT) - 2;
      end
      if gpuDeviceCount % Should be positive when a CUDA gpu is available
        ITb = gpuArray(double(IT(firstUnsat:end)));
        DLb = gpuArray(double(DL(:,:,firstUnsat:end)));
      else
        ITb = double(IT(firstUnsat:end));
        DLb = double(DL(:,:,firstUnsat:end));
      end
      %% Computing the slope and intercept:
      B = reshape(((ITb(:).^([0,P(0)]))\reshape(DLb,[], numel(ITb)).').', [size(DL(:,:,1)) 2]);
      % B(:,:,1) is the intercept (aka Bias), B(:,:,2) is the slope (aka R).
      % DEBUG: figure(); imagesc(inpaintOutliers(gather(B(:,:,2)))); axis image;
      if P(0) == 1        
        if IT(1) > 1
          LogHelper.w(LogMessage('Cannot apply P correction w/o a measured BF (DF @ IT ~ 0)!'));
          BF = B(:,:,1);
          return
        end
        %% Correction for the bias of mu_BF due to a wrong power of P:
        D = B(:,:,1) - DL(:,:,1); % Difference between the measured values at IT=0 and the estimate.
        BF = B(:,:,1) - mean(D(DL(:,:,1)>0)); % Find the offset for pixels that were above 0.
        if offsetByITZero % Replace with the measured values at IT=0:
          % Risky: sometimes (related to choice of IT?) ruins the previous result! 
          BF(DL(:,:,1)>0) = DL(DL(:,:,1)>0);
        end
        %% (alternate) Censoring-based offset:
        %{        
        dM = fitdist(reshape(-DL(:,:,1),[],1),'Normal','Censoring',reshape(DL(:,:,1),[],1) == 0);
        % The mu parameters of d is the opposite sign because only right-censoring is
        % supported by matlab, so the data has to be mirrored about zero.        
        dE = fitdist(BF(:),'Normal'));
        %}        
      else
        BF = B(:,:,1);
      end
    end
    
    % TODO: functionality needs to be split to smaller functions for better reusability
    function BF = acquireBiasFrame(filterNum, keepIntermediateImgs, correctDeadPx)
      %% Constant/globals:      
      hVCam = FLIR_Init();
      %% Handle inputs:
      if nargin <= 1 || isempty(filterNum)
        v = values(CURRENT_MAP); 
        v = [v{:}]; [~,v] = min([v.declaredWidth]);
        filterNum = v-1; % The least permissive filter available.
      end
      if nargin <= 2 || isempty(keepIntermediateImgs)
        keepIntermediateImgs = false;
      end
      if nargin <= 3 || isempty(correctDeadPx)
        correctDeadPx = true;
      end
      %% Prepare for acquisition:
       % TODO Autofind ITs
       % TODO Remember old settings
      % Switch filter:
      hVCam.SetCurrentFilter(filterNum); pause(1);
      % Set ITs:
      BiasFrameHelper.setBF_ITs();
      [DL,IT] = BiasFrameHelper.performAcquisition(keepIntermediateImgs);
      %% Estimate BF:
      BF = BiasFrameHelper.estimateBiasFrame(DL,IT);
      % Apply correction for dead pixels:
      if correctDeadPx
        BF = removeOutliers(BF);
      end
    end
    
  end % Public Static methods
  
  methods (Access = private)        

  end % Private methods
  
  methods (Access = private, Static = true)
    
    function out = sumN(A)
      out = nansum(A(:));
    end

    function setBF_ITs()
      %% Constants:
      MAX_N_IT = 4;
      WAIT_DURATION_B4_AQ = 2;
      IT = BiasFrameHelper.IT; % TODO? Allow a different input
      hVCam = FLIR_Init();
      %% Set camera parameters:
      % Before setting IT, set refresh rate to minimum:
      [~] = FLIR_Camera_SetFrequency(1, 0);
      % Set amount of camera ITs:
      hVCam.SetMultiTi(MAX_N_IT);
      % Wait a bit for the camera to stabilize:
      java.lang.Thread.sleep(WAIT_DURATION_B4_AQ*1000);
      % Set the ITs:      
      for indIT = 1:MAX_N_IT
       [~, ~, ~] = ...
          FLIR_Camera_SetIT(IT(indIT), indIT, false);
      end
      % Set refresh rate to maximum available:
      [~,fMaxFrequency] = hVCam.GetMaxFrequency(0); 
      fMaxFrequency = single(fMaxFrequency);
      [~, ~, ~] = FLIR_Camera_SetFrequency(fMaxFrequency, true);
      % Wait a bit for the camera to stabilize:
      java.lang.Thread.sleep(WAIT_DURATION_B4_AQ*1000);
    end
    
    function [DL,knownIT] = performAcquisition(keepIntermediateImgs)
      %% Constants:
      MAX_N_IT = 4;
      IT = BiasFrameHelper.IT; % TODO? Allow custom input
      %% All the rest:
      eP = struct('filePath','','fileName','','extn','');
      [eP.filePath,eP.fileName,eP.extn] = fileparts(getCurrentAltairFilename());
      % Take a snapshot of the PTW file list before the acquisition:
      ptw_path = fullfile(eP.filePath,'*.ptw');
      PTW_Before = cellstr(ls(ptw_path)); 
      if(isequal(PTW_Before,{''}))
          PTW_Before = {};
      end
      expected_time = getExpectedAcquisitionTime();
      %% Acquisition:
      ControlScriptHelper.pressRecordButtonInAltair();
      fprintf(1,'Please wait while recording...\n\n'); %TODO: Escape to cancel
      %% Wait until camera finished taking and saving images:
      ACQUISITION_DELAY_SAFETY_FACTOR = 0.2;
      IO_DELAY = ControlScriptHelper.getWriteDelay();
      flag = true;
      while flag
        java.lang.Thread.sleep(ACQUISITION_DELAY_SAFETY_FACTOR*(IO_DELAY+expected_time)*1000);
        flag = ~ControlScriptHelper.isAcquisitionStopped;
      end
      %% Take another snapshot of the PTW file list and find new files
      PTW_After = cellstr(ls(ptw_path));
      newPtw = setxor(PTW_After,PTW_Before);
      %% See that there's a correct amount of files:
      assert(numel(newPtw) == MAX_N_IT,...
        ['Wrong amount of images created! Expected: ' ...
        num2str(MAX_N_IT) ', found: ' num2str(numel(newPtw))]);
      %% Fix IT in files:
      if contains(ptw_path,'...') 
        LogHelper.w(LogMessage('PTW directory contains "...". IT fixing disabled.'));
      else
        fp = fullfile(fileparts(ptw_path),newPtw);  
        knownIT = PredictCameraIT(IT(:));
        PTWFixMultiITGroup(fp,true,[],[],knownIT);
        %% Output as DL:
        nfo = PTWFileUtils.getFileInfo(fp{1});
        DL = zeros(nfo.m_rows,nfo.m_cols,MAX_N_IT);
        for indIT = 1:MAX_N_IT
          DL(:,:,indIT) = PTWFileUtils.ReadAndAverageFramesFromFile(fp{indIT});
        end
        %% Cleanup (if needed):
        if ~keepIntermediateImgs
          for indIT = 1:MAX_N_IT
            delete(fp{indIT});
          end
        end
      end   
    end    
    
  end % Private Static methods
end % Classdef