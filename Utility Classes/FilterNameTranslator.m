classdef FilterNameTranslator
  
  properties (Access = private, Constant = true)
    F345_NUM = 1;
    F363_NUM = 2;
    F378_NUM = 3;
  end
  
  methods (Access = public, Static = true)
    function filtNum = numFromName(m_filter_from_ptw)
      switch m_filter_from_ptw
        case '3.46_14'
          filtNum = PTWTranslator.F345_NUM;
        case '3.60_FW-0.14um'
          filtNum = PTWTranslator.F363_NUM;
        case '3.80--040nm'
          filtNum = PTWTranslator.F378_NUM;
        otherwise
          filtNum = NaN;
      end
    end
  end
  
end