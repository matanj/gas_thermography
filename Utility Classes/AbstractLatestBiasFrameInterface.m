classdef (Abstract) AbstractLatestBiasFrameInterface < handle
% This is a specialized interface for storing to be used for Bias frames.
  properties (Abstract = true, Access = protected, Constant = true)
    LATEST_FRAME_FILENAME;
  end  

  methods (Abstract = true, Access = public)    
    bias_frame = getBF(helperObj)
  end
  
end