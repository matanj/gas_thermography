classdef PiecewiseExp < handle

    methods (Access = public)      
        function [this] = PiecewiseExp(X, Y, windowSize) % Constructor
            %% TODO: parse varargin                        
%               windowSize = varargin{1}; 
            assert(~mod(windowSize,2) && windowSize > 0,...
                'windowSize should be a natural even number.');
            %% Example Data
            %{
            X = [0.002723682418630,0.002687088539567,0.002634005004610,0.002582978173834,...
                 0.002530684550171,0.002462144527884,0.002397219225698,0.002341097974950,...
                 0.002287544321171,0.002238889510803]';
            Y = [0.178923076435990,0.213320004768074,0.263918364216839,0.324208349386613,...
                 0.394340431220509,0.511466688684182,0.671285738221314,0.843849959919278,...
                 1.070756756433334,1.292800046096531]';
            %}
            assert(windowSize <= length(X),...
                'windowSize cannot be larger than the amount of points.');
            %% Initialization
            this.nFits = length(X)-windowSize+1;
            this.coeffMat(this.nFits,PiecewiseExp.N_PARAMS_IN_MODEL) = 0; %// Preallocation

            ft = fittype( 'a/(exp(b*x)+c)', 'independent', 'x', 'dependent', 'y' );
            opts = fitoptions( 'Method', 'NonlinearLeastSquares' );
            opts.Lower = [-Inf -Inf -1E4];
            opts.Upper = [ Inf  Inf  1E4];
            opts.MaxFunEvals = 6000;
            opts.MaxIter = 4000;
            opts.Robust = 'LAR';
            opts.StartPoint = [6000,4000,0];
            
            if ~issorted(X) % ascending sorting is needed later for histc
                X = flipud(X); Y = flipud(Y);
            end
            %% Populate coefficient matrix
            for ind1 = 1:this.nFits
                [xData, yData] = prepareCurveData(...
                    X(ind1:ind1+windowSize-1),Y(ind1:ind1+windowSize-1));
                % Fit model to data:
                fitresult = fit( xData, yData, ft, opts );
                % Save coefficients:
                this.coeffMat(ind1,:) = coeffvalues(fitresult);
            end
            clear ft opts ind1 xData yData fitresult ans
            %% Get the transition points
            this.xTrans = [-inf; X(windowSize/2+1:end-windowSize/2); +inf];
            this.yTrans = flipud([+inf; Y(windowSize/2+1:end-windowSize/2); -inf]);
        end
        
        % External interface
        function b = subsref(this,s)
           switch s(1).type
              case '()'
                 xval = s.subs{:};
                 b = this.Evaluate(xval);
              case '{}'
                 xval = s.subs{:};
                 b = this.EvaluateInverse(xval);
              otherwise
                 builtin('subsref',this,s);
           end
        end % subsref
    end
    
    methods (Access = private)
        % Interpolation
        function [DL_IT] = Evaluate(this,iTk)
            if ~isempty(this.xTrans) %// The case of nFits==1
                [~,rightFit] = histc(iTk(:),this.xTrans);
            else
                rightFit = 1;
            end
            %% Actually evaluate the right fit - OVERHEAD?
            DL_IT = reshape(PiecewiseExp.DL_IT(iTk(:),...
                 this.coeffMat(rightFit,1),this.coeffMat(rightFit,2),...
                 this.coeffMat(rightFit,3)),size(iTk));
             
        end
        
        function [iTk] = EvaluateInverse(this,DL_IT)
            if ~isempty(this.yTrans) %// The case of nFits==1
                [~,rightFit] = histc(DL_IT(:)-eps,this.yTrans); %eps since y is reversed
            else
                rightFit = 1;
            end
            rightFit = this.nFits - rightFit + 1;
            %% Actually evaluate the right fit
            iTk = reshape(PiecewiseExp.iTk(DL_IT(:),...
                this.coeffMat(rightFit,1),this.coeffMat(rightFit,2),...
                this.coeffMat(rightFit,3)),size(DL_IT));
        end
            
    end

    properties(SetAccess = private, GetAccess = private)
        xTrans; % transitions between fits, iTk direction
        yTrans; % transitions between fits, DL_IT direction
        coeffMat; % holds the fit coefficients
        nFits; %this is equivalent to "size(this.coeffMat,1)"
    end
    
    properties(Access = private, Constant = true)               
        C_to_K = +273.15;        
        N_PARAMS_IN_MODEL = 3; % For the model y = A./(exp(B*x)+C);
                
        DL_IT = @(iTk,A,B,C)A./(exp(B.*iTk)+C); % x = 1/T [1/k]; 
        iTk = @(DL_IT,A,B,C)log(A./(DL_IT)-C)./B;   %iTk = inverse(T in Kelvin) 
        Tc = @(DL_IT,A,B,C)iTk(DL_IT,A,B,C).^(-1)-PiecewiseExp.C_to_K; 
    end       
end