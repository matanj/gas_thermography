classdef GUIObjectHelper
   % This class contains common actions that are done on UI objects
   properties (Constant = true, Access = private)
     editboxControlDefaultPrefix = 'txt';
     sliderControlDefaultPrefix = 'sld';
   end
   
   methods (Static = true)
      function out = setgetEditboxPrefix(newPrefix)
         persistent editboxUiControlPrefix;
         if nargin
            editboxUiControlPrefix = newPrefix;
         end
         if isempty(editboxUiControlPrefix)
           out = GUIObjectHelper.editboxControlDefaultPrefix;
         else
           out = editboxUiControlPrefix;
         end
      end   
      
      function out = setgetSliderPrefix(newPrefix)
         persistent sliderUiControlPrefix;
         if nargin
            sliderUiControlPrefix = newPrefix;
         end
         if isempty(sliderUiControlPrefix)
           out = GUIObjectHelper.sliderControlDefaultPrefix;
         else
           out = sliderUiControlPrefix;
         end
      end
%%%%%%%%%%     
       function objString = getString(hObject)
           objString = get(hObject,'String');
       end
       
       function putString(hObject,String)
           set(hObject,'String',String);
       end
%%%%%%%%%%
       function objNumber = getNumberFromString(hObject)
           objNumber = str2double(GUIObjectHelper.getString(hObject));
       end
       
       function putNumberAsString(hObject,num)
           GUIObjectHelper.putString(hObject,num2str(num));
       end
%%%%%%%%%%       
       function setValue(hObject,newVal)
           set(hObject,'Value',newVal);       
       end

       function setChecked(hObject)
           GUIObjectHelper.setValue(hObject,1);
       end
       
       function setUnchecked(hObject)
           GUIObjectHelper.setValue(hObject,0);
       end
       
       function isChecked=getChecked(hObject)
           isChecked=get(hObject,'Value')==1;       
       end       
%%%%%%%%%%       
       function radioSelect(hObject)
           setChecked(hObject);
       end
%%%%%%%%%%        
       function enableObj(hObject)
           set(hObject,'Enable','on');
       end
       
       function disableObj(hObject)
           set(hObject,'Enable','off');
       end
%%%%%%%%%%        
       function showObj(hObject)
           set(hObject,'Visible','on');
       end
       
       function hideObj(hObject)
           set(hObject,'Visible','off');
       end
%%%%%%%%%%        
       function newVal = updateSliderAndTextFromUI(hSource,handles)
           objName = get(hSource,'Tag');
           hSlider = [GUIObjectHelper.setgetSliderPrefix  objName(4:end)];
           hText   = [GUIObjectHelper.setgetEditboxPrefix objName(4:end)];
           switch get(hSource,'Style')
               case 'slider'
                   newVal = get(hSource,'Value');
                   GUIObjectHelper.putNumberAsString(handles.(hText),newVal);
               case 'edit'
                   newVal = GUIObjectHelper.getNumberFromString(hSource);
                   set(handles.(hSlider),'Value',newVal);                   
           end
       end
       
       function updateSliderAndTextFromCode(uiElementTag,newVal,filterNum,handles)
           % Get handle name of appropriate slider and editbox:
           hSlider = GUIObjectHelper.buildElementTag(uiElementTag,GUIObjectHelper.setgetSliderPrefix,filterNum);
           hText   = GUIObjectHelper.buildElementTag(uiElementTag,GUIObjectHelper.setgetEditboxPrefix,filterNum);
           GUIObjectHelper.putNumberAsString(handles.(hText),newVal);
           set(handles.(hSlider),'Value',newVal);                   
       end
              
       function updateXY(hLine,newXVal,newYVal)
           set(hLine,'XData',newXVal,'YData',newYVal);
       end
       
       function uiElementTag = buildElementTag(genericTag,elementStyle,filterNum)
           uiElementTag = [elementStyle genericTag(4:end-1) num2str(filterNum)];
       end
       
       function [hSlider,hText] = makeSliderAndTextTags(uiElementTag,filterNum)
            hSlider = GUIObjectHelper.buildElementTag(uiElementTag,GUIObjectHelper.setgetSliderPrefix,filterNum);
            hText   = GUIObjectHelper.buildElementTag(uiElementTag,GUIObjectHelper.setgetEditboxPrefix,filterNum);            
       end       
   end 
   methods (Static=true, Access=private)      

   end
end