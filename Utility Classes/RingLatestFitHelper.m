classdef RingLatestFitHelper < handle & AbstractLatestFitInterface
% DESCRIPTION GOES HERE  
  properties (Access = protected, Constant = true)
    LATEST_FITS_FILENAME = 'Vollmer_FTS'; 
    CELCIUS_TO_KELVIN_T_SHIFT = 273.15;
    TEMPERATURE_SEARCH_RANGE_IN_C = [30 700];
    RADIATION_SEARCH_RANGE = [0 3E2]; %R=300 is about 700C for F3.63
    DEFAULT_R_GUESS = 10;
    MAX_ITER = 10;    
  end
  
  properties (Access = protected)
    fitCell = cell(1,4);
    T2R = cell(4,1); % T in [K] !
    R2T = cell(4,1);
    % These (should have setters/getters)
    lowT = 300;
    topT = 1000;
  end
  
  methods (Access = public)    
    % This constructor has to be modified according to the structure of the data such
    % that in the end fitCell will contain an sfit object.
    function helperObj = RingLatestFitHelper()
      helperObj.fitCell = struct2array(load(RingLatestFitHelper.LATEST_FITS_FILENAME)); 
      %Initialize T2R
      for indF = 1:4
        Ffit = helperObj.fitCell{indF};
        % Formula: A/(exp(B/T)-C)
        helperObj.T2R{indF} = @(T)Ffit.A./(exp(Ffit.B./T)-Ffit.C);        
        helperObj.R2T{indF} = @(R)Ffit.B./log(Ffit.A./R+Ffit.C);
      end
    end
    % It is assumed at this stage that:
    % 1) "fitCell" contains sfit objects (after the appropriate loading/processing was done in the constructor.)
    % 2) Each fit contains parameters named A, B, C that relate T to R
    function hR = getT2R(helperObj,filterNum)
      if isa(filterNum,'Filter')
        hR = helperObj.getT2RwithFilter(filterNum);
      else
        hR = helperObj.T2R{filterNum};
      end
    end
    
    function hR = getT2RwithFilter(helperObj,filterObj)      
      validateattributes(filterObj,{'Filter'},{'scalar','nonempty'});
      switch filterObj
        case F3460
          filterNum = 1;
        case F3600
          filterNum = 2;
        case F3800
          filterNum = 3;
        case F4370
          filterNum = 4;          
        otherwise
          error('Unsupported filter - calibration does not exist.');
      end
      hR = getT2R(helperObj,filterNum);
    end
    
    function hR = getR2TwithFilter(helperObj,filterObj)
      validateattributes(filterObj,{'Filter'},{'scalar','nonempty'});
      switch filterObj
        case F3460
          filterNum = 1;
        case F3600
          filterNum = 2;
        case F3800
          filterNum = 3;
        case F4370
          filterNum = 4;
        otherwise
          error('Unsupported filter - calibration does not exist.');
      end
      hR = getR2T(helperObj,filterNum);
    end
    
    function hR = getR2T(helperObj,filterNum)
      if isa(filterNum,'Filter')
        hR = helperObj.getR2TwithFilter(filterNum);
      else
        hR = helperObj.R2T{filterNum};
      end
    end       
    
    % When dealing with "convenient" R(T) functions, it's easy to derivate the equations
    % analytically.
    function rat = T2RatDiff(helperObj, filterNum1, filterNum2, derivative_order)
      if isa([filterNum1, filterNum2],'Filter')
        fn = helperObj.filterToID([filterNum1, filterNum2]);        
        rat = helperObj.T2RatDiff(fn(1), fn(2), derivative_order);
        return
      end
      
      if ~derivative_order % ==0
        rat = @(T)helperObj.fitCell{filterNum1}.A / helperObj.fitCell{filterNum2}.A * ...
             (exp(helperObj.fitCell{filterNum2}.B./T) - helperObj.fitCell{filterNum2}.C) ./ ...
             (exp(helperObj.fitCell{filterNum1}.B./T) - helperObj.fitCell{filterNum1}.C);
      else
        syms A1 A2 B1 B2 C1 C2 T
        F1 = A1/(exp(B1/T)-C1);
        F2 = A2/(exp(B2/T)-C2);
        rat = matlabFunction(subs(diff(F1/F2,'T',derivative_order),[A1 A2 B1 B2 C1 C2],[...
          helperObj.fitCell{filterNum1}.A, helperObj.fitCell{filterNum2}.A,...
          helperObj.fitCell{filterNum1}.B, helperObj.fitCell{filterNum2}.B,...
          helperObj.fitCell{filterNum1}.C, helperObj.fitCell{filterNum2}.C,...
          ]));
      end
    end  
      
    % The following function is P(R)-model-specific and should be modified. It is assumed at this stage that:
    % 1) "fitCell" contains sfit objects (after the appropriate loading/processing was done in the constructor.)
    % 2) Each fit contains parameters named A, B, C that relate T to R
    function hP = getP(helperObj,filterNum)
      Ffit = helperObj.fitCell{filterNum};
      hP = @(R)(Ffit.A0 + Ffit.A1*R + Ffit.A2*R.^2);
    end
    
    function ratio_of_R = findRatio(helperObj, filterNum1, filterNum2, DL1, DL2, IT1, IT2, R0a, R0b)
      P1 = helperObj.getP(filterNum1);
      P2 = helperObj.getP(filterNum2);
      if isscalar(DL1) && isscalar(DL2) && isscalar(IT1) && isscalar(IT2)
        ratio_of_R = helperObj.DLITtoR(DL1,IT1,P1)./helperObj.DLITtoR(DL2,IT2,P2);
      else
        if nargin == 7
          ratio_of_R = RingLatestFitHelper.MAT_DLITtoR(DL1,IT1,P1) ./ ...
                       RingLatestFitHelper.MAT_DLITtoR(DL2,IT2,P2);          
        elseif nargin == 9
          ratio_of_R = RingLatestFitHelper.MAT_DLITtoR_withGuess(DL1,IT1,P1,R0a) ./ ...
                       RingLatestFitHelper.MAT_DLITtoR_withGuess(DL2,IT2,P2,R0b);  
        else
          ratio_of_R = NaN(size(DL1));
          warning('RingLatestFitHelper.findRatio::Invalid number of input parameters');
        end
      end
    end

    function temp_in_C = getTempInCFromRatio(helperObj, filterNum1, filterNum2, ratio_of_radiations)
      temp_in_C = RingLatestFitHelper.K2C(...
      helperObj.getTempInKFromRatio(filterNum1, filterNum2, ratio_of_radiations));        
    end
    
    function temp_in_K = getTempInKFromRatio(helperObj,filterNum1, filterNum2, ratio_of_radiations)
      T2Ratio = @(T_in_K)helperObj.T2R{filterNum1}(T_in_K)./helperObj.T2R{filterNum2}(T_in_K);
      Ti = 20; Tf = 1000; Tv = (Ti:0.1:Tf)+273.15; Rv = T2Ratio(Tv);
      temp_in_K = nakeinterp1(Rv(:),Tv(:),double(ratio_of_radiations(:))); 
      temp_in_K = reshape(temp_in_K,size(ratio_of_radiations)); 
      temp_in_K(temp_in_K<helperObj.C2K(0) | temp_in_K>1000) = NaN;
    end
    
    function temp_in_K = fzeroTempK(helperObj, filterNum1, filterNum2, ratio_of_radiations)
      R1 = helperObj.getT2R(filterNum1);
      R2 = helperObj.getT2R(filterNum2);
      funT = @(T)ratio_of_radiations-R1(T)/R2(T);
%     Solve functions for T using fzero:
      temp_in_K = fzero(funT,RingLatestFitHelper.CELCIUS_TO_KELVIN_T_SHIFT + ...
                             RingLatestFitHelper.TEMPERATURE_SEARCH_RANGE_IN_C);
    end    
    
    function temp_in_C = fzeroTempC(helperObj, filterNum1, filterNum2, ratio_of_radiations)
      temp_in_C = RingLatestFitHelper.K2C(...
        helperObj.fzeroTempK(filterNum1, filterNum2, ratio_of_radiations));        
    end

    function R = findRadiation(helperObj, filterNum, DL, IT)
      if isscalar(DL)  
        R = RingLatestFitHelper.DLITtoR(DL,IT,helperObj.getP(filterNum));
      else
        R = RingLatestFitHelper.MAT_DLITtoR(DL,IT,helperObj.getP(filterNum));
      end        
    end
    
    function R = findRadiationWithGuess(helperObj, filterNum, DL, IT, R0)
      if isscalar(DL)
        R = RingLatestFitHelper.DLITtoR_withGuess(DL,IT,helperObj.getP(filterNum),R0);
      else
        R = RingLatestFitHelper.MAT_DLITtoR_withGuess(DL,IT,helperObj.getP(filterNum),R0);
      end
    end
    
    function P = getP_at_R(helperObj, filterNum, R)
      hP = helperObj.getP(filterNum);
      P = hP(R);
    end
  end
  
  methods (Access = private, Static = true)
    function id = filterToID(filterObj)
      [~,id] = max(getCWL(filterObj(:)) == getCWL([F3460 F3600 F3800]),[],2);
    end
    
    function R = DLITtoR(DL,IT,P) %%%%%%%%%%%%%%%%%%%% TEST THIS!!!!!!!!!!!!
      DL_IT_R = @(R)R*IT^P(R)-DL;
      if sign(DL_IT_R(RingLatestFitHelper.RADIATION_SEARCH_RANGE(1))) ~= ...
         sign(DL_IT_R(RingLatestFitHelper.RADIATION_SEARCH_RANGE(2)))
        R = fzero(DL_IT_R,RingLatestFitHelper.RADIATION_SEARCH_RANGE);
      else % If the sign is the same, fallback to a different type of fzero:
        R = fzero(DL_IT_R,DL/IT);
      end
    end
    
    function R = DLITtoR_withGuess(DL,IT,P,R0)
      DL_IT_R = @(R)R*IT^P(R)-DL;
      try
        R = fzero(DL_IT_R,R0);
      catch % If the sign is the same, fallback to a different type of fzero:
        R = NaN;
      end
    end
    
    function R = MAT_DLITtoR(DL,IT,P)
      R = RingLatestFitHelper.MAT_DLITtoR_withGuess(DL,IT,P,RingLatestFitHelper.DEFAULT_R_GUESS);
    end
    
    % Find R iteratively over a matrix
    function R = MAT_DLITtoR_withGuess(DL,IT,P,R0)
      R = DL / IT^P(R0); % Initial estimate of R
      for indL = 1:RingLatestFitHelper.MAX_ITER
        oldR = R; % This step may be skipped in memory-heavy (very large matrix) cases
        R = DL ./ IT.^P(R);
        if norm(oldR-R)==0
          break
        end
      end
    end
    
    function TinC = K2C(TinK)
      TinC = TinK - RingLatestFitHelper.CELCIUS_TO_KELVIN_T_SHIFT;
    end
    
    function TinK = C2K(TinC)
      TinK = TinC + RingLatestFitHelper.CELCIUS_TO_KELVIN_T_SHIFT;
    end
  end
  
end