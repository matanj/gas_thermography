classdef ShiftingHelper
    % SHIFTINGHELPER contains functions that handle image misalignment
    % using various methods. There are 2 public methods that can be called:
    % getAutoShiftedIMs(...) , getManuallyShiftedIMs(...)
    %
    % INPUTS (Required):
    % imCell - 1x2 cell of 2d matrices 
    % INPUTS (Optional, name-value pairs):
    % 'Scale'  - Upsampling scale. Natural number. Default: 5.
    % 'ResizeMethod' - followed by one of the methods accepted by imresize(). Default: 'bicubic'.
    % 'TemplateSizeFr' - See image below
    % 'DistFromBound' - See image below
    % 'ShiftAlgo' - Which algorithm to use? 'xcorr', 'hough', 'imregister'.
    % 'ForcedShift' - 2x1 vector, forces a precomputed shift for manual shifting 
    % 'doDownsample' - A boolean flag indicating whether the images should
    %                  be reduced to their original size. Default: false;
%{
        
        <-----------X------------>
  
  ^     |------------------------|  Notes:
  |     |         |              |    1) Ususally: X = Y
  |     |         b        IM1   |    2) TEMPLATE_SIZE_FRACTION = x/X = y/Y
  |     |         |              |    3) DISTANCE_FROM_BOUNDARY = [a b]   
  |     |<--a-->|-------|  |     |    4) SCALE: [X_new,Y_new] = [X Y]*SCALE
  |     |       | Templ-|  y     |
  Y     |       |  ate  |  |     |
  |     |       |-------|  |     |
  |     |                        |
  |     |       <---x--->        |
  |     |                        |
  v     |------------------------|
        
%}
%% Constants
    properties (Constant = true, Access = private) 
        DEFAULT_TEMPLATE_SIZE_FRACTION = 0.2; % [0---x---1]
        DEFAULT_DISTANCE_FROM_BOUNDARY = 50*[1 1]; % [px]
        DEFAULT_ENLARGEMENT_SCALE = 5;
        DEFAULT_SHIFT_DETECTION_METHOD = 'xcorr';
        DEFAULT_HOUGH_RADII = [40 70];
        DEFAULT_FORCED_SHIFT = [0 0];
        DEFAULT_DONWSAMPLE_FLAG = false;
    end
%% Public Methods
    methods (Access = public, Static = true)
        function processedIMs = getAutoShiftedIMs(varargin)           
            % Parse inputs:
            inputs = ShiftingHelper.parseInputs(varargin{:});
            % Upsample: 
            processedIMs = ShiftingHelper.resampleImCell(inputs.imCell,inputs); 
            % Find and fix shift:
            processedIMs{2} = ShiftingHelper.correctMisalignment(processedIMs,inputs);
            % Downsample:
            processedIMs = ShiftingHelper.downsampleIfRequired(processedIMs,inputs);
        end
        
        function smallIms = downsampleIfRequired(IMs,inputs)
            if (inputs.doDownsample) && (inputs.Scale ~= 1)
                inputs.Scale = 1/inputs.Scale;
                smallIms{1} = inputs.imCell{1};
                smallIms{2} = ShiftingHelper.resampleImCell(IMs(2),inputs); 
            else
                smallIms = IMs;
            end
        end
        
        function processedIMs = getManuallyShiftedIMs(varargin)
            % Parse inputs:
            inputs = ShiftingHelper.parseInputs(varargin{:});
            % Upsample:
            processedIMs = ShiftingHelper.resampleImCell(inputs.imCell,inputs); 
            % Shift using a predetermined shift:
            processedIMs{2} = ShiftingHelper.shiftImage(processedIMs{2},inputs.ForcedShift,inputs.Scale);
            % Downsample:
            processedIMs = ShiftingHelper.downsampleIfRequired(processedIMs,inputs);
        end
        
        % This is a replacement\workaround for a "Public Static" field
        function val = lastShift(newval)         
            %% Get\Set the value:
            persistent currentval;
            if nargin >= 1
                %% Making sure the variable can only be set from within the class:
                tmp_stack = dbstack;  % < + \/ = Crazy hack :) 
                if length(tmp_stack)==1 || ~strcmp(tmp_stack(2).name,'ShiftingHelper.shiftImage')
                    warning('Trying to set a static property from outside the class.');
                    return
                else
                    currentval = newval;    
                end                 
            end
            val = currentval;
        end        
    end
%% Private (internal) Methods    
    methods (Access = private, Static = true)
        function inputs = parseInputs(varargin)
            %% Input parsing:
            p = inputParser;
            p.addRequired('imCell',@(x)length(x)==2); % cell array of images, always 1st argument
            p.addParameter('Scale',ShiftingHelper.DEFAULT_ENLARGEMENT_SCALE,...
               @(x)validateattributes(x, {'numeric'},{'scalar', 'integer', 'positive'}));
            p.addParameter('ResizeMethod','bicubic',@(x)validatestring(x,...
                {'nearest','bilinear','bicubic','box','triangle',...
                 'cubic','lanczos2','lanczos3'})); %any argument after 2nd
            p.addParameter('TemplateSizeFr',ShiftingHelper.DEFAULT_TEMPLATE_SIZE_FRACTION,...
               @(x)validateattributes(x, {'numeric'},{'scalar', '<', 1, 'positive'}));
            p.addParameter('DistFromBound',ShiftingHelper.DEFAULT_DISTANCE_FROM_BOUNDARY,...
               @(x)validateattributes(x, {'numeric'},{'row', 'integer', 'nonnegative','numel', 2}));
            p.addParameter('ShiftAlgo',ShiftingHelper.DEFAULT_SHIFT_DETECTION_METHOD,...
               @(x)validatestring(x,'xcorr','hough','imregister'));
            p.addParameter('ForcedShift',ShiftingHelper.DEFAULT_FORCED_SHIFT,...
               @(x)validateattributes(x, {'numeric'},{'row', 'integer', 'numel', 2}));
            p.addParameter('doDownsample',ShiftingHelper.DEFAULT_DONWSAMPLE_FLAG,...
               @(x)validateattributes(x, {'logical'},{'scalar'}));
            % TODO: handle downsampling arguments: 1) isNeeded? 2) method
            p.parse(varargin{:}); inputs = p.Results;
            %% Input validity checks:
            if any(size(inputs.imCell{1})-size(inputs.imCell{2})) || ... size of images is not the same
               false % some other condition
                error('Something wrong with inputs.');                
            end
        end
        
        function resampledIMs = resampleImCell(imCell,inputs)
            nIm = numel(imCell);
            resampledIMs{nIm} = 0; %preallocation
            for ind1 = 1:nIm
                resampledIMs{ind1} = ShiftingHelper.resizeImage(...
                imCell{ind1},inputs.Scale,inputs.ResizeMethod);
            end 
            if (nIm == 1) %fix for downsampling
                resampledIMs = resampledIMs{nIm};
            end
        end
        
        function resizedIM = resizeImage(im,scale,method)
            try   % GPU Resize
                resizedIM = gather(imresize(gpuArray(im),scale,method));
            catch % CPU Resize
                resizedIM = imresize(im,scale,method);
                % If this fails, it must be due to insufficient memory.
            end           
        end
        
        function shiftedIM = correctMisalignment(varargin)            
            %!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            %!!!!!!!!!!!!!!!!!!!!!!!! TODO !!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            %!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            resampledIMs = varargin{1};
            if nargin > 1 %Optional sanity checking
                SCALE = varargin{2}.Scale;
            end
            %% Handle inputs \ defaults            
            windowSzFrac = varargin{2}.TemplateSizeFr; 
            distFromBound = varargin{2}.DistFromBound;
            % TODO: handle non-default subimage location input
            %% Get template (cut a small image out of the 1st one)
            [templ,cutRect] = ShiftingHelper.getXCorrTemplate(resampledIMs{1},windowSzFrac,distFromBound);
            %% Find shift        
            % Selection of the shift-finding method    
            switch varargin{2}.ShiftAlgo
                case 'xcorr'
%                   ############################ Sort this out!!!
                    template_pos = ShiftingHelper.getXCorrPeak(templ,resampledIMs{2});
                    shift = template_pos(1:2)-cutRect(1:2); %TODO: check!!!
                    if all(~shift)
                      disp('XCorr: images seem perfectly aligned - no shift required.');
                    else
                      disp(['XCorr: best match for template was found for a shift of (' ...
                           num2str(shift(1),'%+2d') ',' num2str(shift(2),'%+2d') ') pixels'...
                           ' (the negative of this needs to be applied to the large image).']);
                    end
                case 'hough'
                    shift = ShiftingHelper.getHoughShift(templ,resampledIMs{2},...
                         ShiftingHelper.DEFAULT_HOUGH_RADII);
                case 'imregister'
                    
                otherwise
                    error('Unrecognized shift detection method!'); %should not be possible to get this far
            end
            %% Correct shift
            shiftedIM = ShiftingHelper.shiftImage(resampledIMs{2},-shift,SCALE);        
        end
        
        function shiftedIM = shiftImage(im,shift,scale)
            % Check if the found shift makes sense, and apply it if so:
            if norm(shift)/scale > 3 ...|| norm(xCorrShift) <= 1 ... possibly compare to 
            ;        error(['The found shift is likely wrong... Please adjust its '...
                    'parameters (windowSzFrac & | distFromBound) and try again.']);
            else %Actually apply the shift:
                shiftedIM = circshift(im,shift);
            end
            % Set latest shift in a "static" variable:
            ShiftingHelper.lastShift(shift);
        end
        
        function [templ,cutRect] = getXCorrTemplate(im,windowSzFrac,distFromBound)
          %% Input checking:
          if ~ismatrix(im) % check for 2d input
              error('Input is not a 2D image (matrix).');
          end
          imSz = size(im);
          if any(distFromBound+1 > imSz) % check that distFromBound isn't too big:
              error('distFromBound larger than image.');
          end
          if any(imSz.*windowSzFrac ~= round(imSz*windowSzFrac))
              error(['imSz*windowSzFrac doesn''t result in a whole number'...
                    ' for at least one dimension. A different interpolation'...
                    ' scale should be chosen.']);
          end
          %% Define a valid cut rect:
          cutRect = [distFromBound+1 imSz*windowSzFrac.*[1 1]] - [0 0 1 1];
          if any(distFromBound+1 < 0) || any([cutRect(1)+cutRect(3),cutRect(2)+cutRect(4)] > imSz)
              % check for cut rect fits inside image
            error('cutRect is outside image bounds (windowSzFrac >= 1?)');
          end
          templ = imcrop(im,cutRect);
          end
        
        function out = getXCorrPeak(smallImage,largeImage)
            
            try %try with GPU/CUDA - faster
                gpuDevice;                 %       \/ zero for 'off'
                I1 = padarray(gpuArray(smallImage),0*[50 50],'both');
              % imagesc(I1);
                I2 = gpuArray(largeImage);
                R = gather(normxcorr2(I1,I2)); %2nd input must be larger
            catch MException %fallback for CUDA error
                display(MException.message);
                I1 = padarray(smallImage,0*[50 50],'both');
                I2 = largeImage;
                R = normxcorr2(I1,I2); %2nd input must be larger; can possibly work with gather();
            end
                [rowMax,colMax] = find(R == max(nanmax(R)));
                yoffSet = rowMax-size(smallImage,1);
                xoffSet = colMax-size(smallImage,2);
                out = [xoffSet+1, yoffSet+1, size(smallImage,2)-1, size(smallImage,1)-1];
                %{
                  % DEBUG:
                  hFig = figure; hAx = axes; 
                  imagesc(largeImage,'Parent', hAx); 
                  imrect(hAx, out);
                %}
        end
        
        function shift = getHoughShift(im1,im2,radiusRange)            
            [~, circen{1}, cirrad{1}] = CircularHough_Grd(im1, radiusRange);
            [~, circen{2}, cirrad{2}] = CircularHough_Grd(im2, radiusRange);
            
            if length(cirrad{1}) ~= length(cirrad{2}) || ...
                 any([length(cirrad{1}) length(cirrad{2})]>1)               
             
                warning(['Too many circles found. Please make sure that MIN_RADIUS '... 
                         'and MIN_RADIUS are defined correctly. More than 2 centers'...
                         ' may be considered problematic. Currently found: ' ...
                         num2str(max(length(cirrad{1}),length(cirrad{2})>1)) ' centers.']);
                % If there are several different radii, look for commonalities
                [~,I,J] = intersect(cirrad{1},cirrad{2});
                if isempty(I)
                   error('No identical radii found. Aborting.'); 
                else %arbitrarily choose the 1st agreement
                    circen{1} = circen{1}(I(1),:);
                    circen{2} = circen{2}(J(1),:);                
                    % Note: there's no guarantee that circles of the same 
                    % radius correspond to the same areas of the images. 
                end                
            end
            
            shift = round((circen{1}-circen{2})*SCALE);
            disp(['Hough: a shift of (' num2str(xyShifts(1),'%+2d') ...
            ',' num2str(xyShifts(2),'%+2d') ') pixels'...
            ' needs to be applied to the subset image.']);
            
        end        
                
    end
    %{
    FFT2 Stories: 
    - Raz mentioned that in order for us to know how much information we
    add by interpolation or lose by using filters, we need to look at the
    2d fft transform (or more precisely its PSD) and compare the magnitude
    before and after the processing. If the results are withing 0-2% it's
    most likely ok, if the results are 2-3% it's borderline, and if more
    than that then we really changed the image by much and should rethink.
    
    
    From http://yuzhikov.com/articles/BlurredImagesRestoration1.htm :
    
    I = imread('image_src.png');
    figure(1); imshow(I); title('Source image');
    % Convert image into grayscale
    I = rgb2gray(I);
    % Compute Fourier Transform and center it
    fftRes = fftshift(fft2(I));
    % Show result
    figure(2); imshow(mat2gray(log(1+abs(fftRes)))); title('FFT - amplitude spectrum (log scale)');
    figure(3); imshow(mat2gray(angle(fftRes))); title('FFT - phase spectrum');
    
    %}
end