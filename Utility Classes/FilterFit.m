classdef FilterFit
    % FILTERFIT contains functions that hold fit parameters for DL = A*exp(b/T);   
    properties (Constant = true, Access = private) %definitions
        Celsius_to_Kelvin = +273.15;
        PCHIP_CALIBRATION_RANGE = [1 27]*1E-3;
    end
    
    properties (Constant = false, Access = private)
        A   %
        b   %
        WL  % Characteristic wavelength of the filter, may be CWL; optional
    end
    
    methods 
        function this = FilterFit(A,b,WL) %constructor
            this.A = A;
            this.b = b;
            this.WL = WL;
        end
    end
    
    methods (Access=public, Static=true)
        
        function hDL2T = getDL2T(fFitObj)
            switch class(fFitObj)
                case 'FilterFit'
                    hDL2T = @(DL)(fFitObj.b)./log(DL./(fFitObj.A))-FilterFit.Celsius_to_Kelvin;
                case 'cfit'                    
                    hDL2T = @(DL)1/fzero(@(x)fFitObj(x)-DL,FilterFit. ...
                        PCHIP_CALIBRATION_RANGE)-FilterFit.Celsius_to_Kelvin;
                otherwise
                    error('Unsupported fit type.');
            end
        end
                
        function T2DL = getT2DL(fFitObj)
            switch class(fFitObj)
                case 'FilterFit'
                    T2DL = @(T)(fFitObj.A).*exp((fFitObj.b)./(T+FilterFit.Celsius_to_Kelvin));
                case 'cfit'                    
                    T2DL = @(T)fFitObj(1./(T+FilterFit.Celsius_to_Kelvin));
                otherwise
                    error('Unsupported fit type.');
            end
        end
                
        function R2T = getR2T(fFitObj1,fFitObj2)
            switch class(fFitObj1)
                case 'FilterFit'
                    R2T = @(DL1,DL2)(fFitObj1.b-fFitObj2.b) ./ ...
                        log( ((fFitObj2.A)./(fFitObj1.A)).*(DL1./DL2) ) ...
                        - FilterFit.Celsius_to_Kelvin;
                case 'cfit'     
                    ifelse = @(cond, varargin) varargin{1+~cond}(); %Only for the insane
                    R2T = @(DL1,DL2)arrayfun(@(DL1,DL2)...
                        ifelse(~any(isnan([DL1, DL2])),...        
                        @()1/(fzero(@(x)fFitObj1(x)./fFitObj2(x)-...
                        DL1./DL2,FilterFit.PCHIP_CALIBRATION_RANGE)), ...
                        NaN),DL1,DL2) - FilterFit.Celsius_to_Kelvin;
                otherwise
                    error('Unsupported fit type.');
            end
        end
        
        function WL = getWL(fFitObj)
            %%todo: switch case with p.tag
            switch class(fFitObj)
                case 'FilterFit'
                    WL = fFitObj.WL;
                case 'cfit'                    
                    WL = fFitObj.p.tag;
                otherwise
                    error('Unsupported fit type.');
            end
        end
    end
    
end