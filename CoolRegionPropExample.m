clc;
clear all;
disp('Running BlobsDemo.m...');
originalImage = imread('coins.png'); % Read in standard MATLAB demo image
% binaryImage = im2bw(originalImage, 0.4); % Threshold to binary
thresholdValue = 100;
binaryImage = originalImage > thresholdValue; % Alternate way to threshold.
binaryImage = imfill(binaryImage, 'holes');

subplot(3,2,1); imagesc(originalImage); colormap(gray(256)); title('Original Image');
subplot(3,2,2); imagesc(binaryImage); colormap(gray(256)); title('Binary Image');

labeledImage = bwlabel(binaryImage, 8); % Label each blob so can do calc on it
coloredLabels = label2rgb (labeledImage, 'hsv', 'k', 'shuffle'); % pseudo random color labels

% Save the labeled image as a tif image.
% imwrite(uint16(labeledImage), 'tif_labels.tif');
% tifimage = imread('tif_labels.tif');
% imtool(tifimage, []); % Use imtool so she can inspect the values.

subplot(3,2,3); imagesc(labeledImage); title('Labeled Image');
subplot(3,2,4); imagesc(coloredLabels); title('Pseudo colored labels');

blobMeasurements = regionprops(labeledImage, 'all'); % Get all the blob properties.
numberOfBlobs = size(blobMeasurements, 1);

% bwboundaries returns a cell array, where each cell
% contains the row/column coordinates for an object in the image.
% Plot the borders of all the coins on the original
% grayscale image using the coordinates returned by bwboundaries.
subplot(3,2,5); imagesc(originalImage); title('Outlines');
hold on;
boundaries = bwboundaries(binaryImage);
for k = 1 : numberOfBlobs
thisBoundary = boundaries{k};
plot(thisBoundary(:,2), thisBoundary(:,1), 'g', 'LineWidth', 2);
end
hold off;

fprintf(1,'Blob # Mean Intensity Area Perimeter Centroid\n');
for k = 1 : numberOfBlobs % Loop through all blobs.
% Find the mean of each blob. (R2008a has a better way where you can pass the original image
% directly into regionprops. The way below works for all versions including earlier versions.)
    thisBlobsPixels = blobMeasurements(k).PixelIdxList; % Get list of pixels in current blob.
    meanGL = mean(originalImage(thisBlobsPixels)); % Find mean intensity (in original image!)
    blobArea = blobMeasurements(k).Area;	% Get area.
    blobPerimeter = blobMeasurements(k).Perimeter;	% Get perimeter.
    blobCentroid = blobMeasurements(k).Centroid;	% Get centroid.
    fprintf(1,'#%d %18.1f %11.1f %8.1f %8.1f %8.1f\n', k, meanGL, blobArea, blobPerimeter, blobCentroid);
end
msgbox('Finished running BlobsDemo.m. Check out the figure window and the command window for the results.');