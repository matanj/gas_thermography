function angle = DetectAngle(greenIm)
    edgeIm = edge(greenIm,'canny');
 
    EXPECTED_ANGLE = 0;
    ORTHOGONAL_ANGLE = EXPECTED_ANGLE + 90;
    ANGLE_RAD = 5;
    STEP = (ANGLE_RAD*2) / 100;
 
    theta1 = EXPECTED_ANGLE - ANGLE_RAD : STEP : EXPECTED_ANGLE + ANGLE_RAD;
    theta2 = ORTHOGONAL_ANGLE - ANGLE_RAD : STEP : ORTHOGONAL_ANGLE + ANGLE_RAD;
 
    In = ones(size(edgeIm));
    %     theta =
    R = radon(edgeIm,theta1)./radon( In,theta1);
    Ro = radon(edgeIm,theta2)./radon( In,theta2);
    %     figure;imagesc(theta1,1:size(R,1),R+Ro);
    Rt = R + Ro;
    Rt(isnan(Rt)) = 0;
    nContrastProfile = std(Rt);
%     figure;plot(theta1,nContrastProfile);
    [~,nMaxContrastLocation] = max(nContrastProfile);
    angle = theta2(nMaxContrastLocation);
 
end