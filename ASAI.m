function [ f ] = ASAI( r,I,y,knots )
% r & y are the radial & traversal axes.
% knots = locations of endpoints for spline fits; change for better
% resolution
  if nargin < 4
    error('invalid args!');
  end
%   if nargin < 5
%     knots = 0.:.01*R:R;
%   end

  function K1=K1(y,r)
    s=sqrt(y^2-r^2);
    K1=-1/s;
  end
  function K2=K2(y,r)
    s=sqrt(y^2-r^2);
    K2=-y/s+log(y+s);
  end
  function K3=K3(y,r)
    s=sqrt(y^2-r^2);
    K3=s-r^2/s;
  end
  function K4=K4(y,r)
    s=sqrt(y^2-r^2);
    K4=y*s/2-r^2*y/s+(3/2)*r^2*log(y+s);
  end
  
  n = numel(I)+1;
  R = max(r);
  f = zeros(1,n-1);
  % Create I_fit block with fit information.
  % pieces = # of segments
  % breaks = locations of endpoints of splines
  I_fit.pieces = size(knots,2)-1;
  I_fit.breaks = knots;
  % Fit segments with a cubic function and store coefficients.
  % a*y^3 + b*y^2 + c*y + d
  for i=1:I_fit.pieces
    [~, start_loc] = min(abs(y-I_fit.breaks(i)));
    [~, end_loc] = min(abs(y-I_fit.breaks(i+1)));
    I_fit.coefs(i,:) = polyfit(y(start_loc:end_loc),I(start_loc:end_loc),3);
  end
%% ABEL INVERSION ALGORITHM %%
% See Deutsch and Beniaminy (1982) for exact expression. Note: algorithm as
% printed needs correction. See correction notes.
  for i=1:n-2
    % Identify location of next break point.
    next = I_fit.breaks;
    next(next<r(i)) = NaN;
    [~,next_loc] = min(next);
    for j=next_loc:size(I_fit.breaks,2)-1
      f(i) = f(i) + I_fit.coefs(j,1)*(K4(I_fit.breaks(j+1),r(i))-K4(I_fit.breaks(j),r(i))) + ...
        I_fit.coefs(j,2)*(K3(I_fit.breaks(j+1),r(i))-K3(I_fit.breaks(j),r(i))) + ...
        I_fit.coefs(j,3)*(K2(I_fit.breaks(j+1),r(i))-K2(I_fit.breaks(j),r(i))) + ...
        I_fit.coefs(j,4)*(K1(I_fit.breaks(j+1),r(i))-K1(I_fit.breaks(j),r(i)));
    end
    f(i) = f(i) + I_fit.coefs(next_loc-1,1)*(K4(I_fit.breaks(next_loc),r(i))-K4(r(i+1),r(i))) + ...
      I_fit.coefs(next_loc-1,2)*(K3(I_fit.breaks(next_loc),r(i))-K3(r(i+1),r(i))) + ...
      I_fit.coefs(next_loc-1,3)*(K2(I_fit.breaks(next_loc),r(i))-K2(r(i+1),r(i))) + ...
      I_fit.coefs(next_loc-1,4)*(K1(I_fit.breaks(next_loc),r(i))-K1(r(i+1),r(i)));
    f(i) = f(i) + (I_fit.coefs(end,1)*R^3+I_fit.coefs(end,2)*R^2+I_fit.coefs(end,3)*R+I_fit.coefs(end,4))/sqrt(R^2-r(i)^2) - ...
      (I_fit.coefs(next_loc-1,1)*r(i)^3+I_fit.coefs(next_loc-1,2)*r(i)^2+I_fit.coefs(next_loc-1,3)*r(i)+I_fit.coefs(next_loc-1,4))/sqrt(r(i+1)^2-r(i)^2);
  end
  f = -f/pi;

end

