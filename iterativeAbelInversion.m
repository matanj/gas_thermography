function [ f_rec ] = iterativeAbelInversion( I, R )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
% Assume: R in pixels & odd.
I = I(:);
f_rec = zeros(size(I));
f_rec(end) = I(end);
for i = R : -1 : 2
%   f_rec(i-1) = I(i-1) - 2*sum(f_rec(i:end));
  f_rec(i-1) = I(i-1) - 1.7*sum(f_rec(i:end));
end

end

