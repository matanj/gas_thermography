%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% FLIR PTW FILE READ and TEMPERATURE CALIBRATION SAMPLE
% 
% Warning : This SDK is only compatible with FLIR SC2200, SC2500, SC5xx0, S7xx0 camera
% from FLIR ATS. it requires Vircam drivers version 5.90.004 or more.
% Image acquisiton is done through Altair v5.90.004 or more.
%
% Prerequisites :  
%   - Vircam V5.90.004 or more installed
%   - Altair V5.90.004 or more installed and running.
%
% What does this application do :
%   1. Select a PTW file to open
%   2. Retrieve file header information
%   3. Select an image to read
%   4. Read the selected image
%   5. Convert the selected image into temperature
%   6. Display image

clc

% 1. Select a PTW file to open
[filename, pathname] = uigetfile({'*.ptw', 'FLIR ATS DATA files (*.ptw)'}, 'Select a FLIR ATS PTW Data File');
if isequal(filename,0)
    errordlg('User selected Cancel','Error'); return;
end
m_filename=fullfile(pathname, filename);

% 2. Retrieve file header information
ptw.m_filename = m_filename;
ptw = FLIR_PTWFileInfo(ptw);
disp(ptw) % Display information about the PTW in Command window

% 3. Select an image to read
disp(['This film has ', num2str(ptw.m_nframes) , ' images']);
if(ptw.m_nframes == 1)
    DesiredFrameNumber = 0;
else
    DesiredFrameNumber = input(['Enter frame number to open [0 - ', num2str(ptw.m_nframes-1),'] (image index is zero based) : ']);
    if(DesiredFrameNumber<0 && DesiredFrameNumber>=ptw.m_nframes-1)
        error('ERROR : Selected frame number is invalid');
    end
end

% 4. Read the selected image
[ImageDL, ptw] = FLIR_PTWGetFrame(ptw.m_filename, DesiredFrameNumber); % Retrieve the image in digital level
if(ImageDL == 0)
    error('ERROR : Cannot read frame');
end

% 5. Convert the selected image
[ImageTemperature , CalibrationCurve] = FLIR_ConvertDLToTempHypercal(ptw , ImageDL); % Calibration Curve contains the calculated calibration. Use it in next call to reduce processing time
if(ImageTemperature == 0)
    disp('Cannot convert image to temperature');
end

% Example of reuse of CalibrationCurve
%[ImageTemperature , CalibrationCurve] = FLIR_ConvertDLToTempHypercal(ptw , ImageDL , CalibrationCurve); 

% 6. Display image
if(ImageTemperature)
    imagesc(ImageTemperature); 
    figure(gcf);
    colorbar;
end
