function [fittedR,fittedP,err,conf_bnds] = MultiITtoRP_test(IT,DL,alph,w)
% MULTIITTORP estimates the values of P and R for each pixel of an image, given a 3d 
% matrix of photos and a vector of the corresponding ITs.
%
% Mathematically, every vector along the 3rd dimension of DL represents a separate linear
% regression problem that needs to be solved. The equation whose coefficients need to be
% estimated is of the form: 
%     DL = R*IT^P
% The above equation can be transformed into a linear model after applying a logarithm:
%     log(DL) = log(R) + P*log(IT)  =>  y = a + b*x
%
% The equation system is then expressed in terms of a Vandermonde matrix which is then
% inverted to arrive at the solutions for log(R) and P.
% For more information see the documentation (or source code) for MATLAB's polyfit.
%
% ADDITION REGARDING WEIGHTING (http://www.mathworks.com/moler/leastsquares.pdf) :
% Any algorithm for solving an unweighted least squares problem can be used to solve a 
% weighted problem by scaling the observations and design matrix.
% We simply multiply both y_i and the i-th row of X by w_i. This can be accomplished with
% X = diag(W) * X;
% y = diag(W) * y;
%
% INPUTS:
% DL - 3d array where each "slice" represents a monochrome photograph of the same scene 
%      taken at different exposure times (specified by the vector IT).
% IT - Vector of unique exposure times [us]. The order of elements MUST correspond to the
%      ordering of slices (in the 3rd dimension) of DL.
% alph - Statistical significance level; the probability of rejecting the null hypothesis.
%      Default value: 0.05.
% W -  Optional weights for the polynomial fitting.
%
% OUTPUTS:
% fittedR - The estimated value of R (radiation) for all pixels within the image in question.
% fittedP - The estimated value of P (Schwartzschield's coefficient) for all pixels within 
%           the image in question.
% err     - Standards error (sigma) in each of the parameters.
% conf_bnds - Confidence bounds on parameters using the default or the provided alpha.
%           This is a 4d matrix with size [size(DL,1) size(DL,2), 2, 2], in which the 
%           3rd dimension represents the number of parameter (1 for R, 2 for P) and the 
%           4th dimension represents whether the bount is upper (:,:,:,1) or lower (:,:,:,2).
%% Input checking
if nargin < 4 || isempty(w)
  W = eye(numel(IT));
else
  W = diag(sqrt(w(:)));
end
if nargin < 3 || isempty(alph)
  alph = 0.05; % alpha, 1-confidence
end
if isscalar(IT) || size(DL,3) == 1, error('Wrong input sizes'); end
assert(size(DL,3) == numel(IT),'Mismatch in inputs'' size');
%TODO: test that IT and DL have the same ordering.
%% 
N = 2; %// Degree of polynomial
VM = getVandermondeMatrix(IT(:),N);
nParam = N+1; nPoints = numel(IT);
Y = reshape(DL,[],nPoints).';    
%% Parameter estimation:
B = reshape(((W*VM)\(W*Y)).',[size(DL(:,:,1)) N+1]);
% ^ (:,:,1) is the intercept (ln(R)); (:,:,2) is the slope (P); 
fittedR = real(B(:,:,1));
fittedP = real(B(:,:,2));
%% Error estimation:
% https://en.wikipedia.org/wiki/Hat_matrix
% http://mathoverflow.net/a/44495
% http://stats.stackexchange.com/q/68151
% http://stats.stackexchange.com/q/115011
% "Survey weighted hat matrix and leverages", Jianzhu Li and Richard Valliant, June 2009
% web(fullfile(docroot, 'curvefit/evaluating-goodness-of-fit.html'))
if nargout > 2
  DOF = nPoints-nParam;
  aH = eye(nPoints)-VM/(VM.'*(W*W)*VM)*VM.'*(W*W); %Weighted Hat matrix: H = VM/(VM.'*W*W*VM)*VM.'*W*W;
% Residuals = Y_measured - Y_estimated = Y-H*Y = (I-H)*Y = aH * Y
%        Rz = reshape(((aH * Y).')        ,size(DL));
  SSE = sum(  reshape(((aH * Y).').^2*(W*W) ,size(DL)),3);   
  % MSE = SSE/nPoints; % AKA variance in Y (DL)
  % s_regression = sqrt(SSE/DOF); % Standard error of the regression
  err = reshape(sqrt((diag(eye(nParam)/((VM.'*(W*W)*VM)))*reshape(SSE/DOF,1,[])).'),[size(SSE) N+1]);
  % ^ (:,:,1) is the error in the intercept (DL); (:,:,2) is the error in slope (DL/IT); 
  if nargout > 3
  % tinv(1-0.05/2,DOF); % Student's t, 95% confidence interval
    conf_bnds = bsxfun(@plus,bsxfun(@times,reshape(tinv(1-alph/2,DOF)*[1 -1],1,1,1,2),err),B);
  end
end
%{
%% TESTING:
X = [1,2,3,4,5];
Y = [1.4,2.2,3.5,4.9,2.3];
W = [1,1,1,1,0.1];

% OUR WAY:
[R,P,E,B] = MultiITtoRP_test(X,reshape(Y,1,1,[]),0.05,W);
[R,P,E,B] = MultiITtoRP(X,reshape(Y,1,1,[]),0.05,W,2);
Res = RadiationResult(R,P,EMPTY_FILTER,E,B);
[R,P,E,B] = MultiITtoRP_S(X,reshape(Y,1,1,[]),0.05,W,2);
Res(2) = RadiationResult(R,P,EMPTY_FILTER,E,B);

% MATLAB'S CURVEFIT TOOLBOX:
[fitresult, gof] = fit( X(:), Y(:), fittype( 'poly2' ), ...
                        fitoptions( 'Method', 'LinearLeastSquares','Weights',W(:)) );
%% COMPARISON:
norm(confint(fitresult) - rot90(permute(B,[4,3,1,2]),2))

%% Fair Comparison:


[R,P,E,B] = MultiITtoRP(X,reshape(Y,1,1,[]));
[R,P,E,B] = MultiITtoRP_S(X,reshape(Y,1,1,[]));


%}
function VM = getVandermondeMatrix(pts,degree)
VM = bsxfun(@power, pts, 0:degree); %// Vandermonde matrix