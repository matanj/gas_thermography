function out = TV_norm(im,method,isVolumetric,isIsotropic,Wx,Wy,Wz)
% TV_NORM computes the total variation (TV) norm of 3d data.
%   For more info see:
%   https://en.wikipedia.org/wiki/Total_variation
%   http://dsp.stackexchange.com/q/16271
%   chat.stackoverflow.com/transcript/message/31245978
%   chat.stackoverflow.com/transcript/message/31246341
%   "Total variation approach for adaptive nonuniformity correction in focal-plane
%    arrays", Vera et al., 2011.
%
% By Iliya @ 20/06/2016.
%{

VOLUMETRIC:
This flag determines whether the image's 3rd dimension represents "slices" of 
volumetric data (along a physical dimension), or that it is just some ordering 
of independent 2d images.

A NOTE ON ISOTROPY:
* The isotropic version is non-differentiable so it's not ideal for minimizations;
* The anisotropic version is differentiable and therefore can be used in a minimization problem.
* Typically when discussing anisotropic smoothing/blurring/whatever, the anisotropic
  alternative is designed to preserve edges. i.e. smooth _along_ a gradient.
* In the anisotropic mode, one can weight the different components differently.

%}
%% Dealing with inputs:
DEF_METHOD = 'intermediate';
DEF_VOLUMETRIC_FLAG = false;
DEF_ISOTROPIC_FLAG = true;

if nargin < 7 || isempty(Wz)
  Wz = 1;
end
if nargin < 6 || isempty(Wy)
  Wy = 1;
end
if nargin < 5 || isempty(Wx)
  Wx = 1;
end
if nargin < 4 || isempty(isVolumetric)
  isIsotropic = DEF_ISOTROPIC_FLAG;
end
if nargin < 3 || isempty(isVolumetric)
  isVolumetric = DEF_VOLUMETRIC_FLAG;
end
if nargin < 2 || isempty(method)
  method = DEF_METHOD;
end
% Make sure we're working with compatible types:
if isa(im,'gpuArray')
  im = gather(im);
end
%% Computation:
[Gx,Gy,Gz] = imgradientxyz(im,method);
if isVolumetric
  if isIsotropic
    out = sum(reshape(sqrt(Wx.*Gx.*Gx + Wy.*Gy.*Gy + Wz.*Gz.*Gz),[],1),'omitnan');
  else
    out = sum(reshape(Wx.*abs(Gx) + Wy.*abs(Gy) + Wz.*abs(Gz),[],1),'omitnan');
  end  
else
  if isIsotropic % < DEFAULT SETTING
%   Gmag = reshape(sqrt(Gx.*Gx + Gy.*Gy),size(im,1)*size(im,2),[]);
%   out = reshape(sum(Gmag,1),1,1,[]);
    out = reshape(sum(reshape(sqrt(Wx.*Gx.*Gx + Wy.*Gy.*Gy),...
                      size(im,1)*size(im,2),[]),...
                  1,'omitnan'),...
          1,1,[]);
  else
%   G = reshape(abs(Gx)+abs(Gy),size(im,1)*size(im,2),[]);
%   out = reshape(sum(G,1),1,1,[]);
    out = reshape(sum(reshape(Wx.*abs(Gx) + Wy.*abs(Gy),...
                      size(im,1)*size(im,2),[]),...
                  1,'omitnan'),...
          1,1,[]);
  end 
end
end
