function [Rk,Pk,rez] = MultiITtoRP_NL_D(IT,DL,alph,w,nnl_opts)
% MULTIITTORP_NL is the nonlinear version of the fitting algorithm which has
%  slightly better numerical properties - and should thus be used exclusively.
%
% DOUBLE-precision
%
% To test array inputs:
%   MultiITtoRP_Benchmark.runWithArrayInput_DP();
%
% Benchmark against other methods:
%   MultiITtoRP_Benchmark.benchmark_NLWLS_AllMethods();
%% Debug (verbose) mode identification:
if nargout == 0
  IS_DEBUG = true;
else
  IS_DEBUG = false;
end
%% Handle inputs:
if all(IT<1)
  LogHelper.i(LogMessage('IT seems to have been specified in units of [s] instead of [us].'));
end
      
if nargin < 2
  throw(IllegalArgumentException('MultiITtoRP_NL:InsufficientInputs','Please provide both IT and DL.'));
end

if nargin < 3
  alph = [];
end

if nargin < 4 || isempty(w)
  w = ones(1,numel(IT));
end
W = gpuArray(diag(sqrt(w(:))));

if nargin < 5
  nnl_opts = struct('tol_diff', 1E-3,...  Stopping tolerance for residual change
                    'max_iter', 1,...     Maximum allowed number of iterations
              'autofind_alpha', true,...
              'damping_factor', 0.0025);% Initial "trust region" size.
end                                     % 0.0025 was a good guess during testing.
%% Get initial guess using the linearized solver:
switch nargout
  case {0,1,2,3}
    [R0,P0] = MultiITtoRP_CnS(IT,DL,alph,w);
%   case 3
%     [R0,P0,err] = MultiITtoRP_CnS(IT,DL,alph,w);
%   case 4
%     [R0,P0,err,conf_bnds] = MultiITtoRP_CnS(IT,DL,alph,w);
  otherwise
    throw(ConfigurationException('solver:TooManyOutputs','Unsupported amount of outputs.'));
end
clear alph w
%% Gauss-Newton-Deak :)
% Prepare data for first iteration:
DL = gpuArray(permute(DL,[3,1,2])); % The dimension that corresponds to IT is made first for computational benefits
% DL = gpuArray(mtimesx(W,permute(DL,[3,1,2])));
% ^ same as: reshape(W*reshape(permute(DL,[3,1,2]),numel(IT),[]),circshift(size(DL),[0 1]));
%        or: mmx('mult',W,permute(DL,[3,1,2]));
IT = gpuArray(double(IT(:))); nIT = numel(IT);
szDL = size(DL); if numel(szDL)==2, szDL(3) = 1; end

Pk = gpuArray(real(permute(P0,[3,1,2])));
Rk = 0*Pk;
alpha = gpuArray(zeros(size(Pk)) + nnl_opts.damping_factor);

if ~all(strcmp({classUnderlying(DL),classUnderlying(IT),classUnderlying(alpha),...
                classUnderlying(Rk),classUnderlying(Pk)},'double'))
  throw(IllegalArgumentException('MultiITtoRP_NL_D:BadDataType','Make sure all matrices are of ''double'' precision!'));
end
if IS_DEBUG
  rez = weightedBy(DL - bsxfun(@times,reshape(R0,[1 size(R0)]), bsxfun(@power,IT(:),reshape(P0,[1 size(P0)]))), W, nIT);
  fprintf(1,'\nSSE at iteration %d (Gauss-Newton-Deak; double precision):\n', -1);
  fprintf(1, [repmat('%011.5f\t\t', 1, size(rez, 2)) '\n'], squeeze(sum(rez.^2,1))');
end
for k = 1:nnl_opts.max_iter
    Rk = Rfun2D(IT,Pk,DL,W); % size [1,M,N]    
    % Find the residual in the current iteration:
    rez = weightedBy(DL - Rk.*IT.^Pk,W,nIT);
    if IS_DEBUG
      fprintf(1,'\nSSE at iteration %d (Gauss-Newton-Deak; double precision):\n', k-1);
      fprintf(1, [repmat('%011.5f\t\t', 1, size(rez, 2)) '\n'], squeeze(sum(rez.^2,1))');
    end
    % Evaluate Weighted Jacobian:
%     J = evalJacobian(IT,Rk,Pk);
    J = weightedBy(evalJacobian(IT,Rk,Pk),W,nIT);

    % Evaluate Hessian:
    H = evalHessian(IT,Rk,Pk,J,rez); % Compare with diagJtJ(2) in nlinfit

    % gradient, size [M,N], scalar now for each M,N
    g = sum(J.*rez,1); % size [1,M,N]; essentially squeeze()d
  
    % Update solution:      
    if nnl_opts.autofind_alpha && k == 1 % < Should we try to find the best alpha? (computationally expensive)
      alpha = getBestAlpha(mean(alpha(~isinf(alpha)))*0.5, mean(alpha(~isinf(alpha)))*10,...
                           IT, reshape(DL,[szDL(1) 1 szDL(2:3)]), Pk, H, g, W);
      %{
      figure(); 
      subplot(2,2,1); imagesc(permute(gather(Rk),[2,3,1])); axis image; colorbar; title('R');
      subplot(2,2,3); imagesc(permute(gather(Pk),[2,3,1])); axis image;  colorbar; title('P');
      subplot(2,2,[2 4]); imagesc(permute(gather(alpha),[2,3,1])); axis image;  colorbar; title('\alpha');
      %}
    end
    Pnew = Pk - (alpha.*H).\g; 
    
    if norm(Pnew(:)-Pk(:)) <= nnl_opts.tol_diff %if less than tolerance break
      break
    end
    % Update solution for the next iterRation:
    Pk = Pnew;    
end

if IS_DEBUG
  Pk = Pnew;
  Rk = Rfun3D(IT,Pnew,DL,W);
  rez = weightedBy(DL - bsxfun(@times,Rk,bsxfun(@power,IT,Pk)),W,nIT);
  LogHelper.d(LogMessage(['SSE at iteration ' num2str(k) ' (Gauss-Newton-Deak; double precision):']));
%   fprintf(1,'\nSSE at iteration %d (Gauss-Newton-Deak; double precision):\n', k);
  fprintf(1, [repmat('%011.5f\t\t', 1, size(rez, 2)) '\n'], squeeze(sum(rez.^2,1))');
  Pk = gather(reshape(Pk,size(P0)));
  Rk = gather(reshape(Rk,size(R0)));
else
  Pk = gather(reshape(Pnew,size(P0)));
  Rk = gather(reshape(Rfun3D(IT,Pnew,DL,W),size(R0)));
end
% Report anomalies (aka "sanity checks"):
if any(Rk(:) > 400)
  LogHelper.i(LogMessage('Anomalous R values detected!'));
end

if any(Pk(:) < 0 | Pk(:) > 1)
  LogHelper.i(LogMessage('Anomalous P values detected!'));
end

function alpha = getBestAlpha(alph_min,alph_max,IT,DL,P,H,g,W)
% Find the optimal step size using a "line search" algorithm
ALPHA_EVALS = 3; % The exact amount of data points required for a fit. DO NOT TOUCH.
%% Bracketing stage
% Here we define an interval specifying the range of values of alpha:
alpha_vec = gpuArray.linspace(alph_min,alph_max,ALPHA_EVALS).';
%% Sectioning stage 
% Here we divide the bracket into subintervals, on which the minimum of the 
% objective function is approximated by polynomial interpolation.
Pnew = bsxfun(@minus,P,bsxfun(@ldivide,bsxfun(@times,alpha_vec,H),g));
Pnew = reshape(Pnew,[1 size(Pnew)]);
Rnew = Rfun3D(IT,Pnew,DL,W);
% P and R are [1*ALPHAS*M*N], DL is [O*1*M*N]

% Next we evaluate the new residuals (for every alpha); P,R are the same for all IT so we bsxfun
rez = reshape(sum(reshape(weightedBy(...
                bsxfun(@minus,DL,bsxfun(@times,Rnew,bsxfun(@power,IT,Pnew))),...
                W,numel(IT)).^2,numel(IT),[]),1),ALPHA_EVALS,[]);
% Every column of rez represents a parabola fitting problem that may be solved
% very fast using the "Vandermonde" approach of Luis.
% Test against:   polyfit(gather(alpha_vec(:).\1),gather(rez(:,1)),2)
poly2_coeffs = bsxfun(@power, alpha_vec.^-1, 0:2)\rez;
alpha = reshape(-2*poly2_coeffs(3,:)./poly2_coeffs(2,:),size(P));
alpha(isnan(alpha)) = Inf; % fix for some numeric nonsense when guess is VERY good

function R = Rfun3D(IT,P,DL,W)
% Compute optimal R for given P in the least squares sense
% INPUTS:
%   R,P are [O,M,N]
%   IT is [O,1]
%   DL is [O,M,N]
% OUTPUTS:
%   R(P) [O,M,N]  
R = bsxfun(@rdivide,...
  sum(weightedBy(bsxfun(@times,DL,bsxfun(@power,IT,P)),W,numel(IT)),1),...
  sum(weightedBy(bsxfun(@power,IT,2*P),W,numel(IT)),1));
% Possible modification to Rfun3D:
% sum(bsxfun(@power,IT,2*P),1)
% sum(power(10,bsxfun(@times,log10(IT),2*P)),1

function R = Rfun2D(IT,P,DL,W)
% compute optimal R for given P in the least squares sense
% R,P are [1,M,N]
% IT is [O,1]
% DL is [O,M,N]
% output Ropt(P) is also [1,M,N]
R = sum(weightedBy(DL.*IT.^P,    W,numel(IT)),1) ./ ...
    sum(weightedBy(    IT.^(2*P),W,numel(IT)),1);
                     
function J = evalJacobian(IT,R,P)
% compute Jacobian
% single variable P left --> [O,1] size of Jacobian for each [M,N]
% make it [O,M,N] again on output

% math: res(i) = DL(i) - R*IT(i)^P = DL(i) - R*exp(P*log(IT(i)))
%       J(i,j) = d(res(i))/d(P(j)) = - R(j)*log(IT(i)) * IT(i)^P(j)
% (but now j is a single scalar, one for each O index)
J = -bsxfun(@times,R,log(IT)).*bsxfun(@power,IT,P);

function H = evalHessian(IT,R,P,J,rez)
% Hessian with 1 variable is just the second derivative, scalar
% output is [1,M,N] matrix (one scalar for every M*N case)
H = sum(... the 2* cancels out with g
  J.^2 + ...
... bsxfun(@times,R,log(IT)   ).*bsxfun(@power,IT,P).^2 + ... % J*J
  bsxfun(@times,R,log(IT).^2).*bsxfun(@power,IT,P).*rez ...
  ,1);  %reshape() is actually a squeeze() over first dim
  
function S = weightedBy(s,W,nRows)
S = reshape(W*reshape(s,nRows,[]),size(s));