function out = iftr(cond,in1,in2)
%IFTR is a ternary operator implementation
% note: unlike "&&" and "||" this function does not have lazy evaluation capabilities, 
% thus both inputs need to be known before this function executes.
if cond
    out = in1;
else
    out = in2;
end