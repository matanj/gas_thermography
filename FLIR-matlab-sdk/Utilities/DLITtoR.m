function [fittedR,fittedP,err,conf_bnds] = DLITtoR(IT,DL,varargin)
% DLITTOR redirects inputs into the appropriate solver, to obtain estimates for 
% the values of P and R for each pixel of an image, given a 3d matrix of photos 
% and a vector of the corresponding ITs.
%% Defaults:
DEF_SOLVER = "nonlin";
DEF_CNS = false; % Centering and Scaling toggle (for the linear solver)
DEF_PRECISION = "double"; % For the nonlinear solver
DEF_SIGNIFICANCE = [];
DEF_WEIGHTING = [];
DEF_SUPERFRAMING = true;
%% Input handling:
if nargin < 2
  throw(IllegalArgumentException('DLITtoR:insufficientArguments',...
    'Please specify at least a vector of IT and a 3D array DL of corresponding size.'));
end
p = inputParser();
p.addParameter("solver", DEF_SOLVER, @(x)any( x == ["lin","nonlin"]) );
p.addParameter("cns", DEF_CNS, @(x)islogical(x) && isscalar(x) );
p.addParameter("precision", DEF_PRECISION, @(x)any( x == ["single","double"]));
p.addParameter("alpha", DEF_SIGNIFICANCE, @(x)isscalar(x) && x > 0 && x < 1);
p.addParameter("w", DEF_WEIGHTING, @(x)isnumeric(x) && isvector(x));
p.addParameter("sf", DEF_SUPERFRAMING, @(x)islogical(x) && isscalar(x) );

p.parse(varargin{:});
solver = p.Results.solver;
cns = p.Results.cns;
precision = p.Results.precision;
alpha = p.Results.alpha;
w = p.Results.w;
sf = p.Results.sf;
%% Sanity checks:
% Input Sizes:
if isscalar(IT) || size(DL,3) == 1, error('Wrong input sizes'); end
assert(size(DL,3) == numel(IT),'Mismatch in inputs'' size');
% Input ordering (i.e. that low IT correspond to low DL etc.):
# TODO!

%% Redirect:
switch solver 
  case "lin"
    if cns
      [fittedR,fittedP,err,conf_bnds] = MultiITtoRP_CnS(IT,DL,alpha,w);
    else
      [fittedR,fittedP,err,conf_bnds] = MultiITtoRP(IT,DL,alpha,w);
    end    
  case "nonlin"  
    if precision == "double"
      if sf % Find way to pass in args for nnl_opts & sfMask
        [fittedR,fittedP,err,conf_bnds] = MultiITtoRP_NL_D_SF(IT,DL,alpha,w,[],[]);
      else
        [fittedR,fittedP,err,conf_bnds] = MultiITtoRP_NL_D(IT,DL,alpha,w,[]);
      end
    else
      [fittedR,fittedP,err,conf_bnds] = MultiITtoRP_NL_S(IT,DL,alpha,w);
    end
end
