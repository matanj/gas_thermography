function [ name ] = nameFromMetaclass( mc )
  % NAMEFROMMETACLASS retreives the class name of metaclasses / objects, 
  % usually to be used in isa(obj,'classname') calls. 
  %
  % The idea is use as few hardcoded strings as possible, including the inputs to isa(...,'!!')
  if isobject(mc) && isprop(mc,'Name')
    name = mc.Name;
  else
    name = nameFromMetaclass(metaclass(mc));
  end
end