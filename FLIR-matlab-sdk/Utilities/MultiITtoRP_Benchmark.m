classdef MultiITtoRP_Benchmark
  %MULTIITTORP_BENCHMARK Summary of this class goes here
  %   Detailed explanation goes here

  methods (Access = public, Static = true)
    function runWithArrayInput_DP %TODO: allow function to take inputs
      [IT,DL] = MultiITtoRP_Benchmark.generateInputs();
      DL = permute(DL,[2,3,1]); % Reshape/permute to simulate experimental inputs
      clc; MultiITtoRP_NL_D(IT,DL);
    end
    
    function runWithArrayInput_SP %TODO: allow function to take inputs
      [IT,DL] = MultiITtoRP_Benchmark.generateInputs();
      DL = permute(DL,[2,3,1]); % Reshape/permute to simulate experimental inputs
      clc; MultiITtoRP_NL_S(IT,DL);
    end
    
    function benchmark_NLWLS_AllMethods
      %%%%%% RESULT COMPARISONS: %%%%%
      clc; rng(1337);
      X = 100:100:1000;
      Y = X.^0.3 + sqrt(X).*randn(1,10)*0.001;
      W = sqrt(1:10);
      % Here we provide the methods the same good initial guesses.
      [F,gof] = fit(X(:),Y(:),'power1','Weights',W,'Lower',[0,0],'Upper',[2,2],'StartPoint',[0.997227451252400, 0.300298496542844]);
      fprintf(1,'----- METHOD 1: MATLAB''s fit -----\nR = %1.16f\nP = %1.16f\nSSE = %1.14f\n',F.a,F.b,gof.sse);
      [beta,gof] = nlinfit(X(:),Y(:),@(t,x)t(1).*x.^t(2),[0.996751847380784,0.300372978833992],'Weights',W(:));
      fprintf(1,'----- METHOD 2: MATLAB''s nlinfit -----\nR = %1.16f\nP = %1.16f\nSSE = %1.14f\n',beta(1),beta(2),gof.'*gof);
      [R,P,gof] = MultiITtoRP_NL_D(X,reshape(Y,1,1,[]),[],W);
      fprintf(1,'----- METHOD 3: Iliya''s NLWLS (D) -----\nR = %1.16f\nP = %1.16f\nSSE = %1.14f\n',R,P,gof.'*gof);
      [R,P,gof] = MultiITtoRP_NL_S(X,reshape(Y,1,1,[]),[],W);
      fprintf(1,'----- METHOD 4: Iliya''s NLWLS (S) -----\nR = %1.16f\nP = %1.16f\nSSE = %1.14f\n',R,P,gof.'*gof);
      %%%%%% RUNTIME COMPARISONS: %%%%%
      % Here we don't provide bounds or initial guesses so that the comparison is (more) fair
      t(4) = timeit(@() MultiITtoRP_NL_S(X,reshape(Y,1,1,[]),[],W) );
      t(1) = timeit(@() fit(X(:),Y(:),'power1','Weights',W) );
      t(2) = timeit(@() nlinfit(X(:),Y(:),@(t,x)t(1).*x.^t(2),[0.1,0.1],'Weights',W(:)) );
      t(3) = timeit(@() MultiITtoRP_NL_D(X,reshape(Y,1,1,[]),[],W) );
      fprintf(1,'\n----- METHOD TIMINGS (1 pixel) -----\nt(M1) = %1.16f\nt(M2) = %1.16f\nt(M3) = %1.16f\nt(M4) = %1.16f\n\n',t(:));

    end
  end
  
  methods (Access = private, Static = true)
    function [IT,DL] = generateInputs() %TODO: allow function to take inputs
      % Setup:
      NoiseAmplitude = 50;
      R_mean = 10;   R_var = 2;
      P_mean = 0.94; P_var = 0.01;
      IT_min = 100; IT_max = 1500;
      M = 4; N = 3; O = 15;

      %% Measurement generation 
      if false % Simplified generation
        rng(1337); % Initialize PRNG with a known seed.
        IT = linspace(IT_min,IT_max,O).';
        DL = R_mean * IT.^P_mean + NoiseAmplitude*sqrt(IT/max(IT)).*randn(size(IT));
        DL = repmat(DL,1,M,N);
      else
        % NOTE: there's a difference between the generated values on CPU and GPU despite
        % the same seed being used.
        if false % GPU-generation
          parallel.gpu.rng(1337); % Initialize PRNG with a known seed.

          real_R = gpuArray.randn(1,M,N)*R_var + R_mean;
          real_P = gpuArray.randn(1,M,N)*P_var + P_mean;

          IT = gpuArray.linspace(IT_min,IT_max,O).';
          DL = bsxfun(@times,real_R,bsxfun(@power,IT,real_P)) + ... add some noise:
               bsxfun(@times,NoiseAmplitude*sqrt(IT/max(IT)),gpuArray.randn(O,M,N));
        else     % CPU-Generation
          rng(1337); % Initialize PRNG with a known seed.

          real_R = randn(1,M,N)*R_var + R_mean;
          real_P = randn(1,M,N)*P_var + P_mean;

          IT = linspace(IT_min,IT_max,O).';
          DL = bsxfun(@times,real_R,bsxfun(@power,IT,real_P)) + ... add some noise:
               bsxfun(@times,NoiseAmplitude*sqrt(IT/max(IT)),randn(O,M,N));
        end
      end
    end
    
    function opts = getTestingOptionsStruct()
      opts = struct('tol_diff', 1E-8,...  Stopping tolerance for residual change
                    'max_iter', 10,...    Maximum allowed number of iterations
              'autofind_alpha', true,...
              'damping_factor', 0.0025);% Initial "trust region" size.
    end
  end
  
end

