function [Rk,Pk,rez] = MultiITtoRP_NL_D_SF(IT,DL,alph,w,nnl_opts,sfMask)
% MULTIITTORP_NL is the nonlinear version of the fitting algorithm which has
%  slightly better numerical properties - and should thus be used exclusively.
%
%  SF signifies that this is meant for data with superframes, i.e. where not all
%  DL(IT) are considered for each pixel. Those not considered have W==0.
%
% DOUBLE-precision
%
% To test array inputs:
%   MultiITtoRP_Benchmark.runWithArrayInput_DP();
%
% Benchmark against other methods:
%   MultiITtoRP_Benchmark.benchmark_NLWLS_AllMethods();
%% Debug (verbose) mode identification:
if nargout == 0
  IS_DEBUG = true;
else
  IS_DEBUG = false;
end
%% Handle inputs:
if all(IT<1)
  LogHelper.i(LogMessage('IT seems to have been specified in units of [s] instead of [us].'));
end
      
if nargin < 2
  throw(IllegalArgumentException('MultiITtoRP_NL:InsufficientInputs','Please provide both IT and DL.'));
end

if nargin < 3
  alph = [];
end

if nargin < 6 || isempty(sfMask)
  LogHelper.v(LogMessage('Superframes not specified, applying automatic settings.'));
  minDL = 200; maxDL = 12500;
  sfMask = minDL <= DL & DL <= maxDL;
end

if nargin < 4 || isempty(w)
  w = ones(numel(IT),1);
end
W = gpuArray(permute(sfMask.*sqrt(reshape(w,1,1,[])), [3,1,2]) );

if nargin < 5 || isempty(nnl_opts)
  nnl_opts = struct('tol_diff', 1E-3,...  Stopping tolerance for residual change
                    'max_iter', 1,...     Maximum allowed number of iterations
              'autofind_alpha', true,...
              'damping_factor', 0.0025);% Initial "trust region" size.
end                                     % 0.0025 was a good guess during testing.

%% Get initial guess using the linearized solver:
switch nargout
  case {0,1,2,3}
    [R0,P0] = MultiITtoRP_CnS(IT,DL,alph,w); % TODO: MODIFY FOR SF
%   case 3
%     [R0,P0,err] = MultiITtoRP_CnS(IT,DL,alph,w);
%   case 4
%     [R0,P0,err,conf_bnds] = MultiITtoRP_CnS(IT,DL,alph,w);
  otherwise
    throw(ConfigurationException('solver:TooManyOutputs','Unsupported amount of outputs.'));
end
clear alph w
%% Gauss-Newton-Deak :)
% Prepare data for first iteration:
DL = gpuArray(permute(DL,[3,1,2])); % The dimension that corresponds to IT is made first for computational benefits
% DL = gpuArray(mtimesx(W,permute(DL,[3,1,2])));
% ^ same as: reshape(W*reshape(permute(DL,[3,1,2]),numel(IT),[]),circshift(size(DL),[0 1]));
%        or: mmx('mult',W,permute(DL,[3,1,2]));
IT = gpuArray(double(IT(:))); nIT = numel(IT);
szDL = size(DL); if numel(szDL)==2, szDL(3) = 1; end

Pk = gpuArray(real(permute(P0,[3,1,2])));
Rk = 0*Pk;
alpha = gpuArray(zeros(size(Pk)) + nnl_opts.damping_factor);

if ~all(strcmp({classUnderlying(DL),classUnderlying(IT),classUnderlying(alpha),...
                classUnderlying(Rk),classUnderlying(Pk)},'double'))
  throw(IllegalArgumentException('MultiITtoRP_NL_D:BadDataType','Make sure all matrices are of ''double'' precision!'));
end
if IS_DEBUG
  rez = W .* (DL - reshape(R0,[1 size(R0)]) .* IT(:).^reshape(P0,[1 size(P0)]));
  fprintf(1,'\nSSE at iteration %d (Gauss-Newton-Deak; double precision):\n', -1);
  fprintf(1, [repmat('%011.5f\t\t', 1, size(rez, 2)) '\n'], squeeze(sum(rez.^2,1))');
end
for k = 1:nnl_opts.max_iter
    Rk = Rfun(IT,Pk,DL,W); % size [1,M,N]    
    % Find the residual in the current iteration:
    rez = W.*(DL - Rk.*IT.^Pk);
    if IS_DEBUG
      fprintf(1,'\nSSE at iteration %d (Gauss-Newton-Deak; double precision):\n', k-1);
      fprintf(1, [repmat('%011.5f\t\t', 1, size(rez, 2)) '\n'], squeeze(sum(rez.^2,1))');
    end
    % Evaluate Weighted Jacobian:
%     J = evalJacobian(IT,Rk,Pk);
    J = W.*evalJacobian(IT,Rk,Pk);

    % Evaluate Hessian:
    H = evalHessian(IT,Rk,Pk,J,rez); % Compare with diagJtJ(2) in nlinfit

    % gradient, size [M,N], scalar now for each M,N
    g = sum(J.*rez,1); % size [1,M,N]; essentially squeeze()d
  
    % Update solution:      
    if nnl_opts.autofind_alpha && k == 1 % < Should we try to find the best alpha? (computationally expensive)
      alpha = getBestAlpha(mean(alpha(~isinf(alpha)))*0.5, mean(alpha(~isinf(alpha)))*10,...
                           IT, reshape(DL,[szDL(1) 1 szDL(2:3)]), Pk, H, g,...
                           permute(W,[1,4,2,3]));
      %{
      figure(); 
      subplot(2,2,1); imagesc(permute(gather(Rk),[2,3,1])); axis image; colorbar; title('R');
      subplot(2,2,3); imagesc(permute(gather(Pk),[2,3,1])); axis image;  colorbar; title('P');
      subplot(2,2,[2 4]); imagesc(permute(gather(alpha),[2,3,1])); axis image;  colorbar; title('\alpha');
      %}
    end
    Pnew = Pk - (alpha.*H).\g; 
    
    if norm(Pnew(:)-Pk(:)) <= nnl_opts.tol_diff %if less than tolerance break
      break
    end
    % Update solution for the next iterRation:
    Pk = Pnew;    
end

if IS_DEBUG
  Pk = Pnew;
  Rk = Rfun(IT,Pnew,DL,W);
  rez = W .* (DL - Rk.*IT.^Pk);
  LogHelper.d(LogMessage(['SSE at iteration ' num2str(k) ' (Gauss-Newton-Deak; double precision):']));
%   fprintf(1,'\nSSE at iteration %d (Gauss-Newton-Deak; double precision):\n', k);
  fprintf(1, [repmat('%011.5f\t\t', 1, size(rez, 2)) '\n'], squeeze(sum(rez.^2,1))');
  Pk = gather(reshape(Pk,size(P0)));
  Rk = gather(reshape(Rk,size(R0)));
else
  Pk = gather(reshape(Pnew,size(P0)));
  Rk = gather(reshape(Rfun(IT,Pnew,DL,W),size(R0)));
end
% Report anomalies (aka "sanity checks"):
if any(Rk(:) > 400)
  LogHelper.i(LogMessage('Anomalous R values detected!'));
end

if any(Pk(:) < 0 | Pk(:) > 1)
  LogHelper.i(LogMessage('Anomalous P values detected!'));
end

function alpha = getBestAlpha(alph_min,alph_max,IT,DL,P,H,g,W)
% Find the optimal step size using a "line search" algorithm
ALPHA_EVALS = 3; % The exact amount of data points required for a fit. DO NOT TOUCH.
%% Bracketing stage
% Here we define an interval specifying the range of values of alpha:
alpha_vec = gpuArray.linspace(alph_min,alph_max,ALPHA_EVALS).';
%% Sectioning stage 
% Here we divide the bracket into subintervals, on which the minimum of the 
% objective function is approximated by polynomial interpolation.
Pnew = P - (alpha_vec.*H) .\ g;
Pnew = reshape(Pnew,[1 size(Pnew)]);
Rnew = Rfun(IT,Pnew,DL,W);
% P and R are [1*ALPHAS*M*N], DL is [O*1*M*N]

% Next we evaluate the new residuals (for every alpha); P,R are the same for all IT so we expand
rez = reshape(sum(reshape( (W.*(DL - Rnew .* IT.^Pnew)).^2,numel(IT),[]),1),ALPHA_EVALS,[]);
% Every column of rez represents a parabola fitting problem that may be solved
% very fast using the "Vandermonde" approach of Luis.
% Test against:   polyfit(gather(alpha_vec(:).\1),gather(rez(:,1)),2)
poly2_coeffs = alpha_vec.^-(0:2)\rez;
alpha = reshape(-2*poly2_coeffs(3,:)./poly2_coeffs(2,:),size(P));
alpha(isnan(alpha)) = Inf; % fix for some numeric nonsense when guess is VERY good

function R = Rfun(IT,P,DL,W)
% compute optimal R for given P in the least squares sense
% R,P are [1,M,N]
% IT is [O,1]
% DL is [O,M,N]
% output Ropt(P) is also [1,M,N]
R = sum(W.* DL.*IT.^P     ,1) ./ ...
    sum(W.*     IT.^(2*P) ,1);
                     
function J = evalJacobian(IT,R,P)
% compute Jacobian
% single variable P left --> [O,1] size of Jacobian for each [M,N]
% make it [O,M,N] again on output

% math: res(i) = DL(i) - R*IT(i)^P = DL(i) - R*exp(P*log(IT(i)))
%       J(i,j) = d(res(i))/d(P(j)) = - R(j)*log(IT(i)) * IT(i)^P(j)
% (but now j is a single scalar, one for each O index)
J = -R .* log(IT).* IT.^P ;

function H = evalHessian(IT,R,P,J,rez)
% Hessian with 1 variable is just the second derivative, scalar
% output is [1,M,N] matrix (one scalar for every M*N case)
H = sum( J.^2 + R.*log(IT).^2 .* IT.^P .* rez ,1);