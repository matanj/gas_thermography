function [ T ] = MultiRtoT( RP, eps_vec, T_vec, nMethod, recursionDepth)
% MULTIRTOT attempts to find temperatures for an image based on
% multispectral data.
%
% INPUTS:
% RP - A vector of RadiationResult objects. Has to be non-empty, minimum numel() == 1.
% eps_vec - Vector representing the emissivity search grid.
% T_vec - Vector representing the temperature search grid. [K]
% nMethod - Number of the method to use to find T. Default: 2 (Iliya's adaptive consensus).
% recursionDepth - Used to track the recursion depth (for adaptive solutions). UNUSED
%
% OUTPUTS:
% T - The estimated temperature [K]
%
% Explanation of the N>=3 algorithm:
% 1. Images of radiation are obtained for each pixel. => data of size X*Y*N , 1<=N<=4
%    CODE: cat(3,data.R) 
% 2. A vector of emissivities is generated. 
%    CODE: eps_vec = linspace(EPS_LIMS(1),EPS_LIMS(2),100);
% 3. Radiation data is divided by the emissivity vector to obtained BB radiation 
%    => X*Y*3*numel(eps linspace) CONTAINS BB RADIATIONS
%    CODE: bsxfun(@rdivide,cat(3,data.R),permute(eps_vec,[1,3,4,2]))
% 4. BB radiations are converted to temperature using known calibration,
%    => X*Y*3*numel(eps linspace) CONTAINS TEMPERATURES
%    CODE: T, @computeTforEps
% 5. Temperature agreement is sought using minimal stdev in temperature ("eps-T intersections"). 
%    => X*Y*3*EPS 
%       -> stdev over colors (dim 3) => array of discrepancies in T for each guessed epsilon. 
%       -> min over emissivity (dim 4) -> [I] array of emissivity indices for which the 
%          temperature is closest between two considered filters (intersection).
% 6. Using the matrix I (2d map containing the best emissivity value for each pixel), 
%    the actual value of the best emissivity is obtained using the grid definition.
% 7. Corresponding "best temperatures" are retrieved from the mean of the temperatures in 
%    the positions of highest agreement in T, and are stored for each combo.
%    CODE: T_intersect
% 8. 5-7 are done for every combination of 2 filters.

%% Definitions
DEBUG = false;
% PADDING_STR = '_{ }^{ }'; %For plotting
% SB = 5.67036713E-8; %Stefan�Boltzmann constant
DEF.T_VEC = linspace(30,700,50)+273.15;
DEF.RECURSION_DEPTH = 0;
DEF.EPS_VEC = linspace(1E-1,2E0,500);
DEF.METHOD = 3;
%% Input Validation
validateattributes(RP,{'RadiationResult'},{'nonempty','vector'});

if nargin < 2 || isempty(eps_vec)
  eps_vec = DEF.EPS_VEC;
end
if nargin < 3 || isempty(T_vec)
  T_vec = DEF.T_VEC;
end
if nargin < 4 || isempty(nMethod)
  nMethod = DEF.METHOD;
end
if nargin < 5 || isempty(recursionDepth)
  recursionDepth = DEF.RECURSION_DEPTH; %#ok
end
CWL = getCWL([RP.F]); % [nm]
NUM_COLORS = numel(RP);

%% Fits, ratio derivatives etc.
lfh = RingLatestFitHelper();
[R2T{NUM_COLORS},T2R{NUM_COLORS}] = deal([]);
for indC = 1:NUM_COLORS
  R2T{indC} = lfh.getR2TwithFilter(RP(indC).F);
  T2R{indC} = lfh.getT2RwithFilter(RP(indC).F);
end
if NUM_COLORS == 1
  % Do single-color based directly on calibration using the provided emissivity. 
  % NEEDS TO BE PRACTICALLY INSTANT
  % TODO
else % N>=2
%% Getting derivatives of ratio curves:
combos = VChooseK(1:NUM_COLORS,2);
% Invert problematic ratios (make sure all ratios are > 1)
% Currently manual, we should do this automatically based on "filter size" or something
pos34 = find(getCWL(F3460)==CWL,1); pos36 = find(getCWL(F3600)==CWL,1); 
if ~isempty(pos34) && ~isempty(pos36)
  [tf,L] = ismember([pos34 pos36],combos,'rows');
  if tf
    combos(L,:) = flip(combos(L,:));
  end
end
clear pos34 pos36 L tf

nCombos = size(combos,1);
dRatdT = cell(nCombos,1);
for indC = 1:nCombos
  dRatdT{indC} = lfh.T2RatDiff(combos(indC,1),combos(indC,2),1);
end
switch NUM_COLORS 
  case {3,4}
    info_content = getDerivativeInfoContent();
  case 2
    info_content = ~~T_vec; % Shortest way to get a vector of ones the same size as T
  case 1
    info_content = NaN;
  otherwise
    error('Unexpected number of colors!');
end
%% Shift Images:
RP = shiftImages(RP, ShiftingHelper({[],[]},...
                                    'doDownsample', true,...
                                    ...'DistFromBound', 20*[1 1],...
                                    ...'TemplateSizeFr',0.4,...
                                    'isManual', false,...
                                    'ResizeMethod','bicubic'));
im_size = size(RP(1).R);                                  
%% Gray body assumption:
eps_func = @(eps_vec,cwl)eps_vec;
[Tbb,~] = computeTforEps(eps_func,eps_vec,CWL);
T = findConsensusForT(info_content,Tbb,'Consensus');
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% %%%%%%%% Helper Functions %%%%%%%% %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
function [Tbb,blackR] = computeTforEps(eps_func,epsfun_in1,epsfun_in2)
  eps_vec = eps_func(epsfun_in1,epsfun_in2); % Supposedly supports some function of CWL
  [Tbb,blackR] = deal(bsxfun(@rdivide,cat(3,RP.R),permute(eps_vec,[1,3,4,2])));
%   ################# Keep debugging from here and down
  for indF = 1:NUM_COLORS
    Tbb(:,:,indF,:) = reshape(R2T{indF}(blackR(:,:,indF,:)),size(Tbb)./[1 1 NUM_COLORS 1]);
  end
end

function out = findConsensusForT(info_content,Tbb,~)
  %
  % A double matrix takes up ~8MB per 512x640x3 slice. If we have 4500 of those it would
  % require ~36GB of memory....
  %
  % TODO: Think about a solution to avoid out-of-memory issues. Some alternatives:
  % - Blockwise (loop-based / recursive) solution, where only a portion of the image is 
  %   considered at a time.
  % - Have an adaptive mesh for emissivity so that we don't end up doing needless 
  %   computations and allocating monster arrays. All it requires is to modify EPS_LIMS
  %   and re-run the script.
  % - Run the "1 value" script first (we did the proof of concept 3-color with this), get
  %   some "mean emissivity" and only then run the full 2d script with some small 
  %
  out = []; % Initialize output
  FIRST_OF_THE_TWO = 1; % We just need to select any element from the pair of the combo
  %% Prepare data  
  R_m = coder.nullcopy(zeros([numel(T_vec) NUM_COLORS])); %preallocation
  eps_m = coder.nullcopy(zeros([im_size NUM_COLORS numel(T_vec)])); %preallocation
  for indF = 1:NUM_COLORS
    % Convert our arbitrary temperature vector into BB radiations:
    R_m(:,indF) = T2R{indF}(T_vec.');
    % Divide the measurement data by the different BB radiations to get the "needed eps
    % for this to be correct":
    eps_m(:,:,indF,:) = bsxfun(@rdivide,RP(indF).R,permute(R_m(:,indF),[3 2 1]));
  end  
  clear R_m indF
  [eps_intersect,T_intersect] = deal(NaN([im_size nCombos]));
  for indR = 1:nCombos
    % Locating intersections between combos:
    [M,I] = min(std(Tbb(:,:,combos(indR,:),:),0,3),[],4);
      % ^ an "intersection" is identified by the emissivity for which temperatures from 
      % two filters have the smallest difference. M is quite useless as it only contains
      % "disagreement" info. The I matrix contains the 4th-D "slice" index of the best data.
    
    % Create a logical indexing matrix to select the best values
    eps_intersect(:,:,indR) = sum(sum(eps_m(:,:,combos(indR,FIRST_OF_THE_TWO),:).*...
      bsxfun(@eq,I,permute(1:size(eps_m,4),[3 4 1 2])) ,4),3);    
    T_intersect(:,:,indR)   = sum(sum(mean(Tbb(:,:,combos(indR,:),:),3).*           ...
      bsxfun(@eq,I,permute(1:size(Tbb,    4),[3 4 1 2])) ,4),3);
    % NaNify invalid pixels:
    switch NUM_COLORS 
      case {3,4}      
        invalid = M > 1E-2 | I == 1 | I == numel(T_vec); % ### FIXME to detect "no intersection"
      case {2,1}
        invalid = T_intersect(:,:,indR) < DEF.T_VEC(1) | T_intersect(:,:,indR) > DEF.T_VEC(end);  
    end
    %DEBUG: figure(); spy(~invalid); axis image;
    eps_intersect(:,:,indR) = eps_intersect(:,:,indR).*~invalid./+~invalid;
    T_intersect(:,:,indR)   =   T_intersect(:,:,indR).*~invalid./+~invalid;
  end
 
  % DEBUG: T_intersect - 273.15
  if all(isnan(T_intersect(:)))
    disp('No intersections found. Aborting iterative search.');
  end

  % Method 1 - minimal stdev on eps (using all colors)
  % [~,indT] = nanmin(std(eps_m,0,3),[],4);  
  % < removed since not good>
  if any(nMethod == [2,3])
  % Method 2 - simple average between intersection temperatures
  out = nanmean(T_intersect,3); % This is also the initial guess for Method 3
  if DEBUG
    disp(['Peak of 3-gaussian fit to temperature distribution of intersection averages: ['...
       8 num2str(HistogramHelper.getFittedGauss3Peak(out)-273.15) ']' 8 ' �C']);    
  % MORE DEBUG: 
  % figure(); histogram(T_m2(:)-273.15,352.5:5:497.5);
  % figure(); imagesc(T_m2-273.15); caxis([420 450]); colorbar; axis image; colormap([0 0 0; flipud(repelem(flag(3),40,1)); 0 0 0]);
  end
  
  end
  
  if nMethod == 3 && NUM_COLORS >= 3
  % Method 3 - weighted average between intersection temperatures based on 
  %            "measurement information content" (ratio derivative)
  %% Iterate until a consensus is found
  out = zeros(size(out))+nanmean(out(:));
  % Renormalize info_content weights based on unfound intersections:
  info_content = bsxfun(@times,~isnan(T_intersect),permute(info_content,[4 3 2 1]));
  info_content = bsxfun(@rdivide,abs(info_content),sum(abs(info_content),3));
  oldNorm = zeros(100,1);
  for indL = 1:100
    % For each pixel, find the correct slice index in the info_content matrix. Here this 
    % is done by finding a zero crossing in the TEMPERATURE vector.
    [~,I] = max(bsxfun(@minus,out,permute(T_vec,[1,3,2])) < 0,[],3);
    % After finding the correct slice for each pixel, the intersection temperatures need
    % to be summed according to the right weights.
    newT = nansum(T_intersect.*nansum(bsxfun(@times,...
      bsxfun(@eq,I,permute(1:size(info_content,4),[3 4 1 2])),info_content),4),3);  
    oldNorm(indL) = norm(out(:)-newT(:),2);
    if indL == 1 || oldNorm(indL-1) - oldNorm(indL) > 1E-1
      out = newT;
      % <ADAPTIVE MESH REFINEMENT> T_vec = linspace(0.9*nanmin(T_m3(:)),1.1*nanmax(T_m3(:)),10);
    else
      oldNorm = oldNorm(~~oldNorm); % Trim zeros from vector
      break
    end
  end
  out(out==0) = NaN;
  if indL == 100
    disp(['Maximum iteration number reached without convergence. MCT: [' 8 ...
      num2str(HistogramHelper.getFittedGauss3Peak(out-273.15)) ']' 8 ' �C']);
  else
    if DEBUG
    disp(['Consensus temperature reached after ' num2str(indL) ' iterations, with norm2 of '...
      num2str(oldNorm(end)) '. Final temperature: [' 8 num2str(...
      HistogramHelper.getFittedGauss3Peak(out-273.15)) ']' 8 ' �C']);
    end
  end
  if sum(isnan(out)) > 0.2*numel(out)
    warning(['A large number of NaN values was detected in the temperature map. ' ... 
             'This may be due to insufficiently fine T/eps grid. Current refinement in (T,eps): ('...
             num2str(numel(T_vec)) ',' num2str(numel(eps_vec)) ').']);
  end
  % figure(); histogram(T_m3(:)-273.15,352.5:5:497.5);
  % figure(); imagesc(T_m3-273.15); caxis([420 450]); colorbar; axis image; colormap([0 0 0; flipud(repelem(flag(3),40,1)); 0 0 0]);
  end
end

%{
function getBeniDeltas(eps_vec,T_at_eps) 
  % Method 4 - Beni's weighting:
%   while true
    for indF = size(combos,1):-1:1
      RBB_minus(:,indF) = SB*abs(T_at_eps(combos(indF,1),:).^4-T_at_eps(combos(indF,2),:).^4).';      
      T_avg(:,indF) = mean(T_at_eps(combos(indF,:),:),1).';
      R_avg(:,indF) = SB*T_avg(:,indF).^4;
      W(:,indF) = dRatdT{indF}(T_avg(:,indF));
    end
    W = bsxfun(@rdivide,abs(W),sum(abs(W),2)); 
    Delta1 = sum(RBB_minus.*W,2);
    Delta2 = sum(RBB_minus.*W./R_avg,2);
    % ((TBB3-TBB1)/Tave13*R13+ (TBB2-TBB1)/Tave12*R12+ (TBB2-TBB3)/Tave23*R23)/(R13+R12+R23)
    % ((TBB3-TBB1)*R13+ (TBB2-TBB1)*R12+ (TBB2-TBB3)*R23)/(R13+R12+R23)
%   break;
%   end
%     figure('Position',[1057,404,756,624]); plotyy(eps_vec,Delta1,eps_vec,Delta2);
%     xlabel('Assumed \epsilon_{eff_{ }}','FontSize',18);
%     title('\Delta vs \epsilon','FontSize',24); grid on; grid minor;
%     [~,I] = min(Delta1); 
%     disp(['Effective eps of min(Delta): [' 8 num2str(eps_vec(I)) ']' 8 '. Temperatures at this eps: [' 8 ...
%       mat2str(T_at_eps(:,I).'-273.15,5) ']' 8 ' �C']);
%     disp(['Effective eps of min(Delta2): [' 8 num2str(eps_vec(Delta2==min(Delta2))) ']' 8 ]);
end
%}
function info_content = getDerivativeInfoContent()
 
  for indF = NUM_COLORS:-1:1
    vR(1:numel(T_vec),indF) = dRatdT{indF}(T_vec);
  end
  info_content = bsxfun(@rdivide,abs(vR),sum(abs(vR),2)); 
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% CODE TAKEN FROM CLASS <MultiColorProcessor.m>
% This method returns shifted images based on the provided options
function radEst = shiftImages(radEst, shiftOptions)
    R = {radEst.R};
    % remove NaNs
    for ind1 = 1:numel(R)
      R{ind1}(isnan(R{ind1})) = 0;
    end      
    shiftWhat = setxor(shiftOptions.referenceImageNum,1:numel(radEst));
    for indS = shiftWhat
      tmpImCell = {R{shiftOptions.referenceImageNum},R{indS}};
      shiftOptions.setImages(tmpImCell);
      tmpImCell = shiftOptions.callShiftStatic;
      R{indS} = tmpImCell{end};
    end
    % make sure the reference image is also resized:
    if ~shiftOptions.doDownsample
      R{shiftOptions.referenceImageNum} = tmpImCell{1};
    end
    % Final touch-ups:
    for ind1 = 1:numel(R)
      % re-add NaNs
      R{ind1}(R{ind1} == 0) = NaN;
      % repackage (losing some info in the process):
      radEst(ind1) = RadiationResult(R{ind1},[],radEst(ind1).F);
    end 
end

end

