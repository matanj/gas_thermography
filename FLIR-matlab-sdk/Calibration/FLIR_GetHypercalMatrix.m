function [HypercalPolynomMatrix , HypercalTempMatrix ] = FLIR_GetHypercalMatrix(ptw , IT)
% Convert DL to calibrated value as per hypercal paramater given in ptw
% file
% Input : 
%   ptw     : structure containing file information from PTW file. Retrieve with FLIR_PTWFileInfo.m
%   DL      : DL value, 1D or 2D matrix to convert to temperature
%   CamTempKelvin : (optional) Camera Temperature in K 
% Output : 
%   Temperature : Calibrated data out in �C, with same dimension as DL variable

hCalib = actxserver('VCamServer.VcCalibration');    % Vircam driver need to be installed to get access to calibration interface


M       = ptw.LinearityDLMin  : 1000 : ptw.LinearityDLMax;   
Mtemp   = zeros(size(M,1), size(M,2));
MtempDL = zeros(size(M,1), size(M,2));

%s= warning;
%warning off;
for u=1:size(IT,2)
    hCalib.CreateHyperCalibration();
    if(ptw.hypercalType == 2) % hypercal V1
        hCalib.SetHyperCalibrationParam(ptw.hypercalType, ptw.CalibParamA , ptw.CalibParamB , ptw.CalibParamC , ptw.CalibParamD , ptw.CalibParamE , ptw.CalibParamF , ptw.LinearityDLMax , ptw.LinearityDLMin , ptw.CalibTempMax , ptw.CalibTempMin);                             
    else
        if (ptw.hypercalType == 3) % hypercal V2
        hCalib.SetHyperCalibrationV2Param(ptw.hypercalType, ptw.CalibParamA , ptw.CalibParamB , ptw.CalibParamC , ptw.CalibParamD , ptw.CalibParamE , ptw.CalibParamF , ptw.LinearityDLMax , ptw.LinearityDLMin , ptw.CalibTempMax , ptw.CalibTempMin , ptw.fStreamP2);                             
        else
            error('Not an hypercal calibrated file');
        end
    end
    hCalib.SetRadiometryParam(1,1,1,300,0);                 % Emissivity=1, optical transmission = 100%, Atm trans = 100%, Atm temperature in K, disable extrapoation from calibration range
    hCalib.SetIntegration(IT(u)*1e6);       % Integration time
    i = 0;
    for j=1:size(M,2)
       [ret , tempo]    = hCalib.ConvertSingleDLToTemp(M(j) , (2^ptw.m_bitres)-1);
       if ( tempo ~= 0 && ret==1) 
           i            = i+1;
           Mtemp(i)     = tempo;
           MtempDL(i)   = M(j);
       end
    end
    Mtemp2      = Mtemp(1 : i);
    MtempDL2    = MtempDL(1 : i);
    [polynom , PolyError] = polyfit(MtempDL2 , Mtemp2 , 4);        % Find polynomial regression
    HypercalPolynomMatrix(u,:) = polynom;
    if(nargout == 2)
        HypercalTempMatrix(u,:) = Mtemp2;
    end
    
end
%warning(s);
%HypercalMatrix = polynom;
hCalib.delete;
end


