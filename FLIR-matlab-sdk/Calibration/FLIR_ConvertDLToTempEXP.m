function [Temperature, UnitX, UnitY] = FLIR_ConvertDLToTempEXP(CalibrationFile , DL , CamTempKelvin, MaxDL)
% Convert DL to calibrated value as per calibration file given in input
% parameter. 
% Input : 
%   CalibrationFile : .exp calibration file containing calibration curve
%   DL      : DL value, 1D or 2D matrix to convert to temperature
%   CamTempKelvin : (optional) Camera Temperature in K for Temp compensated EXP files
%   MaxDL   : (optional) Maximum DL value (16383 by default)
% Output : 
%   Temperature : Calibrated data out, with same dimension as DL variable
%   UnitX : (optional) Abcisse Unit (usually DL)
%   unitY : (optional) Ordinates Unit (usually �C), but could be K or
%   radiance



if(nargin == 2)
    CamTempKelvin = 293.15;
    MaxDL = 16383;
end
if(nargin == 3)
    MaxDL = 16383;
end

hCalib = actxserver('VCamServer.VcCalibration');        % Vircam driver need to be installed to get access to calibration interface
hCalib.OpenFile(CalibrationFile);                       % Open calibration file
hCalib.SetCameraTemperature(CamTempKelvin);             % Set camera temperature (saved in ptw file)
hCalib.SetRadiometryParam(1,1,1,300,0);                 % Emissivity=1, optical transmission = 100%, Atm trans = 100%, Atm temperature in K, disable extrapoation from calibration range

% Retrieve BB temperature used in the calibration file
[ret nBBCount] = hCalib.GetCalibrationFileBBCount(1);   % retrieve number of calibration point in the curve
if( ret == 0 || nBBCount == -1)
    % function GetCalibrationFileBBCount not implemented in old Vircam version (<5.90.002)
    % this retrieve BB temp for different DL value but introduce erreur in the polyfit data as retrieved data are linear
    % interpolated between calibration point in the curve.
    M       = 100 : 10 : 16000;               
    Mtemp   = zeros(size(M,1), size(M,2));
    MtempDL = zeros(size(M,1), size(M,2));
    i       = 1;
    for j=1:size(M,2)
       [ret , tempo]    = hCalib.ConvertSingleDLToTemp(M(j) , MaxDL);
       if ( tempo ~= 0) 
           Mtemp(i)     = tempo;
           MtempDL(i)   = M(j);
           i            = i+1;
       end
    end
    Mtemp2      = Mtemp(1 : i);
    MtempDL2    = MtempDL(1 : i);  
    nPolyDetph  = 4;
else
    Mtemp2  = zeros(1 , nBBCount);
    MtempDL2 = zeros(1 , nBBCount);
    for i=1:nBBCount
        [ret Mtemp2(i)]     = hCalib.GetBBTempFromCalibFile(i-1 , 1);   % Retrieve BB calibration point 
        [ret MtempDL2(i)]   = hCalib.ConvertTempToDl(Mtemp2(i) , 1);    % Convert this BB point into DL
    end
    nPolyDetph = min(nBBCount-1 , 4);
end

s= warning;
warning(off , 'Warnings OFF');
[polynom , PolyError] = polyfit(MtempDL2 , Mtemp2 , nPolyDetph);        % Find polynomial regression
Temperature = polyval(polynom , DL);
warning(s);


if(nargout == 2)
    [ret , UnitX]      = hCalib.GetUnitX();
end
if(nargout == 3)
   [ret , UnitX]       = hCalib.GetUnitX();
   [ret , UnitY]       = hCalib.GetUnitY();
end
hCalib.delete;

end


