function [Temperature , CalibrationCurve] = FLIR_ConvertDLToTempHypercal(ptw , DL , CalibrationCurve)
% Convert DL to calibrated value as per hypercal paramater given in ptw
% file
% Input : 
%   ptw     : structure containing file information from PTW file. Retrieve with FLIR_PTWFileInfo.m
%   DL      : DL value, 1D or 2D matrix to convert to temperature
% Output : 
%   Temperature : Calibrated data out in �C, with same dimension as DL variable

if nargin < 3 , CalibrationCurve = 0; end

hCalib = actxserver('VCamServer.VcCalibration');    % Vircam driver need to be installed to get access to calibration interface

hCalib.CreateHyperCalibration();
if(ptw.hypercalType == 2) % hypercal V1
    hCalib.SetHyperCalibrationParam(ptw.hypercalType, ptw.CalibParamA , ptw.CalibParamB , ptw.CalibParamC , ptw.CalibParamD , ptw.CalibParamE , ptw.CalibParamF , ptw.LinearityDLMax , ptw.LinearityDLMin , ptw.CalibTempMax , ptw.CalibTempMin);                             
else
    if (ptw.hypercalType == 3) % hypercal V2
    hCalib.SetHyperCalibrationV2Param(ptw.hypercalType, ptw.CalibParamA , ptw.CalibParamB , ptw.CalibParamC , ptw.CalibParamD , ptw.CalibParamE , ptw.CalibParamF , ptw.LinearityDLMax , ptw.LinearityDLMin , ptw.CalibTempMax , ptw.CalibTempMin , ptw.fStreamP2);                             
    else
        disp('Not an hypercal calibrated file');
        Temperature = 0;
        CalibrationCurve = 0;
        return;
    end
end

hCalib.SetRadiometryParam(1,20,1,20,0);                % Emissivity=1, Background temperature in �C, Atm trans = 100%, Atm temperature in �C, disable extrapoation from calibration range
hCalib.SetIntegration(round(ptw.m_integration*1e6));    % Integration time

% Temperature = zeros(size(DL,1), size(DL,2));
% 
% for j=1:size(DL,2)
%     for i=1:size(DL,1)
%     [ret , tempo]    = hCalib.ConvertSingleDLToTemp(DL(i,j) , (2^ptw.m_bitres)-1);
%     Temperature(i,j) = tempo;
%     end
% end
if(CalibrationCurve == 0)
    % create calibration curve
    CalibrationCurve  = [0  : 1 : (2^ptw.m_bitres)-1; zeros(1 , 2^ptw.m_bitres)];
    for j=1:size(CalibrationCurve , 2)
       [ret , CalibrationCurve(2,j)]    = hCalib.ConvertSingleDLToTemp(CalibrationCurve(1,j) , (2^ptw.m_bitres)-1);
    end
end

Temperature=interp1(CalibrationCurve(1,:),CalibrationCurve(2,:),DL);

hCalib.delete;
end


