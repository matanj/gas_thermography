function hVCam_copy = FLIR_Init
%% Test whether hVCam needs to be initialized or just "retreived":
global hVCam;
% is properly initialized?
if ~isempty(hVCam)
  try
    IsTECOn(hVCam); 
    hVCam_copy = hVCam;
    return
  catch ME
    switch ME.identifier
      case 'MATLAB:UndefinedFunction'
        disp('hVCam initialization required because hVCam was empty.');
      case 'MATLAB:COM:E0'
        disp('hVCam initialization required because hVCam refers to an old object.');
      otherwise
        disp('hVCam initialization required for reasons unknown.');
    end
  end
end
%% Initialisation of all the parameters and libraries used for the functions
pth = mfilename('fullpath'); pth = pth(1:find(pth=='\',1,'last'));
% Add directories where the files are stored
path(path,fullfile(pth,'Acquisition\'))
path(path,fullfile(pth,'Calibration\'))
path(path,fullfile(pth,'CameraControl\'))
path(path,fullfile(pth,'GUI_Tool\'))
path(path,fullfile(pth,'PTWFile\'))
path(path,fullfile(pth,'Utilities\'))


% load libraries (used to send and receive messages)
if (libisloaded('user32'))
    unloadlibrary('user32')
end
loadlibrary('user32.dll','myUser32.h');

if (libisloaded('Kernel32'))
    unloadlibrary('Kernel32')
end
loadlibrary('Kernel32.dll','myKernel32.h');


% Define message parameters
global HWND_BROADCAST;              HWND_BROADCAST              = hex2dec('ffff');
global PM_NOREMOVE;                 PM_NOREMOVE                 = 0;
global PM_REMOVE;                   PM_REMOVE                   = 1;
global PM_NOYIELD;                  PM_NOYIELD                  = 2;

global CM_BROADCAST_NAME;           CM_BROADCAST_NAME           = 'Cedip';
global MESSAGE_ID_CEDIP;            MESSAGE_ID_CEDIP            = calllib('user32', 'RegisterWindowMessageA', CM_BROADCAST_NAME);

if (MESSAGE_ID_CEDIP < hex2dec('C000') || MESSAGE_ID_CEDIP > hex2dec('FFFF'))
    error('Error in RegisterWindowMessage');
end

global TIME_OUT_RECEIVE_MESSAGE;    TIME_OUT_RECEIVE_MESSAGE    = 3;

%Define messages to be processed by Altair
global CM_ON_RECORDER_START;        CM_ON_RECORDER_START        = hex2dec('601');
global CM_ON_RECORDER_STOP;         CM_ON_RECORDER_STOP         = hex2dec('602');
global CM_ON_RECORDER_START_REC;    CM_ON_RECORDER_START_REC    = hex2dec('603');
global CM_ON_RECORDER_STOP_REC;     CM_ON_RECORDER_STOP_REC     = hex2dec('604');
global CM_ON_RECORDER_PREPARED;     CM_ON_RECORDER_PREPARED     = hex2dec('605');
global CM_ON_RECORDER_LIVE;         CM_ON_RECORDER_LIVE         = hex2dec('606');   % Live On (TRUE/FALSE)   
global CM_RECORDER_START_REC;       CM_RECORDER_START_REC       = hex2dec('607');
global CM_RECORDER_STOP_REC;        CM_RECORDER_STOP_REC        = hex2dec('608');
global CM_RECORDER_PREPARE_REC;     CM_RECORDER_PREPARE_REC     = hex2dec('609');
global CM_RECORDER_FILE_NUMBER;     CM_RECORDER_FILE_NUMBER     = hex2dec('60A');   % DWORD File number (Set file number)
global CM_RECORD_STOP_ACCU;         CM_RECORD_STOP_ACCU         = hex2dec('60B');
global CM_ON_RECORD_STOP_ACCU;      CM_ON_RECORD_STOP_ACCU      = hex2dec('60C');

global CM_RM_SET_NB_FRAME;          CM_RM_SET_NB_FRAME          = hex2dec('901');   
global CM_RM_RSP_NB_FRAME;          CM_RM_RSP_NB_FRAME          = hex2dec('902'); 
global CM_RM_ASK_FILE_NAME;         CM_RM_ASK_FILE_NAME         = hex2dec('909');   % i index of created file (0)
global CM_RM_RSP_FILE_NAME;         CM_RM_RSP_FILE_NAME         = hex2dec('90A');   % chaine ATOM (Max 256 char securit� a prevoir)
global CM_RM_ASK_FILE_COUNT;        CM_RM_ASK_FILE_COUNT        = hex2dec('90B');   
global CM_RM_RSP_FILE_COUNT;        CM_RM_RSP_FILE_COUNT        = hex2dec('90C');  
global CM_RM_ASK_START_LIVE;        CM_RM_ASK_START_LIVE        = hex2dec('90F');  
global CM_RM_RSP_START_LIVE;        CM_RM_RSP_START_LIVE        = hex2dec('910');

%{
{4192} Appears when "start live feed" command starts executing.
{4320} Appears when "start live feed" command returns.
{4161} Appears after filter has changed.
{4128} Appears when camera is ready for aquisition after a settings change.
{4272} Appears when change framerate command is issued with valid parameters.
{4133} Appears when change framerate command returns.
{4144,4161,4128,4129,4192,4320} - Appear when NUC is changed.
{4182} Appears when flipping image horizontally
{4183} Appears when flipping image vertically
%}

% VirCam interface : Vircam driver need to be installed
hVCam = actxserver('VCamServer.VirtualCam');    % Vircam driver need to be installed to get access to calibration interface
hVCam_copy = hVCam;