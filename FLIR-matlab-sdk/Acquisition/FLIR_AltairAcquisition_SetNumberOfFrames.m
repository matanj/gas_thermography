function [bSuccess, bSuccededToSetNumberOfFrames ,nActualNumberOfFrames] = FLIR_AltairAcquisition_SetNumberOfFrames(nNumberOfFrames)

% Sets the number of frames of the video to be recorded in Altair
%
% Input :
%   nNumberOfFrames              : Number of Frame to acquire in Altair
%
% Output : 
%   bSuccess                     : 0 if process failed, 1 otherwise
%   bSuccededToSetNumberOfFrames : 0 means the number of frame is truncated
%   nActualNumberOfFrames        : Retrieve the actual number of frames to acquire by Altair


global CM_RM_SET_NB_FRAME;

bSuccess                     = 0;
bSuccededToSetNumberOfFrames = 0;
nActualNumberOfFrames        = 0;

% Post message to set the number of frames
bSuccess = FLIR_PostMessage(CM_RM_SET_NB_FRAME, nNumberOfFrames);

% Check that the number of frames has been correctly set up
if (bSuccess~=0)
    
    [bSuccess, bNumberOfFramesRetrieved, nNumberOfFramesSetUp] = FLIR_AltairAcquisition_GetNumberOfFrames();% Get number of frames set up in Altair
    
    if (bSuccess~=0 && bNumberOfFramesRetrieved==1)
            
        if(nNumberOfFramesSetUp == nNumberOfFrames)         % The number of frames is correctly set up
            bSuccededToSetNumberOfFrames = 1;
        end

        nActualNumberOfFrames = nNumberOfFramesSetUp;       % Actual number of frames set up in Altair
        
    end

end



