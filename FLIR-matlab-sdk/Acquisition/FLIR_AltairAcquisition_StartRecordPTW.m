function [bSuccess, bSuccededToRecord] = FLIR_AltairAcquisition_StartRecordPTW()

% Start Record video in Altair
% 
% Output : 
%   bSuccess          : 0 if process failed, 1 otherwise
%   bSuccededToRecord : 1 if the function succeeds, and 0 otherwise


global CM_RECORDER_START_REC;
% global CM_ON_RECORDER_START_REC;
% global TIME_OUT_RECEIVE_MESSAGE;

bSuccededToRecord = NaN;

% Send message to start record video
bSuccess = FLIR_PostMessage(CM_RECORDER_START_REC);  

%{
if (bSuccess~=0)
    
    wParamReceived = 0;
    t=cputime;
    
    % Reception of the message stating that the record was started
    while ((cputime-t)<TIME_OUT_RECEIVE_MESSAGE && wParamReceived ~= CM_ON_RECORDER_START_REC)
       [test,Message] = FLIR_PeekMessage(); 
       wParamReceived = Message.wParam;
    end
    
    if(wParamReceived == CM_ON_RECORDER_START_REC)
        bSuccededToRecord = 1;
    end  

end

  %}    