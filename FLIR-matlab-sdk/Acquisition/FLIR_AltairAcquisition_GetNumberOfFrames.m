function [bSuccess, bNumberOfFramesRetrieved, nNumberOfFrames] = FLIR_AltairAcquisition_GetNumberOfFrames()

% Get the number of frames to be recorded in Altair
% 
% Output : 
%   bSuccess                     : 0 if process failed, 1 otherwise
%   bNumberOfFramesRetrieved     : 0 means the number of frames could not be retrieved
%   nNumberOfFrames              : Actual number of frames to acquire by Altair


global CM_RM_SET_NB_FRAME;
global CM_RM_RSP_NB_FRAME;
global TIME_OUT_RECEIVE_MESSAGE;

bNumberOfFramesRetrieved =  0;
nNumberOfFrames          = -1;

% request the number of frames
bSuccess = FLIR_PostMessage(CM_RM_SET_NB_FRAME);	

if (bSuccess~=0)
    
    t=cputime;
    wParamReceived = 0;
    
    % Retrieve message answering to the request : Peek Messages until the
    % respond is retrieved or the time out TIME_OUT_RECEIVE_MESSAGE is elapsed
    while (((cputime-t)<TIME_OUT_RECEIVE_MESSAGE) && wParamReceived ~= CM_RM_RSP_NB_FRAME)
       [test,Message] = FLIR_PeekMessage();                
       wParamReceived = Message.wParam;
    end
    
    % Get the number of frames from the message
    if(wParamReceived == CM_RM_RSP_NB_FRAME)        
        bNumberOfFramesRetrieved    = 1;
        nNumberOfFrames             = Message.lParam; 
    end   
    
end


             