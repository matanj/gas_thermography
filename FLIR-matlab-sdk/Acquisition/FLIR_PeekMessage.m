function [bMessageReceived,MessageReceived] = FLIR_PeekMessage(removeFlag)

% dispatches incoming sent messages, checks the thread message queue for a posted message,
% and retrieves the message (if any exist). Messages are removed from the
% queue after processing by FLIR_PeekMessage().
%
% Output :
%   bMessageReceived    : 1 if a message with ID MESSAGE_ID_CEDIP is available, 0 otherwise
%   MessageReceived     : Retrieved Message, with parameters :
%                           - hwnd    : Handle to the window whose window procedure receives the message. 
%                                       hwnd is NULL when the message is a thread message.
%                           - message : Specifies the message identifier. Applications can only use the low word; 
%                                       the high word is reserved by the system. 
%                           - wParam  : Specifies additional information about the message. 
%                                       The exact meaning depends on the value of the message member. 
%                           - lParam  : Specifies additional information about the message. 
%                                       The exact meaning depends on the value of the message member. 
%                           - time    : Specifies the time at which the message was posted. 
%                           - pt      : Specifies the cursor position, in screen coordinates, when the message was posted.

global MESSAGE_ID_CEDIP;
global PM_REMOVE;
global PM_NOREMOVE;
global PM_NOYIELD;

% Initialisation of the Message to retrieve
point.x = 0;
point.y = 0;

Message.hwnd = 0;
Message.message = 0;
Message.wParam = 0;
Message.lParam = 0;
Message.time = 0;
Message.pt = point;

pMessageReceived = libpointer('tagMSGPtr',Message);


% Peek message using the PeekMessageA function loaded in the library 'user32'
if nargin < 1
  bMessageReceived = calllib('user32', 'PeekMessageA', pMessageReceived, 0, MESSAGE_ID_CEDIP, MESSAGE_ID_CEDIP, PM_REMOVE);
elseif nargin == 1 && ~removeFlag
  bMessageReceived = calllib('user32', 'PeekMessageA', pMessageReceived, 0, MESSAGE_ID_CEDIP, MESSAGE_ID_CEDIP, PM_NOREMOVE);
end

if (bMessageReceived~=0)
    MessageReceived = pMessageReceived.Value;
else
    MessageReceived = Message;
end
