typedef unsigned int UINT;
typedef UINT WPARAM;
typedef long LPARAM;
typedef long LRESULT;
typedef unsigned long HANDLE;
typedef unsigned long HWND;
typedef unsigned long HICON;
typedef unsigned long HINSTANCE;
typedef int BOOL;
typedef const char *LPCSTR;
typedef unsigned long DWORD;
typedef long LONG;
typedef unsigned long ULONG;

typedef struct tagPOINT {
	LONG x;
	LONG y;
} POINT,*PPOINT;

typedef struct tagMSG {
	HWND hwnd;
	UINT message;
	WPARAM wParam;
	LPARAM lParam;
	DWORD time;
	POINT pt;
} MSG,*LPMSG,*PMSG;


#define STDCALL	__stdcall


BOOL STDCALL PostMessageA(HWND,UINT,WPARAM,LPARAM);

UINT STDCALL RegisterWindowMessageA(LPCSTR);

//BOOL STDCALL GetMessageA(LPMSG,HWND,UINT,UINT);

BOOL STDCALL PeekMessageA(LPMSG,HWND,UINT,UINT,UINT);



