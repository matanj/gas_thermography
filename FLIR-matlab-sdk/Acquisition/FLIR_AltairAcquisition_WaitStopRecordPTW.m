function [bSuccess, bSuccededToStopRecord] = FLIR_AltairAcquisition_WaitStopRecordPTW(varargin)
% Wait until file acquisition in done in Altair
% 
% input (if 2 inputs):
%   NbImagesToAcquire     : Number of images in the acquisition
%   ImageFrequency        : Image frequency, used in combinaison with
%                           NbImagesToAcquire to determine waiting time
% input (if 1 input):
%   minTime               : Timeout value in seconds.
%
% Output : 
%   bSuccess              : 0 if process failed, 1 otherwise
%   bSuccededToStopRecord : 1 if the record was stopped, 0 otherwise
switch nargin
    case 2
        NbImagesToAcquire = varargin{1};
        ImageFrequency = varargin{2};
    case 1
        % do nothing
    case 0 % set default values
        NbImagesToAcquire = 0;
        ImageFrequency = 50;
    otherwise
        error('Wrong amount of input parameters. Valid numbers: 2,1,0.');
end

global CM_ON_RECORDER_STOP_REC;
global TIME_OUT_RECEIVE_MESSAGE;

if any(nargin == [0,2])
    baseAcqTime = NbImagesToAcquire/ImageFrequency;
else % if nargin == 1
    baseAcqTime = varargin{1};
end

Timeout = baseAcqTime + TIME_OUT_RECEIVE_MESSAGE; % Timeout defined with the time acquisition should last + standard timeout
    
bSuccess                = 1;
bSuccededToStopRecord   = 0;

% Send message to stop record video
wParamReceived = 0;
t=cputime;

% Peek acknowledgement message
while ((cputime-t)<Timeout && wParamReceived ~= CM_ON_RECORDER_STOP_REC)
   [~,Message] = FLIR_PeekMessage(); 
   wParamReceived = Message.wParam;
end

if(wParamReceived == CM_ON_RECORDER_STOP_REC)
    bSuccededToStopRecord = 1;
end   
