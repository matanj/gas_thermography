function [bSuccess, bSuccededToPrepareRecord] = FLIR_AltairAcquisition_PrepareRecordPTW()

% Prepares to record a file using Atair
% 
% Output : 
%   bSuccess                 : 0 if process failed, 1 otherwise
%   bSuccededToPrepareRecord : 1 if the function succeeds, and 0 otherwise


global CM_RECORDER_PREPARE_REC;
global CM_ON_RECORDER_PREPARED;
global TIME_OUT_RECEIVE_MESSAGE;

bSuccededToPrepareRecord = 0;

% Send message asking to prepare record
bSuccess = FLIR_PostMessage(CM_RECORDER_PREPARE_REC); 

if (bSuccess~=0)
    
    t=cputime;
    wParamReceived = 0;
        
     % Reception of the acknowledgement
     while ((cputime-t)<TIME_OUT_RECEIVE_MESSAGE && wParamReceived ~= CM_ON_RECORDER_PREPARED)
       [test,Message] = FLIR_PeekMessage();                   
       wParamReceived = Message.wParam;
    end
    
    if(wParamReceived == CM_ON_RECORDER_PREPARED)
        bSuccededToPrepareRecord = 1;
    end  

end

    