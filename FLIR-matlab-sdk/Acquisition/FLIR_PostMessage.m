function bMessagePosted = FLIR_PostMessage(wparam,lparam)

% The FLIR_PostMessage function places a message in the message queue associated with the thread 
% that created the specified window and returns without waiting for the thread to process the message. 
% The message is posted in broadcast (to all top-level windows in the system)
% The message ID is MESSAGE_ID_CEDIP
%
% Input
%   wParam : Specifies additional message-specific information.
%   lParam : Specifies additional message-specific information.
%
% Output
%   bMessagePosted : If the function succeeds, the return value is nonzero.


global HWND_BROADCAST;
global MESSAGE_ID_CEDIP;

% Post message using the PostMessageA function loaded in the library 'user32'
if (nargin==1)
    bMessagePosted = calllib('user32', 'PostMessageA', HWND_BROADCAST, MESSAGE_ID_CEDIP, wparam, 0);
else
    bMessagePosted = calllib('user32', 'PostMessageA', HWND_BROADCAST, MESSAGE_ID_CEDIP, wparam, lparam);
end


