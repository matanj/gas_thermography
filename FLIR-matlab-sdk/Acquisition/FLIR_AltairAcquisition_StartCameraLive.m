function [bSuccess, bMessageReceivedByAltair] = FLIR_AltairAcquisition_StartCameraLive()

% Start Camera Live in Altair
% 
% Output : 
%   bSuccess                 : 0 if process failed, 1 otherwise
%   bMessageReceivedByAltair : 0 if message not received by Altair, otherwise


global CM_RM_ASK_START_LIVE;
global CM_RM_RSP_START_LIVE;
global TIME_OUT_RECEIVE_MESSAGE;

bMessageReceivedByAltair =0;

% Send message to start live
bSuccess = FLIR_PostMessage(CM_RM_ASK_START_LIVE); 

if (bSuccess~=0)
    
    wParamReceived = 0;
    t=cputime;

    % Retrieve message answering to the request : Peek Messages until the
    % respond is retrieved or the time out TIME_OUT_RECEIVE_MESSAGE is elapsed
    while ((cputime-t)<TIME_OUT_RECEIVE_MESSAGE && wParamReceived ~= CM_RM_RSP_START_LIVE)
       [test,Message] = FLIR_PeekMessage();        
       wParamReceived = Message.wParam;          
    end
    
    if(wParamReceived == CM_RM_RSP_START_LIVE && Message.lParam==1)
        bMessageReceivedByAltair = 1;
    end 
    
end


