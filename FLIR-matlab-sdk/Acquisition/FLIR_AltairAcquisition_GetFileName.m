function [bSuccess, bFileNameRetrieved, strFileName] = FLIR_AltairAcquisition_GetFileName()

% Get the name of the last file recorded by Atair
%
% Output : 
%   bSuccess                    : 0 if process failed, 1 otherwise
%   bFileNameRetrieved          : 1 if the name was correctly retrieved, and 0 otherwise
%   strFileName                 : Name of the last file recorded by Altair


global CM_RM_ASK_FILE_NAME;
global CM_RM_RSP_FILE_NAME;
global TIME_OUT_RECEIVE_MESSAGE;

bFileNameRetrieved  = 0; 
strFileName         = '';

% Post message to request the file name
bSuccess = FLIR_PostMessage(CM_RM_ASK_FILE_NAME);         

if (bSuccess~=0)

    wParamReceived = 0;
    t=cputime;

    % Retrieve acknowledgement : Peek Messages until the respond is 
    % retrieved or the time out TIME_OUT_RECEIVE_MESSAGE is elapsed
    while ((cputime-t)<TIME_OUT_RECEIVE_MESSAGE && wParamReceived ~= CM_RM_RSP_FILE_NAME)
       [test,Message] = FLIR_PeekMessage(); 
       wParamReceived = Message.wParam;                        
    end

    % If the message is correctly received, access to Message.lParam, which is a 16-bit integer ATOM enabling to access the string
    if (wParamReceived == CM_RM_RSP_FILE_NAME) 

        % Creation and initialisation of a string of length 256
        strNull = ''; for i=1:256  strNull = [strNull ' ']; end

        % reception of the file name in the string strFileName
        [bSuccess,strFileName] = calllib('Kernel32', 'GlobalGetAtomNameA', Message.lParam, strNull, 256);

        if (bSuccess>0)
            bFileNameRetrieved = 1;
        end

    end

end









