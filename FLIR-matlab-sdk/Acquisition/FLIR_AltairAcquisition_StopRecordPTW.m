function [bSuccess, bSuccededToStopRecord] = FLIR_AltairAcquisition_StopRecordPTW()

% Stop image acquisition in Altair
% 
% Output : 
%   bSuccess              : 0 if process failed, 1 otherwise
%   bSuccededToStopRecord : 1 if the record was stopped, 0 otherwise


global CM_RECORDER_STOP_REC;
global CM_ON_RECORDER_STOP_REC;
global TIME_OUT_RECEIVE_MESSAGE;

bSuccededToStopRecord   = 0;

% Send message to stop record video
bSuccess = FLIR_PostMessage(CM_RECORDER_STOP_REC);

if (bSuccess~=0)
    
    wParamReceived = 0;
    t=cputime;
    
    % Peek acknowledgement message
    while ((cputime-t)<TIME_OUT_RECEIVE_MESSAGE && wParamReceived ~= CM_ON_RECORDER_STOP_REC)
       [test,Message] = FLIR_PeekMessage(); 
       wParamReceived = Message.wParam;
    end
    
    if(wParamReceived == CM_ON_RECORDER_STOP_REC)
        bSuccededToStopRecord = 1;
    end   

end

   