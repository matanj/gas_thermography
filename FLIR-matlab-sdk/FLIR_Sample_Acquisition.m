%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% FLIR IMAGE ACQUISITION SAMPLE
% 
% Warning : This SDK is only compatible with FLIR SC2200, SC2500, SC5xx0, S7xx0 camera
% from FLIR ATS. it requires Vircam drivers version 5.90.004 or more.
% Image acquisiton is done through Altair v5.90.004 or more.
%
% Prerequisites :  
%   - Vircam V5.90.004 or more installed
%   - Altair V5.90.004 or more installed and running.
%   - Camera connected to the computer
%
% Advise : 
%   - Make sure the option 'Open File After acquisition' in Altair recorder
%   advanced window is unchecked.
%
% This sample should be used to understand the acquisition steps 
% 
% What does this application do :
%   1. Initialise connection with Vircam drivers
%   2. Send message to Altair to Start Live image 
%   3. Send message to Altair to Set number of frames to acquire
%   4. Retrieve the image frequency to calculate the acquisition duration
%   5. Send message to Altair to prepare the acquisition. Memory buffer
%   allocation, disks operation are done in this step to ensure that when
%   the acquisition is triggered, no image is lost.
%   6. Send message to Altair to start recording images
%   7. Check for end of acquisistion message from Altair
%   8. Retrieve the file name where the images are stored.

clc

% 1. Initialise connection with Vircam drivers
disp('Initialise connection with Vircam drivers...')
if(exist('hVCam' , 'var') == 0) % check if Init has been done. No need to redo it.
    FLIR_Init
end

% 2. Send message to Altair to Start Live image
disp(' ')
disp('FLIR_AltairAcquisition_StartCameraLive...');

[bSuccess, bMessageReceivedByAltair] = FLIR_AltairAcquisition_StartCameraLive();

if (bSuccess==0)
    error('ERROR : FLIR_AltairAcquisition_StartCameraLive function failed');
end
    
if (bMessageReceivedByAltair>0)
    disp('FLIR_AltairAcquisition_StartCameraLive : Message processed by Altair');
else
    disp('FLIR_AltairAcquisition_StartCameraLive : Message not processed by Altair');
    break;
end

% 3.Send message to Altair to Set number of frames to acquire
disp(' ')
nNumberOfFrames = input('TYPE NUMBER OF FRAMES FOR ACQUISITION (ENTER = Get number of frame from Altair): ');


if (isempty(nNumberOfFrames))                 % blank input => Get Number of Frames from Altair
    
    % Gets the current number of frames
    [bSuccess, bGetFrameNumber, nNumberOfFramesGet] = FLIR_AltairAcquisition_GetNumberOfFrames();
    
    if (bSuccess==0)
        error('ERROR : FLIR_AltairAcquisition_GetNumberOfFrames failed');
    end

    if (bGetFrameNumber)
        disp(['FLIR_AltairAcquisition_GetNumberOfFrames: The frame number was not changed and is ',num2str(nNumberOfFramesGet)]);
    else
        error('ERROR : FLIR_AltairAcquisition_GetNumberOfFrames : The frame number could not be retrieved');
    end
    
else                                            % user input => Set the number of frames to acquire to this input
    disp('FLIR_AltairAcquisition_SetNumberOfFrames...');
    [bSuccess, bSetFrameNumber,nNumberOfFramesGet] = FLIR_AltairAcquisition_SetNumberOfFrames(nNumberOfFrames);
    
    if (bSuccess==0)
        error('FLIR_AltairAcquisition_SetNumberOfFrames');
    end
    
    if (bSetFrameNumber>0)
        disp(['FLIR_AltairAcquisition_SetNumberOfFrames : The frame number has correctly been set to ',num2str(nNumberOfFrames)]);
    else
        disp(['FLIR_AltairAcquisition_SetNumberOfFrames : The frame number could not be set to ',num2str(nNumberOfFrames),', it has been set to ',num2str(nNumberOfFramesGet)]);
    end
end

% 4. Retrieve the image frequency to calculate the acquisition duration
[bSuccess,fFrequency] = FLIR_Camera_GetFrequency();
if(bSuccess == 0) 
    fFrequency = 50;
end


%   5. Send message to Altair to prepare the acquisition
disp(' ')
disp ('FLIR_AltairAcquisition_PrepareRecordPTW()...')
[bSuccess, bRecordPrepared] = FLIR_AltairAcquisition_PrepareRecordPTW();

if (bSuccess==0)
    error('ERROR : FLIR_AltairAcquisition_PrepareRecordPTW failed');
end
    
if (bRecordPrepared>0)
    disp('FLIR_AltairAcquisition_PrepareRecordPTW() : Message processed by Altair');
else
    disp('ERROR : FLIR_AltairAcquisition_PrepareRecordPTW() : Message not processed by Altair');
    disp('The file might already exist : Close it and allow it to be overwrited');
    break;
end

%   6. Send message to Altair to start recording images
disp(' ')
disp ('FLIR_AltairAcquisition_StartRecordPTW...')
[bSuccess, bRecordStarted] = FLIR_AltairAcquisition_StartRecordPTW();

if (bSuccess==0)
    error('FLIR_AltairAcquisition_StartRecordPTW failed');
end

if (bRecordStarted>0)
    disp('FLIR_AltairAcquisition_StartRecordPTW : Message processed by Altair');
else
    error('ERROR : FLIR_AltairAcquisition_StartRecordPTW : Message not processed by Altair');
end

%   7. Check for end of acquisistion message from Altair
disp (' ')
disp ('FLIR_AltairAcquisition_WaitStopRecordPTW...')

[bSuccess, bRecordStopped] = FLIR_AltairAcquisition_WaitStopRecordPTW(nNumberOfFramesGet , fFrequency);

if (bSuccess==0)
    error('FLIR_AltairAcquisition_WaitStopRecordPTW failed');
end

if (bRecordStopped>0)
     disp('FLIR_AltairAcquisition_WaitStopRecordPTW : Acquisition done by Altair');     
else
     error('ERROR : FLIR_AltairAcquisition_WaitStopRecordPTW : Acquisition not yet done by Altair.');
end
    
%   8. Retrieve the file name where the images are stored.
pause(0.5); % let some time to make sure the file name is updated
disp(' ')
disp('FLIR_AltairAcquisition_GetFileName...');
[bSuccess, bGetFileName, strFileName] = FLIR_AltairAcquisition_GetFileName();

if (bSuccess==0)
    error('ERROR : FLIR_AltairAcquisition_GetFileName failed');
end

if (bGetFileName)
    disp(['FLIR_AltairAcquisition_GetFileName : Last video registered in ',strFileName]);
else
    error('ERROR : FLIR_AltairAcquisition_GetFileName : Could not get the file name');
end
