%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% FLIR CAMERA CONTROL SAMPLE
% 
% Warning : This SDK is only compatible with FLIR SC2200, SC2500, SC5xx0, S7xx0 camera
% from FLIR ATS. it requires Vircam drivers version 5.90.004 or more.
% Image acquisiton is done through Altair v5.90.004 or more.
%
% Prerequisites :  
%   - Vircam V5.90.004 or more installed
%   - Altair V5.90.004 or more installed and running.
%   - Camera connected to the computer 
%
% What does this application do :
%   1. Initialise connection with Vircam drivers if needed,
%   1b. Connects automatically to camera
%   2. Get current and Max image frequency
%   3. Set image frequency
%   4. Get current integration time
%   5. Set image integration time
%   6. Get External trigger status
%   7. Set External trigger status

clc

% 1. Initialise connection with Vircam drivers if needed
disp('Initialise connection with Vircam drivers...')
if(exist('hVCam' , 'var') == 0) % check if Init has been done. No need to redo it.
    FLIR_Init
else
    disp('Already connected to Vircam')
end

% 1b. Connects to camera
disp('Connect to camera...')
[bSuccess , bCameraConnected] = FLIR_Camera_IsConnected();    
if(bCameraConnected == 0)
    bSuccess = FLIR_Camera_Connect(1); % automatically connect to camera. 0:Display connection window
    if(bSuccess == 0) 
        error('ERROR : FLIR_Camera_Connect function failed');
    else
        disp('Connection to camera established');
    end
else
    disp('Already connected to camera...')
end

% 2. Get current and Max image frequency
disp(' ');
disp('FREQUENCY');
[bSuccess,fFrequency] = FLIR_Camera_GetFrequency();
if(bSuccess == 0) 
    error('ERROR : FLIR_Camera_GetFrequency function failed');
else
    disp(['FLIR_Camera_GetFrequency: The current frame rate is ',num2str(fFrequency),' Hz']);
end
[bSuccess,fMaxFrequency] = FLIR_Camera_GetMaxFrequency();
if(bSuccess == 0) 
    error('ERROR : FLIR_Camera_GetMaxFrequency function failed');
else
    disp(['FLIR_Camera_GetMaxFrequency: The max frame rate is ',num2str(fMaxFrequency),' Hz']);
end

% 3. Set image frequency
fRequiredFrequency = input(['Enter required frame frequency (ENTER = current frame rate (',num2str(fFrequency),' Hz / Max:',num2str(fMaxFrequency),' Hz)): ']);
if(isempty(fRequiredFrequency))
    fRequiredFrequency = fFrequency;
end
bAutoFrequency = 0; % allow the camera to set automatically to maximum frequency in desired frequency is not acheivable. (0=don't allow, 1:allow)
[bSuccess, bSucceededToSetFrequency, fActualFrequency] = FLIR_Camera_SetFrequency(fRequiredFrequency, bAutoFrequency);
if (bSuccess==0)
    error('ERROR : FLIR_Camera_SetFrequency function failed');
end
    
if (bSucceededToSetFrequency>0)
    disp(['FLIR_Camera_SetFrequency : Camera frequency set to ',num2str(fActualFrequency),' Hz']);
else
    error('ERROR : FLIR_Camera_SetFrequency : not able to set frequency. Could be out of the frequency range if bAutoFrequency parameter set to 0 ');
end

% 4. Get current integration time
disp(' ');
disp('INTEGRATION TIME');
[bSuccess, fIT] = FLIR_Camera_GetCurrentIT();
if(bSuccess == 0) 
    error('ERROR : FLIR_Camera_GetCurrentIT function failed');
else
    disp(['FLIR_Camera_GetCurrentIT: The current integration time is ',num2str(fIT),' �s']);
end

% 5. Set image integration time
fRequiredIT = input(['Enter required integration time (ENTER = actual integration time (',num2str(fIT),' �s)): ']);
if(isempty(fRequiredIT))
    fRequiredIT = fIT;
end
bAutoFrequency = 0; % allow the camera to change frame frequency to force current integration time. (0=don't allow, 1:allow)
[bSuccess, bSucceededToSetIT, fActualIT] = FLIR_Camera_SetCurrentIT(fRequiredIT, bAutoFrequency);
if (bSuccess==0)
    error('ERROR : FLIR_Camera_SetCurrentIT function failed');
end
if (bSucceededToSetIT>0)
    disp(['FLIR_Camera_SetCurrentIT : Camera frequency set to ',num2str(fActualIT),' �s']);
else
    error('ERROR : FLIR_Camera_SetCurrentIT : not able to set integration. Could be out of the integration time range if bAutoFrequency parameter set to 0 ');
end

% 6. Get External trigger status
disp(' ');
disp('EXTERNAL SYNCHRONISATION');
[bSuccess, bExtTrigger] = FLIR_Camera_GetExternalTrigger();
if(bExtTrigger)
    strTrigger = 'active';  % the camera start the integration of an image on the edge of external clocking signal connected to the camera trigger in connector. Frequency parameter in not used';
else
    strTrigger = 'inactive'; % the camera generates its integration signal internally';
end

if(bSuccess == 0)
    error('ERROR : FLIR_Camera_GetExternalTrigger function failed');
else
    disp(['FLIR_Camera_GetExternalTrigger: The External trigger is ',strTrigger]);
end

% 7. Set External trigger status
bRequiredExtTrigger = input(['Select external trigger status. 0=internal trigger, 1=external trigger. (ENTER = actual status (',strTrigger,')): ']);
if(isempty(bRequiredExtTrigger))
    bRequiredExtTrigger = bExtTrigger;
end
[bSuccess, bSuccededToSetParameters] = FLIR_Camera_SetExternalTrigger(bExtTrigger);
if(bExtTrigger)
    strTrigger = 'active';
else
    strTrigger = 'inactive';
end
if (bSuccess==0)
    error('ERROR : FLIR_Camera_SetTriggerIn function failed');
end
if (bSuccededToSetParameters>0)
    disp(['FLIR_Camera_SetTriggerIn : Camera external trigger is ',strTrigger]);
else
    error('ERROR : FLIR_Camera_SetTriggerIn : not able to set external trigger status.');
end

