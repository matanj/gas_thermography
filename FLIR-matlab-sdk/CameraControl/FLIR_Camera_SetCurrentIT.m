function [bSuccess, bSucceededToSetIT, fActualIT] = FLIR_Camera_SetCurrentIT(fRequestedIT, bAutoFrequency)

% sets integration time on current channel
% 
% Input : 
%  fRequestedIT         : requested integration time
%  bAutoFrequency       : 1 to change frequency if necessary, 0 otherwise
%
% Outputs :
%   bSuccess            : 0 if process failed, 1 otherwise
%   bSucceededToSetIT   : 0 means the integration time has not been set to fIT
%   fActualIT           : Actual integration time of the camera


global hVCam; 

bSucceededToSetIT   =  0;
fActualIT           = -1;

% Set Float integration time
bSuccess = hVCam.SetFloatCurrentIT(fRequestedIT, bAutoFrequency);

% If failure, try to set DWORD integration time
if (bSuccess==0)
    dwRequestedIT = round(fRequestedIT);
    bSuccess = hVCam.SetCurrentIT(dwRequestedIT, bAutoFrequency);
end

% Check that the correct integration time has been set
if (bSuccess~=0)
    [bSuccess, fActualIT] = FLIR_Camera_GetCurrentIT();
    
    if (bSuccess~=0 && (fActualIT - fRequestedIT) < 1e-5)
        bSucceededToSetIT = 1;
    end
end


