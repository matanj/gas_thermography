function [bSuccess, bExternalTrigger] = FLIR_Camera_GetExternalTrigger()

% Get External trigger status
% 
% Output    :
%   bSuccess         : 0 if process failed, 1 otherwise
%   bExternalTrigger : 1 if camera in external trigger mode, 0 in internal
%   clocking


global hVCam;

[bSuccess , bExternalTrigger] = hVCam.GetExternal(0);
bExternalTrigger = bitand(bExternalTrigger , 1);

