function [bSuccess, fIT] = FLIR_Camera_GetCurrentIT()

% gets integration time on current channel
%
% Outputs :
%   bSuccess     : 0 if process failed, 1 otherwise
%   fIT          : current integration time


global hVCam; 

% Get float current IT
[bSuccess, fIT, notused] = hVCam.GetFloatCurrentIT(0, 0);

% if failure (float not supported), try to get current IT (DWORD)
if (bSuccess==0)
    [bSuccess, dwIT, notused] = hVCam.GetCurrentIT(0, 0);
    dwIT    = mod(dwIT,2^16);
    fIT = dwIT;
end