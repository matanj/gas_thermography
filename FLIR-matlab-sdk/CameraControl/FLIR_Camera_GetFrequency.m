function [bSuccess,fFrequency] = FLIR_Camera_GetFrequency()

% Gets current frame rate
%
% Outputs :
%   bSuccess     : 0 if process failed, 1 otherwise
%   fFrequency   : current frequency

global hVCam;

[bSuccess,fFrequency] = hVCam.GetFrequency(0);
