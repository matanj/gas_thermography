function [bSuccess , bCameraConnected] = FLIR_Camera_IsConnected()


% Connect to camera
% Output :
%   bSuccess            : 0 if process failed, 1 otherwise
%   bCameraConnected    : 1 if camera connected, 0 otherwise


global hVCam; 
[bSuccess , bCameraConnected] = hVCam.IsIni(0);
bCameraConnected = bitand(bCameraConnected , 1);
