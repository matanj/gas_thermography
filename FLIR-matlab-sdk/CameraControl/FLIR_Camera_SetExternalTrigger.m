function [bSuccess, bSuccededToSetParameters] = FLIR_Camera_SetExternalTrigger(bEnableExternalTrigger)

% Set trigger input 
%
% Inputs    :
%   bEnableExternalTrigger   : 1 if to enable external trigger (external sync), 0 otherwise
%   
% Output    :
%   bSuccess                 : 0 if process failed, 1 otherwise
%   bSuccededToSetParameters : 1 if parameters successfully set, 0 otherwise


global hVCam;

bSuccededToSetParameters 	= 0;

bSuccess = hVCam.SetExternal(bEnableExternalTrigger);

if (bSuccess~=0)
    bSuccededToSetParameters = 1;
end