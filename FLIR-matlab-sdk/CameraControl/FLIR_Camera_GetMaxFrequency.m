function [bSuccess,fMaxFrequency] = FLIR_Camera_GetMaxFrequency()

% Gets maximum frame rate
%
% Outputs :
%   bSuccess       : 0 if process failed, 1 otherwise
%   fMaxFrequency  : Maximum frequency


global hVCam;
[bSuccess,fMaxFrequency] = hVCam.GetMaxFrequency(0);