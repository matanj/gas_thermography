function [bSuccess, bSucceededToSetIT, fActualIT] = FLIR_Camera_SetIT(fRequestedIT, nChannel, bAutoFrequency)

% sets integration time by channel if multi TI activated. Else, sets
% current integration time
% 
% Input : 
%  fRequestedIT         : requested integration time
%  nChannel             : Channel on which to change the integration time
%                               - 0: current channel
%  bAutoFrequency       : 1 to change frequency if necessary if multi TI not activated, 0 otherwise
%
% Outputs :
%   bSuccess            : 0 if process failed, 1 otherwise
%   bSucceededToSetIT   : 0 means the integration time has not been set to fIT
%   fActualIT           : Actual integration time of the camera


global hVCam; 

bSucceededToSetIT   =  0;
fActualIT           = -1;


bIsMultiTIAvailable = hVCam.IsMultiTI();                                % Check if multi TI available


if (bIsMultiTIAvailable)                                                % If multi TI available
    
    [bSuccess, nNbChannels] = hVCam.GetMultiNbrChannel(0);              % Checks the number of channels
    
    if(bSuccess~=0)             
        
        if(nNbChannels >1)                                              % If multi TI activated
            
            if (nChannel==0)                                            % If requested channel == 0 : Set current IT
                
                [bSuccess, bSucceededToSetIT, fActualIT] = FLIR_Camera_SetCurrentIT(fRequestedIT, bAutoFrequency);
                                
            elseif(nChannel <= nNbChannels)                             % Checks if the requested channel is valid
                             
                % Set float integration time
                bSuccess = hVCam.SetFloatIntegration(fRequestedIT, 0, nChannel, bAutoFrequency);

                % If failure, try to set DWORD integration time
                if (bSuccess==0)
                    dwRequestedIT = round(fRequestedIT);
                    bSuccess = hVCam.SetIntegration(dwRequestedIT, 0, nChannel, bAutoFrequency);
                end

                % Check that the correct IT has been set
                if (bSuccess~=0)
                    [bSuccess, fActualIT] = FLIR_Camera_GetIT(nChannel);

                    if (bSuccess~=0 && fActualIT == fRequestedIT)
                        bSucceededToSetIT = 1;
                    end
                end
                
            else                                                        % if the requested channel not valid, report failure
                bSuccess = 0;
            end
            
        else                                                            % If multi TI not activated : set current Integration time
            
            [bSuccess, bSucceededToSetIT, fActualIT] = FLIR_Camera_SetCurrentIT(fRequestedIT, bAutoFrequency);
            
        end
    end
    
else                                                                    % If multi TI not available : set current Integration time
            
    [bSuccess, bSucceededToSetIT, fActualIT] = FLIR_Camera_SetCurrentIT(fRequestedIT, bAutoFrequency);

end
