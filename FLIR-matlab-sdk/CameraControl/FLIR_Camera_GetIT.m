function [bSuccess, fIT] = FLIR_Camera_GetIT(nChannel)

% gets integration time by channel if multi TI activated. Else, gets
% current IT
% 
% Input : 
%  nChannel     : channel of the requested IT (0 : current channel)
%
% Outputs :
%   bSuccess    : 0 if process failed, 1 otherwise
%   fIT         : current integration time


global hVCam; 

fIT         = -1;

bIsMultiTIAvailable = hVCam.IsMultiTI();                                % Check if multi TI available

if (bIsMultiTIAvailable)                                                % If multi TI available
    [bSuccess, nNbChannels] = hVCam.GetMultiNbrChannel(0);              % Checks the number of channels
    if(bSuccess~=0)             
        if(nNbChannels >1)                                              % If multi TI activated
            if (nChannel==0)                                            % If requested channel == 0 Get current IT
                [bSuccess, fIT] = FLIR_Camera_GetCurrentIT();  
            elseif(nChannel <= nNbChannels)                             % Checks if the requested channel is valid
                [bSuccess, fIT, ~] = hVCam.GetFloatIT(nChannel, 0, 0);  % Gets the integration time (float) of the channel
                if (bSuccess==0)                                        % If failure, tries to get the DWORD integration time
                    [bSuccess, dwIT, ~] = hVCam.GetIT(nChannel, 0, 0);
                    dwIT    = mod(dwIT,2^16);
                    fIT = dwIT;
                end
            else                                                        % if the requested channel not valid, report failure
                bSuccess = 0;
            end
        else                                                            % If multi TI not activated
            [bSuccess, fIT] = FLIR_Camera_GetCurrentIT();               % Get current integration time
        end
    end
else                                                                    % If multi TI not available
    [bSuccess, fIT] = FLIR_Camera_GetCurrentIT();                       % Get current integration time
end
