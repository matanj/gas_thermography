function [bSuccess, bSucceededToSetFrequency, fActualFrequency] = FLIR_Camera_SetFrequency(fFrequency, bAutoFrequency)

% Set frame rate
%
% Inputs
%   fFrequency               : Frame rate to set to the camera
%   bAutoFrequency           : 0 to keep the frame rate unchanged if the command cannot be performed, 
%                              1 to automatically adapt the frame rate upon
%                              integration time
%
% Outputs :
%   bSuccess                 : 0 if process failed, 1 otherwise
%   bSucceededToSetFrequency : 0 means the frame rate has been truncated
%   fActualFrequency         : Retrieve the actual frame rate



global hVCam;

bSucceededToSetFrequency =  0;
fActualFrequency         = -1;

% Set Frequency to fFrequency
bSuccess = hVCam.SetFrequency(fFrequency, bAutoFrequency);

if(bSuccess ~= 0)
    % Check the actual camera frequency
    [bSuccess,fActualFrequency] = FLIR_Camera_GetFrequency();     
    if (bSuccess ~= 0 && ((fActualFrequency - fFrequency) < 1e-5))
        bSucceededToSetFrequency = 1;
    end
end
    

