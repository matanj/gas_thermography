function realIT = FLIR_PredictCameraIT(computedIT)
% FLIR_PREDICTCAMERAIT converts some input IT into 
% For a better explanation of how this is computed, see below:
%   http://chat.stackoverflow.com/transcript/message/27780506#27780506
%{
for 72: single(7.1800001e-05)
single(2E-9)*(single(72/0.2-1)*single(1E2))

for 275: single(2.7480e-04)
single(2E-9)*(single(275/0.2-1)*single(1E2))

for 675: single(0.00067479996)
single(2E-9)*(single(675/0.2-1)*single(1E2))

for 1096: single(0.0010958)
single(2E-9)*(single(1096/0.2-1)*single(1E2))

for 1232: single(0.0012318)
single(2E-9)*(single(1232/0.2-1)*single(1E2))

for 1256: single(0.0012558) <== JACKPOT!!!
single(2E-7)*single(1256/0.2-1)
single(2E-9)*(single(1256/0.2-1)*single(1E2))

for 1350: single(0.0013498)
single(2E-7)*single(1350/0.2-1)

for 7800: single(0.0077998)
single(2E-7)*single(7800/0.2-1)
%}

realIT = ((computedIT <= 1256)*single(2E-9).*(single(computedIT./0.2-1)*single(1E2)) + ...
          (computedIT >  1256)*single(2E-7).* single(computedIT./0.2-1) + ...
          floor(single(computedIT./0.2)-eps(single(computedIT./0.20001)))*1E-6*0.2)/2.0000001;
