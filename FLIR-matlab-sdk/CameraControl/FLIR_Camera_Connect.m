function [bSuccess] = FLIR_Camera_Connect(bAutomaticConnection)


% Connect to camera
% Input : 
%  bAutomaticConnection : 0: display connection window, 
%                         1: connects automatically to camera
% Output :
%   bSuccess            : 0 if process failed, 1 otherwise


global hVCam; 
bSuccess = hVCam.Init(bAutomaticConnection);
