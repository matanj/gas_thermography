function [avgFrame,ptwInfo] = PTWAverageFramesInFiles(ptwFullPath)
%By Iliya Romm - 20/08/2014
%{
DEBUG:
PTWAverageFramesInFiles('F:\Iliya-PhD\MATLAB\Downloaded\FLIR Matlab SDK\PTWFile\14_08_07_AlWithSoot005.ptw');
%}
ptwInfo.m_filename = ptwFullPath;
if (isempty(ptwInfo.m_filename))
    error('file not assigned');
end;
    
ptwInfo = FLIR_PTWFileInfo(ptwInfo);
%% Reading the frames into memory
frameHolder(ptwInfo.m_rows,ptwInfo.m_cols,ptwInfo.m_nframes)=0; %Preallocation
for ind1 = ptwInfo.m_firstframe:ptwInfo.m_nframes % ptwInfo.m_firstframe === 1
    [frameHolder(:,:,ind1),~,~] = FLIR_PTWGetFrame(ptwInfo.m_filename, ind1-1);    
end
% See other, possibly optimized suggestions to do this here:
% http://stackoverflow.com/questions/25408923/working-with-binary-files-in-matlab-load-as-is-to-memory-and-interpret-later
%% Averaging over the 3rd dimension:
avgFrame = mean(frameHolder,3); % return type is double!
% [~]=[]; ...DEBUG