function [ImageDL, ptw, frameTime] = FLIR_PTWGetFrame (filename, frameindex, varargin)

% load FLIR ptw file image at frameindex from a ptw file.
% Input : 
%   filename    : File path
%   frameindex  : Frame index within the sequence to read
%   varargin    : (optional) FLIR_PTWFileInfo
% Output :
%   ImageDL     : 2D array in DL (counts)
%   ptw         : structure filled with ptw file header information
%   frameTime   : time of frame acquisition (HH:mm:SS.uuu)

    s.m_filename = filename;
    if (isempty(s.m_filename))
        error('file not assigned');
    end;
    
    if nargin == 3
        s = varargin{1};
    elseif nargin == 2
        s = FLIR_PTWFileInfo(s);
    end
    
    ptw = s;
    frameTime = struct;
    if(frameindex < s.m_lastframe && frameindex>=0) % ok
        s.m_framepointer = frameindex;
        
        fid=fopen(s.m_filename,'r');                                % open file
        if fid==-1
            error('file open');
        end;

        fseek (fid, s.m_MainHeaderSize,'bof');                    % skip main header
        fseek (fid, (s.m_framepointer) * (s.m_FrameSize), 'cof'); % skip previous image + image header
%       fseek (fid,s.m_FrameHeaderSize,'cof');                    % skip this frame header
        FrameHeader = fread(fid,s.m_FrameHeaderSize,'uint8');        
        frameTime.Minute = FrameHeader(81);
        frameTime.Hour   = FrameHeader(82);
            h_hundred = FrameHeader(83)*10;
            h_second  = FrameHeader(84);
            h_thousands = FrameHeader(161);
        frameTime.Second = h_second+(h_hundred+h_thousands)/1000;
        
        s.m_data = fread(fid, [s.m_cols, s.m_rows],'uint16');     %read one frame
        
        % if a special scale is given then transform the data
        if(s.m_specialscale)
            low = min(s.m_scalevalue);
            high = max(s.m_scalevalue);
            s.m_data = s.m_data .* (high-low)./ 2^16 + low; 
            clear low high;
        end; %if
        s.m_minval = min(min(s.m_data(1:s.m_cols,2:s.m_rows)));
        s.m_maxval = max(max(s.m_data(1:s.m_cols,2:s.m_rows)));
        fclose(fid);                                                %close file
   
        ImageDL = s.m_data';
        clear s;
    else                            % frameindex exceeds no of frames
        disp('Error: cannot load frame. Frameindex exceeds sequence length.');
    end;
end

