function [setsSkipped,setsSorted] = PTWFixMultiITFolder(fullPathToDir,filesPerGroup,... <- Required
                                           forceDLChecks,sortOrder,outputDir,knownIT) % <- Optional
% This function is used to pre-process PTW files output by Altair in Multi-IT scenarios,
% which seem to be shuffled.
% What the script does is open all files, read the IT, optionally do a bit of processing
% to decide the correct order and write the corrected ITs back into the files.
%
% INPUTS
%   fullPathToDir - full path to the directory containing the PTW files to be processed.
%   filesPerGroup - The number of ITs present in each set. Should be an integer that is 2<=x<=4
%   forceDLChecks - (OPTIONAL) An optional boolean flag indicating whether the DL of the
%                   images will be used to determine the correct IT order. Should be 
%                   equally or more accurate at the cost of additional I/O operations.
%                   Default: false.
%   sortOrder     - (OPTIONAL) A string representing the supposed sorting order of the IT in the files. 
%                   valid inputs: 'ascend' or 'descend'.
%                   Default: 'descend'.
%   outputDir     - (OPTIONAL) A parameter specifying where the output files should be  
%                   saved. If unspecifid, the original folder shall be used and original 
%                   files are therefore modified. 
%                   Default: fullPathToDir.
% OUTPUTS:
%   setsSkipped - a counter of skipped sets.
%   setsSorted - a counter of modified sets.
% ----------------------------------------------------------------------------------
% TODO:
% 1) Provide the First IT and the expected increments so that a sanity check may be done
%    on the ITs read from files.
% ----------------------------------------------------------------------------------
% Debug: PTWFixMultiITFolder('H:\badfiles',4)
% By Iliya Romm - 01/09/2015
%% Init
fclose('all'); %Optional
%% Input Checking
assert(~isempty(fullPathToDir) && ~isempty(filesPerGroup),'Missing input arguments.');
if ~isdir(fullPathToDir)
  throw(MException('MultiITFixer:InvalidPath','Invalid folder path, stopping.'));
end
if ~isscalar(filesPerGroup) || (filesPerGroup < 2 || filesPerGroup > 4) || ...
    filesPerGroup ~= round(filesPerGroup)
    throw(MException('MultiITFixer:InvalidSubgroupSize',['Invalid filesPerGroup, please '...
      'provide one of: [2, 3, 4]']));
end
if nargin < 3 || isempty(forceDLChecks)
  forceDLChecks = false;
end
if nargin < 4 || isempty(sortOrder)
  sortOrder = 'descend';
end
if nargin < 5 || isempty(outputDir)
  outputDir = fullPathToDir;
end
if nargin < 6 || isempty(knownIT)
  knownIT = [];
else
  if isvector(knownIT)
    knownIT = reshape(sort(knownIT,sortOrder),[],filesPerGroup);
  else
  % do nothing with knownIT, since user is assumed to know what he's doing.
  end
end
%% Obtain a file list:
fList = dir(fullfile(fullPathToDir,'*.ptw')); fList = {fList.name}';
numSubGroups = length(fList)/filesPerGroup;
assert(isempty(knownIT) || numel(knownIT) == numel(fList) || numel(knownIT) == filesPerGroup,...
  'knownIT must be equal to the number of files in the folder, or to the number of files per subgroup.');
assert(numSubGroups == round(numSubGroups),'Wrong number of PTW files in the provided folder.');
%% Preallocation:
setsSorted = 0;
%% Process files:
for indSG = 1:numSubGroups
  filepaths = cell(filesPerGroup,1);
  for indF = 1:filesPerGroup
    filepaths {indF} = fullfile(fullPathToDir,fList{(indSG-1)*filesPerGroup+indF});
  end  
  %% Write the correct IT into the file
  if ~strcmp(fileparts(filepaths{1}),outputDir)
    if ~isdir(outputDir)
      mkdir(outputDir);
    end
    for indF = 1:filesPerGroup
      if ~copyfile(filepaths{indF},outputDir);
        warning(['File copy failed for indF = ' num2str(indF)]);
      end
    end
  else % if the directory is the same, display a prompt before overwriting.
    if ~exist('answer','var') %only prompt once
      answer = input(['Source and target directories are identical - you are about to '...
                      'edit the original files. Would you like to proceed? y\\n\n'],'s');
    end
    if ~strcmpi('y',answer)
      disp('Non-positive response detected to prompt. Exiting...');  
      return;
    end
  end  
%%  
  if size(knownIT,1) == numSubGroups % the case where knownIT is supplied separately for each subgroup
    setsSorted = setsSorted + PTWFixMultiITGroup(filepaths,forceDLChecks,sortOrder,outputDir,knownIT(indSG,:));
  else
    setsSorted = setsSorted + PTWFixMultiITGroup(filepaths,forceDLChecks,sortOrder,outputDir,knownIT);
  end
end
setsSkipped = numSubGroups - setsSorted;