function dts = InterpretFromMemory(fName)
%% Definitions
UINT8_IN_CHAR = 1;
UINT8_IN_INT16 = 2;
UINT8_IN_INT32 = 4; 
UINT8_IN_FLOAT = 4;
ZERO_POS = 1;
%% DEBUG
%{
ptw1 = InterpretFromMemory('F:\Iliya-PhD\MATLAB\Downloaded\FLIR Matlab SDK\PTWFile\14_08_07_AlWithSoot005.ptw');
%}
%% Initialization
dts = struct('m_filename',fName);
%% Reading
fid=fopen(fName);
fComplete = fread(fid,'*uint8'); 
fclose(fid);
%%
curr_pos = 11;
dts.m_MainHeaderSize = readFromArrayAndAdvance(fComplete,1,'int32');
dts.m_FrameHeaderSize = readFromArrayAndAdvance(fComplete,1,'int32');

curr_pos = 27;
dts.m_nframes = readFromArrayAndAdvance(fComplete,1,'int32');

curr_pos = 212;
dts.m_fHousingTemp  = readFromArrayAndAdvance(fComplete,1,'float');
dts.m_fHousingTemp2 = readFromArrayAndAdvance(fComplete,1,'float');

curr_pos = 245;
dts.m_minlut = readFromArrayAndAdvance(fComplete,1,'int16');
dts.m_maxlut = readFromArrayAndAdvance(fComplete,1,'int16');

function [out]=readFromArrayAndAdvance(hArray,numVals,valType)
    switch valType
        case 'char'
            numBytes = UINT8_IN_CHAR;
            newValType = 'uint8'; %equivalent to char yet supported by typecast            
        case {'int16','uint16'}
            numBytes = UINT8_IN_INT16;
            newValType = valType;
        case {'int32','uint32'}
            numBytes = UINT8_IN_INT32;  
            newValType = valType;
        case 'float'
            numBytes = UINT8_IN_FLOAT;
            newValType = 'single'; %equivalent to float yet supported by typecast            
    end
    out = typecast(hArray(...
        curr_pos+ZERO_POS:curr_pos+ZERO_POS+numVals*numBytes-1),newValType);
    curr_pos = curr_pos + numVals*numBytes;
    if strcmp(valType,'char')
        out=char(out);
    end
end

end
