function wasSorted = PTWFixMultiITGroup(filepathsCell,... <- Required
             forceDLChecks,sortOrder,outputDir,knownIT) % <- Optional
% This function is used to pre-process PTW files output by Altair in Multi-IT scenarios,
% which seem to be shuffled.
% What the script does is open all files, read the IT, optionally do a bit of processing
% to decide the correct order and write the corrected ITs back into the files.
%
% INPUTS
%   filepathsCell - Cell containing the full paths to the PTW files to be processed.
%   forceDLChecks - (OPTIONAL) An optional boolean flag indicating whether the DL of the
%                   images will be used to determine the correct IT order. Should be 
%                   equally or more accurate at the cost of additional I/O operations.
%                   Default: false.
%   sortOrder - (OPTIONAL) A string representing the supposed sorting order of the IT in the files. 
%                valid inputs: 'ascend' or 'descend'. Default value: 'descend'.
%   outputDir - (OPTIONAL) A parameter specifying where the output
%                files should be saved. If unspecifid, the original folder shall be used 
%                and original files are then modified. default: fullPathToDir.
%   knownIT   - (OPTIONAL) The ITs that should be assigned to the files, if known. 
% OUTPUTS:
%   wasSorted - a boolean output stating whether the current set was sorted or not.
% ----------------------------------------------------------------------------------
% Debug: 
%   PTWFixMultiITGroup(cellstr(strcat('H:\badfiles\',ls('H:\badfiles\*.ptw'))),4,[],'H:\badfiles\fixed\')
%   PTWFixMultiITGroup(cellstr(strcat('H:\badfiles\',ls('H:\badfiles\*.ptw'))),4,[],'H:\badfiles\fixed\',)
% By Iliya Romm - 01/09/2015
% warning('This script is under work. Please use `PTWFixMultiITFolder` for now. Exiting...');
% return
DEFAULT_EXT = '.ptw';
wasSorted = false; %default output
%% Input Checking
assert(~isempty(filepathsCell),'Missing input arguments.');
if ~iscell(filepathsCell) || isempty(filepathsCell)
  throw(MException('MultiITFixer:InvalidPath','Invalid file path, stopping.'));
end
filesPerGroup = numel(filepathsCell);
if ~isscalar(filesPerGroup) || (filesPerGroup < 2 || filesPerGroup > 4) || ...
    filesPerGroup ~= round(filesPerGroup)
    throw(MException('MultiITFixer:InvalidSubgroupSize',['Invalid filesPerGroup, please '...
      'provide one of: [2, 3, 4]']));
end
if nargin < 2 || isempty(forceDLChecks)
  forceDLChecks = false;
end
if nargin < 3 || isempty(sortOrder)
  sortOrder = 'descend';
end
if nargin < 4 || isempty(outputDir)
  outputDir = fileparts(filepathsCell{1}); %assuming all files are in the same folder
elseif ~isdir(outputDir) % if a directory was supplied, but it doesn't exist, create it.
  mkdir(outputDir);
end
if nargin < 5 || isempty(knownIT)
  knownIT = [];
end

if ~isempty(knownIT) && ~forceDLChecks
  warning('It is probably a BAD IDEA to specify knownIT without forcing DL checks!');
%   warning(['It is probably a [' 8 'BAD IDEA ]' 8 'to specify knownIT without forcing DL checks!']);
end
%% Preallocation:
frame{filesPerGroup}=[];
fName = filepathsCell;
%% Load files & get info
for indF = 1:filesPerGroup
  tmpInf.m_filename = filepathsCell{indF};
  ptwInfoIMG(indF) = FLIR_PTWFileInfo(tmpInf); %#ok
  [~,fName{indF},~] = fileparts(filepathsCell{indF});
  fName{indF} = strcat(fName{indF},DEFAULT_EXT);
end
old_ITs = single([ptwInfoIMG.m_integration]);
%% Check if sorting is required (according to the direction provided in sortOrder):
if ~issorted((1-2*strcmp(sortOrder,'descend'))*old_ITs) || any(~diff(old_ITs)) || forceDLChecks
  %% Obtain the correct order:
  [sortedITs,newOrder] = sort(old_ITs,sortOrder);
  %% Test if old ITs make sense:
  if any(~diff(sortedITs)) && isempty(knownIT)
    warning('Repeating ITs found! Sorting may not succeed!')
  end        
  if forceDLChecks
    for indF = 1:filesPerGroup
      frame{indF} = FLIR_PTWGetFrame(ptwInfoIMG(indF).m_filename,0);
    end
    [~,DLOrder] = sort(cellfun(@(x)mean(x(:)),frame,'UniformOutput',true),sortOrder);
    % If DLOrder is the same as the sorting indices of the old order (newOrder), it 
    % means that the old order is correct and no change needs to be done.
    % Alternatively
    if (isequal(DLOrder,newOrder) || ...
          (~isempty(diff(old_ITs(DLOrder(DLOrder~=newOrder)))) && ...
                all(diff(old_ITs(DLOrder(DLOrder~=newOrder)))==0) )) && ...
        norm(knownIT-sortedITs,1) < 5e-7                         
      return
    end
    % If DLOrder is sorted, it means that DL changes with the filename, which means that
    % a simple sorting of the IT, as is done when computing newOrder, is sufficient:
    newOrder = DLOrder;
    if ~issorted((1-2*strcmp(sortOrder,'ascend'))*DLOrder)
      disp('DLOrder disagrees with newOrder, taking DLOrder.'); %TODO: change error msg to correspond to problem
    end
  end    
  if isempty(knownIT)
    new_ITs = 0*old_ITs; new_ITs(newOrder) = sortedITs(1:filesPerGroup); %TODO: test if works with reverse order too.
  else
    new_ITs = knownIT;
  end
  for indF = 1:filesPerGroup
    fid = fopen(fullfile(outputDir,fName{indF}),'r+');
    if fid == -1
      copyfile(filepathsCell{indF},fullfile(outputDir,fName{indF}));
      fid = fopen(fullfile(outputDir,fName{indF}),'r+');
    end
    fseek(fid, 407, 'bof');% 407 is the position of IT, see FLIR_PTWFileInfo
    fwrite(fid,new_ITs(indF),'float32'); 
    fclose(fid);
  end
%% Set output:
  wasSorted = true;
% else
%   wasSorted = false;
end    