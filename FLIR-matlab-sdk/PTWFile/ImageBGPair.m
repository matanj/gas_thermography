classdef ImageBGPair < handle
  %IMAGEBGPAIR Is a class for storing pairs of "scene" and "backgorund" for easy
  % association.
  %   TODO: Detailed explanation goes here
  
  properties (Constant = true, Access = private)
    FIELDNAME_IT = 'm_integration';
    FIELDNAME_SIZE_X = 'm_cols';
    FIELDNAME_SIZE_Y = 'm_rows';
    FIELDNAME_FILTER = 'm_filter';
    FIELDNAME_FRAMERATE = 'm_frameperiod';
  end
  
  properties (GetAccess = public, SetAccess = private)
    ImageFullPath = [];
    BackgroundFullPath = [];
    BG_Info = [];
    IMG_Info = [];
    IT = [];
    Filter = [];
  end
  
  methods (Access = public)
    function obj = ImageBGPair(ImageFullPath,IMG_Info)
      if nargin == 0
        return
      end
      if nargin == 1 && ~ischar(ImageFullPath) % when preallocating   
        S = struct; S.type = '()'; 
        if isscalar(ImageFullPath) % in case only a length value is specified
          S.subs = {ImageFullPath,1};
        else % when mimicing the size of an array     
          S.subs = num2cell(size(ImageFullPath));
        end
        obj = subsasgn(obj,S,ImageBGPair);
        return
      end
      obj.ImageFullPath = ImageFullPath;
      if nargin >= 2
        obj.IMG_Info = IMG_Info;
      else
        obj.IMG_Info = PTWFileUtils.getFileInfo(ImageFullPath);
      end
      obj.IT = single(obj.IMG_Info.(ImageBGPair.FIELDNAME_IT));
      obj.Filter = obj.IMG_Info.(ImageBGPair.FIELDNAME_FILTER);
    end
    
    function setBG(thisIBP, BackgroundFullPath, BG_Info)
      % Obtain the image info
      if nargin >= 3
        tmp_Info = BG_Info;
      else
        tmp_Info = PTWFileUtils.getFileInfo(BackgroundFullPath);
      end
      % Make sure the relevant parameters of the images are the same
%       if thisIBP.isValidPair(thisIBP.IMG_Info,tmp_Info)
        thisIBP.BackgroundFullPath = BackgroundFullPath;
        thisIBP.BG_Info = tmp_Info;       
%       end
       
    end
    
    function out = getSubtracted(thisIBP)
      % OUTPUT:
      % 3- or 4-D array, where dims 1,2 are physical, dim 3 is frames per file, and dim 4
      % is different ITs (in case thisIBP is a vector of equally-sized image pairs). 
      % NOTE:
      %   When a 4-D array is returned, it is usually safe to average it over the 3rd 
      %   dimension. This is not done automatically because there exist algorithms smarter
      %   than "plain averaging" (e.g. NLM-Multi).
      if ~isscalar(thisIBP) && ismatrix(thisIBP) %input is array of "ImageBGPair"s
        out = cell(size(thisIBP)); % preallocation
        for ind1 = 1:numel(thisIBP)
          out{ind1} = thisIBP(ind1).getSubtracted;
        end
        % convert to numeric matrix if all cells are the same size
        if ~diff(any(diff(cell2mat(cellfun(@size,out,'UniformOutput',false)))))
          out = cat(4,out{:});
        end
        return
      elseif isempty(thisIBP.BG_Info)
        warning('Attempterd to obtain subtracted image for an incomplete pair!');
        out = []; return
      end
      img = PTWFileUtils.ReadAllFramesFromFile(thisIBP.ImageFullPath,     thisIBP.IMG_Info);
      bg  = PTWFileUtils.ReadAllFramesFromFile(thisIBP.BackgroundFullPath, thisIBP.BG_Info);
      % First average the BG and then subtract it from each images. 
      %   There may be a different amount of images and bg frames.
      out = bsxfun(@minus, double(img), mean(bg,3)); % the output of `mean` is already double 
    end
  end % public methods
  
  methods (Access = private, Static = true)
    % The following method ensures that a specified image pair is valid. The order of the
    % inputs is not important, though the image info should preferably come first.
    function tf = isValidPair(imgOrBgInfo, bgOrImgInfo, IT_tol)
      if nargin < 3 || isempty(IT_tol)
        IT_tol = 2*eps(imgOrBgInfo.m_integration);
      end
      % Check IT equality:
      if abs( bgOrImgInfo.(ImageBGPair.FIELDNAME_IT) - ...
              imgOrBgInfo.(ImageBGPair.FIELDNAME_IT) ) > IT_tol
        throw(MException('setBG:timeMismatch', 'Mismatch between IT of BG and IMG.'));
      % Check size equality:
      elseif (bgOrImgInfo.(ImageBGPair.FIELDNAME_SIZE_X) ~= imgOrBgInfo.(ImageBGPair.FIELDNAME_SIZE_X)) || ...
             (bgOrImgInfo.(ImageBGPair.FIELDNAME_SIZE_Y) ~= imgOrBgInfo.(ImageBGPair.FIELDNAME_SIZE_Y))
        throw(MException('setBG:sizeMismatch', 'Mismatch between size of BG and IMG.'));
      % Check filter equality:
      elseif ~strcmp(bgOrImgInfo.(ImageBGPair.FIELDNAME_FILTER),...
                     imgOrBgInfo.(ImageBGPair.FIELDNAME_FILTER))
        throw(MException('setBG:filterMismatch', 'Mismatch between filter of BG and IMG.'));
      % TODO: add check for whether image is rotated or not.
      else
        tf = true;
      end
    end
  end
  
end

