classdef PTWFileUtils
    %PTWFILEUTILS Summary of this class goes here
    %   Detailed explanation goes here
    
    properties (Access = private, Constant = true)
        ZERO_BASED_FRAME_OFFSET = 1;
    end
    
    methods (Access = public, Static = true)
        
        function varargout = ReadFramesFromFile(ptwFullPath,nFrames,varargin)
            % here, nFrames is assumed to include 1-based indexing
            %todo: validity checking on nFrames
            
            % Get the file info:
            if nargin == 3
                ptwInfo = varargin{1};
            else
                ptwInfo = PTWFileUtils.getFileInfo(ptwFullPath);
            end
            
            % Read all required frames:
            frameHolder(ptwInfo.m_rows,ptwInfo.m_cols,length(nFrames))=0; %Preallocation
            holderInd = 1;
            for ind1 = nFrames
                [frameHolder(:,:,holderInd),~,frameTimes] = ...
                    FLIR_PTWGetFrame(ptwInfo.m_filename,...
                    ind1-PTWFileUtils.ZERO_BASED_FRAME_OFFSET,ptwInfo);    
                holderInd = holderInd + 1;
            end
            
            % Assign outputs
            if nargout == 0, warning('Outputs not assigned!'); end
            if nargout >= 1, varargout{1} = frameHolder; end
            if nargout >= 2, varargout{2} = ptwInfo; end
            if nargout >= 3, varargout{3} = frameTimes; end 
                                
        end
        
        function [frameHolder,ptwInfo] = ReadAllFramesFromFile(ptwFullPath,ptwInfo)
            if nargin < 2 || isempty(ptwInfo)
              % Read file info:
              ptwInfo = PTWFileUtils.getFileInfo(ptwFullPath);
            end
            % Read all frames:
            frameHolder = uint16(PTWFileUtils.ReadFramesFromFile(ptwFullPath,...
                ptwInfo.m_firstframe:ptwInfo.m_nframes,ptwInfo));
              % ptwInfo.m_firstframe === 1
        end
        
        function avgFrame = ReadAndAverageFramesFromFile(ptwFullPath)
            % Average the result
            avgFrame = mean(double(PTWFileUtils.ReadAllFramesFromFile(ptwFullPath)),3);
        end        
        
        function ptwInfo = getFileInfo(ptwFullPath)
            ptwInfo = struct('m_filename',ptwFullPath);
            if (isempty(ptwInfo.m_filename))
                error('file not assigned');
            end

            ptwInfo = FLIR_PTWFileInfo(ptwInfo);
        end
        
        function imagePairs = classifyInfoStructsIntoPairs(structOfImageInfos,structOfBackgoundInfos)         
          [indIMG,indBG] = find(bsxfun(@eq,[structOfImageInfos.m_integration],[structOfBackgoundInfos.m_integration]'));
          if numel(indIMG) ~= numel(structOfImageInfos) % can also check using BG
            throw(MException('FindPairs:MatchNotFound', 'No match found for at least one pair of images.'));
          else            
            for ind1 = numel(indIMG):-1:1
              imagePairs(ind1) = ImageBGPair(structOfImageInfos(indBG(ind1)).m_filename,structOfImageInfos(indBG(ind1)));
              imagePairs(ind1).setBG(structOfBackgoundInfos(indBG(ind1)).m_filename,structOfBackgoundInfos(indBG(ind1)));
            end
          end
        end
        
        function imagePairs = classifyFilenamesIntoPairs(cellOfImagePaths,cellOfBackgroundPaths)
          if numel(cellOfImagePaths) ~= numel(cellOfBackgroundPaths)
            throw(MException('FindPairs:InputSizeMismatch', 'Mismatch between size of BG and IMG cells.'));
          end
          for ind1 = numel(cellOfImagePaths):-1:1
            imgInfo(ind1) = PTWFileUtils.getFileInfo(cellOfImagePaths{ind1});
            bgInfo(ind1) = PTWFileUtils.getFileInfo(cellOfBackgroundPaths{ind1});
          end
          imagePairs = PTWFileUtils.classifyInfoStructsIntoPairs(imgInfo,bgInfo);
        end
          
    end
    
end

