function ptw = FLIR_PTWFileInfo(ptw)
% FLIR_PTWFileInfo
% Reads a FLIR .ptw file header and creates a ptw information structure.
% INPUTS: 
%   ptw.m_filename : File path
% OUTPUTS:
%   ptw - structure filled with file header information
%{
Iliya-2016: Full structure of the file is available in:
In the PDF:                             "DL002U-J Altair Reference Guide.pdf"
- WORD <-> uint16
- DWORD <-> uint32
- QWORD <-> uint64

Iliya-2015: Notes regarding the python code:
- More info @ https://github.com/NelisW/pyradi/blob/master/pyradi/ryptw.py
- Note that a PTW file is Little-endian (the LSB is in the smallest address).
In the python code:
- ord(...) = what's the numeric value of the char? equivalent to uint8().
- myint <-> int16
- mylong <-> int32
%}
%% Open the file for reading
fid=fopen(ptw.m_filename,'r');
if fid==-1
    error('fileopen');
end
%% Read infromation from the main header (PTR_HEADER)
fseek(fid, 0, 'bof'); % Same as: frewind(fid); 
ptw.m_Signature = fread(fid,3,'*char').';  % Iliya-2015: Signature
if strcmp(ptw.m_Signature,'AIO') %AGEMA
    ptw.m_format = 'agema';
elseif strcmp(ptw.m_Signature,'CED') %CEDIP \ FLIR
    ptw.m_format = 'cedip';
else
    ptw.m_format = 'N\A';
end
fseek(fid, 5, 'bof');
ptw.m_Version = deblank(fread(fid,6,'*char').');  % Iliya-2015: Version
% fseek(fid, 11, 'bof');
ptw.m_MainHeaderSize = fread(fid,1,'int32');  % MainHeaderSize
% fseek(fid, 15, 'bof');
ptw.m_FrameHeaderSize = fread(fid,1,'int32'); % FrameHeaderSize
% fseek(fid, 19, 'bof');
ptw.m_OneFandHSize = fread(fid,1,'int32');    % Iliya-2015: OneFrameAndHeaderSize
% fseek(fid, 23, 'bof');
ptw.m_FrameSize = fread(fid,1,'int32');       % Iliya-2015: OneFrameSize (should be 248?)
% fseek(fid, 27, 'bof');
ptw.m_nframes = fread(fid,1,'uint32');        % Iliya-2015: # imgs in file
% fseek(fid, 31, 'bof');
ptw.m_CurrFrame = fread(fid,1,'int32');       % Iliya-2015: Current img #
% fseek(fid, 35, 'bof');
ptw.m_FileDate.Year = fread(fid,1,'uint16');  % Iliya-2015: YY
% fseek(fid, 37, 'bof');
ptw.m_FileDate.Day = fread(fid,1,'uint8');    % Iliya-2015: DD
% fseek(fid, 38, 'bof');
ptw.m_FileDate.Month = fread(fid,1,'uint8');  % Iliya-2015: MM
% fseek(fid, 39, 'bof');
ptw.m_FileDate.Minute = fread(fid,1,'uint8'); % Iliya-2015: mm
% fseek(fid, 40, 'bof');
ptw.m_FileDate.Hour = fread(fid,1,'uint8');   % Iliya-2015: HH
% fseek(fid, 41, 'bof');
ptw.m_FileDate.Cent = fread(fid,1,'uint8');   % Iliya-2015: (sec/100)
% fseek(fid, 42, 'bof');
ptw.m_FileDate.Second = fread(fid,1,'uint8'); % Iliya-2015: SS
% fseek(fid, 43, 'bof');
ptw.m_FileDate.Milli = fread(fid,1,'uint8');  % Iliya-2015: milliseconds (?)
% fseek(fid, 44, 'bof');
ptw.m_CameraName = deblank(fread(fid,20,'*char').'); % Iliya-2015
% fseek(fid, 64, 'bof'); 
ptw.m_lens = deblank(fread(fid,20,'*char').');       % Iliya-2014: Lens info
% fseek(fid, 84, 'bof'); 
ptw.m_filter = deblank(fread(fid,20,'*char').');     % Iliya-2014: Filter info
% fseek(fid, 104, 'bof'); 
ptw.m_aperture = deblank(fread(fid,20,'*char').');   % Iliya-2015: Aperture info
%{
    self.h_IRUSBilletSpeed = 0 #[124:128] # IRUS
    self.h_IRUSBilletDiameter = 0 #myfloat(headerinfo[128:132]) # IRUS
    self.h_IRUSBilletShape = 0 #myint(headerinfo[132:134]) #IRUS
%}
fseek(fid, 141, 'bof'); 
ptw.h_Emissivity = fread(fid,1,'float');           % Iliya-2016: Aperture info
% fseek(fid, 145, 'bof'); 
ptw.h_Ambiant = fread(fid,1,'float');              % Iliya-2016: Background temperature [K]
% fseek(fid, 149, 'bof'); 
ptw.h_Distance = fread(fid,1,'float');             % Iliya-2016: Distance [m]
%{
    self.h_IRUSInductorCoil = 0 #ord(everthing[153:154]) # IRUS
    self.h_IRUSInductorPower = 0 #mylong(headerinfo[154:158]) # IRUS
    self.h_IRUSInductorVoltage = 0 #myint(headerinfo[158:160]) # IRUS
    self.h_IRUSInductorFrequency = 0 #mylong(headerinfo[160:164]) # IRUS
    self.h_IRUSSynchronization = 0 #ord(headerinfo[169:170]) # IRUS
%}
fseek(fid, 170, 'bof'); 
ptw.h_AtmTransmission = fread(fid,1,'float');      % Iliya-2016: Atmospheric Transmission [%]
% fseek(fid, 174, 'bof'); 
ptw.h_ExtinctionCoeficient = fread(fid,1,'float'); 
%{
% fseek(fid, 178, 'bof'); 
ptw.h_Object = fread(fid,1,'int16');
% fseek(fid, 180, 'bof'); 
ptw.h_Optic = fread(fid,1,'int16');
% fseek(fid, 182, 'bof'); 
ptw.h_Atmo = fread(fid,1,'int16');
%}
fseek(fid, 184, 'bof'); 
ptw.h_AtmosphereTemp = fread(fid,1,'float');       % Iliya-2016: Atsmosphere Temperature [K]
% fseek(fid, 188, 'bof'); 
ptw.h_CutOnWavelength = fread(fid,1,'float');      % Iliya-2016: Cut On wavelength @ 50% [um]
% fseek(fid, 192, 'bof'); 
ptw.h_CutOffWavelength = fread(fid,1,'float');     % Iliya-2016: Cut Off wavelength @ 50% [um]
% fseek(fid, 196, 'bof'); 
ptw.h_PixelSize = fread(fid,1,'float');            % Iliya-2016: Pixel size [um]
% fseek(fid, 200, 'bof'); 
ptw.h_PixelPitch = fread(fid,1,'float');           % Iliya-2016: Pixel pitch [um]
% fseek(fid, 204, 'bof'); 
ptw.h_DetectorApperture = fread(fid,1,'float');    % Iliya-2016: F# (Detector aperture)
% fseek(fid, 208, 'bof'); 
ptw.h_OpticsFocalLength = fread(fid,1,'float');    % Iliya-2016: Optics focal length [mm]
% fseek(fid, 212 , 'bof');
ptw.f_HousingTemp = fread(fid,1,'float');    % Camera housing internal temperature [K]
% fseek(fid, 216 , 'bof');
ptw.f_HousingTemp2 = fread(fid,1,'float');   % 2nd camera housing temperature
% fseek(fid, 220, 'bof'); 
ptw.m_serialnumber = str2double(fread(fid,11,'*char')'); %Iliya-2014: Camera S\N
% fseek(fid, 231, 'bof'); 
% [~] = fread(fid,14,'*char')'; < reserved 14 characters> 
fseek(fid, 245, 'bof');
ptw.m_minlut = fread(fid,1,'int16');          % Min LUT value used for display
% fseek(fid, 247, 'bof');
ptw.m_maxlut = fread(fid,1,'int16');          % Max LUT value used for display
if(ptw.m_maxlut==-1)
    ptw.m_maxlut=2^16-1;
end
% fseek(fid, 249, 'bof'); 
% [~] = fread(fid,28,'*char')'; < reserved 28 characters> 
fseek(fid, 277, 'bof');
ptw.m_specialscale = fread(fid,1,'uint16');   % Special scale (Echelle Speciale)
% fseek(fid, 279, 'bof');
scaleunit = fread(fid,10,'*char').';          % Scale Unit
% fseek(fid, 289, 'bof');
ptw.m_scalevalue = fread(fid,17,'float');     % Values for special scale; Echelle[16 + 1]
if ptw.m_specialscale == 0
    ptw.m_unit = 'DL';                        % File contains raw DL value from camera
else
    ptw.m_unit = scaleunit;                   % File contains processed DL value
end
% fseek(fid, 357, 'bof');
ptw.h_LockinGain = fread(fid,1,'float');      % Iliya-2016: Gain scale factor for lockin images
% fseek(fid, 361, 'bof');
ptw.h_LockinOffset = fread(fid,1,'float');    % Iliya-2016: Offset scale factor for lockin images
% fseek(fid, 365, 'bof');
ptw.h_HorizontalZoom = fread(fid,1,'float');  % Iliya-2016: Horizontal zoom factor
% fseek(fid, 369, 'bof');
ptw.h_VerticalZoom = fread(fid,1,'float');    % Iliya-2016: Vertical zoom factor
% fseek(fid, 373, 'bof'); 
% [~] = fread(fid,2,'*char')'; < reserved 2 characters> 
% fseek(fid, 375, 'bof'); 
% [~] = fread(fid,2,'*char')'; < reserved 2 characters> 
fseek(fid, 377, 'bof');
ptw.m_cols = fread(fid,1,'uint16');           % Pixels per line (# cols)
if ~ptw.m_cols % == 0
   ptw.m_cols = 256;
end
% fseek(fid, 379, 'bof');
ptw.m_rows = fread(fid,1,'uint16');           % Lines per frame (# rows)
if ~ptw.m_rows % == 0
   ptw.m_rows = 320;
end
% fseek(fid, 381, 'bof');
ptw.m_bitres = fread(fid,1,'uint16');         % bit resolution / dynamic range
% fseek(fid, 383, 'bof');
ptw.m_TemporalFrameDepth = fread(fid,1,'uint16'); % Iliya-2015
% fseek(fid, 385, 'bof');
ptw.f_Longitude = fread(fid,1,'float');    % GPS longitude location (in �)
% fseek(fid, 389, 'bof');
ptw.f_Latitude =  fread(fid,1,'float');    % GPS latitude location (in �, negative values mean south)
% fseek(fid, 393, 'bof');
ptw.f_Altitude =  fread(fid,1,'float');    % GPS altitude (in m)
% fseek(fid, 397, 'bof');
ptw.f_ExtSynchro = ~~fread(fid,1,'uint8');   % Camera in External Synchronisation, 1=External 0=Internal
% fseek(fid, 398, 'bof'); 
% [~] = fread(fid,5,'*char')'; < reserved 5 characters> 
fseek(fid, 403, 'bof');
ptw.m_frameperiod = round(1/fread(fid,1,'float')); % frame acquisition period [Hz]
% fseek(fid, 407, 'bof');
ptw.m_integration = single(fread(fid,1,'float'));  % Integration time [s]
%{
    self.h_WOLFSubwindowCapability = 0 #myint(headerinfo[411:413]) # WOLF
    self.h_ORIONIntegrationTime = 0 #myfloat(headerinfo[413:437]) # ORION (6 values)
    self.h_ORIONFilterNames = '' #headerinfo[437:557]) # ORION 6 fields of 20 chars each
%}
fseek(fid, 557, 'bof');
ptw.m_NucTable = fread(fid,1,'uint16');     % Current NUC table
% fseek(fid, 559, 'bof'); 
% [~] = fread(fid,4,'*char')'; < reserved 4 characters> 
fseek(fid, 563, 'bof');
ptw.m_comment = deblank(fread(fid,1000,'*char').'); % User Comment 
% fseek(fid, 1563, 'bof');
ptw.m_calibration = fread(fid,100,'*char').'; % calibration file name
%{
    self.h_ToolsFileName = '' #headerinfo[1663:1919]
    self.h_PaletteIndexValid = 0 #ord(headerinfo[1919:1920])
    self.h_PaletteIndexCurrent = 0 #myint(1920:1922])
    self.h_PaletteToggle = 0 #ord(headerinfo[1922:1923])
    self.h_PaletteAGC = 0 #ord(headerinfo[1923:1924])
    self.h_UnitIndexValid = 0 #ord(headerinfo[1924:1925])
    self.h_CurrentUnitIndex = 0 #myint(headerinfo[1925:1927])
    self.h_ZoomPosition = 0 #(headerinfo[1927:1935]) # unknown format POINT
    self.h_KeyFrameNumber = 0 #ord(headerinfo[1935:1936])
    self.h_KeyFramesInFilm = 0 #headerinfo[1936:2056] # set of 30 frames
    self.h_PlayerLocked = 0 # ord(headerinfo[2057:2057])
    self.h_FrameSelectionValid = 0 #ord(headerinfo[2057:2058])
    self.h_FrameofROIStart = 0 #mylong(headerinfo[2058:2062])
    self.h_FrameofROIEnd = 0 #mylong(headerinfo[2062:2066])
    self.h_PlayerLockedROI = 0# ord(headerinfo[2066:2067])
    self.h_PlayerInfinitLoop = 0 #ord(headerinfo[2067:2068])
    self.h_PlayerInitFrame = 0 #mylong(headerinfo[2068:2072])

    self.h_Isoterm0Active = 0 #ord(headerinfo[2072:2073])
    self.h_Isoterm0DLMin = 0 #myint(headerinfo[2073:2075])
    self.h_Isoterm0DLMax = 0 #myint(headerinfo[2075:2077])
    self.h_Isoterm0Color = 0 #headerinfo[2077:2081]

    self.h_Isoterm1Active = 0 #ord(headerinfo[2081:2082])
    self.h_Isoterm1DLMin = 0 #myint(headerinfo[2082:2084])
    self.h_Isoterm1DLMax = 0 #myint(headerinfo[2084:2086])
    self.h_Isoterm1Color = 0 #headerinfo[2086:2090]

    self.h_Isoterm2Active = 0 #ord(headerinfo[2090:2091])
    self.h_Isoterm2DLMin = 0 #myint(headerinfo[2091:2093])
    self.h_Isoterm2DLMax = 0 #myint(headerinfo[2093:2095])
    self.h_Isoterm2Color = 0 #headerinfo[2095:2099]

    self.h_ZeroActive = 0 #ord(headerinfo[2099:2100])
    self.h_ZeroDL = 0 #myint(headerinfo[2100:2102])
    self.h_PaletteWidth = 0 #myint(headerinfo[2102:2104])
    self.h_PaletteFull = 0 #ord(headerinfo[2104:2105])
    self.h_PTRFrameBufferType = 0 #ord(headerinfo[2105:2106]) # 0=word 1=double
    self.h_ThermoElasticity = 0 #headerinfo[2106:2114] # type double (64 bits)
    self.h_DemodulationFrequency = 0 #myfloat(headerinfo[2114:2118])

fseek(fid, 2118, 'bof');
ptw.h_CoordinatesType = fread(fid,1,'int32');
fseek(fid, 2122, 'bof');
ptw.h_CoordinatesXorigin = fread(fid,1,'int32');
fseek(fid, 2126, 'bof');
ptw.h_CoordinatesYorigin = fread(fid,1,'int32');

    self.h_CoordinatesShowOrigin = 0 #ord(headerinfo[2130:2131])
    self.h_AxeColor = 0 #headerinfo[2131:2135]
    self.h_AxeSize = 0 #mylong(headerinfo[2135:2139])
    self.h_AxeValid = 0 #ord(headerinfo[2139:2140])
    self.h_DistanceOffset = 0 #myfloat(headerinfo[2140:2144])
    self.h_HistoEqualizationEnabled = 0 #ord(headerinfo[2144:2145])
    self.h_HistoEqualizationPercent = 0 #myint(headerinfo[2145:2147])
    self.h_CalibrationFileName = '' #headerinfo[2147:2403]
    self.h_PTRTopFrameValid = 0 #ord(headerinfo[2403:2404])
%}
fseek(fid, 2404, 'bof');
ptw.h_SubSampling = fread(fid,1,'int16');
% fseek(fid, 2408, 'bof');
ptw.h_CameraHFlip = ~~fread(fid,1,'uint8');
% fseek(fid, 2409, 'bof');
ptw.h_CameraVFlip = ~~fread(fid,1,'uint8');
%{
% fseek(fid, 2410, 'bof');
% ptw.h_BBTemp = fread(fid,1,'float');
% fseek(fid, 2414, 'bof');
% ptw.h_CaptureWheelIndex = fread(fid,1,'uint8');
    self.h_CaptureFocalIndex = 0 #ord(headerinfo[2415:2416])
    self.h_Reserved7 = '' #headerinfo[2416:3028]
    self.h_Reserved8 = '' #headerinfo[3028:3076]
    self.h_Framatone = 0 #ord(headerinfo[3076:3077]
%}
fseek(fid, 3024, 'bof'); %NOTE: DIFFERENT FROM THE PYTHON FILE!!!
ptw.f_StreamP2 = fread(fid,1,'float');       % Iliya-2016: Hypercal V2 Param
% fseek(fid, 3028, 'bof');
ptw.h_dwSizeByte = fread(fid,1,'uint32');     % Iliya-2016: Hypercal structure size
% fseek(fid, 3032, 'bof');
ptw.h_hypercalTypeId = fread(fid,1,'uint32'); % Iliya-2016: Hypercal calibration type (version of hypercal)
% fseek(fid, 3036, 'bof');
ptw.h_LinearityDLMin = fread(fid,1,'uint32'); % Min DL of linear region
% fseek(fid, 3040, 'bof');
ptw.h_LinearityDLMax = fread(fid,1,'uint32'); % Max DL of linear region
% fseek(fid, 3044, 'bof');
ptw.h_CalibTempMin = fread(fid,1,'float');    % Minimum calibrated temperature
% fseek(fid, 3048, 'bof');
ptw.h_CalibTempMax = fread(fid,1,'float');    % Maximum calibrated temperature
% fseek(fid, 3052, 'bof');
ptw.h_CalibParamC  = fread(fid,1,'float');    % Hypercal param C
% fseek(fid, 3056, 'bof');
ptw.h_CalibParamD  = fread(fid,1,'float');    % Hypercal param D
% fseek(fid, 3060, 'bof');
ptw.h_CalibParamF  = fread(fid,1,'float');    % Hypercal param F
% fseek(fid, 3064, 'bof');
ptw.h_CalibParamE  = fread(fid,1,'float');    % Hypercal param E
% fseek(fid, 3068, 'bof');
ptw.h_CalibParamB  = fread(fid,1,'float');    % Hypercal param B
% fseek(fid, 3072, 'bof');
ptw.h_CalibParamA  = fread(fid,1,'float');    % Hypercal param A
%% Read additional information from the 1st frame header (PTR_IMAGE)
mh = ptw.m_MainHeaderSize; 
fseek(fid,ptw.m_MainHeaderSize,'bof');  % skip main header 
%{
% [~] = fread(fid,80,'*char')'; < reserved 80 characters> 
fseek(fid,mh+80, 'bof'); 
<time1, see FLIR_PTWGetFrame>
fseek(fid,mh+84, 'bof'); 
% [~] = fread(fid,76,'*char')'; < reserved 76 characters> 
fseek(fid,mh+160, 'bof'); 
[~] = fread(fid,1,'uint8');     % Iliya-2016: <time2>
fseek(fid,mh+161, 'bof'); 
[~] = fread(fid,1,'uint16');    % Iliya-2016: <time3>
fseek(fid,mh+163, 'bof'); 
[~] = fread(fid,1,'float');     % Iliya-2016: Demodulaton frequency for this image
...
%}
fseek(fid,mh+248,'bof');
ptw.b_LockinEnable = fread(fid,1,'uint16'); % Iliya-2016: Lockin version (0 == disabled)
%{
    # look if first line contains lockin information
    if(firstline[1:4]==[1220,3907,1204,2382]):
        Header.h_Lockin=1
        Header.h_Rows=Header.h_Rows-1
        print ('* LOCKIN')
    else:
        Header.h_Lockin=0  
%}
fseek(fid,mh+278,'bof');
ptw.b_Wnd = ~~fread(fid,1,'uint16');      % Iliya-2016: Is sub-windowing enabled?
% fseek(fid,mh+280,'bof');
WndLeft = fread(fid,1,'int16');     % Iliya-2016: Left coordinate
% fseek(fid,mh+282,'bof');
WndTop = fread(fid,1,'int16');      % Iliya-2016: Top coordinate
% fseek(fid,mh+284,'bof');
% ptw.n_WndWidth = fread(fid,1,'int16');    % Iliya-2016: Window width
% fseek(fid,mh+286,'bof');
% ptw.n_WndHeight = fread(fid,1,'int16');   % Iliya-2016: Window height
% fseek(fid,mh+288,'bof');
% ptw.f_Integration = single(fread(fid,1,'float')); % Iliya-2016: Integration Time (us)
% ...
% fseek(fid,mh+301,'bof');
% ptw.CameraTimeStamp = fread(fid,1,'uint64'); % Iliya-2016: Time stamp from camera (us)
%% Package some info in a nicer way:
ptw.m_framepointer  = 1;
ptw.m_firstframe    = 1;
ptw.m_cliprect      = [WndLeft WndTop ptw.m_cols ptw.m_rows];
ptw.m_lastframe     = ptw.m_nframes;
ptw.m_FrameSize     = ptw.m_FrameHeaderSize + ptw.m_cols * ptw.m_rows * 2;
%% Finalize & Cleanup:
fclose(fid);