clear; close all; clc
xSizes = [1e4,1e5,1e6,1e7];
y=1e-4;
norm_err_16_terms = zeros(size(xSizes));
norm_err_12_terms = zeros(size(xSizes));
speedup_16 = zeros(size(xSizes));
speedup_12 = zeros(size(xSizes));
for indSize = 1 : numel(xSizes)
  x=linspace(0,10,xSizes(indSize));
  tic;VF_approx_16_terms = voigtf(x,y,1); t_approx_16 = toc;
  tic;VF_approx_12_terms = voigtf(x,y,2); t_approx_12 = toc;
  tic;VF = real(fadf(x+1i*y)); t = toc;
  norm_err_16_terms(indSize) = norm(VF_approx_16_terms-VF);
  norm_err_12_terms(indSize) = norm(VF_approx_12_terms-VF);
  speedup_16(indSize) = t/t_approx_16;
  speedup_12(indSize) = t/t_approx_12;
end

figure; plot(VF); title('Voigt profile');

figure; semilogx(xSizes, norm_err_16_terms); title('||error|| Vs x size'); hold on;
semilogx(xSizes, norm_err_12_terms);legend('16 terms approx.','12 terms approx.');

figure; semilogx(xSizes, speedup_16); title('speedups Vs x size'); hold on;
semilogx(xSizes, speedup_12);legend('16 terms approx.','12 terms approx.');
