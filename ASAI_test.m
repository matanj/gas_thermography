function ASAI_test
% Overview of script_fringe_synth.m %
% 1) Open data file to obtain fringe shift information and axis information.
% 2) Smooth data by flipping over symmetry axis.
% 3) Call Abel inversion algorithm one column of data at a time.
% 4) Multiply by correct factor to obtain density and plot.

close all; clc;

% Loop to validate and improve no_resolve zone
flag=1;
while flag==1
  % Creates the zone with no resolve from interferogram
  no_resolve=imresize(roipoly,[1023,1023]);
  % Determine line of symmetry location by summing perpendicular to
  % symmetry and determining location of the maximum of sums. NaNs must be
  % removed from data for sum() function.
  ZI_noNaN=ZI;
  ZI_noNaN(isnan(ZI_noNaN))=0;
  ZZI=ZI.*(1-no_resolve);
  ZZI(ZZI<=0)=0;
  [trash,data.center]=max(sum(ZI_noNaN,2));
  % Cut fringe shift data along symmetry axis.
  data.set1 = ZI(data.center:end,:);
  data.set2 = flipud(ZI(1:data.center,:));
  data.length = [min([size(data.set1,1), size(data.set2,1)]), size(data.set1,2)];
  % Smooth fringe shift data by averaging across symmetry axis.
  N=zeros(data.length(1),data.length(2));
  %As adding a number and NaN will result in NaN, must check if, while
  % flipping, one of the values is NaN. If so, use only the other value, if
  % both, set fringe shift to 0. Otherwise, average two numbers for
  % smoothing.
  for i=1:data.length(1)
    for j=1:data.length(2)
      N(i,j) = (data.set1(i,j)+data.set2(i,j))/2;
    end
  end
  % Gets the matrix that countains the NaNs
  matrix_nan=isnan(N);
  % Replaces all NaN with zeros, necessary for abel inversion program
  for i=1:data.length(1)
    for j=1:data.length(2)
      if isnan(N(i,j))
        N(i,j)=0;
      end
    end
  end
  % Plots the matrix that will be passed to the abel inversion routine
  figure(2)
  imagesc(N)
  % User may select the zone where data is irrelevant/incorrect (too
  % close to the target surface).
  no_resolve2=imresize(roipoly,[data.length(1),1023]);
  close all
  figure(3)
  imagesc(N.*(1-no_resolve2))
  %ask if this part is OK
  ButtonName=questdlg('Happy with the no_resolve zone?');
  switch ButtonName
    case 'Yes'
      flag=0;
    case'No'
      close
    case'Cancel'
      return
  end
end
N=N.*(1-no_resolve2);

%% ABEL INVERSION ROUTINE
% First, define the constants and necessary matrices
% Lambda used later and R must have same dimensions. Use centimeters.
R=data.length(1)*(YI(2,1)-YI(1,1))*1e-4;
% Create axis arrays r and y.
y=(0.0001:1:data.length(1))*(R/data.length(1));
r=y;

% knots = locations of endpoints for spline fits; change for better
% resolution
knots=0.:.01*R:R;
g_calc=zeros(data.length(1),data.length(2));
progressbar;

% Call inversion algorithm.
disp 'Performing Abel inversion'
for i=1:data.length(2)
  progressbar(i/data.length(2));
  % N vector must be rotated to be a column vector.
  g_calc(:,i)=abel_inversion(r,rot90(N(:,i)),y,knots);
end

% Multiply by proper factors to obtain scaling of g(r) in cm^-3.
lambda=46.9e-9;
lambda_microns=lambda*1e6;
lambda_cm=lambda*1e2;
n_cr=1.1e21/lambda_microns^2;
g_new=2*n_cr*lambda_cm*g_calc;

% Creates a meshgrid to read the data
rr=1e4*[flipud(r);r];
[xt,yt]=meshgrid(XI(1,:),rr);
% Plot inversion solution.
figure;
% Negative values may result from fittings, so remove for log plot.
h=g_new;
h(h<0)=NaN;
densities=[flipud(h);h];

% plots result of abel inversion
figure
pcolor(xt,yt,log10(densities));shading interp;colorbar;
set(gca,'clim',[19,22]);

end

%% Abel inversion algorithm.
function g_calc=abel_inversion(r,I,y,knots)
% Calculate necessary varibles not passed through function call.
n=size(I,2)+1;
R=max(r);
g=zeros(1,n-1);
% Create I_fit block with fit information.
% pieces = # of segments
% breaks = locations of endpoints of splines
I_fit.pieces=size(knots,2)-1;
I_fit.breaks=knots;
% Fit segments with a cubic function and store coefficients.
% a*y^3 + b*y^2 + c*y + d
for i=1:I_fit.pieces
  [trash, start_loc] = min(abs(y-I_fit.breaks(i)));
  [trash, end_loc] = min(abs(y-I_fit.breaks(i+1)));
  I_fit.coefs(i,:) = polyfit(y(start_loc:end_loc),I(start_loc:end_loc),3);
end


%% ABEL INVERSION ALGORITHM %%
% See Deutsch and Beniaminy (1982) for exact expression. Note: algorithm as
% printed needs correction. See correction notes.
for i=1:n-2
  % Identify location of next break point.
  next=I_fit.breaks;
  next(next<r(i))=NaN;
  [trash,next_loc]=min(next);
  for j=next_loc:size(I_fit.breaks,2)-1
    g(i) = g(i) + I_fit.coefs(j,1)*(K4(I_fit.breaks(j+1),r(i))-K4(I_fit.breaks(j),r(i))) + ...
      I_fit.coefs(j,2)*(K3(I_fit.breaks(j+1),r(i))-K3(I_fit.breaks(j),r(i))) + ...
      I_fit.coefs(j,3)*(K2(I_fit.breaks(j+1),r(i))-K2(I_fit.breaks(j),r(i))) + ...
      I_fit.coefs(j,4)*(K1(I_fit.breaks(j+1),r(i))-K1(I_fit.breaks(j),r(i)));
  end
  g(i) = g(i) + I_fit.coefs(next_loc-1,1)*(K4(I_fit.breaks(next_loc),r(i))-K4(r(i+1),r(i))) + ...
    I_fit.coefs(next_loc-1,2)*(K3(I_fit.breaks(next_loc),r(i))-K3(r(i+1),r(i))) + ...
    I_fit.coefs(next_loc-1,3)*(K2(I_fit.breaks(next_loc),r(i))-K2(r(i+1),r(i))) + ...
    I_fit.coefs(next_loc-1,4)*(K1(I_fit.breaks(next_loc),r(i))-K1(r(i+1),r(i)));
  g(i) = g(i) + (I_fit.coefs(end,1)*R^3+I_fit.coefs(end,2)*R^2+I_fit.coefs(end,3)*R+I_fit.coefs(end,4))/sqrt(R^2-r(i)^2) - ...
    (I_fit.coefs(next_loc-1,1)*r(i)^3+I_fit.coefs(next_loc-1,2)*r(i)^2+I_fit.coefs(next_loc-1,3)*r(i)+I_fit.coefs(next_loc-1,4))/sqrt(r(i+1)^2-r(i)^2);
end
g_calc=-g/pi;

end

function K1=K1(y,r)
s=sqrt(y^2-r^2);
K1=-1/s;
end
function K2=K2(y,r)
s=sqrt(y^2-r^2);
K2=-y/s+log(y+s);
end
function K3=K3(y,r)
s=sqrt(y^2-r^2);
K3=s-r^2/s;
end
function K4=K4(y,r)
s=sqrt(y^2-r^2);
K4=y*s/2-r^2*y/s+(3/2)*r^2*log(y+s);
end