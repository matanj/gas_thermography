function [newIm, XY_flip] = alignIM2BiasFrame(im, bf)
% TODO: BAD quality measurement!!!! use "findFlipState.m"
% Gets image and bias frame. Returns aligned image 'newIm' and 'XY_flip' - struct of two booleans:
% whether im is flipped horizontally &/| vertically relative to bias frame.
  if nargin < 2
    error('invalid args');
  end
  if ( (size(im,1)~=size(bf,1)) || (size(im,2)~=size(bf,2)) )
    error('size don''t match!');
  end
  qf  = @(x) norm(x(~isnan(x))); % quality function
  %% permutation definition (all flips are from initial im orinetation relative to the bias frame):
  % '00' - both aligned to bf
  % '01' - im flipped vertically
  % '10' - im flipped horizontally
  % '11' - im flipped both
  q = 1:4; % for norm2
  % '00':
  q(1) = qf(im-bf);
  % '01':
  imFlippedVert = flipud(im);
  q(2) = qf(imFlippedVert-bf);
  % '10':
  imFlippedHor = fliplr(im);
  q(3) = qf(imFlippedHor-bf);
  % '11':
  imFlippedBoth = fliplr(imFlippedVert);
  q(4) = qf(imFlippedBoth-bf);
  
  [~,flip] = min(q);
  flip = flip-1; % convert index to the 0:3 convention above
  XY_flip.isFlippedHorz = (flip==2 || flip==3);
  XY_flip.isFlippedVert = (flip==1 || flip==3);
  
  switch flip % align im to the bf
    case 1
      newIm = imFlippedVert;
    case 2
      newIm = imFlippedHor;
    case 3
      newIm = imFlippedBoth;
    otherwise
      newIm = im;
  end
end