function [ biasSubframe ] = suitBiasSubframeFromBiasFrame( im, imInfo, biasFramePath )
%SUITBIASSUBFRAMEFROMBIASFRAME uses Cross-Correlatin to determine the proper 
%sub frame from a given bias frame, that is suitable to a given image.
if nargin < 2
  error('invalid args!');
end
S = load(biasFramePath);
biasFrameFull = S.IM - S.mu + S.muM; % shift correctly. Size is the max size of Camera- 512x640.
rect = imInfo.m_cliprect + [1 1 -1 -1]; % Altair's to Matlab indexing

biasFrameFullArr{1} = biasFrameFull;
biasFrameFullArr{2} = fliplr(biasFrameFull);
biasFrameFullArr{3} = flipud(biasFrameFull);
biasFrameFullArr{4} = rot90(biasFrameFull,2);

cc = xcorr2(biasFrameFullArr{1}, im);
[max_cc(1), imax(1)] = max(abs(cc(:)));

cc2 = xcorr2(biasFrameFullArr{2}, im);
[max_cc(2), imax(2)] = max(abs(cc2(:)));

cc3 = xcorr2(biasFrameFullArr{3}, im);
[max_cc(3), imax(3)] = max(abs(cc3(:)));

cc4 = xcorr2(biasFrameFullArr{4}, im);
[max_cc(4), imax(4)] = max(abs(cc4(:)));

[~,I] = max(max_cc);
bf = biasFrameFullArr{I};
biasSubframe = bf(rect(2):rect(2)+rect(4), rect(1):rect(1)+rect(3));
switch I
  case 1
  case 2
    biasSubframe = fliplr(biasSubframe);
  case 3
    biasSubframe = flipud(biasSubframe);
  case 4
    biasSubframe = rot90(biasSubframe,2);
end
%{
figure; imagesc(im); colorbar;
figure; imagesc(biasSubframe); colorbar;
%}

end

