function [CC, TT, depths] = GenerateTemperatureConcentrationModelsFit2image(imSize, m2pix, yNozzle_loc, x0_loc, params)
% Gets image 'im', and put on it a map of temperature & concentration based
% on a known gaussian model
% INPUTS:
%   'imSize'  - size of the image
%   'm2pix' - meter to pixels conversion
%   'yNozzle_loc' - index location of the nozzle (Verticaly)
%   'x0_loc' - index location of the jet's symmetry axis (horizontally)
%   'params' - struct with experiment conditions
% OUTPUTS:
%   'C', 'T' - images with concentrations & temperatures modeled
%   'paths' - image with each pixel representing estimated depth of the jet

% S. Sivakumar (2012) - "Characteristics of Turbulent Round Jets in its
% Potential-Core Region": gives estimate on decay coeff. of velocity.
% 

%% Coordinate system: x axis - radial, y axis - along the centerline

%% TODO: improve model when CFD isn't used
  if nargin < 4
    error('invalid args!');
  end
  %%default Inputs
  useCFD = true;
%   CFDpath = 'Z:\Matan\Jet CFD\FreeJet.xlsx';
  D = 1e-2; % Nozzle's diameter, [m]
  tot_mfr = 2; % Total mass flow rate, [g/s]
  co2_mfr = 0.5; % CO2 mass flow rate, [g/s]
  Tamb_C = 25; % ambient temperature, [C]
  Tjet_C = 400; %Jet's outlet temperature, [C]
  Camb = 0;%0.0004; % CO2 concentration in the ambient
  P0 = 101325; %[Pa]. Pressure @ outlet is 1 atm = 101325[Pa]
  if nargin>4
    useCFD = params.useCFD;
%     CFDpath = params.CFDpath;
    data = params.data;
    D = params.D;
    tot_mfr = params.tot_mfr; % Total mass flow rate, [g/s]
    co2_mfr = params.co2_mfr; % CO2 mass flow rate, [g/s]
    Tamb_C = params.Tamb_C; % ambient temperature, [C]
    Tjet_C = params.Tjet_C; %Jet's outlet temperature, [C]
    Camb = params.Camb;% CO2 concentration in the ambient
    P0 = params.P0; %[Pa]. Pressure @ outlet
  end

  M = imSize(1);
  N = imSize(2);

  %Constants
  R = 8.314459848; % universal gas constant [J/mol/K]
  molar_mass_air = 28.9645; %[g/mol]
  moalr_mass_co2 = 44.01; %[g/mol]

  % Auxiliary functions
  C2K = @(T) T + 273.15; % Celsius to Kelvin
  rhoIdealGas = @(p,T,mol_mas) p/(R/mol_mas*1e3*T); % Ideal gas equation, T[K], p[Pa], mol_mas[g/mol]
  %%
  Tamb = C2K(Tamb_C);
  Tjet = C2K(Tjet_C);
  A = pi/4*D^2; %Nozzle's area[m^2]
  C0 = co2_mfr/tot_mfr;

  [mu_co2, mu_air] = CalcViscousity(Tjet); % get the viscousity in the jet's temp
  mu_mass_avged = ( co2_mfr*mu_co2 + (tot_mfr-co2_mfr)*mu_air ) / tot_mfr;% Mass-averaged viscousity

  rho_air = rhoIdealGas(P0,Tjet,molar_mass_air);
  rho_co2 = rhoIdealGas(P0,Tjet,moalr_mass_co2);
  %%% I can take just mu_air, instead of mu_mass_avged. ~1.5% error
  rho_avged = C0*rho_co2 + (1-C0)*rho_air;%averaged density [kg/m^3]
  U0 = tot_mfr*1e-3/A/rho_avged; % Outlet velocity [m/s]
  Re = rho_avged*U0*D / mu_mass_avged; % Reynolds number in the orifice
%   Re_alternative = tot_mfr*1e-3/A*D/mu_mass_avged; % since m_dot = rho*U*A

  g = 9.8; %gravity [m/s^2]
  beta = 1/Tjet; % thermal expansion coefficient for ideal gas
  ni = mu_mass_avged/rho_avged; % kinematic viscosity [m^2/s]
  Gr = g*beta*(Tjet-Tamb)*D^3/ni^2;% Grashof number
  Ar = Gr/Re^2; %Archimedes number, to compare buoyant to inertial forces
  if Ar > 1e-4
    warning('Buoyancy might not be neglegable!');
  end

  %     alpha_air = 2.2e-5; %thermal diffusivity [m^2/s]  of air @ T=300K
  %     Pr = ni/alpha_jet % Prandtl number - ratio of momentum and thermal diffusivites
  
  Pr = 0.71;
  Pe = Re*Pr; % Peclet number - advection to diffusion ratio in forced convection


  y_im = (1:yNozzle_loc)';
  x_im = (1:N)';
  
  %TODO: should have a LUT where I enter Re and get fit parameters.

  if useCFD
    %% NOTE: it is assumed that the CFD image is greater than the measurement, and 
    %% for the CFD: Tjet = 600K, Tamb = 300K 
    CFD_PIPE_LENGTH_M = 0.11; % The pipe length in the CFD [m]
    CFD_PIPE_LENGTH_CM = 100*CFD_PIPE_LENGTH_M; %[cm]
    cm2pix = m2pix/100;
    % build the y & r vectors in [pixels].
    % NOTE: here y is decreasing
    y = (yNozzle_loc - y_im + 1);
    r = (x_im - x0_loc + 1);
    r = r(r>0);
    
    % TODO: better to use spatial reference object and align this system to
    %the original image?
    
    % Get the data from the CFD simulation file. convert [m]->[cm].
    Y = 100*data(:,1) - CFD_PIPE_LENGTH_CM; % set the orifice as Y=0
    R = 100*data(:,2);
    T = data(:,3);
    T = (T-300)/(600-300)*(Tjet-Tamb) + Tamb; % convert CFD to my case using temperature dimentioness variable 'theta'
   
    R = ceil(cm2pix * R); % convert to [pixels].
    indValidR = (R > 0);
    Y = ceil(cm2pix * Y); % convert to [pixels].
    indValidY = (Y > 0);
    indValid = indValidR & indValidY;

    R = R(indValid);
    Y = Y(indValid);
    T = T(indValid);
    
    [TT] = griddata(R',Y,T,r',y, 'nearest'); % no need for other as I want integer pixel coordinates
    if x0_loc <= N/2
      TT = [fliplr(TT(:,2:x0_loc+1)), TT];
    else
      TTT = [fliplr(TT(:,2:end)), TT];
      TT = [nan(M,N-size(TTT,2)), TTT];
    end
    CC = (TT- Tamb)/(Tjet-Tamb)*(C0-Camb) + Camb; % get the concentrations
    depths = [];
  else
    %% Get an estimate of the centerline T & C by eq. [1] in 
    %% [Xu, 2002 - "Effects of initial conditions on the temperature field of a turbulent round free jet"]:
    % Here, y0 is the location of virtual origin:
    % (Tc-Tamb)/(Tj-Tamb) = C1*D/(y-y0) and
    % C1=4.6, y0 = 2.6D @ Re~8e4  &  C1=4.6, y0 = 4.7D @ Re~1.6e4
    % ====> Tc = Tamb + C1*D/(y-y0)*(Tj-Tamb)
    if Re < 2e4
      y0 = 4.7*D;
    else
      y0 = 2.6*D;
    end
%     radii_expansion_rate = D/2/y0;

    % build the y & r vectors in [m].
    % NOTE: here y is decreasing
    y = y0 + (yNozzle_loc - y_im)./m2pix;
    r = (x_im - x0_loc)./m2pix;
    r = r(r>=0);

    [yy,rr] = meshgrid(y,r);
    yy = yy';
    rr = rr';
    %% Calc the profiles based on "Enviromental Fluid Mechanics" ch.9.
    %% There, a universal opening angle of ~11.8 deg is used
    CC = Camb +  0.5*5*D./yy*(C0-Camb).*exp(-50*rr.^2./yy.^2); % 0.5 factor was added to fix orifice's value to C0
    
    %     C = 5*D./yy.*exp(-50*rr.^2./yy.^2);
    %     C = exp(-50*rr.^2./yy.^2);
    
    %     deltaT = 0.5*5*D./yy.*(Tjet-Tamb).*exp(-50*rr.^2./yy.^2);
    
    % "Convective Heat Transfer", 4th edition, p.411 :
    % T-T8 = (Tc-T8) * exp(-(r/bT)^2)
    % Where: bT ~ 0.127y
    %        Tc-T8 =~ 5.65(T0-T8)*D/y
    
    TT = Tamb + 0.5*5.65*D./yy.*(Tjet-Tamb).*exp(-62*rr.^2./yy.^2); %OLD
%     TT = Tamb + (yy-y0<3*D).*(Tjet-Tamb) +...
%       (yy-y0>=3*D).*0.5*5.65*D./yy.*(Tjet-Tamb).*exp(-62*rr.^2./yy.^2);
    % Thus, the virtual origin is -3.937D ~ -4D from the orifice.
    
    D_y = 0.127*(y-y(end))+D;% diameter of the jet as function of y
    %   cc = 5.65/2/y(end); % Constant
    %   T_cl_y = Tamb + (Tjet-Tamb)*0.5*5.65./y/cc; %centerline temperature as function of y
    %   C_cl_y = 0.5*C0*0.5*5*D./y; %centerline concentration as function of y
    depths = nan(M,N);
    for iy = 1:M %iterate over y:
      Dy = D_y(iy); % estimated jet diameter
      depths(iy, abs(r) <= Dy/2 ) = 2*sqrt((Dy/2)^2 - r(abs(r) <= Dy/2).^2);
    end
    
    
    %%%%%
    % 2 options:
    % 1) Given virtual source y0, D(y) = D*(1+y/y0)
    % 2) Given angle a: y0 = D/2/tan(a) and again D(y) from above.
    
    y0 = 2.87*D + y(end); % x0/D
    % In [pixels]:
    y = y_im;
    r = (x_im - x0_loc + 1);
    r = r(r>0);
    D = D * m2pix;
    y0 = y0 * m2pix;
    
    [yy,rr] = meshgrid(y,r);
    yy = yy';
    rr = rr';
    
    C1 =  4.83;%4.6; % decay constant - also "B" in Xu (2002)
%     TT = Tamb + C1*D./abs((yy-y0)).*(Tjet-Tamb).*exp(-62*rr.^2./(yy-y0).^2);
    TT = Tamb + (y0-y(end))./(y0-yy).*(Tjet-Tamb).*exp(-62*rr.^2./(yy-y0).^2);
  end
  
end