clear; close all; warning off; dbstop if error

CALC_CORRECTIONS = false;

load('Nestor_Olsen_correction_factor_fit.mat');
% rng('default');
% rng(999,'twister'); % set the random generator and the seed
%% %%%%% Simulating arbitrary f(r) and reconstruct it: %%%%%%%%%%%%
%% Noise can be added to the sensor as well as to the initial distribution.
alpha = 0.1; % MFH parameter
R = 1; %Radius [cm]
N = 31; %#pixels
sigma = 0.02; % Additive noise sigma (on original distribution)
sigmaI = 0; % Additive noise sigma (on the integral))
test_case = 'flat';% 'gaussian';% 'gaussian', 'triangle' , 'flat'

r = (0.000001:N)'*(R/N);
e_r = zeros(size(r));
X = r;
switch test_case % Test function
  case 'triangle'
    f_no_noise = linspace(1,0,numel(r))';
  case 'flat'
    f_no_noise = tukeywin(2*numel(r), 0.25);
    f_no_noise = f_no_noise(floor(end/2)+1:end);
  otherwise % Gaussian
    f_no_noise = exp(-2*(r/R).^2);
end

f_no_noise = f_no_noise - min(f_no_noise(:));
f_no_noise = f_no_noise/f_no_noise(1); % Normalize the test function
 
% sigma = 1*min(f_no_noise(:));
f = f_no_noise + sigma*randn(size(f_no_noise)); % Add noise

disp(['SNR (f(r)) = ',num2str(snr(f_no_noise, f-f_no_noise)),' [dB]']);

% Simulate Abel transform that reflects the sensor integration along LOS:
h=zeros(length(X),1); % allocate result vector
for c=1:length(X)
  x=X(c);
  % evaluate Abel-transform equation:
%   fun = @(r) (R*exp(-0.01*r.^2)).*r./sqrt(r.^2 - x.^2);
  fun = @(r) (f(c,1)).*r./sqrt(r.^2 - x.^2);
  h(c)=2*integral(fun,x,R);
end

I_no_noise = [flipud(h) ; h(1:end)]; % For the MFH algorithm we need entire row
% sigmaI = 1*min(I_no_noise(:));
I = I_no_noise + sigmaI*randn(size(I_no_noise)); % Add noise to the integral
disp(['SNR (I(r)) = ',num2str(snr(I_no_noise, I-I_no_noise)),' [dB]']);
I = I - min(I(:));
if numel(I) ~= 2*numel(r)
  error('radius and size of data mismatch!');
end
tic
f_rec_NO = NestorOlsenAbelInversion( I(N+1:end), R);
toc
% f_rec_NO = N* f_rec_NO; % TODO: check!!
tic
[f_rec_MFH] = MFHAbelInversion( I, R, alpha );
toc
tic
f_rec_FE = abel_inversion(I(N+1:end), R, 10, 0, 0, 0);
toc

tic
y=(0.0001:1:numel(I(N+1:end)))*(R/numel(I(N+1:end)));
r = y;
knots = 0.:.01*R:R;
f_rec_ASAI = ASAI( r, I(N+1:end)', y, knots );
toc

f_rec_Sainz = SainzAbelInversion( f_rec_NO, f_rec_ASAI);

% f_rec_NO = pi/2 * f_rec_NO; % fix
f_rec_NO = Nestor_Olsen_correction_factor_fit(N) * f_rec_NO; % Applying experimental correction to Nestor-Olsen
F_rec = forwardAbel(f_rec_NO,1);

err_MFH = f_rec_MFH - f_no_noise;
err_MFH_norm2 = norm(err_MFH)
err_MFH_normInf = norm(err_MFH,inf)
err_FE = f_rec_FE - f_no_noise;
err_FE_norm2 = norm(err_FE)
err_FE_normInf = norm(err_FE,inf)
err_NO = f_rec_NO - f_no_noise;
err_NO_norm2 = norm(err_NO)
err_NO_normInf = norm(err_NO, inf)

%% Plots:
figure; plot([-fliplr(r),r], I); title(['Measurement I(r), with noise \sigma=',num2str(sigmaI)]);
figure; plot(r,f_no_noise,'b', r,f_rec_FE,'m', r,f_rec_MFH,'r', r, f_rec_NO,'g', r, f_rec_ASAI,'y', r, f_rec_Sainz,'*');
title(['Comparison for ',test_case,' test function, N = ',num2str(N),' & SNR = ',num2str(snr(I_no_noise, I-I_no_noise)),' dB']);
legend(['Original (with noise \sigma=',num2str(sigma),')'],'FE',['MFH, \alpha=',num2str(alpha)],'N-O', 'ASAI', 'Sainz');
xlabel('X/R'); ylabel('Normalized');
figure; plot(r,f_no_noise/max(f_no_noise(:,1)),'b', r,f_rec_FE/max(f_rec_FE),'m', r,f_rec_MFH/max(f_rec_MFH),'r', r, f_rec_NO/max(f_rec_NO),'g', r, f_rec_ASAI/max(f_rec_ASAI));
title(['Normalized - Comparison for ',test_case,' test function, N = ',num2str(N),' & SNR = ',num2str(snr(I_no_noise, I-I_no_noise)),' dB']);
legend(['Original (with noise \sigma=',num2str(sigma),')'],'FE',['MFH, \alpha=',num2str(alpha)],'N-O','ASAI');

%% Find Nestor-Olsen correction factor p(N):
if CALC_CORRECTIONS
  N = (17:200)';
  NO_pN = zeros(numel(N),1);
  for i = 1 : numel(N)
    r = (0.000001:N(i))'*(R/N(i));
    X = r;
    switch test_case % Test function
      case 'triangle'
        f_no_noise = linspace(1,0,numel(r))';
      case 'flat'
        f_no_noise = tukeywin(2*numel(r), 0.25);
        f_no_noise = f_no_noise(floor(end/2)+1:end);
      otherwise % Gaussian
        f_no_noise = exp(-2*(r/R).^2);
    end
    f_no_noise = f_no_noise - min(f_no_noise(:));
    f_no_noise = f_no_noise/f_no_noise(1); % Normalize the test function
    % Simulate Abel transform that reflects the sensor integration along LOS:
    h=zeros(length(X),1); % allocate result vector
    for c=1:length(X)
      x=X(c);
      % evaluate Abel-transform equation:
      fun = @(r) (f_no_noise(c,1)).*r./sqrt(r.^2 - x.^2);
      h(c)=2*integral(fun,x,R);
    end
    I_no_noise = [flipud(h) ; h(1:end)]; % For the MFH algorithm we need entire row
    f_rec_NO = NestorOlsenAbelInversion( I_no_noise(N(i)+1:end), R);
    NO_pN(i) = 1/f_rec_NO(1);
  end
  
%   [xdata, ydata] = GasHelper.get_data_from_figure_of_plot('C:\Users\matanj\Documents\MATLAB\pixelwise_calibration\Figs\Nestor_Olesn_correction_factor.fig');
%     hold on;
%     plot(xdata, Nestor_Olsen_correction_factor_fit(xdata));
  % cftool(N, NO_pN);
  figure; plot(N, NO_pN); title('Nestor-Olsen correction factor Vs N');
end
