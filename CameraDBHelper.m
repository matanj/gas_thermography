classdef CameraDBHelper %< handle
  properties(Access = public)
    DBFolderPath % location of folder of experiment's files
    fNameFormat % files format
    PropList % list of properties in the experiment (e.g. filter, temp,...)
  end
  properties(Access = private)
    TotatFilesList = {}
    NumTotalFiles = 0;
  end
  methods(Access = public)
    function obj = CameraDBHelper(folderPath, propList, fNameFormat,ext) %Constructor
      if nargin < 2
        throw(MException('CameraDBHelper:invalid args', ...
            'Expecting at least folderLocation and propList'));
      elseif nargin < 3
        fNameFormat = 'G%s_T%05.1f_F%01.3f_IT%06.1f_%s_%s'; %Gxxx_Txxx_Fx.xxx_ITxxxx.x_BG_Geometry.ptw
        ext = 'ptw';
      elseif nargin < 4
        ext = 'ptw';
      end
      obj.DBFolderPath = folderPath;
      obj.PropList = propList;
      obj.fNameFormat = fNameFormat;
      obj.TotatFilesList = dir([folderPath,'/*.',ext]);
      obj.TotatFilesList = {obj.TotatFilesList(:).name}';
      obj.NumTotalFiles = numel(obj.TotatFilesList);
      if ~obj.NumTotalFiles || isempty(obj.TotatFilesList)
        throw(MException('CameraDBHelper:invalid DB', 'Error with folderLocatio of its files'));
      end
      
    end
    
    function totalFileList = getTotalFileList(obj)
      totalFileList = obj.TotatFilesList;
    end
    
    function nFiles = getNumFilesInDB(obj)
      nFiles = obj.NumTotalFiles;
    end
    
    function OK = areValidPropsValues(obj,props,values)
      % TODO: validateattributes(), inputParser()
      OK = false;
      if nargin < 3 || ~iscellstr(props) || ~iscell(values) || ...
          numel(props) > numel(obj.PropList) || ...
          numel(props) ~= numel(values)
        throw(MException('CameraDBHelper:invalid Props-Values', ...
            'Expecting 2 cell arrays of corresponding props-values'));
      end
      for indProp = 1:numel(props)
        if ismember(obj.TotatFilesList, props{indProp}) || ...
            ( class(props{indProp}) ~= class(values{indProp}) )
          throw(MException('CameraDBHelper:invalid Props-Values', ...
            'some properties not in DB, or not match its value!'));
        end
      end
      OK = true;
    end
        
    function fileQueryList = queryByProps(obj, props, values)
       % TODO: make a static function to allow recursive queries?
      fileQueryList = {};
      if nargin < 3 || ~areValidPropsValues(obj,props,values)
        throw(MException('CameraDBHelper:invalid Props-Values', ...
            'Expecting 2 cell arrays of valid corresponding props-values'));
      end
      
      fileQueryList = obj.TotatFilesList;
      % sort props according to the files format:
      if numel(props) > 1
        
      end
      
      for indProp = 1:numel(props)
        
      end
      
    end

  end
  
%   methods(Static = true)
%   end
  
end