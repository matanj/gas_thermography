classdef GUIImage
    % Info: Class for Image representation in GUI with associated common
    %       image processing etc.
    % By:   Matan 21-02-16 
    properties ( Access = private)
        im; imInfo; filtIm;
        % Operations outputs:
        stats = struct('meanDL',NaN,'std',NaN);
        MCDL = NaN;
    end
    
    methods (Access = public)
        function obj = GUIImage(imageFileName) % Constructor
           [obj.imInfo, obj.im] = loadImage(imageFileName);
        end
        function getMeanDL(obj)
            obj.meanDL = 1;
        end
    end
    
%%%%%%%%%%%%%%%%% Static methods %%%%%%%%%%%%%%%%%%%%%
    methods (Access = private, Static = true)
        function isPTW = isImagePTW(imFileName)
            if isempty(imFileName)
                error('File name not found.');
            end
            [~,~,ext] = fileparts(imFileName);
            isPTW = isequal(ext,'.ptw');
        end
        function meanVal = getMeanValueInRange(image)
           meanVal = mean(image(:));
        end
        function MCVal = getMCValueInRange(image)
           MCVal = []; %%%% TODO !!!!
        end 
        
        function boxIm = getBoxFromImage(image,pos)
            if isempty(image)
                error('Invalid image.');
            end
            [M,N] = size(image);
            if nargin < 2 % if no box is given, get interactively:
                hTemp = figure; imshow(image);
                h = imrect;%[xmin ymin width height]
                pos = wait(h);
                pos = uint16(pos);
                close(hTemp);
            end
            if (isempty(pos) || any(pos<=0) || ...
                    pos(1)+pos(3)-1 > N || pos(2)+pos(4)-1 > M)
                error('Invalid box.');
            end
            boxIm = image(pos(2):pos(2)+pos(4)-1,...
                          pos(1):pos(1)+pos(3)-1);
        end
        
        function [imInfo, im] = loadImage(imageFileName)
            if isImagePTW(imageFileName) % Currently only PTW is supported
                imInfo.m_filename = imageFileName;
                imInfo = FLIR_PTWFileInfo(imInfo);
                if imInfo.m_lastframe > 1
                    im = PTWAverageFramesInFiles(imInfo.m_filename);
                else
                    im = FLIR_PTWGetFrame(imInfo.m_filename,0);
                end
            end
        end 
    end
end