function [biasFrameFull, croppedBiasFrame ] = cropBiasFrame2PTWsize(biasFramePath, imInfo)
% Crops a "full HD" camera's bias frame the location of the given
% PTW image as specified by its 'imInfo'.
if nargin < 2
  error('invalid args!');
end
S = load(biasFramePath);
biasFrameFull = S.IM - S.mu + S.muM; % shift correctly. Size is the max size of Camera- 512x640.
% get the actual location of the image in the camera's full 512x640 coordinates
rect = imInfo.m_cliprect + [1 1 -1 -1]; % Altair's to Matlab indexing
% biasFrame = imcrop(biasFrameFull, rect);
croppedBiasFrame = biasFrameFull(rect(2):rect(2)+rect(4), rect(1):rect(1)+rect(3));
end

