function Compare_TC_samples_to_reconstructed_T_new(T_im, T_meas, D_nozzle_pixels, symAxis)
% Gets reconstructed temperature image 'T_im' in [K] and matches to a Nx4 
% matrix of N measurements 'T_meas': 1st column is #row, 2nd is #col 
% (in the raw image), 3rd is the measured temperature [C], 
% 4th is the measurement estimated error [C].
% 'D_nozzle_pixels' - Effective Jet Diameter [pixels]
% 'symAxis' - #columm corresponds to the axis of symmetry in raw the image
assert(size(T_meas,2) == 4);
assert(size(T_meas,1) <= size(T_im,1));

T_meas(:,1) = max(1, T_meas(:,1) - (max(T_meas(:,1)) - size(T_im, 1)) );
T_meas(:,2) = abs(T_meas(:,2) - symAxis) + 1;
T_meas(:,3) = T_meas(:,3) + 273.15; % convert to [K]

i_meas = sub2ind(size(T_im), T_meas(:,1), T_meas(:,2) );
abs_err = T_im(i_meas) - T_meas(:,3); % difference [K]

GasHelper.plotIm(T_im);
title(sprintf('Reconstructed Temperature [K]\n(red: deviations from T.C. samples)'));
hold on;
scatter(T_meas(:,2), T_meas(:,1), '*k');
text_props = {'Color','r','FontSize',9};
text(T_meas(:,2), T_meas(:,1), num2str(abs_err, 3),text_props{:});
GasHelper.normalize_axes_half_im(T_im, D_nozzle_pixels, 2, 5);
GasHelper.maximizeFigure;
end