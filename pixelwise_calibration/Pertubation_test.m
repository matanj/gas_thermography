close all;
%% INSTRUCTIONS:
% Run "script_JetFindTemp_clean" with breakpoint after setting X0.
% If other X0 is needed, modify at this point 'model_params_orig', then set 
% 'model_params = model_params_orig', then reevaluate X0, and then run this.
%% OPTIONS:
% w/ or w/o self-absorption:
WITH_SELF_ABSORPTION = true;
% Required Perturbations:
perturb.T0 = 0.05;
perturb.C0 = 0.1;
perturb.X_PC = -0.2;
perturb.Y_PC = -0.2;
perturb.r_0_5_up = -0.2;
perturb.r_0_5_down = -0.2;
perturb.b = 1;
% ylimit for all temperature relative error plots:
clim = [0 100]; % [%]
% Saved figures base dir path:
parentdir = 'C:\Users\matanj\Google Drive\AE_Research\Thesis\Figures';

%%
if WITH_SELF_ABSORPTION
  case_title_prefix = 'w/ self-absorption: ';
  dirname = 'Perturbation_test_include_self_abs';
  get_R = @(model_params) get_R_sim(model_params, false, false, atm_params, jetParams, calibParams, jet_boundary_line, geometry, symAxis, A_array, F_R, F_TAU, tau_avg);
else
  case_title_prefix = 'w/o self-absorption: ';
  dirname = 'Perturbation_test_no_self_abs';
  get_R = @(model_params) get_R_sim_no_self_absorp(model_params, false, atm_params, jetParams, calibParams, jet_boundary_line, geometry, symAxis, A_array, F_R, tau_avg);
end

symAxis_bkp = symAxis_for_plots;
symAxis_for_plots = 1;

model_params = model_params_orig;
[R_m, T_im, ~] = get_R(model_params);
% [R_m, T_im, ~] = get_R_sim(model_params, false, false, atm_params, jetParams, calibParams, jet_boundary_line, geometry, symAxis, A_array, F_R, F_TAU, tau_avg);

xxx = (size(T_im,1):-50:1)';
yyy = (1:10:size(T_im,2))';
M = numel(xxx);
N = numel(yyy);
zzz = T_im(xxx,yyy) - 273.15;
zzz = zzz';
zzz = zzz(:);

xxx = repmat(xxx,1,N)';
xxx = xxx(:);
yyy = repmat(yyy,M,1);
T_meas_ = [xxx, yyy, zzz, ones(M*N,1)];

mid_title = 'Reconstructed Temperature Relative error [%]';
r_title = 'R Relative error [%]';
desc_title = '';

hF_arr = [];
ax_arr = [];

%% Pertube T0:
model_params = model_params_orig;
X0_pertub_T = X0;
X0_pertub_T(1) = (1+perturb.T0)*X0_pertub_T(1);
case_title = [case_title_prefix,'\DeltaT = ',num2str(floor(100*perturb.T0)), '%'];
model_params = set_model_params(X0_pertub_T);
[R_sim_pertub_T, T_im_pertub_T, ~] = get_R(model_params);
% [R_sim_pertub_T, T_im_pertub_T, ~] = get_R_sim(model_params, false, false, atm_params, jetParams, calibParams, jet_boundary_line, geometry, symAxis, A_array, F_R, F_TAU, tau_avg);
% plot_result(model_params, D_nozzle_pixels, symAxis_for_plots, R_sim_pertub_T, R_m, T_im_pertub_T, T_meas_, weights_im, case_title);
hF = GasHelper.plot_rel_error_half_im(T_im, T_im_pertub_T, weights_im, D_nozzle_pixels, {case_title,mid_title});
hF_arr = [hF_arr, hF];
hF = GasHelper.plot_rel_error_half_im(R_m, R_sim_pertub_T, weights_im, D_nozzle_pixels, {case_title,r_title});
hF_arr = [hF_arr, hF];

%% Pertube C0:
model_params = model_params_orig;
X0_pertub_C = X0;
X0_pertub_C(7) = (1+perturb.C0)*X0_pertub_C(7);
case_title = [case_title_prefix,'\DeltaC = ',num2str(floor(100*perturb.C0)), '%'];
model_params = set_model_params(X0_pertub_C);
[R_sim_pertub_C, T_im_pertub_C, ~] = get_R(model_params);
% [R_sim_pertub_C, T_im_pertub_C, ~] = get_R_sim(model_params, false, false, atm_params, jetParams, calibParams, jet_boundary_line, geometry, symAxis, A_array, F_R, F_TAU, tau_avg);
% plot_result(model_params, D_nozzle_pixels, symAxis_for_plots, R_sim_pertub_C, R_m, T_im_pertub_C, T_meas_, weights_im, case_title);
hF = GasHelper.plot_rel_error_half_im(T_im, T_im_pertub_C, weights_im, D_nozzle_pixels, {case_title,mid_title});
hF_arr = [hF_arr, hF];
hF = GasHelper.plot_rel_error_half_im(R_m, R_sim_pertub_C, weights_im, D_nozzle_pixels, {case_title,r_title});
hF_arr = [hF_arr, hF];

%% Pertube X_PC:
model_params = model_params_orig;
X0_pertub_X_PC = X0;
xpc = size(T_im,1)-X0(2)+1;
dX_PC = round(-perturb.X_PC*xpc);
X0_pertub_X_PC(2) = X0_pertub_X_PC(2) + dX_PC;
case_title = [case_title_prefix,'\DeltaX_P_C = ',num2str(floor(100*perturb.X_PC)), '%'];
model_params = set_model_params(X0_pertub_X_PC);
[R_sim_pertub_X_PC, T_im_pertub_X_PC, ~] = get_R(model_params);
% [R_sim_pertub_X_PC, T_im_pertub_X_PC, ~] = get_R_sim(model_params, false, false, atm_params, jetParams, calibParams, jet_boundary_line, geometry, symAxis, A_array, F_R, F_TAU, tau_avg);
% plot_result(model_params, D_nozzle_pixels, symAxis_for_plots, R_sim_pertub_X_PC, R_m, T_im_pertub_X_PC, T_meas_, weights_im, case_title);
hF = GasHelper.plot_rel_error_half_im(T_im, T_im_pertub_X_PC, weights_im, D_nozzle_pixels, {case_title,mid_title});
hF_arr = [hF_arr, hF];
hF = GasHelper.plot_rel_error_half_im(R_m, R_sim_pertub_X_PC, weights_im, D_nozzle_pixels, {case_title,r_title});
hF_arr = [hF_arr, hF];

%% Pertube Y_PC:
model_params = model_params_orig;
X0_pertub_Y_PC = X0;
X0_pertub_Y_PC(3) = (1+perturb.Y_PC)*X0_pertub_Y_PC(3);
case_title = [case_title_prefix,'\DeltaY_P_C = ',num2str(floor(100*perturb.Y_PC)), '%'];
model_params = set_model_params(X0_pertub_Y_PC);
[R_sim_pertub_Y_PC, T_im_pertub_Y_PC, ~] = get_R(model_params);
% [R_sim_pertub_Y_PC, T_im_pertub_Y_PC, ~] = get_R_sim(model_params, false, false, atm_params, jetParams, calibParams, jet_boundary_line, geometry, symAxis, A_array, F_R, F_TAU, tau_avg);
% plot_result(model_params, D_nozzle_pixels, symAxis_for_plots, R_sim_pertub_Y_PC, R_m, T_im_pertub_Y_PC, T_meas_, weights_im, case_title);
hF = GasHelper.plot_rel_error_half_im(T_im, T_im_pertub_Y_PC, weights_im, D_nozzle_pixels, {case_title,mid_title});
hF_arr = [hF_arr, hF];
hF = GasHelper.plot_rel_error_half_im(R_m, R_sim_pertub_Y_PC, weights_im, D_nozzle_pixels, {case_title,r_title});
hF_arr = [hF_arr, hF];

%% Pertube r_0.5_up:
model_params = model_params_orig;
X0_pertub_r_0_5_up = X0;
X0_pertub_r_0_5_up(end) = (1+perturb.r_0_5_up)*X0_pertub_r_0_5_up(end);
case_title = [case_title_prefix,'\Deltar_0_._5_,_2 = ',num2str(floor(100*perturb.r_0_5_up)), '%'];
model_params = set_model_params(X0_pertub_r_0_5_up);
[R_sim_pertub_r_0_5_up, T_im_pertub_r_0_5_up, ~] = get_R(model_params);
% [R_sim_pertub_r_0_5_up, T_im_pertub_r_0_5_up, ~] = get_R_sim(model_params, false, false, atm_params, jetParams, calibParams, jet_boundary_line, geometry, symAxis, A_array, F_R, F_TAU, tau_avg);
% plot_result(model_params, D_nozzle_pixels, symAxis_for_plots, R_sim_pertub_r_0_5_up, R_m, T_im_pertub_r_0_5_up, T_meas_, weights_im, case_title);
hF = GasHelper.plot_rel_error_half_im(T_im, T_im_pertub_r_0_5_up, weights_im, D_nozzle_pixels, {case_title,mid_title});
hF_arr = [hF_arr, hF];
hF = GasHelper.plot_rel_error_half_im(R_m, R_sim_pertub_r_0_5_up, weights_im, D_nozzle_pixels, {case_title,r_title});
hF_arr = [hF_arr, hF];

%% Pertube r_0.5_down:
model_params = model_params_orig;
X0_pertub_r_0_5_down = X0;
X0_pertub_r_0_5_down(end-1) = (1+perturb.r_0_5_down)*X0_pertub_r_0_5_down(end);
case_title = [case_title_prefix,'\Deltar_0_._5_,_1 = ',num2str(floor(100*perturb.r_0_5_down)), '%'];
model_params = set_model_params(X0_pertub_r_0_5_down);
[R_sim_pertub_r_0_5_down, T_im_pertub_r_0_5_down, ~] = get_R(model_params);
% [R_sim_pertub_r_0_5_down, T_im_pertub_r_0_5_down, ~] = get_R_sim(model_params, false, false, atm_params, jetParams, calibParams, jet_boundary_line, geometry, symAxis, A_array, F_R, F_TAU, tau_avg);
% plot_result(model_params, D_nozzle_pixels, symAxis_for_plots, R_sim_pertub_r_0_5_down, R_m, T_im_pertub_r_0_5_down, T_meas_, weights_im, case_title);
hF = GasHelper.plot_rel_error_half_im(T_im, T_im_pertub_r_0_5_down, weights_im, D_nozzle_pixels, {case_title,mid_title});
hF_arr = [hF_arr, hF];
hF = GasHelper.plot_rel_error_half_im(R_m, R_sim_pertub_r_0_5_down, weights_im, D_nozzle_pixels, {case_title,r_title});
hF_arr = [hF_arr, hF];

%% Pertube b:
model_params = model_params_orig;
X0_pertub_b = X0;
X0_pertub_b(4) = (1+perturb.b)*X0_pertub_b(4);
case_title = [case_title_prefix,'\Deltab = ',num2str(floor(-100*perturb.b)), '%'];
model_params = set_model_params(X0_pertub_b);
[R_sim_pertub_b, T_im_pertub_b, ~] = get_R(model_params);
% [R_sim_pertub_b, T_im_pertub_b, ~] = get_R_sim(model_params, false, false, atm_params, jetParams, calibParams, jet_boundary_line, geometry, symAxis, A_array, F_R, F_TAU, tau_avg);
% plot_result(model_params, D_nozzle_pixels, symAxis_for_plots, R_sim_pertub_b, R_m, T_im_pertub_b, T_meas_, weights_im, case_title);
hF = GasHelper.plot_rel_error_half_im(T_im, T_im_pertub_b, weights_im, D_nozzle_pixels, {case_title,mid_title});
hF_arr = [hF_arr, hF];
hF = GasHelper.plot_rel_error_half_im(R_m, R_sim_pertub_b, weights_im, D_nozzle_pixels, {case_title,r_title});
hF_arr = [hF_arr, hF];

%% Fix same CLIM for all figures:
clim_arr = zeros(numel(hF_arr), 2);
% for iF = 1 : numel(hF_arr)
%   ax = findall(hF_arr(iF), 'type', 'axes');
%   clim_arr(iF, :) = ax.CLim;
%   ax_arr = [ax_arr, ax];
% end
% clim = [min(clim_arr(:,1)),  max(clim_arr(:,2))];
for iF = 1 : numel(hF_arr)
  ax = findall(hF_arr(iF), 'type', 'axes');
  % ax_arr(iF).CLim = clim;
  ax.CLim = clim;
end

symAxis_for_plots = symAxis_bkp;

%% Save all figures:
GasHelper.save_all_figures(parentdir,dirname);