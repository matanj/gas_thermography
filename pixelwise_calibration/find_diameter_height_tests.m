figure;
for i = 510:-4:300
  plot(im(i,:)); hold on
end


figure;
for i = 510:-4:300
  plot(DL_new(i,:)); hold on
end

figure;
for i = 510:-4:300
  plot(R_est_bkp(i,:)); hold on
end




figure;
agy = abs(diff(DL_new, 1, 1));
ss0 = [];
for i = 509:-1:400
  plot(agy(i,:)); hold on
  ss0 = [ss0; nansum(agy(i,:))];
end
figure; plot(ss0); hold on; plot(diff(ss0));


figure;
agy = abs(diff(R_est_bkp, 1, 1));
ss = [];
for i = 509:-1:400
  plot(agy(i,:)); hold on
  ss = [ss; nansum(agy(i,:))];
end
figure; plot(ss); hold on; plot(diff(ss));

figure;
agy = abs(diff(R_est_Abel_Inverted, 1, 1));
ss2 = [];
for i = 509:-1:400
  plot(agy(i,:)); hold on
  ss2 = [ss2; nansum(agy(i,:))];
end
figure; plot(ss2); hold on; plot(diff(ss2));
