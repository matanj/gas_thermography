function test_Radon()
dbstop if error
% Assuming perfectly symmetric distribution.
% For the Gaussian here, check effect of the 'sigma' parameter, combined
% with the image's size.
% This might give hint on minimal jet width for proper back projection.
clear;  close all; clc;
% P = phantom ();
M = 321;
N = M; % TODO: not working for N~=M...
d_theta_deg = 5; % no apparent improvement below 5 deg
% center = [floor(M/2), floor(N/2)];
center = floor(([M N] + 1)/2); % to comply with radon() definition
sigma = 20;
mat = zeros(M,N);
P = gauss2d(mat, sigma, center);
figure, imshow (P, []), title ('Original image'); colorbar; impixelinfo;

projections = radon (P, 0);
projections = repmat(projections, 1, floor(180/d_theta_deg)); % just for the inverse purpose. no new data introduced.
reconstruction = iradon (projections, 0:d_theta_deg:180-d_theta_deg, 'Spline', 'Hann');

figure, imshow (reconstruction, []), title ('Reconstructed image'); colorbar; impixelinfo;
% if M & N even - use reconstruction(2:end-1,2:end-1) below.
% if M & N odd  - use reconstruction(1:end-1,1:end-1) below.
figure, imshow (abs(reconstruction(2-mod(M,2):end-1, 2-mod(N,2):end-1) - P), []), title ('difference image'); colorbar; impixelinfo;
end

function val = gaussC(x, y, sigma, center)
xc = center(1);
yc = center(2);
amp = 1;%1/(2*sqrt(2*pi));
exponent = ((x-xc).^2 + (y-yc).^2)./(2*sigma^2);
val = amp * exp(-exponent);   
end

function mat = gauss2d(mat, sigma, center)
gsize = size(mat);
[R,C] = ndgrid(1:gsize(1), 1:gsize(2));
mat = gaussC(R,C, sigma, center);
end