function [fitresult, gof] = createFit_TC_meas_17_11_23()
%CREATEFIT(X,Y,Z,WEIGHTS)
%  Create a fit.
T_meas = load(fullfile(pwd,'T_meas_17_11_23.mat'));
T_meas = T_meas.T_meas;
D = 120; % nozzle diameter in pixels
symAxis = 215; % symmetry axis location in pixels
N_ROWS = 512;

x = N_ROWS - T_meas(:,1);
y = abs(T_meas(:,2) - symAxis) + 1;
z = T_meas(:,3) + 273.15;
weights = ones(size(x));
for iMeasure = 1 : size(T_meas,1)
  weights(iMeasure) = 1 / (sqrt( x(iMeasure)^2 + y(iMeasure)^2 ) + 1);
end
weights = weights / sum(weights);
tol = 1e-1;

%% Fit: 'untitled fit 1'.
[xData, yData, zData, weights_1] = prepareSurfaceData( x, y, z, weights );

% Set up fittype and options.
ft = fittype( '((((T0-298)./(max(x,Xpc)-b)*(Xpc-b)+298)-298).*exp(-log(2)*((y-(Ypc*(1-x/Xpc)))./(m1*x+HWHM)).^2)+298) .* (y>(Ypc*(1-x/Xpc)))     +   ((T0-298)./(max(x,Xpc)-b)*(Xpc-b)+298) .* (y<=(Ypc*(1-x/Xpc)))', 'independent', {'x', 'y'}, 'dependent', 'z' );
opts = fitoptions( 'Method', 'NonlinearLeastSquares' );
opts.Display = 'Off';
opts.Lower = [5 400 50 10 -3000 0];
opts.MaxFunEvals = 3000;
opts.MaxIter = 1500;
opts.StartPoint = [20 490 100 40 -1000 0.1];
opts.Upper = [70 550 350 70 0 0.5];
opts.Weights = weights_1;

% Fit model to data.
[fitresult, gof] = fit( [xData, yData], zData, ft, opts );

% Create a figure for the plots.
figure( 'Name', 'Reference measurements fit' );

%Modify axes labels:
x_places = uniquetol(xData, tol);
x_lables = round(x_places/D, 2);
y_places = uniquetol(yData, tol);
y_lables = round(y_places/D, 2);

% Plot fit with data.
subplot( 2, 1, 1 );
h = plot( fitresult, [xData, yData], zData, 'Style','predfunc' );
legend( h, 'T model fit', 'T vs. x, y', 'Location', 'NorthEast' );
% Label axes
xticks(x_places);
xticklabels(x_lables);
yticks(y_places);
yticklabels(y_lables);
xlabel x/D
ylabel y/D
zlabel T[K]
grid on

% Plot residuals.
subplot( 2, 1, 2 );
h = plot( fitresult, [xData, yData], zData, 'Style', 'Residual' );
legend( h, 'T model fit - residuals', 'Location', 'NorthEast' );
% Label axes
xticks(x_places);
xticklabels(x_lables);
yticks(y_places);
yticklabels(y_lables);
xlabel x/D
ylabel y/D
zlabel error[K]
grid on

%% Plot along centerline:
figure;
xcl = xData(1:4:end);
ycl = yData(1:4:end);
zcl = zData(1:4:end);
zcl_fit = fitresult(xcl, ycl);
h = plot(xcl, zcl ,'o'); hold on;
plot( xcl, zcl_fit, '*' );
title('T model fit @ centerline');
legend('measurements','fit' ,'Location', 'NorthEast' );
% Label axes
xticks(x_places);
xticklabels(x_lables);
xlabel x/D
ylabel T[K]
