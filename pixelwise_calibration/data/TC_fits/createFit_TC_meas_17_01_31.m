function [fitresult, gof] = createFit_TC_meas_17_01_31()
%CREATEFIT(X,Y,Z,WEIGHTS)
%  Create a fit.
T_meas = load(fullfile(pwd,'T_meas_17_01_31_T387C.mat'));
T_meas = T_meas.T_meas;
D = 80; % nozzle diameter in pixels
symAxis = 183; % symmetry axis location in pixels
N_ROWS = 384;

x = N_ROWS - T_meas(:,1);
y = abs(T_meas(:,2) - symAxis) + 1;
z = T_meas(:,3) + 273.15;
weights = ones(size(x));
for iMeasure = 1 : size(T_meas,1)
  weights(iMeasure) = 1 / (sqrt( x(iMeasure)^2 + y(iMeasure)^2 ) + 1);
end
weights = weights / sum(weights);

tol = 1e-1;
%% Fit: 'untitled fit 1'.
[xData, yData, zData, weights_1] = prepareSurfaceData( x, y, z, weights );

% Set up fittype and options.
ft = fittype( '((((T0-292)./(max(x,Xpc)-b)*(Xpc-b)+292)-292).*exp(-log(2)*((y-(Ypc*(1-x/Xpc)))./(m1*x+HWHM)).^2)+292) .* (y>(Ypc*(1-x/Xpc)))     +   ((T0-292)./(max(x,Xpc)-b)*(Xpc-b)+292) .* (y<=(Ypc*(1-x/Xpc)))', 'independent', {'x', 'y'}, 'dependent', 'z' );
opts = fitoptions( 'Method', 'NonlinearLeastSquares' );
opts.Display = 'Off';
opts.Lower = [5 550 50 10 -3000 0];
opts.MaxFunEvals = 3000;
opts.MaxIter = 1500;
opts.StartPoint = [40 600 100 40 -1000 0.1];
opts.Upper = [50 750 300 45 0 0.5];
opts.Weights = weights_1;

% Fit model to data.
[fitresult, gof] = fit( [xData, yData], zData, ft, opts );

% Create a figure for the plots.
figure( 'Name', 'untitled fit 1' );

%Modify axes labels:
x_places = uniquetol(xData, tol);
x_lables = round(x_places/D, 2);
y_places = uniquetol(yData, tol);
y_lables = round(y_places/D, 2);

% Plot fit with data.
subplot( 2, 1, 1 );
h = plot( fitresult, [xData, yData], zData );
legend( h, 'fit', 'z vs. x, y with weights', 'Location', 'NorthEast' );
% Label axes
xticks(x_places);
xticklabels(x_lables);
yticks(y_places);
yticklabels(y_lables);
xlabel x/D
ylabel y/D
zlabel T[K]
grid on
% Plot residuals.
subplot( 2, 1, 2 );
h = plot( fitresult, [xData, yData], zData, 'Style', 'Residual' );
legend( h, 'fit - residuals', 'Location', 'NorthEast' );
% Label axes
xticks(x_places);
xticklabels(x_lables);
yticks(y_places);
yticklabels(y_lables);
xlabel x/D
ylabel y/D
zlabel error[K]
grid on
