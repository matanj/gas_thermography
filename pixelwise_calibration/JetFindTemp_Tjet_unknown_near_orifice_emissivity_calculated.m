%% Use per-pixel Calibration matrix to retrieve the Jet temperature
%%
% *Synopsis*
%%
%% THIS test differ from 'JetFindTempUsingCalibrationMatrix' in that when checking the calibration on the test case,
%% Its allows radiation estimation from MultiITtoRP(), instead of just DL/IT.

function JetFindTemp_Tjet_unknown_near_orifice_emissivity_calculated
clc; %close all;
dbstop if error
warning off
format short
%% Assumptions: 1) Order experiment of BGs->IMs -> BGs -> ...
%%              2) unique experiment cases.
%%              3) each experiment case hase same #files.
%%              4) All images are of same size (row, cols)
%%              5) New images to be used with the caibration should match its image's size AND PIXEL ALLIGNMENT!

%%              6) The measured Camera's R is independant of solid angle of the object:
%%                  R ~ double_integral(Radiance*dOmega*dA), where here the solid angle is of the detector pixel and Camera's FOV, and the detector's area.

LOAD_WORKSPACE_OF_EXPERIMENT = true; % loads a saved experiment and spares all the tedious choosing. if not exists, it saves one for future use

if ~LOAD_WORKSPACE_OF_EXPERIMENT || ~(exist(['SNAPSHOT_',mfilename,'.mat'], 'file') == 2) % if there isn't a snapshot, else jump to analysis

%% OPTIONS
DEBUG = false; % show figures;
CALC_CALIB_PARAMS = false; % if false, use existing set for testing
REPRESENTATIVE_R_CALIB = true; % if true - representative Rs will be used to obtain 2/3 calibration params. otherwise, 2/3 parameters per pixel.
NUM_CALIB_PARAMS_2_OR_3 = 2; % 2 or 3 parameters for calibration: either R~~ Kw*exp(A0/T) or Kw*exp(A0/T+A1/T^2)
e_BB = 0.97; % emissivity of the Black Body used for calibration. assumed constant spatial-wise and spectrum-wise.
FILTER_PARAMETERS_TH = 0; % percentile-based filtering over calculated fit parameters. '0' for filtering by std
FILTER_PARAMETERS_STD = 0;% std-based filtering over calculated fit parameters. '0' for no filtering
APPLY_ATM_MODEL = true; % apply HITRAN model of atmospheric transmittance when retreiving measurement's temperature
APPLY_ATM_MODEL_IN_CALIBRATION = true;
ALL_PIXELS_SEE_BB = true; % for calibrating only a subset of the image
FIX_JET_ANGLE = false; % fix jet's alignment to be vertical
HITRAN_CALC_RADIANCE = false; % 'true' - calc radiance from HITRAN. 'false' - calc emissivity.
NOZZLE_IN_IMAGE = true; % if the images contain the nozzle, select it and get m2pixel, etc.
USE_BIAS_FRAME = false;
FILTER_THE_RADIATION = true; % filter estimated R with a gaussian LPF.
USE_CFD = true; % for the jet model
REFINE_CALIB_PARAMS = false; % set mean value of "good" pixels to the "Bad" pixels that show absord temperature.
bSize = 2; % Block size [pixels]
nLevels = 6; % otsu's number of levels for label the jet image, for radius estimation
iterative_Convergence_tol = 1e-6; % tolerance for convergance of the iterative process [deg C]
N_iters = 200; % Maximal number of iterations for the iterative process.

%% EXPERIMENT RELATED:
atm_OPL = 1.5; % Atmospheric path in [m]
tot_mfr = 2; %[g/s]
CO2_mfr = 2; %[g/s]
D_nozzle = 1e-2; % Nozzle's diameter [m]
atm_co2_concentration = 1000; % [ppmv]
atm_T = 29; % Atmospheric temperature [C]
pressure = 1; %[atm]

%CO2 filter related:
filterParams.filter_CWL = 4370; % CO2 filter CWL [nm]
filterParams.filter_FWHM = 20; % CO2 filter FWHM [nm]
filterParams.th_l = 0.1;
filterParams.th_h = 0.9;
%%
atm_OPL = atm_OPL*100; % to [cm]
atm_co2_concentration = atm_co2_concentration*1e-6; % ppmv to fraction
D_nozzle_pixels = 60;%[pixels]
m2pixel = D_nozzle_pixels / D_nozzle; % meter to pixel conversion

CalibFilePath = [pwd,'\pixelwise_calibration\JetBBCalibrationSaveR_OUTPUT.mat']; % default
outputName = 'CalibrationParams.mat';
biasFramePath = 'C:\Users\matanj\Documents\MATLAB\BiasFrameComputed.mat'; % default Bias Frame path
%% Default test case and related files:
baseDir = 'Z:\FLIR_Experimental\10-07-16_Jet_\';
imName = [baseDir, 'G001_T500_F4370_IT2693.6_IM_f.ptw'];
% bgName = [baseDir, 'G002_T500_F4370_IT2693.6_BG_f.ptw'];
atm_trans_calib_FileName = 'C:\Users\matanj\Documents\PythonScripts\Atmospheric_Transmittance_T27C_C630ppm_L150cm_RH51.mat'; % path used in the calibration measurements
%% Constants:
h = 6.62606957e-34; %[J*s]
c = 299792458; %[m/s]
Kb = 1.3806488e-23; %[J/K]
Na = 6.02e23; % Avogadro's number
Rgas = 0.08206; % Gas constant, [L*atm*mol^-1*K^-1]
% C2 = h*c/Kb;
% C1 = 2*h*c^2;
C2K = @(x) x + 273.15; % Celsius to Kelvin
K2C = @(x) x - 273.15; % Kelvin to Celsius

addpath(genpath('Z:\Matan\'), '-end');

if CALC_CALIB_PARAMS % conditionally calculate calibration parameters:
  %% Get data: {DL, DL_normalized, experimentCasesArr}
  [CalibFileName, CalibFilePath] = uigetfile({'*.mat','MATLAB data'},'Select Calibration matrix',CalibFilePath);
  if isempty(CalibFileName)
    error('invalid folder path, or claibration matrix there...');
  end
  load([CalibFilePath, CalibFileName]);
  
    %Rotrou 2006: NIR thermography with silicon FPA, [17]:
  % For each pixel we find the following 3 (or 4) params:
  %   Kw, a0, a1, (a2), by this fit:
  %  ln(DL_normalized) = ln(Kw) - C2./T/.lambda_x =~  ln(Kw) - C2.*(a0 + a1.*T + ...)
  % log(Kw)-C2*(a0+a1.*X+a2.*X.^2) yields good results..
  %%
  % Problems:
  % 1) Need to use Plate BB for this calibration - all pixels should see the
  %     same radiation
  % 2) No radiation calibration - it is for DL...
  % 3) Model seems Weinish (https://en.wikipedia.org/wiki/Sakuma-Hattori_equation, 9th form)
  %     but my range might not hold Wein's assumption!!!!!!!!!!!!!!!!!
  
  %% estimate R from DL = R*IT^P model
  T = C2K(experimentCasesArr.temps); % T[K] used in the calibration
  %{
  % DL format:  DL(temps, ITs, rows, cols)
  ITs = squeeze(experimentCasesArr.IT); % (temps, ITs)
  if REPRESENTATIVE_R_CALIB
  end
  % preallocation:
  R_est = nan(size(DL,1), size(DL,3), size(DL,4)); % (temps, rows, cols)
  for indT = 1:numel(experimentCasesArr.temps) % Loop over temperatures
    DLs = permute(squeeze(DL(indT,:,:,:)), [2,3,1]); % MultiITtoRP expects: DL(row,cols,ITs)
    [R_est(indT,:,:),~] = MultiITtoRP(double(squeeze(ITs(indT,:))), DLs);
  end
  clear DLs ITs;
  %}
  if REPRESENTATIVE_R_CALIB
    progressbar('Temp') 
    R_est = nan(size(T));
    for indT = 1:numel(T) % Loop over temperatures
      R_est(indT) = HistogramHelper.getFittedGauss3Peak(squeeze(R(indT,1,:,:)),[],[],[],3); % Get representative R from the R image
      progressbar(indT/numel(T));
    end
  else
    R_est = squeeze(R(:,1,:,:));
  end
  
  X = max(T)./T; % the independant variable. scaled to ease estimation.
  
  [~, nX, nY] = size(R_est);
  
  % Prealloc: parameter matrices
  Kw = nan(nX, nY);
  a0 = nan(nX, nY);
  if NUM_CALIB_PARAMS_2_OR_3 == 3
    a1 = nan(nX, nY);
  end
  
  %%
  % Formulate as a M*P = Y system , where P is the desired parameters vector.
  % We have: ln(DL) =  ln(Kw)-C2*(a0/T + a1/T^2)
  % Denote Y = ln(DL), X = 1/T. We write:
  % Y = ln(Kw) - a0*X + a1*X.^2.
  % Denote: p3 = log(Kw), p2 = -C2*a0, p1 = -C2*a1,  we get:
  % (*) Y = p3 + p2*X + p1*X^2 ======>    Y = [X.^2 X ones(size(X))]*P
  % =====> P  =  M \ Y
  
  % Using (*), we can afterward solve for T :
  % X1,2 = (-p2 +- sqrt(p2^2 - 4*p1*(p3-Y)))/2/p1
  % ===> T = 1/X = 2*p1 ./ (-p2 +- sqrt(p2^2 - 4*p1*(p3-ln(DL_n))))
  
  % And if the object is of known "extended effective emmisivity" (my term),
  % we have: ln(DL) =  ln(Kw) - ln(e) -C2*(a0/T + a1/T^2)
  % that is, (**) Y = p3 - ln(e) + p2*X + p1*X^2
  % solving we get: X1,2 = (-p2 +- sqrt(p2^2 - 4*p1*(p3-ln(e)-Y)))/2/p1
  
  Y = log(R_est) - log(e_BB)*ones(size(R_est)); % measurement - log of DL normalized by IT
  
  if APPLY_ATM_MODEL_IN_CALIBRATION % Apply atmospheric transmittance:
    [AtmFileName, AtmFilePath] = uigetfile({'*.mat','MATLAB data'},'Select atm transmittance matrix',atm_trans_calib_FileName);
    atm_trans_FileName = [AtmFilePath,AtmFileName];
    if isempty(atm_trans_FileName)
      error('invalid path, or transmittance matrix there...');
    end
    trans_eff = getEffectiveTransmittance(filterParams, atm_trans_FileName);
    Y = Y - log(trans_eff)*ones(size(R_est));
  end
  
  if REPRESENTATIVE_R_CALIB % find 2/3 parameters for the whole image
    if NUM_CALIB_PARAMS_2_OR_3 == 3
      M = [X.^2 X ones(size(X))];
      P = M \ Y;
      a1 = P(1);
      a0 = P(2);
      Kw = P(3);
      Kw = Kw*ones(size(R,3), size(R,4));
      a0 = a0*ones(size(R,3), size(R,4));
      a1 = a1*ones(size(R,3), size(R,4));
    else
      M = [X ones(size(X))];
      P = M \ Y;
      a0 = P(1);
      Kw = P(2);
      Kw = Kw*ones(size(R,3), size(R,4));
      a0 = a0*ones(size(R,3), size(R,4));
    end
  else % find 2/3 parameters for each Pixel
    for i = 1: nX
      for j = 1 : nY
        if ~any(isnan(R_est(:,i,j)))
          if NUM_CALIB_PARAMS_2_OR_3 == 3
            P = [X.^2 X ones(size(X))] \ Y(:,i,j); % M is probably ill conditioned!!
            %{
        %%%%% Verifying the solution with several methods %%%%%
        M = [X.^2 X ones(size(X))];
        P2 = inv(M'*M)*M'*Y(:,i,j); % using pseudo-inverse
        [U,S,V] = svd(M,0);
        P3 = V*( (U'*Y(:,i,j))./diag(S) ); %using SVD
        if norm(P2-P,inf) > 1e-11 || norm(P3-P,inf) > 1e-11
          error('Mismatch!!!!');
        end
            %}
            Kw(i,j) = P(3);
            a0(i,j) = P(2);
            a1(i,j) = P(1);
          else
            P = [X ones(size(X))] \ Y(:,i,j); % M is probably ill conditioned!!
            Kw(i,j) = P(2);
            a0(i,j) = P(1);
          end
        end
      end
      progressbar(i/nX, j/nY);
    end
    progressbar(i/nX, 0);
  end
  
  % Save coefficients per pixel:
  if NUM_CALIB_PARAMS_2_OR_3 == 3
    save(outputName, 'Kw', 'a0','a1', 'T');
  else
    save(outputName, 'Kw', 'a0', 'T');
  end

else
  [ParamsFileName, ParamsFilePath] = uigetfile({'*.mat','MATLAB data'},'Select Calibration parameters set',[pwd,'\CalibrationParams.mat']);
  if isempty(ParamsFileName)
    error('invalid folder path...');
  end
  load([ParamsFilePath, ParamsFileName]);
end

%% Filter results by percentile, or by STD:
if FILTER_PARAMETERS_TH > 0 % by percentile
  Kw = filterPrctile(Kw, FILTER_PARAMETERS_TH);
  a0 = filterPrctile(a0, FILTER_PARAMETERS_TH);
  if NUM_CALIB_PARAMS_2_OR_3 == 3
    a1 = filterPrctile(a1, FILTER_PARAMETERS_TH);
  end
elseif FILTER_PARAMETERS_STD > 0 % by std
  Kw = filterStds(Kw, FILTER_PARAMETERS_STD);
  a0 = filterStds(a0, FILTER_PARAMETERS_STD);
  if NUM_CALIB_PARAMS_2_OR_3 == 3
    a1 = filterStds(a1, FILTER_PARAMETERS_STD);  
  end
end
totNaNMask = isnan(Kw) & isnan(a0);
if NUM_CALIB_PARAMS_2_OR_3 == 3
   totNaNMask = totNaNMask & isnan(a1);
end
Kw(totNaNMask) = nan;
a0(totNaNMask) = nan;
if NUM_CALIB_PARAMS_2_OR_3 == 3
  a1(totNaNMask) = nan;
end
if DEBUG
  figure;
  subplot(2,3,1);  imagesc(Kw); colorbar; title('Kw');
  subplot(2,3,2); imagesc(a0); colorbar; title('a0');
  subplot(2,3,4); imagesc(~isnan(Kw)); colorbar; title('"Valid" Kw');
  subplot(2,3,5); imagesc(~isnan(a0)); colorbar; title('"Valid" a0');
  if NUM_CALIB_PARAMS_2_OR_3 == 3
    subplot(2,3,3); imagesc(a1); colorbar; title('a1');
    subplot(2,3,6); imagesc(~isnan(a1)); colorbar; title('"Valid" a1');
  end
  figure; imagesc(totNaNMask); colorbar; title('total mask');
  figure;
  subplot(1,3,1);  histogram(Kw,500); title('Kw');
  subplot(1,3,2);  histogram(a0,500); title('a0');
  if NUM_CALIB_PARAMS_2_OR_3 == 3
    subplot(1,3,3);  histogram(a1,500); title('a1');
  end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% %%%%%%%%%%%%%%%%% Test the calibration: %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  %Get test case:
  [imName, baseDir] = uigetfile({'*.ptw','FLIR images'},'Select image to test calibration',imName);
  if isempty(imName)
    error('invalid image');
  end
  %GET the Background file that matches the Image file's name:
  bgName = getBGfileFromIMfile(imName, baseDir);

  %Get the temperatures of the measurement from file's name:
  matchStr = regexp(imName,'_T+[0-9]+[0-9]+[0-9]','match');
  tempStr = regexprep(matchStr{1},'_T','');
  Tref = str2double(tempStr);
  
  imPair = ImageBGPairExt([baseDir,imName]);
  imPair.setBG([baseDir,bgName{1}]);
  imInfo = imPair.IMG_Info;
  %% Get the Bias frame:
%   [biasFrameFull, biasFrame] = cropBiasFrame2PTWsize(biasFramePath, imInfo);
  %% crop the suitable subframe form it and rotate accordingly:
  if USE_BIAS_FRAME
    [ biasSubframe ] = suitBiasSubframeFromBiasFrame( imPair.getImage(), imInfo, biasFramePath );
  end
  
  subIm = mean(abs(imPair.getSubtracted()), 3);
  if ALL_PIXELS_SEE_BB
    objMask = true(size(subIm));
  else
    [~, object] = processImGetStats(subIm);%get meaningful object within the image
    objMask = true(imPair.IMG_Info.m_rows, imPair.IMG_Info.m_cols);%initial mask
    objMask(isnan(object)) = NaN;
  end
  
  %mask the image to maintain same size:
  DL_new = objMask.*subIm;
  % Normalize DLs map by IT:
  IT = double(imPair.IT)*1e6; %change IT to [us]
  DL_new(DL_new==0) = 1; % set minimal DL to 1 to avoid zero issues
  DL_new_n = DL_new ./ IT;
  
  %trim image to orifice and get pipe's diameter:
  if NOZZLE_IN_IMAGE
    figure(111); imagesc(subIm); colorbar; title('Select nozzle''s orifice & press a key to cntinue');
    maximizeFigure();% maximize figure for convienience
    h_nozzle = imdistline();
    pause;
    apiObj = iptgetapi(h_nozzle);
    teta = apiObj.getAngleFromHorizontal() %[deg]
    D_nozzle_pixels = ceil(apiObj.getDistance());%[pixels]
    m2pixel = D_nozzle_pixels / D_nozzle;
    pos_nozzle = apiObj.getPosition();
    DL_new_n = DL_new_n(1:floor(pos_nozzle(1,2)), :);
    close 111;
  end
  
  if FIX_JET_ANGLE
    %% Apply 2D affine rotating transformation to fix for the angle of the Jet.
    % Radon transformation is used estimate angle.
    %% TODO: should apply same to the calibration maps, or rotate back b4 using the calibration
    % NOTE: I trim the new image to the original size. 'theta' is assumed
    % small enough so that the skew is negligable.
%     DL_new_n_otsu = otsu(DL_new_n,10); % apply otsu thersholding
%     theta = 90 - DetectAngle(DL_new_n_otsu);
    S = 1; % scale factor
    theta =  90 - DetectAngle(DL_new_n); % '90' as desired jet is vertical
    tform = affine2d([S.*cosd(theta) -S.*sind(theta) 0; S.*sind(theta) S.*cosd(theta) 0; 0 0 1]);
    fixed_DL_new_n = imwarp(DL_new_n,tform,'cubic','fillvalues',0); %default is '0'. 
    %{
    % DEBUG: checking that x' = A^-1(A(x)) = x : 
    tform2 = affine2d([S.*cosd(-theta) -S.*sind(-theta) 0; S.*sind(-theta) S.*cosd(-theta) 0; 0 0 1]);
    DL_new_n_rec = imwarp(fixed_DL_new_n,tform2,'cubic','fillvalues',0);
    DL_new_n_rec = DL_new_n_rec(DL_new_n_rec ~= 0);
    DL_new_n_rec = reshape(DL_new_n_rec, size(fixed_DL_new_n, 1), []);
    %}
    if DEBUG
      figure; imagesc(DL_new_n); colorbar; title('original');
%       figure; imagesc(fixed_DL_new_n_otsu); colorbar; title('rotation: angle detected on otsu');
      figure; imagesc(fixed_DL_new_n); colorbar; title('rotation: angle detected on original');
    end
    fixed_DL_new_n(fixed_DL_new_n==0) = nan;% mask the rotation transformation by-product
    mask = ~isnan(fixed_DL_new_n);
    % get the inner image w/o transformation's by-products:
    % need to distinguish between angles signs since the inner rectangle
    % usually intersects each of the outer one's limits by more that one
    % pixel.
    if theta < 0 % rotate CW
      idx_row1 = find(mask(:,1),1,'last');
      idx_row2 = find(mask(:,end),1,'first');
      idx_col1 = find(mask(1,:),1,'first');
      idx_col2 = find(mask(end,:),1,'last');
    else % rotated CCW
      idx_row1 = find(mask(:,1),1,'first');
      idx_row2 = find(mask(:,end),1,'last');
      idx_col1 = find(mask(1,:),1,'last');
      idx_col2 = find(mask(end,:),1,'first');
    end
    min_row = min(idx_row1, idx_row2);
    max_row = max(idx_row1, idx_row2);
    min_col = min(idx_col1, idx_col2);
    max_col = max(idx_col1, idx_col2);
    DL_new_n = fixed_DL_new_n(min_row:max_row, min_col:max_col);
  end
  
  %% Apply calibration with Kw, a0, a1 maps.
  % First, crop object image to the calibration image, if needed:
  if ~(exist('rect','var') ) % if cropping wasn't done:
    [rowsCalib, colsCalib] = size(Kw);
    [rowsNew, colsNew] = size(DL_new_n);
    if rowsNew ~= rowsCalib || colsNew ~= colsCalib
      rows = 1:floor(pos_nozzle(1,2)); % TODO: what if NOZZLE_IN_IMAGE == false?
      cols = 1:size(DL_new_n,2);
    else
      rows = 1:size(DL_new_n,1);
      cols = 1:size(DL_new_n,2);
    end
  else % if 'rect' is present:
    rows = rect(2):rect(2)+rect(4);
    cols = rect(1):rect(1)+rect(3);
  end
  % TODO: bug when image is > calibration image. if representative
  % calibration parameters are chosen - no problem
  if REPRESENTATIVE_R_CALIB % TODO: else, check size and exit if above holds.
    Kw = Kw(1,1)*ones(size(DL_new));
    a0 = a0(1,1)*ones(size(DL_new));
    Kw = Kw(rows, cols); 
    a0 = a0(rows, cols);
    if NUM_CALIB_PARAMS_2_OR_3 == 3
      a1 = a1(1,1)*ones(size(DL_new));
      a1 = a1(rows, cols);
    end
  end
  totNaNMask = isnan(Kw) | isnan(a0);
  if NUM_CALIB_PARAMS_2_OR_3 == 3
    totNaNMask = totNaNMask | isnan(a1);
  end
  DL_new_n(totNaNMask) = nan;
  
  %% Get the HITRAN-calculated emission/emissivity matrix
  if HITRAN_CALC_RADIANCE
    meanEmissionMatrixFilePath = 'C:\Users\matanj\Documents\PythonScripts\';
    [meanEmissionMatrixFileName, meanEmissionMatrixFilePath] = uigetfile({'*.mat','MATLAB data'},'Select mean Emission Matrix file',meanEmissionMatrixFilePath);
    if isempty(meanEmissionMatrixFileName)
      error('invalid folder path, or matrix there...');
    end
    load([meanEmissionMatrixFilePath,meanEmissionMatrixFileName]);
    numT = size(meanEmission, 1); % take #temperatures from the loaded matrix
  else % Calc emissivity
    meanAbsorpMatrixFilePath = 'C:\Users\matanj\Documents\PythonScripts\meanAbsorpMatrix_540K_1_1cm.mat';
    [meanAbsorpMatrixFileName, meanAbsorpMatrixFilePath] = uigetfile({'*.mat','MATLAB data'},'Select mean Absorp Matrix file',meanAbsorpMatrixFilePath);
    if isempty(meanAbsorpMatrixFileName)
      error('invalid folder path, or matrix there...');
    end
    load([meanAbsorpMatrixFilePath,meanAbsorpMatrixFileName]);
    numT = size(meanAbsorp, 1);
  end

  %% estimate the measurement's radiation from DL = R*IT^P
  % first, get R from multiIT measurements:
  totalFilesList = dir([baseDir,'/*.ptw']);
  if isempty(totalFilesList)
    error('invalid folder path, or no .ptw files there...');
  end
  totalFilesList = {totalFilesList(:).name}';
  [props.G,props.T,props.CWL,props.IT,props.isBG,props.name] = parseFilename(totalFilesList); %divide list to params
  [caseList,~,~] = getMatchingCase(totalFilesList, props, {'T'}, {str2double(tempStr)});
  [IMList,~,~] = getMatchingCase(caseList, props, {'isBG'}, {false});
  [BGList,~,~] = getMatchingCase(caseList, props, {'isBG'}, {true});
  
  % prealloc:
  DLs_IM = zeros(size(DL_new_n,1), size(DL_new_n,2), numel(BGList));
  DLs_BG = zeros(size(DL_new_n,1), size(DL_new_n,2), numel(BGList));
  DLs = zeros(size(DL_new_n,1), size(DL_new_n,2), numel(BGList)); % TODO: remove
  ITs = zeros(numel(BGList), 1);
  for indIMPair = 1:numel(BGList)
    imPair = ImageBGPairExt([baseDir,IMList{indIMPair}]);
    imPair.setBG([baseDir,BGList{indIMPair}]);
    im = imPair.getImage();
    bg = imPair.getBG();
    %subtract the bias frame from each frame:
    if USE_BIAS_FRAME
      DLs_IM(:,:,indIMPair) = im(rows,cols) - biasSubframe(rows,cols);
      DLs_BG(:,:,indIMPair) = bg(rows,cols) - biasSubframe(rows,cols);
    else
      subIm = abs(imPair.getSubtracted());% mean(abs(imPair.getSubtracted()), 3); % TODO: remove
      DLs(:,:,indIMPair) = subIm(rows,cols);
    end
    ITs(indIMPair) = double(imPair.IT)*1e6; %change IT to [us]
  end
  
  if USE_BIAS_FRAME
    % TODO: Need to extend the capability of
    % 'suitBiasSubframeFromBiasFrame' to take the correct crop when the
    % image was cropped arbitrarily.
    % (Bad example: Z:\FLIR_Experimental\16-07-24_CloseJet\G101_T500_F4370_IT2530.2_IM_JetDistanceM.ptw)
    [R_est_im,~] = MultiITtoRP(ITs, DLs_IM);
    [R_est_bg,~] = MultiITtoRP(ITs, DLs_BG);
    R_est = R_est_im - R_est_bg;
  else
    [R_est,~] = MultiITtoRP(ITs, DLs); % For now this is the favored one as the bias frame process inject noise
  end
  assert( all(R_est(:) >= 0) || all(R_est(:) <=0) ); % mixed signs are not allowed!
  R_est = abs(R_est); % fix for possible confusion of IM and BG
  %     figure; imagesc(R_est); colorbar;
  if FILTER_THE_RADIATION
    %     R_est_filt = filter2(1/3^2*ones(3), R_est);
    R_est_filt = imgaussfilt(R_est, 1.5); % apply Gaussian LPF. should preserve edges.
    R_est = R_est_filt;
  end
  assert(all(R_est(:)),'invalid R...');
 
  %% apply HITRAN transmittance model for the atmosheric path of the measurement
  if APPLY_ATM_MODEL
    [AtmFileName, AtmFilePath] = uigetfile({'*.mat','MATLAB data'},'Select atm transmittance matrix of the experiment:',atm_trans_calib_FileName);
    atm_trans_FileName = [AtmFilePath,AtmFileName];
    if isempty(atm_trans_FileName)
      error('invalid path, or transmittance matrix there...');
    end
    trans_eff = getEffectiveTransmittance(filterParams, atm_trans_FileName);
  else
    trans_eff = 1;
  end
  trans_eff = trans_eff*ones(size(R_est)); % assuming constant in all of the volume
  

  if 0
    %% Apply Abel inversion over R_est
    % Abel inversion code parameters
    AbelParams.alpha = 0.1;
    %% Calc the symmetry axis
    %% TODO: R_est has NaN here!!!
    fig = figure; imagesc(R_est); impixelinfo; title('Abel Inversion: Select area, then confirm by double clicking');
    maximizeFigure(); % maximize figure for convienience
    [lines, rect] = imcrop;
    %round coordinates, as they are fractions:
    rect(1:2) = ceil(rect(1:2));
    rect(3:4) = floor(rect(3:4));
    [M,~] = size(lines);
    [~,Idx] = max(lines,[],2);
    rows = (rect(2):rect(2)+rect(4))';
    cols = (rect(1):rect(1)+rect(3))';
    colsMax = rect(1)+Idx-1;
    % Find the axis of symmetry from a fit of the maxs to a line:
    % TODO: Maybe better to find the symmetry axis on the raw DL image
    x = (1:length(colsMax))';
    coeffs = polyfit(x, colsMax, 1); % expecting the 'x' coeff to be sub-pixel (<1) and the intersect to be axis of symmetry
    avgCol = round(coeffs(2));
    % Just for plot purpose:
    imAxisMarked = R_est;
    imAxisMarked(rows,avgCol) = min(R_est(:));
    figure(fig); imagesc(imAxisMarked);impixelinfo;
    
    new_lines = cell(M,1);
    jetOtsu = otsu(R_est(rows,:), nLevels);
    % Apply Abel inversion per row
    for l=1:M % loop on each row and perform abel inversion
      %       line = lines(l,:);
      line = R_est(rows(l),:);
      IndLast = find(jetOtsu(l,:) > 1, 1, 'last');
      %       line = line(avgCol:end);
      line = line(avgCol:IndLast);
      Radii = length(line) /m2pixel*1e2; % estimated Radius in [cm]
      F_in = line;
      %% Among FE, ASAI & NO methods - NO seems the most accurate.
      F_in = F_in - min(F_in); % hopefully the minim
      f_rec_NO = NestorOlsenAbelInversion(F_in, Radii);
      %{
      f_rec_FE = abel_inversion(F_in, Radii, 10, 0, 0, 0);
      y=(0.0001:1:numel(F_in))*(Radii/numel(F_in));
      r = y;
      knots = 0.:.01*Radii:Radii;
      f_rec_ASAI = ASAI(r, F_in, y, knots);
      %}
      % Normalizing the distribution according to the centerline:
      % twice its integral should equal the measurement in the center.
      f_rec_NO = f_rec_NO *F_in(1) /(2*trapz(f_rec_NO));
      %       f_rec_FE = f_rec_FE *F_in(1) /(2*trapz(f_rec_FE));
      %       f_rec_ASAI = f_rec_ASAI *F_in(1) /(2*trapz(f_rec_ASAI));
      new_lines{l} = [flipud(f_rec_NO); f_rec_NO];
    end
    %   R_est = new_lines; % set the new image after abel inversion
    sizes = cellfun(@size, new_lines, num2cell(ones(M,1))); % always even by built
    R_est = nan(M,max(sizes));% set the new image after abel inversion
    for indRow = 1 : M
      R_est(indRow, 1+(max(sizes)-sizes(indRow))/2 : end-(max(sizes)-sizes(indRow))/2 ) = new_lines{indRow};
    end
    %   figure; imagesc(R_est); colorbar; title('R image after Abel Inversion');
    
    
    %% take the ROI from the data:
    cols =  (avgCol - max(sizes)/2: avgCol+max(sizes)/2-1)';
    a0 = a0(rows,cols);
    Kw = Kw(rows,cols);
    if NUM_CALIB_PARAMS_2_OR_3 == 3
      a1 = a1(rows,cols);
    end
    trans_eff = trans_eff(rows,cols);
    R_est = R_est_filt(rows,cols); % TODO: this patch negates the inverse abel.
  end

  OPL_step = D_nozzle/D_nozzle_pixels*1e2; %in [cm] for HITRAN
  % approximation of the solid angle of a cell in the jet:
  solid_angle = (OPL_step/(atm_OPL))^2; % TODO: fix to actual
  detector_pitch_um = 15; % [um]
  detector_area_cm2 = (detector_pitch_um*1e-4)^2; % Detector's area in [cm^2]
  
  %% For the interpolation later:
  calcTemps = linspace(C2K(atm_T), C2K(600), numT);
  calcConcenrations = linspace(atm_co2_concentration, CO2_mfr/tot_mfr, 10);
  [cT, cC] = ndgrid(calcTemps,calcConcenrations);
  if HITRAN_CALC_RADIANCE
    F = griddedInterpolant(cT,cC,meanEmission);
  else
    F = griddedInterpolant(cT,cC,meanAbsorp);
  end
  
  %% Parameters of model of temperature & concentration profiles in the jet:
  params.useCFD = USE_CFD;
  params.D = D_nozzle;
  params.tot_mfr = tot_mfr; % Total mass flow rate, [g/s]
  params.co2_mfr = CO2_mfr; % CO2 mass flow rate, [g/s]
  params.Tamb_C = atm_T; % ambient temperature, [C]
  params.Camb = atm_co2_concentration; % CO2 concentration in the ambient
  params.P0 = 101325; %[Pa]. Pressure @ outlet is 1 atm = 101325[Pa]
  [params.CFDpath, ~] = uigetfile({'*.xlsx','ANSYS csv files'},'Select FreeJet CFD file', 'Z:\Matan\Jet CFD\FreeJet.xlsx');
%   [params.CFDpath, ~] = uigetfile({'*.xlsx','ANSYS csv files'},'Select FreeJet CFD file', 'C:\Users\matanj\Documents\MATLAB\pixelwise_calibration\FreeJet.xlsx');% TODO: return to above
  
  [~,idxMax] = max(DL_new_n(end,:));
  
  assert(~isequal(params.CFDpath,0));
  [~,txt,data]  = xlsread(params.CFDpath);
  data = data(size(txt,1)+1:end, :); % remove leading information rows
  data = [data(:,2), data(:,3), data(:,5)];% ColB = X, ColC = Y, ColE = Tot_temperature
  data = cell2mat(data);
  params.data = data;
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  %% Iterate over guessed tempmeratures
  err_norm_vec = nan(N_iters, 1);
  T_guesses = nan(N_iters,1);
  %Initial guess on temperature
  calcTemps_arr = calcTemps; % initialization
  LU_temp_ind = floor(numel(calcTemps)/2); % initial guess
  LU_temp_ind = 2; % TODO: problems when starting from 1st and last indices...
  params.Tjet_C = K2C(calcTemps_arr(LU_temp_ind)); %Jet's outlet temperature, [C]
  
  
  % save SNAPSHOT workspace if needed:
  if LOAD_WORKSPACE_OF_EXPERIMENT 
    SaveCallerWorkspace(''); % enter description in string if needed
  end
  return;
end
%%%%%%%%%%%%%%%%%%%%%%%%
% load the SNAPSHOT:%%%%
%%%%%%%%%%%%%%%%%%%%%%%%
load(['SNAPSHOT_',mfilename,'.mat']);

  for k = 1 : N_iters
    T_guesses(k) = params.Tjet_C;
    if mod(k,5) == 1
      disp(['iteration ',num2str(k),': T_guess = ',num2str(params.Tjet_C),'[C]']);
    end
    %% Apply model of temperature & concentration profiles in the jet:
    %% TODO: this model produces buggy output for small temperatures! (30C)
    %% TODO 2: this can be scaled and shoved out of the loop...
    [C_model, T_model, ~] = GenerateTemperatureConcentrationModelsFit2image(...
                    size(DL_new_n), m2pixel, size(DL_new_n,1), idxMax, params);
    %% Divide to blocks the apparent plane (XY):
    % currently I have same mean value in a block.
    % prepare a pixel-by-pixel emissivity map:
    e_map = ones(size(DL_new_n));
    %chop image to match block size:
    imSize = size(T_model);
    T_model = T_model(1:bSize*floor(imSize(1)/bSize), 1:bSize*floor(imSize(2)/bSize));
    C_model = C_model(1:bSize*floor(imSize(1)/bSize), 1:bSize*floor(imSize(2)/bSize));
    %devide to blocks
    T_blocks = divideIm2Blocks(T_model, bSize);
    C_blocks = divideIm2Blocks(C_model, bSize);
    % set in each block the mean value of it:
    setMeanVal = @(block) nanmean(block(:))*ones(size(block));
    avgTBlocks = cellfun(setMeanVal, T_blocks,'UniformOutput', false);
    avgCBlocks = cellfun(setMeanVal, C_blocks,'UniformOutput', false);
    %Build reconstructed images from the filtered blocks:
    avgTBlocks = cell2mat(avgTBlocks);
    avgCBlocks = cell2mat(avgCBlocks);
    if DEBUG
      figure; imagesc(avgTBlocks); colorbar; title('avg T model');
      figure; imagesc(avgCBlocks); colorbar; title('avg C model');
    end
    %% Find emission using HITRAN, in each block:
    % TODO: save the calculated triplets in the script, and its inputs should
    % be the limits of calculation.
    
%     meanEmission = getRadianceFromHITRAN_HAPI(params.Tjet_C, CO2_mfr/tot_mfr, OPL_step,'C:\Users\matanj\Documents\PythonScripts\getEmissionConstOPL.py');

    % Perform 2d interpolation of the HITRAN data:
    %below, 'unique' is replaced by 'uniquetol' to reduce the memory size in
    %cases of small block size:
    uniquetolFlag = false;
    tolUniquetol = 1e-4;
    Temps = unique(avgTBlocks);
    if numel(Temps) > 1e4 % indication to possible memory problem
      uniquetolFlag = true;
      Temps = uniquetol(avgTBlocks,tolUniquetol,'DataScale', nanmax(avgTBlocks(:))); % tolerance is scaled by max(avgTBlocks)
      Concenrations = uniquetol(avgCBlocks,tolUniquetol,'DataScale', nanmax(avgCBlocks(:)));
    else
      Concenrations = unique(avgCBlocks);
    end
    Temps = Temps(Temps>0); % also get rid of NaNs
    Concenrations = Concenrations(Concenrations>0);% also get rid of NaNs
    % Use 'griddedInterpolant' object 'F' for interpolation:
    [Tq, Cq] = ndgrid(Temps,Concenrations); % replaced meshgrid
    meanEmissionInterp = F(Tq,Cq);
    disp(['block size = ',num2str(bSize),', meanEmissionInterp size = ',num2str(size(meanEmissionInterp))]);
    % apply interpolation to get the emissivity map:
    for i = 1:size(avgTBlocks,1)
      for j = 1:size(avgTBlocks,2)
        if avgTBlocks(i,j) == 0 || isnan(avgTBlocks(i,j))
          e_map(i,j) = nan;
        else
          if uniquetolFlag
             e_map(i,j) = meanEmissionInterp( find(abs(Temps-avgTBlocks(i,j))==min(abs(Temps-avgTBlocks(i,j)))), find(abs(Concenrations-avgCBlocks(i,j))==min(abs(Concenrations-avgCBlocks(i,j)))) );
          else
            e_map(i,j) = meanEmissionInterp( Temps==avgTBlocks(i,j), Concenrations==avgCBlocks(i,j) );
          end
        end
      end
    end
    e_map(e_map==1) = nan;
    e_eff = e_map;
%     e_eff = e_eff(rows,cols);
    
    %% Scale the HITRAN radiance to the units of R:
    %{
    %Definitions: number density = nx, mixing ratio = Cx.
    % nx = Cx * na  (here na is the number density of air)
    % na = Na*N/V   (Na - Avogadro's number, N - number of moles of air)
    % For atmoshepric air: P*V = N*R*T  (R = 8.31 [J*mol^-1*K^-1])
    % ===> nx = Na*P*Cx/(R*T)
    % (Px = P*Cx is the partial pressure)

    N_CO2_molecules = Na * pressure * (OPL_step*1e-2)^3 / Rgas ./ T_model; % n = Na * PV/RT % TODO: incorrect!!
    N_CO2_molecules = N_CO2_molecules(rows,cols);
    N_CO2_molecules =  N_CO2_molecules(:, round(end/2)+1:end);
    %}
    
    
    %{
    %% THE following is in [MKS]:
    %% h = 6.626e-34 [J*s],     c = 2.9979245e8 [m/s],      k = 1.380658e-23 [J/K]
    %% Plank's law in wavenumber units: Lv = 2e8hc^2v^3*1/(exp(100hcv/kT)-1) [W/m^2/sr/cm^-1]. (Spectral radiance)
    %% Convert Plack's law from W/area/sr/cm^-1 to #photons/s/area/sr/cm^-1 (Spectral photon radiance):
    %%    ==> Lp = Lv/photon_energy = Lv/100hcv = 2e6*c*v^2/(exp(100hcv/kT)-1) [photon/s/m^2/sr/cm^-1]
    %% NOTE: the Stefan Boltzmann law for photon radiant emittance dictates T^3 relation rather than the well-known T^4!!!
    
    %% HITRAN gives spectral radiance in [W/sr/cm^2/cm^-1], so need to
    %% divide by photon's energy. 
    %% Two options: 1) Incorporate this during spectral radiance
                        calculation (currently I integrate in the python script)
                    2) divide by the average wavenumber, assuming the band
                        is narrow. 
    %}
    if HITRAN_CALC_RADIANCE
        e_eff = e_eff(:, round(end/2)+1:end); % length is assumed even
%        e_eff = e_eff*solid_angle*detector_area_cm2;
        e_eff = e_eff/1e2/h/c/(1e7/filterParams.filter_CWL); % converting filter_CWL in [nm] to [cm^-1]
        e_eff = e_eff*solid_angle*detector_area_cm2; %  TODO: verify if indeed no need to multiply by the solid angle according to assumption (6)?
        % TODO: also, night need to divide by pi if the jet is assumed diffusive
        e_eff = e_eff/pi; % assuming jet is diffusive source
    end
    % For the distribution on the axis, no need for ABEL inverese:
    tmp_measurement = R_est(end-1, round(end/2)+1)./D_nozzle_pixels; % Assuming uniform distribution in the potential core
%     tmp_measurement = R_est(end-1, round(end/2)+1);

    % Apply calibration (T2R):
%     tmp = e_eff(end-1, round(end/2)+1);
    if NUM_CALIB_PARAMS_2_OR_3 == 3
      R_calc = T2R(C2K(T_guesses(k)), T, 1, 1, Kw, a0, a1);
    else
      R_calc = T2R(C2K(T_guesses(k)), T, 1, 1, Kw, a0);
    end
    tmp = R_calc./e_eff./trans_eff;
    tmp = tmp(end-1, round(end/2)+1);
    
    
    %% perform forward-abel transform on the modeled radiance to mimic the sensor's operation:
    %{
    FA = zeros(size(e_eff));
    for indRow = 1 : size(e_eff,1)
      FA(indRow,:) = forwardAbel(e_eff(indRow,:), size(e_eff,2) ); % TODO!!!!! change to actual radius
    end
    e_eff = [fliplr(FA),FA];
    
    mask = isnan(e_eff) | isnan(R_est); % union of nans
    e_eff(mask) = nan;
    R_est(mask) = nan;
    %}
    %% compare the model to the measurement:
    %{
    figure; imagesc(e_eff); title(['Model''s Emission map @ iteration ',num2str(k)]); colorbar;
    figure; imagesc(R_est); title('Abel inverted measurement''s radiation'); colorbar;
    figure; imagesc(mask); title(['nan mask @ iteration ', num2str(k)]);
    figure; imagesc(FA); title(['Forward Abel of models''s Emission map @ iteration ',num2str(k)]); colorbar;
    %}    
    %% Check convergence and update guess:
%     err = e_eff(~isnan(e_eff)) - R_est(~isnan(R_est));
    err = tmp - tmp_measurement;
    err_norm_vec(k) = norm(err);
%     if 0
    if (norm(err) < iterative_Convergence_tol) || (numel(calcTemps_arr) == 1)
      break;
%     elseif norm(e_eff(~isnan(e_eff))) > norm(R_est(~isnan(R_est))) % search in the lower temperatures
    elseif LU_temp_ind==1 % we've rach 2 elemets array
      calcTemps_arr = calcTemps_arr(1);
    elseif norm(tmp) > norm(tmp_measurement) % search in the lower temperatures
      calcTemps_arr = calcTemps_arr(1:LU_temp_ind-1); % update search range
    else
      calcTemps_arr = calcTemps_arr(LU_temp_ind+1:end); % update search range
    end
    LU_temp_ind = round(numel(calcTemps_arr)/2); % update new guess index
%     end
%     LU_temp_ind = LU_temp_ind + 1;
    params.Tjet_C = K2C(calcTemps_arr(LU_temp_ind)); %New guess for Jet outlet temperature[C]

  end % end of iterative porcess
  
  figure; plot(err_norm_vec); title('||err|| Vs iterations');
  figure; plot(T_guesses); title('Tjet guesses [C] Vs iterations');
  figure; plot(T_guesses, err_norm_vec); title('||err|| Vs Guessed Tjet'); xlabel('Tjet[C]');
  
  %% Apply calibration (R2T):
  %% solve the quadratic equation Y = p3+ln(e_eff)+ln(trans_eff) + p2*X + p1*X^2, for X = 1/T & Y = ln(R_est)
  if NUM_CALIB_PARAMS_2_OR_3 == 3
    T_rec_C = R2T_rec(R_est, T, e_eff, trans_eff, Kw, a0, a1);
  else
    T_rec_C = R2T_rec(R_est, T, e_eff, trans_eff, Kw, a0);
  end

  figure; imagesc(T_rec_C); colorbar; title('Reconstructed T[^oC] using blocks of mean emissivities');
  figure; surf(T_rec_C,'edgecolor','none'); view(181,15); title('Reconstructed T[^oC] using blocks of mean emissivities');
  
  %% Another iteration with refined calibration parameters, using OTSU th.
  if REFINE_CALIB_PARAMS
    % at this point, temperature histogram is assumed to have two well distinced areas.
    if DEBUG
      figure; histogram(T_rec_C,1000);
    end
    good_idx = otsu(T_rec_C); % output: image of 3 lvls: 0(nans), 1 ,2
    good_idx = good_idx > 1;
    Kw_good = Kw(good_idx);
    a0_good = a0(good_idx);
    % Set mean value of "good" pixels to the "bad" ones:
    Kw(~good_idx) = nanmean(Kw_good(:));
    a0(~good_idx) = nanmean(a0_good(:));
    if NUM_CALIB_PARAMS_2_OR_3 == 3
      a1_good = a1(good_idx);
      a1(~good_idx) = nanmean(a1_good(:));
      T_rec_C = R2T_rec(R_est, T, e_eff, trans_eff, Kw, a0, a1);
    else
      T_rec_C = R2T_rec(R_est, T, e_eff, trans_eff, Kw, a0);
    end
    
    figure; imagesc(T_rec_C); colorbar; title('Reconstructed T[^oC] after 2nd iteration of updating "bad" pixels');
  end


end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Auxiliary functions:
function T_rec_C = R2T_rec(R_est, calib_T, e_eff, trans_eff, Kw, a0, a1)
% gets R_est, calib_T[K], effective emissivity, effected transmisivity, and
% calibraion params (either 2 or 3) and returns reconstructed temperatrure[C].
  if nargin < 6
    error('invalid args!');
  end
  K2C = @(temp) temp - 273.15;
  tol = 1e-10;
  if nargin == 7 % 3 parameters calibration:
    % solves the quadratic equation Y = p3+ln(e_eff)+ln(trans_eff) + p2*X + p1*X^2, for X = 1/T & Y = ln(R_est)
    discriminent = a0.^2 - 4*a1.*( Kw+log(e_eff)+log(trans_eff)-log(R_est) );
    x1 = (-a0 + sqrt(discriminent) )./2./a1;
    x2 = (-a0 - sqrt(discriminent) )./2./a1;
    assert(all(x1(~isnan(x1)))>tol && all(x2(~isnan(x2)))>tol && isreal(x1) && isreal(x2));
    T_rec = nan(size(R_est));
    % Get temperature map: take only T>0:
    T_rec(x1>0) = 1./x1(x1>0);
    T_rec(x1<0) = 1./x2(x1<0);
  else % 2 parameters calibration:
    % solves the linear equation Y = p3+ln(e_eff)+ln(trans_eff)+p2*X, for X = 1/T & Y = ln(R_est)
    x = (log(R_est)-log(e_eff)-log(trans_eff)-Kw ) ./ a0;
    assert(all(x(~isnan(x)))>tol && isreal(x));
    T_rec = 1./x;
  end
  T_rec = T_rec * max(calib_T); % since we have normalized by max(T) to improve the fit process
  T_rec_C = K2C(T_rec);
end

function R = T2R(T, calib_T, e_eff, trans_eff, Kw, a0, a1)
% gets T[K], calib_T[K], effective emissivity, effected transmisivity, and
% calibraion params (either 2 or 3) and returns R in camera units.
  if nargin < 6
    error('invalid args!');
  end
  X = max(calib_T(:))./T;% since we have normalized by max(T) to improve the fit process
  if nargin == 7 % 3 parameters calibration:
    % solves the quadratic equation Y = p3+ln(e_eff)+ln(trans_eff) + p2*X + p1*X^2, for X = 1/T & Y = ln(R_est)
    R = e_eff.*trans_eff.*Kw.*exp(a0.*X+a1.*X.^2);
  else % 2 parameters calibration:
    % solves the linear equation Y = p3+ln(e_eff)+ln(trans_eff)+p2*X, for X = 1/T & Y = ln(R_est)
    R = e_eff.*trans_eff.*Kw.*exp(a0.*X);
  end
  assert( all(R(~isnan(R))>0) && isreal(R) );
end

function [trans_eff] = getEffectiveTransmittance(filterParams, atmTransmittanceFileName)
  % get an "effective" transmittance in the atmosphere:
  load(atmTransmittanceFileName);
  numHWHM = 2;
%   filter_CWN = 1e7/filterParams.filter_CWL; %to [cm^-1]
  filter_HWHM = filterParams.filter_FWHM/2;
  filter_wn_l = 1e7/(filterParams.filter_CWL + numHWHM*filter_HWHM);
  filter_wn_h = 1e7/(filterParams.filter_CWL - numHWHM*filter_HWHM);
  wn_ROI_ind = wn >= filter_wn_l & wn <= filter_wn_h;
  wn_ROI = wn(wn_ROI_ind);
  % Crude approximation for the filter's response as a triange. TODO: replace by better
  th_l = filterParams.th_l; th_h = filterParams.th_h;
  filter_response = double(wn_ROI_ind);
  fr = [linspace(th_l,th_h,ceil(numel(wn_ROI)/2)), linspace(th_h,th_l,ceil(numel(wn_ROI)/2))];
  filter_response(filter_response>0) = fr;
%   filter_response = filter_response(1:numel(wn_ROI));
%   trans_eff = mean(transmittance(wn_ROI_ind).*filter_response);
  trans_eff = mean(transmittance(wn_ROI_ind).*filter_response(wn_ROI_ind));
end

function y = filterPrctile(x, p, val)
% Assigns 'val' to places where x is not in the percentile range 'p'. if
% 'val' not stated, 'NaN' is assigned by default.
  if nargin <2 || p<0 || p > 100
    error('invalid args!')
  end
  if nargin < 3
    val = nan;
  end
  x( x < prctile(x(:), p) | x > prctile(x(:), 100-p) ) = val;
  y = x;
end

function y = filterStds(x, nStds, val)
% Assigns 'val' to places where x is not in the range of nStds from its mean. if
% 'val' not stated, 'NaN' is assigned by default.
  if nargin <1
    error('invalid args!')
  end
  if nargin < 2
    nStds = 2;
    val = nan;
  end
  if nargin < 3
    val = nan;
  end
x( abs(x-nanmean(x(:))) > nStds*nanstd(x(:)) ) = val;
y = x;
end

function subBlocks = divideIm2Blocks(I, bSize)
% divide image to bSize*bSize blocks
  if nargin < 2
    error('invalid args');
  end
  s = size(I);
  nB = ceil(s/bSize);
  rowsVec = repmat(bSize, [nB(1) 1]);
  colsVec = repmat(bSize, [nB(2) 1]);
  subBlocks = mat2cell(I, rowsVec, colsVec);
end

%{
function rotatedIm = nanimwrap(im,tform)
% apply 2D transformation on an image with nans. 
% this is WRONG: due to inevitable interpolation in the rotation
  if nargin < 2
    error('invalid args!');
  end
  minVal = nanmin(im(:));
  maxVal = nanmax(im(:));
  tempVal = minVal - abs(1/(maxVal-minVal));
  im(isnan(im)) = tempVal;
  rotatedIm = imwarp(im, tform);
  rotatedIm(rotatedIm==tempVal) = nan;
end
%}

function meanRadiance = getRadianceFromHITRAN_HAPI(T, C, l, script_path)
  % Get mean (spectral) radiance HITRAN, by running a python script
  % based on HAPI
  if nargin < 3
    error('invalid args!');
  end
  if nargin<4
    scriptPath = 'C:\Users\matanj\Documents\PythonScripts\getEmissionConstOPL.py';
  else
    scriptPath = script_path;
  end
  %build command string:
  argString = [num2str(T),' ',num2str(C),' ',num2str(l)];
  commandStr = ['C:\Users\matanj\AppData\Local\Continuum\Anaconda3\python.exe ',...
                scriptPath, ' ', argString];
  [status, commandOut] = system(commandStr);
  if status == 0 % if succeeded:
    [script_folder, ~, ~] = fileparts(script_path);
    meanEmissionMatrixFileName = ['meanEmissionMatrix_',num2str(T),'C_','C',num2str(C),'_',num2str(l),'cm.mat'];
    load([script_folder,'\',meanEmissionMatrixFileName]);
    meanRadiance = meanEmission;
  else
    error('failed!');
  end
end

function [stats, object] = processImGetStats(im, hAx, calcStats)
%Function to process an image and extract the object and optionally its stats
%'im' - image
%'hAx' - handle to an axis to modify.
%'calcStats' - whether to output statistics in 'stats' or not.
    stats = [];
    lvl = graythresh(mat2gray(im)); binaryImage = im2bw(mat2gray(im), lvl); % alternative
    binaryImage = imfill(binaryImage, 'holes');
    labeledImage = bwlabel(binaryImage, 8);
    
    blobMeasurements = regionprops(logical(labeledImage), 'all'); % Get all the blob properties
    numberOfBlobs = size(blobMeasurements, 1);
    assert(numberOfBlobs == 1); % allow one blob
    
    object = im .* binaryImage;
    object(~object) = NaN;
      
    if nargin > 1 %need to calc stats and change given axis plot
        DLInterval = 10; % for histogram bin size
        axes(hAx); hold on;
        boundaries = bwboundaries(binaryImage);
        for k = 1 : numberOfBlobs % preferably just one...
          thisBoundary = boundaries{k};
          plot(thisBoundary(:,2), thisBoundary(:,1), 'r', 'LineWidth', 1.5);
          blobCentroid = blobMeasurements(k).Centroid;	% Get centroid
          plot(blobCentroid(1),blobCentroid(2),'r+', 'MarkerSize', 20);
        end
        hold off;
        % Calc Stats:
        if calcStats
          maxDL = nanmax(object(:));
          minDL = nanmin(object(:));
          nBins =  int16((maxDL-minDL)/DLInterval);
          [hist,edges] = histcounts(object,nBins);
          [~,I] = nanmax(hist(:));
          stats.Mean = nanmean(object(:));
          stats.MCDL = edges(I+1);
          stats.Max = maxDL;
        end
    end
end

function [caseList, I, hPropsListStructFiltered] = getMatchingCase(hTotFileList, hPropsListStruct, caseParamsNames, caseParamVals)
% Query list of files according to properties. 
% Allows reqursive queries by this scheme:
% [caseList1, I1, hPropsListStructFiltered] = getMatchingCase(hTotFileList, hPropsListStruct, caseParamsNames, caseParamVals)
% [caseList2, I2, ~] = getMatchingCase(caseList1, hPropsListStructFiltered, caseParamsNames, caseParamVals)

%INPUT: 'hTotFileList' - Total list of files of the experiment. cell array.
%       'hPropsListStruct' - struct array holding experiment props per
%           file. may be > than actual number mentioned in the filenames' format. 
%       'caseParamsNames' - parameters names to query. cell array.
%       'caseParamVals' - associated values. cell array.
%OUTPUT: 'caseList' - String cell array of filenames that match the input query.
%        'I' - indices array of corresponding files inside input list
%              'hTotFileList'
%        'hPropsListStructFiltered' - to allow recuring 

    caseList = {}; I = [];
    if nargin < 4
      return;
    end
    I = 1:numel(hTotFileList);
    for indCaseParams = 1:numel(caseParamsNames)
      %The following can handle cell array as well as numeric values: 
      subI = find(ismember(hPropsListStruct.(caseParamsNames{indCaseParams}), caseParamVals{indCaseParams} ));    
      I = intersect(subI, I);
    end
    caseList = {hTotFileList(I)};
    caseList = caseList{1};
    hPropsListStructFiltered = structfun(@(x) x(I), hPropsListStruct, 'UniformOutput', false);
end

function bgName = getBGfileFromIMfile(imName, baseDir)
  [propsIm.G,propsIm.T,propsIm.CWL,propsIm.IT,propsIm.isBG,~] = parseFilename(imName);
  totalFilesList = dir([baseDir,'/*.ptw']);
  if isempty(totalFilesList)
    error('invalid folder path, or no .ptw files there...');
  end
  totalFilesList = {totalFilesList(:).name}';
  [props.G,props.T,props.CWL,props.IT,props.isBG,props.name] = parseFilename(totalFilesList); %divide list to params
  [bgName,~,~] = getMatchingCase(totalFilesList, props,...
        {'T','CWL','IT','isBG'}, {propsIm.T,propsIm.CWL,propsIm.IT,true});
end

function maximizeFigure(hFig)
% maximize current figure
  if nargin > 0
    set(hFig,'units','normalized','outerposition',[0 0 1 1]);
  else
    set(gcf,'units','normalized','outerposition',[0 0 1 1]);
  end
end
