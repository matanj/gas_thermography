function test_integral_calibration_new()
%% Tests effect of filter OD.
%% Calibration parameter K in the integral calibration is either calculated once for initial OD guess, or per OD guess.
%% OD is found as a minimizer of ||errors|| of the images.
clear; %close all
C2K = @(x) x + 273.15; % Celsius to Kelvin

%% TODODODODOD: Alternative: Optimal OD should be such that K(OD,R) = K(OD) (that is, independant of R)
%% TOOO: run per pixel and average all OPTIMAL ODs

IS_CAVITY_BB = true; % 'true' for Cavity BB, 'false' for Plate BB
pixel = [32,80]; % calculate for a given pixel. if empty - for all.
IS_CONSTANT_K = true; % whether to calculate calibration parameters K(OD) or just once K(OD0)
NUM_OD = 50; % number of OD guesses to check
MIN_OD = 1;
MAX_OD = 5;
OD0 = 1.4; % Initial guess of OD

%% Calibration related:
Jet_T_range_K = C2K([130, 380]); % Jet's temperature range (since calibration range is typically wider)
apparatus_tf_path = 'apparatus_tfs.mat'; % default apparatus tranfer functions

if IS_CAVITY_BB % Cavity BB calibration parameters
  CalibFilePath = 'C:\Users\matanj\Documents\MATLAB\pixelwise_calibration\CavityBBCalibrationSaveR_OUTPUT.mat';%Cavity BB experiment from 16-12-29
  calib_experiment_params.geometryStruct.geometryFactor = 4.525289256198347e-14; % For the 16-12-29 Cavity calibration.
  calib_experiment_params.e_BB = 0.97;
  calib_atm_params.OPL = 220; % Calibration Atmospheric path in[cm]
  calib_atm_params.co2_concentration = 700e-6; % Calibration Atmospheric CO2 concentration [fraction]
  calib_atm_params.T = C2K(24.5); % Calibration Atmospheric temperature [K]
  calib_atm_params.RH = 38; % [%]
else % Plate BB calibration parameters
  CalibFilePath = 'C:\Users\matanj\Documents\MATLAB\pixelwise_calibration\JetBBCalibrationSaveR_OUTPUT.mat';
  calib_experiment_params.geometryStruct.geometryFactor = 4.481415842872007e-14;
  calib_experiment_params.e_BB = 0.95;
  calib_atm_params.OPL = 155;
  calib_atm_params.co2_concentration = 600-6;
  calib_atm_params.T = C2K(24.5);
  calib_atm_params.RH = 42;
end
calib_atm_params.pressure_tot = 1; %[atm]
calib_experiment_params.detector_pitch_um = 15;
calib_experiment_params.atmParams = calib_atm_params;
%% For HITRAN spectral calculations:
hitranParams.wn_step = 0.01; % HITRAN's wavenumber step [cm^-1]
hitranParams.min_wn = 2000; % HITRAN's minimal wavenumber [cm^-1]
hitranParams.max_wn = 3333; % HITRAN's maximal wavenumber [cm^-1]
hitranParams.HW_to_eval = 500; % Number of HWHM for Voigt evaluation. Orignally 6000 by Ilya. 500 also good enough.
hitranParams.hitranFileFullPath = 'C:\Users\matanj\Documents\MATLAB\pixelwise_calibration\data\hitranCO2_H2O_2000_3333_main_isotopologs.par';
%%%%%
%% Get Calibration test cases:
S = load(CalibFilePath);
R = S.R;
R = squeeze(R); %Dimensions = [T,rows,cols]
experimentCasesArr = S.experimentCasesArr;
T_K_arr = C2K(experimentCasesArr.temps);
% calc nan mask of the calibration cases:
nan_mask_R = false(size(R,2), size(R,3));
nan_mask_R(30:110,30:100) = true; % for the Cavity BB
% nan_mask_R(100:290,70:260) = true; % for Plate BB
nan_mask = nan_mask_R; % TODO: remove
% for indT = 1 : numel(T_K_arr)
%   nan_mask_R = nan_mask_R | isnan(squeeze(R(indT,:,:)));
% end
plot_str = '||\DeltaR||';
if ~isempty(pixel)
  R = squeeze(R(:,pixel(1),pixel(1)));
  nan_mask = 1;
  plot_str = ['\DeltaR of pixel [',num2str(pixel),']'];
else
  GasHelper.plotIm(squeeze(R(end,:,:)).*nan_mask);
end
%% calculate atmosheric path transmittance for the calibration cases:
[wn_vect, ~, tau_spectra_wn] = MyGetSpectrumGPU(true, 'tau', hitranParams, calib_atm_params);
%% Get Planck in [photons/s/cm^2/sr/cm^-1]:
L_BB_photons_wn = zeros(numel(T_K_arr), numel(wn_vect));
for indT = 1 : numel(T_K_arr)
L_BB_photons_wn(indT,:) = calib_experiment_params.e_BB * 2*100*GasHelper.c .* (wn_vect.^2) ./ (exp(100*GasHelper.C2*wn_vect/T_K_arr(indT)) -1);
end
%% Get calibration parameter for a starting guess of OD
[wl_vect, optics_cfits, ~] = GasHelper.get_optics_tf(apparatus_tf_path, hitranParams, false);
hitranParams.tau_spectra_wn = tau_spectra_wn; % save additional recomputations assuming same tau
orig_filter_tf = optics_cfits.Filter(wl_vect);
orig_OD_guess = log10( max(orig_filter_tf) / mean(orig_filter_tf(orig_filter_tf<=0.1)) ); % Considering tails of <0.1% transmission
orig_OD_guess_old = orig_OD_guess; % patch
orig_OD_guess = OD0; % for starting guess OD>=3, OD converges to 1.8344.
optics_tf_wl = GasHelper.modify_optics_tf_by_OD(wl_vect, optics_cfits, orig_OD_guess);
calib_experiment_params.filter_OD = orig_OD_guess;
[calibParams] = GasHelper.getSpectralCalibrationFromR(CalibFilePath, wl_vect, hitranParams, calib_experiment_params, optics_tf_wl, false);
if ~isempty(pixel)
  calibParams.K = calibParams.K(pixel(1), pixel(2));
end
% calibParams.K = 0.0103 * ones(size(nan_mask)); % TODO: remove?

geometry = calibParams.geometryStruct.geometryFactor; % Assume for all test cases same geometry
orig_OD_guess = orig_OD_guess_old; % restore
%% Iterate over OD:
OD_guess_arr = linspace(MIN_OD, MAX_OD, NUM_OD);
if isempty(pixel)
  R_simulated = zeros(numel(OD_guess_arr), numel(T_K_arr), size(R,2), size(R,3));
else
  R_simulated = zeros(numel(OD_guess_arr), numel(T_K_arr));
end
norm_2_error_R   = zeros(numel(OD_guess_arr), numel(T_K_arr));
norm_inf_error_R = zeros(numel(OD_guess_arr), numel(T_K_arr));
% nan_mask = ~nan_mask_sim & ~nan_mask_R; % total nan mask %TODO: uncomment
for indOD = 1:numel(OD_guess_arr) % Loop over OD
  OD_guess = OD_guess_arr(indOD);
  calib_experiment_params.filter_OD = OD_guess;
  %update filter tf with new OD:
  optics_tf_wl = GasHelper.modify_optics_tf_by_OD(wl_vect, optics_cfits, OD_guess);
  calibParams.optics_tf = optics_tf_wl;
  if ~IS_CONSTANT_K
    [calibParams] = GasHelper.getSpectralCalibrationFromR(CalibFilePath, wl_vect, hitranParams, calib_experiment_params, optics_tf_wl, false);
    if ~isempty(pixel)
      calibParams.K = calibParams.K(pixel(1), pixel(2));
    end
  end
  for indT = 1 : numel(T_K_arr) % Loop over BB images
    R_simulated(indOD,indT,:,:) = GasHelper.getRfromSpectralCalibration(calibParams, geometry, wn_vect, L_BB_photons_wn(indT,:)', tau_spectra_wn);
    R_sim_temp = squeeze(R_simulated(indOD,indT,:,:));
    R_test = squeeze(R(indT,:,:));
    diff_R = abs( R_sim_temp(nan_mask) - R_test(nan_mask) );
    norm_2_error_R(indOD,indT)   = norm( diff_R./R_test(nan_mask) );
    norm_inf_error_R(indOD,indT) = norm( diff_R./R_test(nan_mask), Inf );
  end
%   % Normalize the errors per OD:
%   norm_2_error_R(indOD,:) = norm_2_error_R(indOD,:) / max(norm_2_error_R(indOD,:));
%   norm_inf_error_R(indOD,:) = norm_inf_error_R(indOD,:) / max(norm_inf_error_R(indOD,:));
end
%% Plot images of errors(OD,T):
figure;
imagesc(T_K_arr, OD_guess_arr, norm_2_error_R); colorbar; impixelinfo
xlabel('T[K]'); ylabel('OD');
if isempty(pixel)
  title([plot_str,'_2']);
  figure;
  imagesc(T_K_arr, OD_guess_arr, norm_inf_error_R); colorbar; impixelinfo
  title([plot_str,'_\infty']);
  xlabel('T[K]'); ylabel('OD');
else
  title(plot_str);
end
%% find the "Optimal" OD by finding the minimal norm2 of norm(error)(T) of each OD:
norm2_of_norm2_R = zeros(size(OD_guess_arr));
% find optimal OD only over the Jet temeprature range:
actual_T_range = find(T_K_arr >= Jet_T_range_K(1) & T_K_arr<= Jet_T_range_K(2)) ;
for indOD = 1 : numel(OD_guess_arr)
  norm2_of_norm2_R(indOD) = norm( norm_2_error_R(indOD,actual_T_range) );
end
[norm_optimal,ind_OD_optimal] = min(norm2_of_norm2_R);
figure; plot(OD_guess_arr, norm2_of_norm2_R);
title(['norm2 of ',plot_str,'. optimal OD = ',num2str(OD_guess_arr(ind_OD_optimal))]); xlabel('OD');
hold on; plot(OD_guess_arr(ind_OD_optimal), norm_optimal,'r*');
end