close all; clc; clear;
S = load('R_DB.mat');
R_DB=  S.R_DB_struct;
R = R_DB.R;

T_Filled = [];
C_Filled = [];
OPL_Filled = [];
for i = 1 : size(R,1)
  if any(any(any(R(i,:,:))))
    T_Filled = [T_Filled, R_DB.T(i)];
  end
end
for i = 1 : size(R,2)
  if any(any(any(R(:,i,:))))
    C_Filled = [C_Filled, R_DB.C(i)];
  end
end
for i = 1 : size(R,3)
  if any(any(any(R(:,:,i))))
    OPL_Filled = [OPL_Filled, R_DB.OPL(i)];
  end
end

T_Filled
C_Filled
OPL_Filled