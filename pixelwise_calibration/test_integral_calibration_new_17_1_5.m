function test_integral_calibration_new_17_1_5()
%% Tests effect of filter OD.
%% Calibration parameter K in the integral calibration is either calculated once for initial OD guess, or per OD guess.
%% OD is found as a minimizer of ||errors|| of the images.
clear; %close all
C2K = @(x) x + 273.15; % Celsius to Kelvin

%% Optimal OD produces K(OD,R) = K(OD) (that is, independant of R):
%{
% Algo:
    1. for each OD guess:
            get K(OD,R) from the BB images
    2. Optimal OD - the one that causes K(OD*,R) = K(OD*):
            OD* = argmin{ variance(K(OD,R)) }
    3. run above per pixel and average all OPTIMAL ODs
%}

%% SUMMARY : THIS TEST FOUND OD* = 2.099 ONLY FOR THE CAVITY BB.

%% TODO: align all images to same pixels => problem either in experiment alignment or in the cropping tool.
IS_CAVITY_BB = true; % 'true' for Cavity BB, 'false' for Plate BB
pixel = [32,22]; % calculate for a given pixel. if empty - for all.
pixel = []; %[283,176]; % calculate for a given pixel. if empty - for all.
NUM_OD = 100; % number of OD guesses to check
MIN_OD = 1;
MAX_OD = 4.2;
%% Calibration related:
Jet_T_range_K = C2K([130, 380]); % Jet's temperature range (since calibration range is typically wider)
apparatus_tf_path = 'apparatus_tfs.mat'; % default apparatus tranfer functions

if IS_CAVITY_BB % Cavity BB calibration parameters
  CalibFilePath = 'C:\Users\matanj\Documents\MATLAB\pixelwise_calibration\CavityBBCalibrationSaveR_OUTPUT.mat';%Cavity BB experiment from 16-12-29
  calib_experiment_params.geometryStruct.geometryFactor = 4.525289256198347e-14; % For the 16-12-29 Cavity calibration.
  calib_experiment_params.e_BB = 0.97;
  calib_atm_params.OPL = 220; % Calibration Atmospheric path in[cm]
  calib_atm_params.co2_concentration = 700e-6; % Calibration Atmospheric CO2 concentration [fraction]
  calib_atm_params.T = C2K(24.5); % Calibration Atmospheric temperature [K]
  calib_atm_params.RH = 38; % [%]
else % Plate BB calibration parameters
  CalibFilePath = 'C:\Users\matanj\Documents\MATLAB\pixelwise_calibration\JetBBCalibrationSaveR_OUTPUT.mat';
  calib_experiment_params.geometryStruct.geometryFactor = 4.481415842872007e-14;
  calib_experiment_params.e_BB = 0.95;
  calib_atm_params.OPL = 155;
  calib_atm_params.co2_concentration = 600-6;
  calib_atm_params.T = C2K(24.5);
  calib_atm_params.RH = 42;
end
calib_atm_params.pressure_tot = 1; %[atm]
calib_experiment_params.detector_pitch_um = 15;
calib_experiment_params.atmParams = calib_atm_params;
%% For HITRAN spectral calculations:
hitranParams.wn_step = 0.01; % HITRAN's wavenumber step [cm^-1]
hitranParams.min_wn = 2000; % HITRAN's minimal wavenumber [cm^-1]
hitranParams.max_wn = 3333; % HITRAN's maximal wavenumber [cm^-1]
hitranParams.HW_to_eval = 500; % Number of HWHM for Voigt evaluation. Orignally 6000 by Ilya. 500 also good enough.
hitranParams.hitranFileFullPath = 'C:\Users\matanj\Documents\MATLAB\pixelwise_calibration\data\hitranCO2_H2O_2000_3333_main_isotopologs.par';
%%%%%
%% Get Calibration test cases:
S = load(CalibFilePath);
R = S.R;
R = squeeze(R); %Dimensions = [T,rows,cols]
SELECTIVE_INDICES_VEC =  1:size(R,1);
% SELECTIVE_INDICES_VEC =  4:6 ;
R = R(SELECTIVE_INDICES_VEC,:,:); % TODO: remove after proper alignment!
% % Perform image registration process to align all the images
% imRef = mat2gray(squeeze(R(end,:,:))); % take an R image as the reference for the registration process
% [optimizer, metric] = imregconfig('Multimodal');
% moving = mat2gray(squeeze(R(6,:,:)));
% registered = imregister(moving, imRef, 'Similarity', optimizer, metric);
% 
% figure; imshow(imRef); colorbar; impixelinfo;
% figure; imshow(moving); colorbar; impixelinfo
% figure; imshow(abs(moving - imRef)); colorbar; impixelinfo
% figure; imshowpair(registered, imRef); colorbar; impixelinfo
% 
% figure; imshow(abs(registered - imRef));colorbar; impixelinfo

experimentCasesArr = S.experimentCasesArr;
T_K_arr = C2K(experimentCasesArr.temps);
T_K_arr = T_K_arr(SELECTIVE_INDICES_VEC);
% calc nan mask of the calibration cases:
nan_mask_R = false(size(R,2), size(R,3));
nan_mask_R(30:110,30:100) = true; % for the Cavity BB
% nan_mask_R(100:290,70:260) = true; % for Plate BB
nan_mask = nan_mask_R; % TODO: remove
if IS_CAVITY_BB
  BB_plot_str = 'Cavity BB: ';
else
  BB_plot_str = 'Plate BB: ';
end
plot_str = 'K';
if ~isempty(pixel)
  R = squeeze(R(:,pixel(1),pixel(2)));
  nan_mask = 1;
  plot_str = ['K of pixel [',num2str(pixel),']'];
else
  GasHelper.plotIm(squeeze(R(end,:,:)).*nan_mask);
end
%% calculate atmosheric path transmittance for the calibration cases:
[wn_vect, ~, tau_spectra_wn] = MyGetSpectrumGPU(true, 'tau', hitranParams, calib_atm_params);
%% Get Planck in [photons/s/cm^2/sr/cm^-1]:
L_BB_photons_wn = zeros(numel(T_K_arr), numel(wn_vect));
for indT = 1 : numel(T_K_arr)
L_BB_photons_wn(indT,:) = calib_experiment_params.e_BB * 2*100*GasHelper.c .* (wn_vect.^2) ./ (exp(100*GasHelper.C2*wn_vect/T_K_arr(indT)) -1);
end
%% Get calibration parameter for a starting guess of OD
[wl_vect, optics_cfits, ~] = GasHelper.get_optics_tf(apparatus_tf_path, hitranParams, false);
hitranParams.tau_spectra_wn = tau_spectra_wn; % save additional recomputations assuming same tau
%% Iterate over OD:
OD_guess_arr = linspace(MIN_OD, MAX_OD, NUM_OD);
if isempty(pixel)
  K_arr = zeros(numel(OD_guess_arr), numel(T_K_arr), size(R,2), size(R,3));
else
  K_arr = zeros(numel(OD_guess_arr), numel(T_K_arr));
end
for indOD = 1:numel(OD_guess_arr) % Loop over OD
  OD_guess = OD_guess_arr(indOD);
  calib_experiment_params.filter_OD = OD_guess;
  %update filter tf with new OD:
  optics_tf_wl = GasHelper.modify_optics_tf_by_OD(wl_vect, optics_cfits, OD_guess);
  calibParams.optics_tf = optics_tf_wl;
  for indT = 1 : numel(T_K_arr) % Loop over R (BB images)
    % Get K(OD,R):
    calibParams = GasHelper.getSpectralCalibrationFromSingleR(squeeze(R(indT,:,:)),...
      T_K_arr(indT), wl_vect, hitranParams, calib_experiment_params, optics_tf_wl, false);
    if isempty(pixel)
      K_arr(indOD,indT,:,:) = calibParams.K; %/ nanmean(calibParams.K(:)); %BUG
    else
      K_arr(indOD,indT)     = calibParams.K ;
    end
  end
end
%% Plot images of K(OD,R(T)):
if ~isempty(pixel)
  figure;
  imagesc(T_K_arr, OD_guess_arr, K_arr); colorbar; impixelinfo
  xlabel('T[K]'); ylabel('OD');
  if isempty(pixel)
    title([plot_str,'_2']);
  else
    title(plot_str);
  end
end
%% find the "Optimal" OD* by finding the minimal variance of K(OD,R):
K_arr_means = nanmean(K_arr, 2);
% K_arr_means = repmat(K_arr_means, [1, numel(T_K_arr), 1, 1]);
% sum_mat = squeeze(nansum(K_arr, 2));
variance_mat = squeeze(nanstd(K_arr, 0, 2) ./ K_arr_means);
% variance_mat = squeeze(nanstd(bsxfun(@rdivide,K_arr,K_arr_means), [], 2)); % apply var along the R(T) axis
%{
figure(); hold on;
for ind1 = 40:1:90
%   var_pixel = variance_mat(:,ind1,70);
  var_pixel = variance_mat(:,70,ind1);
  plot(OD_guess_arr, var_pixel);
  pause(0.2); drawnow;
end
%}
sums_mat = nansum( nansum(variance_mat, 2), 3);
[sum_OD_star, ind_OD_star] = nanmin(sums_mat);
OD_star = OD_guess_arr(ind_OD_star);
figure; plot(OD_guess_arr, sums_mat);
title([BB_plot_str,'\Sigma_p_i_x_e_l_sVar(K) Vs OD.   OD* = ',num2str(OD_star)]); hold on;
xlabel('OD value');
plot(OD_star,sum_OD_star,'r*');
end