%% Use FTS to retreieve R
%%
% *Synopsis*
%%
function script_JetFindTemp_solve_integral_Global_Optimization_FTS
% clc; close all;
dbstop if error
format short
dbstatus
%% Assumptions:
%%  1) each experiment case hase same #files.
%%  2) All images are of same size (row, cols) & pixel allignment
%%  3) Measured Camera's R is independant of solid angle of the object:
%%     R ~ double_integral(Radiance*dOmega*dA), where here the solid angle is of the detector pixel and Camera's FOV, and the detector's area.

C2K = @(x) x + 273.15; % Celsius to Kelvin
K2C = @(x) x - 273.15; % Kelvin to Celsius

%% OPTIONS
FIT_JET_BOUNDARY_TO_LINE = false; % {true}, false
SEPARATE_CONCENTRAION_JET_PARAMS = true; %{false}, true
CALIB_K_OOB_MEAN_OR_NAN = 'nan'; %Replace calibration K by nan or mean(K) where indices are out of K bound %{'nan'} , 'mean'
FIT_TO_LINE_METHOD = 'OTSU'; %{'WLS'} (weighted least-squares) ,'RANSAC', 'OTSU'
INV_ABEL_METHOD = 'NO'; %{'NO'}, 'FE'
WEIGHTS_TYPE = 4; % {1..6} options. 3 - Radiation. 4 - inverse Abel. 5 - uniform weights
IGNORE_SELF_ABSORPTION = false;
GENERATE_R_DB = false;   % loops over T,C and build the R_DB.mat database
GENERATE_TAU_DB = false; % loops over T,C and build the TAU_DB.mat database
TAU_OUTSIDE_INTEGRAL = true;
VERIFY_SINGLE_MEASUREMENT = false; % For e.g. OD testing. Temperature of the measurment as specify in its filename will be used alone.
DO_BINARY_SEARCH = true; % If the temperature dependancy is monotone, binary search would be fastest.
CALC_CALIB_PARAMS = false; % if false, use existing set for testing
REPRESENTATIVE_R_CALIB = false; % if true - representative Rs will be used to obtain calibration params. otherwise, parameters per pixel.
ALL_PIXELS_SEE_BB = true; % for calibrating only a subset of the image
FIX_JET_ANGLE = false;
FIX_JET_ANGLE_DEG_TH = 6; % Skew angle Threshold [deg]. Above it, jet will be aligned verticaly.
NOZZLE_IN_IMAGE = true; % if the images contain the nozzle, select it and get m2pixel, etc.
USE_BIAS_FRAME = false;
FILTER_THE_RADIATION = true; % filter estimated R with a gaussian LPF.
USE_CFD = false; % for the jet model
bSize = 1; % Block size [pixels]
iterative_Convergence_tol = 1e-7; % tolerance for convergance of the iterative process [R]
N_T_iters = 200; % Maximal number of iterations for the iterative process.
N_C = 100; % number of concentrations
max_guessed_temperature_C = 600; % maximal temperature to guess [C]
%% EXPERIMENT RELATED:
% Round pipe orifice:
tot_mfr = 2; %[g/s]
CO2_mfr = 0.5; %[g/s]
D_nozzle = 1e-2; % Nozzle's diameter [m]
atm_OPL = 95; % Atmospheric path in [cm].

atm_co2_concentration = 600*1e-6;%560e-6; % Atmospheric CO2 concentration [fraction]
atm_T = 25.5;%18; % Atmospheric temperature [C]
p_tot = 1; % total pressure [atm]
RH = 48;%42; % [%] Relative Humidity in the Air portion of the Jet

atm_co2_concentration = 560e-6; % Atmospheric CO2 concentration [fraction]
atm_T = 18; % Atmospheric temperature [C]
p_tot = 1; % total pressure [atm]
RH = 42; % [%] Relative Humidity in the Air portion of the Jet

% New Jet - contracted orifice, D = 10mm:
tot_mfr = 2; %3; %[g/s]
CO2_mfr = 0.5;%0.75; %[g/s]
D_nozzle = 1e-2;%1.5e-2; % Nozzle's diameter [m]
atm_OPL = 95;%100; % Atmospheric path in [cm].

atm_co2_concentration = 600e-6; % Atmospheric CO2 concentration [fraction]
atm_T = 28;%25.5; % Atmospheric temperature [C]
p_tot = 1; % total pressure [atm]
RH = 48; % [%] Relative Humidity in the Air portion of the Jet

% % New Jet - contracted orifice, D = 40mm:
tot_mfr = 2; %5; %[g/s]
CO2_mfr = 0.5; %1; %[g/s]
D_nozzle = 3.294e-2; %Vena Contracta @140pixels.  %4e-2; %@170pixels. % Nozzle's diameter [m]
atm_OPL = 155; % Atmospheric path in [cm].

atm_co2_concentration = 600e-6; % Atmospheric CO2 concentration [fraction]
atm_T = 26.5; % Atmospheric temperature [C]
p_tot = 1; % total pressure [atm]
RH = 48; % [%] Relative Humidity in the Air portion of the Jet


% % Round pipe orifice:
tot_mfr = 2; %[g/s]
CO2_mfr = 0.5; %[g/s]
D_nozzle = 1e-2; % Nozzle's diameter [m]
atm_OPL = 95; % Atmospheric path in [cm].

atm_co2_concentration = 600*1e-6;%560e-6; % Atmospheric CO2 concentration [fraction]
atm_T = 25.5;%18; % Atmospheric temperature [C]
p_tot = 1; % total pressure [atm]
RH = 48;%42; % [%] Relative Humidity in the Air portion of the Jet

% % Round pipe orifice, D=20mm:
tot_mfr = 2; %[g/s]
CO2_mfr = 0.5; %[g/s]
D_nozzle = 2e-2; % Nozzle's diameter [m]
atm_OPL = 140; % Atmospheric path in [cm].

atm_co2_concentration = 520*1e-6;%560e-6; % Atmospheric CO2 concentration [fraction]
atm_T = 26.5;%18; % Atmospheric temperature [C]
p_tot = 1; % total pressure [atm]
RH = 48;%42; % [%] Relative Humidity in the Air portion of the Jet



% DB files:
R_DB_file_name = 'R_DB.mat'; % DataBase file for logging R(c,T,distance...)
TAU_DB_file_name = 'TAU_DB.mat'; % DataBase file for logging TAU(c,T,distance...)
% R_DB_file_name = 'R_DB_155cm_D4cm.mat'; % DataBase file for logging R(c,T,distance...)
% TAU_DB_file_name = 'TAU_DB_155cm_D4cm.mat'; % DataBase file for logging TAU(c,T,distance...)

%% Calibration related:
OD_guess = 2.0667; %2.099; % Best estimation from "test_integral_calibration_new_17_1_5.m". Declared OD = 2.
detector_pitch_um = 15; % [um] As declared by SCD (Pelican-D)
e_BB = 0.95; % Plate BB
calib_atm_params.OPL = 155; % Calibration Atmospheric path in[cm]
calib_atm_params.co2_concentration = 560*1e-6;% Calibration Atmospheric CO2 concentration [fraction]
calib_atm_params.T = C2K(24); % Calibration Atmospheric temperature [K]
calib_atm_params.RH = 50; %[%]
% e_BB = 0.97; % emissivity of the Black Body used for calibration. assumed constant spatial-wise and spectrum-wise.
% calib_atm_params.OPL = 220; % Calibration Atmospheric path in[cm]
% calib_atm_params.co2_concentration = 650*1e-6;% Calibration Atmospheric CO2 concentration [fraction]
% calib_atm_params.T = C2K(24.5); % Calibration Atmospheric temperature [K]
% calib_atm_params.RH = 38; %[%]
calib_atm_params.pressure_tot = 1; %[atm]
%% For HITRAN spectral calculations:
hitranParams.wn_step = 0.01; % HITRAN's wavenumber step [cm^-1]
hitranParams.min_wn = 2000; % HITRAN's minimal wavenumber [cm^-1]
hitranParams.max_wn = 3333; % HITRAN's maximal wavenumber [cm^-1]
hitranParams.HW_to_eval = 500; % Number of HWHM for Voigt evaluation. Orignally 6000 by Ilya. 500 also good enough.
% Number of HWHM is a tradoff between computation time and accuracy. Worst
% case for validation is with high concentration and temperatures, as
% expected.
hitranParams.hitranFileFullPath = fullfile(pwd,'data','hitranCO2_H2O_2000_3333_main_isotopologs.par');
%% Default paths:
CalibFilePath = fullfile(pwd,'JetBBCalibrationSaveR_OUTPUT.mat'); % default
CalibOutputName = 'CalibrationParams_Integral.mat';
biasFramePath = 'C:\Users\matanj\Documents\MATLAB\BiasFrameComputed.mat'; % default Bias Frame path
apparatus_tf_path = 'apparatus_tfs.mat'; % default apparatus transfer functions
%% Default test case and related files:
baseDir = 'D:\Laptop BKP\FLIR_Experimental\17-07-13_New_Jet_covered\';
imName = [baseDir, 'G27_T270_F4370_IT3199.8_IM_.ptw'];

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Atmospheric path struct for the experiment case:
atm_params.OPL = atm_OPL; % Atmospheric path [cm]
atm_params.co2_concentration = atm_co2_concentration; % Atmospheric CO2 concentration [fraction]
atm_params.T = C2K(atm_T); % Atmospheric temperature [K]
atm_params.RH = RH; % Atmospheric Water Relative Humidity (RH) [%]
atm_params.pressure_tot = p_tot; % [atm]
% Jet params:
jetParams.OPL = 100*D_nozzle; % Nozzle diameter in [cm]
jetParams.co2_concentration = CO2_mfr/tot_mfr;
jetParams.pressure_tot = p_tot;
%% Load apparatus' transfer functions - Detector, Lens & Filter %%%
[wl_vect, optics_cfits, ~] = GasHelper.get_optics_tf(apparatus_tf_path, hitranParams, false);
% update the initial guess of the filter's OD from its curve:
optics_tf_wl = GasHelper.modify_optics_tf_by_OD(wl_vect, optics_cfits, OD_guess);
%% conditionally calculate calibration parameters:
if CALC_CALIB_PARAMS
  calib_experiment_params.atmParams = calib_atm_params;
  calib_experiment_params.e_BB = e_BB;
  calib_experiment_params.detector_pitch_um = detector_pitch_um;
  calib_experiment_params.filter_OD = OD_guess;
  tic
  [calibParams] = GasHelper.getSpectralCalibrationFromR(CalibFilePath, wl_vect, hitranParams, calib_experiment_params, optics_tf_wl, REPRESENTATIVE_R_CALIB, TAU_OUTSIDE_INTEGRAL);
  toc
  save(CalibOutputName, 'calibParams');% Save coefficients per pixel
else % Use exisiting calibration params:
  if isempty(CalibOutputName)
    error('invalid folder path...');
  end
  load(fullfile(pwd, CalibOutputName));
end
%% Create a file for logging calculated R(T,C,distance), if not exist:
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% HITRAN's DB - Stores R(T,C,OPL) for a given atmospheric scenario (tau) %%%%%
% DB structure's fields:
%   1) T,C,OPL arrays.
%   2) R(T,C,OPL) stores: integral(radiance_photon_flux .* tau .* optics_tf_wn)
%   3) Atmospheric parameters struct.
%   4) HITRAN parameters struct.
%   5) Description.
% Use like: R_simulated = gemoetry_factor * K * R(T,C,OPL).
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if exist(fullfile(pwd,R_DB_file_name), 'file') == 2 % there is already such file
  tic
  S = load(fullfile(pwd,R_DB_file_name));
  toc
  R_DB_struct = S.R_DB_struct;
  % check that HITRAN & atmospheric params of experiment matches the DB's ones:
  assert(R_DB_helper.is_DB_legit_to_case(R_DB_struct, atm_params, hitranParams, TAU_OUTSIDE_INTEGRAL), 'DB''s params don''t match this case!');
else  % create new file
  R_DB_struct = struct('T',[],'C',[],'OPL',[],'R',[]);
  R_DB_struct.T = linspace(C2K(10), C2K(450), N_T_iters )';
  R_DB_struct.C = linspace(400e-6, 1, N_C)';
  R_DB_struct.OPL = [1/1000*(10:1000), 1/100*(101:150) ]'; 
  R_DB_struct.R = zeros(N_T_iters, N_C, numel(R_DB_struct.OPL));
  R_DB_struct.atm_params = atm_params;
  R_DB_struct.hitranParams = hitranParams;
  % Save in version compatible with efficient partial loading/saving.
  % Saving like this should be done only here. Subsequent saves can omit
  % the '-v7.3' flag.
  save(fullfile(pwd,R_DB_file_name),'R_DB_struct','-v7.3');
end
%% Create a file for logging calculated TAU(T,C,distance), if not exist:
if exist(fullfile(pwd,TAU_DB_file_name), 'file') == 2 % there is already such file
  tic
  S = load(fullfile(pwd,TAU_DB_file_name));
  toc
  TAU_DB_struct = S.TAU_DB_struct;
else
  TAU_DB_struct = struct('T',[],'C',[],'OPL',[],'TAU',[]);
  TAU_DB_struct.T = linspace(C2K(10), C2K(450), 100 )';
  TAU_DB_struct.C = linspace(400e-6, 1, 100)';
  TAU_DB_struct.OPL = [1./(1:100)]';
  TAU_DB_struct.TAU = zeros(numel(TAU_DB_struct.T), numel(TAU_DB_struct.C), numel(TAU_DB_struct.OPL));
  TAU_DB_struct.hitranParams = hitranParams;
  % Save in version compatible with efficient partial loading/saving.
  % Saving like this should be done only here. Subsequent saves can omit
  % the '-v7.3' flag.
  save(fullfile(pwd,TAU_DB_file_name),'TAU_DB_struct','-v7.3');
end


if ~(GENERATE_R_DB || GENERATE_TAU_DB)
%% Create griddedInterpolant objects for faster access to DBs:
tic
F_R   = griddedInterpolant({R_DB_struct.T, R_DB_struct.C, R_DB_struct.OPL}, R_DB_struct.R);
F_TAU = griddedInterpolant({TAU_DB_struct.T, TAU_DB_struct.C, TAU_DB_struct.OPL}, TAU_DB_struct.TAU);
toc
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% %%%%%%%%%%%%%%%%% Test the calibration: %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Get test case:
 [imName, baseDir] = uigetfile({'*.ptw','FLIR images'},'Select image to test calibration',imName);
if isempty(imName)
  error('invalid image');
end
%Get the Background file that matches the Image file's name:
bgName = GasHelper.getBGfileFromIMfile(imName, baseDir);
%Get the temperature of the measurement from file's name:
matchStr = regexp(imName,'_T+[0-9]+[0-9]+[0-9]','match');
TrefStr = regexprep(matchStr{1},'_T','');
%Set the image pair object:
imPair = ImageBGPairExt([baseDir,imName]);
imPair.setBG([baseDir,bgName{1}]);
%% Get the Bias frame:
%   [biasFrameFull, biasFrame] = cropBiasFrame2PTWsize(biasFramePath, imInfo);
%% crop the suitable subframe from it and rotate accordingly:
if USE_BIAS_FRAME
  [ biasSubframe ] = suitBiasSubframeFromBiasFrame( imPair.getImage(), imPair.IMG_Info, biasFramePath );
end
subIm = mean(abs(imPair.getSubtracted()), 3); % time averaging the frame sequence
if ALL_PIXELS_SEE_BB
  objMask = true(size(subIm));
else
  [~, object] = GasHelper.processImGetStats(subIm);%get meaningful object within the image
  objMask = true(imPair.IMG_Info.m_rows, imPair.IMG_Info.m_cols);%initial mask
  objMask(isnan(object)) = NaN;
end
%Mask the image to maintain same size:
DL_IM = objMask.*subIm;
DL_IM(DL_IM==0) = 1; % set minimal DL to 1 to avoid zero issues

%% Align image to calibration images (map1):
imgCalibInfo.m_cliprect = [160 100 320 360]; % TODO: remove and supply true file!
mask_calib_im_in_full_frame = GasHelper.FLIR_mask_location_of_image_in_full_frame(imgCalibInfo);

%% trim image to orifice and get pipe's diameter in [pixels]:
if NOZZLE_IN_IMAGE
  [DL_new, D_nozzle_pixels, m2pixel, pos_nozzle, ~] = GasHelper.trimIm2Nozzle(DL_IM, D_nozzle);
end

%% Align image to calibration images (map1):
Nozzle_Cropped_IMG_Info.m_cliprect = imPair.IMG_Info.m_cliprect;
Nozzle_Cropped_IMG_Info.m_cliprect(4) = floor(pos_nozzle(1,2));
mask_target_im_in_full_frame = GasHelper.FLIR_mask_location_of_image_in_full_frame(Nozzle_Cropped_IMG_Info);

%% Modify the calibration image to fit required image:
K_bkp = calibParams.K;
mask_calib_im_in_full_frame(mask_target_im_in_full_frame & mask_calib_im_in_full_frame) = 2;
mask_target_im_in_full_frame(mask_target_im_in_full_frame & mask_calib_im_in_full_frame) = 2;

K_full_frame = mask_calib_im_in_full_frame;
if isequal(CALIB_K_OOB_MEAN_OR_NAN, 'mean')
  K_full_frame(mask_calib_im_in_full_frame < 2) =  nanmean(calibParams.K(:));
else
  K_full_frame(mask_calib_im_in_full_frame < 2) = nan;
end
K_full_frame(mask_calib_im_in_full_frame > 0) = calibParams.K;
calibParams.K = K_full_frame( mask_target_im_in_full_frame > 0 );
calibParams.K = reshape( calibParams.K, size(DL_new) );



%% Rotate Jet if needed:
if FIX_JET_ANGLE
  %% TODO: should apply same to the calibration maps, or rotate back b4 using the calibration
  DL_new = GasHelper.fixJetAngle(DL_new);
  %Align image to calibration images:
  remove_rotation_added_rows();
  map2();
end

%% Apply calibration with K
K = calibParams.K;
% First, crop object image to the calibration image, if needed:
if ~(exist('rect','var') ) % if cropping wasn't done:
  [rowsCalib, colsCalib] = size(calibParams.K);
  [rowsNew, colsNew] = size(DL_new);
  if rowsNew ~= rowsCalib || colsNew ~= colsCalib
    rows = 1:floor(pos_nozzle(1,2)); % TODO: what if NOZZLE_IN_IMAGE == false?
    cols = 1:size(DL_new,2);
  else
    rows = 1:size(DL_new,1);
    cols = 1:size(DL_new,2);
  end
else % if 'rect' is present:
  rows = rect(2):rect(2)+rect(4);
  cols = rect(1):rect(1)+rect(3);
end
% If doing pixel-by-pixel and image's size > calibration image, should exit.
% if representative calibration parameters are chosen - no problem
%% Below block is obsolete after claibration-target image alignment fix:
%{
if REPRESENTATIVE_R_CALIB
  K = K(1,1)*ones(numel(rows), numel(cols));
elseif (size(DL_new,1) > size(calibParams.K,1)) || (size(DL_new,2) > size(calibParams.K,2))% exit if image size > calibration image size
  warning('Image size > calibraion image size!! cropping the image.');
  % cropping jet's image:
  dRows = size(DL_new,1) - size(calibParams.K,1);
  dCols = size(DL_new,2) - size(calibParams.K,2);
  if dRows > 0
    rows = rows(dRows+1:end);
  end
  if dCols > 0
    cols = cols(floor(dCols/2)+1 : end-floor(dCols/2));
  end
  DL_new = DL_new(rows, cols);
end
totNaNMask = isnan(K);
DL_new(totNaNMask) = nan;
%}
%% Get the a mask of the actual jet shape, and the centerline of symmetry:
jetBW = GasHelper.getActualJetFromImage(DL_new);
[symAxis, skew_angle_deg] = GasHelper.getAxisOfSymFromJetIm(DL_new.*jetBW);
% DEBUG: check that axis of symetry is poperly retreived:
GasHelper.plotIm(DL_new,'Estimated symmetry axis'); hold on; scatter(symAxis*ones(1,size(DL_new,1)), 1:size(DL_new,1), 'r.');
disp(['Is axis of symmetry locaterd @ column ',num2str(symAxis),'?']);
tmp = input('If Yes - please hit Enter. Else - please enter the column: ');
if ~isempty(tmp)
  symAxis = tmp;
  GasHelper.plotIm(DL_new,'Custom symmetry axis'); hold on; scatter(symAxis*ones(1,size(DL_new,1)), 1:size(DL_new,1), 'r.');
end
%% estimate the measurement's radiation from DL = R*IT^P
if USE_BIAS_FRAME % Subtraction of R images
  R_est = GasHelper.GetMeasurementRByMultiIT2RP(baseDir, TrefStr, DL_new, rows, cols, biasSubframe, ALL_PIXELS_SEE_BB);
else % subtraction of DL images
  R_est = GasHelper.GetMeasurementRByMultiIT2RP(baseDir, TrefStr, DL_new, rows, cols, [], ALL_PIXELS_SEE_BB);
end
assert( all(R_est(~isnan(R_est)) >= 0) || all(R_est(~isnan(R_est)) <=0) ); % mixed signs are not allowed!
%assign 'NaN' to pixels that are more than 7 stds away the mean, to filter
%spikey noise:
R_est = GasHelper.filterStds(R_est, 7);

R_est0 = abs(R_est); % fix for possible confusion of IM and BG
R_orig = abs(R_est);

% Estimate the noise level of the R image:
% [nlevel th] = NoiseLevel(R_orig); nlevel / nanmean(R_orig(:));
bg_part_of_image = R_est.*~jetBW;
bg_part_of_image(jetBW>0) = nan;
fg_part_of_image = R_est.*jetBW;
fg_part_of_image(jetBW<=0) = nan;

mu_bg_part = nanmean(bg_part_of_image(:));
mu_fg_part = nanmean(fg_part_of_image(:));
RMS_noise = sqrt( nanmean( (fg_part_of_image(:) - mu_bg_part).^2 ) );

noise_parameter = mu_fg_part / nanstd(bg_part_of_image(:)) ;
% SNR = nanstd(fg_part_of_image(:)) / nanstd(bg_part_of_image(:));
SNR = (mu_fg_part-mu_bg_part) / nanstd(bg_part_of_image(:)) ;
assert(SNR > 0, 'Invalid SNR value!');
SNR = 10*log10(SNR); % convert to [dB]
SNR2 = 20*log10(mu_fg_part / RMS_noise); %in [dB]
gaussian_LPF_param = 0.5; % DEFAULT value
% if noise_parameter > 0.5
if SNR < 20 % poorer than 20dB
  gaussian_LPF_param = 1.5;
end

if FILTER_THE_RADIATION
  R_est_filt = imgaussfilt(R_orig, gaussian_LPF_param); % Gaussian LPF. should preserve edges.
  R_est0 = R_est_filt;
end
assert(all(R_est0(:)),'invalid R...');

%% Align the jet vertically, if it worth it:
%% TODO: What about rotating and translating the K image accordingly? 
RWorld = imref2d(size(R_est0)); % Set R_est coordinate frame as the World coordinate frame. % TODO: ?
if abs(skew_angle_deg) > FIX_JET_ANGLE_DEG_TH
  if sum(sum(isnan(R_est0))) % If there's NaNs in image, set a warning:
    warning('R images has NaNs. Will be converted to 0 before rotation... check the output image!');
    R_est0(isnan(R_est0)) = 0; % cannot transform image with NaN.
  end
%   [R_est, ~ ] = GasHelper.fixJetAngle(R_est0, RWorld, skew_angle_deg); %
%   BUG - I don't want to rotate around the center, but around the jet
%   nozzle's center!
  R_est = rotateAround(R_est0, size(R_est0,1), symAxis, skew_angle_deg);
  R_est(isnan(R_est)) = 0; % TODO!!!
else
  R_est = R_est0;
end

%% Fuse the two half-jets into a single one (average):
R_left_half_jet = fliplr(R_est(:,1:symAxis));
R_right_half_jet = R_est(:,symAxis:end);
width_half_jet = min(size(R_left_half_jet,2), size(R_right_half_jet,2)); % '1' due to the centerline
R_left_half_jet  = R_left_half_jet(:, 1:width_half_jet);
R_right_half_jet = R_right_half_jet(:, 1:width_half_jet);
R_fuzed = (R_left_half_jet + R_right_half_jet)/2; % just averaging the two halfs
R_est_bkp = R_est; % Backup
R_est = zeros(size(R_est_bkp));
R_est(:,symAxis:symAxis+width_half_jet-1) = R_fuzed;
%{
% DEBUG:
figure; plot(R_left_half_jet(end,:),'b'); hold on; 
plot(R_right_half_jet(end,:),'r'); plot(R_fuzed(end,:),'m');
legend('left half','right half','average'); title('R @ nozzle');
%}

%% Align image to calibration images (map3):
% Currently not needed as I use the entire image as input:
if 0
Half_Jet_Cropped_IMG_Info.m_cliprect = Nozzle_Cropped_IMG_Info.m_cliprect;
Half_Jet_Cropped_IMG_Info.m_cliprect(1) = Half_Jet_Cropped_IMG_Info.m_cliprect(1) + symAxis;
Half_Jet_Cropped_IMG_Info.m_cliprect(3) = width_half_jet;
mask_target_im_in_full_frame = GasHelper.FLIR_mask_location_of_image_in_full_frame(Half_Jet_Cropped_IMG_Info);
mask_target_im_in_full_frame(mask_target_im_in_full_frame & mask_calib_im_in_full_frame) = 2;
end

%% Modify the calibration image to fit required image:
% calibParams.K = nanmean(calibParams.K(:))*ones(size(calibParams.K));% 13/6/17 TODO: check!!!
%{
K_bkp = calibParams.K;
mask_calib_im_in_full_frame(mask_target_im_in_full_frame & mask_calib_im_in_full_frame) = 2;
K_full_frame = mask_calib_im_in_full_frame;
K_full_frame(mask_calib_im_in_full_frame < 2) =  nanmean(calibParams.K(:));
K_full_frame(mask_calib_im_in_full_frame > 0) = calibParams.K;
calibParams.K = K_full_frame( mask_target_im_in_full_frame > 0 );
calibParams.K = reshape( calibParams.K, size(R_est) );
%}
%% apply HITRAN transmittance model for the atmosheric path of the measurement
[wn_vect, ~, tau_spectra_wn] = MyGetSpectrumGPU(true, 'tau', hitranParams, atm_params);

if TAU_OUTSIDE_INTEGRAL
  [~, optics_tf_wn] = GasHelper.spectralFunc_wl2wn(calibParams.wl_vect, calibParams.optics_tf, true);
  tau_avg = GasHelper.getWeightedTransmittance(hitranParams, optics_tf_wn, atm_params);
  R_est = R_est ./ tau_avg;
end


%% GENEREATE A FULL DB FOR RADIATION WITH OPTIC PATH = PIXEL SIZE:
if GENERATE_R_DB
  jetParams.OPL = 1/m2pixel*100;
  for indT = 1 : numel(R_DB_struct.T)
    for indC = 1 : numel(R_DB_struct.C)
      jetParams.T = R_DB_struct.T(indT);
      jetParams.co2_concentration = R_DB_struct.C(indC);
      params.hitranParams = hitranParams; params.wn_vect = wn_vect; params.jetParams = jetParams; params.calibParams = calibParams;
      [~, R_DB_struct] = R_DB_helper.get_R_from_DB(R_DB_struct, jetParams.T, jetParams.co2_concentration, jetParams.OPL, params );    
    end
    disp(['Finished inner loop #',num2str(indT)]);
  end
  save((fullfile(pwd,R_DB_file_name)),'R_DB_struct');
  return;
end
%% GENEREATE A FULL DB FOR TRANSMISSION WITH OPTIC PATH = PIXEL SIZE:
if GENERATE_TAU_DB
  jetParams.OPL = 1/m2pixel*100;
  jetParams.RH = 40; %TODO: remove hard-coded
  for indT = 1 : numel(TAU_DB_struct.T)
    for indC = 1 : numel(TAU_DB_struct.C)
      jetParams.T = TAU_DB_struct.T(indT);
      jetParams.co2_concentration = TAU_DB_struct.C(indC);
      params.hitranParams = hitranParams; params.optics_tf_wn = optics_tf_wn; params.path_params = jetParams;
      [~, TAU_DB_struct] = TAU_DB_helper.get_TAU_from_DB(TAU_DB_struct, jetParams.T, jetParams.co2_concentration, jetParams.OPL, params );
    end
    disp(['Finished inner loop #',num2str(indT)]);
  end
  save((fullfile(pwd,TAU_DB_file_name)),'TAU_DB_struct');
  return;
end

%% Calculate solid_angle of a single Pixel and detector area for later use:
OPL_step = D_nozzle/D_nozzle_pixels*1e2; %in [cm] for HITRAN
% Approximate the solid angle of a cell in the jet:
solid_angle = (OPL_step/(atm_OPL))^2;
detector_area_cm2 = (detector_pitch_um*1e-4)^2; % Detector's area in [cm^2], to match HITRAN units
geometry = solid_angle*detector_area_cm2;

%% Parameters for the models of temperature & concentration profiles in the jet:
params.useCFD = USE_CFD;
if USE_CFD
  CFDpath = fullfile(pwd,'FreeJet.xlsx');
  params.data = GasHelper.GetCFDdataFromANSYS(CFDpath);
end
params.D = D_nozzle;
params.tot_mfr = tot_mfr; % Total mass flow rate, [g/s]
params.co2_mfr = CO2_mfr; % CO2 mass flow rate, [g/s]
params.Tamb_C = atm_T; % ambient temperature, [C]
params.Camb = atm_co2_concentration; % CO2 concentration in the ambient
params.P0 = 101325; %[Pa]. Pressure @ outlet is 1 atm = 101325[Pa]
idxMax = symAxis;

RWorld = imref2d(size(R_est)); 

%% TAKE only the lines next to the exshaust: % TODO: make better
% next to the nozzle's exhaust:
ROI_rows = bSize*floor(size(R_est,1)/bSize);  % TODO: remove

%% Get the measured R at the centerline:
% For the distribution on the axis, no need for ABEL inverese:
R_measurement = R_est(ROI_rows,symAxis); % Assuming uniform distribution in the potential core
disp(['R from measurement = ',num2str(R_measurement)]);
% Pre-alloc for the iterative process:
T_guesses = nan(N_T_iters,1);
err_norm_vec = nan(N_T_iters,1);

%% Define searchable temperatures:
if VERIFY_SINGLE_MEASUREMENT
  calcTemps_arr = C2K(str2double(TrefStr));
else
  calcTemps_arr = linspace(C2K(atm_T), C2K(max_guessed_temperature_C), N_T_iters);
end
if DO_BINARY_SEARCH
  LU_temp_ind = ceil(numel(calcTemps_arr)/2); % initial guess
else
  LU_temp_ind = 1;
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% ITERATIVE PROCESS: Sweep Tjet in nozzle's mid pixel, compare simulated irradiance to measurement.
R_guess_arr = {};
for k = 1 : min(N_T_iters, numel(calcTemps_arr))
  jetParams.T = calcTemps_arr(LU_temp_ind);
  params.Tjet_C = K2C(calcTemps_arr(LU_temp_ind)); %New guess for Jet outlet temperature[C]
  T_guesses(k) = params.Tjet_C;
  %% Find emission radiance using HITRAN, in each block, and scale to photon flux (the camera counts photons)
  % Check DB b4 time-consuming evaluation:
  params.hitranParams = hitranParams; params.jetParams = jetParams;params.wn_vect = wn_vect; params.calibParams = calibParams; params.tau_spectra_wn = tau_spectra_wn;
  [R_normalized, R_DB_struct] = R_DB_helper.get_R_from_DB(R_DB_struct, jetParams.T, jetParams.co2_concentration, jetParams.OPL, params );
  R_guess = geometry .* calibParams.K .* R_normalized;

  if REPRESENTATIVE_R_CALIB
    R_guess = R_guess * ones(size(DL_new));
  end
  
  %% Update current value:
  R_guess = R_guess(ROI_rows,symAxis);
  disp(['R_guess(Tjet=',num2str(T_guesses(k)),'C) = ',num2str(R_guess)]);
  R_guess_arr{k} = R_guess;
  %% Check convergence and update guess:
  R_err = R_guess - R_measurement;
  err_norm_vec(k) = norm(R_err);
  if (norm(R_err) < iterative_Convergence_tol) || (numel(calcTemps_arr) == 1)
    break;
  end
  if DO_BINARY_SEARCH % Apply binary search to find temperature:
    if LU_temp_ind==1 % we've reach 2 elemets array
      calcTemps_arr = calcTemps_arr(1);
    elseif norm(R_guess) > norm(R_measurement) % search in the lower temperatures
      calcTemps_arr = calcTemps_arr(1:LU_temp_ind-1); % update search range
    else
      calcTemps_arr = calcTemps_arr(LU_temp_ind+1:end); % update search range
    end
    LU_temp_ind = round(numel(calcTemps_arr)/2); % update new guess index
  else % sweep over entire temperature candidates:
    LU_temp_ind = LU_temp_ind + 1;
  end
end % end of iterative process
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Here we assume the outlet temperature have been found correctly: %%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[~,ind_T_recovered] = min(err_norm_vec); % the assumed index of the recovered temperature
T_center_nozzle = T_guesses(ind_T_recovered);

% DEBUG:
% Plotting result:
figure;
final_error = 100*abs(T_center_nozzle-str2double(TrefStr))/C2K(str2double(TrefStr)); % percentage error
plot(C2K(T_guesses), err_norm_vec,'bo'); title('||err|| Vs Guessed temperature [K]'); hold on;
plot(C2K(T_center_nozzle), err_norm_vec(ind_T_recovered), 'r*', 'MarkerSize', 10);
suptitle(['Upstream T @ CO_2 fraction = ',num2str(CO2_mfr/tot_mfr), ', Distance = ',num2str(atm_OPL),'cm']);
title(['Measured = ',num2str(C2K(str2double(TrefStr))),'K,  Estimated = ',num2str(C2K(T_center_nozzle)),'K.  Error[%] = ',num2str(final_error)]);
xlabel('T guess [K]'); ylabel('||Radiation error||');

% DEBUG
%{
T_guesses = C2K(T_guesses(~isnan(T_guesses)));
err_norm_vec = err_norm_vec(~isnan(err_norm_vec));

dx = diff(T_guesses);
dy = diff(err_norm_vec);
plot(T_guesses,err_norm_vec,'bo'); hold on;
plot(C2K(T_center_nozzle), err_norm_vec(ind_T_recovered), 'r*');
quiver(T_guesses(1:end-1),err_norm_vec(1:end-1),dx,dy,0,'MaxHeadSize',0.02); hold off;
suptitle(['Upstream T @ CO_2 fraction = ',num2str(CO2_mfr/tot_mfr), ', Distance = ',num2str(atm_OPL),'cm']);
title(['Measured = ',num2str(C2K(str2double(TrefStr))),'K,  Estimated = ',num2str(C2K(T_center_nozzle)),'K.  Error[%] = ',num2str(final_error)]);
xlabel('T guess [K]'); ylabel('||Radiation error||');

figure;
for ip = 1 : numel(T_guesses)-1
  arrow([T_guesses(ip),err_norm_vec(ip)], [T_guesses(ip+1),err_norm_vec(ip+1)],'Length',10,'tipangle',10);
  hold on;
end
%}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Set the OPL of a single pixel in [cm]:
jetParams.OPL = 1/m2pixel*100; % pixel equivalent OPL in [cm]

%% Get a better estimate on the Jet's boundaries:
if FIT_JET_BOUNDARY_TO_LINE
  [jetBW, h_jet_bounds]= GasHelper.get_mask_of_jet(R_est,symAxis, false);
  %DEBUG:
  ax = findall(h_jet_bounds, 'type', 'axes');
  jet_bounds_im = get(ax,'Children'); % expect: "Line", "Scatter", "Image"
  % Get the jet bound points and shift to match full image:
  jet_bounds_points.X = symAxis + jet_bounds_im(2).XData;
  jet_bounds_points.Y = jet_bounds_im(2).YData;
  % Get the jet bound line and shift to match full image:
  jet_bounds_line.X = symAxis + jet_bounds_im(1).XData;
  jet_bounds_line.Y = jet_bounds_im(1).YData;
  %Plot all together:
  figure; imagesc(R_est0); colorbar; axis image; hold on;
  plot(symAxis*ones(1,size(R_est,1)), 1:size(R_est,1),'r', 'Linewidth', 2);
  scatter(jet_bounds_points.X, jet_bounds_points.Y, 20, '.m');
  plot(jet_bounds_line.X, jet_bounds_line.Y, 'r', 'Linewidth', 2);
  title('Determining jet boundary & axis of symmetry from R image');
  % Convert axes:
  normalize_axes_full_im(R_est0,D_nozzle_pixels, symAxis, 2, 2);
else
%   jet_bounds_points = symAxis + sum(jetBW,2) - 1;
  jet_bounds_points = symAxis + sum(jetBW(:,symAxis:end),2) - 1;
  GasHelper.plotIm(R_est); hold on; scatter(jet_bounds_points, 1:numel(jet_bounds_points), 20, '.m');
  normalize_axes_full_im(R_est0,D_nozzle_pixels, symAxis, 2, 2);
end


%% Apply Inverse Abel, to get the potential core edges of the measurement:
im = R_est.*jetBW;
[R_est_Abel_Inverted] = GasHelper.InverseAbelHalfImage(im(:,symAxis:end), 1, INV_ABEL_METHOD, true);

%% release assumption on jet's boundaries mask
jet_validation_mask = jetBW(:, symAxis:end); % save for validation
% jetBW = ones(size(jetBW));

%% Estimate the potential core line from the inverse Abel image:
[X, Y, ~] = GasHelper.get_potential_core_from_half_image(R_est_Abel_Inverted,[],[],[],FIT_TO_LINE_METHOD);
X = X(:); Y = Y(:);
potential_core_slope = (Y(1)-Y(end))/(X(1)-X(end));
potential_core_line = [X,Y];

%% Estimate potenial core top point from its edge line:
assert(potential_core_slope>0,'Potential core line estimated wrong!');
measured_potential_core_length = X(end) + Y(1)/potential_core_slope; %[pixels]

%% Another try, using only the inverse Abel transform of R image and iterate the R_DB.
%{
% First, restrict search ranges:
conditions_ranges_struct.Tmax = C2K(1.05*T_center_nozzle);
conditions_ranges_struct.Tmin = C2K(atm_T);
conditions_ranges_struct.Cmax = 1.1*CO2_mfr/tot_mfr;
conditions_ranges_struct.Cmin = 0.9*CO2_mfr/tot_mfr;%atm_co2_concentration;
conditions_ranges_struct.OPL = 1/m2pixel*100;
% TODO: the following is very sensative to R_scale error!!!!
R_est_Abel_Inverted_scaled = R_est_Abel_Inverted ./ geometry ./ calibParams.K(1:ROI_rows,symAxis:end);
R_scaled_temp = R_est_Abel_Inverted_scaled(ROI_rows, 1);
R_DB_tol = 0.005*R_est_Abel_Inverted(ROI_rows, 1) ./ geometry ./ calibParams.K(ROI_rows,symAxis); % tolerance for the centerline
[candidate_conditions] = R_DB_helper.get_parameters_from_R_DB(R_DB_struct, R_scaled_temp, conditions_ranges_struct, R_DB_tol);
assert(~isempty(candidate_conditions),'No entries with OPL in R_DB!')
%}

%% Recalculate the radiation field based on the updated model: 
  function [R_sim_projected, T_im, C_im] = get_R_sim(model_params, ignoreSelfAbsorption, doNotProjectForward)
    if nargin < 1
      error('Invalid args!');
    end
    if nargin < 2
      ignoreSelfAbsorption = false;
    end
    if nargin < 3
      doNotProjectForward = false;
    end
    %Generate T & C images:
    if SEPARATE_CONCENTRAION_JET_PARAMS
      [T_im, C_im] = GasHelper.gen_T_C_images_from_model_seperated(model_params, atm_params, numel(jet_boundary_line), jet_boundary_line);
    else
      [T_im, C_im] = GasHelper.gen_T_C_images_from_model(model_params, atm_params, numel(jet_boundary_line), jet_boundary_line); % ~0.04 sec
    end
    R_sim_projected = nan(size(T_im));
    %Calculate profile's emission:
    emission_sim_im = F_R(T_im(:), C_im(:), jetParams.OPL*ones(numel(T_im),1));% Interpolate R DB
    emission_sim_im =  geometry .* calibParams.K(end-size(T_im,1)+1:end,symAxis:symAxis+size(T_im,2)-1) .* reshape(emission_sim_im, size(T_im,1),[]); % BUG fix 1/10/17
    %Calculate profile's transmission:
    if ignoreSelfAbsorption % Ignore self-absorption
      tau_sim_im = ones(size(T_im));
    else
      tau_sim_im = F_TAU(T_im(:), C_im(:), jetParams.OPL*ones(numel(T_im),1));% interpolate TAU DB
      tau_sim_im = reshape(tau_sim_im, size(T_im,1),[]);
    end
    % Project the profiles to the camera's domain if needed:
    for iRow = 1 : size(T_im, 1)% Loop over rows:
%       r = find(T_im(iRow,:),1,'last'); %radius of current row
      r = jet_boundary_line(iRow); % neglegable improvement over above
      % Estimating the measured radiation using the T,C models:
      if doNotProjectForward % Set R_sim_projected==emission_sim:
        R_sim_projected(iRow, 1:r) = emission_sim_im(iRow,1:r);
      else % Otherwise, Apply Forward Projection:
        R_sim_projected(iRow, 1:r) = GasHelper.forward_onion_peeling(emission_sim_im(iRow,1:r), tau_sim_im(iRow,1:r), A_array{iRow});
      end
    end   
    % Approximate path's transmission as constant if needed:
    if TAU_OUTSIDE_INTEGRAL
      R_sim_projected = R_sim_projected ./ tau_avg;
    end
  end


% Faster function w/o self-absorption
  function [R_sim_projected, T_im, C_im] = get_R_sim_no_self_absorp(model_params, doNotProjectForward)
    if nargin < 1
      error('Invalid args!');
    end
    if nargin < 2
      doNotProjectForward = false;
    end
    %Generate T & C images:
    if SEPARATE_CONCENTRAION_JET_PARAMS
      [T_im, C_im] = GasHelper.gen_T_C_images_from_model_seperated(model_params, atm_params, numel(jet_boundary_line), jet_boundary_line); % ~0.04 sec
    else
      [T_im, C_im] = GasHelper.gen_T_C_images_from_model(model_params, atm_params, numel(jet_boundary_line), jet_boundary_line); % ~0.04 sec
    end
    R_sim_projected = nan(size(T_im));
    %Calculate profile's emission:
    emission_sim_im = F_R(T_im(:), C_im(:), jetParams.OPL*ones(numel(T_im),1));% Interpolate R DB
    % TODO: WRONG calibration indices!!!!!
    emission_sim_im =  geometry .* calibParams.K(1:size(T_im,1),symAxis:symAxis+size(T_im,2)-1) .* reshape(emission_sim_im, size(T_im,1),[]);
    % Project the profiles to the camera's domain if needed:
    for iRow = 1 : size(T_im, 1)% Loop over rows:
      r = jet_boundary_line(iRow); % neglegable improvement over above
      % Estimating the measured radiation using the T,C models:
      if doNotProjectForward % Set R_sim_projected==emission_sim:
        R_sim_projected(iRow, 1:r) = emission_sim_im(iRow,1:r);
      else % Otherwise, Apply Forward Projection:
        R_sim_projected(iRow, 1:r) = GasHelper.forward_onion_peeling_no_self_absorp(emission_sim_im(iRow,1:r), A_array{iRow});
      end
    end
    % Approximate path's transmission as constant if needed:
    if TAU_OUTSIDE_INTEGRAL
      R_sim_projected = R_sim_projected ./ tau_avg;
    end
  end



%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Testing influence of model's parameters on R_sim %%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% close all; % TODO

%% Define a struct for the model parameters:
%% Concentration model is same as temperature up to scaling.
max_OPL_radius = max( sum(jetBW(:,symAxis:end),2) );
N_ROWS = size(R_est,1);
model_params.T0 = C2K(T_center_nozzle); % T_nozzle [K]
model_params.C0 = CO2_mfr/tot_mfr; % C_nozzle [1]
model_params.X_PC = N_ROWS - ceil(measured_potential_core_length); % Potential Core height [pixels]
model_params.Y_PC = Y(end); % Potential Core width @ nozzle [pixels]
model_params.Y_up = max_OPL_radius; %nansum(jetBW(1,symAxis:end)); % Jet width @ the upper row of the image [pixels]
model_params.Y_nozzle = D_nozzle_pixels/2;%nansum(jetBW(end,symAxis:end)); % Jet width @ the bottom row of the image [pixels]
model_params.b = -0.95*measured_potential_core_length; % Parameter 'b' [pixels] of the centerline model:
                                %T(x) = (Tx0-Tatm)*(x0-b)/(x-b) + Tatm
model_params.r_0_5_nozzle = (model_params.Y_PC + model_params.Y_nozzle) / 2; % take as mean of those
% TODO: Maybe better estimate below using some common known slope:
model_params.r_0_5_up = (model_params.r_0_5_nozzle + model_params.Y_up) / 2; % take as mean of those

model_params.separate_C_and_T = SEPARATE_CONCENTRAION_JET_PARAMS;
if SEPARATE_CONCENTRAION_JET_PARAMS % Concentration parameters:
  model_params.C_Y_PC = model_params.Y_nozzle;
  model_params.C_r_0_5_nozzle = model_params.r_0_5_nozzle;
  set_model_params = @(x) set_model_params_seperated(x);
else
  set_model_params = @(x) set_model_params(x);
end

model_params_orig = model_params;


%% Crop the measurement R image of interest:
if SEPARATE_CONCENTRAION_JET_PARAMS
  [T_im, ~] = GasHelper.gen_T_C_images_from_model_seperated(model_params, atm_params, N_ROWS);
else
  [T_im, ~] = GasHelper.gen_T_C_images_from_model(model_params, atm_params, N_ROWS);
end
%% Allow another boundary for the jet:
jet_boundary_line = GasHelper.get_jet_boundary_line(R_est(:,symAxis:end));
jet_boundary_line(jet_boundary_line > size(T_im,2)) = size(T_im,2);

%% Crop the measurement:
R_m = R_est(:, symAxis:symAxis+size(T_im,2)-1) - repmat(R_est(:, symAxis+size(T_im,2)-1), [1 size(T_im,2)]);


%% release assumption:
jet_boundary_line = size(T_im,2) * ones(size(T_im,1), 1);

%% %% Calculate the matrices of geometric terms for the onion-peeling stuff, for each row:
% TODO: save it to file and load!
A_array = cell(N_ROWS, 1);
for ii = 1 : N_ROWS
  A_array{ii} = GasHelper.calculate_geometric_matrix(jet_boundary_line(ii));
end


%% Generate image of weights and the ROI:
x0 = floor(measured_potential_core_length);
rows = 1 : N_ROWS;
max_OPL_radius = max( sum(jetBW(rows,symAxis:end),2) );
max_dim = max(N_ROWS, max_OPL_radius);

switch WEIGHTS_TYPE
  case 1 % OPTION 1:
    weights_im = jetBW(rows, symAxis:symAxis+max_OPL_radius-1);
    % Use the estimated potential core for weights:
    for xx = X(Y>0)
      weights_im(xx, 1:ceil(Y(xx)) ) = xx; % give linearly increasing weight toward the nozzle
    end
    
  case 2 % OPTION 2: Linear decay dowstream and heavy weighting the axis:
    w = repmat(max_dim:-1:1, [N_ROWS, 1]);
    w = flipud(w + w');
    w = w(:,1:max_OPL_radius);
    w(:,1) = 10*w(:,1); % ADDED, Check
    weights_im = w .* jetBW(rows, symAxis:symAxis+max_OPL_radius-1);
    
  case 3 % OPTION 3: use R_est
    weights_im = R_est(rows, symAxis:symAxis+max_OPL_radius-1).*jetBW(rows, symAxis:symAxis+max_OPL_radius-1);
    weights_im = R_fuzed(rows,1:max_OPL_radius) .* jetBW(rows, symAxis:symAxis+max_OPL_radius-1);
    % Add additional weights for the Potential Core.
    for xx = X(Y>0)
      weights_im(xx, 1:ceil(Y(xx))) = 10 * weights_im(xx, 1:ceil(Y(xx)));
    end

  case 4 % OPTION 4: use R_est_Abel_inverse
    weights_im = R_est_Abel_Inverted(:, 1:size(R_m, 2)) .* jetBW(rows, symAxis:symAxis+max_OPL_radius-1);
    weights_im(isnan(weights_im)) = 0;
    weights_im(weights_im < 0) = 0;

  case 5 % OPTION 5: uniform weights
    weights_im = jetBW(rows, symAxis:symAxis+max_OPL_radius-1);
    % weights_im(:,1) = 100; % centerline is important
    % weights_im(end,weights_im(end,:)>0) = 100; % nozzle is important

  case 6 % OPTION 6 - similar to op.2, w/o decay downstream
    weights_im = jet_validation_mask;
    w = repmat((1:max_dim)', [1, size(weights_im, 2)]);
    weights_im = weights_im .* w;
    % weights_im(jet_validation_mask==0) = 1;
    weights_im(:,1) = 10*max(weights_im(:));
  otherwise
end
weights_im = weights_im ./ nansum(weights_im(:)); % Normalization

%% Test T_center_nozzle (T0):
%% First,estimate T0 again from several rows above the nozzle (rather than just single pixel):
N_ROWS_ABOVE_NOZZLE = 10; % TODO: replace by f(N_ROWS), e.g. 0.1*N_ROWS ? 
potential_core_line_only_above_nozzle = [1:N_ROWS_ABOVE_NOZZLE; Y(end-N_ROWS_ABOVE_NOZZLE+1:end)']';

jet_boundary_line_bkp = jet_boundary_line;
jet_boundary_line = jet_boundary_line(end-N_ROWS_ABOVE_NOZZLE+1 : end);

T_c_n_vect = linspace(0.9*T_center_nozzle, 1*T_center_nozzle, 10);
cost = zeros(size(T_c_n_vect));
model_params.X_PC = N_ROWS_ABOVE_NOZZLE - ceil(measured_potential_core_length);
model_params.Y_up = nansum(jetBW(end-N_ROWS_ABOVE_NOZZLE+1,symAxis:end));
model_params.Y_nozzle =  D_nozzle_pixels/2;%nansum(jetBW(end,symAxis:end));
model_params.b = -0.95*measured_potential_core_length;
for iTcn = 1 : numel(T_c_n_vect)
  model_params.T0 = C2K(T_c_n_vect(iTcn));
  [R_sim_projected, ~, ~] = get_R_sim(model_params,IGNORE_SELF_ABSORPTION,false);
%   title_str = ['T_r_e_f=',TrefStr,'C: T_n_o_z_z_l_e = ',num2str(T_c_n_vect(iTcn)),'C'];
%   [~, S] = plot_4_result_graphs(R_m(end-N_ROWS_ABOVE_NOZZLE+1:end, 1:size(R_sim_projected,2)), R_sim_projected, model_params, weights_im(end-N_ROWS_ABOVE_NOZZLE+1:end, :), potential_core_line_only_above_nozzle, title_str);
  S = GasHelper.distance_between_images(R_m(end-N_ROWS_ABOVE_NOZZLE+1:end, 1:size(R_sim_projected,2)), R_sim_projected, weights_im(end-N_ROWS_ABOVE_NOZZLE+1:end,:) );
  cost(iTcn) = S.L1;
end
jet_boundary_line = jet_boundary_line_bkp;
model_params_orig.T0 = C2K(T_c_n_vect(cost == min(cost))); % update T0
model_params = model_params_orig;

%% DEBUG performance:
if 0
for ii = 1 : 10
  tic; [R_sim_projected, ~, ~] = get_R_sim_no_self_absorp(model_params); toc;
end
return;
end

%% Implementing a pattern search with the minimal mesh of N+1 directions:
%% In my case, N = 3 so the directions are [1 0 0], [0 1 0], [0 0 1], [-1 -1 -1].

%% cost-function wrappers:
  function [cost] = f(x)
    model_params = set_model_params(x);
    [r_sim, ~, ~] = get_R_sim_no_self_absorp(model_params);
    similarity_val = GasHelper.distance_between_images( R_m(~isnan(r_sim)), r_sim(~isnan(r_sim)), weights_im(~isnan(r_sim)) );
    cost = similarity_val.L1;
  end

  function [cost] = f_self_abs(x)
    model_params = set_model_params(x);
    [r_sim, ~, ~] = get_R_sim(model_params, IGNORE_SELF_ABSORPTION, false);
    similarity_val = GasHelper.distance_between_images( R_m(~isnan(r_sim)), r_sim(~isnan(r_sim)), weights_im(~isnan(r_sim)) );
    cost = similarity_val.L1;
  end


%% Initialization: Initial guess & bounds over the model's parameters:
%% x = [T0, X_PC, Y_PC, b, Y_nozzle, Y_up, C0, r_0_5_nozzle, r_0_5_up]'
tol = 1e-5;
X0 = [model_params.T0;              model_params.X_PC;          model_params.Y_PC; ...
      model_params.b;               model_params.Y_nozzle;      model_params.Y_up; ...
      CO2_mfr/tot_mfr;              model_params.r_0_5_nozzle;  model_params.r_0_5_up];
   
%% Wide bounds:
lb = [max(0.5*X0(1),C2K(50)) ;      X0(2)-N_ROWS;  1; ...
      X0(4)-2*N_ROWS;  0.95*model_params.Y_nozzle; model_params.Y_nozzle; ...
      CO2_mfr/tot_mfr;              0.5*model_params.Y_nozzle;  model_params.Y_nozzle];
    
ub = [min(2*X0(1),C2K(500));        N_ROWS;        1.5*model_params.Y_nozzle; ...
      X0(4)+N_ROWS;    1.05*model_params.Y_nozzle; model_params.Y_up; ...
      CO2_mfr/tot_mfr;              2*model_params.Y_nozzle;    model_params.Y_up];

%% Narrowed bounds, in spirit of meeting with Benny (13/6/17) - much better:
lb = [max(C2K(0.9*K2C(X0(1))), C2K(50)) ;      X0(2);           floor(D_nozzle_pixels/4); ...
      X0(4)-2*N_ROWS;  model_params.Y_nozzle;      model_params.Y_nozzle; ...
      CO2_mfr/tot_mfr;              max(0.5*model_params.Y_nozzle,floor(D_nozzle_pixels/4));  model_params.Y_nozzle];
    
ub = [min(1.1*X0(1),C2K(500));        X0(2);                      1.05*model_params.Y_nozzle; ...
      X0(4)+N_ROWS;    1.05*model_params.Y_nozzle; model_params.Y_up; ...
      CO2_mfr/tot_mfr;              min(2*model_params.Y_nozzle,model_params.Y_up);    model_params.Y_up];
    
%% Even narrower - holding Y_PC constant
lb = [max(C2K(0.9*K2C(X0(1))), C2K(50)) ;      X0(2);           floor(model_params.Y_PC/2); ...
      X0(4)-2*N_ROWS;  model_params.Y_nozzle;      model_params.Y_nozzle; ...
      CO2_mfr/tot_mfr;              max(0.5*model_params.Y_nozzle,floor(D_nozzle_pixels/4));  model_params.Y_nozzle];
    
ub = [min(1.1*X0(1),C2K(500));        X0(2);                     ceil(model_params.Y_nozzle); ...
      X0(4)+N_ROWS;    1.05*model_params.Y_nozzle; model_params.Y_up; ...
      CO2_mfr/tot_mfr;              min(2*model_params.Y_nozzle,model_params.Y_up);    model_params.Y_up];
  
    
%{
    %% Shitty: takes lots of time and the Global Optimization methods are better.
if 0
  tic
  [x,fopt] = GasHelper.gradientDescent(@f, X0, lb, ub);
  toc
%   [R_sim, ~, ~] = get_R_sim(model_params);
  [R_sim, ~, ~] = get_R_sim_no_self_absorp(model_params);
  plot_result(model_params, R_sim, R_m, weights_im, 'Optimal model by Gradient Descent');
end

%% Better:
if 0
% Actual process:
tic
[x, cost_vect, norm_steps_vect, nIters] = GasHelper.my_pattern_Search(X0, @f, lb, ub, tol);
toc
% I get the "optimal" x vector already set in 'model_params'.
figure; plot(norm_steps_vect); title('||step|| vs iteration');
figure; scatter(~isnan(cost_vect).*(1:(nIters-1))', cost_vect, 'x'); title('cost vs iteration');

[R_sim, ~, ~] = get_R_sim(model_params);
plot_result(model_params, R_sim, R_m, weights_im, 'Optimal model - my P.S.');
end
%}

%% x = [T0, X_PC, Y_PC, b, Y_nozzle, Y_up, C0. r_0_5_nozzle, r_0_5_up]'
%% Define the set of linear constraints that satisfies:  Ax <= b
A = [0  1  0  0  0  0  0  0  0; % X_PC     <= N_ROWS
     0  0  1  0 -1  0  0  0  0; % Y_PC     <= Y_nozzle
     0  0  0  0  1 -1  0  0  0; % Y_nozzle <= Y_up
     0  0  1  0  0  0  0 -1  0; % Y_PC <= r_0_5_nozzle
     0  0  0  0 -1  0  0  1  0; % r_0_5_nozzle <= Y_nozzle
     0  0  0  0  0 -1  0  0  1];% r_0_5_up <= Y_up
%      0  1  0  1  0  0  0  0  0];% b        <= -X_PC
b = [N_ROWS; 0; 0; 0; 0; 0];

    
if SEPARATE_CONCENTRAION_JET_PARAMS
  X0 = [X0; model_params.C_Y_PC;  model_params.C_r_0_5_nozzle];
  lb = [lb; X0(10); lb(8)];
  ub = [ub; X0(10); X0(5)];
  A = [A, zeros(size(A,1), 2)];
  A = [A; ...
      [0 0 1 0 0 0 0 0 0 -1 0]; ... % Y_PC <= C_Y_PC
      [0 0 0 0 0 0 0 1 0 0 -1] ]; % r_0_5_nozzle <= C_r_0_5_nozzle
  b = [b; 0; 0];
end


if size(jet_validation_mask,2) ~= size(weights_im,2)
  jet_validation_mask = jet_validation_mask(:,1:size(weights_im,2));
end

%% Solve for the negligible self-absorption case (much faster):
options = optimset('MaxIter',1000,'MaxFunEvals',2000,'TolX',tol);
case_title = 'w/o self-absorption: ';
% options = optimset('MaxIter',500,'MaxFunEvals',1000,'TolX',tol,'PlotFcns',@optimplotx); %DEBUG
tic
[x,fval,exitflag,output ]= fminsearchcon(@f, X0, lb, ub, A, b, [], options);
toc
model_params = set_model_params(x);
[R_sim, T_im, ~] = get_R_sim_no_self_absorp(model_params);
plot_result(model_params, D_nozzle_pixels, R_sim, R_m, weights_im.*jet_validation_mask, ['T_r_e_f=',num2str(C2K(str2num(TrefStr))),'K: Neglecting self-absorption']);
plot_R_simulated_Vs_measured(R_m, R_sim, D_nozzle_pixels, case_title);

% Plot Reconstructed T image:
GasHelper.plotIm(T_im,[case_title,'Reconstructed temperature [K]']);
normalize_axes_half_im(T_im, D_nozzle_pixels, 2, 5);

% model_params = model_params_orig;
% rng default % for reproducibility
% options_sa = optimset('MaxIter',500,'MaxFunEvals',1000,'TolX',tol);
% [x_sa,fval_sa, exitflag_sa,output_sa] =  simulannealbnd(@f_self_abs, X0, lb, ub, options_sa);
% 
% % [R_sim, ~, ~] = get_R_sim_no_self_absorp(model_params);
% model_params = set_model_params(x_sa);
% [R_sim, T_im, ~] = get_R_sim(model_params, IGNORE_SELF_ABSORPTION, false);
% plot_result(model_params, R_sim, R_m, weights_im, 'sa: WITH self-absorption');
% 
% 

%% GlobalSearch with fmincon - Shitty results :(
if 0
  gs = GlobalSearch('StartPointsToRun','bounds-ineqs','MaxTime',60*5,'TolX',tol,'NumTrialPoints',2000,'NumStageOnePoints',500);
  opts = optimoptions(@fmincon,'Algorithm','interior-point');
  problem = createOptimProblem('fmincon','x0',X0,'objective',@f,'Aineq',A,...
    'bineq',b,'lb',lb,'ub',ub,'options',opts);
  rng(7,'twister'); % for reproducibility
  [x_gs,fval_gs,exitflag_gs,output_gs,solutionset_gs] = run(gs,problem);
  f(x_gs);
  model_params = set_model_params(x_gs);
  [R_sim, T_im, ~] = get_R_sim_no_self_absorp(model_params);
  plot_result(model_params, D_nozzle_pixels, R_sim, R_m, weights_im, 'globalSearch, no self-absorption');
end

keyboard;

%% Now, feed 'x' to the model WITH self-absorption:
options_so = optimset('MaxIter',500,'MaxFunEvals',700,'TolX',tol);
case_title = 'w/ self-absorption: ';
tic
[x2,fval2,exitflag2,output2 ]= fminsearchcon(@f_self_abs, X0, lb, ub, A, b, [], options_so);
toc
model_params = set_model_params(x2);
[R_sim, T_im, ~] = get_R_sim(model_params, IGNORE_SELF_ABSORPTION, false);
plot_result(model_params, D_nozzle_pixels, R_sim, R_m, weights_im.*jet_validation_mask, ['T_r_e_f=',num2str(C2K(str2num(TrefStr))),'K: Considering self-absorption']);
plot_R_simulated_Vs_measured(R_m, R_sim, D_nozzle_pixels, case_title);

% Plot Reconstructed T image:
GasHelper.plotIm(T_im, [case_title,'Reconstructed temperature [K]']);
normalize_axes_half_im(T_im, D_nozzle_pixels, 2, 5);

keyboard;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Test Abel inversion: comparing emission (hopefully) radial profile to the generated one:
model_params = model_params_orig;

% cost-function wrapper:
  function [cost] = f2(x)
    model_params.T0 = x(1);
    model_params.X_PC = ceil(x(2));
    model_params.Y_PC = x(3);
    [emission_sim, ~, ~] = get_R_sim(model_params, true, true);
    similarity_val = GasHelper.distance_between_images( R_m_Abel(~isnan(emission_sim)), emission_sim(~isnan(emission_sim)), weights_im(~isnan(emission_sim)) );
    cost = similarity_val.L1;
  end

R_m_Abel = R_est_Abel_Inverted(:, 1:size(R_m, 2));

tol = 1e-9;
options_abel = optimset('MaxIter',500,'MaxFunEvals',2000,'TolX',tol);
case_title = 'Matching to Inverse Able: ';

% TODO: Modify the following
lb = [570 ;            ceil(0.95*X0(2));          floor(D_nozzle_pixels*3/8); ...
      X0(4)-2*N_ROWS;  model_params.Y_nozzle;         model_params.Y_nozzle; ...
      CO2_mfr/tot_mfr; max(0.5*model_params.Y_nozzle,floor(D_nozzle_pixels/4));  model_params.Y_nozzle];

ub = [650;             ceil(1.05*X0(2));          1.05*model_params.Y_nozzle; ...
      X0(4)+N_ROWS;    1.05*model_params.Y_nozzle;        model_params.Y_up; ...
      CO2_mfr/tot_mfr; min(2*model_params.Y_nozzle,model_params.Y_up);    model_params.Y_up];

%{
tic
[x_Abel, cost_vect, norm_steps_vect, nIters] = GasHelper.my_pattern_Search(X0, @f2, lb, ub, tol);
toc
% I get the "optimal" x vector already set in 'model_params'.
figure; plot(norm_steps_vect); title('Abel: ||step|| vs iteration');
figure; scatter(~isnan(cost_vect).*(1:(nIters-1))', cost_vect, 'x'); title('Abel: cost vs iteration');
%}
tic
[x_Abel,fval_abel,exitflag_abel,output_abel ]= fminsearchcon(@f2, X0, lb, ub, A, b, [], options_abel);
toc
model_params = set_model_params(x_Abel);
[R_sim, T_im, ~] = get_R_sim(model_params, true, true);
plot_result(model_params, D_nozzle_pixels, R_sim, R_m_Abel, weights_im.*jet_validation_mask, 'Inv. Abel: Optimal model');
% Plot Reconstructed T image:
GasHelper.plotIm(T_im, [case_title,'Reconstructed temperature [K]']);
normalize_axes_half_im(T_im, D_nozzle_pixels, 2, 5);

keyboard;

%% Using the 'Simulated Annealing' method:
model_params = model_params_orig;
rng default % for reproducibility
options = optimset('MaxIter',1000,'MaxFunEvals',2000,'TolX',tol);
[x_Abel_sa,fval_sa, exitflag_sa,output_sa] =  simulannealbnd(@f2,X0,lb,ub,options);

model_params = set_model_params(x_Abel_sa);
[R_sim, ~, ~] = get_R_sim(model_params, true, true);
plot_result(model_params, D_nozzle_pixels, R_sim, R_m_Abel, weights_im, 'Abel_sa: Optimal model');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


return

%% Save the updated DataBase:
tic
save((fullfile(pwd,R_DB_file_name)),'R_DB_struct');
% save(fullfile(pwd,R_DB_file_name),'R_DB_struct','-v7.3'); % Save in version compatible with efficient partial loading/saving.
toc
end

%_______________________________________________________________________________

function [hF] = plot_result(model_params, D_nozzle_pixels, R_simulated, R_measured, weights_im, title_str)
  N_ROWS = size(R_simulated,1);
  PC_slope = model_params.Y_PC/(N_ROWS-model_params.X_PC); % Potential core slope
  Y_PC_up = model_params.Y_PC - PC_slope*N_ROWS;
  Y = linspace(Y_PC_up, model_params.Y_PC, N_ROWS);
  potential_core_line = [(1:N_ROWS)', Y(:)];
  hF = plot_4_result_graphs(R_measured, R_simulated, model_params, D_nozzle_pixels, weights_im, potential_core_line, title_str);
end


function [hF, similarity_values] = plot_4_result_graphs(R_m, R_sim, model_params, D_nozzle_pixels, weights_im, potential_core_line, title_str)
  if ~exist('weights_im', 'var')
    weights_im = [];
  end
  if ~exist('title_str', 'var')
    title_str = [];
  end
  plot_potential_core = false;
  if exist('potential_core_line', 'var')
    plot_potential_core = true;
  end
  N_ROWS = size(R_sim,1);
  % Calc evaluation metrics:
  similarity_values = GasHelper.distance_between_images( R_m(~isnan(R_sim)), R_sim(~isnan(R_sim)), weights_im(~isnan(R_sim)) );
  tv_norm = GasHelper.tvnorm( abs(R_m-R_sim) );
  % Normalization:
  if isempty(weights_im)
    nElements = numel(R_m(~isnan(R_sim)));
  else
    nElements = nnz(weights_im);
  end
  tv_norm = tv_norm ./ nElements;
  similarity_values.L1 = similarity_values.L1 ./ nElements;
  similarity_values.L2 = similarity_values.L2 ./ nElements;
  % Plot intermediate results:
%   eval_str = ['||_L_1 =',num2str(similarity_values.L1),'  ||_L_2 =',num2str(similarity_values.L2),',  ||_t_v =',num2str(tv_norm)];
  eval_str = ['||error||_1 =',num2str(similarity_values.L1)];  
% Plotting
  hF = figure();
  if ~isempty(title_str)
    suptitle(title_str);
  end
  rel_error_im = 100*abs((R_m-R_sim)./R_m);
  rel_error_im(~weights_im) = NaN;
  subplot(2,2,[1 3]); 
  imagesc(rel_error_im);
%   ax = gca;
%   ax.YDir = 'normal';
  yt = fliplr(size(rel_error_im,1):-floor(D_nozzle_pixels/2):1);
  ytl = strsplit(num2str((numel(yt)-1:-1:0)/2), ' ');
  set(gca,'ytick',yt);
  set(gca,'yticklabel',ytl);
  xt = strsplit(num2str(linspace(0, ceil(size(rel_error_im,2)/D_nozzle_pixels), 5)), ' ');
  set(gca,'xtick',1+linspace(0, ceil(size(rel_error_im,2)/D_nozzle_pixels), 5)*D_nozzle_pixels );
  set(gca,'xticklabel',xt);
  colorbar; caxis([0 100]); impixelinfo; axis image;
  title(['Error in R [%].     ',eval_str]);
  if plot_potential_core
    potential_core_line(potential_core_line < 0) = NaN;
    hold on; plot(potential_core_line(:,2), potential_core_line(:,1),'r');    
  end
  plot(linspace(model_params.r_0_5_up,model_params.r_0_5_nozzle, N_ROWS), 1:N_ROWS,'m');
  xlabel('y/D'); ylabel('x/D');
  
  subplot(2,2,2); plot((1:numel(R_m(end,:)))/D_nozzle_pixels, R_m(end,:),'DisplayName','measured'); hold on;
  plot((1:numel(R_sim(end,:)))/D_nozzle_pixels, R_sim(end,:),'DisplayName','simulated');
  title('R comparison @ orifice'); xlabel('y/D'); ylabel('R'); 
  legend('-DynamicLegend', 'Location','northeast');
  subplot(2,2,4); plot((1:numel(R_m(:,1)))/D_nozzle_pixels, flipud(R_m(:,1)),'DisplayName','measured'); xlabel('x/D'); ylabel('R'); hold on;
  plot((1:numel(R_sim(:,1)))/D_nozzle_pixels, flipud(R_sim(:,1)),'DisplayName','simulated');
  title('R comparison @ Centerline');
  legend('-DynamicLegend', 'Location','southeast');

  GasHelper.maximizeFigure;
end


function [hF, similarity_values] = plot_4_result_graphs_verbose(R_m, R_sim, model_params, weights_im, potential_core_line , title_str)
  if ~exist('weights_im', 'var')
    weights_im = [];
  end
  if ~exist('title_str', 'var')
    title_str = [];
  end
  plot_potential_core = false;
  if exist('potential_core_line', 'var')
    plot_potential_core = true;
  end
  N_ROWS = size(R_sim,1);
  % Calc evaluation metrics:
  similarity_values = GasHelper.distance_between_images( R_m(~isnan(R_sim)), R_sim(~isnan(R_sim)), weights_im(~isnan(R_sim)) );
  tv_norm = GasHelper.tvnorm( abs(R_m-R_sim) );
  % Normalization:
  if isempty(weights_im)
    nElements = numel(R_m(~isnan(R_sim)));
  else
    nElements = nnz(weights_im);
  end
  tv_norm = tv_norm ./ nElements;
  similarity_values.L1 = similarity_values.L1 ./ nElements;
  similarity_values.L2 = similarity_values.L2 ./ nElements;
  % Plot intermediate results:
%   eval_str = ['||_L_1 =',num2str(similarity_values.L1),'  ||_L_2 =',num2str(similarity_values.L2),',  ||_t_v =',num2str(tv_norm)];
  eval_str = ['||_L_1 =',num2str(similarity_values.L1)];  
% Plotting
  hF = figure();
  if ~isempty(title_str)
    suptitle(title_str);
  end
  rel_error_im = 100*abs((R_m-R_sim)./R_m);
  rel_error_im(~weights_im) = NaN;
  subplot(2,2,1); imagesc(rel_error_im); colorbar; caxis([0 100]); impixelinfo; axis image;
  hold on; plot(1:size(R_m,2), floor(size(R_m,1)/2)*ones(1,size(R_m,2)), '.m','MarkerSize',3); hold off;
  title(['Error in R [%]']);
  if plot_potential_core
    potential_core_line(potential_core_line < 0) = NaN;
    hold on; plot(potential_core_line(:,2), potential_core_line(:,1),'r');
  end
  plot(linspace(model_params.r_0_5_up,model_params.r_0_5_nozzle, N_ROWS), 1:N_ROWS,'m');
  xlabel(eval_str);
  subplot(2,2,2); plot(R_m(floor(end/2),:),'DisplayName','measured'); xlabel('column'); ylabel('R'); hold on;
  plot(R_sim(floor(end/2),:),'DisplayName','simulated');
  title('R comparison @ half row');
  legend('-DynamicLegend', 'Location','northeast');
  subplot(2,2,3); plot(R_m(:,1),'DisplayName','measured'); xlabel('row'); ylabel('R'); hold on;
  plot(R_sim(:,1),'DisplayName','simulated');
  title('R comparison @ Centerline');
  legend('-DynamicLegend', 'Location','southeast');
  subplot(2,2,4); plot(R_m(end,:),'DisplayName','measured'); hold on;
  plot(R_sim(end,:),'DisplayName','simulated');
  title('R comparison @ Nozzle'); xlabel('column'); ylabel('R');
  legend('-DynamicLegend', 'Location','northeast');
  GasHelper.maximizeFigure;

  if exist('model_params', 'var')
    % Print the table of model parameters:
    % https://www.mathworks.com/matlabcentral/answers/254690-how-can-i-display-a-matlab-table-in-a-figure
    names = fieldnames(model_params);
    vals = zeros(size(names));
    for iF = 1 : numel(names)
      vals(iF) = getfield(model_params, names{iF});
    end
    vals(1) = vals(1); % T[K]
    T = table(vals, 'RowNames', names); % Generate a table
    % Get the table in string form.
    TString = evalc('disp(T)');
    % Use TeX Markup for bold formatting and underscores.
    TString = strrep(TString,'<strong>','\bf');
    TString = strrep(TString,'</strong>','\rm');
    TString = strrep(TString,'_','\_');
    % Get a fixed-width font.
    FixedWidth = get(0,'FixedWidthFontName');
    annotation(gcf,'Textbox','LineStyle','none','String',TString,'Interpreter','Tex',...
      'FontName',FixedWidth,'Units','Normalized','Position',[0 0.9 0.1 0.1]);
  end
end

% Faster function w/o self-absorption
function [R_sim_projected, T_im, C_im] = get_R_sim_no_self_absorp_new(model_params, doNotProjectForward, atm_params, jet_boundary_line, jetParams, F_R, geometry, calibParams, symAxis, A_array, tau_avg)
  %Generate T & C images:
  [T_im, C_im] = GasHelper.gen_T_C_images_from_model(model_params, atm_params, numel(jet_boundary_line), jet_boundary_line); % ~0.04 sec
  R_sim_projected = nan(size(T_im));
  %Calculate profile's emission:
  emission_sim_im = F_R(T_im(:), C_im(:), jetParams.OPL*ones(numel(T_im),1));% Interpolate R DB
  % TODO: WRONG calibration indices!!!!!
  emission_sim_im =  geometry .* calibParams.K(1:size(T_im,1),symAxis:symAxis+size(T_im,2)-1) .* reshape(emission_sim_im, size(T_im,1),[]);
  % Project the profiles to the camera's domain if needed:
  for iRow = 1 : size(T_im, 1)% Loop over rows:
    r = jet_boundary_line(iRow);
    % Estimating the measured radiation using the T,C models:
    if doNotProjectForward % Set R_sim_projected==emission_sim:
      R_sim_projected(iRow, 1:r) = emission_sim_im(iRow,1:r);
    else % Otherwise, Apply Forward Projection:
      R_sim_projected(iRow, 1:r) = GasHelper.forward_onion_peeling_no_self_absorp(emission_sim_im(iRow,1:r), A_array{iRow});
    end
  end
  % Approximate path's transmission as constant:
  R_sim_projected = R_sim_projected ./ tau_avg;
end

% Generate R image with self-absorption within the jet:
function [R_sim_projected, T_im, C_im] = get_R_sim_new(model_params, doNotProjectForward, atm_params, jet_boundary_line, jetParams, F_R, F_TAU, geometry, calibParams, symAxis, A_array, tau_avg)
  %Generate T & C images:
  [T_im, C_im] = GasHelper.gen_T_C_images_from_model(model_params, atm_params, numel(jet_boundary_line), jet_boundary_line); % ~0.04 sec
  R_sim_projected = nan(size(T_im));
  %Calculate profile's emission:
  emission_sim_im = F_R(T_im(:), C_im(:), jetParams.OPL*ones(numel(T_im),1));% Interpolate R DB
  % TODO: WRONG calibration indices!!!!!
  emission_sim_im =  geometry .* calibParams.K(1:size(T_im,1),symAxis:symAxis+size(T_im,2)-1) .* reshape(emission_sim_im, size(T_im,1),[]);
  %Calculate profile's transmission:
  tau_sim_im = F_TAU(T_im(:), C_im(:), jetParams.OPL*ones(numel(T_im),1));% interpolate TAU DB
  tau_sim_im = reshape(tau_sim_im, size(T_im,1),[]);
  % Project the profiles to the camera's domain if needed:
  for iRow = 1 : size(T_im, 1)% Loop over rows:
    r = jet_boundary_line(iRow);
    % Estimating the measured radiation using the T,C models:
    if doNotProjectForward % Set R_sim_projected==emission_sim:
      R_sim_projected(iRow, 1:r) = emission_sim_im(iRow,1:r);
    else % Otherwise, Apply Forward Projection:
      R_sim_projected(iRow, 1:r) = GasHelper.forward_onion_peeling(emission_sim_im(iRow,1:r), tau_sim_im(iRow,1:r), A_array{iRow});
    end
  end
  % Approximate path's transmission as constant:
  R_sim_projected = R_sim_projected ./ tau_avg;
end

function model_params = set_model_params(x)
% Gets a vector 'x' and assign model parameters accordingly:
% x === [T0, X_PC, Y_PC, b, Y_nozzle, Y_up, C0. r_0_5_nozzle, r_0_5_up]'
  model_params.T0 = x(1);
  model_params.X_PC = ceil(x(2));
  model_params.Y_PC = x(3);
  model_params.b = ceil(x(4));
  model_params.Y_nozzle = x(5);
  model_params.Y_up = x(6);
  model_params.C0 = x(7);
  model_params.r_0_5_nozzle = x(8);
  model_params.r_0_5_up= x(9);
end

function model_params = set_model_params_seperated(x)
% Gets a vector 'x' and assign model parameters accordingly:
% x === [T0, X_PC, Y_PC, b, Y_nozzle, Y_up, C0. r_0_5_nozzle, r_0_5_up, C_Y_PC, C_r_0_5_nozzle]'
  model_params.T0 = x(1);
  model_params.X_PC = ceil(x(2));
  model_params.Y_PC = x(3);
  model_params.b = ceil(x(4));
  model_params.Y_nozzle = x(5);
  model_params.Y_up = x(6);
  model_params.C0 = x(7);
  model_params.r_0_5_nozzle = x(8);
  model_params.r_0_5_up = x(9);
  model_params.C_Y_PC = x(10);
  model_params.C_r_0_5_nozzle = x(11);
end


function h = plot_R_simulated_Vs_measured(R_m, R_sim, D_nozzle_pixels,title)
  im = [fliplr(R_sim) R_m];
  symAxis = size(R_sim,2)+1;
  h = GasHelper.plotIm(im, 'R simulated (left) Vs R measured (right)');
  if nargin > 3
    suptitle(title);
  end
  normalize_axes_full_im(im,D_nozzle_pixels, symAxis, 2, 2);
end

function normalize_axes_full_im(im,D_nozzle_pixels, symAxis, vertical_tick_factor, lateral_tick_factor)
  yt = fliplr(size(im,1):-floor(D_nozzle_pixels/vertical_tick_factor):1);
  ytl = strsplit(num2str((numel(yt)-1:-1:0)/vertical_tick_factor), ' ');
  set(gca,'ytick',yt);
  set(gca,'yticklabel',ytl);
  xt = [fliplr(symAxis-floor(D_nozzle_pixels/lateral_tick_factor):-floor(D_nozzle_pixels/lateral_tick_factor):1) symAxis:floor(D_nozzle_pixels/lateral_tick_factor):size(im,2)];
  xtl = strsplit(num2str( (-floor(numel(xt)/2) : floor(numel(xt)/2) ) / lateral_tick_factor ), ' ');
  set(gca,'xtick',xt );
  set(gca,'xticklabel',xtl);
  xlabel('y/D'); ylabel('x/D');
end


function normalize_axes_half_im(im, D_nozzle_pixels, vertical_tick_factor, n_lateral_ticks)
  yt = fliplr(size(im,1):-floor(D_nozzle_pixels/vertical_tick_factor):1);
  ytl = strsplit(num2str((numel(yt)-1:-1:0)/vertical_tick_factor), ' ');
  set(gca,'ytick',yt);
  set(gca,'yticklabel',ytl);
  xt = strsplit(num2str(linspace(0, ceil(size(im,2)/D_nozzle_pixels), n_lateral_ticks)), ' ');
  set(gca,'xtick',1+linspace(0, ceil(size(im,2)/D_nozzle_pixels), n_lateral_ticks)*D_nozzle_pixels );
  set(gca,'xticklabel',xt);
  xlabel('y/D'); ylabel('x/D');
end