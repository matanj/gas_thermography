%% Use per-pixel Calibration matrix to retrieve the Jet temperature
%%
% *Synopsis*
%%
%  Apply Calibratoin data on measurements of CO2-Air Jet taken with
%  specific filter(s)

function JetFindTempUsingCalibrationMatrix_R_P
clc; close all;
dbstop if error
format short
%% Assumptions: 1) Order experiment of BGs->IMs -> BGs -> ...
%%              2) unique experiment cases.
%%              3) each experiment case has same #files.
%%              4) All images are of same size (row, cols)
%%              5) New images to be used with the caibration should match its image's size AND PIXEL ALLIGNMENT!

%% OPTIONS
DEBUG = false; % show figures;
CALC_CALIB_PARAMS = true; % if false, use existing set for testing
FILTER_PARAMETERS_TH = 0; % percentile-based filtering over calculated fit parameters. '0' for no filtering
TEST_CALIBRATION_ON_EXTERNAL_CASE = true;
ALL_PIXELS_SEE_BB = true; % for calibrating only a subset of the image
FIX_JET_ANGLE = false; % fix jet's alignment to be vertical
APPLY_ABEL = false; % apply abel transformation. % TODODODODO: I do it wrong - on DL rather than on R!!
nLevels = 30; % otsu's number of levels for label the jet image, for radius estimation
D_nozzle = 1e-2; % Nozzle's diameter [m]
D_nozzle_pixels = 60;%[pixels]
m2pixel = D_nozzle_pixels / D_nozzle; % meter to pixel conversion
NOZZLE_IN_IMAGE = true; % if the images contain the nozzle, select it and get m2pixel, etc.
bSize = 10; % Block size [pixels] 

tot_mfr = 2; %[g/s]
CO2_mfr = 2; %[g/s]

CalibFilePath = [pwd,'\pixelwise_calibration\JetBBCalibration_OUTPUT_No_ignored_ITs.mat']; % default
outputName = 'CalibrationParams.mat';
% Default test case:
baseDir = 'Z:\FLIR_Experimental\10-07-16_Jet_\';
imName = [baseDir, 'G001_T500_F4370_IT2693.6_IM_f.ptw'];
bgName = [baseDir, 'G002_T500_F4370_IT2693.6_DF_f.ptw'];
%%
h = 6.62606957e-34; %[J*s]
c = 299792458; %[m/s]
Kb = 1.3806488e-23; %[J/K]
C2 = h*c/Kb;
% C1 = 2*h*c^2;
C2K = @(x) x + 273.15; % Celsius to Kelvin
K2C = @(x) x - 273.15; % Kelvin to Celsius

addpath(genpath('Z:\Matan\'), '-end');

if CALC_CALIB_PARAMS % conditionally calculate calibration parameters:
  %% Get data:
  [CalibFileName, CalibFilePath] = uigetfile({'*.mat','MATLAB data'},'Select Calibration matrix',CalibFilePath);
  if isempty(CalibFileName)
    error('invalid folder path, or claibration matrix there...');
  end
  load([CalibFilePath, CalibFileName]);
  
  ITs = experimentCasesArr.IT;
  ITs = squeeze(ITs); % TODO: remove & fix for more than 1 color
  DL = squeeze(DL); % TODO: check as above
  
  %Rotrou 2006: NIR thermography with silicon FPA, [17] & DL = R*IT^P:
  % For each pixel we find the following 4 params:
  %   Kw, a0, a1, p, by this fit:
  %  ln(R) = ln(DL) - P*ln(IT) = ln(Kw) - C2./T/.lambda_x =~  ln(Kw) - C2.*(a0/T + a1/T^2 + ...)
  
  %%
  % Problems:
  % 1) Need to use Plate BB for this calibration - all pixels should see the
  % same radiation
  % 2) No use of multiple ITs
  % 3) No radiation calibration - it is for DL...
  % 4) Seems that the model is Weinish.  but my range doesn't hold Wein
  % assumption!!!!!!!!!!!!!!!!!
  
  T = C2K(experimentCasesArr.temps); % T[K]
  X = max(T)./T; % the independant variable. scaled to ease estimation.
  
  [~, ~, nX, nY] = size(DL);
  
  % Prealloc: parameter matrices
  Kw = nan(nX, nY);
  a0 = nan(nX, nY);
  a1 = nan(nX, nY);
  p  = nan(nX, nY);
  
  progressbar('rows','cols')
  %% Better way:
  % Formulate as a M*P = Y system , where P is the desired parameters vector.
  % We have: ln(DL) =  ln(Kw)-C2*(a0/T + a1/T^2)
  % Denote Y = ln(DL), X = 1/T. We write:
  % Y = ln(Kw) - a0*X + a1*X.^2.
  % Denote: p3 = log(Kw), p2 = -C2*a0, p1 = -C2*a1,  we get:
  % (*) Y = p3 + p2*X + p1*X^2 ======>    Y = [X.^2 X ones(size(X))]*P
  % =====> P  =  M \ Y
  
  % Using (*), we can afterward solve for T :
  % X1,2 = (-p2 +- sqrt(p2^2 - 4*p1*(p3-Y)))/2/p1
  % ===> T = 1/X = 2*p1 ./ (-p2 +- sqrt(p2^2 - 4*p1*(p3-ln(DL_n))))
  
  % And if the object is of known "extended effective emmisivity" (my term),
  % we have: ln(DL) =  ln(Kw) - ln(e) -C2*(a0/T + a1/T^2)
  % that is, (**) Y = p3 - ln(e) + p2*X + p1*X^2
  % solving we get: X1,2 = (-p2 +- sqrt(p2^2 - 4*p1*(p3-ln(e)-Y)))/2/p1
  
  % preparation - make vectors from matrix and apply log to get simple system Y = M * P
  Y = log(DL); % measurement - log of DL
  its = log(ITs)'; its = its(:);
  X = repmat(X', [size(DL,2), 1]); X = X(:);
  %%%%%% TODO: I assume here for simplicity that for a given pixel, p is constant!!
  for i = 1: nX
    for j = 1 : nY
      if ~any(isnan(DL(:,:,i,j)))
        y = Y(:,:,i,j)';
        P = [X.^2 X ones(numel(its),1) its] \ y(:); % M is probably ill conditioned!!
        p(i,j)  = P(4);
        Kw(i,j) = P(3);
        a0(i,j) = P(2);
        a1(i,j) = P(1);
      end
      progressbar(i/nX, j/nY);
    end
    progressbar(i/nX, 0);
  end
  assert(all(isreal(p)) && all(isreal(Kw)) && all(isreal(a0)) && all(isreal(a1)));
  
  % Save coefficients per pixel:
  save(outputName, 'p', 'Kw', 'a0','a1', 'T');
  
  %   TODO: try X = 1/lambda/T for my known lambda, assuming effective
  %   lambda doesn't change in my IR range.

else
  [ParamsFileName, ParamsFilePath] = uigetfile({'*.mat','MATLAB data'},'Select Calibration parameters set',[pwd,'\CalibrationParams.mat']);
  if isempty(ParamsFileName)
    error('invalid folder path...');
  end
  load([ParamsFilePath, ParamsFileName]);
end

%% Filter results by percentile:
if FILTER_PARAMETERS_TH > 0
  Kw = filterPrctile(Kw, FILTER_PARAMETERS_TH);% Kw(Kw < prctile(Kw(:), p_th) | Kw > prctile(Kw(:), 100-p_th)) = nan;
  a0 = filterPrctile(a0, FILTER_PARAMETERS_TH);
  a1 = filterPrctile(a1, FILTER_PARAMETERS_TH);
  totNaNMask = isnan(Kw) & isnan(a0) & isnan(a1);
  
  Kw(totNaNMask) = nan;
  a1(totNaNMask) = nan;
  a0(totNaNMask) = nan;
  
  if DEBUG
    figure;
    subplot(2,3,1);  imagesc(Kw); colorbar; title('Kw');
    subplot(2,3,2); imagesc(a0); colorbar; title('a0');
    subplot(2,3,3); imagesc(a1); colorbar; title('a1');
    subplot(2,3,4); imagesc(~isnan(Kw)); colorbar; title('"Valid" Kw');
    subplot(2,3,5); imagesc(~isnan(a0)); colorbar; title('"Valid" a0');
    subplot(2,3,6); imagesc(~isnan(a1)); colorbar; title('"Valid" a1');
    figure; imagesc(totNaNMask); colorbar; title('total mask');
  end
end

%% Test the calibration:
if TEST_CALIBRATION_ON_EXTERNAL_CASE
  %Get test case:
  [imName, baseDir] = uigetfile({'*.ptw','FLIR images'},'Select image to test calibration',imName);
  if isempty(imName)
    error('invalid image');
  end
  [bgName, baseDir] = uigetfile({'*.ptw','FLIR images'},'Select bg to test calibration',bgName);
  if isempty(bgName)
    error('invalid bg');
  end
  
  imPair = ImageBGPairExt([baseDir,imName]);
  imPair.setBG([baseDir,bgName]);
  subIm = mean(abs(imPair.getSubtracted()), 3);
  if ALL_PIXELS_SEE_BB
    objMask = true(size(subIm));
  else
    [~, object] = processImGetStats(subIm);%get meaningful object within the image
    objMask = true(imPair.IMG_Info.m_rows, imPair.IMG_Info.m_cols);%initial mask
    objMask(isnan(object)) = NaN;
  end
  
  %mask the image to maintain same size:
  DL_new = objMask.*subIm;
  % Normalize DLs map by IT:
  IT = double(imPair.IT)*1e6; %change IT to [us]
  DL_new(DL_new==0) = 1; % set minimal DL to 1 to avoid zero issues
  DL_new_n = DL_new ./ IT;
  
  %trim image to orifice
  if NOZZLE_IN_IMAGE
    figure; imagesc(subIm); colorbar; title('Select nozzle''s orifice:');
    h_nozzle = imdistline();
    pause;
    apiObj = iptgetapi(h_nozzle);
    teta = apiObj.getAngleFromHorizontal() %[deg]
    D_nozzle_pixels = ceil(apiObj.getDistance());%[pixels]
    m2pixel = D_nozzle_pixels / D_nozzle;
    pos_nozzle = apiObj.getPosition();
    DL_new_n = DL_new_n(1:floor(pos_nozzle(1,2)), :); %TODO: assure this OK
  end
  
  if FIX_JET_ANGLE
    %% Apply 2D affine rotating transformation to fix for the angle of the Jet.
    % Radon transformation is used estimate angle.
    %% TODO: should apply same to the calibration maps, or rotate back b4 using the calibration
    % NOTE: I trim the new image to the original size. 'theta' is assumed
    % small enough so that the skew is negligable.
%     DL_new_n_otsu = otsu(DL_new_n,10); % apply otsu thersholding
%     theta = 90 - DetectAngle(DL_new_n_otsu);
    S = 1; % scale factor
    theta =  90 - DetectAngle(DL_new_n); % '90' as desired jet is vertical
    tform = affine2d([S.*cosd(theta) -S.*sind(theta) 0; S.*sind(theta) S.*cosd(theta) 0; 0 0 1]);
    fixed_DL_new_n = imwarp(DL_new_n,tform,'fillvalues',0); %default is '0'. 

%     % DEBUG: check that x' = A^-1(A(x)) = x : 
%     tform2 = affine2d([S.*cosd(-theta) -S.*sind(-theta) 0; S.*sind(-theta) S.*cosd(-theta) 0; 0 0 1]);
%     DL_new_n_rec = imwarp(fixed_DL_new_n,tform2);
    

%     f2 = imrotate(DL_new_n,theta); Obselete!!
    if DEBUG
      figure; imagesc(DL_new_n); colorbar; title('original');
%       figure; imagesc(fixed_DL_new_n_otsu); colorbar; title('rotation: angle detected on otsu');
      figure; imagesc(fixed_DL_new_n); colorbar; title('rotation: angle detected on original');
    end
    fixed_DL_new_n(fixed_DL_new_n==0) = nan;% mask the rotation transformation by-product
    mask = ~isnan(fixed_DL_new_n);
    % get the inner image w/o transformation's by-products:
    % need to distinguish between angles signs since the inner rectangle
    % usually intersects each of the outer one's limits by more that one
    % pixel.
    if theta < 0 % rotate CW
      idx_row1 = find(mask(:,1),1,'last');
      idx_row2 = find(mask(:,end),1,'first');
      idx_col1 = find(mask(1,:),1,'first');
      idx_col2 = find(mask(end,:),1,'last');
    else % rotated CCW
      idx_row1 = find(mask(:,1),1,'first');
      idx_row2 = find(mask(:,end),1,'last');
      idx_col1 = find(mask(1,:),1,'last');
      idx_col2 = find(mask(end,:),1,'first');
    end
    min_row = min(idx_row1, idx_row2);
    max_row = max(idx_row1, idx_row2);
    min_col = min(idx_col1, idx_col2);
    max_col = max(idx_col1, idx_col2);
    DL_new_n = fixed_DL_new_n(min_row:max_row, min_col:max_col);
  end
  
  if 0
    %% Shift the jet horizontally to the center: %TODO: remove, I shouldn't do it!!
    [~,idxM] = max(DL_new_n(end,:));
    ls = idxM;
    rs = size(DL_new_n,2)-idxM;
    if ls > rs % shift left
      DL_new_n = DL_new_n(:,ls-rs:end);
    else
      DL_new_n = DL_new_n(:,1:end-(ls-rs));
    end
  end

  if APPLY_ABEL
    % Abel inversion code parameters
    upf = 10;
    plot_results = 0;
    lsq_solve = 0;
    inPixels = 1;
    normalize = 0;  % normalize plots
    normBy2R = 1;   % normalize measurements plot by 2R
    
    %% Calc the symmetry axis
    fig = figure; imagesc(DL_new_n); impixelinfo; title('Select area, then confirm by double clicking');
    [lines, rect] = imcrop;
    %round coordinates, as they are fractions:
    rect(1:2) = ceil(rect(1:2));
    rect(3:4) = floor(rect(3:4));
    [M,N] = size(lines);
    [~,Idx] = max(lines,[],2);
    rows = (rect(2):rect(2)+rect(4))';
    cols = rect(1)+Idx-1;
    avgCol = round(mean(cols));
    % Just for plot purpose:
    imAxisMarked = DL_new_n;
    imAxisMarked(rows,avgCol) = min(DL_new_n(:));
    figure(fig); imagesc(imAxisMarked);impixelinfo;
    
    % Apply Abel inversion per row
    new_lines = zeros(size(lines));
    jetMaskOtsu = otsu(lines,nLevels);
    jetMask = true(size(lines));
    jetMask(jetMaskOtsu == 1) = false;
    if DEBUG
      figure; imagesc(jetMask); colorbar;
    end
    
    for l=1:M % loop on each row and perform abel inversion
      line = lines(l,:);
      [~,Idx] = max(line,[],2);
      %estimate diameter of the jet:
%       D = numel(jetMask(l,:) > 0);
      ll = find(jetMask(l,:)>0, 1, 'first');
      rl = find(jetMask(l,:)>0, 1, 'last');
      for ii = 1:2 % loop on each side of the jet (for verification)
        if ii == 2 % left side
          R = Idx-ll+1;
          f_in = flipud(line(ll:Idx)');
          % The actual inversion:
          [ f_rec , X ] = abel_inversion(f_in,R,upf,plot_results,lsq_solve,inPixels);
          new_lines(l,ll:Idx-1) =  flipud(f_rec(2:end));
        else % right side
          R = rl-Idx+1; % radius
          f_in = line(Idx:rl)';
          % The actual inversion:
          [ f_rec , X ] = abel_inversion(f_in,R,upf,plot_results,lsq_solve,inPixels);
          new_lines(l,Idx:rl) = f_rec;
        end
      end
    end
    DL_new_n = new_lines; % set the new image after abel inversion
    DL_new_n(DL_new_n==0) = nan;
    figure; imagesc(DL_new_n); colorbar;
  end

  %% Apply calibration with Kw, a0, a1 maps.
  % First, resize object image to the calibration image, if needed, or
  % choose an area of interest:
  if ~(exist('rect','var') == 1) % if cropping wasn't done:
    [rowsCalib, colsCalib] = size(Kw);
    [rowsNew, colsNew] = size(DL_new_n);
    if rowsNew ~= rowsCalib || colsNew ~= colsCalib
%       % Size of data image does not match size of calibration images,
%       % so resize the first to match the latter:
%       DL_new_n = imresize(DL_new_n, [rowsCalib colsCalib]);
      rows = 1:floor(pos_nozzle(1,2));
      cols = 1:size(DL_new_n,2);
    end
  else % if 'rect' is present:
    rows = rect(2):rect(2)+rect(4);
    cols = rect(1):rect(1)+rect(3);
  end
  Kw = Kw(rows, cols);
  a0 = a0(rows, cols);
  a1 = a1(rows, cols);
   
  totNaNMask = isnan(Kw) | isnan(a0) | isnan(a1);
  DL_new_n(totNaNMask) = nan;
  
  %% Get the "effective emissivity" from HITRAN:
  % 1) TODO - I don't know my temperature, so I cant get the correct emissivity.
  % Need to iterate over T, until convergance...
  % 2) This might be the place to combine my model for the jet, for some
  % spatial resolution
  
  T0 = C2K(200); % initial guess on centerline temperature
  
  wl_l = 4.1; %[um]
  wl_h = 4.4; %[um]
  
  wn_h = 1e4/wl_l; %[cm^-1]
  wn_l = 1e4/wl_h; %[cm^-1]
  
  %  emissivity = getEmissivityFromHITRAN(c0, T0, wl_range); % BELOW
  absorp_file = 'C:\Users\matanj\Documents\PythonScripts\Jet_absorption_spectrum_T_120C_OPL  1cm.txt';
  [absorp_file, absorp_file_path] = uigetfile({'*.txt','HAPI generated .txt files'},'Select input HAPI file',absorp_file);
  absorp_file = [absorp_file_path absorp_file];
  
  fileID = fopen(absorp_file,'r');
  HITRAN_file_format = '%f %f';
  HITRAN_data = textscan(fileID, HITRAN_file_format);
  fclose(fileID);
  wnGrid = HITRAN_data{1};%[cm-1]
  absorp_spectrum_wn = HITRAN_data{2};
  absorp_spectrum_wl = flipud(absorp_spectrum_wn); %No need to fix absorpution as in intensity, as it is not "per unit of wn/wl"
  wnArray=flipud(wnGrid);
  WL = wnArray.\1E4; % [cm-1] to [um]
  
%   e_eff = getEffectiveEmisssivity(wl_range,emissivity); %TODO
%   e_eff = sum(absorp_spectrum_wl).*1e-6./(WL(end)-WL(1));
  planck_wn = 2e8*h*c^2*wnArray.^3 ./ (exp(100*h*c*wnArray./Kb./T0)-1);
  e_eff = trapz(absorp_spectrum_wn.*planck_wn) ./ trapz(planck_wn);
% e_eff = 1e-4;

  % solve the quadratic equation Y = p3-ln(e) + p2*X + p1*X^2, for X = 1/T:
  x1 = (-a0 + sqrt(a0.^2 - 4*a1.*(Kw+log(e_eff)-log(DL_new_n))))./2./a1;
  x2 = (-a0 - sqrt(a0.^2 - 4*a1.*(Kw+log(e_eff)-log(DL_new_n))))./2./a1;
  tol = 1e-10;
  assert(all(x1(~isnan(x1)))>tol && all(x2(~isnan(x2)))>tol && isreal(x1) && isreal(x2));
  
  
  T_rec = nan(size(DL_new_n));
  % Get temperature map: take only T>0
  T_rec(x1>0) = 1./x1(x1>0);
  T_rec(x1<0) = 1./x2(x1<0);
  
  T_rec = T_rec * max(T); % since we have normalized by max(T) to improve the fit process
  
  matchStr = regexp(imName,'_T+[0-9]+[0-9]+[0-9]','match');
  tempStr = regexprep(matchStr{1},'_T','');
  Tref = str2double(tempStr);
  titleStr = ['Reconstructed T[^oC], T_{ref}=',num2str(Tref),'[^oC]'];
  
  %% Finally, plot the temperature map:
  if DEBUG
    figure; suptitle(titleStr);
    subplot(1,2,1); imagesc(K2C(T_rec)); colorbar;
    T_rec(T_rec > C2K(Tref+100)) = nan;
    subplot(1,2,2); histogram(K2C(T_rec),1000);
    figure; surf(K2C(T_rec),'EdgeColor','none');
  end
  %% apply model of temperature & concentration profiles
  % TODO: modify hard coded
  params.DEBUG = DEBUG;
  params.calcFromVirtualOrigin = false;
  params.displayGraphs = false;
  params.D = D_nozzle;
  params.tot_mfr = tot_mfr; % Total mass flow rate, [g/s]
  params.co2_mfr = CO2_mfr; % CO2 mass flow rate, [g/s] %TODO:
  params.Tamb_C = 26; % ambient temperature, [C]
  params.Tjet_C = 200; %Jet's outlet temperature, [C]
  params.Camb = 0.0004; % CO2 concentration in the ambient
  params.P0 = 101325; %[Pa]. Pressure @ outlet is 1 atm = 101325[Pa]

  [~,idxMax] = max(DL_new_n(end,:));
  [C_model, T_model, depths] = GenerateTemperatureConcentrationModelsFit2image(DL_new_n, m2pixel, size(DL_new_n,1), idxMax, params);
  %TODO: maybe need to set in depths some value instead of 'nan'
  if DEBUG
    figure; imagesc(DL_new_n); colorbar
    figure; imagesc(T_model); colorbar
    figure; surf(C_model,'EdgeColor','none'); title('Concentration model');
    figure; surf(T_model,'EdgeColor','none'); title('Temperature model');
  end
  %{
  %% Zero order approximation - average everything within the jet - BAD RESULTS!!%%%
  C0 = 0.25;
  x = xx(1,:)/m2pixel; % x [m]
  D_x = D_nozzle*(0.127*(x-x(1))+1);% diameter of the jet as function of x
  cc = 5.65/2/x(1);
  Tamb = C2K(params.Tamb_C);
  T_cl_x = Tamb + (T0-Tamb)*0.5*5.65./x/cc; % centerline temperature as function of x
  T_avg_x = sqrt(pi)*erf(0.5)*T_cl_x; %[K]
  C_cl_x = 0.5*C0*0.5*5*D_nozzle./x; % centerline concentration.
  C_avg_x = sqrt(pi)*erf(0.5)*C_cl_x;
  l_avg_x = 2*D_x/pi;% mean path of the jet in x
  T_avg = mean(T_avg_x);
  C_avg = mean(C_avg_x);
  l_avg = mean(l_avg_x)*1e2; %[cm]
  %}
%% Divide to blocks the apparent plane (XY):
  % prepare a pixel-by-pixel emissivity map:
  e_map = ones(size(DL_new_n));
  % currently I have same mean value in a block.
  %chop image to match block size:
  imSize = size(T_model);
  T_model = T_model(1:bSize*floor(imSize(1)/bSize), 1:bSize*floor(imSize(2)/bSize));
  C_model = C_model(1:bSize*floor(imSize(1)/bSize), 1:bSize*floor(imSize(2)/bSize));
  depths = 100*depths(1:bSize*floor(imSize(1)/bSize), 1:bSize*floor(imSize(2)/bSize)); %also convert OPL to [cm]
  %devide to blocks
  T_blocks = divideIm2Blocks(T_model, bSize);
  C_blocks = divideIm2Blocks(C_model, bSize);
  depths = divideIm2Blocks(depths, bSize);
  % set in each block the mean value of it:
  setMeanVal = @(block) mean(block(:))*ones(size(block)); %TODO: replace by nanmean??
  avgTBlocks = cellfun(setMeanVal, T_blocks,'UniformOutput', false);
  avgCBlocks = cellfun(setMeanVal, C_blocks,'UniformOutput', false);
  avgDepthsBlocks = cellfun(setMeanVal, depths,'UniformOutput', false);
  %Build reconstructed images from the filtered blocks:
  avgTBlocks = cell2mat(avgTBlocks);
  avgCBlocks = cell2mat(avgCBlocks);
  avgDepthsBlocks = cell2mat(avgDepthsBlocks);
  if DEBUG
    figure; imagesc(avgTBlocks); colorbar; title('avg T model');
    figure; imagesc(avgCBlocks); colorbar; title('avg C model');
    figure; imagesc(avgDepthsBlocks); colorbar; title('avg depths model');
  end
  
  % set T,C images to have same jet dimentions as the depths image: 
  total_mask = ~isnan(avgDepthsBlocks);
  avgTBlocks = avgTBlocks .* total_mask;
  avgCBlocks = avgCBlocks .* total_mask;
  avgDepthsBlocks(~total_mask) = 0;
  
  
  %% Find emissivity map using HITRAN, in each block:
  % Load the HAPI calculated values:
  % using results of the script C:\Users\matanj\Documents\PythonScripts\getAbsorpCoeff.py
  % TODO: save the calculated triplets in the script, and its inputs should
  % be the limits of calculation.
  load('C:\Users\matanj\Documents\PythonScripts\meanAbsorpMatrix.mat');
  
  %perform 3d interpolation of the HITRAN data:
  calcTemps = linspace(C2K(params.Tamb_C), 600, 10);
  calcConcenrations = linspace(500e-6, CO2_mfr/tot_mfr, 10);
  calcOPLs = linspace(0.8, 1.6, 10);
  
  Temps = unique(avgTBlocks); Temps = Temps(Temps>0);
  Concenrations = unique(avgCBlocks); Concenrations = Concenrations(Concenrations>0);
  OPLs = unique(avgDepthsBlocks); OPLs = OPLs(OPLs>0); % also convert to [cm]
  
  [cT, cC, cO ] = meshgrid(calcTemps,calcConcenrations,calcOPLs);
  [Tq, Cq, Oq] = meshgrid(Temps,Concenrations,OPLs);

  meanAbsorptInterp = interp3(cT, cC, cO, meanAbsorp, Tq, Cq, Oq);
  
  % apply interpolation to get the emissivity map:
  for i = 1:size(avgTBlocks,1)
    for j = 1:size(avgTBlocks,2)
      if avgTBlocks(i,j) == 0
        e_map(i,j) = nan; % TODO: check if OK
      else
        e_map(i,j) = meanAbsorptInterp( Temps==avgTBlocks(i,j), Concenrations==avgCBlocks(i,j), OPLs==avgDepthsBlocks(i,j) );
      end
    end
  end
%   e_eff = trapz(absorp_spectrum_wn.*planck_wn) ./ trapz(planck_wn);
  e_map(e_map==1) = nan;
  e_eff = e_map;
  
  %% rotate back emissivity map to align with the calibration maps:
%   tform2 = affine2d([S.*cosd(-theta) -S.*sind(-theta) 0; S.*sind(-theta) S.*cosd(-theta) 0; 0 0 1]);
%   e_eff = imwarp(e_map,tform2);
  % trim & translate to original image coordinates:
  %TODO
  
  %% Apply calibration per pixel with the emissivity map:
  % solve the quadratic equation Y = p3-ln(e_eff) + p2*X + p1*X^2, for X = 1/T:
  x1 = (-a0 + sqrt(a0.^2 - 4*a1.*(Kw+log(e_eff)-log(DL_new_n))))./2./a1;
  x2 = (-a0 - sqrt(a0.^2 - 4*a1.*(Kw+log(e_eff)-log(DL_new_n))))./2./a1;
  tol = 1e-10;
  assert(all(x1(~isnan(x1)))>tol && all(x2(~isnan(x2)))>tol && isreal(x1) && isreal(x2));
  T_rec = nan(size(DL_new_n));
  % Get temperature map: take only T>0:
  T_rec(x1>0) = 1./x1(x1>0);
  T_rec(x1<0) = 1./x2(x1<0);
  T_rec = T_rec * max(T); % since we have normalized by max(T) to improve the fit process
  T_rec_C = K2C(T_rec);
  figure; imagesc(T_rec_C); colorbar; title('Reconstructed T[^oC] using blocks of mean emissivities');
  figure; surf(T_rec_C); view(181,15); title('Reconstructed T[^oC] using blocks of mean emissivities');
end

end

%% Auxiliary function:
function y = filterPrctile(x, p, val)
% Assigns 'val' to places where x is not in the percentile range 'p'. if
% 'val' not stated, 'NaN' is assigned by default.
    if nargin <2 || p<0 || p > 100
      error('invalid args!')
    end
    if nargin < 3
      val = nan;
    end
    x( x < prctile(x(:), p) | x > prctile(x(:), 100-p) ) = val;
    y = x;
end

%%%%%%%%% Divide image into blocks %%%%%%%%%
function subBlocks = divideIm2Blocks(I, bSize)
subBlocks = {};
if nargin < 2 || bSize < 2
  error('invalid args');
end
s   = size(I);
nB = ceil(s/bSize);
rowsVec = repmat(bSize, [nB(1) 1]);
colsVec = repmat(bSize, [nB(2) 1]);
subBlocks = mat2cell(I, rowsVec, colsVec);
% cellfun(@(x) mean(x(:)),C,'UniformOutput',false);
end

function rotatedIm = nanimwrap(im,tform)
% apply 2D transformation on an image with nans. 
% this is WRONG: due to inevitable interpolation in the rotation
    if nargin < 2
      error('invalid args!');
    end
    minVal = nanmin(im(:));
    maxVal = nanmax(im(:));
    tempVal = minVal - abs(1/(maxVal-minVal));
    im(isnan(im)) = tempVal;
    rotatedIm = imwarp(im, tform);
    rotatedIm(rotatedIm==tempVal) = nan;
end

function meanEmissivity = getAbsoptCoeffFromHITRAN_HAPI(T, C, l, script_path)
  % Get absorption coefficients from HITRAN, by running a python script
  % based on HAPI
  if nargin < 3
    error('invalid args!');
  end
  if nargin<4
    scriptPath = 'C:\Users\matanj\Documents\PythonScripts\getAbsorpCoeff.py';
  else
    scriptPath = script_path;
  end
  %build command string:
  argString = [num2str(T),' ',num2str(C),' ',num2str(l)];
  commandStr = ['python ', scriptPath, ' ', argString];
  [status, commandOut] = system(commandStr);
  if status ~= 0 % if succeeded:
    meanEmissivity = str2double(commandOut);
  else
    error('failed!');
  end
end

function [stats, object] = processImGetStats(im, hAx, calcStats)
%Function to process an image and extract the object and optionally its stats
%'im' - image
%'hAx' - handle to an axis to modify.
%'calcStats' - whether to output statistics in 'stats' or not.
    stats = [];
    lvl = graythresh(mat2gray(im)); binaryImage = im2bw(mat2gray(im), lvl); % alternative
    binaryImage = imfill(binaryImage, 'holes');
    labeledImage = bwlabel(binaryImage, 8);
    
    blobMeasurements = regionprops(logical(labeledImage), 'all'); % Get all the blob properties
    numberOfBlobs = size(blobMeasurements, 1);
    assert(numberOfBlobs == 1); % allow one blob
    
    object = im .* binaryImage;
    object(~object) = NaN;
      
    if nargin > 1 %need to calc stats and change given axis plot
        DLInterval = 10; % for histogram bin size
        axes(hAx); hold on;
        boundaries = bwboundaries(binaryImage);
        for k = 1 : numberOfBlobs % preferably just one...
          thisBoundary = boundaries{k};
          plot(thisBoundary(:,2), thisBoundary(:,1), 'r', 'LineWidth', 1.5);
          blobCentroid = blobMeasurements(k).Centroid;	% Get centroid
          plot(blobCentroid(1),blobCentroid(2),'r+', 'MarkerSize', 20);
        end
        hold off;
        % Calc Stats:
        if calcStats
          maxDL = nanmax(object(:));
          minDL = nanmin(object(:));
          nBins =  int16((maxDL-minDL)/DLInterval);
          [hist,edges] = histcounts(object,nBins);
          [~,I] = nanmax(hist(:));
          stats.Mean = nanmean(object(:));
          stats.MCDL = edges(I+1);
          stats.Max = maxDL;
        end
    end
end