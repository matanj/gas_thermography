function SaveCallerWorkspace( description_str, savefile_name )
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
callerName = dbstack(1);
callerName = callerName.name;
if nargin < 1
  description_str = '';
  savefile_name = ['SNAPSHOT_',callerName,'.mat'];
elseif nargin < 2
  savefile_name = ['SNAPSHOT_',callerName,'.mat'];
end

assignin('caller','description_str', description_str) % in case it doesn't exist in the base function
evalin('caller', ['save ',savefile_name]);
end

