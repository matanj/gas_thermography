%% Performance comparison for the forward Onion-peeling:
clc;
clear;

N = 200; % column size
N_ROWS = 300; %
n = (0:N-1)/N;
% ev = linspace(1,0,N); % emission vector
% ev = ones(N,1);       % emission vector
% ev = randn(N,1);      % emission vector
ev = exp(-5*n.^2);      % emission vector
tv = ones(N,1); % Set transmission vector all to 1 to, both cases should give same result
A = GasHelper.calculate_geometric_matrix(N); %

%% correction test:
I1 = GasHelper.forward_onion_peeling_no_self_absorp(ev,A);
I2 = GasHelper.forward_onion_peeling(ev,tv, A);
I2 = I2(:);
figure; plot(ev); title('ev');
figure; plot(I2./I1); title('I2/I1');
figure; plot(I1,'r'); hold on; plot(I2,'b'); legend('I1','I2');
assert(all( abs(I1-I2) < 1e-12),'Different results!');

%% Performance test:
tic
for i=1 : N_ROWS
  GasHelper.forward_onion_peeling_no_self_absorp(ev,A);
end
toc

tic
for i=1 : N_ROWS
  GasHelper.forward_onion_peeling(ev,tv, A);
end
toc
