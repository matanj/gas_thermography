%% Build per-pixel Calibration matrix for the Jet problem
%%
function JetBBCalibration
clc; close all;
dbstop if error
format short
%% Assumptions: 1) Order experiment of BGs->IMs -> BGs -> ...
%%              2) unique experiment cases.
%%              3) each experiment case hase same #files.
%%              4) All images are of same size (row, cols)
NUM_SMALLEST_ITS_TO_IGNORE = 0;
SUBTRACT_BIAS_FRAME = true; % whrther to subtract bias frame from ALL images
DefaultFolderPath = 'C:\Users\matanj\Documents\MATLAB\16-06-28_MatanStuff\'; % default
biasFramePath = 'C:\Users\matanj\Documents\MATLAB\BiasFrameComputed.mat';
OutputDesc = '_No_ignored_ITs';
OutputName = [mfilename,'_OUTPUT',OutputDesc];%name of file to save/load the output
ALL_PIXELS_SEE_BB = true;
%%
path(path,'Z:\Matan\');

%% Get data:
totalFilesList = dir([DefaultFolderPath,'/*.ptw']);
if isempty(totalFilesList)
  error('invalid folder path, or no .ptw files there...');
end
totalFilesList = {totalFilesList(:).name}';
[props.G,props.T,props.CWL,props.IT,props.isBG,props.name] = parseFilename(totalFilesList); %divide list to params
colorsArr = unique(props.CWL);
NUM_COLORS = numel(colorsArr);
Temps = unique(props.T);
Temps = Temps(Temps >= 100); % Avoid low radiation images
NUM_TEMPS = numel(Temps);

[totalFilesList,I1, props] = getMatchingCase(totalFilesList, props, {'T'}, {Temps});
nFiles = numel(totalFilesList);
% Get #ITs from one arbitrary experiment case:
[caseList,~,~] = getMatchingCase(totalFilesList, props, {'T','CWL','isBG'}, {Temps(1),colorsArr(1),true});
nITs = numel(caseList);

assert(nITs - NUM_SMALLEST_ITS_TO_IGNORE > 1, 'Err - number of ITs to be ignored is DAMN too high');

NfilesPerCase = NUM_COLORS*nITs*2; % (bg+im each)
assert(round(nFiles/NfilesPerCase) == numel(Temps), 'Err!');

% Process only CO2 related. % TODO - REMOVE.
colorsArr = colorsArr(end); % CO2 specific
NUM_WANTED_COLORS = numel(colorsArr);

%% Get Images' size
imPair = ImageBGPairExt([DefaultFolderPath,totalFilesList{1}]);
imInfo = imPair.IMG_Info;
[n_rows, n_cols] = deal(imInfo.m_rows, imInfo.m_cols);

%Preallocation:
DL = NaN(NUM_TEMPS, nITs, n_rows, n_cols); % Per-Pixel calibration
DL_normalized = DL;
IT = NaN(NUM_TEMPS,NUM_COLORS,nITs);

%% Main
%%
progressbar('Temperatures','ITs')

for indT = 1:NUM_TEMPS
  T = Temps(indT);
  for indColor = 1:NUM_WANTED_COLORS
    color = colorsArr(indColor);
    % Query the specific experiment case:
    [caseList,~,~] = getMatchingCase(totalFilesList, props, {'T','CWL'}, {T,color});
    assert(numel(caseList) == NfilesPerCase/NUM_COLORS);
    
    %From that case, get image-BG file lists for building IM-BG pairs:
    %Also, drop NUM_SMALLEST_ITS_TO_IGNORE from each list:
    [BGList,I,~] = getMatchingCase(caseList, props, {'isBG'}, {true});
    BGList = dropSmallestNITs(BGList, props.IT(I), NUM_SMALLEST_ITS_TO_IGNORE);
    BGList = strcat(DefaultFolderPath, BGList);
    [IMList,I,~] = getMatchingCase(caseList, props, {'isBG'}, {false});
    IMList = dropSmallestNITs(IMList, props.IT(I), NUM_SMALLEST_ITS_TO_IGNORE);
    IMList = strcat(DefaultFolderPath, IMList);
    
    %prealloc
%     IT = NaN(1,numel(BGList));
    imPair = ImageBGPairExt(IMList{1});%to get size of the image
    objectsArr = NaN(imPair.IMG_Info.m_rows, imPair.IMG_Info.m_cols, numel(BGList));
    
    %use mask to allow equal #pixels in all objects: calc mask from 1st
    %image pair.
    objMask = ones(imPair.IMG_Info.m_rows, imPair.IMG_Info.m_cols);%initial mask
    imPair = ImageBGPairExt(IMList{1});
    imPair.setBG(BGList{1});
    subIm = mean(abs(imPair.getSubtracted()), 3); % TODO: I dont need the abs here, check!!
    if ALL_PIXELS_SEE_BB
      object=subIm;
    else
      [~, object] = processImGetStats(subIm);
    end
    objMask(isnan(object)) = NaN;
    
    %iterate over pairs in the case file list:
    for indIMPair = 1:numel(BGList)
      imPair = ImageBGPairExt(IMList{indIMPair});
      imPair.setBG(BGList{indIMPair});
      subIm = mean(abs(imPair.getSubtracted()), 3);% TODO: I dont need the abs here, check!!
      IT(indT,indColor,indIMPair) = imPair.IT*1e6; %change IT to [us]
      %           %get meaningful object within the image:
      %           [~, object] = processImGetStats(subIm);
      %mask the image to maintain same size:
      object = objMask.*subIm;
      %get represntative DL from a 3-gaussians fit:

      %record object image for later processing
      objectsArr(:,:,indIMPair) = object;
      DL(indT,indIMPair,:,:) = objectsArr(:,:,indIMPair);
      % Normalize DLs map by IT:
      DL_normalized(indT,indIMPair,:,:) = objectsArr(:,:,indIMPair) ./ IT(indT,indColor,indIMPair);
      progressbar(indT/NUM_TEMPS,indIMPair/nITs);
    end
    % Calculate R images (pixel by pixel):
%     [R, ~] =  MultiITtoRP(IT,objectsArr);
  end
  progressbar(indT/NUM_TEMPS,0);
end

% save calculated values:
experimentCasesArr.temps = Temps;
experimentCasesArr.colors = colorsArr;
experimentCasesArr.IT = IT;
experimentCasesArr.exp_desciption = 'IT(Temps,color, nITs)';
save(OutputName, 'DL','DL_normalized', 'experimentCasesArr');


% if 0
% %% Plots
% %   oFile = dir(OutputName);
%   [oFile, oFilePath] = uigetfile({'*.mat','mat files'},'Select 1st mat file');
%   if isempty(oFile)
%     error('invalid OutputName, exiting');
%   end
%   load([oFilePath oFile]);
%  
%   Rfrom3Gaussians1 = Rfrom3Gaussians;
%   RfromRimagep3Gaussians1 = RfromRimagep3Gaussians;
%   sumR1 = sumR;
%   meanR1 = meanR;
%   
%   %%Show precentage difference between calculating Representative R, either
%   %%from DL image -> one DL -> R1, or R image -> one R2.
%   percentageArr = 100*abs(RfromRimagep3Gaussians(:) - Rfrom3Gaussians(:))./ RfromRimagep3Gaussians(:);
%   hAllR = figure; plot(Rfrom3Gaussians(:),'b*'); hold on;
%   plot(RfromRimagep3Gaussians(:),'bo');
% %   text([1:size(percentageArr)]',RfromRimagep3Gaussians(:),[num2str(percentageArr)]);
%   ylabel('R [a.u]');
%   xlabel('image number');
%   suptitle(['Calculating representative R for each experiment case (e.g. for calibration use):']);
% %   title('Values shown are relative R diff [%] between 2 methods. In cyan: calculation with 3 ITs dropped');
% %   legend('DL map => single DL => single R','R map => single R');
%   
%   %2nd file (some ITs dropped):
%   [oFile2, oFile2Path] = uigetfile({'*.mat','mat files'},'Select 2nd mat file');
%   if isempty(oFile2)
%     error('invalid OutputName, exiting');
%   end
%   load([oFile2Path oFile2]);
%   
%   percentageArr2 = 100*abs(RfromRimagep3Gaussians(:) - Rfrom3Gaussians(:))./ RfromRimagep3Gaussians(:);
%   figure(hAllR); plot(Rfrom3Gaussians(:),'r*'); hold on;
%   plot(RfromRimagep3Gaussians(:),'ro');
% %   text([1:size(percentageArr2)]',RfromRimagep3Gaussians(:),[num2str(percentageArr2)],'color','c');
% %   legend('No ITs drop, DL map => single DL => single R','No ITs drop, R map => single R', '3ITs dropped, DL map => single DL => single R','3ITs dropped, R map => single R');
%   legend('No ITs drop, DL map => single DL => single R','No ITs drop, R map => single R', '3ITs dropped, DL map => single DL => single R','3ITs dropped, R map => single R');  
% 
%   %compare IT drop vs no drop:
%   figure;
%   edges = 0:1:100;%percentage
%   histogram(percentageArr, edges,'FaceColor','r'); hold on;
%   histogram(percentageArr2, edges,'FaceColor','y');
%   title('Histograms of relative R diffs (reference: R from Rs method)');
%   xlabel('relative R diff [%]');
%   legend('No ITs drop','3 ITs drop');
%   
%   %%Now compare how dropping ITs affect each method:
%   percentageArrDLimage =  100*abs(Rfrom3Gaussians(:) - Rfrom3Gaussians1(:))./ Rfrom3Gaussians1(:);
%   percentageArrRimage =  100*abs(RfromRimagep3Gaussians(:) - RfromRimagep3Gaussians1(:))./ RfromRimagep3Gaussians1(:);
%   figure;
%   histogram(percentageArrDLimage, edges,'FaceColor','r'); hold on;
%   histogram(percentageArrRimage, edges,'FaceColor','y');
%   title('Histograms of relative R diffs between taking all ITs and ignoring 3 ITs: (reference: calculating w/o dropping ITs)');
%   xlabel('relative R diff [%]');
%   legend('R from DLs','R from Rs');
%   
%   
%   %% Back to BB comparison, based on 1 color and smallest ITs dropped:
%   indColor = 3;
%   Rfrom3Gaussians = Rfrom3Gaussians(:,:,:,indColor);
%   RfromRimagep3Gaussians = RfromRimagep3Gaussians(:,:,:,indColor);
%   sumR = sumR(:,:,:,indColor);
%   meanR = meanR(:,:,:,indColor);
%   
%   globalTitleExt = [',  filter=',num2str(experimentCasesArr.colors(indColor)),'\mum'];
%   
%   Tvec = repmat(Temps,[NUM_DISTANCES, 1]); Tvec = Tvec(:);
%   DistVec = repmat(distances,[1, NUM_TEMPS]);
%   hFigRepresentativeR_from_Rimage= figure();
%   hFigRepresentativeR_from_DLimage = figure();
%   hFigSumRs = figure();
%   hFigSumRsFits = figure();
%   hFigMeanRs = figure();
%   hFigRatioMeanRs = figure();
%   r = distances';
%   style = {'b.','bo','b*','r.','ro','r*'};
%   styleFit = {'b.','b','b--','r.','r','r--'};
%   legendStrArr = cell(NUM_OBJECTS_TYPES*NUM_TEMPS, 1);
%   dr = 0.1;
%   dR = 0.0;
%   
%   for indType = 1:NUM_OBJECTS_TYPES % for each object type
%     for indT = 1:NUM_TEMPS
%       indStyle = NUM_TEMPS*(indType-1) + indT;
%       legendStrArr{indStyle} = [ObjTypes{indType},' T=',num2str(Temps(indT)),'C'];
%       %Plot representative R per experiment case, from DL images:
%       Rfrom3Gaussians_i =  Rfrom3Gaussians(indT,:,indType)';
%       figure(hFigRepresentativeR_from_DLimage);
%       hRDL(indStyle) = semilogy(distances,Rfrom3Gaussians_i,[style{indStyle}]); hold on;
%       text(distances+dr, Rfrom3Gaussians_i+dR, num2str(Rfrom3Gaussians_i),'HorizontalAlignment','center');
%       xlabel('Distance[m]'); ylabel('log(R) (3 gaussians fit) [a.u]');
%       %Plot representative R per experiment case, from R images:
%       RfromRimagep3Gaussians_i =  RfromRimagep3Gaussians(indT,:,indType)';
%       figure(hFigRepresentativeR_from_Rimage);
%       hRRs(indStyle) = semilogy(distances,RfromRimagep3Gaussians_i,[style{indStyle}]); hold on;
%       text(distances+dr, RfromRimagep3Gaussians_i+dR, num2str(RfromRimagep3Gaussians_i),'HorizontalAlignment','center');
%       xlabel('Distance[m]'); ylabel('log(R) (3 gaussians fit) [a.u]');
%       %Plot sum(R):
%       sumR_i = sumR(indT,:,indType)';
%       figure(hFigSumRs);
%       hsumR(indStyle) = semilogy(distances,sumR_i, [style{indStyle}]); hold on;
%       xlabel('Distance[m]'); ylabel('log(sum of Rs) [a.u]');
%       %Plot fit of sumR to the 1/r^2 law
%       sR = sumR(indT,:,indType)';
%       figure(hFigSumRsFits);
%       [fitresult, gof] = fit(r, 1./sqrt(sR),'poly1');
%       hd(indStyle) = plot( r, 1./sqrt(sR), [style{indStyle}]); hold on;
%       hf(indStyle) = plot(fitresult, styleFit{indStyle}); hold on;
%       legendStrArrFit{indStyle} = [ObjTypes{indType},' T=',num2str(Temps(indT)),'C', '  R = ',num2str(gof.rsquare)];
%       xlabel('Distance[m]'); ylabel('1/sqrt(sum(R)) [a.u]');
%       %Plot mean(R):
%       meanR_i = meanR(indT,:,indType)';
%       figure(hFigMeanRs);
%       hm(indStyle) = semilogy(distances,meanR_i,style{indStyle}); hold on;
%       xlabel('Distance[m]'); ylabel('log(Mean of Rs) [a.u]');
%     end
%     
%   end
%   figure(hFigRepresentativeR_from_DLimage); legend(hRDL, legendStrArr);
%   title(['R from 3 gaussian fit of DL images, for multiple temperatures',globalTitleExt]);
%   figure(hFigRepresentativeR_from_Rimage); legend(hRRs, legendStrArr);
%   title(['R from 3 gaussian fit of R images, for multiple temperatures',globalTitleExt]);
%   figure(hFigSumRs); legend(hsumR, legendStrArr); 
%   title(['sum of Rs from pixel-by-pixel calculation, for multiple temperatures',globalTitleExt]);
%   figure(hFigSumRsFits); legend(hd, legendStrArrFit,'Location', 'Best');
%   title(['sum of Rs fitted to radiation law Vs distance for multiple temperatures',globalTitleExt]);
%   figure(hFigMeanRs); legend(hm, legendStrArr,'Location', 'Best');
%   suptitle(['mean of Rs Vs distance for multiple temperatures',globalTitleExt]);
%   title('Relative difference is shown in [%]');
% 
%   for indT = 1:NUM_TEMPS
%     
%     relativeDiffMeanR_i = 100*(meanR(indT,:,1) - meanR(indT,:,2)) ./ meanR(indT,:,1);
%     relativeDiffMeanR_i = relativeDiffMeanR_i';
%     figure(hFigRatioMeanRs);
%   
%     figure(hFigMeanRs);
%     relativeDiffMeanR_i_ = num2cell(relativeDiffMeanR_i);
%     text(distances+dr, meanR(indT,:,2)+dR, cellfun(@(c) [num2str(c) '%'], relativeDiffMeanR_i_, 'uni', false) );
%   end
%   figure(hFigRatioMeanRs); legend(arrayfun(@num2str, experimentCasesArr.temps, 'unif', 0));
%   return;
% 
% end
end
%% Auxiliary function:

function [caseList, I, hPropsListStructFiltered] = getMatchingCase(hTotFileList, hPropsListStruct, caseParamsNames, caseParamVals)
% Query list of files according to properties. 
% Allows reqursive queries by this scheme:
% [caseList1, I1, hPropsListStructFiltered] = getMatchingCase(hTotFileList, hPropsListStruct, caseParamsNames, caseParamVals)
% [caseList2, I2, ~] = getMatchingCase(caseList1, hPropsListStructFiltered, caseParamsNames, caseParamVals)

%INPUT: 'hTotFileList' - Total list of files of the experiment. cell array.
%       'hPropsListStruct' - struct array holding experiment props per
%           file. may be > than actual number mentioned in the filenames' format. 
%       'caseParamsNames' - parameters names to query. cell array.
%       'caseParamVals' - associated values. cell array.
%OUTPUT: 'caseList' - String cell array of filenames that match the input query.
%        'I' - indices array of corresponding files inside input list
%              'hTotFileList'
%        'hPropsListStructFiltered' - to allow recuring 

%TODO: add validation.
    caseList = {}; I = [];
    if nargin < 4
      return;
    end
    I = 1:numel(hTotFileList);
    for indCaseParams = 1:numel(caseParamsNames)
      %The following can handle cell array as well as numeric values: 
      subI = find(ismember(hPropsListStruct.(caseParamsNames{indCaseParams}), caseParamVals{indCaseParams} ));    
      I = intersect(subI, I);
    end
    caseList = {hTotFileList(I)};
    caseList = caseList{1};
    hPropsListStructFiltered = structfun(@(x) x(I), hPropsListStruct, 'UniformOutput', false);
end

function NewCaseList = dropSmallestNITs(caseList, ITArr, N)
%Drop the smallest N ITs from file list 'caseList' with 'ITArr',
% as they produces shitty results:
    if nargin < 3 || numel(ITArr)-N < 1
      error('invalid paramters!');
    end
    NewCaseList = caseList;
    sortedITimes = sort(ITArr,'descend');
    th = sortedITimes(end-N);
    NewCaseList(ITArr < th) = '';
end

function [stats, object] = processImGetStats(im, hAx, calcStats)
%Function to process an image and extract the object and optionally its stats
%'im' - image
%'hAx' - handle to an axis to modify.
%'calcStats' - whether to output statistics in 'stats' or not.
    stats = [];
    lvl = graythresh(mat2gray(im)); binaryImage = im2bw(mat2gray(im), lvl); % alternative
    binaryImage = imfill(binaryImage, 'holes');
    labeledImage = bwlabel(binaryImage, 8);
    
    blobMeasurements = regionprops(logical(labeledImage), 'all'); % Get all the blob properties
    numberOfBlobs = size(blobMeasurements, 1);
    assert(numberOfBlobs == 1); % allow one blob
    
    object = im .* binaryImage;
    object(~object) = NaN;
      
    if nargin > 1 %need to calc stats and change given axis plot
        DLInterval = 10; % for histogram bin size
        axes(hAx); hold on;
        boundaries = bwboundaries(binaryImage);
        for k = 1 : numberOfBlobs % preferably just one...
          thisBoundary = boundaries{k};
          plot(thisBoundary(:,2), thisBoundary(:,1), 'r', 'LineWidth', 1.5);
          blobCentroid = blobMeasurements(k).Centroid;	% Get centroid
          plot(blobCentroid(1),blobCentroid(2),'r+', 'MarkerSize', 20);
        end
        hold off;
        % Calc Stats:
        if calcStats
          maxDL = nanmax(object(:));
          minDL = nanmin(object(:));
          nBins =  int16((maxDL-minDL)/DLInterval);
          [hist,edges] = histcounts(object,nBins);
          [~,I] = nanmax(hist(:));
          stats.Mean = nanmean(object(:));
          stats.MCDL = edges(I+1);
          stats.Max = maxDL;
        end
    end
end