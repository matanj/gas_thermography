function [ FA ] = forwardAbel( f, R )
% Implementing the Forward Abel transform of a 1D signal.
% 'f' should include values up until the radius (and not be zero-padded)
  f = f(:);
  N = numel(f);
  r = (0.000001:N)'*(R/N);
  X = r;
  FA = zeros(size(f));
  for c=1:length(X)
    x=X(c);
    % evaluate Abel-transform equation:
    fun = @(r) (f(c)).*r./sqrt(r.^2 - x.^2);
    FA(c)=2*integral(fun,x,R);
  end

end

