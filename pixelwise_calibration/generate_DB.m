function generate_DB(OPL)
C2K = @(x) x + 273.15; % Celsius to Kelvin
%% DB generation parameters:
% [0.01, 0.0125, 0.015, 0.0175, 0.02, 0.0225, 0.1, 0.5, 0.8, 1, 1.2, 1.8, 2, 2.2, 4]
N_T_iters = 200; % Maximal number of iterations for the iterative process.
N_C = 100; % number of concentrations
%% DB files:
R_DB_file_name = ['R_DB_OPL_',num2str(OPL),'cm.mat']; % DataBase file for logging R(c,T,distance...)
TAU_DB_file_name = ['TAU_DB_OPL_',num2str(OPL),'cm.mat']; % DataBase file for logging TAU(c,T,distance...)
%% HITRAN spectral calculations:
hitranParams.wn_step = 0.01; % HITRAN's wavenumber step [cm^-1]
hitranParams.min_wn = 2000; % HITRAN's minimal wavenumber [cm^-1]
hitranParams.max_wn = 3333; % HITRAN's maximal wavenumber [cm^-1]
hitranParams.HW_to_eval = 500; % Number of HWHM for Voigt evaluation. Orignally 6000 by Ilya. 500 also good enough.
hitranParams.min_uncertainty_code = '000000';%'342222';% minimal uncertainty 6-digit code, see http://hitran.org/docs/uncertainties/
hitranParams.line_strength_th = 1e-31; % Line stregth threshold.
% Number of HWHM is a tradoff between computation time and accuracy. Worst
% case for validation is with high concentration and temperatures, as
% expected.
hitranParams.hitranFileFullPath = fullfile(pwd,'data','hitranCO2_H2O_2000_3333_main_isotopologs.par');
%% Default paths:
CalibOutputName = 'CalibrationParams_Integral.mat';

if isempty(CalibOutputName)
  error('invalid folder path...');
end
S = load(fullfile(pwd, CalibOutputName));
calibParams = S.calibParams;
%% Create a file for logging calculated R(T,C,distance), if not exist:
if exist(fullfile(pwd,R_DB_file_name), 'file') == 2 || exist(fullfile(pwd,TAU_DB_file_name), 'file') == 2% there is already such file
  error('file(s) Exist');
end
% create new files
R_DB_struct = struct('T',[],'C',[],'OPL',[],'R',[]);
R_DB_struct.T = linspace(C2K(10), C2K(500), N_T_iters )';
R_DB_struct.C = linspace(400e-6, 1, N_C)';
R_DB_struct.OPL = OPL;
R_DB_struct.R = zeros(N_T_iters, N_C, numel(R_DB_struct.OPL));
R_DB_struct.hitranParams = hitranParams;

TAU_DB_struct = struct('T',[],'C',[],'OPL',[],'TAU',[]);
TAU_DB_struct.T = R_DB_struct.T;
TAU_DB_struct.C = R_DB_struct.C;
TAU_DB_struct.OPL = R_DB_struct.OPL;
TAU_DB_struct.TAU = zeros(N_T_iters, N_C, numel(R_DB_struct.OPL));
TAU_DB_struct.hitranParams = hitranParams;
% Save in version compatible with efficient partial loading/saving.
% Saving like this should be done only here. Subsequent saves can omit
% the '-v7.3' flag.
save(fullfile(pwd,R_DB_file_name),'R_DB_struct','-v7.3');
save(fullfile(pwd,TAU_DB_file_name),'TAU_DB_struct','-v7.3');

calibParams.K = 1;
[~, optics_tf_wn] = GasHelper.spectralFunc_wl2wn(calibParams.wl_vect, calibParams.optics_tf, true);

atm_params.OPL = 1; % Atmospheric path [cm]
atm_params.co2_concentration = 600e-6; % Atmospheric CO2 concentration [fraction]
atm_params.T = C2K(25); % Atmospheric temperature [K]
atm_params.RH = 45; % Atmospheric Water Relative Humidity (RH) [%]
atm_params.pressure_tot = 1; % [atm]
[wn_vect, ~, ~] = MyGetSpectrumGPU(true, 'tau', hitranParams, atm_params);
%% GENEREATE A FULL DB FOR RADIATION WITH OPTIC PATH = PIXEL SIZE:
params.hitranParams = hitranParams;
params.calibParams = calibParams;
params.wn_vect = wn_vect;
params.optics_tf_wn = optics_tf_wn;
% params.atm_params = atm_params;
jetParams.pressure_tot = 1;
jetParams.RH = 45;
jetParams.OPL = OPL;

for indT = 1 : numel(R_DB_struct.T)
  for indC = 1 : numel(R_DB_struct.C)
    jetParams.T = R_DB_struct.T(indT);
    jetParams.co2_concentration = R_DB_struct.C(indC);
    params.jetParams = jetParams;
    params.path_params = jetParams;
    [~, R_DB_struct] =     R_DB_helper.get_R_from_DB(    R_DB_struct, jetParams.T, jetParams.co2_concentration, jetParams.OPL, params );
    [~, TAU_DB_struct] = TAU_DB_helper.get_TAU_from_DB(TAU_DB_struct, jetParams.T, jetParams.co2_concentration, jetParams.OPL, params );
  end
  disp(['Finished inner loop #',num2str(indT)]);
end
save((fullfile(pwd,R_DB_file_name)),'R_DB_struct');
save((fullfile(pwd,TAU_DB_file_name)),'TAU_DB_struct');
