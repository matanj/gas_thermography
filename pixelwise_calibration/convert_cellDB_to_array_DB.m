tic
S = load('R_DB_as_cell_array.mat');
toc
R_DB_struct = S.R_DB_struct;
R = R_DB_struct.R;
%% convert [] to -1 before cell2mat:
emptyIndex = cellfun('isempty',R);
R(emptyIndex) = {0};
RR = cell2mat(R);
R_DB_struct.R = RR;
%%
tic
save('R_DB.mat','R_DB_struct','-v7.3'); % Save in version compatible with efficient partial loading/saving.
toc
