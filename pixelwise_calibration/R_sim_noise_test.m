ignoreSelfAbsorption  = false;
SNR= 20;

%Generate T & C images:
[T_im, C_im] = GasHelper.gen_T_C_images_from_model(model_params, atm_params, numel(jet_boundary_line), jet_boundary_line); % ~0.04 sec
R_sim_projected = nan(size(T_im));
%Calculate profile's emission:
emission_sim_im = F_R(T_im(:), C_im(:), jetParams.OPL*ones(numel(T_im),1));% Interpolate R DB
emission_sim_im =  geometry .* calibParams.K(1:size(T_im,1),symAxis:symAxis+size(T_im,2)-1) .* reshape(emission_sim_im, size(T_im,1),[]);
%Calculate profile's transmission:
if ignoreSelfAbsorption % Ignore self-absorption
  tau_sim_im = ones(size(T_im));
else
  tau_sim_im = F_TAU(T_im(:), C_im(:), jetParams.OPL*ones(numel(T_im),1));% interpolate TAU DB
  tau_sim_im = reshape(tau_sim_im, size(T_im,1),[]);
end

emission_sim_im = awgn(emission_sim_im,SNR,'measured');
tau_sim_im = awgn(tau_sim_im,SNR,'measured');
% Project the profiles to the camera's domain if needed:
for iRow = 1 : size(T_im, 1)% Loop over rows:
  r = jet_boundary_line(iRow); % neglegable improvement over above
  % Estimating the measured radiation using the T,C models:
  %Apply Forward Projection:
  R_sim_projected(iRow, 1:r) = 0.5 * GasHelper.forward_onion_peeling(emission_sim_im(iRow,1:r), tau_sim_im(iRow,1:r), A_array{iRow}); % '0.5' is tmp bug fix - TODO: modify
end
% Approximate path's transmission as constant:
R_sim_projected = R_sim_projected ./ tau_avg;


GasHelper.plotIm(100*abs(R_sim_projected-R_sim_projected_noised)./R_sim_projected-R_sim_projected_noised); caxis([0 100]);
