%% Build per-pixel Calibration matrix for the Jet problem
%%
function JetBBCalibrationSaveR
clc;
dbstop if error
format short
%% Assumptions: 1) Order experiment of BGs->IMs -> BGs -> ...
%%              2) unique experiment cases names.
%%              3) each experiment case has same #files.
%%              4) All images are of same size (row, cols)
%%              5) IM & BG frames were taken at IDENTICAL ITs

%% OPTIONS:
IS_CAVITY_BB = true; % if true - use automatic registration. else - GUI.
USE_BIAS_FRAME = false; % whether to subtract Bias Frame from all images, or subtract BG
REGISTER_IMAGES = true;%false; % if true - use control points to find transform between successive images and register them. 
%%

biasFramePath = 'C:\Users\matanj\Documents\MATLAB\BiasFrameComputed.mat';
OutputDesc = '';
if IS_CAVITY_BB
  DefaultFolderPath = 'C:\\Users\\matanj\\Documents\\MATLAB\\16-12-29_CavityBB_Calib\\';
  OutputName = ['CavityBBCalibrationSaveR_OUTPUT',OutputDesc];
  ALL_PIXELS_SEE_BB = false;
else
  DefaultFolderPath = 'C:\\Users\\matanj\\Documents\\MATLAB\\16-11-07_CO2Calib\\';
  % DefaultFolderPath = 'C:\\Users\\matanj\\Documents\\MATLAB\\16-10-06_CO2_Calib\\'; % default
  OutputName = [mfilename,'_OUTPUT',OutputDesc];%name of file to save/load the output
  ALL_PIXELS_SEE_BB = true;
end
OutputName = ['CavityBBCalibrationSaveR_OUTPUT',OutputDesc];

%% Get data:
totalFilesList = dir([DefaultFolderPath,'/*.ptw']);
if isempty(totalFilesList)
  error('invalid folder path, or no .ptw files there...');
end
totalFilesList = {totalFilesList(:).name}';
[props.G,props.T,props.CWL,props.IT,props.isBG,props.name] = parseFilename(totalFilesList); %divide list to params
colorsArr = unique(props.CWL);
NUM_COLORS = numel(colorsArr);
Temps = unique(props.T);
Temps = Temps(Temps >= 100); % Avoid low radiation images
NUM_TEMPS = numel(Temps);

% Get the list of files:
[totalFilesList,~, props] = GasHelper.getMatchingCase(totalFilesList, props, {'T'}, {Temps});
nFiles = numel(totalFilesList);
% Get #ITs from one arbitrary experiment case:
[caseList,~,~] = GasHelper.getMatchingCase(totalFilesList, props, {'T','CWL','isBG'}, {Temps(1),colorsArr(1),true});
nITs = numel(caseList);

NfilesPerCase = NUM_COLORS*nITs*2; % (bg+im each)
assert(round(nFiles/NfilesPerCase) == numel(Temps), 'Err!');

% Process only CO2 related. % TODO - REMOVE.
colorsArr = colorsArr(end); % CO2 specific
NUM_WANTED_COLORS = numel(colorsArr);

%% Get Images' size
imPair = ImageBGPairExt([DefaultFolderPath,totalFilesList{1}]);
imInfo = imPair.IMG_Info;
[n_rows, n_cols] = deal(imInfo.m_rows, imInfo.m_cols);

%% Get the Bias frame, if needed
if USE_BIAS_FRAME
  biasFrame = cropBiasFrame2PTWsize(biasFramePath, imInfo);
end

%% Main
BG_IM_mismatch = 1; % Flag for inconsistensy between BG-IM sequence, if found
%Preallocation:
R = NaN(NUM_TEMPS, NUM_WANTED_COLORS, n_rows, n_cols);
progressbar('Temperatures','ITs')

%{
for indT = 1:NUM_TEMPS
  T = Temps(indT);
  for indColor = 1:NUM_WANTED_COLORS
    %prealloc
    objectsIMArr = NaN(imPair.IMG_Info.m_rows, imPair.IMG_Info.m_cols, nITs);
    objectsBGArr = NaN(imPair.IMG_Info.m_rows, imPair.IMG_Info.m_cols, nITs);
    IT = NaN(nITs,1);
    
    color = colorsArr(indColor);
    % Query the specific experiment case:
    [caseList,~,~] = GasHelper.getMatchingCase(totalFilesList, props, {'T','CWL'}, {T,color});
    assert(numel(caseList) == NfilesPerCase/NUM_COLORS);
    
    %From that case, get image-BG file lists for building IM-BG pairs:
    [BGList,~,~] = GasHelper.getMatchingCase(caseList, props, {'isBG'}, {true});
    BGList = strcat(DefaultFolderPath, BGList);
    [IMList,~,~] = GasHelper.getMatchingCase(caseList, props, {'isBG'}, {false});
    IMList = strcat(DefaultFolderPath, IMList);
   
    %Calc mask to allow equal #pixels in all objects (calc from 1st image pair).
    objMask = ones(imPair.IMG_Info.m_rows, imPair.IMG_Info.m_cols);%initial mask
    imPair = ImageBGPairExt(IMList{1});
    imPair.setBG(BGList{1});
    subIm = mean(abs(imPair.getSubtracted()), 3);
    if ALL_PIXELS_SEE_BB
      object=subIm;
    else
      [~, object] = GasHelper.processImGetStats(subIm);
    end
    objMask(isnan(object)) = NaN;
    
    %iterate over pairs in the case file list:
    for indIMPair = 1:numel(BGList)
      imPair = ImageBGPairExt(IMList{indIMPair});
      imPair.setBG(BGList{indIMPair});     
      IT(indIMPair) = imPair.IT*1e6; %change IT to [us]
      if USE_BIAS_FRAME
        % align frames to the bias frame, if needed:
        [im, ~] = findFlipState(biasFrame, imPair.getImage());
        [bg, ~] = findFlipState(biasFrame, imPair.getBG());
        %subtract the bias frame from each frame & mask it to maintain same size:
        %{
        imm = im - biasFrame;
        figure; imagesc(im); axis image; colorbar; title('orig');
        figure; imagesc(imm); axis image; colorbar; title('minus bias');
        %}
        objectsIMArr(:,:,indIMPair) = (im - biasFrame) .* objMask;
        objectsBGArr(:,:,indIMPair) = (bg - biasFrame) .* objMask;
      end
      
      progressbar(indT/NUM_TEMPS,indIMPair/nITs);
    end
    % Calculate R images (pixel by pixel):
    [R_im,~] = MultiITtoRP(IT, objectsIMArr);
    [R_bg,~] = MultiITtoRP(IT, objectsBGArr);
    % Subtract R_bg from R_im:
    R_temp = BG_IM_mismatch*(R_im - R_bg); % first time, this order is assumed.
    RR = R_temp(~isnan(R_temp));
    if (~all(RR >= 0 ))
      if BG_IM_mismatch==1
        warning('Non positive subtracted R!! probably BG<=>IM name mismatch. ');
        BG_IM_mismatch = -1;
        R_temp = -R_temp;
      else
        error('BG and IM ordering might not be consistent!!');
      end
    end
    R(indT,indColor,:,:) = R_temp;
  end
%   ITs = reshape(IT', numel(IT), []);
  progressbar(indT/NUM_TEMPS,0);
end
%}

%%
rows = 1:n_rows;
cols = 1:n_cols;
stats = cell(NUM_TEMPS,1);
for indT = NUM_TEMPS:-1:1 %
  T = Temps(indT);
  for indColor = 1:NUM_WANTED_COLORS
    color = colorsArr(indColor);
    % Query the specific experiment case:
    [caseList,~,~] = GasHelper.getMatchingCase(totalFilesList, props, {'T','CWL'}, {T,color});
    assert(numel(caseList) == NfilesPerCase/NUM_COLORS);
    %Caculate R from MultiIT, with either Bias Frame, or the usual BG
    %subtraction:
    if USE_BIAS_FRAME
      RR = GasHelper.GetMeasurementRByMultiIT2RP(DefaultFolderPath, num2str(T), zeros(n_rows,n_cols), rows, cols, biasSubframe, ALL_PIXELS_SEE_BB);
    else
      RR = GasHelper.GetMeasurementRByMultiIT2RP(DefaultFolderPath, num2str(T), zeros(n_rows,n_cols), rows, cols, [], ALL_PIXELS_SEE_BB);
    end
    
    if REGISTER_IMAGES % register all images to align with the one with strongest SNR:
      if IS_CAVITY_BB
        % Get the main ROI:
        imOtsu = otsu(RR, 2);
        imOtsuBW = imOtsu > 1;
        stat = regionprops('table', imOtsuBW, 'Area','Centroid','Orientation','MajorAxisLength','MinorAxisLength','BoundingBox');
        if size(stat,1) ~= 1 %  if one BLOB, it's assumed the main, no need to further filter.
          error('several objects detected!');
        end
        stats{indT}.centers = stat.Centroid;
        stats{indT}.diameters = mean([stat.MajorAxisLength stat.MinorAxisLength],2);
        stats{indT}.areas = stat.Area;
        % DEBUG: GasHelper.plotIm(RR); hold on;
        % DEBUG: viscircles(stat.centers,stat.diameters/2); plot(stat.centers(1),stat.centers(2),'r*','MarkerSize',8) ;hold off;
        RR(isnan(RR)) = 0;
        if indT == NUM_TEMPS % define the fixed image
          fixedPoint = [stat.Centroid(1), stat.Centroid(2)];
          fixed = RR;
          Rfixed = imref2d(size(fixed));
          fixedArea = stat.Area;
        else
          movingPoint = [stat.Centroid(1), stat.Centroid(2)];
          translation = movingPoint - fixedPoint;
          theta_deg = 0; %atan2d(translation(2), translation(1));
          S = sqrt(stat.Area / fixedArea);
          tform = affine2d([S.*cosd(theta_deg) -S.*sind(theta_deg) 0;...
            S.*sind(theta_deg) S.*cosd(theta_deg) 0; ...
            -translation(1) -translation(2) 1]);
          RR = imwarp(RR, tform,'FillValues', 0, 'OutputView', Rfixed);
        end
      else
        %% TODO: better here to use normalized correlation?
        % Using GUI tool to select control points:
        if indT == NUM_TEMPS % define the fixed image
%           fixedPoints =  [50 50; 60 60];
          fixedPoints = [floor(size(RR)/2)];
          movingPoints = fixedPoints;
          fixed = RR;
          Rfixed = imref2d(size(fixed));
          fixed(isnan(fixed)) = 0; % cpselect crushes if images has 'NaN'.
        else
          RRR = RR; RRR(isnan(RRR)) = 0; % cpselect crushes if images has 'NaN'.
          [movingPoints, fixedPoints] = cpselect(mat2gray(RRR), mat2gray(fixed), movingPoints, fixedPoints,'Wait',true);
          if size(fixedPoints,1) > 1 % translation + rotation + scale
            tform = fitgeotrans(movingPoints, fixedPoints, 'nonreflectivesimilarity'); % no need for 'projective'
          else % pure translation
            theta_deg = 0; S = 1;
            translation = movingPoints - fixedPoints;
            tform = affine2d([S.*cosd(theta_deg) -S.*sind(theta_deg) 0;...
                              S.*sind(theta_deg) S.*cosd(theta_deg) 0; ...
                              -translation(1) -translation(2) 1]);
          end
          RR = imwarp(RRR, tform,'FillValues', 0, 'OutputView', Rfixed);
        end
      end
     
    end
    
    % Check BG<->IM sequence consistency:
    R_temp = BG_IM_mismatch*RR; % first time, this order is assumed.
    RR = R_temp(~isnan(R_temp));
    if (~all(RR >= 0 ))
      if BG_IM_mismatch==1
        warning('Non positive subtracted R!! probably BG<=>IM name mismatch. ');
        BG_IM_mismatch = -1;
        R_temp = -R_temp;
      else
        error('BG and IM ordering might not be consistent!!');
      end
    end
    R(indT,indColor,:,:) = R_temp; % bookeeping
    progressbar((NUM_TEMPS-indT)/NUM_TEMPS,indColor/NUM_WANTED_COLORS);
  end
  progressbar((NUM_TEMPS-indT)/NUM_TEMPS,0);
end

if REGISTER_IMAGES
  figure;
  for ii = 1 : NUM_TEMPS
    pause(0.8);
    imagesc(squeeze(R(ii,1,:,:))); colorbar; axis image; impixelinfo; hold on;
    drawnow;
  end
end
pause;
%% save calculated values:
experimentCasesArr.temps = Temps;
experimentCasesArr.colors = colorsArr;
% experimentCasesArr.IT = IT;
if USE_BIAS_FRAME
  experimentCasesArr.BiasFrame = biasFrame;
  experimentCasesArr.exp_desciption = sprintf(['Raw iamges from: ',DefaultFolderPath,'\nGenerated by MultiIT2RP with subtraction of a Bias Frame']);
else
  experimentCasesArr.exp_desciption = sprintf(['Raw iamges from: ',DefaultFolderPath,'\nGenerated by MultiIT2RP with subtraction of a BG']);
end
save(OutputName, 'R', 'experimentCasesArr');

end