function test_integral_calibration()
%% Tests effect of filter OD on calibration parameter K in the integral calibration process.
clear;
%% Calibration related:
C2K = @(x) x + 273.15; % Celsius to Kelvin
% Using the Cavity BB experiment from 16-12-29
ALL_PIXELS_SEE_BB = false;
NUM_OD = 50; % number of OD guesses to check
CalibFilePath = 'C:\Users\matanj\Documents\MATLAB\pixelwise_calibration\CavityBBCalibrationSaveR_OUTPUT.mat'; 
DefaultFolderPath = 'C:\\Users\\matanj\\Documents\\MATLAB\\16-12-29_CavityBB_Calib\\test case\\'; % for test case
apparatus_tf_path = 'apparatus_tfs.mat'; % default apparatus tranfer functions
calib_atm_params.OPL = 220; % Calibration Atmospheric path in[cm]
calib_atm_params.co2_concentration = 700*1e-6;% Calibration Atmospheric CO2 concentration [fraction]
calib_atm_params.T = C2K(24.5); % Calibration Atmospheric temperature [K]
calib_atm_params.RH = 38; %[%]
calib_atm_params.pressure_tot = 1; %[atm]
calib_experiment_params.e_BB = 0.97;
calib_experiment_params.detector_pitch_um = 15;
calib_experiment_params.atmParams = calib_atm_params;
%% For HITRAN spectral calculations:
hitranParams.wn_step = 0.01; % HITRAN's wavenumber step [cm^-1]
hitranParams.min_wn = 2000; % HITRAN's minimal wavenumber [cm^-1]
hitranParams.max_wn = 3333; % HITRAN's maximal wavenumber [cm^-1]
hitranParams.HW_to_eval = 500; % Number of HWHM for Voigt evaluation. Orignally 6000 by Ilya. 500 also good enough.
hitranParams.hitranFileFullPath = 'C:\Users\matanj\Documents\MATLAB\pixelwise_calibration\data\hitranCO2_H2O_2000_3333_main_isotopologs.par';
%%%%%
%% Test the calibration:
%Get test case:
[imName, baseDir] = uigetfile({'*.ptw','FLIR images'},'Select image to test calibration',DefaultFolderPath);
if isempty(imName)
  error('invalid image');
end
%Get the Background file that matches the Image file's name:
bgName = GasHelper.getBGfileFromIMfile(imName, baseDir);
%Get the temperature of the measurement from file's name:
matchStr = regexp(imName,'_T+[0-9]+[0-9]+[0-9]','match');
TrefStr = regexprep(matchStr{1},'_T','');
T_K = C2K(str2num(TrefStr));
%Set the image pair object:
imPair = ImageBGPairExt([baseDir,imName]);
imPair.setBG([baseDir,bgName{1}]);
n_rows = imPair.IMG_Info.m_rows;
n_cols = imPair.IMG_Info.m_cols;
rows = 1:n_rows;
cols = 1:n_cols;
R_test_case = GasHelper.GetMeasurementRByMultiIT2RP(baseDir, TrefStr, zeros(n_rows,n_cols), rows, cols, [], ALL_PIXELS_SEE_BB);
nan_test_case = isnan(R_test_case);
%% calculate atmosheric path transmittance for the test case:
[wn_vect, ~, tau_spectra_wn] = MyGetSpectrumGPU(true, 'tau', hitranParams, calib_atm_params);

%% Iterate over OD:
[wl_vect, optics_cfits, ~] = GasHelper.get_optics_tf(apparatus_tf_path, hitranParams, false);
% wn_vect = GasHelper.wl2wn( wl_vect); % TODO: remove
% tau_spectra_wn = 1; % TODO: remove
hitranParams.tau_spectra_wn = tau_spectra_wn; % save additional recomputations assuming same tau
orig_filter_tf = optics_cfits.Filter(wl_vect);
orig_OD_guess = log10( max(orig_filter_tf) / mean(orig_filter_tf(orig_filter_tf<=0.1)) ); % Considering tails of <0.1% transmission
optics_tf_wl = GasHelper.modify_optics_tf_by_OD(wl_vect, optics_cfits, orig_OD_guess);
calib_experiment_params.filter_OD = orig_OD_guess;
[calibParams] = GasHelper.getSpectralCalibrationFromR(CalibFilePath, wl_vect, hitranParams, calib_experiment_params, optics_tf_wl, false);

OD_guess_arr = linspace(1, orig_OD_guess + 0.5, NUM_OD);
R_simulated = zeros(numel(OD_guess_arr), n_rows, n_cols);
norm_2_error_R = zeros(size(OD_guess_arr));
norm_inf_error_R = zeros(size(OD_guess_arr));
for indOD = 1:numel(OD_guess_arr) % Loop over OD
  OD_guess = OD_guess_arr(indOD);
  calib_experiment_params.filter_OD = OD_guess;
  %update filter tf with new OD:
  optics_tf_wl = GasHelper.modify_optics_tf_by_OD(wl_vect, optics_cfits, OD_guess);
  %   [calibParams] = GasHelper.getSpectralCalibrationFromR(CalibFilePath, wl_vect, hitranParams, calib_experiment_params, optics_tf_wl, false);
  %   GasHelper.histIm(calibParams.K,1000);
  calibParams.optics_tf = optics_tf_wl;
  %% Asssume for the test case same geometry as in calibration
  geometry = calibParams.geometryStruct.geometryFactor;
  if indOD == 1 % Save repeating calculations
    nan_mask_sim = isnan(calibParams.K);
%     nan_mask = ~nan_mask_sim & ~nan_test_case;
    nan_mask = false(size(R_test_case));
    nan_mask(30:110, 30:100) = true;
    calib_experiment_params.geometryStruct = calibParams.geometryStruct; % save interactive calculation for the rest of the cases
  end
  % Planck in [photons/s/cm^2/sr/cm^-1]
  L_BB_photons_wn = calib_experiment_params.e_BB * 2*100*GasHelper.c .* (wn_vect.^2) ./ (exp(100*GasHelper.C2*wn_vect/T_K) -1);
  R_simulated(indOD,:,:) = GasHelper.getRfromSpectralCalibration(calibParams, geometry, wn_vect, L_BB_photons_wn, tau_spectra_wn);
  R_sim_temp = squeeze(R_simulated(indOD,:,:));
  norm_2_error_R(indOD) = norm( (R_sim_temp(nan_mask) - R_test_case(nan_mask)) ./R_test_case(nan_mask) );
  norm_inf_error_R(indOD) = norm( (R_sim_temp(nan_mask) - R_test_case(nan_mask)) ./R_test_case(nan_mask)  , Inf );
end
figure;
plot(OD_guess_arr, norm_2_error_R/max(norm_2_error_R)); hold on; title('error_R Norms (normalized) Vs OD guesses');
plot(OD_guess_arr, norm_inf_error_R/max(norm_inf_error_R));
xlabel('OD guess');
legend('||\DeltaR||_2','||\DeltaR||_\infty');

%{
  % Test variance in K(T) for several pixels. this requires saving K(T) in
  % GasHelper.getSpectralCalibrationFromR()
  figure;
  plot(calibParams.T_K, calibParams.K(:,35,65),'DisplayName','pixel (35,65)'); hold on;
  title(['K(T) for several pixels. OD = ',num2str(OD_guess)]);
  plot(calibParams.T_K, calibParams.K(:,40,40),'DisplayName','pixel (40,40)');
  plot(calibParams.T_K, calibParams.K(:,100,100),'DisplayName','pixel (100,100)');
  plot(calibParams.T_K, calibParams.K(:,80,70),'DisplayName','pixel (80,70)');
  legend('-DynamicLegend');
  hold off;
%}
end