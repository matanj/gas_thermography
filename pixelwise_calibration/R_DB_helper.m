classdef R_DB_helper
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% HITRAN's DB - Stores R(T,C,OPL) W/O atmospheric tau %%%%%
% NEW ASSUMPTION: trasmission could be approximated by an averaged value,
% thus decoupled from the radiative integral.
% DB structure's fields:
%   1) T,C,OPL arrays.
%   2) R(T,C,OPL) stores: integral(radiance_photon_flux .* optics_tf_wn)
%   3) Atmospheric parameters struct.
%   4) HITRAN parameters struct.
%   5) Description.
% Use like: R_simulated = gemoetry_factor * K * R(T,C,OPL) * tau_avg.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  properties (Constant)
  end
  
  methods (Static = true)
    
    function [closest_val, closest_ind] = get_closest_val_in_array(A, val)
      [~, closest_ind] = min(abs(A-val));
      closest_val = A(closest_ind); % Finds first one only!
    end
    
    function [S] = get_nearest_DB_entry(DB, Tq, Cq, OPLq)
      [S.T, S.Ti]     = R_DB_helper.get_closest_val_in_array(DB.T, Tq);
      [S.C, S.Ci]     = R_DB_helper.get_closest_val_in_array(DB.C, Cq);
      [S.OPL, S.OPLi] = R_DB_helper.get_closest_val_in_array(DB.OPL, OPLq);
    end
    
    function DB = update_R_DB(DB, Tq, Cq, OPLq, new_R)
      % See Image Analyist's comment @
      % "https://www.mathworks.com/matlabcentral/answers/49390-pass-variable-by-reference-to-function"
      S = R_DB_helper.get_nearest_DB_entry(DB, Tq, Cq, OPLq);
      if ~(DB.R(S.Ti, S.Ci, S.OPLi))
        DB.R(S.Ti, S.Ci, S.OPLi) = new_R;
      end
    end
    
    function isMatch = is_DB_legit_to_case(DB, case_atm_params, hitran_params, is_tau_outside_R)
      % Below works ONLY if structs have same field ordering!!!
      % But Since DB is created according to the case' ordering, should be OK.
      
      % Fix for the case of different full paths:
      [~,DB_name,DB_ext] = fileparts(DB.hitranParams.hitranFileFullPath);
      [HP_path,HP_name,HP_ext] = fileparts(hitran_params.hitranFileFullPath);
      if isequal([DB_name,DB_ext], [HP_name,HP_ext]) % we care only about the file
        DB.hitranParams.hitranFileFullPath = fullfile(HP_path,[DB_name DB_ext]);
      end
      
      if is_tau_outside_R
        isMatch = isequal(DB.hitranParams, hitran_params);
      else
        isMatch = isequal(DB.atm_params, case_atm_params) & ...
          isequal(DB.hitranParams, hitran_params);
      end
    end
    
    function [R_scaled, DB] = get_R_from_DB(DB, Tq, Cq, OPLq, params)
      % 'params' should include: hitranParams, jetPatams, wn_vect,
      % calibParams.
      S = R_DB_helper.get_nearest_DB_entry(DB, Tq, Cq, OPLq);
      if ~(DB.R(S.Ti, S.Ci, S.OPLi))
        [~,~,raw_radiance_wn] = MyGetSpectrumGPU(false, 'radiance', params.hitranParams, params.jetParams); % in [W/sr/cm^2/cm^-1]
        radiance_photon_flux_spectra_wn = raw_radiance_wn ./ GasHelper.getPhotonEnergyFromWN(params.wn_vect); % convert to photons flux [#photons/s/sr/cm^2/cm^-1]
        % Store in DB "scaled R", by calling with 'geometry_factor==1', 'K==1'
        % and 'tau_spectra_wn==1':
        calibParams_temp = params.calibParams;
        calibParams_temp.K = 1;
        R_scaled = GasHelper.getRfromSpectralCalibration(calibParams_temp, 1, params.wn_vect, radiance_photon_flux_spectra_wn, ones(size(params.wn_vect)));
        DB = R_DB_helper.update_R_DB(DB, S.T, S.C, S.OPL, R_scaled);
      else
        R_scaled = DB.R(S.Ti,S.Ci,S.OPLi);
      end
    end
    
    function [R_scaled] = get_R_from_full_DB(F, Tq, Cq, OPLq)
      %get interpolated R from a FULL DB (doesn't update it) for the given
      %vectors of queries.
      R_scaled = F(Tq, Cq, OPLq);
    end
    
    function [S] = get_parameters_from_R_DB2(DB, R_scaled, conditions_ranges_struct)
      Tmax = conditions_ranges_struct.Tmax;
      Tmin = conditions_ranges_struct.Tmin;
      Cmax = conditions_ranges_struct.Cmax;
      Cmin = conditions_ranges_struct.Cmin;
      OPLmax = conditions_ranges_struct.OPLmax;
      OPLmin = conditions_ranges_struct.OPLmin;
      S = [];
      R_arr = DB.R;
      [~, closest_OPL_ind] = R_DB_helper.get_closest_val_in_array(DB.OPL, OPLmax);
      % TODO: change to more efficient restriction of the search. Also, add
      % tolerances:
      R_arr(DB.T < Tmin | DB.T > Tmax, :, :) = nan;
%       R_arr(:, DB.C < Cmin | DB.C > Cmax, :) = nan;
      R_arr(:, 1:numel(DB.C) ~= closest_C_ind, :) = nan;
      R_arr(:, :, 1:numel(DB.OPL) ~= closest_OPL_ind) = nan;
      R_arr = R_arr(:);
      [~, closest_ind] = R_DB_helper.get_closest_val_in_array(R_arr, R_scaled);
      if ~isempty(closest_ind);
        [S.Ti, S.Ci, S.OPLi] = ind2sub(size(DB.R),closest_ind);
      end
    end
    
    function [S] = get_parameters_from_R_DB(DB, R_scaled, conditions_ranges_struct, tol)
      Tmax = conditions_ranges_struct.Tmax;
      Tmin = conditions_ranges_struct.Tmin;
      Cmax = conditions_ranges_struct.Cmax;
      Cmin = conditions_ranges_struct.Cmin;
      OPL = conditions_ranges_struct.OPL;
      S = [];
      
      [~, closest_OPL_ind] = R_DB_helper.get_closest_val_in_array(DB.OPL, OPL);
      R = DB.R(:,:,closest_OPL_ind);
      if ~any(R(:)) % No entries in DB with this OPL
        return;
      end
      R_arr = R;
%       [TT,CC] = meshgrid(DB.T, DB.C);
      [TT,CC] = ndgrid(DB.T, DB.C);
      mask = abs(R_arr-R_scaled) <= tol;
      t = TT.*mask;
      c = CC.*mask;
      t = t(:);
      c = c(:);
      t = t(t>0);
      c = c(c>0);
      maskT = t>=Tmin & t <=Tmax;
      maskC = c>=Cmin & c <= Cmax;
      mask_tot = maskT & maskC;
      S.T = t(mask_tot);
      S.C = c(mask_tot);
      
%       % TODO: change to more efficient restriction of the search. Also, add
%       % tolerances:
%       R_arr(DB.T < Tmin | DB.T > Tmax, :) = nan;
% %       R_arr(:, DB.C < Cmin | DB.C > Cmax, :) = nan;
%       R_arr(:, 1:numel(DB.C) ~= closest_C_ind) = nan;
%       R_arr = R_arr(:);
%       [~, closest_ind] = R_DB_helper.get_closest_val_in_array(R_arr, R_scaled);
%       if ~isempty(closest_ind);
%         [S.Ti, S.Ci] = ind2sub(size(R),closest_ind);
%       end
    end
    
    
   
    
  end
end