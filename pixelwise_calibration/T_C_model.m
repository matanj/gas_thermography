classdef T_C_model
  %T_C_MODEL class for the model of T & C fields in an image
  properties ( Constant = true )
    CELCIUS_TO_KELVIN_T_SHIFT = 273.15;
  end
  
  properties
    N_ROWS;
    T0;
    C0;
    X_PC;
    Y_PC;
    Y_nozzle;
    Y_up;
    b;
  end
  
  methods ( Access = public)
    
    function obj = T_C_model()
      
    end
    
  end%methods
  
  methods (Static = true)  
    function T_K = C2K(T_C)
      T_K = T_C + T_C_model.CELCIUS_TO_KELVIN_T_SHIFT;
    end
    function T_C = K2C(T_K)
      T_C = T_K - T_C_model.CELCIUS_TO_KELVIN_T_SHIFT;
    end
  end
  
end%ClassDef