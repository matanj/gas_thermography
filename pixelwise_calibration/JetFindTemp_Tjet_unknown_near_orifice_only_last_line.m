%% Use per-pixel Calibration matrix to retrieve the Jet temperature
%%
% *Synopsis*
%%
%% THIS test differ from 'JetFindTempUsingCalibrationMatrix' in that when checking the calibration on the test case,
%% Its allows radiation estimation from MultiITtoRP(), instead of just DL/IT.

function JetFindTemp_Tjet_unknown_near_orifice_only_last_line
clc; close all;
dbstop if error
warning off
format short
dbstatus
%% Assumptions: 1) Order experiment of BGs->IMs -> BGs -> ...
%%              2) unique experiment cases.
%%              3) each experiment case hase same #files.
%%              4) All images are of same size (row, cols)
%%              5) New images to be used with the caibration should match its image's size AND PIXEL ALLIGNMENT!

%%              6) The measured Camera's R is independant of solid angle of the object:
%%                  R ~ double_integral(Radiance*dOmega*dA), where here the solid angle is of the detector pixel and Camera's FOV, and the detector's area.

LOAD_WORKSPACE_OF_EXPERIMENT = true; % loads a saved experiment and spares all the tedious choosing. if not exists, it saves one for future use

if ~LOAD_WORKSPACE_OF_EXPERIMENT || ~(exist(['SNAPSHOT_',mfilename,'.mat'], 'file') == 2) % if there isn't a snapshot, else jump to analysis

%% OPTIONS
DEBUG = false; % show figures;
DO_BINARY_SEARCH = false; % If the temperature dependancy is monotone, binary search would be fastest.
DO_ABEL_INVERSION = true; % Do Inverese Abel on R_cam, or use calculated R based on simulated data
CALC_CALIB_PARAMS = true; % if false, use existing set for testing
REPRESENTATIVE_R_CALIB = true; % if true - representative Rs will be used to obtain 2/3 calibration params. otherwise, 2/3 parameters per pixel.
NUM_CALIB_PARAMS_2_OR_3 = 2; % 2 or 3 parameters for calibration: either R~~ Kw*exp(A0/T) or Kw*exp(A0/T+A1/T^2)
e_BB = 0.97; % emissivity of the Black Body used for calibration. assumed constant spatial-wise and spectrum-wise.
FILTER_PARAMETERS_TH = 0; % percentile-based filtering over calculated fit parameters. '0' for filtering by std
FILTER_PARAMETERS_STD = 0;% std-based filtering over calculated fit parameters. '0' for no filtering
APPLY_ATM_MODEL = true; % apply HITRAN model of atmospheric transmittance when retreiving measurement's temperature
APPLY_ATM_MODEL_IN_CALIBRATION = true;
ALL_PIXELS_SEE_BB = true; % for calibrating only a subset of the image
FIX_JET_ANGLE = false; % fix jet's alignment to be vertical
HITRAN_CALC_RADIANCE = false; % 'true' - calc radiance from HITRAN. 'false' - calc emissivity.
NOZZLE_IN_IMAGE = true; % if the images contain the nozzle, select it and get m2pixel, etc.
USE_BIAS_FRAME = false;
FILTER_THE_RADIATION = true; % filter estimated R with a gaussian LPF.
USE_CFD = true; % for the jet model
REFINE_CALIB_PARAMS = false; % set mean value of "good" pixels to the "Bad" pixels that show absord temperature.
bSize = 2; % Block size [pixels]
iterative_Convergence_tol = 1e-6; % tolerance for convergance of the iterative process [deg C]
N_iters = 200; % Maximal number of iterations for the iterative process.
max_guessed_temperature_C = 600; % maximal temperature to guess [C]
%% EXPERIMENT RELATED:
tot_mfr = 2; %[g/s]
CO2_mfr = 1; %[g/s]
D_nozzle = 1e-2; % Nozzle's diameter [m]
atm_OPL = 0.95; % Atmospheric path in [m]
atm_co2_concentration = 600; % Atmospheric CO2 concentration [ppmv]
atm_T = 26; % Atmospheric temperature [C]
% atm_params.OPL = 0.95; % Atmospheric path in [m]
% atm_params.co2_concentration = 540; % Atmospheric CO2 concentration [ppmv]
% atm_params.T = 27; % Atmospheric temperature [C]
% atm_params.RH = 50; % Atmospheric Water Relative Humidity (RH) [%]

%% CO2 filter related (assumed symetric):
% Based on spectrometer measurements from:
% "Z:\Matan\Gas measurements\Calsys1200BB\15_12_1\With Filters\15_12_2\1200BB_400C_4370nm_step1nm.xlsx"
filterParams.filter_CWL = 4.366; % CO2 filter CWL [um]
filterParams.filter_HWHM = GasHelper.sigma2HWHM(8.644e-3);%20e-3; % CO2 filter FWHM [um]
filterParams.th_h = 0.8; % peak transmission
filterParams.wl_cut_on = 4.345; %[um] 5% of peak transmittance
filterParams.wl_cut_off = 4.388; %[um] 5% of peak transmittance
filterParams.shape = 'gaussian'; % 'gaussian' or 'triangle'
%%
atm_OPL = atm_OPL*100; % to [cm]
atm_co2_concentration = atm_co2_concentration*1e-6; % ppmv to fraction

%% Default paths:
CalibFilePath = [pwd,'\pixelwise_calibration\JetBBCalibrationSaveR_OUTPUT.mat']; % default
outputName = 'CalibrationParams.mat';
biasFramePath = 'C:\Users\matanj\Documents\MATLAB\BiasFrameComputed.mat'; % default Bias Frame path
%% Default test case and related files:
baseDir = 'Z:\FLIR_Experimental\16-10-10_CloseJet\';
imName = [baseDir, 'G001_T500_F4370_IT2693.6_IM_f.ptw'];
atm_trans_calib_FileName = 'C:\Users\matanj\Documents\PythonScripts\Atmospheric_Transmittance_T27C_C630ppm_L150cm_RH51.mat'; % path used in the calibration measurements

C2K = @(x) x + 273.15; % Celsius to Kelvin
K2C = @(x) x - 273.15; % Kelvin to Celsius

addpath(genpath('Z:\Matan\'), '-end');

%% conditionally calculate calibration parameters:
if CALC_CALIB_PARAMS 
  calib_experiment_params.filterParams = filterParams;
  calib_experiment_params.e_BB = e_BB;
  calib_experiment_params.atm_trans_calib_FileName = atm_trans_calib_FileName;
  [T, Calib_params] = GasHelper.getCalibrationFromR(CalibFilePath, NUM_CALIB_PARAMS_2_OR_3, REPRESENTATIVE_R_CALIB, APPLY_ATM_MODEL_IN_CALIBRATION, calib_experiment_params);
  % Save coefficients per pixel:
  Kw = Calib_params.Kw;
  a0 = Calib_params.a0;
  if NUM_CALIB_PARAMS_2_OR_3 == 3
    a1 = Calib_params.a1;
    save(outputName, 'Kw', 'a0','a1', 'T');
  else
    save(outputName, 'Kw', 'a0', 'T');
  end
else % Use exisiting calibration params:
  [ParamsFileName, ParamsFilePath] = uigetfile({'*.mat','MATLAB data'},'Select Calibration parameters set',[pwd,'\CalibrationParams.mat']);
  if isempty(ParamsFileName)
    error('invalid folder path...');
  end
  load([ParamsFilePath, ParamsFileName]);
end

%% Filter results by percentile, or by STD:
if FILTER_PARAMETERS_TH > 0 % by percentile
  Kw = GasHelper.filterPrctile(Kw, FILTER_PARAMETERS_TH);
  a0 = GasHelper.filterPrctile(a0, FILTER_PARAMETERS_TH);
  if NUM_CALIB_PARAMS_2_OR_3 == 3
    a1 = GasHelper.filterPrctile(a1, FILTER_PARAMETERS_TH);
  end
elseif FILTER_PARAMETERS_STD > 0 % by std
  Kw = GasHelper.filterStds(Kw, FILTER_PARAMETERS_STD);
  a0 = GasHelper.filterStds(a0, FILTER_PARAMETERS_STD);
  if NUM_CALIB_PARAMS_2_OR_3 == 3
    a1 = GasHelper.filterStds(a1, FILTER_PARAMETERS_STD);  
  end
end
totNaNMask = isnan(Kw) & isnan(a0);
if NUM_CALIB_PARAMS_2_OR_3 == 3
   totNaNMask = totNaNMask & isnan(a1);
end
Kw(totNaNMask) = nan;
a0(totNaNMask) = nan;
if NUM_CALIB_PARAMS_2_OR_3 == 3
  a1(totNaNMask) = nan;
end
if DEBUG
  figure;
  subplot(2,3,1);  imagesc(Kw); colorbar; title('Kw');
  subplot(2,3,2); imagesc(a0); colorbar; title('a0');
  subplot(2,3,4); imagesc(~isnan(Kw)); colorbar; title('"Valid" Kw');
  subplot(2,3,5); imagesc(~isnan(a0)); colorbar; title('"Valid" a0');
  if NUM_CALIB_PARAMS_2_OR_3 == 3
    subplot(2,3,3); imagesc(a1); colorbar; title('a1');
    subplot(2,3,6); imagesc(~isnan(a1)); colorbar; title('"Valid" a1');
  end
  figure; imagesc(totNaNMask); colorbar; title('total mask');
  figure;
  subplot(1,3,1);  histogram(Kw,500); title('Kw');
  subplot(1,3,2);  histogram(a0,500); title('a0');
  if NUM_CALIB_PARAMS_2_OR_3 == 3
    subplot(1,3,3);  histogram(a1,500); title('a1');
  end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% %%%%%%%%%%%%%%%%% Test the calibration: %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  %Get test case:
  [imName, baseDir] = uigetfile({'*.ptw','FLIR images'},'Select image to test calibration',imName);
  if isempty(imName)
    error('invalid image');
  end
  %GET the Background file that matches the Image file's name:
  bgName = GasHelper.getBGfileFromIMfile(imName, baseDir);

  %Get the temperatures of the measurement from file's name:
  matchStr = regexp(imName,'_T+[0-9]+[0-9]+[0-9]','match');
  tempStr = regexprep(matchStr{1},'_T','');
  Tref = str2double(tempStr);
  
  imPair = ImageBGPairExt([baseDir,imName]);
  imPair.setBG([baseDir,bgName{1}]);
  %% Get the Bias frame:
%   [biasFrameFull, biasFrame] = cropBiasFrame2PTWsize(biasFramePath, imInfo);
  %% crop the suitable subframe form it and rotate accordingly:
  if USE_BIAS_FRAME
    [ biasSubframe ] = suitBiasSubframeFromBiasFrame( imPair.getImage(), imPair.IMG_Info, biasFramePath );
  end
  
  subIm = mean(abs(imPair.getSubtracted()), 3);
  if ALL_PIXELS_SEE_BB
    objMask = true(size(subIm));
  else
    [~, object] = GasHelper.processImGetStats(subIm);%get meaningful object within the image
    objMask = true(imPair.IMG_Info.m_rows, imPair.IMG_Info.m_cols);%initial mask
    objMask(isnan(object)) = NaN;
  end
  %mask the image to maintain same size:
  DL_new = objMask.*subIm;
  DL_new(DL_new==0) = 1; % set minimal DL to 1 to avoid zero issues
  
  %% trim image to orifice and get pipe's diameter:
  if NOZZLE_IN_IMAGE
    [DL_new, D_nozzle_pixels, m2pixel, pos_nozzle, ~] = GasHelper.trimIm2Nozzle(DL_new, D_nozzle);
  end
  
  %% Rotate Jet if needed:
  if FIX_JET_ANGLE
    %% TODO: should apply same to the calibration maps, or rotate back b4 using the calibration
    DL_new = GasHelper.fixJetAngle(DL_new);
  end
  
  %% Apply calibration with Kw, a0, a1 maps.
  % First, crop object image to the calibration image, if needed:
  if ~(exist('rect','var') ) % if cropping wasn't done:
    [rowsCalib, colsCalib] = size(Kw);
    [rowsNew, colsNew] = size(DL_new);
    if rowsNew ~= rowsCalib || colsNew ~= colsCalib
      rows = 1:floor(pos_nozzle(1,2)); % TODO: what if NOZZLE_IN_IMAGE == false?
      cols = 1:size(DL_new,2);
    else
      rows = 1:size(DL_new,1);
      cols = 1:size(DL_new,2);
    end
  else % if 'rect' is present:
    rows = rect(2):rect(2)+rect(4);
    cols = rect(1):rect(1)+rect(3);
  end
  % If doing pxiel-by-pixel and image's size > calibration image, should exit.
  % if representative calibration parameters are chosen - no problem
  if REPRESENTATIVE_R_CALIB
    Kw = Kw(1,1)*ones(size(DL_new));
    a0 = a0(1,1)*ones(size(DL_new));
    Kw = Kw(rows, cols); 
    a0 = a0(rows, cols);
    if NUM_CALIB_PARAMS_2_OR_3 == 3
      a1 = a1(1,1)*ones(size(DL_new));
      a1 = a1(rows, cols);
    end
  elseif (size(DL_new,1) > size(Kw,1)) || (size(DL_new,2) > size(Kw,2))% exit if image size > calibration image size
    error('Image size > calibraion image size !!');
  end
  totNaNMask = isnan(Kw) | isnan(a0);
  if NUM_CALIB_PARAMS_2_OR_3 == 3
    totNaNMask = totNaNMask | isnan(a1);
  end
  DL_new(totNaNMask) = nan;
  
  %% Get the HITRAN-calculated emission/emissivity matrix
  if HITRAN_CALC_RADIANCE
    meanEmissionMatrixFilePath = 'C:\Users\matanj\Documents\PythonScripts\';
    [meanEmissionMatrixFileName, meanEmissionMatrixFilePath] = uigetfile({'*.mat','MATLAB data'},'Select mean Emission Matrix file',meanEmissionMatrixFilePath);
    if isempty(meanEmissionMatrixFileName)
      error('invalid folder path, or matrix there...');
    end
    load([meanEmissionMatrixFilePath,meanEmissionMatrixFileName]);
    numT = size(meanEmission, 1); % take #temperatures from the loaded matrix
    numC = size(meanEmission, 2); % take #concentrations from the loaded matrix
  else % Calc emissivity
    meanAbsorpMatrixFilePath = 'C:\Users\matanj\Documents\PythonScripts\meanAbsorpMatrix_540K_1_1cm.mat';
    [meanAbsorpMatrixFileName, meanAbsorpMatrixFilePath] = uigetfile({'*.mat','MATLAB data'},'Select mean Absorp Matrix file',meanAbsorpMatrixFilePath);
    if isempty(meanAbsorpMatrixFileName)
      error('invalid folder path, or matrix there...');
    end
    load([meanAbsorpMatrixFilePath,meanAbsorpMatrixFileName]);
    numT = size(meanAbsorp, 1);
    numC = size(meanAbsorp, 2); % take #concentrations from the loaded matrix
  end

  %% estimate the measurement's radiation from DL = R*IT^P
  if USE_BIAS_FRAME % Subtraction of R images
    R_est = GasHelper.GetMeasurementRByMultiIT2RP(baseDir, tempStr, DL_new, rows, cols, biasSubframe);
  else % subtraction of DL images
    R_est = GasHelper.GetMeasurementRByMultiIT2RP(baseDir, tempStr, DL_new, rows, cols, []);
  end
  
  assert( all(R_est(~isnan(R_est)) >= 0) || all(R_est(~isnan(R_est)) <=0) ); % mixed signs are not allowed!
  R_est = abs(R_est); % fix for possible confusion of IM and BG
  if FILTER_THE_RADIATION
    %     R_est_filt = filter2(1/3^2*ones(3), R_est);
    R_est_filt = imgaussfilt(R_est, 1.5); % apply Gaussian LPF. should preserve edges.
    R_est = R_est_filt;
  end
  assert(all(R_est(:)),'invalid R...');
 
  %% apply HITRAN transmittance model for the atmosheric path of the measurement
  if APPLY_ATM_MODEL
    [AtmFileName, AtmFilePath] = uigetfile({'*.mat','MATLAB data'},'Select atm transmittance matrix of the experiment:',atm_trans_calib_FileName);
    atm_trans_FileName = [AtmFilePath,AtmFileName];
    if isempty(atm_trans_FileName)
      error('invalid path, or transmittance matrix there...');
    end
    trans_eff = GasHelper.getEffectiveTransmittance(filterParams, atm_trans_FileName);
  else
    trans_eff = 1;
  end
  trans_eff = trans_eff*ones(size(R_est)); % assuming constant in all of the volume

  if 0
    %% Apply Abel inversion over R_est
    [R_est, rows, cols] = GasHelper.InverseAbelImage(R_est, m2pixel);
    %% take the ROI from the data:
    a0 = a0(rows,cols);
    Kw = Kw(rows,cols);
    if NUM_CALIB_PARAMS_2_OR_3 == 3
      a1 = a1(rows,cols);
    end
    trans_eff = trans_eff(rows,cols);
    R_est = R_est_filt(rows,cols); % TODO: this patch negates the inverse abel, yet allows the cropping
  end

  %% Calculate solid_angle and detector area for later use:
  OPL_step = D_nozzle/D_nozzle_pixels*1e2; %in [cm] for HITRAN
  % approximation of the solid angle of a cell in the jet:
  solid_angle = (OPL_step/(atm_OPL))^2;
  detector_pitch_um = 50; % [um] % TODO: fix to actual
  detector_area_cm2 = (detector_pitch_um*1e-4)^2; % Detector's area in [cm^2]
  
  %% Define Interpolant for later interpolation of HITRAN calculation:
  calcTemps = linspace(C2K(atm_T), C2K(max_guessed_temperature_C), numT);
  calcConcenrations = linspace(atm_co2_concentration, CO2_mfr/tot_mfr, numC);
  if HITRAN_CALC_RADIANCE
    F = GasHelper.generateInterpolantFromHITRANData(calcTemps,calcConcenrations,meanEmission);
  else
    F = GasHelper.generateInterpolantFromHITRANData(calcTemps,calcConcenrations,meanAbsorp);
  end
  
  %% Parameters for the models of temperature & concentration profiles in the jet:
  params.useCFD = USE_CFD;
  if USE_CFD
    [CFDpath, ~] = uigetfile({'*.xlsx','ANSYS csv files'},'Select FreeJet CFD file', [pwd,'\FreeJet.xlsx']);
    params.data = GasHelper.GetCFDdataFromANSYS(CFDpath);
  end
  params.D = D_nozzle;
  params.tot_mfr = tot_mfr; % Total mass flow rate, [g/s]
  params.co2_mfr = CO2_mfr; % CO2 mass flow rate, [g/s]
  params.Tamb_C = atm_T; % ambient temperature, [C]
  params.Camb = atm_co2_concentration; % CO2 concentration in the ambient
  params.P0 = 101325; %[Pa]. Pressure @ outlet is 1 atm = 101325[Pa]
  [~,idxMax] = max(DL_new(end,:)); %index location of the jet's symmetry axis
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  % save SNAPSHOT workspace if needed:
  if LOAD_WORKSPACE_OF_EXPERIMENT 
    SaveCallerWorkspace(''); % enter description in string if needed
  end
  return;
end
%%%%%%%%%%%%%%%%%%%%%%%%
% load the SNAPSHOT:%%%%
%%%%%%%%%%%%%%%%%%%%%%%%
load(['SNAPSHOT_',mfilename,'.mat']);

  %% TAKE only the lines next to the exshaust: % TODO: make better
  % next to the nozzle's exhaust:
%   ROI_rows = size(DL_new,1)-1;
%   ROI_cols = 1:size(DL_new,2);
  ROI_rows = bSize*floor(size(DL_new,1)/bSize);  % TODO: remove
  ROI_cols = 1:bSize*floor(size(DL_new,2)/bSize);  % TODO: remove

  % crop parameters accordingly:
  arr = {trans_eff, Kw, a0};
  if NUM_CALIB_PARAMS_2_OR_3 == 3
    arr = {arr, a1};
  end
  ROI_arr = GasHelper.cropROIFromImagesCellArray(ROI_rows, ROI_cols, arr);
  trans_eff_ROI = ROI_arr{1};
  Kw_ROI = ROI_arr{2};
  a0_ROI = ROI_arr{3};
  if NUM_CALIB_PARAMS_2_OR_3 == 3
    a1_ROI = ROI_arr{4};
  end
  
  % For the distribution on the axis, no need for ABEL inverese:
  % TODO: WHO SAYS THAT'S THE CENTER??
  % TODO: check if dividing by D_nozzle in [cm] is wrong..
%   tmp_measurement = R_est(ROI_rows, round(end/2)+1)./D_nozzle; % Assuming uniform distribution in the potential core
  tmp_measurement = R_est(ROI_rows, round(end/2)+1)./D_nozzle_pixels; % Assuming uniform distribution in the potential core
  
  % Pre-alloc:
  T_guesses = nan(N_iters,1);
  err_norm_vec = nan(N_iters,1);
  
  % Guessed tempmeratures:
  calcTemps_arr = calcTemps; % initialization
  if DO_BINARY_SEARCH
    LU_temp_ind = floor(numel(calcTemps)/2); % initial guess
  else
    LU_temp_ind = 2; % instead of '1' to aviod params.Tjet_C==params.Tamb_C and the bug below
  end
  params.Tjet_C = K2C(calcTemps_arr(LU_temp_ind)); %Jet's outlet temperature, [C]
  
  %% Apply model of temperature & concentration profiles in the jet:
  %% TODO: this model produces buggy output for small temperatures! (30C)
  [C_model, T_model, ~] = GenerateTemperatureConcentrationModelsFit2image(...
                        size(DL_new), m2pixel, size(DL_new,1), idxMax, params);
 
  assert(abs(params.Tjet_C - params.Tamb_C) > eps); % bug below if params.Tjet_C == params.Tamb_C
  theta_model = (T_model - C2K(params.Tamb_C))./(params.Tjet_C - params.Tamb_C); % non-dimentional thermal variable.
  % Divide to blocks the apparent plane (XY):
  % currently I have same mean value in a block.
  theta_model_Blocks = GasHelper.imFromFilteredBlocks(theta_model, bSize);
  avgCBlocks = GasHelper.imFromFilteredBlocks(C_model, bSize);
  avgCBlocks = avgCBlocks(ROI_rows,ROI_cols);
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  %% ITERATIVE PROCESS: 
  for k = 1 : min(N_iters, numel(calcTemps_arr)-1)
    params.Tjet_C = K2C(calcTemps_arr(LU_temp_ind)); %New guess for Jet outlet temperature[C]
    T_guesses(k) = params.Tjet_C;
    if mod(k,5) == 1
      disp(['iteration ',num2str(k),': T_guess = ',num2str(params.Tjet_C),'[C]']);
    end
%     T_model = C2K(params.Tamb_C + (params.Tjet_C - params.Tamb_C).*theta_model); % scaling the model
    avgTBlocks = C2K(params.Tamb_C + (params.Tjet_C - params.Tamb_C).*theta_model_Blocks);
    avgTBlocks = avgTBlocks(ROI_rows,ROI_cols);
    if DEBUG
      GasHelper.plotIm(K2C(avgTBlocks),'avg T model[C]');
      GasHelper.plotIm(avgCBlocks,'avg C model');
    end
    
    %% Find emission radiance / emissivity using HITRAN, in each block:
    [e_eff] = GasHelper.interpHITRANData(avgTBlocks, avgCBlocks, F);
    
    if HITRAN_CALC_RADIANCE % Scale the HITRAN radiance to photon flux (the camera counts photons)
%         e_eff = e_eff(:, round(end/2)+1:end); % length is assumed even
        e_eff = GasHelper.HITRAN_radiance2Photon_Radiance(e_eff, filterParams.filter_CWL);% converting filter_CWL in [nm] to [cm^-1]
        e_eff = GasHelper.photons_flux2R_camera(e_eff, solid_angle, detector_area_cm2);
        tmp = e_eff.*trans_eff_ROI;
    else % Use Calibration model to estimate R_camera based on calculated emissivity and guessed T:
      % Apply calibration (T2R):
      if NUM_CALIB_PARAMS_2_OR_3 == 3
        R_calc = GasHelper.T2R(C2K(T_guesses(k)), T, e_eff, trans_eff_ROI, Kw_ROI, a0_ROI, a1_ROI);
      else
        R_calc = GasHelper.T2R(C2K(T_guesses(k)), T, e_eff, trans_eff_ROI, Kw_ROI, a0_ROI);
      end
        tmp = R_calc;
        
        %% Simulating T2R of the system:
        if 0
          c_vec = [0.1,1];%0.1:0.1:1;
          temps = 100:500;
          temps_K = C2K(temps);
          RRR = zeros(size(temps));
          RRR_BB = zeros(size(temps));
          
          [AtmFileName, AtmFilePath] = uigetfile({'*.mat','MATLAB data'},'Select another atm transmittance matrix to compare:',atm_trans_calib_FileName);
          atm_trans_FileName = [AtmFilePath,AtmFileName];
          if isempty(atm_trans_FileName)
            error('invalid path, or transmittance matrix there...');
          end
          trans_eff_2 = GasHelper.getEffectiveTransmittance(filterParams, atm_trans_FileName);
          
          % Loop for the BB:
          for indT=1:numel(temps)
            RR_BB = GasHelper.T2R(C2K(temps(indT)), T, 1, trans_eff_ROI(1), Kw_ROI, a0_ROI);
            RRR_BB(indT) = RR_BB(1);
          end
          figure(998); plot(temps,RRR_BB,'DisplayName','BB'); title('T2R for BB & CO2 concentrations'); xlabel('T[C]'); hold on;
          figure(999); plot(temps(1:end-1), diff(RRR_BB),'DisplayName','BB'); title('dR/dT for BB CO2 concentrations'); xlabel('T[C]'); hold on;
          %Loops for the CO2:
          for indC=1:numel(c_vec)
            for indT=1:numel(temps)
              ee = GasHelper.interpHITRANData(temps_K(indT), c_vec(indC), F);
              RR = GasHelper.T2R(C2K(temps(indT)), T, ee, trans_eff_ROI(1), Kw_ROI, a0_ROI);
              RRR(indT) = RR(1);
              if indC %numel(c_vec)
                RR_high_atm_path = GasHelper.T2R(C2K(temps(indT)), T, ee, trans_eff_2, Kw_ROI, a0_ROI);
                RRR_high_atm_path(indT) = RR_high_atm_path(1);
              end
            end
            figure(998); plot(temps,RRR,'DisplayName',[num2str(c_vec(indC)),' CO_2']);
            if indC %numel(c_vec)
              plot(temps,RRR_high_atm_path,'DisplayName',[num2str(c_vec(indC)),' CO_2 with another atm path']);
            end
            legend('-DynamicLegend');
            figure(999); plot(temps(1:end-1),diff(RRR),'DisplayName',[num2str(c_vec(indC)),' CO_2']);
            if indC %numel(c_vec)
              plot(temps(1:end-1),diff(RRR_high_atm_path),'DisplayName',[num2str(c_vec(indC)),' CO_2 with another atm path']);
            end
            legend('-DynamicLegend');
          end
        end
        
    end
    tmp = tmp(round(end/2)+1); % TODO: WHO SAYS THAT'S THE CENTER??
    
    %% perform forward-abel transform on the modeled radiance to mimic the sensor's operation:
    %{
    e_eff = GasHelper.ApplyForwardAbel(e_eff,1,true);
    
    mask = isnan(e_eff) | isnan(R_est); % union of nans
    e_eff(mask) = nan;
    R_est(mask) = nan;
    %}
    %% compare the model to the measurement:
    %{
    figure; imagesc(e_eff); title(['Model''s Emission map @ iteration ',num2str(k)]); colorbar;
    figure; imagesc(R_est); title('Abel inverted measurement''s radiation'); colorbar;
    figure; imagesc(mask); title(['nan mask @ iteration ', num2str(k)]);
    figure; imagesc(FA); title(['Forward Abel of models''s Emission map @ iteration ',num2str(k)]); colorbar;
    %}    
    %% Check convergence and update guess:
    err = tmp - tmp_measurement;
    err_norm_vec(k) = norm(err);
    
    if (norm(err) < iterative_Convergence_tol) || (numel(calcTemps_arr) == 1)
      break;
    end
    if DO_BINARY_SEARCH % Apply binary search to find temperature:
      if LU_temp_ind==1 % we've reach 2 elemets array
        calcTemps_arr = calcTemps_arr(1);
      elseif norm(tmp) > norm(tmp_measurement) % search in the lower temperatures
        calcTemps_arr = calcTemps_arr(1:LU_temp_ind-1); % update search range
      else
        calcTemps_arr = calcTemps_arr(LU_temp_ind+1:end); % update search range
      end
      LU_temp_ind = round(numel(calcTemps_arr)/2); % update new guess index
    else % sweep over entire temperature candidates:
      LU_temp_ind = LU_temp_ind + 1;
    end

  end % end of iterative process
  
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%% Here we assume the outlet temperature have been found correctly: %%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Algorithm to resolve the full temperature field, knowing the outlet temperature:
%%% 1. Apply T2R over the modelded temperature field.
%%% 2. Compare R 2 Camera's R.
%%% modify model accordingly.
  [~,ind_T_recovered] = min(err_norm_vec); % the assumed index of the recovered temperature
  
  figure;
  plot(T_guesses, err_norm_vec); title('||err|| Vs Guessed temperature [C]'); hold on;
  plot(T_guesses(ind_T_recovered), err_norm_vec(ind_T_recovered), 'r*'); hold off;
  suptitle(['Measured temperature = ',tempStr,'^oC, CO_2 concentration = ',num2str(CO2_mfr/tot_mfr)]);
  
  %% Apply calibration (R2T) with the final parameters:
  % First, get the full image of e_eff:
  avgTBlocks = C2K(params.Tamb_C + (T_guesses(ind_T_recovered) - params.Tamb_C).*theta_model_Blocks);
  avgCBlocks = GasHelper.imFromFilteredBlocks(C_model, bSize);
  [e_eff] = GasHelper.interpHITRANData(avgTBlocks, avgCBlocks, F);
  
  % Crop parameters images to multiple of block size,if needed:
  arr = {R_est, trans_eff, Kw, a0};
  if NUM_CALIB_PARAMS_2_OR_3 == 3
    arr = {arr, a1};
  end
  ROI_arr = GasHelper.cropROIFromImagesCellArray(1:ROI_rows, ROI_cols, arr);
  R_est = ROI_arr{1};
  trans_eff = ROI_arr{2};
  Kw = ROI_arr{3};
  a0 = ROI_arr{4};
  if NUM_CALIB_PARAMS_2_OR_3 == 3
    a1 = ROI_arr{5};
  end
  
  if HITRAN_CALC_RADIANCE % scale the calculated radiance to photon flux. TODO: even needed?!?
    e_eff = GasHelper.HITRAN_radiance2Photon_Radiance(e_eff, filterParams.filter_CWL);% converting filter_CWL in [nm] to [cm^-1]
    e_eff = GasHelper.photons_flux2R_camera(e_eff, solid_angle, detector_area_cm2);
  end

  % solve the quadratic equation Y = p3+ln(e_eff)+ln(trans_eff) + p2*X + p1*X^2, for X = 1/T & Y = ln(R_est)  
  if HITRAN_CALC_RADIANCE
    if NUM_CALIB_PARAMS_2_OR_3 == 3
      T_rec_C = GasHelper.R2T_rec(e_eff, T, ones(size(e_eff)), trans_eff, Kw*solid_angle*detector_area_cm2, a0, a1);
    else
      T_rec_C = GasHelper.R2T_rec(e_eff, T, ones(size(e_eff)), trans_eff, Kw*solid_angle*detector_area_cm2, a0);
    end
  else
    if DO_ABEL_INVERSION % Apply Abel inversion on the measurement R
      [final_R, new_rows, new_cols, symAxis] = GasHelper.InverseAbelImage(R_est, m2pixel);
    else % Just for DEBUG - Calc R based on simulated emissivities & temps - final T should be similar to 'avgTBlocks'
      if NUM_CALIB_PARAMS_2_OR_3 == 3
        final_R = GasHelper.T2R(avgTBlocks, T, e_eff, trans_eff, Kw, a0, a1);
      else
        final_R = GasHelper.T2R(avgTBlocks, T, e_eff, trans_eff, Kw, a0);
      end
      new_rows = 1:size(final_R,1);
      new_cols = 1:size(final_R,2);
    end
    % Crop parameters images to multiple of block size,if needed:
    arr = {e_eff, trans_eff, Kw, a0};
    if NUM_CALIB_PARAMS_2_OR_3 == 3
      arr = {arr, a1};
    end
    ROI_arr = GasHelper.cropROIFromImagesCellArray(new_rows, new_cols, arr);
    e_eff = ROI_arr{1};
    trans_eff = ROI_arr{2};
    Kw = ROI_arr{3};
    a0 = ROI_arr{4};
    if NUM_CALIB_PARAMS_2_OR_3 == 3
      a1 = ROI_arr{5};
    end
    
    if NUM_CALIB_PARAMS_2_OR_3 == 3
      T_rec_C = GasHelper.R2T_rec(final_R, T, e_eff, trans_eff, Kw, a0, a1);
    else
      T_rec_C = GasHelper.R2T_rec(final_R, T, e_eff, trans_eff, Kw, a0);
    end
  end
  GasHelper.plotIm(T_rec_C,'Reconstructed T[^oC] using blocks of mean emissivities');
  h_cl = GasHelper.plotIm(flipud(T_rec_C(:,symAxis)),'Reconstructed T[^oC] along the Centerline (diameters)',(1:size(T_rec_C,1))/D_nozzle_pixels);
  GasHelper.plotIm(K2C(avgTBlocks),'Reconstructed T[^oC] using simulation model of the jet alone');
  GasHelper.plotIm(K2C(flipud(avgTBlocks(:,symAxis))),'Reconstructed T[^oC] using simulation along the Centerline (diameters)',(1:size(avgTBlocks,1))/D_nozzle_pixels,h_cl);
  GasHelper.surfIm(T_rec_C,'Reconstructed T[^oC] using blocks of mean emissivities');
  GasHelper.histIm(T_rec_C,1000, 'Histogram of Reconstructed T[^oC]');
  
  % Filtering a little bit:
  T_rec_C(T_rec_C < 0 ) = nan;
  T_rec_C_filtered = GasHelper.filterPrctile(T_rec_C, 1);
  GasHelper.plotIm(T_rec_C_filtered, 'Filtered T_rec[^oC]');
  GasHelper.plotIm(flipud(T_rec_C_filtered(:,symAxis)),'Reconstructed filtered T[^oC] along the Centerline (diameters)',(1:size(T_rec_C_filtered,1))/D_nozzle_pixels);
  GasHelper.surfIm(T_rec_C_filtered, 'Filtered T_rec[^oC]');
  
  input('\n\nFinished. Press any key to quit.\n','s');
  
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  %% Another iteration with refined calibration parameters, using OTSU th.
  if REFINE_CALIB_PARAMS
    % at this point, temperature histogram is assumed to have two well distinced areas.
    if DEBUG
      GasHelper.histIm(T_rec_C,1000, 'Histogram of Reconstructed T[^oC]');
    end
    good_idx = otsu(T_rec_C); % output: image of 3 lvls: 0(nans), 1 ,2
    good_idx = good_idx > 1;
    Kw_good = Kw(good_idx);
    a0_good = a0(good_idx);
    % Set mean value of "good" pixels to the "bad" ones:
    Kw(~good_idx) = nanmean(Kw_good(:));
    a0(~good_idx) = nanmean(a0_good(:));
    if NUM_CALIB_PARAMS_2_OR_3 == 3
      a1_good = a1(good_idx);
      a1(~good_idx) = nanmean(a1_good(:));
      T_rec_C = GasHelper.R2T_rec(final_R, T, e_eff, trans_eff, Kw, a0, a1);
    else
      T_rec_C = GasHelper.R2T_rec(final_R, T, e_eff, trans_eff, Kw, a0);
    end
    GasHelper.plotIm(T_rec_C,'Reconstructed T[^oC] after 2nd iteration of updating "bad" pixels');
  end
  
end