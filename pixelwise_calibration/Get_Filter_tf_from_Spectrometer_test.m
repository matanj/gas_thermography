%% Determining Filter's tf from the spectrometer measurements of a BB with and w/o filter
%% NOTE: The two measurement are assumed to have same bias (taken from the filter measurement)
%%  and the BB is linearly interpolated.
%%  Also note the 
clear; close all;

[~,txt,data_BB]  = xlsread('Z:\Matan\Gas measurements\Calsys1200BB\15_12_1\1200BB_400C_0_5mm_bothSlits.xlsx');
data_BB = data_BB(2:end, 1:2); % remove leading information rows
data_BB = cell2mat(data_BB);
wl_bb = data_BB(:,1);
I_bb = data_BB(:,2);

[~,txt,data_filter]  = xlsread('Z:\Matan\Gas measurements\Calsys1200BB\15_12_1\With Filters\15_12_2\1200BB_400C_4370nm_step1nm.xlsx');
data_filter = data_filter(2:end, 1:2); % remove leading information rows
data_filter = cell2mat(data_filter);
wl_filter = data_filter(:,1);
I_filter = data_filter(:,2);
I_filter = I_filter - min(I_filter);

I_bb = I_bb - min(I_filter);

I_BB_interp = interp1(wl_bb,I_bb, wl_filter);

% assuming same bias etc.:
transmission = 100 * I_filter ./ I_BB_interp; % in [%] to match rest of fits
figure; plot(wl_filter, transmission); title('filter transmission');

figure; plot(wl_bb, I_bb);

tail_left = 0.5;
tail_right = 0.48;
tail = (tail_right + tail_left) /2;

transmission_modified = transmission;
transmission_modified(1:135) = tail;
transmission_modified(203:end) = tail;

% Fit to Gaussian:
gaussEqn = 'a*exp(-((x-b)/c)^2)+d';
startPoints = [max(transmission_modified) 4.36 0.01 0.5];
f = fit(wl_filter,transmission_modified,gaussEqn, 'Start',startPoints);
figure; plot(f, wl_filter, transmission_modified); title('Gaussian fit'); 
% Spline fit:
[filter_fit, goodness, output] = fit(wl_filter, transmission_modified, 'smoothingspline');
figure; plot(filter_fit, wl_filter, transmission_modified); title('Smoothing spline fit'); 

save('Filter_cfit_from_Spectrometer.mat','f');