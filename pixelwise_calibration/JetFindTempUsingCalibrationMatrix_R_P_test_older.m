%% Use per-pixel Calibration matrix to retrieve the Jet temperature
%%
% *Synopsis*
%%
%% THIS test differ from 'JetFindTempUsingCalibrationMatrix' in that when checking the calibration on the test case,
%% Its allows radiation estimation from MultiITtoRP(), instead of just DL/IT.

function JetFindTempUsingCalibrationMatrix_R_P_test
clc; close all;
dbstop if error
warning off
format short
dbstatus
%% Assumptions: 1) Order experiment of BGs->IMs -> BGs -> ...
%%              2) unique experiment cases.
%%              3) each experiment case hase same #files.
%%              4) All images are of same size (row, cols)
%%              5) New images to be used with the caibration should match its image's size AND PIXEL ALLIGNMENT!

%% OPTIONS
DEBUG = false; % show figures;
CALC_CALIB_PARAMS = true; % if false, use existing set for testing
e_BB = 0.97; % emissivity of the Black Body used for calibration. assumed constant spatial-wise and spectrum-wise...
FILTER_PARAMETERS_TH = 0; % percentile-based filtering over calculated fit parameters. '0' for filtering by std
FILTER_PARAMETERS_STD = 20;% std-based filtering over calculated fit parameters. '0' for no filtering
TEST_CALIBRATION_ON_EXTERNAL_CASE = true;
APPLY_ATM_MODEL = true; % apply HITRAN model of atmospheric transmittance when retreiving measurement's temperature
APPLY_ATM_MODEL_IN_CALIBRATION = true;
USE_MULTIIT2RP = true; % whether to use that, or simply DL/IT for a single IT (maximal)
ALL_PIXELS_SEE_BB = true; % for calibrating only a subset of the image
FIX_JET_ANGLE = false; % fix jet's alignment to be vertical
APPLY_ABEL = true; % apply abel transformation.
nLevels = 30; % otsu's number of levels for label the jet image, for radius estimation
NOZZLE_IN_IMAGE = true; % if the images contain the nozzle, select it and get m2pixel, etc.
REFINE_CALIB_PARAMS = true; % set mean value of "good" pixels to the "Bad" pixels that show absord temperature.
bSize = 6; % Block size [pixels] 

%% EXPERIMENT RELATED:
atm_OPL = 1.5; % Atmospheric path in [m]
tot_mfr = 2; %[g/s]
CO2_mfr = 2; %[g/s]
D_nozzle = 1e-2; % Nozzle's diameter [m]
atm_co2_concentration = 500; % [ppmv]
atm_T = 26; % Atmospheric temperature [C]
Tjet = 200; % Jet orifice temperature [C] (measured by TC, NOT the one stated in file's name)
%CO2 filter related:
filterParams.filter_CWL = 4370; % CO2 filter CWL [nm]
filterParams.filter_FWHM = 20; % CO2 filter FWHM [nm]
filterParams.th_l = 0.1;
filterParams.th_h = 0.9;
%%
atm_OPL = atm_OPL*100; % to [cm]
atm_co2_concentration = atm_co2_concentration*1e-6; % ppmv to fraction
D_nozzle_pixels = 60;%[pixels]
m2pixel = D_nozzle_pixels / D_nozzle; % meter to pixel conversion

CalibFilePath = [pwd,'\pixelwise_calibration\JetBBCalibration_OUTPUT_No_ignored_ITs.mat']; % default
outputName = 'CalibrationParams.mat';
%% Default test case and related files:
baseDir = 'Z:\FLIR_Experimental\10-07-16_Jet_\';
imName = [baseDir, 'G001_T500_F4370_IT2693.6_IM_f.ptw'];
% bgName = [baseDir, 'G002_T500_F4370_IT2693.6_BG_f.ptw'];
atm_trans_calib_FileName = 'C:\Users\matanj\Documents\PythonScripts\Atmospheric_Transmittance_T27C_C630ppm_L150cm_RH51.mat'; % path used in the calibration measurements
%% Constants:
h = 6.62606957e-34; %[J*s]
c = 299792458; %[m/s]
Kb = 1.3806488e-23; %[J/K]
% C2 = h*c/Kb;
% C1 = 2*h*c^2;
C2K = @(x) x + 273.15; % Celsius to Kelvin
% K2C = @(x) x - 273.15; % Kelvin to Celsius

addpath(genpath('Z:\Matan\'), '-end');

if CALC_CALIB_PARAMS % conditionally calculate calibration parameters:
  %% Get data: {DL, DL_normalized, experimentCasesArr}
  [CalibFileName, CalibFilePath] = uigetfile({'*.mat','MATLAB data'},'Select Calibration matrix',CalibFilePath);
  if isempty(CalibFileName)
    error('invalid folder path, or claibration matrix there...');
  end
  load([CalibFilePath, CalibFileName]);
  
    %Rotrou 2006: NIR thermography with silicon FPA, [17]:
  % For each pixel we find the following 3 (or 4) params:
  %   Kw, a0, a1, (a2), by this fit:
  %  ln(DL_normalized) = ln(Kw) - C2./T/.lambda_x =~  ln(Kw) - C2.*(a0 + a1.*T + ...)
  % log(Kw)-C2*(a0+a1.*X+a2.*X.^2) yields good results..
  %%
  % Problems:
  % 1) Need to use Plate BB for this calibration - all pixels should see the
  %     same radiation
  % 2) No radiation calibration - it is for DL...
  % 3) Model seems Weinish (https://en.wikipedia.org/wiki/Sakuma-Hattori_equation, 9th form)
  %     but my range might not hold Wein's assumption!!!!!!!!!!!!!!!!!
  
  %% estimate R from DL = R*IT^P model
  % DL format:  DL(temps, ITs, rows, cols)
  T = C2K(experimentCasesArr.temps); % T[K] used in the calibration
  ITs = squeeze(experimentCasesArr.IT); % (temps, ITs)
  % preallocation:
%   R_est = nan(size(DL,1), size(DL,3), size(DL,4)); % (rows, cols)
  R_est = nan(size(T));
  progressbar('Temp');
  for indT = 1:numel(T) % Loop over temperatures
    DLs = permute(squeeze(DL(indT,:,:,:)), [2,3,1]); % MultiITtoRP expects: DL(row,cols,ITs)
    [R_est_,~] = MultiITtoRP(double(squeeze(ITs(indT,:))), DLs);
    R_est(indT) = HistogramHelper.getFittedGauss3Peak(R_est_,[],[],[],3); % Get representative R from the R image
    progressbar(indT/numel(T));
  end
  clear DLs ITs R_est_;

  X = max(T)./T; % the independant variable. scaled to ease estimation.

  %%
  % Formulate as a M*P = Y system , where P is the desired parameters vector.
  % We have: ln(DL) =  ln(Kw)-C2*(a0/T + a1/T^2)
  % Denote Y = ln(DL), X = 1/T. We write:
  % Y = ln(Kw) - a0*X + a1*X.^2.
  % Denote: p3 = log(Kw), p2 = -C2*a0, p1 = -C2*a1,  we get:
  % (*) Y = p3 + p2*X + p1*X^2 ======>    Y = [X.^2 X ones(size(X))]*P
  % =====> P  =  M \ Y
  
  % Using (*), we can afterward solve for T :
  % X1,2 = (-p2 +- sqrt(p2^2 - 4*p1*(p3-Y)))/2/p1
  % ===> T = 1/X = 2*p1 ./ (-p2 +- sqrt(p2^2 - 4*p1*(p3-ln(DL_n))))
  
  % And if the object is of known "extended effective emmisivity" (my term),
  % we have: ln(DL) =  ln(Kw) - ln(e) -C2*(a0/T + a1/T^2)
  % that is, (**) Y = p3 - ln(e) + p2*X + p1*X^2
  % solving we get: X1,2 = (-p2 +- sqrt(p2^2 - 4*p1*(p3-ln(e)-Y)))/2/p1
  
  Y = log(R_est) - log(e_BB)*ones(size(R_est)); % measurement - log of estimated R, in addition to BB's emissivity
  
  if APPLY_ATM_MODEL_IN_CALIBRATION % Apply atmospheric transmittance:
    [AtmFileName, AtmFilePath] = uigetfile({'*.mat','MATLAB data'},'Select atm transmittance matrix',atm_trans_calib_FileName);
    atm_trans_FileName = [AtmFilePath,AtmFileName];
    if isempty(atm_trans_FileName)
      error('invalid path, or transmittance matrix there...');
    end
    trans_eff = getEffectiveTransmittance(filterParams, atm_trans_FileName);
    Y = Y - log(trans_eff)*ones(size(R_est));
  end
  
  M = [X.^2 X ones(size(X))]; % M is probably ill conditioned!!
  P = M \ Y;
  Kw = P(3);
  a0 = P(2);
  a1 = P(1);
  
  Kw = Kw*ones(size(DL,3), size(DL,4));
  a0 = a0*ones(size(DL,3), size(DL,4));
  a1 = a1*ones(size(DL,3), size(DL,4));

  % Save coefficients per pixel:
  save(outputName, 'Kw', 'a0','a1', 'T');
  
  %   TODO: try X = 1/lambda/T for my known lambda, assuming effective
  %   lambda doesn't change in my IR range.

else
  [ParamsFileName, ParamsFilePath] = uigetfile({'*.mat','MATLAB data'},'Select Calibration parameters set',[pwd,'\CalibrationParams.mat']);
  if isempty(ParamsFileName)
    error('invalid folder path...');
  end
  load([ParamsFilePath, ParamsFileName]);
end

%% Filter results by percentile, or by STD:
if FILTER_PARAMETERS_TH > 0 % by percentile
  Kw = filterPrctile(Kw, FILTER_PARAMETERS_TH);
  a0 = filterPrctile(a0, FILTER_PARAMETERS_TH);
  a1 = filterPrctile(a1, FILTER_PARAMETERS_TH);  
else % by std
  Kw = filterStds(Kw, FILTER_PARAMETERS_STD);
  a0 = filterStds(a0, FILTER_PARAMETERS_STD);
  a1 = filterStds(a1, FILTER_PARAMETERS_STD);  
end
totNaNMask = isnan(Kw) & isnan(a0) & isnan(a1);
Kw(totNaNMask) = nan;
a1(totNaNMask) = nan;
a0(totNaNMask) = nan;
if DEBUG
  figure;
  subplot(2,3,1);  imagesc(Kw); colorbar; title('Kw');
  subplot(2,3,2); imagesc(a0); colorbar; title('a0');
  subplot(2,3,3); imagesc(a1); colorbar; title('a1');
  subplot(2,3,4); imagesc(~isnan(Kw)); colorbar; title('"Valid" Kw');
  subplot(2,3,5); imagesc(~isnan(a0)); colorbar; title('"Valid" a0');
  subplot(2,3,6); imagesc(~isnan(a1)); colorbar; title('"Valid" a1');
  figure; imagesc(totNaNMask); colorbar; title('total mask');
  figure;
  subplot(1,3,1);  histogram(Kw,500); title('Kw');
  subplot(1,3,2);  histogram(a0,500); title('a0');
  subplot(1,3,3);  histogram(a1,500); title('a1');
end

%% Test the calibration:
if TEST_CALIBRATION_ON_EXTERNAL_CASE
  %Get test case:
  [imName, baseDir] = uigetfile({'*.ptw','FLIR images'},'Select image to test calibration',imName);
  if isempty(imName)
    error('invalid image');
  end
  [bgName, baseDir] = uigetfile({'*.ptw','FLIR images'},'Select bg to test calibration',baseDir);
  if isempty(bgName)
    error('invalid bg');
  end
  
  imPair = ImageBGPairExt([baseDir,imName]);
  imPair.setBG([baseDir,bgName]);
  subIm = mean(abs(imPair.getSubtracted()), 3);
  if ALL_PIXELS_SEE_BB
    objMask = true(size(subIm));
  else
    [~, object] = processImGetStats(subIm);%get meaningful object within the image
    objMask = true(imPair.IMG_Info.m_rows, imPair.IMG_Info.m_cols);%initial mask
    objMask(isnan(object)) = NaN;
  end
  
  %mask the image to maintain same size:
  DL_new = objMask.*subIm;
  % Normalize DLs map by IT:
  IT = double(imPair.IT)*1e6; %change IT to [us]
  DL_new(DL_new==0) = 1; % set minimal DL to 1 to avoid zero issues
  DL_new_n = DL_new ./ IT;
  
  %trim image to orifice
  if NOZZLE_IN_IMAGE
    figure; imagesc(subIm); colorbar; title('Select nozzle''s orifice & press a key to cntinue');
    set(gcf,'units','normalized','outerposition',[0 0 1 1]); % maximize figure for convienience
    h_nozzle = imdistline();
    pause;
    apiObj = iptgetapi(h_nozzle);
    teta = apiObj.getAngleFromHorizontal() %[deg]
    D_nozzle_pixels = ceil(apiObj.getDistance());%[pixels]
    m2pixel = D_nozzle_pixels / D_nozzle;
    pos_nozzle = apiObj.getPosition();
    DL_new_n = DL_new_n(1:floor(pos_nozzle(1,2)), :);
  end
  
  if FIX_JET_ANGLE
    %% Apply 2D affine rotating transformation to fix for the angle of the Jet.
    % Radon transformation is used estimate angle.
    %% TODO: should apply same to the calibration maps, or rotate back b4 using the calibration
    % NOTE: I trim the new image to the original size. 'theta' is assumed
    % small enough so that the skew is negligable.
%     DL_new_n_otsu = otsu(DL_new_n,10); % apply otsu thersholding
%     theta = 90 - DetectAngle(DL_new_n_otsu);
    S = 1; % scale factor
    theta =  90 - DetectAngle(DL_new_n); % '90' as desired jet is vertical
    tform = affine2d([S.*cosd(theta) -S.*sind(theta) 0; S.*sind(theta) S.*cosd(theta) 0; 0 0 1]);
    fixed_DL_new_n = imwarp(DL_new_n,tform,'fillvalues',0); %default is '0'. 

%     % DEBUG: check that x' = A^-1(A(x)) = x : 
%     tform2 = affine2d([S.*cosd(-theta) -S.*sind(-theta) 0; S.*sind(-theta) S.*cosd(-theta) 0; 0 0 1]);
%     DL_new_n_rec = imwarp(fixed_DL_new_n,tform2);
    

%     f2 = imrotate(DL_new_n,theta); Obselete!!
    if DEBUG
      figure; imagesc(DL_new_n); colorbar; title('original');
%       figure; imagesc(fixed_DL_new_n_otsu); colorbar; title('rotation: angle detected on otsu');
      figure; imagesc(fixed_DL_new_n); colorbar; title('rotation: angle detected on original');
    end
    fixed_DL_new_n(fixed_DL_new_n==0) = nan;% mask the rotation transformation by-product
    mask = ~isnan(fixed_DL_new_n);
    % get the inner image w/o transformation's by-products:
    % need to distinguish between angles signs since the inner rectangle
    % usually intersects each of the outer one's limits by more that one
    % pixel.
    if theta < 0 % rotate CW
      idx_row1 = find(mask(:,1),1,'last');
      idx_row2 = find(mask(:,end),1,'first');
      idx_col1 = find(mask(1,:),1,'first');
      idx_col2 = find(mask(end,:),1,'last');
    else % rotated CCW
      idx_row1 = find(mask(:,1),1,'first');
      idx_row2 = find(mask(:,end),1,'last');
      idx_col1 = find(mask(1,:),1,'last');
      idx_col2 = find(mask(end,:),1,'first');
    end
    min_row = min(idx_row1, idx_row2);
    max_row = max(idx_row1, idx_row2);
    min_col = min(idx_col1, idx_col2);
    max_col = max(idx_col1, idx_col2);
    DL_new_n = fixed_DL_new_n(min_row:max_row, min_col:max_col);
  end
  

  %% Apply calibration with Kw, a0, a1 maps.
  % First, crop object image to the calibration image, if needed:
  if ~(exist('rect','var') ) % if cropping wasn't done:
    [rowsCalib, colsCalib] = size(Kw);
    [rowsNew, colsNew] = size(DL_new_n);
    if rowsNew ~= rowsCalib || colsNew ~= colsCalib
      rows = 1:floor(pos_nozzle(1,2)); % TODO: what if NOZZLE_IN_IMAGE == false?
      cols = 1:size(DL_new_n,2);
    else
      rows = 1:size(DL_new_n,1);
      cols = 1:size(DL_new_n,2);
    end
  else % if 'rect' is present:
    rows = rect(2):rect(2)+rect(4);
    cols = rect(1):rect(1)+rect(3);
  end
  Kw = Kw(rows, cols);
  a0 = a0(rows, cols);
  a1 = a1(rows, cols);
   
  totNaNMask = isnan(Kw) | isnan(a0) | isnan(a1);
  DL_new_n(totNaNMask) = nan;
  
  %% Get the "effective emissivity" from HITRAN:
  % 1) TODO - I don't know my temperature, so I cant get the correct emissivity.
  % Need to iterate over T, until convergance...
  % 2) This might be the place to combine my model for the jet, for some
  % spatial resolution
  
  T0 = C2K(Tjet); % initial guess on centerline temperature
  
  wl_l = 4.1; %[um]
  wl_h = 4.4; %[um]
  
  wn_h = 1e4/wl_l; %[cm^-1]
  wn_l = 1e4/wl_h; %[cm^-1]
  
  %  emissivity = getEmissivityFromHITRAN(c0, T0, wl_range); % BELOW
  absorp_file = 'C:\Users\matanj\Documents\PythonScripts\Jet_absorption_spectrum_T_120C_OPL  1cm.txt';
  [absorp_file, absorp_file_path] = uigetfile({'*.txt','HAPI generated .txt files'},'Select input HAPI file',absorp_file);
  absorp_file = [absorp_file_path absorp_file];
  
  fileID = fopen(absorp_file,'r');
  HITRAN_file_format = '%f %f';
  HITRAN_data = textscan(fileID, HITRAN_file_format);
  fclose(fileID);
  wnGrid = HITRAN_data{1};%[cm-1]
  absorp_spectrum_wn = HITRAN_data{2};
  absorp_spectrum_wl = flipud(absorp_spectrum_wn); %No need to fix absorpution as in intensity, as it is not "per unit of wn/wl"
  wnArray=flipud(wnGrid);
  WL = wnArray.\1E4; % [cm-1] to [um]
  
  planck_wn = 2e8*h*c^2*wnArray.^3 ./ (exp(100*h*c*wnArray./Kb./T0)-1);
  e_eff = trapz(absorp_spectrum_wn.*planck_wn) ./ trapz(planck_wn);

  trans_eff = ones(size(DL_new_n));
  if APPLY_ATM_MODEL % if needed
    [AtmFileName, AtmFilePath] = uigetfile({'*.mat','MATLAB data'},'Select atm transmittance matrix',atm_trans_calib_FileName);
    atm_trans_FileName = [AtmFilePath,AtmFileName];
    if isempty(atm_trans_FileName)
      error('invalid path, or transmittance matrix there...');
    end
    trans_ef = getEffectiveTransmittance(filterParams, atm_trans_FileName);
    trans_eff = trans_ef*trans_eff;
  end

  % solve the quadratic equation Y = p3-ln(e) + p2*X + p1*X^2, for X = 1/T:
  T_rec_C = R2T_rec(DL_new_n, T, e_eff, trans_eff, Kw, a0, a1); % TODO: reciprocity assumption here!!

  matchStr = regexp(imName,'_T+[0-9]+[0-9]+[0-9]','match');
  tempStr = regexprep(matchStr{1},'_T','');
  Tref = str2double(tempStr);
  titleStr = ['Reconstructed T[^oC], T_{ref}=',num2str(Tref),'[^oC]'];
  
  %% Finally, plot the temperature map:
  if DEBUG
    figure; suptitle(titleStr);
    subplot(1,2,1); imagesc(K2C(T_rec_C)); colorbar;
    T_rec_C(T_rec_C > Tref+100) = nan;
    subplot(1,2,2); histogram(T_rec_C,1000);
    figure; surf(T_rec_C,'EdgeColor','none');
  end
  %% apply model of temperature & concentration profiles
  params.DEBUG = DEBUG;
  params.calcFromVirtualOrigin = false;
  params.displayGraphs = false;
  params.D = D_nozzle;
  params.tot_mfr = tot_mfr; % Total mass flow rate, [g/s]
  params.co2_mfr = CO2_mfr; % CO2 mass flow rate, [g/s]
  params.Tamb_C = atm_T; % ambient temperature, [C]
  params.Tjet_C = Tjet; %Jet's outlet temperature, [C]
  params.Camb = atm_co2_concentration; % CO2 concentration in the ambient
  params.P0 = 101325; %[Pa]. Pressure @ outlet is 1 atm = 101325[Pa]

  [~,idxMax] = max(DL_new_n(end,:));
  [C_model, T_model, depths] = GenerateTemperatureConcentrationModelsFit2image(DL_new_n, m2pixel, size(DL_new_n,1), idxMax, params);
  %TODO: maybe need to set in depths some value instead of 'nan'
  if DEBUG
    figure; imagesc(DL_new_n); colorbar
    figure; imagesc(T_model); colorbar
    figure; surf(C_model,'EdgeColor','none'); title('Concentration model');
    figure; surf(T_model,'EdgeColor','none'); title('Temperature model');
  end

%% Divide to blocks the apparent plane (XY):
  % prepare a pixel-by-pixel emissivity map:
  e_map = ones(size(DL_new_n));
  % currently I have same mean value in a block.
  %chop image to match block size:
  imSize = size(T_model);
  T_model = T_model(1:bSize*floor(imSize(1)/bSize), 1:bSize*floor(imSize(2)/bSize));
  C_model = C_model(1:bSize*floor(imSize(1)/bSize), 1:bSize*floor(imSize(2)/bSize));
  depths = 100*depths(1:bSize*floor(imSize(1)/bSize), 1:bSize*floor(imSize(2)/bSize)); %also convert OPL to [cm]
  %devide to blocks
  T_blocks = divideIm2Blocks(T_model, bSize);
  C_blocks = divideIm2Blocks(C_model, bSize);
  depths = divideIm2Blocks(depths, bSize);
  % set in each block the mean value of it:
  setMeanVal = @(block) nanmean(block(:))*ones(size(block));
  avgTBlocks = cellfun(setMeanVal, T_blocks,'UniformOutput', false);
  avgCBlocks = cellfun(setMeanVal, C_blocks,'UniformOutput', false);
  avgDepthsBlocks = cellfun(setMeanVal, depths,'UniformOutput', false);
  %Build reconstructed images from the filtered blocks:
  avgTBlocks = cell2mat(avgTBlocks);
  avgCBlocks = cell2mat(avgCBlocks);
  avgDepthsBlocks = cell2mat(avgDepthsBlocks);
  if DEBUG
    figure; imagesc(avgTBlocks); colorbar; title('avg T model');
    figure; imagesc(avgCBlocks); colorbar; title('avg C model');
    figure; imagesc(avgDepthsBlocks); colorbar; title('avg depths model');
  end
  
  % set T,C images to have same jet dimentions as the depths image: 
  total_mask = ~isnan(avgDepthsBlocks);
  avgTBlocks = avgTBlocks .* total_mask;
  avgCBlocks = avgCBlocks .* total_mask;
  avgDepthsBlocks(~total_mask) = 0;
  
  %% Find emissivity map using HITRAN, in each block:
  % Load the HAPI calculated values:
  % using results of the script C:\Users\matanj\Documents\PythonScripts\getAbsorpCoeff.py
  % TODO: save the calculated triplets in the script, and its inputs should
  % be the limits of calculation.
  meanAbsorpMatrixFilePath = 'C:\Users\matanj\Documents\PythonScripts\meanAbsorpMatrix.mat';
  [meanAbsorpMatrixFileName, meanAbsorpMatrixFilePath] = uigetfile({'*.mat','MATLAB data'},'Select mean Absorp Matrix file',meanAbsorpMatrixFilePath);
  if isempty(meanAbsorpMatrixFileName)
    error('invalid folder path, or matrix there...');
  end
  load([meanAbsorpMatrixFilePath,meanAbsorpMatrixFileName]);

  %perform 3d interpolation of the HITRAN data:
  calcTemps = linspace(C2K(params.Tamb_C), 600, 10);
  calcConcenrations = linspace(atm_co2_concentration, CO2_mfr/tot_mfr, 10);
  calcOPLs = linspace(0.8, 1.6, 10);
  
  Temps = unique(avgTBlocks); Temps = Temps(Temps>0);
  Concenrations = unique(avgCBlocks); Concenrations = Concenrations(Concenrations>0);
  OPLs = unique(avgDepthsBlocks); OPLs = OPLs(OPLs>0); % also convert to [cm]
  
  [cT, cC, cO ] = meshgrid(calcTemps,calcConcenrations,calcOPLs);
  [Tq, Cq, Oq] = meshgrid(Temps,Concenrations,OPLs);

  meanAbsorptInterp = interp3(cT, cC, cO, meanAbsorp, Tq, Cq, Oq);
  
  % apply interpolation to get the emissivity map:
  for i = 1:size(avgTBlocks,1)
    for j = 1:size(avgTBlocks,2)
      if avgTBlocks(i,j) == 0
        e_map(i,j) = nan;
      else
        e_map(i,j) = meanAbsorptInterp( Temps==avgTBlocks(i,j), Concenrations==avgCBlocks(i,j), OPLs==avgDepthsBlocks(i,j) );
      end
    end
  end
%   e_eff = trapz(absorp_spectrum_wn.*planck_wn) ./ trapz(planck_wn);
  e_map(e_map==1) = nan;
  e_eff = e_map;
 
  %% rotate back emissivity map to align with the calibration maps:
%   tform2 = affine2d([S.*cosd(-theta) -S.*sind(-theta) 0; S.*sind(-theta) S.*cosd(-theta) 0; 0 0 1]);
%   e_eff = imwarp(e_map,tform2);
  % trim & translate to original image coordinates:
 
  
  %% Apply calibration per pixel with the emissivity map:
  if USE_MULTIIT2RP % estimate radiation from DL = R*IT^P
    % first, get R from multiIT measurements:
    totalFilesList = dir([baseDir,'/*.ptw']);
    if isempty(totalFilesList)
      error('invalid folder path, or no .ptw files there...');
    end
    totalFilesList = {totalFilesList(:).name}';
    [props.G,props.T,props.CWL,props.IT,props.isBG,props.name] = parseFilename(totalFilesList); %divide list to params
    [caseList,~,~] = getMatchingCase(totalFilesList, props, {'T'}, {str2double(tempStr)});
    [IMList,~,~] = getMatchingCase(caseList, props, {'isBG'}, {false});
    [BGList,~,~] = getMatchingCase(caseList, props, {'isBG'}, {true});

    for indIMPair = 1:numel(BGList)
      imPair = ImageBGPairExt([baseDir,IMList{indIMPair}]);
      imPair.setBG([baseDir,BGList{indIMPair}]);
      subIm = mean(abs(imPair.getSubtracted()), 3);
      ITs(indIMPair) = imPair.IT*1e6; %change IT to [us]
      DLs(indIMPair,:,:) = subIm;
    end
    DLs = permute(DLs, [2,3,1]);
    [R_est,~] = MultiITtoRP(double(ITs), DLs);
    R_est = R_est(rows,cols);
  else
    R_est = DL_new_n; % estimate R from reciprocity: R ~~ DL/IT 
  end
  
  if APPLY_ABEL % Apply Abel inversion over R_est
    % Abel inversion code parameters
    upf = 10;
    plot_results = 0;
    lsq_solve = 0;
    inPixels = 1;
    normalize = 0;  % normalize plots
    normBy2R = 1;   % normalize measurements plot by 2R
    %% Calc the symmetry axis
     %% TODO: R_est has NaN here!!!
    fig = figure; imagesc(R_est); impixelinfo; title('Abel Inversion: Select area, then confirm by double clicking');
    set(gcf,'units','normalized','outerposition',[0 0 1 1]); % maximize figure for convienience
    [lines, rect] = imcrop;
    %round coordinates, as they are fractions:
    rect(1:2) = ceil(rect(1:2));
    rect(3:4) = floor(rect(3:4));
    [M,N] = size(lines);
    [~,Idx] = max(lines,[],2);
    rows = (rect(2):rect(2)+rect(4))';
    cols = (rect(1):rect(1)+rect(3))';
    colsMax = rect(1)+Idx-1;
    avgCol = round(mean(colsMax));
    % Just for plot purpose:
    imAxisMarked = R_est;
    imAxisMarked(rows,avgCol) = min(R_est(:));
    figure(fig); imagesc(imAxisMarked);impixelinfo;
    
    % Apply Abel inversion per row
    new_lines = zeros(size(lines));
    jetMaskOtsu = otsu(lines,nLevels);
    jetMask = true(size(lines));
    jetMask(jetMaskOtsu == 1) = false;
    if DEBUG
      figure; imagesc(jetMask); colorbar;
    end
    
    for l=1:M % loop on each row and perform abel inversion
      line = lines(l,:);
      [~,Idx] = max(line,[],2);
      %estimate diameter of the jet:
%       D = numel(jetMask(l,:) > 0);
      ll = find(jetMask(l,:)>0, 1, 'first');
      rl = find(jetMask(l,:)>0, 1, 'last');
      for ii = 1:2 % loop on each side of the jet (for verification)
        if ii == 2 % left side
          R = Idx-ll+1;
          f_in = flipud(line(ll:Idx)');
          % The actual inversion:
          [ f_rec , X ] = abel_inversion(f_in,R,upf,plot_results,lsq_solve,inPixels);
          new_lines(l,ll:Idx-1) =  flipud(f_rec(2:end));
        else % right side
          R = rl-Idx+1; % radius
          f_in = line(Idx:rl)';
          % The actual inversion:
          [ f_rec , X ] = abel_inversion(f_in,R,upf,plot_results,lsq_solve,inPixels);
          new_lines(l,Idx:rl) = f_rec;
        end
      end
    end
    R_est = new_lines; % set the new image after abel inversion
    R_est(R_est==0) = nan;
    figure; imagesc(R_est); colorbar; title('image after Abel Inversion');
    
    a0 = a0(rows,cols);
    a1 = a1(rows,cols);
    Kw = Kw(rows,cols);
    e_eff = e_eff(rows,cols);
  end
  
  if APPLY_ATM_MODEL % apply HITRAN transmittance model
    [AtmFileName, AtmFilePath] = uigetfile({'*.mat','MATLAB data'},'Select atm transmittance matrix',atm_trans_calib_FileName);
    atm_trans_FileName = [AtmFilePath,AtmFileName];
    if isempty(atm_trans_FileName)
      error('invalid path, or transmittance matrix there...');
    end
    trans_eff = getEffectiveTransmittance(filterParams, atm_trans_FileName);
  else
    trans_eff = 1;
  end  
  trans_eff = trans_eff*ones(size(R_est)); % assuming constant in all of the volume
  
  %% solve the quadratic equation Y = p3+ln(e_eff)+ln(trans_eff) + p2*X + p1*X^2, for X = 1/T & Y = ln(R_est)
  T_rec_C = R2T_rec(R_est, T, e_eff, trans_eff, Kw, a0, a1);
  figure; imagesc(T_rec_C); colorbar; title('Reconstructed T[^oC] using blocks of mean emissivities');
%   figure; surf(T_rec_C); view(181,15); title('Reconstructed T[^oC] using blocks of mean emissivities');
  
  %% Another iteration with refined calibration parameters, using OTSU th.
  if REFINE_CALIB_PARAMS
    % at this point, temperature histogram is assumed to have two well distinced areas.
    if DEBUG
      figure; histogram(T_rec_C,1000);
    end
    good_idx = otsu(T_rec_C); % output: image of 3 lvls: 0(nans), 1 ,2
    good_idx = good_idx > 1;
    Kw_good = Kw(good_idx);
    a0_good = a0(good_idx);
    a1_good = a1(good_idx);
    % Set mean value of "good" pixels to the "bad" ones:
    Kw(~good_idx) = nanmean(Kw_good(:));
    a0(~good_idx) = nanmean(a0_good(:));
    a1(~good_idx) = nanmean(a1_good(:));
    % Reconstruct Temperature again:
    T_rec_C = R2T_rec(R_est, T, e_eff, trans_eff, Kw, a0, a1);
    figure; imagesc(T_rec_C); colorbar; title('Reconstructed T[^oC] after 2nd iteration of updating "bad" pixels');
  end
  
end

end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Auxiliary functions:
function T_rec_C = R2T_rec(R_est, calib_T, e_eff, trans_eff, Kw, a0, a1)
% gets R_est, calib_T[K], effective emissivity, effected transmisivity, and calibraion params,
% and returns reconstructed temperatrure[C].
% solves the quadratic equation Y = p3+ln(e_eff)+ln(trans_eff) + p2*X + p1*X^2, for X = 1/T & Y = ln(R_est)
  if nargin < 7
    error('invalid args!');
  end
  K2C = @(temp) temp - 273.15;
  discriminent = a0.^2 - 4*a1.*( Kw+log(e_eff)+log(trans_eff)-log(R_est) );
  x1 = (-a0 + sqrt(discriminent) )./2./a1;
  x2 = (-a0 - sqrt(discriminent) )./2./a1;
  tol = 1e-10;
  assert(all(x1(~isnan(x1)))>tol && all(x2(~isnan(x2)))>tol && isreal(x1) && isreal(x2));
  T_rec = nan(size(R_est));
  % Get temperature map: take only T>0:
  T_rec(x1>0) = 1./x1(x1>0);
  T_rec(x1<0) = 1./x2(x1<0);
  T_rec = T_rec * max(calib_T); % since we have normalized by max(T) to improve the fit process
  T_rec_C = K2C(T_rec);
end

function [trans_eff] = getEffectiveTransmittance(filterParams, atmTransmittanceFileName)
  % get an "effective" transmittance in the atmosphere:
  load(atmTransmittanceFileName);
  numHWHM = 2;
%   filter_CWN = 1e7/filterParams.filter_CWL; %to [cm^-1]
  filter_HWHM = filterParams.filter_FWHM/2;
  filter_wn_l = 1e7/(filterParams.filter_CWL + numHWHM*filter_HWHM);
  filter_wn_h = 1e7/(filterParams.filter_CWL - numHWHM*filter_HWHM);
  wn_ROI_ind = wn >= filter_wn_l & wn <= filter_wn_h;
  wn_ROI = wn(wn_ROI_ind);
  % Crude approximation for the filter's response as a triange. TODO: replace by better
  th_l = filterParams.th_l; th_h = filterParams.th_h;
  filter_response = double(wn_ROI_ind);
  fr = [linspace(th_l,th_h,ceil(numel(wn_ROI)/2)), linspace(th_h,th_l,ceil(numel(wn_ROI)/2))];
  filter_response(filter_response>0) = fr;
%   filter_response = filter_response(1:numel(wn_ROI));
%   trans_eff = mean(transmittance(wn_ROI_ind).*filter_response);
  trans_eff = mean(transmittance(wn_ROI_ind).*filter_response(wn_ROI_ind));
end

function y = filterPrctile(x, p, val)
% Assigns 'val' to places where x is not in the percentile range 'p'. if
% 'val' not stated, 'NaN' is assigned by default.
  if nargin <2 || p<0 || p > 100
    error('invalid args!')
  end
  if nargin < 3
    val = nan;
  end
  x( x < prctile(x(:), p) | x > prctile(x(:), 100-p) ) = val;
  y = x;
end

function y = filterStds(x, nStds, val)
% Assigns 'val' to places where x is not in the range of nStds from its mean. if
% 'val' not stated, 'NaN' is assigned by default.
  if nargin <1
    error('invalid args!')
  end
  if nargin < 2
    nStds = 2;
    val = nan;
  end
  if nargin < 3
    val = nan;
  end
x( abs(x-nanmean(x(:))) > nStds*nanstd(x(:)) ) = val;
y = x;
end

function subBlocks = divideIm2Blocks(I, bSize)
% divide image to bSize*bSize blocks
  if nargin < 2 || bSize < 2
    error('invalid args');
  end
  s = size(I);
  nB = ceil(s/bSize);
  rowsVec = repmat(bSize, [nB(1) 1]);
  colsVec = repmat(bSize, [nB(2) 1]);
  subBlocks = mat2cell(I, rowsVec, colsVec);
end

function rotatedIm = nanimwrap(im,tform)
% apply 2D transformation on an image with nans. 
% this is WRONG: due to inevitable interpolation in the rotation
  if nargin < 2
    error('invalid args!');
  end
  minVal = nanmin(im(:));
  maxVal = nanmax(im(:));
  tempVal = minVal - abs(1/(maxVal-minVal));
  im(isnan(im)) = tempVal;
  rotatedIm = imwarp(im, tform);
  rotatedIm(rotatedIm==tempVal) = nan;
end

function meanEmissivity = getAbsoptCoeffFromHITRAN_HAPI(T, C, l, script_path)
  % Get absorption coefficients from HITRAN, by running a python script
  % based on HAPI
  if nargin < 3
    error('invalid args!');
  end
  if nargin<4
    scriptPath = 'C:\Users\matanj\Documents\PythonScripts\getAbsorpCoeff.py';
  else
    scriptPath = script_path;
  end
  %build command string:
  argString = [num2str(T),' ',num2str(C),' ',num2str(l)];
  commandStr = ['python ', scriptPath, ' ', argString];
  [status, commandOut] = system(commandStr);
  if status ~= 0 % if succeeded:
    meanEmissivity = str2double(commandOut);
  else
    error('failed!');
  end
end

function [stats, object] = processImGetStats(im, hAx, calcStats)
%Function to process an image and extract the object and optionally its stats
%'im' - image
%'hAx' - handle to an axis to modify.
%'calcStats' - whether to output statistics in 'stats' or not.
    stats = [];
    lvl = graythresh(mat2gray(im)); binaryImage = im2bw(mat2gray(im), lvl); % alternative
    binaryImage = imfill(binaryImage, 'holes');
    labeledImage = bwlabel(binaryImage, 8);
    
    blobMeasurements = regionprops(logical(labeledImage), 'all'); % Get all the blob properties
    numberOfBlobs = size(blobMeasurements, 1);
    assert(numberOfBlobs == 1); % allow one blob
    
    object = im .* binaryImage;
    object(~object) = NaN;
      
    if nargin > 1 %need to calc stats and change given axis plot
        DLInterval = 10; % for histogram bin size
        axes(hAx); hold on;
        boundaries = bwboundaries(binaryImage);
        for k = 1 : numberOfBlobs % preferably just one...
          thisBoundary = boundaries{k};
          plot(thisBoundary(:,2), thisBoundary(:,1), 'r', 'LineWidth', 1.5);
          blobCentroid = blobMeasurements(k).Centroid;	% Get centroid
          plot(blobCentroid(1),blobCentroid(2),'r+', 'MarkerSize', 20);
        end
        hold off;
        % Calc Stats:
        if calcStats
          maxDL = nanmax(object(:));
          minDL = nanmin(object(:));
          nBins =  int16((maxDL-minDL)/DLInterval);
          [hist,edges] = histcounts(object,nBins);
          [~,I] = nanmax(hist(:));
          stats.Mean = nanmean(object(:));
          stats.MCDL = edges(I+1);
          stats.Max = maxDL;
        end
    end
end

function [caseList, I, hPropsListStructFiltered] = getMatchingCase(hTotFileList, hPropsListStruct, caseParamsNames, caseParamVals)
% Query list of files according to properties. 
% Allows reqursive queries by this scheme:
% [caseList1, I1, hPropsListStructFiltered] = getMatchingCase(hTotFileList, hPropsListStruct, caseParamsNames, caseParamVals)
% [caseList2, I2, ~] = getMatchingCase(caseList1, hPropsListStructFiltered, caseParamsNames, caseParamVals)

%INPUT: 'hTotFileList' - Total list of files of the experiment. cell array.
%       'hPropsListStruct' - struct array holding experiment props per
%           file. may be > than actual number mentioned in the filenames' format. 
%       'caseParamsNames' - parameters names to query. cell array.
%       'caseParamVals' - associated values. cell array.
%OUTPUT: 'caseList' - String cell array of filenames that match the input query.
%        'I' - indices array of corresponding files inside input list
%              'hTotFileList'
%        'hPropsListStructFiltered' - to allow recuring 

    caseList = {}; I = [];
    if nargin < 4
      return;
    end
    I = 1:numel(hTotFileList);
    for indCaseParams = 1:numel(caseParamsNames)
      %The following can handle cell array as well as numeric values: 
      subI = find(ismember(hPropsListStruct.(caseParamsNames{indCaseParams}), caseParamVals{indCaseParams} ));    
      I = intersect(subI, I);
    end
    caseList = {hTotFileList(I)};
    caseList = caseList{1};
    hPropsListStructFiltered = structfun(@(x) x(I), hPropsListStruct, 'UniformOutput', false);
end
