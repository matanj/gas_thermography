%% Test GPU imporvement for Spectra calculations
close all; clear; clc;

atm_params.OPL = 1; % Atmospheric path [cm]
atm_params.co2_concentration = 1; % Atmospheric CO2 concentration [fraction]
atm_params.T = 400; % Atmospheric temperature [K]
atm_params.RH = 50; % Atmospheric Water Relative Humidity (RH) [%]
atm_params.pressure_tot = 1; % [atm]

hitranParams.wn_step = 0.01; % HITRAN's wavenumber step [cm^-1]
hitranParams.min_wn = 2000; % HITRAN's minimal wavenumber [cm^-1]
hitranParams.max_wn = 3333; % HITRAN's maximal wavenumber [cm^-1]
hitranParams.HW_to_eval = 500; % Number of HWHM for Voigt evaluation. Orignally 6000 by Ilya. 500 also good enough.
hitranParams.min_uncertainty_code = '000000';%'342222';% minimal uncertainty 6-digit code, see http://hitran.org/docs/uncertainties/
hitranParams.line_strength_th = 1e-31; % Line stregth threshold.
% Number of HWHM is a tradoff between computation time and accuracy. Worst
% case for validation is with high concentration and temperatures, as
% expected.
hitranParams.hitranFileFullPath = fullfile(pwd,'data','hitranCO2_H2O_2000_3333_main_isotopologs.par');

[wn_vect, ~, tau_spectra_wn] = MyGetSpectrumGPU(true, 'tau', hitranParams, atm_params);
