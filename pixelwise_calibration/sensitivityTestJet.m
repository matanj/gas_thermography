function sensitivityTestJet
% clc; close all;
dbstop if error
warning off
format short
dbstatus
%
C2K = @(x) x + 273.15; % Celsius to Kelvin
%% Define the parameters and output function
%% R = e * tr * Kw * exp(A0*b/T)
% [ParamsFileName, ParamsFilePath] = uigetfile({'*.mat','MATLAB data'},'Select Calibration parameters set',[pwd,'\CalibrationParams.mat']);
% if isempty(ParamsFileName)
%   error('invalid folder path...');
% end
ParamsFilePath = pwd;
ParamsFileName = '\CalibrationParams.mat';
load([ParamsFilePath, ParamsFileName]);

T2R = @(temp, em, trans, Kw, a0)    GasHelper.T2R(temp, T, em, trans, Kw, a0);

%% Defining the possible samples domain:
TrueEps = 1;
dE = 0.7*TrueEps;
TrueTau = 0.95;
dTau = 0.8*TrueTau;
step = 1e-2;
Ne = 100;
Nt = 100;
% ee = dE:step:TrueEps;%1e-3:step:5e-2; % possible emissivites
% tr = dTau:step:TrueTau; % 1e-1:step:1;% possible trasmissivities
ee = linspace(dE, TrueEps, Ne);
tr = linspace(dTau, TrueTau, Nt);

mean_Kw = nanmean(Kw(:));
mean_a0 = nanmean(a0(:));
% Kws = getSampleRangeFromValue(mean_Kw, step); % jitter on calibration param
% a0s = getSampleRangeFromValue(mean_a0, step); % jitter on calibration param
Kws = mean_Kw;
a0s = mean_a0;

if 0

% Jet's temperature doesn't participate in this simple analysis as it affects 
% emissivity in addition to the direct influence in T2R
temps_C = [100:100:400]; %[C]
temps = C2K(temps_C); %[K]

% [eeGrid, trGrid, KwGrid, a0Grid] = ndgrid(ee, tr, Kws, a0s);
[eeGrid, trGrid, tempsGrid] = ndgrid(ee, tr, temps);

% R = T2R(temps, eeGrid, trGrid, KwGrid, a0Grid);
R = T2R(tempsGrid, eeGrid, trGrid, Kws, a0s);
T_rec_C = GasHelper.R2T_rec(R, T, eeGrid, trGrid, Kws, a0s);
T_rec_C2 = GasHelper.R2T_rec(R, T, TrueEps*ones(size(R)), TrueTau*ones(size(R)), Kws, a0s);
dEps =  ones(size(ee)) - ee;
dTaus =  ones(size(tr)) - tr;

figure; 
for indT = 1 : numel(temps)
  err = T_rec_C(:,:,indT) - T_rec_C2(:,:,indT);
%   figure; imagesc(tr, ee, err ); colorbar; axis image; impixelinfo;
%   title(['Temperature error for T_r_e_f = ',num2str(temps_C(indT)),' [C]']);
%   xlabel('\Delta\tau'); ylabel('\Delta\epsilon');
  subplot(2,2,indT);
  imagesc(tr, ee, 100*abs(err ./ C2K(T_rec_C(:,:,indT)))); colorbar; impixelinfo;
  title(['T %error for T_r_e_f = ',num2str(temps_C(indT)),' [C]', ... 
      '.  true \epsilon=',num2str(TrueEps),', true \tau=',num2str(TrueTau), ...
          '.  0<\Delta\epsilon<',num2str(1-dE/TrueEps),', 0<\Delta\tau<',num2str(1-dTau/TrueTau)]);
  xlabel('\Delta\tau'); ylabel('\Delta\epsilon');
end
end

%% Testing emissivity influence:
CO2_concentration = 0.1; % Test true concentration
% temps_C = [100,200,300,400]; % Test temperatures
temps_C = [128, 230, 315, 375]; %[C]
TrueTau = 0.9; % assumed true transmissivity of the atmospheric path
error_margin_percent = 10; % margin of error allowed [%]. For more informative plots.
error_margin_C = 100; % margin of error allowed [C]. For more informative plots.

% Emissivity matrix from HITRAN, for a 0.02cm cell (typical pixel size)
% meanAbsorpMatrixFilePath = 'C:\Users\dell\Google Drive\AE_Research\PythonScripts\meanAbsorpMatrix_25.0C_to_600.0C_C1_0.02cm.mat';
% meanAbsorpMatrixFilePath = 'C:\Users\dell\Google Drive\AE_Research\PythonScripts\meanAbsorpMatrix_25.0C_to_500.0C_C1_0.2cm.mat';
% meanAbsorpMatrixFilePath = 'C:\Users\matanj\Documents\PythonScripts\meanAbsorpMatrix_25.0C_to_500.0C_C1_0.2cm.mat';
% meanAbsorpMatrixFilePath = 'C:\Users\matanj\Documents\PythonScripts\meanAbsorpMatrix_25.0C_to_600.0C_C1_0.02cm.mat';
meanAbsorpMatrixFilePath = 'C:\Users\matanj\Documents\PythonScripts\meanAbsorpMatrix_25.0C_to_600.0C_C1_0.0125cm_100concentrations.mat';
% meanAbsorpMatrixFilePath = 'C:\Users\matanj\Documents\PythonScripts\meanAbsorpMatrix_25.0C_to_600.0C_C1_0.02cm_nT200_nC10.mat';

e_mat = load(meanAbsorpMatrixFilePath);
e_mat = e_mat.meanAbsorp;
% limit to the actual T range (100-500C):
t_vec_C = linspace(25,600,size(e_mat,1));
indT100_500C = t_vec_C >= 100 & t_vec_C <=500;
e_mat = e_mat(indT100_500C, :);
t_vec_C = t_vec_C(indT100_500C);
c_vec = linspace(500e-6, 1, size(e_mat,2));
% figure; imagesc(c_vec, t_vec_C, e_mat); colorbar; impixelinfo;
% title('HITRAN calculated \epsilon matrix for a 0.02cm cell size (typical pixel)');
% xlabel('CO_2 concentration'); ylabel('T [C]');

findIndClosestT = @(t) min(abs(t - t_vec_C)); % find index of temperature which is as close to value 't'
findIndClosestC = @(c) min(abs(c - c_vec)); % find index of concentration which is as close to value 'c'
[~, indTrueT] = arrayfun(findIndClosestT, temps_C);
[~, indTrueC] = arrayfun(findIndClosestC, CO2_concentration);
TrueEps = e_mat(indTrueT, indTrueC); % mark the true emissivities
temps = C2K(temps_C);

tau = TrueTau*ones(size(e_mat));
figure(111);
figure(222);
figure(333);
% set colormap limits:
maxC1 = error_margin_C; % [C]
minC1 = 0;%-maxC1; % [C]
minC2 = 0; % [%]
maxC2 = error_margin_percent; % [%]

for indT = 1 : numel(temps)
  R = T2R(temps(indT), e_mat, tau, Kws, a0s);
  T_rec_C = GasHelper.R2T_rec(R, T, e_mat, tau, Kws, a0s); % True temperature
  T_rec_C2 = GasHelper.R2T_rec(R, T, TrueEps(indT)*ones(size(R)), TrueTau*ones(size(R)), Kws, a0s);% estimated temperature
  err = abs(T_rec_C2 - T_rec_C);
  
  figure(111);
  subplot(2,2,indT);
  imagesc(c_vec, t_vec_C, err ); colorbar; impixelinfo; set(gca,'clim',[minC1,maxC1]); %colormap(inferno(300)); 
  title(['Temperature error for T_r_e_f = ',num2str(temps_C(indT)),' [C]']);
  xlabel('CO_2 concentration'); ylabel('T[C] assumed');
  figure(222); 
  subplot(2,2,indT);
  imagesc(c_vec, t_vec_C, 100.*abs(err ./ C2K(T_rec_C))); colorbar; impixelinfo; set(gca,'clim',[minC2,maxC2]); %colormap(inferno(300));
  title(['T %error for T_r_e_f = ',num2str(temps_C(indT)),' [C] & CO_2 concentration = ',num2str(CO2_concentration)]);
  xlabel('CO_2 concentration'); ylabel('T[C] assumed');
  hold on;
  [min_errs,min_errs_idx] = min(abs(err));
  plot(c_vec,t_vec_C(min_errs_idx), 'r+', 'MarkerSize', 10, 'LineWidth', 1);
  hold on;
  [~, absolute_min_col] = min(min_errs);
  plot(c_vec(absolute_min_col), t_vec_C(min_errs_idx(absolute_min_col)), 'g+', 'MarkerSize', 10, 'LineWidth', 1);
  figure(333);
  subplot(2,2,indT);
  imagesc(c_vec, t_vec_C, R ); colorbar; impixelinfo; %colormap(inferno(300)); 
  title(['R for T_r_e_f = ',num2str(temps_C(indT)),' [C]']);
  xlabel('CO_2 concentration'); ylabel('T[C] assumed');
  
end

%% Testing filter tails influence on Emission matrix:


end
%{
function [y] = getSampleRangeFromValue(x, a)
%Gets value 'x' and optionally size of neigberhood 'a' and returns range around this value.
if nargin < 1
  error('invalid args');
end
if nargin < 2 || a==0
  a = 10;
end
y = (x - x*a) : x*a/10 : (x + x*a);
end
%}