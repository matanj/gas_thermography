Nrows = 200;
Ncols = 100;
tau_vect = ones(Ncols,1);
emission_vect = ones(Ncols,1);
for iR = 1 : Nrows
  [I] = GasHelper.forward_onion_peeling(emission_vect, tau_vect);
end
