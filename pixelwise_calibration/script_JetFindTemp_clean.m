%% Use per-pixel Calibration matrix to retrieve the Jet temperature
%%
% *Synopsis*
%%
function script_JetFindTemp_clean
% clc; close all;
dbstop if error
format short
dbstatus
%% Assumptions:
%%  1) each experiment case hase same #files.
%%  2) All images are of same size (row, cols) & pixel allignment
%%  3) Measured Camera's R is independant of solid angle of the object:
%%     R ~ double_integral(Radiance*dOmega*dA), where here the solid angle is of the detector pixel and Camera's FOV, and the detector's area.
%%  4) Nozzle is shown in the images.
C2K = @(x) x + 273.15; % Celsius to Kelvin
K2C = @(x) x - 273.15; % Kelvin to Celsius

%% OPTIONS
SUPPLY_T_C_REFS = true; % {false}, true. If 'true', loads .mat file of a Nx4 matrix 'T_meas' where each row is T.C sample {i, j, T[C], error[C]}, where {i,j} are the {rows,cols} indices in the original FLIR image
NARROW_OPTIMIZATION_BOUNDS = false;
CONSTANT_POTENTIAL_CORE = true; % {true}, false. If false - horizontal decrease inside potential core (from c.l. to edge).
SEPARATE_C_T_MODELS = false; % {false}, true. If true - the C geometric model parameters differ from T model
GEN_T_BASED_ON_ORIG_DIM = false; % {false}, true. If true - simulates T image with same dim, and then crops it.
SUBTRACT_BG_R = true; % {true}, false. Optionally subtract the background R from the R image
FIND_T0_FROM_R_DB = false; % {false}, true. If false - use the iterative guess process.
INITIALIZE_SLOW_OPTIMIZATION_FROM_SOLUTION_OF_FAST_ONE = false; %{false}, true. 'true' - use solution of w/o self-absorption as start point of w/ self-absorption
VERIFY_SINGLE_MEASUREMENT = false; % {false}, true. For e.g. OD testing. Temperature of the measurment as specify in its filename will be used alone.

%% Image processing related:
N_FRAMES = 0; % {0} Number of frames to take from camera *.ptw files. '0' for 'all'
FLIP_BG_IMG_HORIZONTAL = false; %{false}, true
FLIP_BG_IMG_VERTICALLY = false; %{false}, true
FIT_JET_BOUNDARY_TO_LINE = true; % {true}, false
FIT_TO_LINE_METHOD = 'OTSU'; %{'WLS'} (weighted least-squares) ,'RANSAC', 'OTSU'
INV_ABEL_METHOD = 'NO'; %{'NO'}, 'FE'
WEIGHTS_TYPE = 4; % {4} 8 options. 3 - Radiation. 4 - inverse Abel. 5 - uniform weights
FIX_JET_ANGLE = false; % {false}
FIX_JET_ANGLE_DEG_TH = 6; % {6} Skew angle Threshold [deg]. Above it, jet will be aligned verticaly.
FILTER_THE_RADIATION = true; %{true}. Filter estimated R with a gaussian LPF.
%% Calibration related:
CALIB_K_CONST = true; % {true}, false. If 'true' - taken as mean(K)
CALIB_K_OOB_MEAN_OR_NAN = 'mean'; %  {'mean'}, 'nan'. Replace calibration K by nan or mean(K) where indices are out of K bound
CALC_CALIB_PARAMS = false; %{false}, true. If false, use existing set for testing
REPRESENTATIVE_R_CALIB = false; % {false}, true.  If true - representative Rs will be used to obtain calibration params. otherwise, parameters per pixel.
ALL_PIXELS_SEE_BB = true; % {true}, false. for calibrating only a subset of the image
%% Upstream T0 iterative process:
ITERATIVE_CONVERGENCE_TOL = 1e-7; % {1e-7} tolerance for convergance of the iterative process [R]
%% DB generation parameters:
GENERATE_R_DB = false;   % loops over T,C and build the R_DB.mat database
GENERATE_TAU_DB = false; % loops over T,C and build the TAU_DB.mat database
N_T_iters = 200; % Maximal number of iterations for the iterative process.
N_C = 100; % number of concentrations
MAX_GUESSED_TEMPERATURE = 600; % maximal temperature to guess [C]

%% EXPERIMENT RELATED:
% % 10/11/16 Round pipe orifice, D=10mm, 2m away:
% tot_mfr = 2; %[g/s]
% CO2_mfr = 2; %[g/s]
% D_nozzle = 1e-2; % Nozzle's diameter [m]
% atm_OPL = 200; % Atmospheric path in [cm].
% atm_co2_concentration = 800e-6; % Atmospheric CO2 concentration [fraction]
% atm_T = 25.5; % Atmospheric temperature [C]
% p_tot = 1; % total pressure [atm]
% RH = 38; % [%] Relative Humidity in the Air portion of the Jet

% 31/1/17 Round pipe orifice, D=10mm:
tot_mfr = 2; %[g/s]
CO2_mfr = 0.5; %[g/s]
D_nozzle = 1e-2; % Nozzle's diameter [m]
atm_OPL = 95; % Atmospheric path in [cm].
atm_co2_concentration = 560e-6; % Atmospheric CO2 concentration [fraction]
atm_T = 18; % Atmospheric temperature [C]
p_tot = 1; % total pressure [atm]
RH = 42; % [%] Relative Humidity in the Air portion of the Jet

% 18/9/17 Contracted orifice, D=40mm:
tot_mfr = 5; %[g/s]
CO2_mfr = 0.5; %[g/s]
D_nozzle = 4e-2; % Nozzle's diameter [m]
atm_OPL = 155; % Atmospheric path in [cm].
atm_co2_concentration = 620e-6; % Atmospheric CO2 concentration [fraction]
atm_T = 26.5; % Atmospheric temperature [C]
p_tot = 1; % total pressure [atm]
RH = 48; % [%] Relative Humidity in the Air portion of the Jet

% % 23/11/17 Round pipe orifice, D=20mm:
% tot_mfr = 4; %[g/s]
% CO2_mfr = 0.2; %[g/s]
% D_nozzle = 2e-2; % Nozzle's diameter [m]
% atm_OPL = 125; % Atmospheric path in [cm].
% atm_co2_concentration = 800e-6; % Atmospheric CO2 concentration [fraction]
% atm_T = 22; % Atmospheric temperature [C]
% p_tot = 1; % total pressure [atm]
% RH = 40; % [%] Relative Humidity in the Air portion of the Jet

% % 6/12/17 Contracted Jet, D=20mm:
% tot_mfr = 5; %[g/s]
% CO2_mfr = 0.5; %[g/s]
% D_nozzle = 2e-2; %92/114*2e-2; % Nozzle's diameter [m]
% atm_OPL = 125; % Atmospheric path in [cm].
% atm_co2_concentration = 600*1e-6; % Atmospheric CO2 concentration [fraction]
% atm_T = 25.2; % Atmospheric temperature [C]
% p_tot = 1; % total pressure [atm]
% RH = 30; % [%] Relative Humidity in the Air portion of the Jet

%% DB files:
R_DB_file_name = 'R_DB_tot.mat';
TAU_DB_file_name = 'TAU_DB_tot.mat';

%% Calibration related:
OD_guess = 2.0667; %2.099; % Best estimation from "test_integral_calibration_new_17_1_5.m". Declared OD = 2.
detector_pitch_um = 15; % [um] As declared by SCD (Pelican-D)
% e_BB = 0.95; % Plate BB
% calib_atm_params.OPL = 155; % Calibration Atmospheric path in[cm]
% calib_atm_params.co2_concentration = 560*1e-6;% Calibration Atmospheric CO2 concentration [fraction]
% calib_atm_params.T = C2K(24); % Calibration Atmospheric temperature [K]
% calib_atm_params.RH = 50; %[%]
e_BB = 0.99; % emissivity of the Black Body used for calibration. assumed constant spatial-wise and spectrum-wise.
calib_atm_params.OPL = 220; % Calibration Atmospheric path in[cm]
calib_atm_params.co2_concentration = 650*1e-6;% Calibration Atmospheric CO2 concentration [fraction]
calib_atm_params.T = C2K(24.5); % Calibration Atmospheric temperature [K]
calib_atm_params.RH = 38; %[%]
calib_atm_params.pressure_tot = 1; %[atm]
%% HITRAN spectral calculations:
hitranParams.wn_step = 0.01; % HITRAN's wavenumber step [cm^-1]
hitranParams.min_wn = 2000; % HITRAN's minimal wavenumber [cm^-1]
hitranParams.max_wn = 3333; % HITRAN's maximal wavenumber [cm^-1]
hitranParams.HW_to_eval = 500; % Number of HWHM for Voigt evaluation. Orignally 6000 by Ilya. 500 also good enough.
hitranParams.min_uncertainty_code = '000000';%'342222';% minimal uncertainty 6-digit code, see http://hitran.org/docs/uncertainties/
hitranParams.line_strength_th = 1e-31; % Line stregth threshold.
% Number of HWHM is a tradoff between computation time and accuracy. Worst
% case for validation is with high concentration and temperatures, as
% expected.
hitranParams.hitranFileFullPath = fullfile(pwd,'data','hitranCO2_H2O_2000_3333_main_isotopologs.par');
%% Default paths:
CalibFilePath = fullfile(pwd,'JetBBCalibrationSaveR_OUTPUT.mat'); % default
CalibOutputName = 'CalibrationParams_Integral.mat';
CalibFilePath = fullfile(pwd,'CavityBBCalibrationSaveR_OUTPUT_registered_img.mat'); % default

apparatus_tf_path = 'apparatus_tfs.mat'; % default apparatus transfer functions
%% Default test case and related files:
baseDir = 'D:\Laptop BKP\FLIR_Experimental\';
imName = [baseDir, 'G27_T270_F4370_IT3199.8_IM_.ptw'];
%% Default directory to store figures:
Figures_parent_dir = 'C:\Users\matanj\Google Drive\AE_Research\Thesis\Figures';

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Atmospheric path struct for the experiment case:
atm_params.OPL = atm_OPL; % Atmospheric path [cm]
atm_params.co2_concentration = atm_co2_concentration; % Atmospheric CO2 concentration [fraction]
atm_params.T = C2K(atm_T); % Atmospheric temperature [K]
atm_params.RH = RH; % Atmospheric Water Relative Humidity (RH) [%]
atm_params.pressure_tot = p_tot; % [atm]

%% Load a matrix of T.C. reference temperatures
if SUPPLY_T_C_REFS
  [T_C_ref_file, T_C_ref_dir] = uigetfile({'*.mat','T_C_reference_file'},'Select reference T_C measurements',baseDir);
  if isempty(T_C_ref_file)
    error('invalid file');
  end
  T_meas = load(fullfile(T_C_ref_dir, T_C_ref_file));
  T_meas = T_meas.T_meas;
else
  T_meas = [];
end

%% Load apparatus' transfer functions - Detector, Lens & Filter %%%
[wl_vect, optics_cfits, ~] = GasHelper.get_optics_tf(apparatus_tf_path, hitranParams, false);

optics_tf_wl = GasHelper.modify_optics_tf_by_OD(wl_vect, optics_cfits, OD_guess);
%% conditionally calculate calibration parameters:
if CALC_CALIB_PARAMS
  calib_experiment_params.atmParams = calib_atm_params;
  calib_experiment_params.e_BB = e_BB;
  calib_experiment_params.detector_pitch_um = detector_pitch_um;
  calib_experiment_params.filter_OD = OD_guess;
  tic
  [calibParams] = GasHelper.getSpectralCalibrationFromR(CalibFilePath, wl_vect, hitranParams, calib_experiment_params, optics_tf_wl, REPRESENTATIVE_R_CALIB, true);
  toc
  save(CalibOutputName, 'calibParams');% Save coefficients per pixel
  return;
else % Use exisiting calibration params:
  if isempty(CalibOutputName)
    error('invalid folder path...');
  end
  load(fullfile(pwd, CalibOutputName));
end
%% Create a file for logging calculated R(T,C,distance), if not exist:
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% HITRAN's DB - Stores R(T,C,OPL) for a given atmospheric scenario (tau) %%%%%
% DB structure's fields:
%   1) T,C,OPL arrays.
%   2) R(T,C,OPL) stores: integral(radiance_photon_flux .* tau .* optics_tf_wn)
%   3) Atmospheric parameters struct.
%   4) HITRAN parameters struct.
%   5) Description.
% Use like: R_simulated = gemoetry_factor * K * R(T,C,OPL).
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if exist(fullfile(pwd,R_DB_file_name), 'file') == 2 % there is already such file
  tic
  S = load(fullfile(pwd,R_DB_file_name));
  toc
  R_DB_struct = S.R_DB_struct;
  % check that HITRAN & atmospheric params of experiment matches the DB's ones:
  % assert(R_DB_helper.is_DB_legit_to_case(R_DB_struct, atm_params, hitranParams, true), 'DB''s params don''t match this case!');
else  % create new file
  R_DB_struct = struct('T',[],'C',[],'OPL',[],'R',[]);
  R_DB_struct.T = linspace(C2K(10), C2K(450), N_T_iters )';
  R_DB_struct.C = linspace(400e-6, 1, N_C)';
  R_DB_struct.OPL = [1/1000*(10:1000), 1/100*(101:150) ]'; 
  R_DB_struct.R = zeros(N_T_iters, N_C, numel(R_DB_struct.OPL));
  R_DB_struct.atm_params = atm_params;
  R_DB_struct.hitranParams = hitranParams;
  % Save in version compatible with efficient partial loading/saving.
  % Saving like this should be done only here. Subsequent saves can omit
  % the '-v7.3' flag.
  save(fullfile(pwd,R_DB_file_name),'R_DB_struct','-v7.3');
end
%% Create a file for logging calculated TAU(T,C,distance), if not exist:
if exist(fullfile(pwd,TAU_DB_file_name), 'file') == 2 % there is already such file
  tic
  S = load(fullfile(pwd,TAU_DB_file_name));
  toc
  TAU_DB_struct = S.TAU_DB_struct;
else
  TAU_DB_struct = struct('T',[],'C',[],'OPL',[],'TAU',[]);
  TAU_DB_struct.T = linspace(C2K(10), C2K(450), 100 )';
  TAU_DB_struct.C = linspace(400e-6, 1, 100)';
  TAU_DB_struct.OPL = [1./(1:100)]';
  TAU_DB_struct.TAU = zeros(numel(TAU_DB_struct.T), numel(TAU_DB_struct.C), numel(TAU_DB_struct.OPL));
  TAU_DB_struct.hitranParams = hitranParams;
  % Save in version compatible with efficient partial loading/saving.
  % Saving like this should be done only here. Subsequent saves can omit
  % the '-v7.3' flag.
  save(fullfile(pwd,TAU_DB_file_name),'TAU_DB_struct','-v7.3');
end


if ~(GENERATE_R_DB || GENERATE_TAU_DB)
  %% Create griddedInterpolant objects for faster access to DBs:
  tic
  F_R   = griddedInterpolant({R_DB_struct.T, R_DB_struct.C, R_DB_struct.OPL}, R_DB_struct.R);
  F_TAU = griddedInterpolant({TAU_DB_struct.T, TAU_DB_struct.C, TAU_DB_struct.OPL}, TAU_DB_struct.TAU);
  toc
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% %%%%%%%%%%%%%%%%% Test the calibration: %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Get test case:
[imName, baseDir] = uigetfile({'*.ptw','FLIR images'},'Select image to test calibration',imName);
if isempty(imName)
  error('invalid image');
end
%Get the Background file that matches the Image file's name:
bgName = GasHelper.getBGfileFromIMfile(imName, baseDir);
%Get the temperature of the measurement from file's name:
matchStr = regexp(imName,'_T+[0-9]+[0-9]+[0-9]','match');
TrefStr = regexprep(matchStr{1},'_T','');
%Set the image pair object:
imPair = ImageBGPairExt([baseDir,imName]);
imPair.setBG([baseDir,bgName{1}]);

subIm = abs(imPair.getSubtracted(FLIP_BG_IMG_HORIZONTAL,FLIP_BG_IMG_VERTICALLY));
if N_FRAMES > size(subIm, 3) || ~N_FRAMES % average all available frames
  N_FRAMES = size(subIm, 3);
end
subIm = mean(subIm(:,:,1:N_FRAMES), 3); % time averaging the frame sequence
if ALL_PIXELS_SEE_BB
  objMask = true(size(subIm));
else
  [~, object] = GasHelper.processImGetStats(subIm);%get meaningful object within the image
  objMask = true(imPair.IMG_Info.m_rows, imPair.IMG_Info.m_cols);%initial mask
  objMask(isnan(object)) = NaN;
end
%Mask the image to maintain same size:
DL_IM = objMask.*subIm;
DL_IM(DL_IM==0) = 1; % set minimal DL to 1 to avoid zero issues

%% Align image to calibration images (map1):
imgCalibInfo.m_cliprect = [160 100 320 360]; % TODO: remove and supply true file!
mask_calib_im_in_full_frame = GasHelper.FLIR_mask_location_of_image_in_full_frame(imgCalibInfo);

%% trim image to orifice and get pipe's diameter in [pixels]:
[DL_new, D_nozzle_pixels, m2pixel, pos_nozzle, ~, D_nozzle] = GasHelper.trimIm2Nozzle(DL_IM, D_nozzle, true, true);

%% Align image to calibration images (map1):
Nozzle_Cropped_IMG_Info.m_cliprect = imPair.IMG_Info.m_cliprect;
Nozzle_Cropped_IMG_Info.m_cliprect(4) = floor(pos_nozzle(1,2));
mask_target_im_in_full_frame = GasHelper.FLIR_mask_location_of_image_in_full_frame(Nozzle_Cropped_IMG_Info);

%% Modify the calibration image to fit required image:
K_bkp = calibParams.K;
mask_calib_im_in_full_frame(mask_target_im_in_full_frame & mask_calib_im_in_full_frame) = 2;
mask_target_im_in_full_frame(mask_target_im_in_full_frame & mask_calib_im_in_full_frame) = 2;

K_full_frame = mask_calib_im_in_full_frame;
if isequal(CALIB_K_OOB_MEAN_OR_NAN, 'mean') || CALIB_K_CONST
  K_full_frame(mask_calib_im_in_full_frame < 2) =  nanmean(calibParams.K(:));
else
  K_full_frame(mask_calib_im_in_full_frame < 2) = nan;
end
if CALIB_K_CONST
  K_full_frame(mask_calib_im_in_full_frame > 0) = nanmean(calibParams.K(:));
else
  K_full_frame(mask_calib_im_in_full_frame > 0) = calibParams.K;
end
calibParams.K = K_full_frame( mask_target_im_in_full_frame > 0 );
calibParams.K = reshape( calibParams.K, size(DL_new) );


%% Rotate Jet if needed:
if FIX_JET_ANGLE
  %% TODO: should apply same to the calibration maps, or rotate back b4 using the calibration
  DL_new = GasHelper.fixJetAngle(DL_new);
  %Align image to calibration images:
  remove_rotation_added_rows();
  map2();
end

%% Apply calibration with K
K = calibParams.K;
% First, crop object image to the calibration image, if needed:
if ~(exist('rect','var') ) % if cropping wasn't done:
  [rowsCalib, colsCalib] = size(calibParams.K);
  [rowsNew, colsNew] = size(DL_new);
  if rowsNew ~= rowsCalib || colsNew ~= colsCalib
    rows = 1:floor(pos_nozzle(1,2));
    cols = 1:size(DL_new,2);
  else
    rows = 1:size(DL_new,1);
    cols = 1:size(DL_new,2);
  end
else % if 'rect' is present:
  rows = rect(2):rect(2)+rect(4);
  cols = rect(1):rect(1)+rect(3);
end

%% Get the a mask of the actual jet shape, and the centerline of symmetry:
jetBW = GasHelper.getActualJetFromImage(DL_new);
[symAxis, skew_angle_deg] = GasHelper.getAxisOfSymFromJetIm(DL_new.*jetBW);

% DEBUG: check that axis of symetry is poperly retreived:
GasHelper.plotIm(DL_new,'Estimated symmetry axis'); hold on; scatter(symAxis*ones(1,size(DL_new,1)), 1:size(DL_new,1), 'r.');
disp(['Is axis of symmetry located @ column ',num2str(symAxis),'?']);
tmp = input('If Yes - please hit Enter. Else - please enter the column: ');
if ~isempty(tmp)
  symAxis = tmp;
  GasHelper.plotIm(DL_new,'Custom symmetry axis'); hold on; scatter(symAxis*ones(1,size(DL_new,1)), 1:size(DL_new,1), 'r.');
end

% GasHelper.get_D_height_above_nozzle(DL_new, symAxis, []);

%% estimate the measurement's radiation from DL = R*IT^P
R_est = GasHelper.GetMeasurementRByMultiIT2RP(baseDir, TrefStr, DL_new, rows, cols, [], ALL_PIXELS_SEE_BB, FLIP_BG_IMG_HORIZONTAL, FLIP_BG_IMG_VERTICALLY, N_FRAMES);
assert( all(R_est(~isnan(R_est)) >= 0) || all(R_est(~isnan(R_est)) <=0) ); % mixed signs are not allowed!

symAxis_for_plots = symAxis;

%assign 'NaN' to pixels that are more than 7 stds away the mean, to filter
%spikey noise:
R_est = GasHelper.filterStds(R_est, 7);

R_est0 = abs(R_est); % fix for possible confusion of IM and BG
R_orig = abs(R_est);

% Estimate the noise level of the R image:
% [nlevel th] = NoiseLevel(R_orig); nlevel / nanmean(R_orig(:));
bg_part_of_image = R_est.*~jetBW;
bg_part_of_image(jetBW>0) = nan;
fg_part_of_image = R_est.*jetBW;
fg_part_of_image(jetBW<=0) = nan;

mu_bg_part = nanmean(bg_part_of_image(:));
mu_fg_part = nanmean(fg_part_of_image(:));
RMS_noise = sqrt( nanmean( (fg_part_of_image(:) - mu_bg_part).^2 ) );

noise_parameter = mu_fg_part / nanstd(bg_part_of_image(:)) ;
% SNR = nanstd(fg_part_of_image(:)) / nanstd(bg_part_of_image(:));
SNR = (mu_fg_part-mu_bg_part) / nanstd(bg_part_of_image(:)) ;
assert(SNR > 0, 'Invalid SNR value!');
SNR = 10*log10(SNR); % convert to [dB]
SNR2 = 20*log10(mu_fg_part / RMS_noise); %in [dB]
gaussian_LPF_param = 0.5; % DEFAULT value
% if noise_parameter > 0.5
if SNR < 20 % poorer than 20dB
  gaussian_LPF_param = 1.5;
end

if FILTER_THE_RADIATION
  R_est_filt = imgaussfilt(R_orig, gaussian_LPF_param); % Gaussian LPF. should preserve edges.
  R_est0 = R_est_filt;
end
assert(all(R_est0(:)),'invalid R...');

%% Align the jet vertically, if it worth it:
%% TODO: What about rotating and translating the K image accordingly? 
RWorld = imref2d(size(R_est0)); % Set R_est coordinate frame as the World coordinate frame. % TODO: ?
if abs(skew_angle_deg) > FIX_JET_ANGLE_DEG_TH
  if sum(sum(isnan(R_est0))) % If there's NaNs in image, set a warning:
    warning('R images has NaNs. Will be converted to 0 before rotation... check the output image!');
    R_est0(isnan(R_est0)) = 0; % cannot transform image with NaN.
  end
%   [R_est, ~ ] = GasHelper.fixJetAngle(R_est0, RWorld, skew_angle_deg); %
%   BUG - I don't want to rotate around the center, but around the jet
%   nozzle's center!
  R_est = rotateAround(R_est0, size(R_est0,1), symAxis, skew_angle_deg);
  R_est(isnan(R_est)) = 0; % TODO!!!
else
  R_est = R_est0;
end

%% Fuse the two half-jets into a single one (average):
R_left_half_jet = fliplr(R_est(:,1:symAxis));
R_right_half_jet = R_est(:,symAxis:end);
width_half_jet = min(size(R_left_half_jet,2), size(R_right_half_jet,2)); % '1' due to the centerline
R_left_half_jet  = R_left_half_jet(:, 1:width_half_jet);
R_right_half_jet = R_right_half_jet(:, 1:width_half_jet);
R_fuzed = (R_left_half_jet + R_right_half_jet)/2; % just averaging the two halfs
R_est_bkp = R_est; % Backup
R_est = zeros(size(R_est_bkp));
R_est(:,symAxis:symAxis+width_half_jet-1) = R_fuzed;
%{
% DEBUG:
figure; plot(R_left_half_jet(end,:),'b'); hold on; 
plot(R_right_half_jet(end,:),'r'); plot(R_fuzed(end,:),'m');
legend('left half','right half','average'); title('R @ nozzle');
%}

%% apply HITRAN transmittance model for the atmosheric path of the measurement
[wn_vect, ~, tau_spectra_wn] = MyGetSpectrumGPU(true, 'tau', hitranParams, atm_params);

[~, optics_tf_wn] = GasHelper.spectralFunc_wl2wn(calibParams.wl_vect, calibParams.optics_tf, true);
tau_avg = GasHelper.getWeightedTransmittance(hitranParams, optics_tf_wn, atm_params);
R_est = R_est ./ tau_avg;

%% Estimate the Effective Diameter & its height above orifice:
get_D_height_above_orifice_options.USE_THEORY = false;
get_D_height_above_orifice_options.D0 = D_nozzle_pixels;
[D_eff_height] = GasHelper.get_D_height_above_orifice(R_est(:,symAxis:end), get_D_height_above_orifice_options );
tmp = input(['D_eff height found in row ',num2str(D_eff_height),'. If OK - hit Enter. Else - enter the row:']);
if ~isempty(tmp)
  D_eff_height = tmp;
end
% Update images:
R_est = R_est(1:D_eff_height, :);
jetBW = jetBW(1:D_eff_height, :);

%% GENEREATE A FULL DB FOR RADIATION WITH OPTIC PATH = PIXEL SIZE:
if GENERATE_R_DB
  jetParams.OPL = 1/m2pixel*100;
  for indT = 1 : numel(R_DB_struct.T)
    for indC = 1 : numel(R_DB_struct.C)
      jetParams.T = R_DB_struct.T(indT);
      jetParams.co2_concentration = R_DB_struct.C(indC);
      params.hitranParams = hitranParams; params.wn_vect = wn_vect; params.jetParams = jetParams; params.calibParams = calibParams;
      [~, R_DB_struct] = R_DB_helper.get_R_from_DB(R_DB_struct, jetParams.T, jetParams.co2_concentration, jetParams.OPL, params );    
    end
    disp(['Finished inner loop #',num2str(indT)]);
  end
  save((fullfile(pwd,R_DB_file_name)),'R_DB_struct');
  return;
end
%% GENEREATE A FULL DB FOR TRANSMISSION WITH OPTIC PATH = PIXEL SIZE:
if GENERATE_TAU_DB
  jetParams.OPL = 1/m2pixel*100;
  jetParams.RH = 40; %TODO: remove hard-coded
  for indT = 1 : numel(TAU_DB_struct.T)
    for indC = 1 : numel(TAU_DB_struct.C)
      jetParams.T = TAU_DB_struct.T(indT);
      jetParams.co2_concentration = TAU_DB_struct.C(indC);
      params.hitranParams = hitranParams; params.optics_tf_wn = optics_tf_wn; params.path_params = jetParams;
      [~, TAU_DB_struct] = TAU_DB_helper.get_TAU_from_DB(TAU_DB_struct, jetParams.T, jetParams.co2_concentration, jetParams.OPL, params );
    end
    disp(['Finished inner loop #',num2str(indT)]);
  end
  save((fullfile(pwd,TAU_DB_file_name)),'TAU_DB_struct');
  return;
end


%% Jet params:
jetParams.co2_concentration = CO2_mfr/tot_mfr;
jetParams.pressure_tot = p_tot;

%% Calculate solid_angle of a single Pixel and detector area for later use:
OPL_step = D_nozzle/D_nozzle_pixels*1e2; %in [cm] for HITRAN
% Approximate the solid angle of a cell in the jet:
solid_angle = (OPL_step/(atm_OPL))^2;
detector_area_cm2 = (detector_pitch_um*1e-4)^2; % Detector's area in [cm^2], to match HITRAN units
geometry = solid_angle*detector_area_cm2;

%% TAKE only the lines next to the exshaust:
% next to the nozzle's exhaust:
ROI_rows = size(R_est,1);

%% Conditionally subtract noise floor R from R image: 
if SUBTRACT_BG_R
  hIm = R_est(:, symAxis:end);
  bg = otsu(hIm, 20);
  bg = hIm(bg == 1);
  R_est(:, symAxis:end) = R_est(:, symAxis:end) - nanmean(bg(:));
%   R_est(ROI_rows, symAxis:end) = R_est(ROI_rows, symAxis:end) - nanmean(bg(:));
  R_est(R_est < 0 & ~isnan(R_est)) = 0;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Get a better estimate on the Jet's boundaries:
if FIT_JET_BOUNDARY_TO_LINE
  [jetBW, h_jet_bounds]= GasHelper.get_mask_of_jet(R_est,symAxis, false);
  %DEBUG:
  ax = findall(h_jet_bounds, 'type', 'axes');
  jet_bounds_im = get(ax,'Children'); % expect: "Line", "Scatter", "Image"
  % Get the jet bound points and shift to match full image:
  jet_bounds_points.X = symAxis + jet_bounds_im(2).XData;
  jet_bounds_points.Y = jet_bounds_im(2).YData;
  % Get the jet bound line and shift to match full image:
  jet_bounds_line.X = symAxis + jet_bounds_im(1).XData;
  jet_bounds_line.Y = jet_bounds_im(1).YData;
  %Plot all together:
  figure; imagesc(R_est0); colorbar; axis image; hold on;
  plot(symAxis*ones(1,size(R_est,1)), 1:size(R_est,1),'r', 'Linewidth', 2);
  scatter(jet_bounds_points.X, jet_bounds_points.Y, 20, '.m');
  plot(jet_bounds_line.X, jet_bounds_line.Y, 'r', 'Linewidth', 2);
  title('Determining jet boundary & axis of symmetry from R image');
  % Convert axes:
  GasHelper.normalize_axes_full_im(R_est0,D_nozzle_pixels, symAxis, 2, 2);
else
  jet_bounds_points = symAxis + sum(jetBW(:,symAxis:end),2) - 1;
  GasHelper.plotIm(R_est); hold on; scatter(jet_bounds_points, 1:numel(jet_bounds_points), 20, '.m');
  GasHelper.normalize_axes_full_im(R_est0,D_nozzle_pixels, symAxis, 2, 2);
end

%% Plot estimated row of effective diameter:
GasHelper.plotIm(DL_IM);
hold on; scatter(1:size(DL_IM,2), D_eff_height*ones(1,size(DL_IM,2)), '.r');
title('Estimated Effective Diameter over raw DL image');
GasHelper.normalize_axes_full_im(DL_IM,D_nozzle_pixels, symAxis, 2, 2);

%% Apply Inverse Abel, to get the potential core edges of the measurement:
im = R_est.*jetBW;
[R_est_Abel_Inverted] = GasHelper.InverseAbelHalfImage(im(:,symAxis:end), 1, INV_ABEL_METHOD, true);

% D_effs = zeros(size(R_est,1), 1);
% for ii = size(R_est,1) : -1 : size(R_est,1)-200
%   [R_est_Abel_Inverted_row] = GasHelper.InverseAbelHalfImage(im(ii,symAxis:end), 1, INV_ABEL_METHOD, true);
%   [D_effs(ii)] = GasHelper.get_D_eff(R_est_Abel_Inverted_row, get_D_height_above_orifice_options );
% end
% figure; plot(D_effs);

%% Use Inverse Abel to assess the Effective Diameter D_eff:
D_orifice_pixels = D_nozzle_pixels; % backup
get_D_height_above_orifice_options.HALF_IM = true;
[D_eff] = GasHelper.get_D_eff(R_est_Abel_Inverted(D_eff_height,:), get_D_height_above_orifice_options );
D_nozzle_pixels = D_eff;
tmp = input(['Found D_eff = ',num2str(D_eff),' [pixels]. If OK - hit Enter. Else - enter another:']);
if ~isempty(tmp)
  D_nozzle_pixels = tmp;
end

%% Print some dimensionless stats:
cacl_jet_dimensionless_numbers_params.D = D_eff/m2pixel; % diameter [m]
cacl_jet_dimensionless_numbers_params.tot_mfr = tot_mfr; % Total mass flow rate, [g/s]
cacl_jet_dimensionless_numbers_params.co2_mfr = CO2_mfr; % CO2 mass flow rate, [g/s]
cacl_jet_dimensionless_numbers_params.Tamb_C = atm_T; % ambient temperature, [C]
cacl_jet_dimensionless_numbers_params.Tjet_C = str2double(TrefStr); %Jet's outlet temperature, [C]
cacl_jet_dimensionless_numbers_params.P0 = p_tot*101325; %[Pa]. Pressure @ outlet
[Re, Pe, Gr] = GasHelper.cacl_jet_dimensionless_numbers(cacl_jet_dimensionless_numbers_params);
disp('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%');
disp(['Re = ',num2str(Re), ', Pe = ', num2str(Pe), ', Gr/Re^2 = ',num2str(Gr/Re^2)]);
disp('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%');

%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Get the measured R at the centerline:
% For the distribution on the axis, no need for ABEL inverese:
jetParams.OPL = 100*D_nozzle_pixels/m2pixel; % [cm]
R_measurement = R_est(ROI_rows,symAxis); % Assuming uniform distribution in the potential core
disp(['R from measurement = ',num2str(R_measurement)]);
% Pre-alloc for the iterative process:
T_guesses = nan(N_T_iters,1);
err_norm_vec = nan(N_T_iters,1);
%% Define searchable temperatures:
if VERIFY_SINGLE_MEASUREMENT
  calcTemps_arr = C2K(str2double(TrefStr));
else
  calcTemps_arr = linspace(C2K(atm_T), C2K(MAX_GUESSED_TEMPERATURE), N_T_iters);
end
LU_temp_ind = ceil(numel(calcTemps_arr)/2); % initial guess
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% ITERATIVE PROCESS: Sweep Tjet in nozzle's mid pixel, compare simulated irradiance to measurement.
% TODO: repeat with new D_eff after it is found!!
R_guess_arr = {};
for k = 1 : min(N_T_iters, numel(calcTemps_arr))
  jetParams.T = calcTemps_arr(LU_temp_ind);
  params.Tjet_C = K2C(calcTemps_arr(LU_temp_ind)); %New guess for Jet outlet temperature[C]
  T_guesses(k) = params.Tjet_C;
  %% Find emission radiance using HITRAN, in each block, and scale to photon flux (the camera counts photons)
  % Check DB b4 time-consuming evaluation:
  params.hitranParams = hitranParams; params.jetParams = jetParams;params.wn_vect = wn_vect; params.calibParams = calibParams; params.tau_spectra_wn = tau_spectra_wn;
  if exist('F_R', 'var')
    R_normalized = R_DB_helper.get_R_from_full_DB(F_R, jetParams.T, jetParams.co2_concentration, jetParams.OPL);
  else
    [R_normalized, R_DB_struct] = R_DB_helper.get_R_from_DB(R_DB_struct, jetParams.T, jetParams.co2_concentration, jetParams.OPL, params );
  end
  R_guess = geometry .* calibParams.K .* R_normalized;
  if REPRESENTATIVE_R_CALIB
    R_guess = R_guess * ones(size(DL_new));
  end
  %% Update current value:
  R_guess = R_guess(ROI_rows,symAxis);
  disp(['R_guess(Tjet=',num2str(T_guesses(k)),'C) = ',num2str(R_guess)]);
  R_guess_arr{k} = R_guess;
  %% Check convergence and update guess:
  R_err = R_guess - R_measurement;
  err_norm_vec(k) = norm(R_err);
  if (norm(R_err) < ITERATIVE_CONVERGENCE_TOL) || (numel(calcTemps_arr) == 1)
    break;
  end
  % Apply binary search to find temperature:
  if LU_temp_ind==1 % we've reach 2 elemets array
    calcTemps_arr = calcTemps_arr(1);
  elseif norm(R_guess) > norm(R_measurement) % search in the lower temperatures
    calcTemps_arr = calcTemps_arr(1:LU_temp_ind-1); % update search range
  else
    calcTemps_arr = calcTemps_arr(LU_temp_ind+1:end); % update search range
  end
  LU_temp_ind = round(numel(calcTemps_arr)/2); % update new guess index
end % end of iterative process
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Here we assume the outlet temperature have been found correctly: %%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[~,ind_T_recovered] = min(err_norm_vec); % the assumed index of the recovered temperature
T_center_nozzle = T_guesses(ind_T_recovered);
% DEBUG:
% Plotting result:
figure;
final_error = 100*abs(T_center_nozzle-str2double(TrefStr))/C2K(str2double(TrefStr)); % percentage error
plot(C2K(T_guesses), err_norm_vec,'bo'); title('||err|| Vs Guessed temperature [K]'); hold on;
plot(C2K(T_center_nozzle), err_norm_vec(ind_T_recovered), 'r*', 'MarkerSize', 10);
suptitle(['Upstream T @ CO_2 fraction = ',num2str(CO2_mfr/tot_mfr), ', Distance = ',num2str(atm_OPL),'cm']);
title(['Measured = ',num2str(C2K(str2double(TrefStr))),'K,  Estimated = ',num2str(C2K(T_center_nozzle)),'K.  Error[%] = ',num2str(final_error)]);
xlabel('T guess [K]'); ylabel('||Radiation error||');

% DEBUG
%{
T_guesses = C2K(T_guesses(~isnan(T_guesses)));
err_norm_vec = err_norm_vec(~isnan(err_norm_vec));

dx = diff(T_guesses);
dy = diff(err_norm_vec);
plot(T_guesses,err_norm_vec,'bo'); hold on;
plot(C2K(T_center_nozzle), err_norm_vec(ind_T_recovered), 'r*');
quiver(T_guesses(1:end-1),err_norm_vec(1:end-1),dx,dy,0,'MaxHeadSize',0.02); hold off;
suptitle(['Upstream T @ CO_2 fraction = ',num2str(CO2_mfr/tot_mfr), ', Distance = ',num2str(atm_OPL),'cm']);
title(['Measured = ',num2str(C2K(str2double(TrefStr))),'K,  Estimated = ',num2str(C2K(T_center_nozzle)),'K.  Error[%] = ',num2str(final_error)]);
xlabel('T guess [K]'); ylabel('||Radiation error||');

figure;
for ip = 1 : numel(T_guesses)-1
  arrow([T_guesses(ip),err_norm_vec(ip)], [T_guesses(ip+1),err_norm_vec(ip+1)],'Length',10,'tipangle',10);
  hold on;
end
%}


%% Set the OPL of a single pixel in [cm]:
jetParams.OPL = 1/m2pixel*100; % pixel equivalent OPL in [cm]

%% release assumption on jet's boundaries mask
jet_validation_mask = jetBW(:, symAxis:end); % save for validation
% jetBW = ones(size(jetBW));

%% Estimate the potential core line from the inverse Abel image:
[X, Y, ~] = GasHelper.get_potential_core_from_half_image(R_est_Abel_Inverted,[],[],[],FIT_TO_LINE_METHOD);
X = X(:); Y = Y(:);
potential_core_slope = (Y(1)-Y(end))/(X(1)-X(end));
potential_core_line = [X,Y];

%% Estimate potenial core top point from its edge line:
assert(potential_core_slope>0,'Potential core line estimated wrong!');
measured_potential_core_length = X(end) + Y(1)/potential_core_slope; %[pixels]

%% Another try, using only the inverse Abel transform of R image and iterate the R_DB.

% First, restrict search ranges:
conditions_ranges_struct.Tmax = C2K(1.1*T_center_nozzle);
conditions_ranges_struct.Tmin = C2K(atm_T);
conditions_ranges_struct.Cmax = 1.1*CO2_mfr/tot_mfr;
conditions_ranges_struct.Cmin = 0.9*CO2_mfr/tot_mfr;%atm_co2_concentration;
conditions_ranges_struct.OPL = 1/m2pixel*100;

if 0 % TODO: the following is very sensative to R_scale error!!!!
R_est_Abel_Inverted_scaled = R_est_Abel_Inverted ./ geometry ./ calibParams.K(1:ROI_rows,symAxis:end);
R_scaled_temp = R_est_Abel_Inverted_scaled(ROI_rows, 1);
R_DB_tol = 0.005*R_est_Abel_Inverted(ROI_rows, 1) ./ geometry ./ calibParams.K(ROI_rows,symAxis); % tolerance for the centerline
end
R_scaled_temp = R_est(ROI_rows,symAxis) ./ D_nozzle_pixels ./ geometry ./ calibParams.K(ROI_rows,symAxis);
R_DB_tol = 0.015*R_scaled_temp; % tolerance for the centerline

[candidate_conditions] = R_DB_helper.get_parameters_from_R_DB(R_DB_struct, R_scaled_temp, conditions_ranges_struct, R_DB_tol);
assert(~isempty(candidate_conditions),'No entries with OPL in R_DB!')

if FIND_T0_FROM_R_DB && ~isempty(candidate_conditions)
  T_center_nozzle = mean(K2C(candidate_conditions.T));
  figure; imagesc(R_DB_struct.C, R_DB_struct.T, R_DB_struct.R(:,:,1));
  title('Recovering T from R plane'); xlabel('C'); ylabel('T[K]'); hold on;
  for ii = 1 : numel(candidate_conditions.T)
    plot(candidate_conditions.C(ii), candidate_conditions.T(ii) ,'r*'); % works for linspaced T & C vectors
  end
end

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Testing influence of model's parameters on R_sim %%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Define a struct for the model parameters:
%% Concentration model is same as temperature up to scaling.
max_OPL_radius = max( sum(jetBW(:,symAxis:end),2) );
N_ROWS = size(R_est,1);
model_params.T0 = C2K(T_center_nozzle); % T_nozzle [K]
model_params.C0 = CO2_mfr/tot_mfr; % C_nozzle [1]
model_params.X_PC = N_ROWS - ceil(measured_potential_core_length); % Potential Core height [pixels]
model_params.Y_PC = Y(end); % Potential Core width @ nozzle [pixels]
model_params.Y_up = max_OPL_radius; %nansum(jetBW(1,symAxis:end)); % Jet width @ the upper row of the image [pixels]
model_params.Y_nozzle = D_nozzle_pixels/2;%nansum(jetBW(end,symAxis:end)); % Jet width @ the bottom row of the image [pixels]
model_params.Y_PC = min(model_params.Y_PC, model_params.Y_nozzle); % enforce this. added 28.11.17
model_params.b = -measured_potential_core_length; % Parameter 'b' [pixels] of the centerline model:
                                %T(x) = (Tx0-Tatm)*(x0-b)/(x-b) + Tatm
model_params.r_0_5_nozzle = (model_params.Y_PC + model_params.Y_nozzle) / 2; % take as mean of those
% TODO: Maybe better estimate below using some common known slope:
model_params.r_0_5_up = (model_params.r_0_5_nozzle + model_params.Y_up) / 2; % take as mean of those

if SEPARATE_C_T_MODELS % if we allow for different C geometric parameters:
  model_params.C_Y_PC = model_params.Y_nozzle; % C Potential Core width @ nozzle [pixels]
end

model_params.is_constant_pc = CONSTANT_POTENTIAL_CORE;

% Backup:
model_params_orig = model_params;

%% Crop the measurement R image of interest:
if GEN_T_BASED_ON_ORIG_DIM
  [T_im, ~] = GasHelper.gen_T_C_images_from_model(model_params, atm_params, N_ROWS, (size(R_est,2)-symAxis+1)*ones(N_ROWS, 1));
  T_im = T_im(:,1:max_OPL_radius); % crop
else
  [T_im, ~] = GasHelper.gen_T_C_images_from_model(model_params, atm_params, N_ROWS);
end
%% Allow another boundary for the jet:
jet_boundary_line = GasHelper.get_jet_boundary_line(R_est(:,symAxis:end));
jet_boundary_line(jet_boundary_line > size(T_im,2)) = size(T_im,2);

%% Crop the measurement:
R_m = R_est(:, symAxis:symAxis+size(T_im,2)-1) - repmat(R_est(:, symAxis+size(T_im,2)-1), [1 size(T_im,2)]);


%% release assumption:
jet_boundary_line = size(T_im,2) * ones(size(T_im,1), 1);

%% %% Pre-Calculate the matrices of geometric terms for the onion-peeling stuff, for each row:
% TODO: save it to file and load!
A_array = cell(N_ROWS, 1);
for ii = 1 : N_ROWS
  A_array{ii} = GasHelper.calculate_geometric_matrix(jet_boundary_line(ii));
end


%% Generate image of weights and the ROI:
x0 = floor(measured_potential_core_length);
rows = 1 : N_ROWS;
max_OPL_radius = max( sum(jetBW(rows,symAxis:end),2) );
max_dim = max(N_ROWS, max_OPL_radius);

switch WEIGHTS_TYPE
  case 1 % OPTION 1:
    weights_im = jetBW(rows, symAxis:symAxis+max_OPL_radius-1);
    % Use the estimated potential core for weights:
    for xx = X(Y>0)
      weights_im(xx, 1:ceil(Y(xx)) ) = xx; % give linearly increasing weight toward the nozzle
    end
  case 2 % OPTION 2: Linear decay dowstream and heavy weighting the axis:
    w = repmat(max_dim:-1:1, [N_ROWS, 1]);
    w = flipud(w + w');
    w = w(:,1:max_OPL_radius);
    w(:,1) = 10*w(:,1); % ADDED, Check
    weights_im = w .* jetBW(rows, symAxis:symAxis+max_OPL_radius-1);
  case 3 % OPTION 3: use R_est
    weights_im = R_est(rows, symAxis:symAxis+max_OPL_radius-1).*jetBW(rows, symAxis:symAxis+max_OPL_radius-1);
    weights_im = R_fuzed(rows,1:max_OPL_radius) .* jetBW(rows, symAxis:symAxis+max_OPL_radius-1);
    % Add additional weights for the Potential Core.
    for xx = X(Y>0)
      weights_im(xx, 1:ceil(Y(xx))) = 10 * weights_im(xx, 1:ceil(Y(xx)));
    end
  case 4 % OPTION 4: use R_est_Abel_inverse
    weights_im = R_est_Abel_Inverted(:, 1:size(R_m, 2)) .* jetBW(rows, symAxis:symAxis+max_OPL_radius-1);
    weights_im(isnan(weights_im)) = 0;
    weights_im(weights_im < 0) = 0;
  case 5 % OPTION 5: uniform weights
    weights_im = jetBW(rows, symAxis:symAxis+max_OPL_radius-1);
    % weights_im(:,1) = 100; % centerline is important
    % weights_im(end,weights_im(end,:)>0) = 100; % nozzle is important
  case 6 % OPTION 6 - similar to op.2, w/o decay downstream
    weights_im = jet_validation_mask;
    w = repmat((1:max_dim)', [1, size(weights_im, 2)]);
    weights_im = weights_im .* w;
    % weights_im(jet_validation_mask==0) = 1;
    weights_im(:,1) = 10*max(weights_im(:));
  case 7 % OPTION 7 - weight according to distance from the center of the orifice. Favor the downstream axis.
    [M,~] = size(R_est_Abel_Inverted);
    weights_im = nan(M,max_OPL_radius);
    for iR = 1:size(weights_im,1)
      for iC = 1:size(weights_im,2)
        weights_im(iR,iC) = 1 / sqrt((iR-M)^2 + iC^2) ; % no division by zero as indices >0
%         weights_im(iR,iC) = sqrt(iR^2 + (M/max_OPL_radius*(iC-max_OPL_radius))^2) ;
      end
    end
    weights_im = weights_im .* jetBW(rows, symAxis:symAxis+max_OPL_radius-1);
  case 8 % OPTION 8 - weight according to distance from the center of the orifice. Favor the downstream axis.
    [M,~] = size(R_est_Abel_Inverted);
    weights_im = nan(M,max_OPL_radius);
    for iR = 1:size(weights_im,1)
      for iC = 1:size(weights_im,2)
        weights_im(iR,iC) = sqrt((iR-M)^2 + 10*iC^2) ; % more weight on columns
      end
    end
    weights_im = weights_im + nanmax(weights_im(:)); % ???
    weights_im = 1 ./ weights_im;
    weights_im = weights_im .* jetBW(rows, symAxis:symAxis+max_OPL_radius-1);    
  otherwise
end
weights_im = weights_im ./ nansum(weights_im(:)); % Normalization

%% Initialization: Initial guess & bounds over the model's parameters:
%% x = [T0, X_PC, Y_PC, b, Y_nozzle, Y_up, C0, r_0_5_nozzle, r_0_5_up]'
%% or, if SEPARATE_C_T_MODELS==true - x = [x, C_Y_PC]
tol = 1e-3;%1e-2; % 2/12/17 updated from 1e-5
X0 = [model_params.T0;              model_params.X_PC;          model_params.Y_PC; ...
      model_params.b;               model_params.Y_nozzle;      model_params.Y_up; ...
      CO2_mfr/tot_mfr;              model_params.r_0_5_nozzle;  model_params.r_0_5_up];
if SEPARATE_C_T_MODELS
  X0 = [X0;  model_params.C_Y_PC];
end
%%
case_title = ['T_r_e_f=',num2str(C2K(str2num(TrefStr))),'K: Initial Guess: '];
model_params = set_model_params(X0);
[R_sim, T_im, ~] = get_R_sim(model_params, false, false, atm_params, jetParams, calibParams, jet_boundary_line, geometry, symAxis, A_array, F_R, F_TAU, tau_avg);
plot_result(model_params, D_nozzle_pixels, symAxis_for_plots, R_sim, R_m, T_im, T_meas, weights_im, case_title);


%% Setting lower & upper bounds - holding Y_PC constant
%% x = [T0, X_PC, Y_PC, b, Y_nozzle, Y_up, C0, r_0_5_nozzle, r_0_5_up]'
if NARROW_OPTIMIZATION_BOUNDS
  lb = [max(C2K(0.95*K2C(X0(1))), C2K(50)); floor(0.8*X0(2)); floor(0.5*X0(3)); ...
        X0(4)-5*N_ROWS;                     X0(5);            X0(6); ...
        X0(7);                              0.5*X0(5);        0.5*X0(6)];
  
  ub = [min(C2K(1.05*K2C(X0(1))),C2K(500)); ceil(1.2*X0(2));  ceil(X0(5)); ...
        0;                                  X0(5);            X0(6); ...
        X0(7);                              2*X0(5);          2*X0(6)];

else
  
  lb = [max(C2K(0.85*K2C(X0(1))), C2K(50)); floor(0.5*X0(2)); floor(0.5*X0(3)); ...
        X0(4)-4*N_ROWS;                     X0(5);            X0(6); ...
        X0(7);                              0.5*X0(5);        0.5*X0(6)];
  
  ub = [min(C2K(1/0.85*K2C(X0(1))),C2K(500)); ceil(2*X0(2));  ceil(1.1*X0(5)); ...
        0;                                    X0(5);          X0(6); ...
        X0(7);                                2*X0(5);           2*X0(6)];
end

%% x = [T0, X_PC, Y_PC, b, Y_nozzle, Y_up, C0, r_0_5_nozzle, r_0_5_up]'
%% Define the set of linear constraints that satisfies:  Ax <= b
% Modified - 7/1/18:
% The following constraints were released: r_0_5_nozzle <= Y_nozzle & r_0_5_up <= Y_up
% And added"  r_0_5_nozzle <= r_0_5_up
A = [0  1  0  0  0  0  0  0  0; % X_PC     <= N_ROWS
     0  0  1  0 -1  0  0  0  0; % Y_PC     <= Y_nozzle
     0  0  0  0  1 -1  0  0  0; % Y_nozzle <= Y_up
     0  0  1  0  0  0  0 -1  0; % Y_PC <= r_0_5_nozzle
     0  0  0  0  0  0  0  1 -1]; % r_0_5_nozzle <= r_0_5_up
b = [N_ROWS; 0; 0; 0; 0];

if SEPARATE_C_T_MODELS %modify the constraints:
  lb = [lb; floor(model_params.C_Y_PC/2)];
  ub = [ub; ceil(1.1*model_params.Y_nozzle)];
  A = [A, zeros(size(A,1),1)];
  A = [A; [0 0 1 0 0 0 0 0 0 -1]]; % Y_PC <= C_Y_PC (T has boundary layer, C don't)
  b = [b; 0];
end

if size(jet_validation_mask,2) ~= size(weights_im,2)
  jet_validation_mask = jet_validation_mask(:,1:size(weights_im,2));
end

%% Solve for the negligible self-absorption case (much faster):
options = optimset('MaxIter',1000,'MaxFunEvals',2000,'TolX',tol,'PlotFcns',@optimplotfval);
case_title = ['T_r_e_f=',num2str(C2K(str2num(TrefStr))),'K: w/o self-absorption: '];

f = @(x) f0(x, R_m, weights_im, false, atm_params, jetParams, calibParams, jet_boundary_line, geometry, symAxis, A_array, F_R, tau_avg);

tic
[x,fval,exitflag,output ]= fminsearchcon(f, X0, lb, ub, A, b, [], options);
toc
model_params = set_model_params(x);
[R_sim, T_im, ~] = get_R_sim_no_self_absorp(model_params, false, atm_params, jetParams, calibParams, jet_boundary_line, geometry, symAxis, A_array, F_R, tau_avg);
plot_result(model_params, D_nozzle_pixels, symAxis_for_plots, R_sim, R_m, T_im, T_meas, weights_im, case_title);
disp('Final Loss value:');
disp(fval);

% keyboard;

%% Optionaly feed last solution to the model WITH self-absorption:
if INITIALIZE_SLOW_OPTIMIZATION_FROM_SOLUTION_OF_FAST_ONE
  x0_self_abs = x;
else
  x0_self_abs = X0;
end
options_so = optimset('MaxIter',500,'MaxFunEvals',700,'TolX',tol,'PlotFcns',@optimplotfval);
case_title = ['T_r_e_f=',num2str(C2K(str2num(TrefStr))),'K: w/ self-absorption: '];

f_self_abs = @(x) f_self_abs0(x, R_m, weights_im, false, false, atm_params, jetParams, calibParams, jet_boundary_line, geometry, symAxis, A_array, F_R, F_TAU, tau_avg);

tic
[x2,fval2,exitflag2,output2 ]= fminsearchcon(f_self_abs, x0_self_abs, lb, ub, A, b, [], options_so);
toc
model_params = set_model_params(x2);
[R_sim, T_im, ~] = get_R_sim(model_params, false, false, atm_params, jetParams, calibParams, jet_boundary_line, geometry, symAxis, A_array, F_R, F_TAU, tau_avg);
plot_result(model_params, D_nozzle_pixels, symAxis_for_plots, R_sim, R_m, T_im, T_meas, weights_im, case_title);
disp('Final Loss value:');
disp(fval2)

keyboard;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
return

% %% Save the updated DataBase:
% tic
% save((fullfile(pwd,R_DB_file_name)),'R_DB_struct');
% toc
end

%_______________________________________________________________________________

function [hF] = plot_result(model_params, D_nozzle_pixels, symAxis, R_simulated, R_measured, T_im, T_meas, weights_im, title_str, compare_mid_title, compare_desc_title)
  if nargin < 10
    compare_mid_title = '';
    compare_desc_title = '';
  end
  N_ROWS = size(R_simulated,1);
  PC_slope = model_params.Y_PC/(N_ROWS-model_params.X_PC); % Potential core slope
  Y_PC_up = model_params.Y_PC - PC_slope*N_ROWS;
  Y = linspace(Y_PC_up, model_params.Y_PC, N_ROWS);
  potential_core_line = [(1:N_ROWS)', Y(:)];
  hF = plot_4_result_graphs(R_measured, R_simulated, model_params, D_nozzle_pixels, weights_im, potential_core_line, title_str);
  plot_R_simulated_Vs_measured(R_measured, R_simulated, D_nozzle_pixels, title_str);
  % Plot Reconstructed T image:
  if ~isempty(T_meas)
    GasHelper.Compare_TC_samples_to_reconstructed_T(T_im, T_meas, D_nozzle_pixels, symAxis, title_str, compare_mid_title, compare_desc_title);
    axis square
  else
    h = GasHelper.plotIm(T_im, [title_str,'Reconstructed temperature [K]']);
    GasHelper.addTitle2Colorbar(h, 'T[K]');
    GasHelper.normalize_axes_half_im(T_im, D_nozzle_pixels, 2, 5);
  end
  disp('---------------------------------------------------------------------');
  disp(title_str);
  disp(model_params);
end

function [hF, similarity_values] = plot_4_result_graphs(R_m, R_sim, model_params, D_nozzle_pixels, weights_im, potential_core_line, title_str)
  if ~exist('weights_im', 'var')
    weights_im = [];
  end
  if ~exist('title_str', 'var')
    title_str = [];
  end
  plot_potential_core = false;
  if exist('potential_core_line', 'var')
    plot_potential_core = true;
  end
  plot_C_potential_core = false;
  if isfield(model_params,'C_Y_PC')
    plot_C_potential_core = true;
  end
  N_ROWS = size(R_sim,1);
  % Calc evaluation metrics:
  similarity_values = GasHelper.distance_between_images( R_m(~isnan(R_sim)), R_sim(~isnan(R_sim)), weights_im(~isnan(R_sim)) );
  tv_norm = GasHelper.tvnorm( abs(R_m-R_sim) );
  % Normalization:
  if isempty(weights_im)
    nElements = numel(R_m(~isnan(R_sim)));
  else
    nElements = nnz(weights_im);
  end
  tv_norm = tv_norm ./ nElements;
  similarity_values.L1 = similarity_values.L1 ./ nElements;
  similarity_values.L2 = similarity_values.L2 ./ nElements;
  % Plot intermediate results:
%   eval_str = ['||_L_1 =',num2str(similarity_values.L1),'  ||_L_2 =',num2str(similarity_values.L2),',  ||_t_v =',num2str(tv_norm)];
  eval_str = ['||error||_1 =',num2str(similarity_values.L1)];  
% Plotting
  hF = figure();
  if ~isempty(title_str)
    suptitle(title_str);
  end
  rel_error_im = 100*abs((R_m-R_sim)./R_m);
  rel_error_im(~weights_im) = NaN;
  subplot(2,2,[1 3]); 
  imagesc(rel_error_im);
%   ax = gca;
%   ax.YDir = 'normal';
  yt = fliplr(size(rel_error_im,1):-floor(D_nozzle_pixels/2):1);
  ytl = strsplit(num2str((numel(yt)-1:-1:0)/2), ' ');
  set(gca,'ytick',yt);
  set(gca,'yticklabel',ytl);
  xt = strsplit(num2str(linspace(0, ceil(size(rel_error_im,2)/D_nozzle_pixels), 5)), ' ');
  set(gca,'xtick',1+linspace(0, ceil(size(rel_error_im,2)/D_nozzle_pixels), 5)*D_nozzle_pixels );
  set(gca,'xticklabel',xt);
  hc = colorbar; ylabel(hc, 'Relative error [%]'); caxis([0 100]); impixelinfo; axis image;
  title(['Error in R [%].     ',eval_str]);
  if plot_potential_core
    potential_core_line(potential_core_line < 0) = NaN;
    hold on; plot(potential_core_line(:,2), potential_core_line(:,1),'r');
    if plot_C_potential_core
      C_x = [model_params.C_Y_PC, 1];
      C_y = [N_ROWS, model_params.X_PC];
      line(C_x,C_y,'Color','cyan','LineStyle','-');
    end
  end
  plot(linspace(model_params.r_0_5_up,model_params.r_0_5_nozzle, N_ROWS), 1:N_ROWS,'m');
  xlabel('y/D'); ylabel('x/D');
  
  subplot(2,2,2); plot((1:numel(R_m(end,:)))/D_nozzle_pixels, R_m(end,:),'DisplayName','measured'); hold on;
  plot((1:numel(R_sim(end,:)))/D_nozzle_pixels, R_sim(end,:),'DisplayName','simulated');
  title('R comparison @ orifice'); xlabel('y/D'); ylabel('R'); 
  legend('-DynamicLegend', 'Location','northeast');
  subplot(2,2,4); plot((1:numel(R_m(:,1)))/D_nozzle_pixels, flipud(R_m(:,1)),'DisplayName','measured'); xlabel('x/D'); ylabel('R'); hold on;
  plot((1:numel(R_sim(:,1)))/D_nozzle_pixels, flipud(R_sim(:,1)),'DisplayName','simulated');
  title('R comparison @ Centerline');
  legend('-DynamicLegend', 'Location','northeast');

  GasHelper.maximizeFigure;
end


function [hF, similarity_values] = plot_4_result_graphs_verbose(R_m, R_sim, model_params, weights_im, potential_core_line , title_str)
  if ~exist('weights_im', 'var')
    weights_im = [];
  end
  if ~exist('title_str', 'var')
    title_str = [];
  end
  plot_potential_core = false;
  if exist('potential_core_line', 'var')
    plot_potential_core = true;
  end
  N_ROWS = size(R_sim,1);
  % Calc evaluation metrics:
  similarity_values = GasHelper.distance_between_images( R_m(~isnan(R_sim)), R_sim(~isnan(R_sim)), weights_im(~isnan(R_sim)) );
  tv_norm = GasHelper.tvnorm( abs(R_m-R_sim) );
  % Normalization:
  if isempty(weights_im)
    nElements = numel(R_m(~isnan(R_sim)));
  else
    nElements = nnz(weights_im);
  end
  tv_norm = tv_norm ./ nElements;
  similarity_values.L1 = similarity_values.L1 ./ nElements;
  similarity_values.L2 = similarity_values.L2 ./ nElements;
  % Plot intermediate results:
%   eval_str = ['||_L_1 =',num2str(similarity_values.L1),'  ||_L_2 =',num2str(similarity_values.L2),',  ||_t_v =',num2str(tv_norm)];
  eval_str = ['||_L_1 =',num2str(similarity_values.L1)];  
% Plotting
  hF = figure();
  if ~isempty(title_str)
    suptitle(title_str);
  end
  rel_error_im = 100*abs((R_m-R_sim)./R_m);
  rel_error_im(~weights_im) = NaN;
  subplot(2,2,1); imagesc(rel_error_im); colorbar; caxis([0 100]); impixelinfo; axis image;
  hold on; plot(1:size(R_m,2), floor(size(R_m,1)/2)*ones(1,size(R_m,2)), '.m','MarkerSize',3); hold off;
  title(['Error in R [%]']);
  if plot_potential_core
    potential_core_line(potential_core_line < 0) = NaN;
    hold on; plot(potential_core_line(:,2), potential_core_line(:,1),'r');
  end
  plot(linspace(model_params.r_0_5_up,model_params.r_0_5_nozzle, N_ROWS), 1:N_ROWS,'m');
  xlabel(eval_str);
  subplot(2,2,2); plot(R_m(floor(end/2),:),'DisplayName','measured'); xlabel('column'); ylabel('R'); hold on;
  plot(R_sim(floor(end/2),:),'DisplayName','simulated');
  title('R comparison @ half row');
  legend('-DynamicLegend', 'Location','northeast');
  subplot(2,2,3); plot(R_m(:,1),'DisplayName','measured'); xlabel('row'); ylabel('R'); hold on;
  plot(R_sim(:,1),'DisplayName','simulated');
  title('R comparison @ Centerline');
  legend('-DynamicLegend', 'Location','northeast');
  subplot(2,2,4); plot(R_m(end,:),'DisplayName','measured'); hold on;
  plot(R_sim(end,:),'DisplayName','simulated');
  title('R comparison @ Nozzle'); xlabel('column'); ylabel('R');
  legend('-DynamicLegend', 'Location','northeast');
  GasHelper.maximizeFigure;

  if exist('model_params', 'var')
    % Print the table of model parameters:
    % https://www.mathworks.com/matlabcentral/answers/254690-how-can-i-display-a-matlab-table-in-a-figure
    names = fieldnames(model_params);
    vals = zeros(size(names));
    for iF = 1 : numel(names)
      vals(iF) = getfield(model_params, names{iF});
    end
    vals(1) = vals(1); % T[K]
    T = table(vals, 'RowNames', names); % Generate a table
    % Get the table in string form.
    TString = evalc('disp(T)');
    % Use TeX Markup for bold formatting and underscores.
    TString = strrep(TString,'<strong>','\bf');
    TString = strrep(TString,'</strong>','\rm');
    TString = strrep(TString,'_','\_');
    % Get a fixed-width font.
    FixedWidth = get(0,'FixedWidthFontName');
    annotation(gcf,'Textbox','LineStyle','none','String',TString,'Interpreter','Tex',...
      'FontName',FixedWidth,'Units','Normalized','Position',[0 0.9 0.1 0.1]);
  end
end


function model_params = set_model_params(x)
% Gets a vector 'x' and assign model parameters accordingly:
% x === [T0, X_PC, Y_PC, b, Y_nozzle, Y_up, C0. r_0_5_nozzle, r_0_5_up]'
% OR: [T0, X_PC, Y_PC, b, Y_nozzle, Y_up, C0. r_0_5_nozzle, r_0_5_up, C_Y_PC]'
  model_params.T0 = x(1);
  model_params.X_PC = ceil(x(2));
  model_params.Y_PC = x(3);
  model_params.b = ceil(x(4));
  model_params.Y_nozzle = x(5);
  model_params.Y_up = x(6);
  model_params.C0 = x(7);
  model_params.r_0_5_nozzle = x(8);
  model_params.r_0_5_up= x(9);
  if numel(x) > 9
    model_params.C_Y_PC = x(10);
  end
end

function h = plot_R_simulated_Vs_measured(R_m, R_sim, D_nozzle_pixels,title)
  im = [fliplr(R_sim) R_m];
  symAxis = size(R_sim,2)+1;
  h = GasHelper.plotIm(im, 'R simulated (left) Vs R measured (right)');
  if nargin > 3
    suptitle(title);
  end
  GasHelper.normalize_axes_full_im(im,D_nozzle_pixels, symAxis, 2, 2);
end

function [R_sim_projected, T_im, C_im] = get_R_sim(model_params, ignoreSelfAbsorption, doNotProjectForward,  atm_params, jetParams, calibParams, jet_boundary_line, geometry, symAxis, A_array, F_R, F_TAU, tau_avg)
  %Generate T & C images:
  if numel(fieldnames(model_params)) > 9 % Separated C & T models:
    [T_im, C_im] = GasHelper.gen_T_C_images_from_model_separated(model_params, atm_params, numel(jet_boundary_line), jet_boundary_line); % ~0.04 sec
  else
    [T_im, C_im] = GasHelper.gen_T_C_images_from_model(model_params, atm_params, numel(jet_boundary_line), jet_boundary_line); % ~0.04 sec
  end
  R_sim_projected = nan(size(T_im));
  %Calculate profile's emission:
  emission_sim_im = F_R(T_im(:), C_im(:), jetParams.OPL*ones(numel(T_im),1));% Interpolate R DB
  emission_sim_im =  geometry .* calibParams.K(1:size(T_im,1),symAxis:symAxis+size(T_im,2)-1) .* reshape(emission_sim_im, size(T_im,1),[]);
  %Calculate profile's transmission:
  if ignoreSelfAbsorption % Ignore self-absorption
    tau_sim_im = ones(size(T_im));
  else
    tau_sim_im = F_TAU(T_im(:), C_im(:), jetParams.OPL*ones(numel(T_im),1));% interpolate TAU DB
    tau_sim_im = reshape(tau_sim_im, size(T_im,1),[]);
  end
  % Project the profiles to the camera's domain if needed:
  for iRow = 1 : size(T_im, 1)% Loop over rows:
    r = jet_boundary_line(iRow); % neglegable improvement over above
    % Estimating the measured radiation using the T,C models:
    if doNotProjectForward % Set R_sim_projected==emission_sim:
      R_sim_projected(iRow, 1:r) = emission_sim_im(iRow,1:r);
    else % Otherwise, Apply Forward Projection:
      R_sim_projected(iRow, 1:r) = GasHelper.forward_onion_peeling(emission_sim_im(iRow,1:r), tau_sim_im(iRow,1:r), A_array{iRow});
    end
  end
  % Approximate path's transmission as constant:
  R_sim_projected = R_sim_projected ./ tau_avg;
end

% Faster function w/o self-absorption
function [R_sim_projected, T_im, C_im] = get_R_sim_no_self_absorp(model_params, doNotProjectForward, atm_params, jetParams, calibParams, jet_boundary_line, geometry, symAxis, A_array, F_R, tau_avg)
  %Generate T & C images:
  if numel(fieldnames(model_params)) > 9 % Separated C & T models:
    [T_im, C_im] = GasHelper.gen_T_C_images_from_model_separated(model_params, atm_params, numel(jet_boundary_line), jet_boundary_line); % ~0.04 sec
  else
    [T_im, C_im] = GasHelper.gen_T_C_images_from_model(model_params, atm_params, numel(jet_boundary_line), jet_boundary_line); % ~0.04 sec
  end
  R_sim_projected = nan(size(T_im));
  %Calculate profile's emission:
  emission_sim_im = F_R(T_im(:), C_im(:), jetParams.OPL*ones(numel(T_im),1));% Interpolate R DB
  emission_sim_im =  geometry .* calibParams.K(1:size(T_im,1),symAxis:symAxis+size(T_im,2)-1) .* reshape(emission_sim_im, size(T_im,1),[]);
  % Project the profiles to the camera's domain if needed:
  for iRow = 1 : size(T_im, 1)% Loop over rows:
    r = jet_boundary_line(iRow);
    % Estimating the measured radiation using the T,C models:
    if doNotProjectForward % Set R_sim_projected==emission_sim:
      R_sim_projected(iRow, 1:r) = emission_sim_im(iRow,1:r);
    else % Otherwise, Apply Forward Projection:
      R_sim_projected(iRow, 1:r) = GasHelper.forward_onion_peeling_no_self_absorp(emission_sim_im(iRow,1:r), A_array{iRow});
    end
  end
  % Approximate path's transmission as constant:
  R_sim_projected = R_sim_projected ./ tau_avg;
end


%% cost-function wrappers:
function [cost] = f0(x, R_m, weights_im, varargin)
  % varargin === rest_of_parameters_of_simulator.
  model_params = set_model_params(x);
  [r_sim, ~, ~] = get_R_sim_no_self_absorp(model_params, varargin{:});
  similarity_val = GasHelper.distance_between_images( R_m(~isnan(r_sim)), r_sim(~isnan(r_sim)), weights_im(~isnan(r_sim)) );
  cost = similarity_val.L1;
end

function [cost] = f_self_abs0(x, R_m, weights_im, varargin)
  % varargin === rest_of_parameters_of_simulator.
  model_params = set_model_params(x);
  [r_sim, ~, ~] = get_R_sim(model_params, varargin{:});
  similarity_val = GasHelper.distance_between_images( R_m(~isnan(r_sim)), r_sim(~isnan(r_sim)), weights_im(~isnan(r_sim)) );
  cost = similarity_val.L1;
end

function [cost] = f_abel0(x, R_m_Abel, weights_im, model_params, varargin)
  % varargin === rest_of_parameters_of_simulator.
  model_params.T0 = x(1);
  model_params.X_PC = ceil(x(2));
  model_params.Y_PC = x(3);
  [emission_sim, ~, ~] = get_R_sim(model_params, varargin{:});
  similarity_val = GasHelper.distance_between_images( R_m_Abel(~isnan(emission_sim)), emission_sim(~isnan(emission_sim)), weights_im(~isnan(emission_sim)) );
  cost = similarity_val.L1;
end