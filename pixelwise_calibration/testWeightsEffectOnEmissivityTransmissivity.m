%% This test checks the effect of using the Camera's detector transfer function
%% together with Planck as the weighting function for calculation of emissivity & transmissivity,
%% instead of using Plack alone.
%% Rational: These are the only functions that relate photons flux directly to R.
function testWeightsEffectOnEmissivityTransmissivity()
clc; clear; close all;

comparisonMatrixPathArr = ...
  {'C:\Users\matanj\Documents\PythonScripts\WeightComparison_MeanAbsorpMatrix_25.0C_to_500C_C0.0005_to_1_0.02cm.mat',...
  'C:\Users\matanj\Documents\PythonScripts\WeightComparison_MeanAbsorpMatrix_25.0C_to_500C_C0.0005_to_1_0.1cm.mat',...
  'C:\Users\matanj\Documents\PythonScripts\WeightComparison_MeanAbsorpMatrix_25.0C_to_500C_C0.0005_to_1_1cm.mat',...
  'C:\Users\matanj\Documents\PythonScripts\WeightComparison_MeanAbsorpMatrix_25.0C_to_500C_C0.0005_to_1_10cm.mat'};
%     'C:\Users\matanj\Documents\PythonScripts\WeightComparison_MeanAbsorpMatrix_25.0C_to_500C_C0.0005_to_1_100cm.mat'};
OPLArr = [0.02,0.1,1,10];
figure(1);
suptitle(['Weighting fnc effect: \Delta\epsilon for some jet widths',sprintf('\n\n')]);
figure(2);
suptitle(['Weighting fnc effect: 100*\Delta\epsilon/\epsilon (%error) for some jet widths',sprintf('\n\n')]);
for indOPL = 1 : numel(OPLArr)
  comparisonMatrixPath = comparisonMatrixPathArr{indOPL};
  OPL = OPLArr(indOPL); %Jet optic path[cm]
  S = load(comparisonMatrixPath);
  emissivity_Planck = S.meanAbsorpPlanck;
  emissivity_PlanckDetector = S.meanAbsorpDetector;
  y = linspace(25,500,size(emissivity_Planck,1));
  x = linspace(0.0005,1,size(emissivity_Planck,2));
  de = abs(emissivity_Planck - emissivity_PlanckDetector);
  figure(1);
  subplot(2,2,indOPL); imagesc(x,y,de); colorbar;
  title([num2str(OPL),'[cm]']);
  xlabel('CO_2 fraction'); ylabel('T[C]');
  figure(2);
  subplot(2,2,indOPL); imagesc(x,y,100*de./emissivity_Planck); colorbar;
  title([num2str(OPL),'[cm]']);
  xlabel('CO_2 fraction'); ylabel('T[C]');
end
%% Transmission
clear;
comparisonMatrixPathArr = ...
  {'C:\Users\matanj\Documents\PythonScripts\WeightComparison_MeanTransMatrix_OPLs50cm_to_300cm_C0.0005_to_0.003_T0.0[C].mat',...
  'C:\Users\matanj\Documents\PythonScripts\WeightComparison_MeanTransMatrix_OPLs50cm_to_300cm_C0.0005_to_0.003_T0.0[C].mat',...
  'C:\Users\matanj\Documents\PythonScripts\WeightComparison_MeanTransMatrix_OPLs50cm_to_300cm_C0.0005_to_0.003_T0.0[C].mat',...
  'C:\Users\matanj\Documents\PythonScripts\WeightComparison_MeanTransMatrix_OPLs50cm_to_300cm_C0.0005_to_0.003_T0.0[C].mat'};
TArr = [0,20,25,30];

figure(3);
suptitle(['Weighting fnc effect: \Delta\tau for some amibent temperatures',sprintf('\n\n')]);
figure(4);
suptitle(['Weighting fnc effect: 100*\Delta\tau/\tau (%error) for some amibent temperatures',sprintf('\n\n')]);
for indT = 1 : numel(TArr)
  comparisonMatrixPath = comparisonMatrixPathArr{indT};
  T = TArr(indT); %Jet optic path[cm]
  S = load(comparisonMatrixPath);
  transmissivity_Planck = S.meanTransPlanck;
  transmissivity_PlanckDetector = S.meanTransDetector;
  y = linspace(50,300,size(transmissivity_Planck,1));
  x = linspace(0.0005,1,size(transmissivity_Planck,2));
  dTau = abs(transmissivity_Planck - transmissivity_PlanckDetector);
  figure(3);
  subplot(2,2,indT); imagesc(x,y,dTau); colorbar;
  title([num2str(T),'[^oC]']);
  xlabel('CO_2 fraction'); ylabel('OPL[cm]');
  figure(4);
  subplot(2,2,indT); imagesc(x,y,100*dTau./transmissivity_Planck); colorbar;
  title([num2str(T),'[^oC]']);
  xlabel('CO_2 fraction'); ylabel('OPL[cm]');
end






