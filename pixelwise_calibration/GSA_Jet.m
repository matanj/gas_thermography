function GSA_Jet
%% Using GSAT toolbox to apply Global Sensitivity Analysis to the Jet problem
% Source:
% https://www.mathworks.com/matlabcentral/fileexchange/40759-global-sensitivity-analysis-toolbox

clc; clear;
addpath(genpath('C:\Users\matanj\Documents\MATLAB\GSAT\'), '-end');

% Get the data:
[ParamsFileName, ParamsFilePath] = uigetfile({'*.mat','MATLAB data'},'Select Calibration parameters set',[pwd,'\CalibrationParams.mat']);
if isempty(ParamsFileName)
  error('invalid folder path...');
end
load([ParamsFilePath, ParamsFileName]);

C2K = @(x) x + 273.15; % Celsius to Kelvin
T2R = @(temp, em, trans, Kw, a0)    GasHelper.T2R(temp, T, em, trans, Kw, a0);

Kw = nanmean(Kw(:));
a0 = nanmean(a0(:));


N = 10000; %number of samples
% create a new project 
pro = pro_Create();
% add 4 input variables with a pdf uniformely distributed
pro = pro_AddInput(pro, @(N)pdf_Uniform(N, [0.001 1]), 'e');
pro = pro_AddInput(pro, @(N)pdf_Uniform(N, [0.4 1]), 'tr');
pro = pro_AddInput(pro, @(N)pdf_Uniform(N, [Kw-0.1*Kw Kw+0.1*Kw]), 'Kw');
pro = pro_AddInput(pro, @(N)pdf_Uniform(N, [a0-0.1*a0 a0+0.1*a0]), 'a0');
% Alternatively, add 4 input variables with a Sobol pdf distribution
pro = pro_AddInput(pro, @(N)pdf_Sobol(N, [0.001 1]), 'e');
pro = pro_AddInput(pro, @(N)pdf_Sobol(N, [0.4 1]), 'tr');
pro = pro_AddInput(pro, @(N)pdf_Sobol(N, [Kw-0.1*Kw Kw+0.1*Kw]), 'Kw');
pro = pro_AddInput(pro, @(N)pdf_Sobol(N, [a0-0.1*a0 a0+0.1*a0]), 'a0');

temps_C = 100; %[C]
temps = C2K(temps_C);

parameters.T = temps;
parameters.calib_T = T;
% set the model, and name it as 'model', to the project:
pro = pro_SetModel(pro, @(x)RModel(x, parameters), 'model');

% set the number of samples for the quasi-random Monte Carlo simulation
pro.N = N;

% initialize the project by calculating the model at the sample points
pro = GSA_Init(pro);

% calculate the total global sensitivity coefficient for the set of 2 variables
% ('param1 and param5) (see sections 2.3 and 2.4)
% [Stot eStot pro] = GSA_GetTotalSy(pro, {'param1', 'param5'});

% calculate the global sensitivity coefficient for the set of all the input
% variables
[S, eS, pro] = GSA_GetSy(pro, {1,2,3,4});

% calculate the first order global sensitivity coefficient for emissivity
[Se, eSe, pro] = GSA_GetSy(pro, {'e'});
% calculate the first order global sensitivity coefficient for
% transmissivity
[Str, eStr, pro] = GSA_GetSy(pro, {'tr'});
% calculate the first order global sensitivity coefficient for
% Kw
[SKw, eSKw, pro] = GSA_GetSy(pro, {'Kw'});
% calculate the first order global sensitivity coefficient for
% Kw
[Sa0, eSa0, pro] = GSA_GetSy(pro, {'a0'});

% calculate the first order global sensitivity coefficients by using FAST
% algorithm
Sfast = GSA_FAST_GetSi(pro);


end