%% Use per-pixel Calibration matrix to retrieve the Jet temperature
%%
% *Synopsis*
%%
function script_JetFindTemp_Tjet_unknown_solve_integral
% clc; close all;
dbstop if error
format short
dbstatus
%% Assumptions:
%%  1) each experiment case hase same #files.
%%  2) All images are of same size (row, cols) & pixel allignment
%%  3) Measured Camera's R is independant of solid angle of the object:
%%     R ~ double_integral(Radiance*dOmega*dA), where here the solid angle is of the detector pixel and Camera's FOV, and the detector's area.

C2K = @(x) x + 273.15; % Celsius to Kelvin
K2C = @(x) x - 273.15; % Kelvin to Celsius

%% OPTIONS
GENERATE_R_DB = false; % loops over T,C and build the R_DB.mat database
GENERATE_TAU_DB = false; % loops over T,C and build the R_DB.mat database
TAU_OUTSIDE_INTEGRAL = true; 
SIMULATE_R_ALONG_ORIFICE = false;
VERIFY_SINGLE_MEASUREMENT = false; % For e.g. OD testing. Temperature of the measurment as specify in its filename will be used alone.
DO_BINARY_SEARCH = true; % If the temperature dependancy is monotone, binary search would be fastest.
CALC_CALIB_PARAMS = false; % if false, use existing set for testing
REPRESENTATIVE_R_CALIB = false; % if true - representative Rs will be used to obtain calibration params. otherwise, parameters per pixel.
ALL_PIXELS_SEE_BB = true; % for calibrating only a subset of the image
FIX_JET_ANGLE = false;
FIX_JET_ANGLE_DEG_TH = 0.5; % Skew angle Threshold [deg]. Above it, jet will be aligned verticaly.
NOZZLE_IN_IMAGE = true; % if the images contain the nozzle, select it and get m2pixel, etc.
USE_BIAS_FRAME = false;
FILTER_THE_RADIATION = true; % filter estimated R with a gaussian LPF.
USE_CFD = true; % for the jet model
bSize = 1; % Block size [pixels]
iterative_Convergence_tol = 1e-7; % tolerance for convergance of the iterative process [R]
N_T_iters = 200; % Maximal number of iterations for the iterative process.
N_C = 100; % number of concentrations
N_OPL = 1000; % number of OPLs
max_guessed_temperature_C = 600; % maximal temperature to guess [C]
%% EXPERIMENT RELATED:
tot_mfr = 2; %[g/s]
CO2_mfr = 0.5; %[g/s]
D_nozzle = 1e-2; % Nozzle's diameter [m]
atm_OPL = 95; % Atmospheric path in [cm].
atm_co2_concentration = 560*1e-6; % Atmospheric CO2 concentration [fraction]
atm_T = 19;%25.5; % Atmospheric temperature [C]
p_tot = 1; % total pressure [atm]
RH = 42;%48; % [%] Relative Humidity in the Air portion of the Jet
%% Calibration related:
OD_guess = 2.0667; %2.099; % Best estimation from "test_integral_calibration_new_17_1_5.m". Declared OD = 2.
detector_pitch_um = 15; % [um] As declared by SCD (Pelican-D)
e_BB = 0.95; % Plate BB
calib_atm_params.OPL = 155; % Calibration Atmospheric path in[cm]
calib_atm_params.co2_concentration = 560*1e-6;% Calibration Atmospheric CO2 concentration [fraction]
calib_atm_params.T = C2K(24); % Calibration Atmospheric temperature [K]
calib_atm_params.RH = 50; %[%]
% e_BB = 0.97; % emissivity of the Black Body used for calibration. assumed constant spatial-wise and spectrum-wise.
% calib_atm_params.OPL = 220; % Calibration Atmospheric path in[cm]
% calib_atm_params.co2_concentration = 650*1e-6;% Calibration Atmospheric CO2 concentration [fraction]
% calib_atm_params.T = C2K(24.5); % Calibration Atmospheric temperature [K]
% calib_atm_params.RH = 38; %[%]
calib_atm_params.pressure_tot = 1; %[atm]
%% For HITRAN spectral calculations:
hitranParams.wn_step = 0.01; % HITRAN's wavenumber step [cm^-1]
hitranParams.min_wn = 2000; % HITRAN's minimal wavenumber [cm^-1]
hitranParams.max_wn = 3333; % HITRAN's maximal wavenumber [cm^-1]
hitranParams.HW_to_eval = 500; % Number of HWHM for Voigt evaluation. Orignally 6000 by Ilya. 500 also good enough.
% Number of HWHM is a tradoff between computation time and accuracy. Worst
% case for validation is with high concentration and temperatures, as
% expected.
hitranParams.hitranFileFullPath = 'C:\Users\matanj\Documents\MATLAB\pixelwise_calibration\data\hitranCO2_H2O_2000_3333_main_isotopologs.par';
%% Default paths:
CalibFilePath = [pwd,'\JetBBCalibrationSaveR_OUTPUT.mat']; % default
% CalibFilePath = [pwd,'\CavityBBCalibrationSaveR_OUTPUT.mat']; % default
CalibOutputName = 'CalibrationParams_Integral.mat';
biasFramePath = 'C:\Users\matanj\Documents\MATLAB\BiasFrameComputed.mat'; % default Bias Frame path
apparatus_tf_path = 'apparatus_tfs.mat'; % default apparatus transfer functions
% filter_tf_from_spectrometer_path = [pwd,'\Filter_cfit_from_Spectrometer.mat'];
R_DB_file_name = 'R_DB.mat'; % DataBase file for logging R(c,T,distance...)
TAU_DB_file_name = 'TAU_DB.mat'; % DataBase file for logging TAU(c,T,distance...)
%% Default test case and related files:
baseDir = 'Z:\FLIR_Experimental\16-10-10_CloseJet\';
imName = [baseDir, 'G001_T500_F4370_IT2693.6_IM_f.ptw'];

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Atmospheric path struct for the experiment case:
atm_params.OPL = atm_OPL; % Atmospheric path [cm]
atm_params.co2_concentration = atm_co2_concentration; % Atmospheric CO2 concentration [fraction]
atm_params.T = C2K(atm_T); % Atmospheric temperature [K]
atm_params.RH = RH; % Atmospheric Water Relative Humidity (RH) [%]
atm_params.pressure_tot = p_tot; % [atm]
% Jet params:
jetPatams.OPL = 100*D_nozzle; % Nozzle diameter in [cm]
jetPatams.co2_concentration = CO2_mfr/tot_mfr;
jetPatams.pressure_tot = p_tot;
%% Load apparatus' transfer functions - Detector, Lens & Filter %%%
[wl_vect, optics_cfits, ~] = GasHelper.get_optics_tf(apparatus_tf_path, hitranParams, false);
% update the initial guess of the filter's OD from its curve:
% OD_guess = 2.615;% calced @ (C=0.5,T=230C). %2.78 calced @(C=0.25,T=129C). %2.56 calced @(C=0.25,T=378C).
optics_tf_wl = GasHelper.modify_optics_tf_by_OD(wl_vect, optics_cfits, OD_guess);
%% conditionally calculate calibration parameters:
if CALC_CALIB_PARAMS
  calib_experiment_params.atmParams = calib_atm_params;
  calib_experiment_params.e_BB = e_BB;
  calib_experiment_params.detector_pitch_um = detector_pitch_um;
  calib_experiment_params.filter_OD = OD_guess;
  tic
  [calibParams] = GasHelper.getSpectralCalibrationFromR(CalibFilePath, wl_vect, hitranParams, calib_experiment_params, optics_tf_wl, REPRESENTATIVE_R_CALIB, TAU_OUTSIDE_INTEGRAL);
  toc
  save(CalibOutputName, 'calibParams');% Save coefficients per pixel
else % Use exisiting calibration params:
  if isempty(CalibOutputName)
    error('invalid folder path...');
  end
  load([pwd, '\', CalibOutputName]);
end
%% Create a file for logging calculated R(T,C,distance), if not exist:
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% HITRAN's DB - Stores R(T,C,OPL) for a given atmospheric scenario (tau) %%%%%
% DB structure's fields:
%   1) T,C,OPL arrays.
%   2) R(T,C,OPL) stores: integral(radiance_photon_flux .* tau .* optics_tf_wn)
%   3) Atmospheric parameters struct.
%   4) HITRAN parameters struct.
%   5) Description.
% Use like: R_simulated = gemoetry_factor * K * R(T,C,OPL).
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if exist([pwd,'\',R_DB_file_name], 'file') == 2 % there is already such file
  tic
  S = load([pwd,'\',R_DB_file_name]);
  toc
  R_DB_struct = S.R_DB_struct;
  % check that HITRAN & atmospheric params of experiment matches the DB's ones:
  assert(R_DB_helper.is_DB_legit_to_case(R_DB_struct, atm_params, hitranParams, TAU_OUTSIDE_INTEGRAL), 'DB''s params don''t match this case!');
else  % create new file
  R_DB_struct = struct('T',[],'C',[],'OPL',[],'R',[]);
  R_DB_struct.T = linspace(C2K(10), C2K(450), N_T_iters )';
  R_DB_struct.C = linspace(400e-6, 1, N_C)';
  R_DB_struct.OPL = [1/1000*(10:1000), 1/100*(101:150) ]'; 
  R_DB_struct.R = zeros(N_T_iters, N_C, numel(R_DB_struct.OPL));
  R_DB_struct.atm_params = atm_params;
  R_DB_struct.hitranParams = hitranParams;
  % Save in version compatible with efficient partial loading/saving.
  % Saving like this should be done only here. Subsequent saves can omit
  % the '-v7.3' flag.
  save([pwd,'\',R_DB_file_name],'R_DB_struct','-v7.3');
end
%% Create a file for logging calculated TAU(T,C,distance), if not exist:
if exist([pwd,'\',TAU_DB_file_name], 'file') == 2 % there is already such file
  tic
  S = load([pwd,'\',TAU_DB_file_name]);
  toc
  TAU_DB_struct = S.TAU_DB_struct;
else
  TAU_DB_struct = struct('T',[],'C',[],'OPL',[],'TAU',[]);
  TAU_DB_struct.T = linspace(C2K(10), C2K(450), 100 )';
  TAU_DB_struct.C = linspace(400e-6, 1, 100)';
  TAU_DB_struct.OPL = [1./(1:100)]';
  TAU_DB_struct.TAU = zeros(numel(TAU_DB_struct.T), numel(TAU_DB_struct.C), numel(TAU_DB_struct.OPL));
  TAU_DB_struct.hitranParams = hitranParams;
  % Save in version compatible with efficient partial loading/saving.
  % Saving like this should be done only here. Subsequent saves can omit
  % the '-v7.3' flag.
  save([pwd,'\',TAU_DB_file_name],'TAU_DB_struct','-v7.3');
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% %%%%%%%%%%%%%%%%% Test the calibration: %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Get test case:
 [imName, baseDir] = uigetfile({'*.ptw','FLIR images'},'Select image to test calibration',imName);
if isempty(imName)
  error('invalid image');
end
%Get the Background file that matches the Image file's name:
bgName = GasHelper.getBGfileFromIMfile(imName, baseDir);
%Get the temperature of the measurement from file's name:
matchStr = regexp(imName,'_T+[0-9]+[0-9]+[0-9]','match');
TrefStr = regexprep(matchStr{1},'_T','');
%Set the image pair object:
imPair = ImageBGPairExt([baseDir,imName]);
imPair.setBG([baseDir,bgName{1}]);
%% Get the Bias frame:
%   [biasFrameFull, biasFrame] = cropBiasFrame2PTWsize(biasFramePath, imInfo);
%% crop the suitable subframe from it and rotate accordingly:
if USE_BIAS_FRAME
  [ biasSubframe ] = suitBiasSubframeFromBiasFrame( imPair.getImage(), imPair.IMG_Info, biasFramePath );
end
subIm = mean(abs(imPair.getSubtracted()), 3); % time averaging the frame sequence
if ALL_PIXELS_SEE_BB
  objMask = true(size(subIm));
else
  [~, object] = GasHelper.processImGetStats(subIm);%get meaningful object within the image
  objMask = true(imPair.IMG_Info.m_rows, imPair.IMG_Info.m_cols);%initial mask
  objMask(isnan(object)) = NaN;
end
%Mask the image to maintain same size:
DL_IM = objMask.*subIm;
DL_IM(DL_IM==0) = 1; % set minimal DL to 1 to avoid zero issues
%% trim image to orifice and get pipe's diameter:
if NOZZLE_IN_IMAGE
  [DL_new, D_nozzle_pixels, m2pixel, pos_nozzle, ~] = GasHelper.trimIm2Nozzle(DL_IM, D_nozzle);
end
%% Rotate Jet if needed:
if FIX_JET_ANGLE
  %% TODO: should apply same to the calibration maps, or rotate back b4 using the calibration
  DL_new = GasHelper.fixJetAngle(DL_new);
end
%% Apply calibration with K
K = calibParams.K;
% First, crop object image to the calibration image, if needed:
if ~(exist('rect','var') ) % if cropping wasn't done:
  [rowsCalib, colsCalib] = size(calibParams.K);
  [rowsNew, colsNew] = size(DL_new);
  if rowsNew ~= rowsCalib || colsNew ~= colsCalib
    rows = 1:floor(pos_nozzle(1,2)); % TODO: what if NOZZLE_IN_IMAGE == false?
    cols = 1:size(DL_new,2);
  else
    rows = 1:size(DL_new,1);
    cols = 1:size(DL_new,2);
  end
else % if 'rect' is present:
  rows = rect(2):rect(2)+rect(4);
  cols = rect(1):rect(1)+rect(3);
end
% If doing pixel-by-pixel and image's size > calibration image, should exit.
% if representative calibration parameters are chosen - no problem
if REPRESENTATIVE_R_CALIB
  K = K(1,1)*ones(numel(rows), numel(cols));
elseif (size(DL_new,1) > size(calibParams.K,1)) || (size(DL_new,2) > size(calibParams.K,2))% exit if image size > calibration image size
  warning('Image size > calibraion image size!! cropping the image.');
  % cropping jet's image:
  dRows = size(DL_new,1) - size(calibParams.K,1);
  dCols = size(DL_new,2) - size(calibParams.K,2);
  if dRows > 0
    rows = rows(dRows+1:end);
  end
  if dCols > 0
    cols = cols(floor(dCols/2)+1 : end-floor(dCols/2));
  end
  DL_new = DL_new(rows, cols);
end
totNaNMask = isnan(K);
DL_new(totNaNMask) = nan;

%% Get the a mask of the actual jet shape, and the centerline of symmetry:
jetBW = GasHelper.getActualJetFromImage(DL_new);
[symAxis, skew_angle_deg] = GasHelper.getAxisOfSymFromJetIm(DL_new.*jetBW);

%% estimate the measurement's radiation from DL = R*IT^P
if USE_BIAS_FRAME % Subtraction of R images
  R_est = GasHelper.GetMeasurementRByMultiIT2RP(baseDir, TrefStr, DL_new, rows, cols, biasSubframe, ALL_PIXELS_SEE_BB);
else % subtraction of DL images
  R_est = GasHelper.GetMeasurementRByMultiIT2RP(baseDir, TrefStr, DL_new, rows, cols, [], ALL_PIXELS_SEE_BB);
end
assert( all(R_est(~isnan(R_est)) >= 0) || all(R_est(~isnan(R_est)) <=0) ); % mixed signs are not allowed!
R_est0 = abs(R_est); % fix for possible confusion of IM and BG
R_orig = abs(R_est);

% Estimate the noise level of the R image:
% [nlevel th] = NoiseLevel(R_orig); nlevel / nanmean(R_orig(:));
bg_part_of_image = R_est.*~jetBW;
bg_part_of_image(jetBW>0) = nan;
noise_parameter = nanstd(bg_part_of_image(:)) / nanmean(bg_part_of_image(:));

gaussian_LPF_param = 1.5; % DEFAULT value
if noise_parameter > 0.5
  gaussian_LPF_param = 2.5;
end

if FILTER_THE_RADIATION
  R_est_filt = imgaussfilt(R_est0, gaussian_LPF_param); % Gaussian LPF. should preserve edges.
  R_est0 = R_est_filt;
end
assert(all(R_est0(:)),'invalid R...');

%% Align the jet vertically, if it worth it:
%% TODO: What about rotating and translating the K image? accordingly? 
RWorld = imref2d(size(R_est0)); % Set R_est coordinate frame as the World coordinate frame. % TODO: ?
if abs(skew_angle_deg) > FIX_JET_ANGLE_DEG_TH
  if sum(sum(isnan(R_est0))) % If there's NaNs in image, set a warning:
    warning('R images has NaNs. Will be converted to 0 before rotation... check the output image!');
    R_est0(isnan(R_est0)) = 0; % cannot transform image with NaN.
  end
  [R_est, ~ ] = GasHelper.fixJetAngle(R_est0, RWorld, skew_angle_deg);
else
  R_est = R_est0;
end

%% apply HITRAN transmittance model for the atmosheric path of the measurement
% tau_spectra_wn = 1; % default w/o applying atm model
[wn_vect, ~, tau_spectra_wn] = MyGetSpectrumGPU(true, 'tau', hitranParams, atm_params);

if TAU_OUTSIDE_INTEGRAL
  [~, optics_tf_wn] = GasHelper.spectralFunc_wl2wn(calibParams.wl_vect, calibParams.optics_tf, true);
  tau_avg = GasHelper.getWeightedTransmittance(hitranParams, optics_tf_wn, atm_params);
  R_est = R_est ./ tau_avg;
end

%% GENEREATE A FULL DB FOR RADIATION WITH OPTIC PATH = PIXEL SIZE:
if GENERATE_R_DB
  jetPatams.OPL = 1/m2pixel*100;
  for indT = 1 : numel(R_DB_struct.T)
    for indC = 1 : numel(R_DB_struct.C)
      jetPatams.T = R_DB_struct.T(indT);
      jetPatams.co2_concentration = R_DB_struct.C(indC);
      params.hitranParams = hitranParams; params.wn_vect = wn_vect; params.jetPatams = jetPatams; params.calibParams = calibParams;
      [~, R_DB_struct] = R_DB_helper.get_R_from_DB(R_DB_struct, jetPatams.T, jetPatams.co2_concentration, jetPatams.OPL, params );    
    end
    disp(['Finished inner loop #',num2str(indT)]);
  end
  save(([pwd,'\',R_DB_file_name]),'R_DB_struct');
  return;
end
%% GENEREATE A FULL DB FOR TRANSMISSION WITH OPTIC PATH = PIXEL SIZE:
if GENERATE_TAU_DB
  jetPatams.OPL = 1/m2pixel*100;
  jetPatams.RH = 40;
  for indT = 1 : numel(TAU_DB_struct.T)
    for indC = 1 : numel(TAU_DB_struct.C)
      jetPatams.T = TAU_DB_struct.T(indT);
      jetPatams.co2_concentration = TAU_DB_struct.C(indC);
      params.hitranParams = hitranParams; params.optics_tf_wn = optics_tf_wn; params.path_params = jetPatams;
      [~, TAU_DB_struct] = TAU_DB_helper.get_TAU_from_DB(TAU_DB_struct, jetPatams.T, jetPatams.co2_concentration, jetPatams.OPL, params );
    end
    disp(['Finished inner loop #',num2str(indT)]);
  end
  save(([pwd,'\',TAU_DB_file_name]),'TAU_DB_struct');
  return;
end

%% Calculate solid_angle of a single Pixel and detector area for later use:
OPL_step = D_nozzle/D_nozzle_pixels*1e2; %in [cm] for HITRAN
% Approximate the solid angle of a cell in the jet:
solid_angle = (OPL_step/(atm_OPL))^2;
detector_area_cm2 = (detector_pitch_um*1e-4)^2; % Detector's area in [cm^2], to match HITRAN units
geometry = solid_angle*detector_area_cm2;

%% Parameters for the models of temperature & concentration profiles in the jet:
params.useCFD = USE_CFD;
if USE_CFD
  CFDpath = [pwd,'\FreeJet.xlsx'];
  params.data = GasHelper.GetCFDdataFromANSYS(CFDpath);
end
params.D = D_nozzle;
params.tot_mfr = tot_mfr; % Total mass flow rate, [g/s]
params.co2_mfr = CO2_mfr; % CO2 mass flow rate, [g/s]
params.Tamb_C = atm_T; % ambient temperature, [C]
params.Camb = atm_co2_concentration; % CO2 concentration in the ambient
params.P0 = 101325; %[Pa]. Pressure @ outlet is 1 atm = 101325[Pa]
idxMax = symAxis;

RWorld = imref2d(size(R_est)); 

%% TAKE only the lines next to the exshaust: % TODO: make better
% next to the nozzle's exhaust:
ROI_rows = bSize*floor(size(DL_new,1)/bSize);  % TODO: remove
ROI_cols = 1:bSize*floor(size(DL_new,2)/bSize);  % TODO: remove

%% Get the measured R at the centerline:
calibParams.K = 0.02*ones(size(calibParams.K));% 5/3/17 TODO: check!!!
% calibParams.K = nanmean(calibParams.K(:))*ones(size(calibParams.K)); % 5/3/17 TODO: check!!!
% For the distribution on the axis, no need for ABEL inverese:
R_measurement = R_est(ROI_rows,symAxis); % Assuming uniform distribution in the potential core
disp(['R from measurement = ',num2str(R_measurement)]);
% Pre-alloc for the iterative process:
T_guesses = nan(N_T_iters,1);
err_norm_vec = nan(N_T_iters,1);

%% Define searchable temperatures:
if VERIFY_SINGLE_MEASUREMENT
  calcTemps_arr = C2K(str2double(TrefStr));
else
  calcTemps_arr = linspace(C2K(atm_T), C2K(max_guessed_temperature_C), N_T_iters);
  % calcTemps_arr = calibParams.T_K; % TODO: remove. this is for testing R_jet Vs R_BB
end
if DO_BINARY_SEARCH
  LU_temp_ind = ceil(numel(calcTemps_arr)/2); % initial guess
else
  LU_temp_ind = 1;
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% ITERATIVE PROCESS: Sweep Tjet in nozzle, compare simulated irradiance to measurement.
R_guess_arr = {};
for k = 1 : min(N_T_iters, numel(calcTemps_arr))
  jetPatams.T = calcTemps_arr(LU_temp_ind);
  params.Tjet_C = K2C(calcTemps_arr(LU_temp_ind)); %New guess for Jet outlet temperature[C]
  T_guesses(k) = params.Tjet_C;
  %% Find emission radiance using HITRAN, in each block, and scale to photon flux (the camera counts photons)
  % Check DB b4 time-consuming evaluation:
  params.hitranParams = hitranParams; params.jetPatams = jetPatams;params.wn_vect = wn_vect; params.calibParams = calibParams; params.tau_spectra_wn = tau_spectra_wn;
  [R_normalized, R_DB_struct] = R_DB_helper.get_R_from_DB(R_DB_struct, jetPatams.T, jetPatams.co2_concentration, jetPatams.OPL, params );
  R_guess = geometry .* calibParams.K .* R_normalized;

  if REPRESENTATIVE_R_CALIB
    R_guess = R_guess * ones(size(DL_new));
  end
  
%   if TAU_OUTSIDE_INTEGRAL
%     R_guess = R_guess ./ tau_avg; % FIX BUG - was multiplication
%   end
  %% Update current value:
  R_guess = R_guess(ROI_rows,symAxis);
  disp(['R_guess(Tjet=',num2str(T_guesses(k)),'C) = ',num2str(R_guess)]);
  R_guess_arr{k} = R_guess;
  %% Check convergence and update guess:
  R_err = R_guess - R_measurement;
  err_norm_vec(k) = norm(R_err);
  if (norm(R_err) < iterative_Convergence_tol) || (numel(calcTemps_arr) == 1)
    break;
  end
  if DO_BINARY_SEARCH % Apply binary search to find temperature:
    if LU_temp_ind==1 % we've reach 2 elemets array
      calcTemps_arr = calcTemps_arr(1);
    elseif norm(R_guess) > norm(R_measurement) % search in the lower temperatures
      calcTemps_arr = calcTemps_arr(1:LU_temp_ind-1); % update search range
    else
      calcTemps_arr = calcTemps_arr(LU_temp_ind+1:end); % update search range
    end
    LU_temp_ind = round(numel(calcTemps_arr)/2); % update new guess index
  else % sweep over entire temperature candidates:
    LU_temp_ind = LU_temp_ind + 1;
  end
end % end of iterative process
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Here we assume the outlet temperature have been found correctly: %%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[~,ind_T_recovered] = min(err_norm_vec); % the assumed index of the recovered temperature
T_center_nozzle = T_guesses(ind_T_recovered);
final_error = 100*abs(T_center_nozzle-str2double(TrefStr))/C2K(str2double(TrefStr)); % percentage error
% Plotting result:
figure;
plot(T_guesses, err_norm_vec); title('||err|| Vs Guessed temperature [C]'); hold on;
plot(T_center_nozzle, err_norm_vec(ind_T_recovered), 'r*');
suptitle(['Measured temperature = ',TrefStr,'^oC, CO_2 concentration = ',num2str(CO2_mfr/tot_mfr), ', Distance = ',num2str(atm_OPL),'cm']);
title(['Estimated temperature = ',num2str(T_center_nozzle),'^oC. ', ' Error[%] = ',num2str(final_error)]);
xlabel('T guess [^oC]'); ylabel('||Radiation error||');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Simulate the apparent R in the nozzle row assuming uniform T_nozzle there:
if SIMULATE_R_ALONG_ORIFICE
  % First, Get the measurement row in the nozzle:
  R_nozzle_row = R_est(ROI_rows,:) .* jetBW(ROI_rows,:);
%   if TAU_OUTSIDE_INTEGRAL
%     R_nozzle_row = R_nozzle_row ./ tau_avg; % FIX BUG - was multiplication
%   end
  locations = find(R_nozzle_row > 0);
  locations_centered = locations - symAxis;
  Radius_nozzle_pixels_new = numel(locations);
  % calculate the vector of OPL(r) in this row:
  OPL_nozzle_vect = 2*sqrt((Radius_nozzle_pixels_new/2)^2 - locations_centered.^2);
  OPL_nozzle_vect = OPL_nozzle_vect/m2pixel*100; % convert to [cm]
  locations_ind_vect = symAxis+1 : locations(end-1);
  % locations_ind_vect = symAxis + ceil(Radius_nozzle_pixels_new/22)*(1:10)';
  R_estimates_at_locations = zeros(size(locations_ind_vect));
  jetPatams.T = C2K(T_center_nozzle);
  for indLocation = 1 : numel(locations_ind_vect)
    jetPatams.OPL = OPL_nozzle_vect(locations == locations_ind_vect(indLocation));
    % Check DB b4 time-consuming evaluation:
    params.hitranParams = hitranParams; params.jetPatams = jetPatams;params.wn_vect = wn_vect; params.calibParams = calibParams; params.tau_spectra_wn = tau_spectra_wn;
    [R_normalized, R_DB_struct] = R_DB_helper.get_R_from_DB(R_DB_struct, jetPatams.T, jetPatams.co2_concentration, jetPatams.OPL, params );
    R_estimate = geometry .* calibParams.K .* R_normalized;
    R_estimates_at_locations(indLocation) = R_estimate(ROI_rows, locations_ind_vect(indLocation));
  end
  figure;
  x = [0, (locations_ind_vect-symAxis)/D_nozzle_pixels];
  y1 = R_nozzle_row([symAxis, locations_ind_vect]);
  y2 = [R_guess, R_estimates_at_locations];
  sim_error = 100 * abs(y2 - y1) ./ y1 ;
  [hAx,hLine1,hLine2] = plotyy([x', x'],[y1',y2'],x,sim_error);
  title('Simulating R Vs Measured R assuming uniform T in the nozzle row');
  xlabel('distance from centerline (D)');
  ylabel(hAx(1),'R'); % left y-axis
  ylabel(hAx(2),'relative error [%]'); % right y-axis
  legend([hLine1(1), hLine1(2), hLine2],'Measured R','Simulated R','error');
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if 0
  %% Restrict the search range of the full image based on the found core termperature:
  N_iters_image = floor(N_T_iters/2);
  calcTemps_arr = linspace(C2K(atm_T), C2K(T_center_nozzle+10), N_iters_image); %found temperature plus addition to account for noise
  
  %% Apply model of temperature & concentration profiles in the jet:
  [C_model, T_model, ~] = GenerateTemperatureConcentrationModelsFit2image(...
    size(DL_new), m2pixel, size(DL_new,1), idxMax, params);
  assert(abs(params.Tjet_C - params.Tamb_C) > eps); % bug below if params.Tjet_C == params.Tamb_C
  theta_model = (T_model - C2K(params.Tamb_C))./(params.Tjet_C - params.Tamb_C); % non-dimentional thermal variable.
  % Divide the image to blocks of mean values:
  theta_model_Blocks = GasHelper.imFromFilteredBlocks(theta_model, bSize);
  avgCBlocks = GasHelper.imFromFilteredBlocks(C_model, bSize);
  % avgCBlocks = avgCBlocks(ROI_rows,ROI_cols);
  % T_model = C2K(params.Tamb_C + (T_center_nozzle - params.Tamb_C).*theta_model); % scaling the model
  avgTBlocks = C2K(params.Tamb_C + (T_center_nozzle - params.Tamb_C).*theta_model_Blocks);% scaling the model
  % avgTBlocks = avgTBlocks(ROI_rows,ROI_cols);
  
  GasHelper.plotIm(avgTBlocks,'Temperature model based on T_c_l_-_n_o_z_z_e_l');
  GasHelper.plotIm(avgCBlocks,'Concentration model based on C_n_o_z_z_e_l');
  
  %% Calc the temperature on the centerline, far away from the nozzle:
  N_Diameters_downstream = 3;
  ROI_rows = max(size(R_est,1) - N_Diameters_downstream*floor(D_nozzle_pixels), 1);
  %% Concentration still assumed equal to orifice's value
  OPL_pixels = sum(jetBW(ROI_rows,symAxis:end));
  R_measurement = R_est(ROI_rows,symAxis); % Assuming uniform distribution in the potential core
  disp(['R from measurement far away from nozzle = ',num2str(R_measurement)]);
  %% Estimating the measured radiation using the T,C models:
  C_vect = avgCBlocks(ROI_rows, symAxis:end);
  T_vect = avgTBlocks(ROI_rows, symAxis:end);
  T_vect = T_vect( ~isnan(T_vect) & T_vect > C2K(params.Tamb_C)+1 );
  C_vect = C_vect( ~isnan(T_vect) & T_vect > C2K(params.Tamb_C)+1 );
  R_estimated_cl_downstream = 0;
  calibParams2 = calibParams;
  calibParams2.K = calibParams.K(ROI_rows,symAxis:symAxis+OPL_pixels-1);
  for indCol = 1 : OPL_pixels % Loop over pixels
    jetPatams.OPL = 1/m2pixel*100;
    jetPatams.T = T_vect(indCol);
    jetPatams.co2_concentration = C_vect(indCol);
    % Check DB b4 time-consuming evaluation:
    params.hitranParams = hitranParams; params.jetPatams = jetPatams;params.wn_vect = wn_vect; params.calibParams = calibParams; params.tau_spectra_wn = tau_spectra_wn;
    [R_normalized, R_DB_struct] = R_DB_helper.get_R_from_DB(R_DB_struct, jetPatams.T, jetPatams.co2_concentration, jetPatams.OPL, params );
    R_guess = geometry .* calibParams2.K(indCol) .* R_normalized;
    %   R_estimated_cl_downstream = R_estimated_cl_downstream + ...
    %         GasHelper.getRfromSpectralCalibration(calibParams2, geometry, wn_vect, radiance_photon_flux_spectra_wn, tau_spectra_wn);
    R_estimated_cl_downstream = R_estimated_cl_downstream + R_guess;
  end
  R_estimated_cl_downstream = 2*R_estimated_cl_downstream; % symmatry assumed; models are symmetric anyway..
  disp(['R estimated far away from nozzle = ',num2str(R_estimated_cl_downstream)]);
end

%% Get a better estimate on the Jet's boundaries:
jetBW = GasHelper.get_mask_of_jet(R_est,symAxis, false);
% Alternativly:
% jetBW = GasHelper.get_mask_of_jet(R_est,symAxis, true);

%{
%% Estimate the noise level in the image:
[nlevel th] = NoiseLevel(R_orig);
nlevel / nanmean(R_orig(:))
bg_part_of_image = R_est.*~jetBW;
bg_part_of_image(jetBW>0) = nan;
noise_parameter = nanstd(bg_part_of_image(:)) / nanmean(bg_part_of_image(:));
disp(['std/max_val of R_est = ',num2str(noise_parameter)]);
%}

%% Apply Inverse Abel, to get the potential core edges of the measurement:
im = R_est.*jetBW;
% if TAU_OUTSIDE_INTEGRAL
%   im = im ./ tau_avg;
% end
[R_est_Abel_Inverted] = GasHelper.InverseAbelHalfImage(im(:,symAxis:end), 1, 'NO', true);
xxx = R_est_Abel_Inverted; xxx(isnan(xxx)) = 0;
R_est_Abel_Inverted_forwarded = GasHelper.ApplyForwardAbel(xxx);
% [X, Y] = GasHelper.get_edge_line_from_image(R_est_Abel_Inverted);
[X, Y] = GasHelper.get_potential_core_from_half_image(R_est_Abel_Inverted);
potential_core_slope = (Y(1)-Y(end))/(X(1)-X(end));
% figure; plot(diff(diff(R_est_Abel_Inverted(end,:))./R_est_Abel_Inverted(end,2:end)))
%% Estimate potenial core top point from its edge line:
potential_core_slope = (Y(end)-Y(1))/(X(end)-X(1));
assert(potential_core_slope>0,'Potential core line estimated wrong!');
measured_potential_core_length = X(end) + Y(1)/potential_core_slope; %[pixels]

if 0
  % Get the potential core edges of the model:
  [C_model, T_model, ~] = GenerateTemperatureConcentrationModelsFit2image(...
    size(DL_new), m2pixel, size(DL_new,1), idxMax, params);
  assert(abs(params.Tjet_C - params.Tamb_C) > eps); % bug below if params.Tjet_C == params.Tamb_C
  theta_model = (T_model - C2K(params.Tamb_C))./(params.Tjet_C - params.Tamb_C); % non-dimentional thermal variable.
  % Divide the image to blocks of mean values:
  theta_model_Blocks = GasHelper.imFromFilteredBlocks(theta_model, bSize);
  avgCBlocks = GasHelper.imFromFilteredBlocks(C_model, bSize);
  avgTBlocks = C2K(params.Tamb_C + (T_center_nozzle - params.Tamb_C).*theta_model_Blocks);% scaling the model
  
  avgTblocksTHed = otsu(avgTBlocks, 6);
  avgTblocksTHed(avgTblocksTHed < 2) = nan;
  avgTblocksTHed(avgTblocksTHed >= 2) = true;
  imModel = avgTBlocks .* avgTblocksTHed;
  dRows = size(imModel,1)-size(R_est_Abel_Inverted,1);
  if dRows > 0
    imModel = imModel(dRows+1:end,:);
  end
  [Xmodel, Ymodel] = GasHelper.get_edge_line_from_image(imModel(:, symAxis:end));
end
%{
%% modify model by the potential core lines difference:
model_potential_core_line = Ymodel;
measured_potential_core_line = X;
[new_im] = GasHelper.shift_model_using_potential_core_line(imModel, symAxis, model_potential_core_line, measured_potential_core_line);

%% Fit model at each row to  a*exp(-((x-c)./d).^(2*n))+b (hyper-gaussian)
paramsss.Tamb = atm_params.T;
paramsss.Camb = atm_params.co2_concentration;
paramsss.Cjet = jetPatams.co2_concentration;
[cIm, TIm] = GasHelper.generate_2d_models_from_hyper_gaussian_fits(new_im, symAxis, paramsss);

%% Recalculate the radiation field based on the updated model:
% Calc the temperature on the centerline, far away from the nozzle:
N_Diameters_downstream = 3;
ROI_rows = max(size(R_est,1) - N_Diameters_downstream*floor(D_nozzle_pixels), 1);
%% Concentration still assumed equal to orifice's value
OPL_pixels = sum(jetBW(ROI_rows,symAxis:end));
R_measurement = R_est(ROI_rows,symAxis); % Assuming uniform distribution in the potential core
disp(['R from measurement far away from nozzle = ',num2str(R_measurement)]);
%% Estimating the measured radiation using the T,C models:
C_vect = cIm(ROI_rows, symAxis:end);
T_vect = TIm(ROI_rows, symAxis:end);
T_vect = T_vect( ~isnan(T_vect) & T_vect > C2K(params.Tamb_C)+1 );
C_vect = C_vect( ~isnan(T_vect) & T_vect > C2K(params.Tamb_C)+1 );
R_estimated_cl_downstream = 0;
calibParams2 = calibParams;
calibParams2.K = calibParams.K(ROI_rows,symAxis:symAxis+OPL_pixels-1);
for indCol = 1 : OPL_pixels % Loop over pixels
  jetPatams.OPL = 1/m2pixel*100;
  jetPatams.T = T_vect(indCol);
  jetPatams.co2_concentration = C_vect(indCol);
  % Check DB b4 time-consuming evaluation:
  params.hitranParams = hitranParams; params.jetPatams = jetPatams;params.wn_vect = wn_vect; params.calibParams = calibParams; params.tau_spectra_wn = tau_spectra_wn;
  [R_normalized, R_DB_struct] = R_DB_helper.get_R_from_DB(R_DB_struct, jetPatams.T, jetPatams.co2_concentration, jetPatams.OPL, params );
  R_guess = geometry .* calibParams2.K(indCol) .* R_normalized;
  %   R_estimated_cl_downstream = R_estimated_cl_downstream + ...
  %         GasHelper.getRfromSpectralCalibration(calibParams2, geometry, wn_vect, radiance_photon_flux_spectra_wn, tau_spectra_wn);
  R_estimated_cl_downstream = R_estimated_cl_downstream + R_guess;
end
R_estimated_cl_downstream = 2*R_estimated_cl_downstream; % symmatry assumed; models are symmetric anyway..
disp(['R estimated far away from nozzle = ',num2str(R_estimated_cl_downstream)]);

%}


%% Fit model at each row to  a*exp(-((x-c)./d).^(2*n))+b (hyper-gaussian)
% paramsss.Tamb = atm_params.T;
% paramsss.Camb = atm_params.co2_concentration;
% paramsss.Cjet = jetPatams.co2_concentration;
% [cIm, TIm] = GasHelper.generate_2d_models_from_hyper_gaussian_fits(imModel, symAxis, paramsss);

%% Another try, using only the inverse Abel transform of R image and iterate the R_DB.
% First, restrict search ranges:

conditions_ranges_struct.Tmax = C2K(1.05*T_center_nozzle);
conditions_ranges_struct.Tmin = C2K(atm_T);
conditions_ranges_struct.Cmax = 1.1*CO2_mfr/tot_mfr;
conditions_ranges_struct.Cmin = 0.9*CO2_mfr/tot_mfr;%atm_co2_concentration;
conditions_ranges_struct.OPL = 1/m2pixel*100;


% TODO: the following is very sensative to R_scale error!!!! 
R_est_Abel_Inverted_scaled = R_est_Abel_Inverted ./ geometry ./ calibParams.K(1:ROI_rows,symAxis:end);
R_scaled_temp = R_est_Abel_Inverted_scaled(ROI_rows, 1);
R_DB_tol = 0.005*R_est_Abel_Inverted(ROI_rows, 1) ./ geometry ./ calibParams.K(ROI_rows,symAxis); % tolerance for the centerline

[candidate_conditions] = R_DB_helper.get_parameters_from_R_DB(R_DB_struct, R_scaled_temp, conditions_ranges_struct, R_DB_tol);
assert(~isempty(candidate_conditions),'No entries with OPL in R_DB!')

%% Recalculate the radiation field based on the updated model:
% Calc the temperature on the centerline, far away from the nozzle:
N_Diameters_downstream = 2.3;
% N_Diameters_downstream = 0.01;
ROI_rows = max(size(R_est,1) - floor(N_Diameters_downstream*D_nozzle_pixels), 1);
%% Concentration still assumed equal to orifice's value
OPL_pixels = sum(jetBW(ROI_rows,symAxis:end));
R_measurement = R_est(ROI_rows,symAxis);
% if TAU_OUTSIDE_INTEGRAL
%   R_measurement = R_measurement ./ tau_avg;
% end
disp(['R from measurement far away from nozzle = ',num2str(R_measurement)]);

%% Model T, C with a gaussian
if ROI_rows > numel(Y)
  Y_ROI = Y(end) + potential_core_slope*(ROI_rows-numel(Y));
else
  Y_ROI = Y(ROI_rows);
end
mu = max(floor(Y_ROI), 1);
Ta = C2K(atm_T);
% T0 = C2K(0.85*T_center_nozzle);
T0 = C2K(T_center_nozzle);
T_x2 = 0.1*(T0-Ta)+Ta; % ~2*sigma
sigma = GasHelper.get_sigma_of_gaussian(OPL_pixels,mu,Ta,T0, T_x2);
T_vect = T0*ones(1, OPL_pixels);
T_vect(1:mu) = linspace(T0, 0.99*T0, mu);
T_vect(mu:end) = GasHelper.gen_gaussian(mu:numel(T_vect), (0.99*T0-Ta), mu, sigma, Ta);
C0 = CO2_mfr/tot_mfr;
C_vect = C0*ones(1, OPL_pixels);
C_vect(mu:end) = GasHelper.gen_gaussian(mu:numel(C_vect), (C0-atm_co2_concentration), mu, sigma, atm_co2_concentration);
%% Estimating the measured radiation using the T,C models:
T_vect = T_vect( ~isnan(T_vect) & T_vect > C2K(params.Tamb_C)+1 );
C_vect = C_vect( ~isnan(T_vect) & T_vect > C2K(params.Tamb_C)+1 );
emission_sim = zeros(1, OPL_pixels);
calibParams2 = calibParams;
calibParams2.K = calibParams.K(ROI_rows,symAxis:symAxis+OPL_pixels-1);
% calibParams2.K = mean(calibParams2.K)*ones(size(calibParams2.K)); % TODO: check!!!!!!
jetPatams.OPL = 1/m2pixel*100;
params.hitranParams = hitranParams; params.wn_vect = wn_vect; params.optics_tf_wn = optics_tf_wn; params.calibParams = calibParams; %params.tau_spectra_wn = tau_spectra_wn;
tau_sim = zeros(1, OPL_pixels);
for indCol = 1 : OPL_pixels % Loop over pixels, convert models to R
  jetPatams.T = T_vect(indCol);
  jetPatams.co2_concentration = C_vect(indCol);
  % Check DB b4 time-consuming evaluation:
  params.jetPatams = jetPatams;
  [R_normalized, R_DB_struct] = R_DB_helper.get_R_from_DB(R_DB_struct, jetPatams.T, jetPatams.co2_concentration, jetPatams.OPL, params );
  emission_sim(indCol) = geometry .* calibParams2.K(indCol) .* R_normalized;
  % Get the transmission in each pixel:
  params.path_params = jetPatams;
  [tau_sim(indCol), TAU_DB_struct] = TAU_DB_helper.get_TAU_from_DB(TAU_DB_struct, jetPatams.T, jetPatams.co2_concentration, jetPatams.OPL, params );
end

%% Create simulated projection by Onion-Peeling:
% tau_sim = 0.995*ones(1, OPL_pixels); % Constant TAU approx.
[R_sim_projected] = GasHelper.forward_onion_peeling(emission_sim, tau_sim);

% R_sim_projected = GasHelper.ApplyForwardAbel(emission_sim);
% if TAU_OUTSIDE_INTEGRAL
%   R_sim_projected = R_sim_projected ./ tau_avg;
% end
disp(['R estimated far away from nozzle = ',num2str(R_sim_projected(1))]);

R_m = R_est(ROI_rows,symAxis:symAxis+OPL_pixels-1) - R_est(ROI_rows,symAxis+OPL_pixels-1);
[S,nS] = GasHelper.get_similarity_of_2_arrays(R_m, R_sim_projected,'NRMSE');
figure; plot(R_m,'DisplayName','R measured'); hold on;
neglected_jet_tau_str = '';
if sum(tau_sim - ones(1,OPL_pixels))==0
  neglected_jet_tau_str = ' \tau_j_e_t=1';
end
plot(R_sim_projected,'DisplayName',['R simulated:',neglected_jet_tau_str,' \mu=',num2str(mu),...
 ', T0=',num2str(T_center_nozzle),'C. RMSE=',num2str(S),' NRMSE=',num2str(nS)]);
legend('-DynamicLegend');





%% Save the updated DataBase:
tic
save(([pwd,'\',R_DB_file_name]),'R_DB_struct');
% save([pwd,'\',R_DB_file_name],'R_DB_struct','-v7.3'); % Save in version compatible with efficient partial loading/saving.
toc
end
