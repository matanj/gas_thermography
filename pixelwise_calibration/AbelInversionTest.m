%% Abel inversion Test
% 16.1.16 by MatanJ

% Based on code from:
% http://www.mathworks.com/matlabcentral/fileexchange/43639-abel-inversion-algorithm

%% Notes: The measurements are normalized always by 2R (max integration distance),
%% and R is evaluated from the user cropped area's borders, so be careful
%%
dbstop if error;
clc; clear; close all;

addpath(genpath('Z:\Matan\AbelInversionTest'));

%General paramters
simulation = 0; % simulated function
normalize = 0;  % normalize plots
normBy2R = 1;   % normalize measurements plot by 2R
CalcBothSizes = 1; % Calculate also inversion from symmetric axis leftwise.
% NOTE: In this case, the user should crop an area that contains the full
% jet crossection.

% Abel inversion code parameters
upf = 10;
plot_results = 0;
lsq_solve = 1;
inPixels = 1; % Added by me, to handle distance in [pixels]
%% %%%%% Simulating arbitrary f(r) and reconstruct it: %%%%%%%%%%%%
if simulation
    R = 2; %Radius
    dr = .05;
    X=(0:dr:R-dr)'; %Spatial coordinates
    theta = linspace(0,2*pi,length(X));
    [TH,rr] = meshgrid(theta,X);
    f = (17.*(rr./R).^4-32.*(rr./R).^3+14.*(rr./R).^2+1);
    [XX,YY] = pol2cart(TH,rr);
    % Simulate the abel transform that reflects the sensor integration
    % nature along LOS:
    h=zeros(length(X),1);    % allocate result vector
    for c=1:length(X) 
        x=X(c);
        % evaluate Abel-transform equation (1)
        fun = @(r) (17.*(r./R).^4-32.*(r./R).^3+14.*(r./R).^2+1).*r./sqrt(r.^2 - x.^2);
        h(c,1)=2*integral(fun,x,R);   
    end

    % The actual inversion
    [ f_rec , X ] = abel_inversion(h,R,upf,plot_results,lsq_solve,inPixels);
    % Plots:
    f_rec = repmat(f_rec,1,length(f_rec));
    err = f - f_rec;
    az = 0;
    el = 90;
    if normalize
        figure; surf(XX,YY,f./max(f(:))); title('Original function, normalized'); view(az, el); colorbar;
        figure; surf(XX,YY,f_rec./max(f_rec(:))); title('Reconstructed function, normalized'); view(az, el);colorbar;
        figure; surf(XX,YY,err./max(err(:))); title('Error, normalized'); colorbar;
    else
        figure; surf(XX,YY,f); title('Original function'); view(az, el); colorbar;
        figure; surf(XX,YY,f_rec); title('Reconstructed function'); view(az, el);colorbar;
        figure; surf(XX,YY,err); title('Error'); colorbar;
    end    
    
else
%% %%%%%%%%%% Real images %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Load images
    addpath(genpath('Z:\Matan\FLIR Matlab SDK'));
    inFile = 'Z:\Matan\Gas measurements\15_12_27\Jet425C\Jet_425C_3gs_0_45gs_CO_1.ptw'; %default
    bgFile = 'Z:\Matan\Gas measurements\15_12_27\Jet425C\Jet_425C_3gs_0_gs_CO_1.ptw'; %default
    [inFile, inFilePath] = uigetfile({'*.ptw*','ptw Camera files'},'Select input ptw file',inFile);
    [bgFile, bgFilePath] = uigetfile({'*.ptw*','ptw Camera files'},'Select BackGround ptw file',bgFile);
    assert(~isequal(inFile,0) && ~isequal(bgFile,0));
    addpath(inFilePath);
    addpath(bgFilePath);

    ptwInfo_in = PTWFileUtils.getFileInfo(inFile);
    ptwInfo_bg = PTWFileUtils.getFileInfo(bgFile);
    inIT = ptwInfo_in.m_integration;
    bgIT = ptwInfo_bg.m_integration;
    Fs = ptwInfo_in.m_frameperiod; %sample rate [Hz]
    assert(abs(inIT-bgIT) < 1e-5, 'IT values don''t match');

    im_set = PTWFileUtils.ReadAllFramesFromFile(inFile);
    BG_set = PTWFileUtils.ReadAllFramesFromFile(bgFile);
    BG = mean(BG_set,3);
    im_sub = bsxfun(@minus, double(im_set), BG);
    nImgs = size(im_set,3);
    im_avg = mean(im_sub,3);

    %% Calc the symmetry axis
    imAxisMarked = im_avg;
    fig = figure; imagesc(im_avg); impixelinfo;
    [lines, rect] = imcrop;
    %round coordinates, as they are fractions:
    rect(1:2) = ceil(rect(1:2));
    rect(3:4) = floor(rect(3:4));
    [M,N] = size(lines);
    [~,Idx] = max(lines,[],2);
    rows = (rect(2):rect(2)+rect(4))';
    cols = rect(1)+Idx-1;
    avgCol = round(mean(cols));

    imAxisMarked(rows,avgCol) = min(im_avg(:)); % Just for plot purpose
    figure(fig); imagesc(imAxisMarked);impixelinfo;
    %% Apply Abel inversion per row
    line = lines(M,:);
    for ii = 1:1+CalcBothSizes
        plot_str = '';
        if ii == 2
            % [1 2 9 3 3 0 0 0 ]
             [~,Idx] = max(line,[],2); % Idx = 3
            halfLine = flipud(line(1:Idx)')'; % flip the line and do same as b4
            % [9 2 1]
            plot_str = ', this time from the axis leftwise.';
        else
            axisIdx = avgCol - rect(1);
            halfLine = line(axisIdx:end);
        end
        disp(['measurement value @ first index = ',num2str(halfLine(1))]);
%         f_recs = zeros(length(halfLine),M);
    
        % Loop is done once for now, only for the bottom line in the cropped area
        M = 1;
        lines = lines(M,:);
        for l=1:M 
            line = lines(l,:);
            [~,Idx] = max(line,[],2);
            if ii == 2
                halfLine = flipud(line(1:Idx)')';
            else
                halfLine = line(Idx:end);
%                 halfLine = line(axisIdx:end);
            end
            %% Abel inversion parameters
            h = halfLine';
            if inPixels
                R = length(halfLine);
            else
                R = 0.5; % [m]
            end
            % The actual inversion
            [ f_rec , X ] = abel_inversion(h,R,upf,plot_results,lsq_solve,inPixels);
%             f_recs(:,l) = f_rec;
        end
        %% Plots
        theta = linspace(0,2*pi,10*length(X));
        [th,rr] = meshgrid(theta,X);
        [XX,YY] = pol2cart(th,rr);
        f_rec = repmat(f_rec,1,length(theta));
        halfLine = halfLine';
        halfLine_rep = repmat(halfLine,1,length(theta));
        normFactor = normBy2R*2*R + ~normBy2R;
        halfLine_rep = halfLine_rep./normFactor; % normalize measurements by max integration distance, 2R

        az = 15; el = 15;
        if normalize
            figure; surf(XX,YY,halfLine_rep./max(halfLine_rep(:))); title(['Original Radiation, normalized (replicated along \theta)',plot_str]); view(az, el);colorbar;
            figure; surf(XX,YY,f_rec./max(f_rec(:))); title(['Reconstructed Radiation, normalized (replicated along \theta)',plot_str]); view(az, el);colorbar
        else
            figure; surf(XX,YY,halfLine_rep); title(['Original Radiation (replicated along \theta)',plot_str]); view(az, el);colorbar;
            figure; surf(XX,YY,f_rec); title(['Reconstructed Radiation (replicated along \theta)',plot_str]); view(az, el);colorbar;
        end
    end
end