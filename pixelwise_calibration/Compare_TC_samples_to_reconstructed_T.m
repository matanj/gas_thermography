global CL_arr CL_meas lateral_meas lateral_col d_cl d_lateral text_props
CL_arr = fliplr(size(T_im,1):-D_nozzle_pixels:1);
CL_meas = C2K([325, 355, 377, 382, 387]'); % T.C. samples on centerline 
lateral_meas = C2K([265, 280, 300, 340, 365]');% T.C. samples on lateral column 
lateral_col = 40;  % Lateral column of interest
d_cl = CL_meas - T_im(CL_arr, 1);
d_lateral = lateral_meas - T_im(CL_arr, lateral_col);

GasHelper.plotIm(T_im);
title(sprintf('Reconstructed Temperature [K]\n(red: deviations from T.C. samples)'));
normalize_axes_half_im(T_im, D_nozzle_pixels, 2, 5);
hold on;
plot(ones(size(CL_arr)), CL_arr,'.r','MarkerSize',10);
plot(lateral_col*ones(size(CL_arr)), CL_arr,'.r','MarkerSize',10);
text_props = {'Color','r','FontSize',11};
text(ones(size(CL_arr)), CL_arr, num2str(d_cl, 3),text_props{:});
text(lateral_col*ones(size(CL_arr)), CL_arr, num2str(d_lateral, 3),text_props{:});

