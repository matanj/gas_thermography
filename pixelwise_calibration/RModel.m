function y = RModel(x, parameters)
%% The model of R as function of input variables vector 'x' and 'parameters'
% Here x = [e, tr, Kw, a0]' and 'parameters' is a strucure with the fields 'calib_T' and 'T'.
y = GasHelper.T2R(parameters.T, parameters.calib_T, x(1), x(2), x(3), x(4));
end
