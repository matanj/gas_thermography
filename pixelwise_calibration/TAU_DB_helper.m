classdef TAU_DB_helper
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% HITRAN's DB - Stores TAU(T,C,OPL) %%%%%
% NEW ASSUMPTION: trasmission could be approximated by an averaged value,
% thus decoupled from the radiative integral.
% DB structure's fields:
%   1) T,C,OPL arrays.
%   2) TAU(T,C,OPL) stores: weighted averaged tau
%   3) Atmospheric parameters struct.
%   4) HITRAN parameters struct.
%   5) Description.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  properties (Constant)
  end
  
  methods (Static = true)
    
    function [closest_val, closest_ind] = get_closest_val_in_array(A, val)
      [~, closest_ind] = min(abs(A-val));
      closest_val = A(closest_ind); % Finds first one only!
    end
    
    function [S] = get_nearest_DB_entry(DB, Tq, Cq, OPLq)
      [S.T, S.Ti]     = TAU_DB_helper.get_closest_val_in_array(DB.T, Tq);
      [S.C, S.Ci]     = TAU_DB_helper.get_closest_val_in_array(DB.C, Cq);
      [S.OPL, S.OPLi] = TAU_DB_helper.get_closest_val_in_array(DB.OPL, OPLq);
    end
    
    function DB = update_TAU_DB(DB, Tq, Cq, OPLq, new_TAU)
      % See Image Analyist's comment @
      % "https://www.mathworks.com/matlabcentral/answers/49390-pass-variable-by-reference-to-function"
      S = TAU_DB_helper.get_nearest_DB_entry(DB, Tq, Cq, OPLq);
      if ~(DB.TAU(S.Ti, S.Ci, S.OPLi))
        DB.TAU(S.Ti, S.Ci, S.OPLi) = new_TAU;
      end
    end
    
    function isMatch = is_DB_legit_to_case(DB, hitran_params)
      % Below works ONLY if structs have same field ordering!!!
      % But Since DB is created according to the case' ordering, should be OK.
      
      % Fix for the case of different full paths:
      [~,DB_name,DB_ext] = fileparts(DB.hitranParams.hitranFileFullPath);
      [HP_path,HP_name,HP_ext] = fileparts(hitran_params.hitranFileFullPath);
      if isequal([DB_name,DB_ext], [HP_name,HP_ext]) % we care only about the file
        DB.hitranParams.hitranFileFullPath = fullfile(HP_path,[DB_name DB_ext]);
      end

      isMatch = isequal(DB.hitranParams, hitran_params);
    end
    
    function [TAU_avg, DB] = get_TAU_from_DB(DB, Tq, Cq, OPLq, params)
      % 'params' should include: hitranParams, optics_tf_wn & path_params.
      S = TAU_DB_helper.get_nearest_DB_entry(DB, Tq, Cq, OPLq);
      if ~(DB.TAU(S.Ti, S.Ci, S.OPLi))
        % Average the spectral transmissivity with weights of Planck &
        % optics transfer function:
        TAU_avg = GasHelper.getWeightedTransmittance(params.hitranParams, params.optics_tf_wn, params.path_params);
        DB = TAU_DB_helper.update_TAU_DB(DB, S.T, S.C, S.OPL, TAU_avg);
      else
        TAU_avg = DB.TAU(S.Ti,S.Ci,S.OPLi);
      end
    end
    
    function [TAU_avg] = get_TAU_from_full_DB(F, Tq, Cq, OPLq)
      %get interpolated TAU from a FULL DB (doesn't update it) for the given
      %vectors of queries.
      TAU_avg = F(Tq, Cq, OPLq);
    end
    
  end
end