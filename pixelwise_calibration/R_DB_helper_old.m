classdef R_DB_helper_old
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% HITRAN's DB - Stores R(T,C,OPL) for a given atmospheric scenario (tau) %%%%%
% DB structure's fields:
%   1) T,C,OPL arrays.
%   2) R(T,C,OPL) stores: integral(radiance_photon_flux .* tau .* optics_tf_wn)
%   3) Atmospheric parameters struct.
%   4) HITRAN parameters struct.
%   5) Description.
% Use like: R_simulated = gemoetry_factor * K * R(T,C,OPL).
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  properties (Constant)
  end
  
  methods (Static = true)
    
    function [closest_val, closest_ind] = get_closest_val_in_array(A, val)
      [~, closest_ind] = min(abs(A-val));
      closest_val = A(closest_ind); % Finds first one only!
    end
    
    function [S] = get_nearest_DB_entry(DB, Tq, Cq, OPLq)
      [S.T, S.Ti]     = R_DB_helper.get_closest_val_in_array(DB.T, Tq);
      [S.C, S.Ci]     = R_DB_helper.get_closest_val_in_array(DB.C, Cq);
      [S.OPL, S.OPLi] = R_DB_helper.get_closest_val_in_array(DB.OPL, OPLq);
    end
    
    function DB = update_R_DB(DB, Tq, Cq, OPLq, new_R)
      % See Image Analyist's comment @
      % "https://www.mathworks.com/matlabcentral/answers/49390-pass-variable-by-reference-to-function"
      S = R_DB_helper.get_nearest_DB_entry(DB, Tq, Cq, OPLq);
      if ~(DB.R(S.Ti, S.Ci, S.OPLi))
        DB.R(S.Ti, S.Ci, S.OPLi) = new_R;
      end
    end
    
    function isMatch = is_DB_legit_to_case(DB, case_atm_params, hitran_params)
      % Below works ONLY if structs have same field ordering!!!
      % But Since DB is created according to the case' ordering, should be OK.
      isMatch = isequal(DB.atm_params, case_atm_params) & ...
        isequal(DB.hitranParams, hitran_params);
    end
    
    function [R_normalized, DB] = get_R_from_DB(DB, Tq, Cq, OPLq, params)
      % 'params' should include: hitranParams, jetPatams, wn_vect,
      % calibParams, tau_spectra_wn.
      S = R_DB_helper.get_nearest_DB_entry(DB, Tq, Cq, OPLq);
      if ~(DB.R(S.Ti, S.Ci, S.OPLi))
        [~,~,raw_radiance_wn] = MyGetSpectrumGPU(false, 'radiance', params.hitranParams, params.jetPatams); % in [W/sr/cm^2/cm^-1]
        radiance_photon_flux_spectra_wn = raw_radiance_wn ./ GasHelper.getPhotonEnergyFromWN(params.wn_vect); % convert to photons flux [#photons/s/sr/cm^2/cm^-1]
        % get "normalized R" to store in DB by calling with geometry_factor==1
        % and K==1:
        calibParams_temp = params.calibParams;
        calibParams_temp.K = 1;
        R_normalized = GasHelper.getRfromSpectralCalibration(calibParams_temp, 1, params.wn_vect, radiance_photon_flux_spectra_wn, params.tau_spectra_wn);
        DB = R_DB_helper.update_R_DB(DB, S.T, S.C, S.OPL, R_normalized);
      else
        R_normalized = DB.R(S.Ti,S.Ci,S.OPLi);
      end
    end
    
    
  end
end