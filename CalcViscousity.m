function [mu_CO2, mu_air] = CalcViscousity(T)
% Estimates viscousity in [Pa * s] (or N * s / m^2) at the 
% desired temperature T[K] for CO2 and Air
C_CO2=240; %[K]
T0_CO2=293.15; %[K]
mu0_CO2=14.8; %[uPa * s]
C_air=120;%[K]
T0_air=291.15;%[K]
mu0_air=18.27;%[uPa * s]

mu_CO2 = 1e-6 * mu0_CO2*(T0_CO2+C_CO2)/(T+C_CO2)*(T/T0_CO2)^1.5;
mu_air = 1e-6 * mu0_air*(T0_air+C_air)/(T+C_air)*(T/T0_air)^1.5;
end