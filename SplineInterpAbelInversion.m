function [ f ] = SplineInterpAbelInversion( I, R )
%Implements: Spline interpolation based Abel inversion, by:
%Deutsch and Beniaminy (1983) - "Inversion of Abel�s integral equation for experimental data"
%In particular, option (c) is implemented.

if nargin < 2
  error('Invalid args!');
end
I = I(:);
f = zeros(size(I));

k1 = @(y) -1/sqrt(y.^2-r.^2);
k2 = @(y) -y./sqrt(y.^2-r.^2) + log(y+sqrt(y.^2-r.^2));
k3 = @(y) sqrt(y.^2-r.^2).*(1-r.^2);
k4 = @(y) y/2.*sqrt(y.^2-r.^2) - y.*r.^2./sqrt(y.^2-r.^2) + 3*r.^2.*log(0.5*(y+sqrt(y.^2-r.^2)));
K1 = @(a,b) k1(a) - k1(b);
K2 = @(a,b) k2(a) - k2(b);
K3 = @(a,b) k3(a) - k3(b);
K4 = @(a,b) k4(a) - k4(b);

for r = 0 : R-1 % loop over r:
  for 
  f(r+1) = 
end
f = -f/pi;

end

