function f = NestorOlsenAbelInversion(I, R)
%Implements: O. H. NESTOR AND H. N. OLSEN, 1960 - "NUMERICAL METHODS FOR REDUCING LINE
%AND SURFACE PROBE DATA"
% X is the lateral axis, R is the radial.
% Here I(x) is Q(x) in the original paper.
% R is the radius.
% n is the index of X <==> X = a*n, and a = R/(N-1)
% k is the radial index.

% f(k) = f(a*k) = -2/pi/a* sum(from n=k to N of B(k,n)*I(n)) 
% where:
%   B(k,n) = -A(k,k)            ; if n == k
%   B(k,n) = A(k,n-1) - A(k,n)  ; if n >= k+1
% And:
%   A(k,n) = ( sqrt((n+1)^2-k^2) - sqrt(n^2-k^2) ) / (2*n+1)
if nargin < 2
  error('Invalid args!');
end
I = I(:);
f = zeros(size(I));
N = numel(I);
a = R/(N-1);%dx
for k = 0:N-1 % over radial axis:
  n = k:N;
  A = ( sqrt((n+1).^2-k.^2) - sqrt(n.^2-k.^2) ) ./ (2*n+1);
  B = -[A(1), diff(A)];
  f(k+1) = sum(B*I(k+1));
end
 f = -2/a/pi * f;

end