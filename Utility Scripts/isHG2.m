function tf = isHG2(fig)
try
  tf = ~verLessThan('MATLAB', '8.4') || isa(fig,'matlab.ui.Figure') || ~graphicsversion(fig, 'handlegraphics');
catch
  tf = false;
end