function outString = fitToText(fitObj,n2s_prec,bForceStrrep)
% This function returns the equation of a fit as a string such that all coefficients
% are replaced with the fitted values. The output string is convertible to a function 
% handle using str2func(outString).
% Iliya @ 17/08/2015
if ~exist('n2s_prec','var') || ~isinteger(n2s_prec) || ~isscalar(n2s_prec)
  N2S_PRECISION = 10;
else
  N2S_PRECISION = n2s_prec;
end
if ~exist('bForceStrrep','var') || ~islogical(bForceStrrep) || ~isscalar(bForceStrrep)
  FORCE_STRREP = false;
else
  FORCE_STRREP = bForceStrrep; 
end
outString=formula(fitObj);
cen=coeffnames(fitObj);
cev=cellstr(num2str(coeffvalues(fitObj)',N2S_PRECISION));
can=argnames(fitObj);

if license('test','Symbolic_Toolbox') && ~FORCE_STRREP
  fun = sym(outString);
  for k=1:numel(cev)
    fun = subs(fun,cen{k},cev{k});
  end
  outString = char(fun);
else
  % We replace the longest variables first:
  toReplace = sortrows([cen cev num2cell(cellfun(@length,cen))],-3);
  for k=1:numel(cev)
    outString = strrep(outString,toReplace{k,1},num2str(toReplace{k,2}));
  end
end
% Cleanup:
outString = strrep(outString,' ',''); outString = strrep(outString,'+-','-');
outString = ['@(' strjoin(setdiff(can,cen)',',') ')' outString];

%{
Other ideas:

  elseif license('test','Curve_Fitting_Toolbox')
    mFittype = fittype(outString);
#############################
  syms_var = strjoin(cen,' ');
  eval(sprintf('syms %s;',syms_var))
#############################
  for k=1:length(cev),
    eval([cen{k} ' = ' num2str(cev(k)) ';']);
  end
#############################
  argnames = symvar(fun);
  args_char = cell(numel(argnames),1);
  for k=1:numel(argnames)
    args_char{k} = char(argnames(k));
  end
  fInputsStr = ['@(' strjoin(setdiff(args_char,cen)',',') ')'];
%}