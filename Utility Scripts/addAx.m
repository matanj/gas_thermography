function hNewAx = addAx(hFigContainingAxes, toX)

NORMALIZED_Y_OFFSET = 0.1;
NORMALIZED_X_OFFSET = 0.1;

if nargin==0 %// Debugging Code
 hFigContainingAxes = figure();
 hOldAx = axes('units','normalized','position',[.1 .1 .85 .85],'xlim',[0 144],'xtick',0:12:144);
 xlabel(hOldAx,'Some x label'); ylabel(hOldAx,'Some y label');
 toX = randi(2)-1;
end

existingAxes = findobj(hFigContainingAxes,'type','axes');
% Find bottom axes and leftmost axes:
allpos = vertcat(existingAxes.Position); allpos(allpos==eps)=NaN;
npos = {nanmax(allpos,[],1) , nanmin(allpos,[],1)};
if toX 
  npos = [npos{1}(1), npos{2}(2), npos{1}(3), eps];
else
  npos = [npos{2}(1), npos{1}(2), eps, npos{1}(4)];
end

for ind1 = 1:numel(existingAxes)
  if toX  
    existingAxes(ind1).Position = existingAxes(ind1).Position + [0,1,...
                          0,-1*(existingAxes(ind1).Position(4)~=eps)]*NORMALIZED_Y_OFFSET;
  else
    existingAxes(ind1).Position = existingAxes(ind1).Position + [1,0,...
                          -1*(existingAxes(ind1).Position(3)~=eps) 0]*NORMALIZED_X_OFFSET;  
  end
end

if toX  
  hNewAx = axes('units','normalized','position',npos,'color','none');
else
  hNewAx = axes('units','normalized','position',npos,'color','none');
end