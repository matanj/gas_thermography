%MAXIMIZE  Maximize a figure window to fill the entire screen
%
% Examples:
%   maximize
%   maximize(hFig)
%
% Maximizes the current or input figure so that it fills the whole of the
% screen that the figure is currently on. This function is platform
% independent.
%
%IN:
%   hFig - Handle of figure to maximize. Default: gcf.

function maximize(hFig)
if nargin < 1
    hFig = gcf;
end
% Enlarge figure to full screen.
warning('off','MATLAB:HandleGraphics:ObsoletedProperty:JavaFrame');
jFrame = get(hFig,'JavaFrame'); 
warning('on','MATLAB:HandleGraphics:ObsoletedProperty:JavaFrame');
drawnow; pause(0.1);
if isHG2
  jFrame.fHG2Client.setMaximized(true);  % HG2
else
  jFrame.fHG1Client.setMaximized(true);  % HG1
end

function tf = isHG2(fig)
try
  tf = ~verLessThan('MATLAB', '8.4') || isa(fig,'matlab.ui.Figure') || ~graphicsversion(fig, 'handlegraphics');
catch
  tf = false;
end