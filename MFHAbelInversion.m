function [ e_r ] = MFHAbelInversion( I, R, alpha )
% Implementing MA (2008) - "Modified Fourier�Hankel method based on
% analysis of errors in Abel inversion using Fourier transform techniques".

% 'I' - ????full???? row of the forward Abel transformation (Camera's
% apparent radiation). Expecting both limits nearly 0, and max in I(n).
% 'R' - the radius.
% 'alpha' in the range (0,1], typically alpha ~ 0.1.
if nargin < 2
  error('Invalid args!');
end

if nargin < 3
  alpha = 0.1; % good for lots of cases.
end
%{
DEBUG: MATLAB besselj Vs reported one:
R = 10;
n = R;
r = 1/R*(0:R-1)';  
e_r = zeros(size(r));
x = r;
J0 = besselj(0, r); % zero-order Bessel function of the 1st kind, for eq. (10)
for i = 1:n
    xx = x(i);
    func = @(xx) sin(xx)./sqrt(xx.^2-r(i).^2);
    J00(i) = 2/pi*integral(func, r(i), 10000);
end
figure; plot(r, J0,'r', r, J00, 'b')
%}
n = numel(I)/2;
if numel(I) ~= 2*n
  error('radius and size of data mismatch!');
end
I = I(:); % assuring it's a column vector
r = (0:n-1)'*R/(n-1);
% r = r*R; % TODO: ??????????????????
e_r = zeros(size(r));
%{
% Eq.(23) - simplest, slowest implementation:
tic
for i = 0:n-1 % over each r_i
  outer_sum = 0;
  for j = -n : n-1;
    inner_sum = 0;
    for k = 1 : floor(n/alpha)
      inner_sum = inner_sum + k.*besselj(0,alpha*i*k*pi/n).*cos(alpha*j*k*pi/n);
    end
    outer_sum = outer_sum + I(j+n+1)*inner_sum;
  end
  e_r(i+1) = outer_sum;
end
e_r = alpha^2*pi/2/n/R * e_r;
toc
%}
%{
% Eq.(23) - improved, but still can be better:
for i = 0:n-1 % over each r_i
  outer_sum = 0;
  for j = -n : n-1;
    k = 1 : floor(n/alpha);% row vector
    J0 = besselj(0,alpha*i*k*pi/n); % row vector
    inner_sum = (k.*J0)*cos(alpha*j*k*pi/n)'; % calc the inner sum as an inner product
    outer_sum = outer_sum + I(j+n+1)*inner_sum;
  end
  e_r(i+1) = outer_sum;
end
e_r = alpha^2*pi/2/n/R * e_r;

%%
% Eq.(23) - best so far: precalculation of J0 & COS matrices
i = 0:n-1;
j = -n:n-1;
k = 1: floor(n/alpha);
[jj, kk] = meshgrid(j,k);
CS = cos(alpha.*jj.*kk.*pi./n);
[kk,ii] = meshgrid(k,i);
J0 = besselj(0, alpha.*ii.*kk.*pi./n );
for i = 0:n-1 % over each r_i
  outer_sum = 0;
  for j = -n : n-1;
    inner_sum = (k.*J0(i+1,:))*CS(:,j+n+1); % calc the inner sum as an inner product
    outer_sum = outer_sum + I(j+n+1).*inner_sum;
  end
  e_r(i+1) = outer_sum;
end
e_r = alpha^2*pi/2/n/R * e_r;
%}

%% Eq.(23) - best so far: precalculation of J0 & COS matrices, with 1 loop
i = 0:n-1;
j = -n:n-1;
k = 1: floor(n/alpha);
[jj, kk] = meshgrid(j,k);
CS = cos(alpha.*jj.*kk.*pi./n);
[kk,ii] = meshgrid(k,i);
J0 = besselj(0, alpha.*ii.*kk.*pi./n );
for i = 0:n-1 % over each r_i
  % Left term in the RHS is the inner sum calced as an inner product, and
  % entire RHS is then the outer sum, again calced as an inner product:
  e_r(i+1) = ( (k.*J0(i+1,:))*CS(:,j+n+1) ) * I(j+n+1);
end
e_r = alpha^2*pi/2/n/R * e_r;

end

