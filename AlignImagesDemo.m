%% Demo of estimation geometric transformation to align 2 images using phase correlation
%
%Ref:
%http://www.mathworks.com/help/images/ref/imregcorr.html

close all; clc;

fixed  = imread('cameraman.tif');

theta = 20;
S = 2.3;
tform = affine2d([S.*cosd(theta) -S.*sind(theta) 0; S.*sind(theta) S.*cosd(theta) 0; 0 0 1]);
moving = imwarp(fixed,tform);
moving = moving + uint8(10*rand(size(moving)));

figure, imshowpair(fixed,moving,'montage');

tformEstimate = imregcorr(moving,fixed);

Rfixed = imref2d(size(fixed));
movingReg = imwarp(moving,tformEstimate,'OutputView',Rfixed);
 
figure, imshowpair(fixed,movingReg,'montage');
figure, imshowpair(fixed,movingReg,'falsecolor');